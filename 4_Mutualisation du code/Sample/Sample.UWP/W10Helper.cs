﻿using Sample.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.UWP
{
    public class W10Helper : IPlatformHelper
    {
        public async void CallPhone(string phoneNumber)
        {
            //On utilise l'application Skype pour lancer un appel
            Uri uri = new Uri(string.Format("skype:{0}?call", phoneNumber));
            await Windows.System.Launcher.LaunchUriAsync(uri);

        }
    }

}
