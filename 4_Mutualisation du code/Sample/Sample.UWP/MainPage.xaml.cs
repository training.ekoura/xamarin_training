﻿using Sample.Core;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Sample.UWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            // En enregistrement au locator
            Locator locator = Locator.Instance;
            locator.Register<IPlatformHelper>(() => new W10Helper());

            //Execution d'un algorithme partagé dans la PCL
            ClasseMetierCommune instance = new ClasseMetierCommune();
            instance.ExecuterCodeCommun();
        }
    }
}
