using Foundation;
using Sample.Core;
using UIKit;

namespace Sample.iOS
{
    public class iOSHelper : IPlatformHelper
    {

        public void CallPhone(string phoneNumber)
        {
            var url = new NSUrl("tel:" + phoneNumber);
            UIApplication.SharedApplication.OpenUrl(url);
        }
    }

}