﻿using Sample.Core;
using System;

using UIKit;

namespace Sample.iOS
{
    public partial class ViewController : UIViewController
    {
        public ViewController(IntPtr handle) : base(handle)
        {
            // Enregistrement au locator
            Locator locator = Locator.Instance;
            locator.Register<IPlatformHelper>(() => new iOSHelper());

            //Execution d'un algorithme partagé dans la PCL
            ClasseMetierCommune instance = new ClasseMetierCommune();
            instance.ExecuterCodeCommun();

        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}