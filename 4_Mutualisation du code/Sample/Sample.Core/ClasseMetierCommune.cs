﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.Core
{
    public class ClasseMetierCommune
    {
        public void ExecuterCodeCommun()
        {
            Locator.Instance.GetInstance<IPlatformHelper>().CallPhone("01010101");
        }
    }
}
