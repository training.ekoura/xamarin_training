﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sample.Core
{
    public class Locator
    {
        /// <summary>
        /// Dictionnaire d’interfaces enregistré dans le locator
        /// </summary>
        readonly Dictionary<Type, Func<object>> _cache = new Dictionary<Type, Func<object>>();

        static Locator _instance = new Locator();

        private Locator()
        {
        }

        /// <summary>
        /// Obtient l'instance du locator
        /// </summary>s
        /// <value>
        /// The instance.
        /// </value>
        public static Locator Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///  Méthode permettant d’enregistrer un type au locator
        /// </summary>
        /// <typeparam name="TInterface">The type of the interface.</typeparam>
        /// <param name="createHandler">The create handler.</param>
        public void Register<TInterface>(Func<object> createHandler)
                where TInterface : class
        {
            var @interface = typeof(TInterface);
            if (!_cache.ContainsKey(@interface))
                _cache.Add(@interface, createHandler);
            else
                _cache[@interface] = createHandler;
        }

        /// <summary>
        /// Cree une instance du type de l'interface
        /// </summary>
        public TInterface GetInstance<TInterface>()
            where TInterface : class
        {
            var type = typeof(TInterface);
            if (!_cache.ContainsKey(type))
                throw new TypeLoadException(type.Name);
            return _cache[type].Invoke() as TInterface;
        }



    }

}
