﻿using Android.App;
using Android.Content;
using Sample.Core;

namespace Sample.Droid
{
    public class AndroidHelper : IPlatformHelper
    {
        public void CallPhone(string phoneNumber)
        {
            var strUri = string.Format("tel:{0}", phoneNumber);
            var uri = global::Android.Net.Uri.Parse(strUri);
            var intent = new Intent(Intent.ActionDial, uri);
            intent.AddFlags(ActivityFlags.NewTask);
            Application.Context.StartActivity(intent);
        }
    }
}
