﻿using Android.App;
using Android.OS;
using Sample.Core;

namespace Sample.Droid
{
    [Activity(Label = "Sample.Droid", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            // Enregistrement au locator
            Locator locator = Locator.Instance;
            locator.Register<IPlatformHelper>(() => new AndroidHelper());

            // Execution d'un algorithme partagé dans la PCL
            ClasseMetierCommune instance = new ClasseMetierCommune();
            instance.ExecuterCodeCommun();
        }
    }
}

