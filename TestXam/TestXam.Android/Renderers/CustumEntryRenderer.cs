﻿using Android.Content;
using TestXam.Controls;
using TestXam.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustumEntry), typeof(CustumEntryRenderer))]
namespace TestXam.Droid.Renderers
{
    public class CustumEntryRenderer : EntryRenderer
    {
        public CustumEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.SetBackgroundColor(global::Android.Graphics.Color.LightGreen);
            }
        }
    }
}
