﻿using System;
using SQLite;
using TestXam.ViewModels;

namespace TestXam.Models
{
    public class Person : BaseViewModel
    {
        private string _name;
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private string _LastName;
        public string LastName
        {
            get => _LastName;
            set => SetProperty(ref _LastName, value);
        }

        private string _Email;
        public string Email {
            get => _Email;
            set => SetProperty(ref _Email, value);
        }
    }
}
