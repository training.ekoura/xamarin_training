﻿using System;
using SQLite;

namespace TestXam.Models
{
    public class Personne
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public string Name
        {
            get ; set ;
        }

        public string LastName
        {
            get; set;
        }

        public string Email
        {
            get; set;
        }
    }
}
