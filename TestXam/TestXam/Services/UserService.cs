﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SQLite;
using TestXam.Config;
using TestXam.Models;

namespace TestXam.Services
{
    public class UserService
    {

        private string wsLink = "https://jsonplaceholder.typicode.com/users";

        public async Task<List<User>> GetUsersList() {

            using (var http = new HttpClient()) {

                var result = await http.GetStringAsync(wsLink);
                var jsonResult = JsonConvert.DeserializeObject<List<User>>(result);
                return jsonResult;
            }
        }

        #region Database

        readonly SQLiteAsyncConnection _database;

        public UserService()
        {
            _database = new SQLiteAsyncConnection(DataBaseConfig.FilePath);
            _database.CreateTableAsync<Personne>().Wait();
        }

        public Task<List<Personne>> GetPersonnesAsync()
        {
            return _database.Table<Personne>().ToListAsync();
        }

        public Task<Personne> GetPersonneAsync(int id)
        {
            return _database.Table<Personne>()
                            .Where(i => i.ID == id)
                            .FirstOrDefaultAsync();
        }

        public Task<int> SavePersonneAsync(Personne Personne)
        {
            if (Personne.ID != 0)
            {
                return _database.UpdateAsync(Personne);
            }
            else
            {
                return _database.InsertAsync(Personne);
            }
        }

        public Task<int> DeletePersonneAsync(Personne Personne)
        {
            return _database.DeleteAsync(Personne);
        }

        #endregion

    }
}
