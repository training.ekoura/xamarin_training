﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace TestXam.Converters
{
    public class StringLengthColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!string.IsNullOrEmpty((string)value)){
                if (((string)value).Length > 30)
                    return Color.Red;
                else
                    return Color.Green;
            }
            else
                return Color.Red;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
