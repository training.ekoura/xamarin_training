﻿using System;
using System.Collections.ObjectModel;
using SQLite;
using TestXam.Models;
using TestXam.Services;
using Xamarin.Forms;

namespace TestXam.ViewModels
{
    public class PersonViewModel : BaseViewModel
    {
        private UserService userService; 

        //private bool
        public bool AddUserIsEnable {
            get;
            set;
        }

        public Command AddUserCommand { get; }

        public Command AddUserWithParam { get;  }

        public Person CurrentUser { get; set; }

        public ObservableCollection<Models.Person> Persons { get; set; }
        public ObservableCollection<Models.User> Users { get; set; }
        public ObservableCollection<Personne> Personnes { get; set; }

        public PersonViewModel()
        {

            AddUserIsEnable = false;
            AddUserCommand = new Command(AddUser);

            userService = new UserService();

            AddUserWithParam = new Command<string>(user=> AddUser(user));

            CurrentUser = new Person();
            Persons = new ObservableCollection<Person>();
            Personnes = new ObservableCollection<Personne>();
            Users = new ObservableCollection<User>();

            InitializeData();


        }

        async void InitializeData()
        {
            var listUsersRetrieved = await userService.GetUsersList();

            foreach (var user in listUsersRetrieved)
            {
                Users.Add(user);
            }

            var listPersonnes = await userService.GetPersonnesAsync();
            foreach (var pers in listPersonnes)
            {
                Personnes.Add(pers);
            }


        }

        private async void AddUser() {

            var p = new Personne()
            {
                Email = CurrentUser.Email,
                Name = CurrentUser.Name,
                LastName = CurrentUser.LastName
            };
            await userService.SavePersonneAsync(p);

            CurrentUser.Name = "";
            CurrentUser.LastName = "";
            CurrentUser.Email = "";
        }


        private void AddUser(string t)
        {
            var param = t;
            Persons.Add(
                new Person()
                {
                    Email = CurrentUser.Email,
                    Name = CurrentUser.Name,
                    LastName = CurrentUser.LastName
                }
            );
            CurrentUser.Name = "";
            CurrentUser.LastName = "";
            CurrentUser.Email = "";
        }

        public void DoSomeThing()
        {
            CurrentUser.Name = "Je fais un test";
        }
    }
}
