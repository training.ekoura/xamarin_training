﻿using System;
using TestXam.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace TestXam.ViewModels
{
    public class CompassViewModel : BaseViewModel
    {
        public Command StartCommand { get; }
        public Command StopCommand { get; }
        public Command SendEmail { get; }


        private string headingDisplay;
        public string HeadingDisplay
        {
            get => headingDisplay;
            set => SetProperty(ref headingDisplay, value);
        }

        private double heading;
        public double Heading
        {
            get => heading;
            set => SetProperty(ref heading, value);
        }

        public CompassViewModel()
        {
            StartCommand = new Command(Start);
            StopCommand = new Command(Stop);
            SendEmail = new Command(EmailSender);

            //InitializeData();
        }

        

        private void EmailSender(object obj)
        {
            var message = new EmailMessage
            {
                Subject = "Infos",
                Body = "Need some news",
                To = new System.Collections.Generic.List<string>() { "godwin.avodagbe@ekoura.com"}
                //Cc = ccRecipients,
                //Bcc = bccRecipients
            };
            Email.ComposeAsync(message);
        }

        void Start()
        {
            if (Compass.IsMonitoring)
                return;

            Compass.ReadingChanged += Compass_ReadingChanged;
            Compass.Start(SensorSpeed.UI);
        }

        void Stop()
        {
            if (!Compass.IsMonitoring)
                return;
            Compass.Stop();
            Compass.ReadingChanged -= Compass_ReadingChanged;
        }

        private void Compass_ReadingChanged(object sender, CompassChangedEventArgs e)
        {
            Heading = e.Reading.HeadingMagneticNorth;
            HeadingDisplay = string.Format("Heading : {0}", Heading.ToString());
        }
    }
}
