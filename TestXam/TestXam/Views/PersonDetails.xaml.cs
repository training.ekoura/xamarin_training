﻿using System;
using System.Collections.Generic;
using TestXam.Models;
using TestXam.ViewModels;
using Xamarin.Forms;

namespace TestXam.Views
{
    public partial class PersonDetails : ContentPage
    {
        public PersonDetails()
        {
            InitializeComponent();
            PersonViewModel vm = new PersonViewModel();

            //this.Nom.TextChanged += Nom_TextChanged;
            
            vm.CurrentUser.Email = "ajc@t.com";
            vm.CurrentUser.Name = "Xamarin";
            vm.CurrentUser.LastName = "M2i";
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = vm;
        }

        private void NomTextChanged(object sender, TextChangedEventArgs e)
        {
            ((PersonViewModel)this.BindingContext).DoSomeThing();
        }
    }
}
