﻿using System;
using System.Collections.Generic;
using TestXam.ViewModels;
using Xamarin.Forms;

namespace TestXam.Views
{
    public partial class CompassSample : ContentPage
    {
        public CompassSample(CompassViewModel vm)
        {
            InitializeComponent();
            BindingContext = vm;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            ((CompassViewModel)BindingContext).StopCommand.Execute(null);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ((CompassViewModel)BindingContext).StartCommand.Execute(null);

        }
    }
}
