﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.Services.Models
{
    public class ManagedShowRoomLead
    {
        public string FormatedDate { get; set; }
        public int LeadCount { get; set; }
        public string Message { get { return (LeadCount > 1) ? String.Format("You received {0} new Leads", LeadCount) : String.Format("You received {0} new Lead", LeadCount); } }
    }


    public class LeadFilterModels
    {
        public string Libelle { get; set; }
        public bool IsSelected { get; set; }
    }


    public class LeadModels
    {
    }

    public class User
    {
        public string Title         { get; set; }
        public string FirstName     { get; set; }
        public string LastName      { get; set; }
        public string ShortFirstName { get { return string.IsNullOrEmpty(FirstName) ? string.Empty : FirstName.Substring(0, 1) + "."; } }
        public string ShortName { get { return string.IsNullOrEmpty(LastName) ? ShortFirstName : ShortFirstName + LastName; } }
    }

    public class Lead
    {
        public string LeadId                { get; set; }
        public string ShowroomId            { get; set; }
        public string UserId                { get; set; }
        public string Tag                   { get; set; }
        public string Status                { get; set; }
        public string Title                 { get; set; }
        public string FirstName             { get; set; }
        public string LastName              { get; set; }
        public string Email                 { get; set; }
        public string RecontactOn           { get; set; }
        public DateTime CreatedDate         { get; set; }
        public DateTime LastModified        { get; set; }
        public Showroom Showroom            { get; set; }
        public User User                    { get; set; }

        // Added for the View
        //public bool IsSeleted               { get {return (Constants._LeadsStatus.Contains(Status)) ? true : false;} }
        //public string StatusColor           { get { return GetStatusColor(CreatedDate); } }
        //public bool ShowSelectionIcone      { get; set; }

    }

    public class History
    {
        public string Title             { get; set; }
        public string Status { get; set; }
        public string UserId { get; set; }
        public string Comment { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Timestamp { get; set; }
    }

    public class LeadInfos
    {
        public string LeadId { get; set; }
        public string ShowroomId { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FacebookId { get; set; }
        public string TwitterId { get; set; }
        public string KakaoId { get; set; }
        public string Tag { get; set; }
        public string Status { get; set; }
        public string RecontactOn { get; set; }
        public string Comment { get; set; }
        public string Models { get; set; }
        public string Category { get; set; }
        public IList<History> History { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModified { get; set; }

        // Add for View 
        //public DateTime FormattedRecontactOn { get { return (RecontactOn != null) ? Convert.ToDateTime(RecontactOn)  : DateTime.MinValue;} }
    }

    public class CountLeadModel
    {
        public string Count { get; set; }
    }
}
