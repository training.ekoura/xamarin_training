﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.DAOServices.Models
{
    public class NotificationBatchConfig
    {    
        [JsonProperty(PropertyName = "nbcak")]
        public int APIKey { get; set; }

        [JsonProperty(PropertyName = "nbcrak")]
        public string RestAPIKey { get; set; }
    }
}
