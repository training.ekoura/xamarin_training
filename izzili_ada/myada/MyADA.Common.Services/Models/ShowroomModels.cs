﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.Services.Models
{
    public class ShowroomModels
    {
        public string ShowroomId { get; set; }
        public string Name { get; set; }
        public string LocalName { get; set; }
        public string City { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModified { get; set; }

        // Add for views
        //public bool IsSelected { get; set; }
    }
}
