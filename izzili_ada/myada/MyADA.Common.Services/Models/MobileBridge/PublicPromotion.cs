﻿using System;

namespace MyADA.Common.DAOServices.Models
{
	public class PublicPromotion
	{
		public PublicPromotionID id { get; set; }

		public string title { get; set; }

		public string description { get; set; }

		public string image_url { get; set; }

		public string image_name { get; set; }

		public int end_date { get; set; }

		public int loyalty_cost { get; set; }

		public PublicPromotionTag[] tags { get; set; }
	}

	public class PublicPromotionID
	{
		public int id { get; set; }

		public string type { get; set; }
	}

	public class PublicPromotionTag
	{
		public string slug { get; set; }

		public string tag { get; set; }
	}

}

