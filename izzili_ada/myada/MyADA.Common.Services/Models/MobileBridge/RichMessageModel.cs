﻿using System;
using System.Collections.Generic;

namespace MyADA.Common.DAOServices.Models
{
	public class RichMessageModel
	{
		public string description { get; set; }
		public string image_url { get; set; }
		public int id { get; set; }
		public List<object> tags { get; set; }
		public string content { get; set; }
		public string deleted_at { get; set; }
		public string image_name { get; set; }
		public string title { get; set; }
	}


    public class Id
    {
        public int id { get; set; }
        public string type { get; set; }
    }

    public class Tag
    {
        public string slug { get; set; }
        public string tag { get; set; }
    }

    public class AllRichMessageModel
    {
        public Id id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public IList<Tag> tags { get; set; }
        public string content { get; set; }
        public string image_name { get; set; }
        public string image_url { get; set; }
    }
}

