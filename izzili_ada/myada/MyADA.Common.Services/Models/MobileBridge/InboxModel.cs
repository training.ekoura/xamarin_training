﻿using System;
using System.Collections.Generic;

namespace MyADA.Common.DAOServices.Models
{
		public class AssetId
		{
			public string inbox_id { get; set; }
			public string id { get; set; }
			public string type { get; set; }
		}

		public class Data
		{
		}

		public class ARTICLE
		{
			public string title { get; set; }
			public AssetId asset_id { get; set; }
			public Data data { get; set; }
			public string inbox_id { get; set; }
			public string date_added { get; set; }
			public string image { get; set; }
			public int viewed { get; set; }
			public string body { get; set; }
		}

		public class Messages
		{
			public List<ARTICLE> ARTICLE { get; set; }
		}

		public class InboxModel
		{
			public int total { get; set; }
			public Messages messages { get; set; }
			public int unviewed { get; set; }
		}
}

