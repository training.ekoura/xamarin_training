﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.Services.Models
{
    public class SignInResponse
    {
        public string Secret { get; set; }
        public string Token { get; set; }
    }

    public class DefaultReturnResponse
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    
}
