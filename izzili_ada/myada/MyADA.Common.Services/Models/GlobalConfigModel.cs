﻿using MyADA.Common.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.DAOServices.Models
{
    public class GlobalConfigModel
    {
        public Token OldToken { get; set; }
        
        public string Token { get; set; }

        public string CultureInfo { get; set; }

        public string AuthorizationKey { get; set; }

        public string Secret { get; set; }

        public DateTime TokenDate { get; set; }

        public DateTime DateWhenOnSleep { get; set; }
        
        public string DeviceOs { get; set; }

        public string AppVersion { get; set; }

        public string AppName { get; set; }

        public string LoginMail { get; set; }

        public List<int> ViewedOrdersIds { get; set; }

        public List<string> BookedRSSList { get; set; }

        public DateTime SaveBookingLeadDate { get; set; }

        public string DeviceToken { get; set; }

        public string DevicePlatform { get; set; }
    }
}
