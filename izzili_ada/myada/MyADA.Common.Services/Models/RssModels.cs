﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.Services.Models
{
    public class RssModels
    {
    }

    public class FeedInfos
    {
        public string   FeedId       { get; set; }
        public string   Title        { get; set; }
        public string   Url          { get; set; }
        public DateTime CreatedDate  { get; set; }
        public DateTime LastModified { get; set; }
    }
}
