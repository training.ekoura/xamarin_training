﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.DAOServices.Models
{
    public class Token
    {
        [JsonProperty(PropertyName = "usid")]
        public int UserSolId { get; set; }

        [JsonProperty(PropertyName = "t")]
        public string TokenVal { get; set; }


    }
}
