﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.Services.Models
{
    public class UserModels
    {
    }

    public class Showroom
    {
        public string Name      { get; set; }
        public string LocalName { get; set; }
        public string LeadConfig { get; set; }
    }

    public class UserInfos
    {
        public string UserId            { get; set; }
        public string ShowroomId        { get; set; }
        public string Title             { get; set; }
        public string FirstName         { get; set; }
        public string LastName          { get; set; }
        public string Email             { get; set; }
        public string Role              { get; set; }
        public DateTime CreatedDate     { get; set; }
        public DateTime LastModified    { get; set; }
        public Showroom Showroom        { get; set; }

        // Add for View
        public string FullName          { get { return FirstName + " " + LastName; } }
        public string ShortFirstName    { get { return string.IsNullOrEmpty(FirstName) ? string.Empty : FirstName.Substring(0, 1) + "."; } }
        public string ShortName         { get { return string.IsNullOrEmpty(LastName) ? ShortFirstName : ShortFirstName + LastName;} }
    }

    #region ==================================== Get Profile ====================================

    public class Position
    {
        public bool managerother { get; set; }
        public bool salesmanager { get; set; }
        public bool showroomdirector { get; set; }
        public bool nomanagementposition { get; set; }
    }

    public class Organization
    {
        public bool employee { get; set; }
        public string Showroom { get; set; } 
    }

    public class DomainsOfAction
    {

        public bool admin { get; set; }
        public bool other { get; set; }
        public bool parts { get; set; }
        public bool finance { get; set; }
        public bool newcars { get; set; }
        public bool service { get; set; }
        public bool usedcars { get; set; }
        public bool marketing { get; set; }
        public bool accessories { get; set; }

        // Add for Setting View
        public string adminColor { get { return (admin == true) ? "#DA002E" : "#76858F"; } }
        public string otherColor { get { return (other == true) ? "#DA002E" : "#76858F"; } }
        public string partsColor { get { return (parts == true) ? "#DA002E" : "#76858F"; } }
        public string financeColor { get { return (finance == true) ? "#DA002E" : "#76858F"; } }
        public string newcarsColor { get { return (newcars == true) ? "#DA002E" : "#76858F"; } }
        public string serviceColor { get { return (service == true) ? "#DA002E" : "#76858F"; } }
        public string usedcarsColor { get { return (usedcars == true) ? "#DA002E" : "#76858F"; } }
        public string marketingColor { get { return (marketing == true) ? "#DA002E" : "#76858F"; } }
        public string accessoriesColor { get { return (accessories == true) ? "#DA002E" : "#76858F"; } }
    }

    public class Profile
    {
        public Position Position { get; set; }
        public Organization Organization { get; set; }
        public DomainsOfAction DomainsOfAction { get; set; }
    }

    public class FormattedProfile
    {
        public PositionModel Position { get; set; }
        public OrganisationModel Organization { get; set; }
        public List<DomainOfActionModel> DomainsOfAction { get; set; }
        public UserTitle Title { get; set; }
    }

    public class CurrentUserProfile
    {
        public string UserId { get; set; }
        public string ShowroomId { get; set; }
        public string Role { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Profile Profile { get; set; }
        public string CreatedDate { get; set; }
        public string LastModified { get; set; }
        public Showroom Showroom { get; set; }
        public FormattedProfile FormattedProfile { get; set; }
        // Add for Views
        public string FormattedRole { get { return (string.IsNullOrEmpty(Role)) ? string.Empty : Role.Substring(0, 1).ToUpper() + Role.Substring(1, Role.Length-1); } }
    }

    public class PositionModel
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
        public string IdForView { get; set; }
        public bool IsSelected { get; set; }
    }

    public class DomainOfActionModel
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
        public string IdForView { get; set; }
        public bool IsSelected { get; set; }
        public string adminColor { get { return (IsSelected == true) ? "DA002E" : "76858F"; } }
    }

    public class OrganisationModel
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
        public string IdForView { get; set; }
        public bool IsSelected { get; set; }
    }
    #endregion

    public class UserTitle
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
        public bool IsSelected { get; set; }
    }


    #region ==================================== RSS ====================================
    
    // Un flux RSS
    public class RssFeed
    {
        public string   FeedId       { get; set; }
        public string   Title        { get; set; }
        public string   Url          { get; set; }
        public DateTime CreatedDate  { get; set; }
        public DateTime LastModified { get; set; }
    }

    // Un item du flux RSS
    public class Post
    {
        public string Title { get; set; }
        public string Link  { get; set; }
    }

    #endregion

}
