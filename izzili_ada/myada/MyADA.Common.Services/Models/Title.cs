﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.DAOServices.Models
{
    public class Title
    {
        [JsonProperty(PropertyName = "tid")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "tl")]
        public string Label { get; set; }
    }
}
