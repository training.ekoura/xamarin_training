﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;
using MyADA.Common.DAOServices.Services;
using MyADA.Common.Services.Services;


namespace MyADA.Common.DAOServices
{
    public sealed class Initializer
    {
        public static void RegisterAll()
        {
            // Préparation du service d'appels distants
            var l_RemoteCallService = CoreSDK.Framework.Services.RemoteCallService();

#if DEBUG
            // En debug, on n'utilise peut être pas les mêmes services qu'en release.
            l_RemoteCallService.ServerUri = Constants.DOMAIN;
#else
            l_RemoteCallService.ServerUri = Constants.DOMAIN;
#endif
            l_RemoteCallService.GlobalParameters = new { returnformat = "json" };

            // Enregistrement des services
            CoreSDK.Framework.Services.Container().Register<IGlobalConfigService, GlobalConfigService>(new SingletonInstanceManager());
            CoreSDK.Framework.Services.Container().Register<INotificationService, NotificationService>();
            CoreSDK.Framework.Services.Container().Register<IUserService, UserService>();
            CoreSDK.Framework.Services.Container().Register<ILoginService, LoginService>();
            CoreSDK.Framework.Services.Container().Register<ILeadService, LeadService>();
            CoreSDK.Framework.Services.Container().Register<IChangeRequestService, ChangeRequestService>();
            CoreSDK.Framework.Services.Container().Register<IShowroomService, ShowroomService>();
            CoreSDK.Framework.Services.Container().Register<IReportService, ReportService>();
        }
    }
}
