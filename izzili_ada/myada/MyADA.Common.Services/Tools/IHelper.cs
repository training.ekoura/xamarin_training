﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace MyADA.Common.Services.Tools
{
    public interface IHelper
    {
        Task<HttpClient> GetClient();
    }
}
