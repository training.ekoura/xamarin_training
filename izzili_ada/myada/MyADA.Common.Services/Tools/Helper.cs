﻿using MyADA.Common.Services.Requests;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;

namespace MyADA.Common.Services.Tools
{
    public class Helper : IHelper
    {
        private string authorizationKey;
        private string Url = Constants.DOMAIN;
        public async Task<HttpClient> GetClient()
        {
            HttpClient client = new HttpClient();
            if (!string.IsNullOrEmpty(CoreSDK.Framework.Services.GlobalConfigService().Data.AuthorizationKey))
            {
                client.DefaultRequestHeaders.Add("Authorization", CoreSDK.Framework.Services.GlobalConfigService().Data.AuthorizationKey);
            }

            client.DefaultRequestHeaders.AcceptEncoding.Add(new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            
            return client;
        }


        //System.Net.WebUtility.UrlEncode(JsonConvert.SerializeObject(obj))

        private async Task<string> ReadContentAsString(HttpResponseMessage response)
        {
            // Check whether response is compressed
            if (response.Content.Headers.ContentEncoding.Any(x => x == "gzip"))
            {
                // Decompress manually
                using (var s = await response.Content.ReadAsStreamAsync())
                {
                    /*using (var decompressed = new GZipStream(s, CompressionMode.Decompress))
                    {
                        using (var rdr As New IO.StreamReader(decompressed))
                        {
                            return await rdr.ReadToEndAsync();
                        }
                    }*/

                    return "";
                }
            }
            else
                // Use standard implementation if not compressed
                return await response.Content.ReadAsStringAsync();
        }

    }
}
