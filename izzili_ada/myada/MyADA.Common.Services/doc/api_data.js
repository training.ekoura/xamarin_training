define({ "api": [
  {
    "type": "post",
    "url": "/signin",
    "title": "01-Signin (Dedicated to MobileApp )",
    "name": "1_Signin",
    "description": "<p>This API will generate a random secret. then when the user will confirm its email by clicking on the link.</p> <ul> <li>Mobile Application will only use Signin</li> <li>As soon as the Mobile is signin, it's forever until uninstalation. As soon as the mobile devid have a valid Token, this API call will not be used any more... The first API call might be the one to get LeadsUpdates...</li> </ul>",
    "group": "Authentication",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "Secret",
            "description": "<p>( Serverside generated secret to be able to connect after email confirmation )</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "DeviceId",
            "description": "<p>(IMEI)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "\"android\"",
              "\"ios\""
            ],
            "optional": false,
            "field": "DeviceOs",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "PushNotificationToken",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "MBTempUserKey",
            "description": "<p>(UserContextForeignKey)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "MBUserKey",
            "description": "<p>(The Very first UserContextForeignKey allocated to that user, no matter the device )</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Secret",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": true,
            "field": "Token",
            "description": "<p>Sent only if the user is valid JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"Secret\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Error",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"401\",\"message\":\"Unauthorized\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/auth/index.js",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/logon",
    "title": "02-Logon ( dedicated to Backoffice )",
    "group": "Authentication",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Password",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Token",
            "description": "<p>JSON Web Token,</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"Token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Int",
            "allowedValues": [
              "401",
              "423"
            ],
            "optional": false,
            "field": "Error.status",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"401\",\"message\":\"Unauthorized\"}\n{\"status\":\"423\",\"message\":\"Password new password not confirmed\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/auth/index.js",
    "groupTitle": "Authentication",
    "name": "PostLogon"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "GET",
    "url": "/changerequests",
    "title": "05-GetChangeRequests",
    "group": "ChangeRequest",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "UserId",
            "description": "<p>UserID is specify only for an Update</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "RequesterId",
            "description": "<p>Requester UserID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "'pending'",
              "'validated'",
              "'refused'"
            ],
            "optional": true,
            "field": "Status",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "\"create\"",
              "\"update\"",
              "\"delete\""
            ],
            "optional": true,
            "field": "type",
            "description": "<p>Note: Delete will only disable the account first. Only Audi Employee can delete a user from a backoffice.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "ShowroomId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": true,
            "field": "Employee",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": true,
            "field": "UserId",
            "description": "<p>UserID is specify only for an Update</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "allowedValues": [
              "\"pending\"",
              "\"accepted\"",
              "\"refused\""
            ],
            "optional": false,
            "field": "Status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "allowedValues": [
              "\"create\"",
              "\"update\"",
              "\"delete\""
            ],
            "optional": false,
            "field": "type",
            "description": "<p>Note: Delete will only disable the account first. Only Audi Employee can delete a user from a backoffice.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "ShowroomId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Title",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "FirstName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "LastName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile.DomainsOfAction",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.accessories",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.admin",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.finance",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.marketing",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.newcars",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.other",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.parts",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.service",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.usedcars",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile.Organization",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Organization.employee",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile.Position",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.managerother",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.nomanagementposition",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.salesmanager",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.showroomdirector",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "integer",
            "optional": false,
            "field": "CreatedDate",
            "description": "<p>Timestamp</p>"
          },
          {
            "group": "Success 200",
            "type": "integer",
            "optional": false,
            "field": "LastModified",
            "description": "<p>Timestamp</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": true,
            "field": "RequesterId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/changerequests/index.js",
    "groupTitle": "ChangeRequest",
    "name": "GetChangerequests"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "GET",
    "url": "/changerequests/count",
    "title": "06-CountChangeRequest",
    "group": "ChangeRequest",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "UserId",
            "description": "<p>UserID is specify only for an Update</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "RequesterId",
            "description": "<p>Requester UserID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "\"pending\"",
              "\"validated\"",
              "\"refused\""
            ],
            "optional": true,
            "field": "Status",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "\"create\"",
              "\"update\"",
              "\"delete\""
            ],
            "optional": true,
            "field": "Type",
            "description": "<p>Note: Delete will only disable the account first. Only Audi Employee can delete a user from a backoffice.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "ShowroomId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": true,
            "field": "Employee",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/changerequests/index.js",
    "groupTitle": "ChangeRequest",
    "name": "GetChangerequestsCount"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "PATCH",
    "url": "/changerequests/:changerequestId/validate",
    "title": "03-Validate",
    "group": "ChangeRequest",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ\nTODO: The Change Request will inform the requester when the change is done",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/changerequests/index.js",
    "groupTitle": "ChangeRequest",
    "name": "PatchChangerequestsChangerequestidValidate"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "POST",
    "url": "/changerequests",
    "title": "01-Create",
    "group": "ChangeRequest",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "UserId",
            "description": "<p>UserID is specify only for an Update</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "\"create\"",
              "\"update\"",
              "\"delete\""
            ],
            "optional": false,
            "field": "type",
            "description": "<p>Note: Delete will only disable the account first. Only Audi Employee can delete a user from a backoffice.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "ShowroomId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Title",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "FirstName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "LastName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "Profile",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "Profile.DomainsOfAction",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.accessories",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.admin",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.finance",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.marketing",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.newcars",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.other",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.parts",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.service",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.usedcars",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "Profile.Organization",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Organization.employee",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "Profile.Position",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.managerother",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.nomanagementposition",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.salesmanager",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.showroomdirector",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/changerequests/index.js",
    "groupTitle": "ChangeRequest",
    "name": "PostChangerequests"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "PUT",
    "url": "/changerequests/:changerequestId/change",
    "title": "02-Change/Correct",
    "description": "<p>This API allow Audi to perform minus change on the Change request. Useful if the requester did a small mistake and Audi want to validate anyway with diffrents parameters.</p>",
    "group": "ChangeRequest",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "UserId",
            "description": "<p>UserID is specify only for an Update</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "\"create\"",
              "\"update\"",
              "\"delete\""
            ],
            "optional": false,
            "field": "type",
            "description": "<p>Note: Delete will only disable the account first. Only Audi Employee can delete a user from a backoffice.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "ShowroomId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Title",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "FirstName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "LastName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "Profile",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "Profile.DomainsOfAction",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.accessories",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.admin",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.finance",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.marketing",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.newcars",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.other",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.parts",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.service",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.usedcars",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "Profile.Organization",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Organization.employee",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "Profile.Position",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.managerother",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.nomanagementposition",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.salesmanager",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.showroomdirector",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "CreatedDate",
            "description": "<p>Timestamp</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "LastModified",
            "description": "<p>Timestamp</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/changerequests/index.js",
    "groupTitle": "ChangeRequest",
    "name": "PutChangerequestsChangerequestidChange"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "D__Izzili_Dev_Ada_adaApi_doc_main_js",
    "groupTitle": "D__Izzili_Dev_Ada_adaApi_doc_main_js",
    "name": ""
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "delete",
    "url": "/leads/:leadId",
    "title": "02-DeleteLead",
    "group": "Lead",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/leads/index.js",
    "groupTitle": "Lead",
    "name": "DeleteLeadsLeadid"
  },
  {
    "permission": [
      {
        "name": "Authenticated user"
      }
    ],
    "type": "get",
    "url": "/leads",
    "title": "04-GetLeads",
    "group": "Lead",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ\n  TODO: make ShowroomId, UserId and add status criteria all as Optional",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "ShowroomId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "UserId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "array",
            "allowedValues": [
              "\"new\"",
              "\"notallocated\"",
              "\"allocated\"",
              "\"ongoing\"",
              "\"nointrest\"",
              "\"closed\""
            ],
            "optional": true,
            "field": "Status",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "Limit",
            "description": "<p>Limit field for pagination.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "Offset",
            "description": "<p>Offset field for pagination.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Leads with paging",
          "content": "/leads?Limit=25&Offset=1",
          "type": "string"
        },
        {
          "title": "Leads belonging to a showroom",
          "content": "/leads?ShowroomId=527056ed-b767-447b-a456-e753d23a95b7",
          "type": "string"
        },
        {
          "title": "Leads belonging to an user",
          "content": "/leads?UserId=527056ed-b767-447b-a456-e753d23a95b7",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Leads",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.LeadId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.ShowroomId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.UserId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.Tag",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.Category",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.Status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.Title",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.FirstName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.LastName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.Email",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.PhoneNumber",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.RecontactOn",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Leads.Showroom",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.Showroom.Name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.Showroom.LocalName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Leads.User",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.User.Title",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.User.FirstName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Leads.User.LastName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "integer",
            "optional": false,
            "field": "Leads.CreateDate",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "integer",
            "optional": false,
            "field": "Leads.LastModified",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/leads/index.js",
    "groupTitle": "Lead",
    "name": "GetLeads"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "GET",
    "url": "/leads/count",
    "title": "08-CountLead",
    "group": "Lead",
    "description": "<ul> <li>Audi user can count all leads from all Showroom <ul> <li>Director/manager can only count lead from their own showroom</li> <li>Dealer can only count unallocated leads and their own leads.</li> </ul> </li> </ul>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "ShowroomId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "UserId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "status",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "count",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\",\"count\":37}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/leads/index.js",
    "groupTitle": "Lead",
    "name": "GetLeadsCount"
  },
  {
    "permission": [
      {
        "name": "Authenticated user"
      }
    ],
    "type": "get",
    "url": "/leads/:leadId",
    "title": "03-GetLead",
    "group": "Lead",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "LeadId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "ShowroomId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "UserId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Tag",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Title",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "FirstName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "LastName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "PhoneNumber",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Email",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "FacebookId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "KakaoId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "TwitterId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "RecontactOn",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Showroom",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showroom.Name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showroom.LocalName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "User",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "User.Title",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "User.FirstName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "User.LastName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "History",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "History.UserId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "History.Status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "History.Comment",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "integer",
            "optional": false,
            "field": "History.Timestamp",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "integer",
            "optional": false,
            "field": "CreatedDate",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "integer",
            "optional": false,
            "field": "LastModified",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP:1.1 404 Not Found\n{\"status\":\"404\",\"message\":\"Not Found\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/leads/index.js",
    "groupTitle": "Lead",
    "name": "GetLeadsLeadid"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "GET",
    "url": "/leads/:userId/notifications",
    "title": "09-GetLeadNotifications",
    "group": "Lead",
    "description": "<p>Allow the application to display Lead update notification history</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "Notification",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Notification.message",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Notification.timestamp",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/leads/index.js",
    "groupTitle": "Lead",
    "name": "GetLeadsUseridNotifications"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "patch",
    "url": "/leads/:leadId/status",
    "title": "06-SetLeadStatus",
    "group": "Lead",
    "description": "<p>@apiHeader {string} Authorization JSON Web Token</p>",
    "header": {
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"new\"",
              "\"notallocated\"",
              "\"allocated\"",
              "\"ongoing\"",
              "\"nointerest\"",
              "\"closed\""
            ],
            "optional": false,
            "field": "Status",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "RecontactOn",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Comment",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/leads/index.js",
    "groupTitle": "Lead",
    "name": "PatchLeadsLeadidStatus"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "post",
    "url": "/leads",
    "title": "01-CreateLead",
    "group": "Lead",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "LeadId",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"LeadId\":\"428b9176-38c4-472d-b3cc-20c2c2d5ffc7\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}\nTODO: Add parameters to add lead from R8 App . A Batch will do the import/export from R8 to ADA.",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/leads/index.js",
    "groupTitle": "Lead",
    "name": "PostLeads"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "post",
    "url": "/leads/user",
    "title": "07-AssignLeadToUser",
    "group": "Lead",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "UserId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string[]",
            "optional": false,
            "field": "LeadId",
            "description": "<p>LeadId list to allocate to users.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/leads/index.js",
    "groupTitle": "Lead",
    "name": "PostLeadsUser"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "put",
    "url": "/leads/:leadId",
    "title": "05-UpdateLead",
    "group": "Lead",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "ShowroomId",
            "description": "<p>Only Audi employee can change this parameter and only to unallocated Lead. Once a lead has been attributed to a user, we cannot change the showroom any more.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "UserId",
            "description": "<p>Only Director &amp; Manager can change this user (from and to a user from their showroom). If we want to deallocate a Lead, we just need to change the Status parameter, no need to change the UserID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"new\"",
              "\"notallocated\"",
              "\"allocated\"",
              "\"ongoing\"",
              "\"nointerest\"",
              "\"closed\""
            ],
            "optional": true,
            "field": "Status",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "recontacton",
            "defaultValue": "null",
            "description": "<p>Timestamp, when should we recontact the lead ?</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/leads/index.js",
    "groupTitle": "Lead",
    "name": "PutLeadsLeadid"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "get",
    "url": "/reports/consolidated/:showroomid",
    "title": "02-Consolidated report",
    "group": "Reporting",
    "description": "<p>Get report for specified showroom</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Html",
            "description": "<p>document</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/reports/index.js",
    "groupTitle": "Reporting",
    "name": "GetReportsConsolidatedShowroomid"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "get",
    "url": "/reports/my/:userid",
    "title": "01-My report",
    "group": "Reporting",
    "description": "<p>Get report for specified user</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Html",
            "description": "<p>document</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/reports/index.js",
    "groupTitle": "Reporting",
    "name": "GetReportsMyUserid"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "get",
    "url": "/reports/overall/",
    "title": "03-Overall report",
    "group": "Reporting",
    "description": "<p>Get overall report</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Html",
            "description": "<p>document</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/reports/index.js",
    "groupTitle": "Reporting",
    "name": "GetReportsOverall"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "delete",
    "url": "/rssfeeds/:feedId",
    "title": "02-DeleteFeed",
    "group": "Rss",
    "description": "<p>Delete Rss feed</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/rssfeeds/index.js",
    "groupTitle": "Rss",
    "name": "DeleteRssfeedsFeedid"
  },
  {
    "permission": [
      {
        "name": "Authenticated user"
      }
    ],
    "type": "get",
    "url": "/rssfeeds",
    "title": "04-GetRssfeeds",
    "group": "Rss",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Rssfeeds",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Rssfeeds.Title",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Rssfeeds.Url",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP:1.1 404 Not Found\n{\"status\":\"404\",\"message\":\"Not Found\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/rssfeeds/index.js",
    "groupTitle": "Rss",
    "name": "GetRssfeeds"
  },
  {
    "permission": [
      {
        "name": "Authenticated user"
      }
    ],
    "type": "get",
    "url": "/rssfeeds/:feedId",
    "title": "03-GetRssfeed",
    "group": "Rss",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "FeedId",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Title",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Url",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/rssfeeds/index.js",
    "groupTitle": "Rss",
    "name": "GetRssfeedsFeedid"
  },
  {
    "permission": [
      {
        "name": "Authenticated user"
      }
    ],
    "type": "post",
    "url": "/rssfeeds",
    "title": "01-CreateRssfeed",
    "group": "Rss",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "feedId",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"FeedId\":\"428b9176-38c4-472d-b3cc-20c2c2d5ffc7\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Title",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Url",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/rssfeeds/index.js",
    "groupTitle": "Rss",
    "name": "PostRssfeeds"
  },
  {
    "permission": [
      {
        "name": "Authenticated user"
      }
    ],
    "type": "put",
    "url": "/rssfeeds/:feedId",
    "title": "05-UpdateRssfeed",
    "group": "Rss",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "feedId",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/rssfeeds/index.js",
    "groupTitle": "Rss",
    "name": "PutRssfeedsFeedid"
  },
  {
    "permission": [
      {
        "name": "superadmin"
      }
    ],
    "type": "delete",
    "url": "/users/:userId",
    "title": "02-DeleteSetting",
    "group": "Setting",
    "description": "<p>@apiHeader {string} Authorization JSON Web Token</p>",
    "header": {
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/settings/index.js",
    "groupTitle": "Setting",
    "name": "DeleteUsersUserid"
  },
  {
    "permission": [
      {
        "name": "superadmin"
      }
    ],
    "type": "get",
    "url": "/settings",
    "title": "04-GetSettings",
    "group": "Setting",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Settings",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Settings.Key",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "Settings.CreatedDate",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "Settings.LastModified",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/settings/index.js",
    "groupTitle": "Setting",
    "name": "GetSettings"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "get",
    "url": "/settings/:settingId",
    "title": "03-GetSetting",
    "group": "Setting",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Key",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "Value",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "CreatedDate",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "LastModified",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/settings/index.js",
    "groupTitle": "Setting",
    "name": "GetSettingsSettingid"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "post",
    "url": "/settings",
    "title": "01-CreateSetting",
    "group": "Setting",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Key",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "Value",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/settings/index.js",
    "groupTitle": "Setting",
    "name": "PostSettings"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "post",
    "url": "/settings/:settingid",
    "title": "05-UpdateSetting",
    "group": "Setting",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "Value",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/settings/index.js",
    "groupTitle": "Setting",
    "name": "PostSettingsSettingid"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "delete",
    "url": "/showrooms/:showroomId",
    "title": "02-DeleteShowroom",
    "group": "Showroom",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/showrooms/index.js",
    "groupTitle": "Showroom",
    "name": "DeleteShowroomsShowroomid"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "get",
    "url": "/showrooms",
    "title": "04-GetShowrooms",
    "group": "Showroom",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "TODO: Make that API call public for R8 application. Listing Showroom isn't a private information.\nAuthorization: none ( Authentication might be require to display disabled showroom )",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Showrooms",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showrooms.ShowroomId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showrooms.Name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showrooms.LocalName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showrooms.Address",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showrooms.City",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showrooms.ZipCode",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showrooms.Description",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showrooms.Latitude",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showrooms.Longitude",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showrooms.Elevation",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "allowedValues": [
              "\"managed\"",
              "\"available\""
            ],
            "optional": false,
            "field": "LeadConfig",
            "description": "<p>Option1: Available, the dealer are ableto select the leads and save them. Option2: managed means that only the Manager see incoming lead and attribute lead manually to dealers</p>"
          },
          {
            "group": "Success 200",
            "type": "string[]",
            "optional": true,
            "field": "Director",
            "description": "<p>Director UserID in a Array</p>"
          },
          {
            "group": "Success 200",
            "type": "string[]",
            "optional": true,
            "field": "SalesManagers",
            "description": "<p>SalesManagers UserID in a Array</p>"
          },
          {
            "group": "Success 200",
            "type": "string[]",
            "optional": true,
            "field": "OtherManagers",
            "description": "<p>OtherManagers UserID in a Array</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "Showrooms.disable",
            "description": "<p>By default a new showroom is disable=true. Disabled showroom will be displayed only to authenticated users. We can enable a showroom only if the a director and at least one sales manager is set.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/showrooms/index.js",
    "groupTitle": "Showroom",
    "name": "GetShowrooms"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "get",
    "url": "/showrooms/:showroomId",
    "title": "03-GetShowroom",
    "group": "Showroom",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "ShowroomId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "LocalName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Address",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "City",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "ZipCode",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Description",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Latitude",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Longitude",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Elevation",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "allowedValues": [
              "\"managed\"",
              "\"available\""
            ],
            "optional": false,
            "field": "LeadConfig",
            "description": "<p>Option1: Available, the dealer are able to select the leads and save them. Option2: managed means that only the Manager see incoming lead and attribute lead manually to dealers</p>"
          },
          {
            "group": "Success 200",
            "type": "string[]",
            "optional": true,
            "field": "Director",
            "description": "<p>Director UserID in a Array</p>"
          },
          {
            "group": "Success 200",
            "type": "string[]",
            "optional": true,
            "field": "SalesManagers",
            "description": "<p>SalesManagers UserID in a Array</p>"
          },
          {
            "group": "Success 200",
            "type": "string[]",
            "optional": true,
            "field": "OtherManagers",
            "description": "<p>OtherManagers UserID in a Array</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "disable",
            "description": "<p>By default a new showroom is disable=true. Disabled showroom will be displayed only to authenticated users. We can enable a showroom only if the a director and at least one sales manager is set.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/showrooms/index.js",
    "groupTitle": "Showroom",
    "name": "GetShowroomsShowroomid"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "post",
    "url": "/showrooms",
    "title": "01-CreateShowroom",
    "group": "Showroom",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "LocalName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Address",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "City",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "ZipCode",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Description",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Latitude",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Longitude",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Elevation",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string[]",
            "optional": true,
            "field": "Director",
            "description": "<p>Director UserID in a Array</p>"
          },
          {
            "group": "Parameter",
            "type": "string[]",
            "optional": true,
            "field": "SalesManagers",
            "description": "<p>SalesManagers UserID in a Array</p>"
          },
          {
            "group": "Parameter",
            "type": "string[]",
            "optional": true,
            "field": "OtherManagers",
            "description": "<p>OtherManagers UserID in a Array</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "\"managed\"",
              "\"available\""
            ],
            "optional": false,
            "field": "LeadConfig",
            "description": "<p>Option1: Available, the dealer are ableto select the leads and save them. Option2: managed means that only the Manager see incoming lead and attribute lead manually to dealers</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/showrooms/index.js",
    "groupTitle": "Showroom",
    "name": "PostShowrooms"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "put",
    "url": "/showrooms/:showroomId",
    "title": "05-UpdateShowroom",
    "group": "Showroom",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "LocalName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Address",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "City",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "ZipCode",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Description",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Latitude",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Longitude",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Elevation",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string[]",
            "optional": true,
            "field": "Director",
            "description": "<p>Director UserID in a Array</p>"
          },
          {
            "group": "Parameter",
            "type": "string[]",
            "optional": true,
            "field": "SalesManagers",
            "description": "<p>SalesManagers UserID in a Array</p>"
          },
          {
            "group": "Parameter",
            "type": "string[]",
            "optional": true,
            "field": "OtherManagers",
            "description": "<p>OtherManagers UserID in a Array</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "disable",
            "description": "<p>By default a new showroom is disable=true. Disabled showroom will be displayed only to authenticated users. We can enable a showroom only if the a director and at least one sales manager is set.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "\"managed\"",
              "\"available\""
            ],
            "optional": false,
            "field": "LeadConfig",
            "description": "<p>Option1: Available, the dealer are ableto select the leads and save them. Option2: managed means that only the Manager see incoming lead and attribute lead manually to dealers</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/showrooms/index.js",
    "groupTitle": "Showroom",
    "name": "PutShowroomsShowroomid"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "delete",
    "url": "/users/:userId",
    "title": "02-DeleteUser",
    "group": "User",
    "description": "<p>@apiHeader {string} Authorization JSON Web Token</p>",
    "header": {
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/users/index.js",
    "groupTitle": "User",
    "name": "DeleteUsersUserid"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "delete",
    "url": "/users/:userId/rssfeeds/:feedId",
    "title": "11-UnsubscribeRssFeed",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/users/index.js",
    "groupTitle": "User",
    "name": "DeleteUsersUseridRssfeedsFeedid"
  },
  {
    "permission": [
      {
        "name": "Authenticated user"
      }
    ],
    "type": "get",
    "url": "/users",
    "title": "04-GetUsers",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "ShowroomId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "hidedisabled",
            "defaultValue": "true",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "Limit",
            "description": "<p>Limit field for pagination.</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "Offset",
            "description": "<p>Offset field for pagination.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Users",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Users.UserId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Users.ShowroomId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Users.Email",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Users.Role",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Users.Showroom",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Users.Showroom.Name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Users.Showroom.LocalName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Users.LastModfied",
            "description": "<p>Timestamp</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Users.CreatedDate",
            "description": "<p>Timestamp</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/users/index.js",
    "groupTitle": "User",
    "name": "GetUsers"
  },
  {
    "permission": [
      {
        "name": "Authenticated user"
      }
    ],
    "type": "get",
    "url": "/users/count",
    "title": "08-CountUsers",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "description": "<p>How many user match to that criteria</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "ShowroomId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "UserId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "Status",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "\"dealer\"",
              "\"manager\"",
              "\"admin\"",
              "\"superadmin\""
            ],
            "optional": true,
            "field": "Position",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "allowedValues": [
              "\"newcars\"",
              "\"usedcars\"",
              "\"service\"",
              "\"parts\"",
              "\"accessories\"",
              "\"finance\"",
              "\"admin\"",
              "\"marketing\"",
              "\"other\""
            ],
            "optional": true,
            "field": "ActionDomain",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/users/index.js",
    "groupTitle": "User",
    "name": "GetUsersCount"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "get",
    "url": "/users/profile",
    "title": "07-Profile",
    "description": "<p>Return current logon user information</p>",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "ShoowromId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "UserId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Email",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Role",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Title",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "FirstName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "LastName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Showroom",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showroom.Name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showroom.LocalName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showroom.LeadConfig",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile.DomainsOfAction",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.accessories",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.admin",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.finance",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.marketing",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.newcars",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.other",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.parts",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.service",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.usedcars",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile.Organization",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Organization.employee",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile.Position",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.managerother",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.nomanagementposition",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.salesmanager",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.showroomdirector",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "integer",
            "optional": false,
            "field": "CreatedDate",
            "description": "<p>Timestamp</p>"
          },
          {
            "group": "Success 200",
            "type": "integer",
            "optional": false,
            "field": "LastModified",
            "description": "<p>Timestamp</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP:1.1 404 Not Found\n{\"status\":\"404\",\"message\":\"Not Found\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/users/index.js",
    "groupTitle": "User",
    "name": "GetUsersProfile"
  },
  {
    "permission": [
      {
        "name": "Authenticated user"
      }
    ],
    "type": "get",
    "url": "/users/:userId",
    "title": "03-GetUser",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "ShoowromId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "UserId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Email",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Role",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Title",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "FirstName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "LastName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Showroom",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showroom.Name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showroom.LocalName",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Showroom.LeadConfig",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile.DomainsOfAction",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.accessories",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.admin",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.finance",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.marketing",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.newcars",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.other",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.parts",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.service",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.usedcars",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile.Organization",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Organization.employee",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile.Position",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.managerother",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.nomanagementposition",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.salesmanager",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.showroomdirector",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "integer",
            "optional": false,
            "field": "CreatedDate",
            "description": "<p>Timestamp</p>"
          },
          {
            "group": "Success 200",
            "type": "integer",
            "optional": false,
            "field": "LastModified",
            "description": "<p>Timestamp</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "HTTP:1.1 404 Not Found\n{\"status\":\"404\",\"message\":\"Not Found\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/users/index.js",
    "groupTitle": "User",
    "name": "GetUsersUserid"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "get",
    "url": "/users/:userId/rssfeeds",
    "title": "09-GetUserRssFeeds",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/users/index.js",
    "groupTitle": "User",
    "name": "GetUsersUseridRssfeeds"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "patch",
    "url": "/users/:userId/password",
    "title": "06-ChangePassword",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "OldPassword",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "NewPassword",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/users/index.js",
    "groupTitle": "User",
    "name": "PatchUsersUseridPassword"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "post",
    "url": "/users",
    "title": "01-CreateUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "ShowroomId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Title",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "FirstName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "LastName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "CreatedDate",
            "description": "<p>Timestamp</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "LastModified",
            "description": "<p>Timestamp</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile.DomainsOfAction",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.accessories",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.admin",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.finance",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.marketing",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.newcars",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.other",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.parts",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.service",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.usedcars",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile.Organization",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Organization.employee",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "Profile.Position",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.managerother",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.nomanagementposition",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.salesmanager",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.showroomdirector",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "UserId",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"UserId\":\"428b9176-38c4-472d-b3cc-20c2c2d5ffc7\"}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/users/index.js",
    "groupTitle": "User",
    "name": "PostUsers"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "post",
    "url": "/users/:userId/rssfeeds",
    "title": "10-SubscribeRssFeed",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/users/index.js",
    "groupTitle": "User",
    "name": "PostUsersUseridRssfeeds"
  },
  {
    "permission": [
      {
        "name": "Authenticated and Authorized user"
      }
    ],
    "type": "put",
    "url": "/users/:userId",
    "title": "05-UpdateUser",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JSON Web Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization Header",
          "content": "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
          "type": "string"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "ShowroomId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Title",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "FirstName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "LastName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "Profile",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "Profile.DomainsOfAction",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.accessories",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.admin",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.finance",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.marketing",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.newcars",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.other",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.parts",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.service",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.DomainsOfAction.usedcars",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "Profile.Organization",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Organization.employee",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "Profile.Position",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.managerother",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.nomanagementposition",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.salesmanager",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "Profile.Position.showroomdirector",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "CreatedDate",
            "description": "<p>Timestamp</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "LastModified",
            "description": "<p>Timestamp</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\"status\":\"200\",\"message\":\"OK\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "string",
            "optional": false,
            "field": "Message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response",
          "content": "{\"status\":\"403\",\"message\":\"Forbidden\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./functions/router/lib/controllers/users/index.js",
    "groupTitle": "User",
    "name": "PutUsersUserid"
  }
] });
