﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.Services.Requests
{
    public class LoginRequest
    {
        public string Email { get; set; }
        public string Secret { get; set; }
        public string DeviceId { get; set; }
        public string DeviceOs { get; set; }
        public string PushNotificationToken { get; set; }
        public string MBTempUserKey { get; set; }
    }
}
