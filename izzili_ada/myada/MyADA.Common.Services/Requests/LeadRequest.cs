﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.Services.Requests
{
    public class LeadRequest
    {
    }

    public class UpdateLeadRequest
    {
        public string ShowroomId    { get; set; }
        public string UserId        { get; set; }
        public string Status        { get; set; }
        public string recontacton   { get; set; }
    }

    public class SetLeadStatusRequest
    {
        public string Status { get; set; }
        public string Comment { get; set; }
        public string RecontactOn { get; set; }
    }

    public class AssignLeadToUserRequest
    {
        public string UserId { get; set; }
        public string[] LeadId { get; set; }
    }

    public class CountLeadRequest
    {
        public string ShowroomId { get; set; }
        public string UserId { get; set; }
        public string status { get; set; }
    }
}
