﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.Services.Requests
{
    public class ChangeRequest
    {
        public string UserId { get; set; }
        public string Type { get; set; }
        public string ShowroomId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string[] ActionDomain { get; set; }
        public bool Employee { get; set; }
        public int CreatedDate { get; set; }// passer 0 par défaut
        public int LastModified { get; set; }
    }
}
