﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.Services.Requests
{
    public class UserRequest
    {
    }

    public class UserRssRequest
    {
        public string[] FeedIds { get; set; }
    }

    public class GetUsersRequest 
    {
        public string ShowroomId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CreatedDate { get; set; }
        public int LastModified { get; set; }
    }

    /// <summary>
    /// Model for creating new user
    /// </summary>
    public class UpdateUserRequest
    {
        public string ShowroomId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserProfile Profile { get; set; }
        public int CreatedDate { get; set; }
        public int LastModified { get; set; }
    }

    public class UserProfile
    {
        public GlobalsDomainsOfAction DomainsOfAction { get; set; }
        public GlobalsOrganization Organization { get; set; }
        public GlobalsPosition Position { get; set; }
    }

    public class GlobalsPosition
    {
        public bool ManagerOther { get; set; }
        public bool NoManagementPosition { get; set; }
        public bool SalesManager { get; set; }
        public bool ShowroomDirector { get; set; }
    }

    public class GlobalsOrganization
    {
        public string Showroom { get; set; }
        public bool Employee { get; set; }
    }

    public class GlobalsDomainsOfAction
    {
        public bool Accessories { get; set; }
        public bool Admin { get; set; }
        public bool Finance { get; set; }
        public bool Marketing { get; set; }
        public bool Newcars { get; set; }
        public bool Other { get; set; }
        public bool Parts { get; set; }
        public bool Service { get; set; }
        public bool Usedcars { get; set; }
    }

    public class UserCountRequest
    {
        public string ShowroomId    { get; set; }
        public string UserId        { get; set; }
        public string Status        { get; set; }
        public string Position      { get; set; }
        public string ActionDomain  { get; set; }
    }
}
