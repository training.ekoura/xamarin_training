﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.DAOServices.Requests
{
    public class RefreshRequest
    {
        public string Token { get; set; }
        public string Locale { get; set; }
        public string Os { get; set; }
        public string AppVersion { get; set; }
        public string AppName { get; set; }
    }
}
