﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.DAOServices.Requests
{
    public class GetStatutPossiblesRequest
    {
        public string Token { get; set; }
        public string Date { get; set; }
        public int CommandeId { get; set; }
        public string ZoneRestaurationId { get; set; }
    }
}
