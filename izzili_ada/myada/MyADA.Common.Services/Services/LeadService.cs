﻿using MyADA.Common.Services.Models;
using MyADA.Common.Services.Tools;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;
using MyADA.Common.Services.Requests;

namespace MyADA.Common.Services.Services
{
    public class LeadService : ILeadService
    {
        public IHelper Helper = new Helper();

        /// <summary>
        /// Get infos on one lead
        /// </summary>
        /// <param name="p_LeadId"></param>
        /// <returns></returns>
        public async Task<LeadInfos> GetLead(string p_LeadId)
        {
            using(HttpClient client = await Helper.GetClient())
            {
                var response = await client.GetAsync(Constants.DOMAIN + String.Format(Constants.GetLead, p_LeadId));

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<LeadInfos>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    var l_ErrorObject = JsonConvert.DeserializeObject<DefaultReturnResponse>(await response.Content.ReadAsStringAsync());
                    await CoreSDK.Framework.Services.InteractionService().Alert("", "LeadService - GetLead: " + response.ReasonPhrase);
                    return null;
                }
            }
        }

        /// <summary>
        /// Get all Leads
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Lead>> GetLeads(string p_Parameter = "")
        {
            using(HttpClient client = await Helper.GetClient())
            {
                var response = await client.GetAsync(Constants.DOMAIN + Constants.GetLeads + p_Parameter).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    var test = response.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<IEnumerable<Lead>>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    var l_ErrorObject = JsonConvert.DeserializeObject<DefaultReturnResponse>(await response.Content.ReadAsStringAsync());
                    await CoreSDK.Framework.Services.InteractionService().Alert("", "LeadService - GetLeads: " + response.ReasonPhrase);
                    return null;
                }
            }
        }

        /// <summary>
        /// UpdateLead
        /// </summary>
        /// <param name="p_LeadID"></param>
        /// <param name="p_UpdateLeadRequest"></param>
        /// <returns></returns>
        public async Task UpdateLead(string p_LeadID, UpdateLeadRequest p_UpdateLeadRequest)
        {
            using(HttpClient client = await Helper.GetClient())
            {
                var response = await client.PostAsync(Constants.DOMAIN + string.Format(Constants.UpdateLead,p_LeadID),
                    new StringContent(
                        JsonConvert.SerializeObject(p_UpdateLeadRequest),
                        Encoding.UTF8, Constants.DefaultServicesFormat));

                var l_ErrorObject = JsonConvert.DeserializeObject<DefaultReturnResponse>(await response.Content.ReadAsStringAsync());
                await CoreSDK.Framework.Services.InteractionService().Alert(l_ErrorObject.status, l_ErrorObject.message);
            }
        }


        /// <summary>
        /// Change Leads status.
        /// </summary>
        /// <param name="p_LeadId"></param>
        /// <param name="p_SetLeadStatusRequest"></param>
        /// <returns></returns>
        public async Task<bool> SetLeadStatus(string p_LeadId,SetLeadStatusRequest p_SetLeadStatusRequest)
        {
            // TODO-GAV : Implemente for SetLead
            using (HttpClient client = await Helper.GetClient())
            {

                HttpRequestMessage request = new HttpRequestMessage
                {
                    Method = new HttpMethod("PATCH"),
                    RequestUri = new Uri(Constants.DOMAIN + String.Format(Constants.SetLeadStatus,p_LeadId)),
                    Content = new StringContent(
                        JsonConvert.SerializeObject(p_SetLeadStatusRequest),
                        Encoding.UTF8, Constants.DefaultServicesFormat)
                };

                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode) 
                {
                    return true;
                }
                else 
                {
                    return false;
                }

            }
        }

        /// <summary>
        /// Assign lead to a user
        /// </summary>
        /// <param name="p_AssignLeadToUserRequest"></param>
        /// <returns></returns>
        public async Task AssignLeadToUser(AssignLeadToUserRequest p_AssignLeadToUserRequest)
        {
            using(HttpClient client = await Helper.GetClient())
            {
                var response = await client.PostAsync(Constants.DOMAIN + Constants.AssignLeadToUser,
                    new StringContent(
                        JsonConvert.SerializeObject(p_AssignLeadToUserRequest),
                        Encoding.UTF8, Constants.DefaultServicesFormat));

                if (response.IsSuccessStatusCode)
                    await CoreSDK.Framework.Services.InteractionService().Alert("", "Lead(s) allocated", 1, AlertType.Save);
                else
                    await CoreSDK.Framework.Services.InteractionService().Alert("", "LeadService - AssignLeadToUser: " + response.ReasonPhrase);
            }
        }

        public async Task<CountLeadModel> CountLead(CountLeadRequest p_CountLeadRequest)
        {
            using(HttpClient client = await Helper.GetClient())
            {
                var response = await client.GetAsync(Constants.DOMAIN + Constants.CountLead);

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<CountLeadModel>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    var l_ErrorObject = JsonConvert.DeserializeObject<DefaultReturnResponse>(await response.Content.ReadAsStringAsync());
                    await CoreSDK.Framework.Services.InteractionService().Alert("", "LeadService - CountLead: " + response.ReasonPhrase);
                    return null;
                }
            }
        }
    
    }
}
