﻿using MyADA.Common.DAOServices.Models;
using CoreSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using MyADA.Common.Services.Tools;
using Newtonsoft.Json;
using MyADA.Common.Services.Models;
using MyADA.Common.Services.Requests;
using System.Xml.Linq;

namespace MyADA.Common.DAOServices.Services
{
    public class UserService : IUserService
    {
        public IHelper Helper = new Helper();

        /// <summary>
        /// Get user infos
        /// </summary>
        /// <returns></returns>
        public async Task<CurrentUserProfile> GetUserInfos(string p_UserId)
        {
            using(HttpClient client = await Helper.GetClient()){
                var response = await client.GetAsync(Constants.DOMAIN + String.Format(Constants.InfosUser,p_UserId));

                if (response.IsSuccessStatusCode)
                {
                    //var data = response.Content.ReadAsStringAsync().Result;
                    var currentProfile = JsonConvert.DeserializeObject<CurrentUserProfile>(await response.Content.ReadAsStringAsync());
                    if (currentProfile != null && currentProfile.Profile != null)
                    {
                        currentProfile.FormattedProfile = new FormattedProfile();
                         //Title
                        if (currentProfile.Title == "Mr")
                            currentProfile.FormattedProfile.Title = new UserTitle() { Id = 0, Libelle = "Mr" };
                        else if (currentProfile.Title == "Mrs")
                            currentProfile.FormattedProfile.Title = new UserTitle() { Id = 1, Libelle = "Mrs" };
                        else
                            currentProfile.FormattedProfile.Title = new UserTitle() { Id = 2, Libelle = "Ms" };

                        //Position
                        if (currentProfile.Profile.Position != null)
                        {
                            currentProfile.FormattedProfile.Position = new PositionModel();
                            if (currentProfile.Profile.Position.managerother)
                                currentProfile.FormattedProfile.Position = new PositionModel() { Id = 0, Libelle = "Other manager", IdForView = "othermanager" };
                            else if (currentProfile.Profile.Position.nomanagementposition)
                                currentProfile.FormattedProfile.Position = new PositionModel() { Id = 1, Libelle = "No management position", IdForView = "nomanagementposition" };
                            else if (currentProfile.Profile.Position.salesmanager)
                                currentProfile.FormattedProfile.Position = new PositionModel() { Id = 2, Libelle = "Sales manager", IdForView = "salesmanager" };
                            else if (currentProfile.Profile.Position.showroomdirector)
                                currentProfile.FormattedProfile.Position = new PositionModel() { Id = 3, Libelle = "Showroom director", IdForView = "showroomdirector" };
                        }
                        //Organisation
                        if (currentProfile.Profile.Organization != null)
                        {
                            currentProfile.FormattedProfile.Organization = currentProfile.Profile.Organization.employee ? new OrganisationModel() { Id = 0, Libelle = "Yes" } :
                            new OrganisationModel() { Id = 0, Libelle = "No" };
                        }
                        //Domain of actions
                        if (currentProfile.Profile.DomainsOfAction != null)
                        {
                            currentProfile.FormattedProfile.DomainsOfAction = new List<DomainOfActionModel>();
                            if (currentProfile.Profile.DomainsOfAction.accessories)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 1, Libelle = "Accessories", IdForView = "accessories", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.admin)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 0, Libelle = "Admin", IdForView = "admin", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.finance)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 2, Libelle = "Finance", IdForView = "finance", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.marketing)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 3, Libelle = "Marketing", IdForView = "marketing", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.newcars)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 4, Libelle = "New cars", IdForView = "newcars", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.other)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 5, Libelle = "Other", IdForView = "other", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.parts)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 6, Libelle = "Parts", IdForView = "parts", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.service)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 7, Libelle = "Service", IdForView = "service", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.usedcars)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 8, Libelle = "Used cars", IdForView = "usedcars", IsSelected = true });
                        }
                    }
                    return currentProfile;
                }
                else
                {
                    var l_ErrorObject = JsonConvert.DeserializeObject<DefaultReturnResponse>(await response.Content.ReadAsStringAsync());
                    await CoreSDK.Framework.Services.InteractionService().Alert("", "UserService - GetUserInfos: " + response.ReasonPhrase);
                    return null;
                }
            }
        }

        /// <summary>
        /// Get lists of users
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<UserInfos>> GetUsers(string p_Parameter="")
        {
            using(HttpClient client = await Helper.GetClient()) {
                var response = await client.GetAsync(Constants.DOMAIN + Constants.Users + p_Parameter).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<IEnumerable<UserInfos>>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    var l_ErrorObject = JsonConvert.DeserializeObject<DefaultReturnResponse>(await response.Content.ReadAsStringAsync());
                    await CoreSDK.Framework.Services.InteractionService().Alert("", "UserService - GetUsers: " + response.ReasonPhrase);

                    return null;
                }
            }
        }

        /// <summary>
        /// Create user
        /// </summary>
        /// <param name="p_CreateUserRequest"></param>
        /// <returns></returns>
        public async Task CreateUser(UpdateUserRequest p_CreateUserRequest)
        {
            using(HttpClient client = await Helper.GetClient()) {
                var response = await client.PostAsync(Constants.DOMAIN + Constants.CreateUSer,
                    new StringContent(
                        JsonConvert.SerializeObject(p_CreateUserRequest),
                        Encoding.UTF8, Constants.DefaultServicesFormat));

                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                    //return JsonConvert.DeserializeObject<SignInResponse>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    var l_ErrorObject = JsonConvert.DeserializeObject<DefaultReturnResponse>(await response.Content.ReadAsStringAsync());
                    await CoreSDK.Framework.Services.InteractionService().Alert("", "UserService - CreateUser: " + response.ReasonPhrase);

                    //return null;
                }
            }
        }

        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="p_UserId"></param>
        /// <returns></returns>
        public async Task DeleteUser(string p_UserId)
        {
            using(HttpClient client = await Helper.GetClient()) {
                var response = await client.DeleteAsync(Constants.DOMAIN + string.Format(Constants.DeleteUser,p_UserId));

                var l_ErrorObject = JsonConvert.DeserializeObject<DefaultReturnResponse>(await response.Content.ReadAsStringAsync());
                await CoreSDK.Framework.Services.InteractionService().Alert(l_ErrorObject.status, l_ErrorObject.message);
            }
        }

        /// <summary>
        /// Get user profile
        /// </summary>
        /// <returns></returns>
        public async Task<CurrentUserProfile> GetProfile()
        {
            using(HttpClient client = await Helper.GetClient()) {
                var response = await client.GetAsync(Constants.DOMAIN + Constants.Profile);

                if (response.IsSuccessStatusCode)
                {
                    //var data = response.Content.ReadAsStringAsync().Result;
                    var currentProfile = JsonConvert.DeserializeObject<CurrentUserProfile>(await response.Content.ReadAsStringAsync());
                    if (currentProfile != null && currentProfile.Profile != null)
                    {
                        currentProfile.FormattedProfile = new FormattedProfile();
                        //Title
                        if (currentProfile.Title == "Mr")
                            currentProfile.FormattedProfile.Title = new UserTitle() { Id = 0, Libelle = "Mr" };
                        else if (currentProfile.Title == "Mrs")
                            currentProfile.FormattedProfile.Title = new UserTitle() { Id = 1, Libelle = "Mrs" };
                        else
                            currentProfile.FormattedProfile.Title = new UserTitle() { Id = 2, Libelle = "Ms" };

                        //Position
                        if (currentProfile.Profile.Position != null)
                        {
                            if (currentProfile.Profile.Position.managerother)
                                currentProfile.FormattedProfile.Position = new PositionModel() { Id = 0, Libelle = "Other manager", IdForView = "othermanager" };
                            else if (currentProfile.Profile.Position.nomanagementposition)
                                currentProfile.FormattedProfile.Position = new PositionModel() { Id = 1, Libelle = "No management position", IdForView = "nomanagementposition" };
                            else if (currentProfile.Profile.Position.salesmanager)
                                currentProfile.FormattedProfile.Position = new PositionModel() { Id = 2, Libelle = "Sales manager", IdForView = "salesmanager" };
                            else if (currentProfile.Profile.Position.showroomdirector)
                                currentProfile.FormattedProfile.Position = new PositionModel() { Id = 3, Libelle = "Showroom director", IdForView = "showroomdirector" };
                        }
                        //Organisation
                        if (currentProfile.Profile.Organization != null)
                        {
                            currentProfile.FormattedProfile.Organization = currentProfile.Profile.Organization.employee ? new OrganisationModel() { Id = 0, Libelle = "Yes" } :
                            new OrganisationModel() { Id = 0, Libelle = "No" };
                        }
                        //Domain of actions
                        if (currentProfile.Profile.DomainsOfAction != null)
                        {
                            currentProfile.FormattedProfile.DomainsOfAction = new List<DomainOfActionModel>();
                            if (currentProfile.Profile.DomainsOfAction.accessories)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 1, Libelle = "Accessories", IdForView = "accessories", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.admin)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 0, Libelle = "Admin", IdForView = "admin", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.finance)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 2, Libelle = "Finance", IdForView = "finance", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.marketing)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 3, Libelle = "Marketing", IdForView = "marketing", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.newcars)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 4, Libelle = "New cars", IdForView = "newcars", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.other)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 5, Libelle = "Other", IdForView = "other", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.parts)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 6, Libelle = "Parts", IdForView = "parts", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.service)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 7, Libelle = "Service", IdForView = "service", IsSelected = true });
                            if (currentProfile.Profile.DomainsOfAction.usedcars)
                                currentProfile.FormattedProfile.DomainsOfAction.Add(new DomainOfActionModel() { Id = 8, Libelle = "Used cars", IdForView = "usedcars", IsSelected = true });
                        }
                    }
                    return currentProfile;
                }
                else
                {
                    var l_ErrorObject = JsonConvert.DeserializeObject<DefaultReturnResponse>(await response.Content.ReadAsStringAsync());
                    await CoreSDK.Framework.Services.InteractionService().Alert("", "UserService - GetProfile: " + response.ReasonPhrase);

                    return null;
                }
            }
        }

        /// <summary>
        /// Update user infos
        /// </summary>
        /// <param name="p_UserId"></param>
        /// <param name="p_UpdateUserRequest"></param>
        /// <returns></returns>
        public async Task UpdateUser(string p_UserId, UpdateUserRequest p_UpdateUserRequest)
        {
            using(HttpClient client = await Helper.GetClient()) {
                var response = await client.PutAsync(Constants.DOMAIN + String.Format(Constants.Profile, p_UserId), new StringContent(
                    JsonConvert.SerializeObject(p_UpdateUserRequest),
                    Encoding.UTF8, Constants.DefaultServicesFormat));

                var l_ErrorObject = JsonConvert.DeserializeObject<DefaultReturnResponse>(await response.Content.ReadAsStringAsync());
                await CoreSDK.Framework.Services.InteractionService().Alert(l_ErrorObject.status, l_ErrorObject.message);
            }
        }

        /// <summary>
        /// Get user count
        /// </summary>
        /// <returns></returns>
        public async Task GetUsersCount(UserCountRequest p_UserCountRequest) 
        {
            using(HttpClient client = await Helper.GetClient()) {
                var response = await client.GetAsync(Constants.DOMAIN + Constants.CountUsers);

                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                    //return JsonConvert.DeserializeObject<SignInResponse>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    var l_ErrorObject = JsonConvert.DeserializeObject<DefaultReturnResponse>(await response.Content.ReadAsStringAsync());
                    await CoreSDK.Framework.Services.InteractionService().Alert("", "UserService - GetUsersCount: " + response.ReasonPhrase);

                    //return null;
                }
            }
        }

        /// <summary>
        /// Get user RSSfeed
        /// </summary>
        /// <param name="p_UserId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<RssFeed>> GetUserRssFeeds(string p_UserId)
        {
            using(HttpClient client = await Helper.GetClient())
            {
                try
                {
                    var response = await client.GetAsync(Constants.DOMAIN + string.Format(Constants.GetUserRssFeeds, p_UserId));

                    if (response.IsSuccessStatusCode)
                    {
                        var data = response.Content.ReadAsStringAsync().Result;
                        return JsonConvert.DeserializeObject<IEnumerable<RssFeed>>(await response.Content.ReadAsStringAsync());
                    }
                    else
                    {
                        var l_ErrorObject = JsonConvert.DeserializeObject<DefaultReturnResponse>(await response.Content.ReadAsStringAsync());
                        await CoreSDK.Framework.Services.InteractionService().Alert("", "UserService - GetUserRssFeeds: " + response.ReasonPhrase);

                        return null;
                    }
                }
                catch
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Subscribe to a rss checked in EnterRssUrlView
        /// </summary>
        /// <param name="p_UserId"></param>
        /// <returns></returns>
        public async Task<bool> SubscribeRssFeed(string p_UserId, UserRssRequest p_Request)
        {
            try
            {
                using (HttpClient client = await Helper.GetClient())
                {
                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Method = new HttpMethod("PATCH"),
                        RequestUri = new Uri(Constants.DOMAIN + String.Format(Constants.SubscribeRssFeed, p_UserId)),
                        Content = new StringContent(
                            JsonConvert.SerializeObject(p_Request),
                            Encoding.UTF8, Constants.DefaultServicesFormat)
                    };

                    var response = await client.SendAsync(request);

                    if (response.IsSuccessStatusCode)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Unsubscribe to a rss unchecked in EnterRssUrlView
        /// </summary>
        /// <param name="p_UserId"></param>
        /// <returns></returns>
        public async Task<bool> UnsubcribeRssFeed(string p_UserId, UserRssRequest p_Request)
        {
            try
            {
                using (HttpClient client = await Helper.GetClient())
                {
                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Method = new HttpMethod("PATCH"),
                        RequestUri = new Uri(Constants.DOMAIN + String.Format(Constants.UnsubscribeRssFeed, p_UserId)),
                        Content = new StringContent(
                            JsonConvert.SerializeObject(p_Request),
                            Encoding.UTF8, Constants.DefaultServicesFormat)
                    };

                    var response = await client.SendAsync(request);

                    if (response.IsSuccessStatusCode)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region WS Static
        public List<DomainOfActionModel> GetDomainOfActionsList()
        {
            var DomainOfActionsList = new List<DomainOfActionModel>();
            DomainOfActionsList.Add(new DomainOfActionModel() { Id = 0, Libelle = "Admin", IdForView = "admin" });
            DomainOfActionsList.Add(new DomainOfActionModel() { Id = 1, Libelle = "Accessories", IdForView = "accessories" });
            DomainOfActionsList.Add(new DomainOfActionModel() { Id = 2, Libelle = "Finance", IdForView = "finance" });
            DomainOfActionsList.Add(new DomainOfActionModel() { Id = 3, Libelle = "Marketing", IdForView = "marketing" });
            DomainOfActionsList.Add(new DomainOfActionModel() { Id = 4, Libelle = "New cars", IdForView = "newcars" });
            DomainOfActionsList.Add(new DomainOfActionModel() { Id = 5, Libelle = "Other", IdForView = "other" });
            DomainOfActionsList.Add(new DomainOfActionModel() { Id = 6, Libelle = "Parts", IdForView = "parts" });
            DomainOfActionsList.Add(new DomainOfActionModel() { Id = 7, Libelle = "Service", IdForView = "service" });
            DomainOfActionsList.Add(new DomainOfActionModel() { Id = 8, Libelle = "Used cars", IdForView = "usedcars" });
            return DomainOfActionsList;
        }

        public List<OrganisationModel> GetOrganizationsList()
        {
            var OrganizationsList = new List<OrganisationModel>();
            OrganizationsList.Add(new OrganisationModel() { Id = 0, Libelle = "Yes" });
            OrganizationsList.Add(new OrganisationModel() { Id = 1, Libelle = "No" });
            return OrganizationsList;
        }

        public List<PositionModel> GetPositionsList()
        {
            var PositionsList = new List<PositionModel>();
            PositionsList.Add(new PositionModel() { Id = 0, Libelle = "Other manager", IdForView = "othermanager" });
            PositionsList.Add(new PositionModel() { Id = 1, Libelle = "No management position", IdForView = "nomanagementposition" });
            PositionsList.Add(new PositionModel() { Id = 2, Libelle = "Sales manager", IdForView = "salesmanager" });
            PositionsList.Add(new PositionModel() { Id = 3, Libelle = "Showroom director", IdForView = "showroomdirector" });
            return PositionsList;
        }

        public List<UserTitle> GetTitlesList()
        {
            var UserTitleList = new List<UserTitle>();
            UserTitleList.Add(new UserTitle() { Id = 0, Libelle = "Mr" });
            UserTitleList.Add(new UserTitle() { Id = 1, Libelle = "Mrs" });
            UserTitleList.Add(new UserTitle() { Id = 2, Libelle = "Ms" });
            return UserTitleList;
        }
     
        #endregion

        public async Task<IEnumerable<RssFeed>> GetRssfeeds()
        {
            try
            {
                using (HttpClient client = await Helper.GetClient())
                {
                    var response = await client.GetAsync(Constants.DOMAIN + Constants.GetRssfeeds);

                    if (response.IsSuccessStatusCode)
                    {
                        var data = response.Content.ReadAsStringAsync().Result;
                        return JsonConvert.DeserializeObject<IEnumerable<RssFeed>>(await response.Content.ReadAsStringAsync());
                    }
                    else
                    {
                        await CoreSDK.Framework.Services.InteractionService().Alert("", "UserService - GetRssfeeds: " + response.ReasonPhrase);
                        return null;
                    }
                }
            }
            catch
            {
                return null;
            }
        }
        
        /// <summary>Récupère les items RSS d'une URL fournie et les renvoie sous forme d'objet Post.</summary>
        /// <param name="url">L'URL du flux RSS fourni.</param>
        /// <returns>Object Post.</returns>
        public async Task<IEnumerable<Post>> ReadItem(string url)
        {
            try
            {
                var rssFeed = XDocument.Load(url);

                var posts = from item in rssFeed.Descendants("item") select new Post
                {
                    Title = item.Element("title").Value,
                    Link  = item.Element("link").Value
                };

                return posts;
            }
            catch(Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(MyADA.Common.Services.Properties.Resources.Error, ex.Message);
                return null;
            }
        }
    }
}
