﻿using MyADA.Common.DAOServices.Models;
using MyADA.Common.DAOServices.Requests;
using CoreSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.DAOServices.Services
{
    public class NotificationService : INotificationService
    {
        public Task<RemoteCallResult> AddUserSolAsync(string p_Token)
        {
            return CoreSDK.Framework.Services.RemoteCallService().Call(Constants.NotificationService, "addUserSol", new
            {
                token = p_Token
            });
        }

        public Task<RemoteCallResult<NotificationBatchConfig>> GetNotificationBatchConfigAsync(GetNotificationBatchConfigRequest p_Request)
        {
            return CoreSDK.Framework.Services.RemoteCallService().Call<NotificationBatchConfig>(Constants.NotificationService, "getNotificationBatchConfig", new
            {
                os = p_Request.Os,
                appName = p_Request.AppName,
                date = p_Request.Date,
                hash = p_Request.Hash,
                origin = p_Request.Hash,
                locale = p_Request.Locale
            });
        }
    }
}
