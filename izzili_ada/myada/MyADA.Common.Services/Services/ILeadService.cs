﻿using MyADA.Common.Services.Models;
using System;
using System.Collections.Generic;
namespace MyADA.Common.Services.Services
{
    public interface ILeadService
    {
        System.Threading.Tasks.Task AssignLeadToUser(MyADA.Common.Services.Requests.AssignLeadToUserRequest p_AssignLeadToUserRequest);
        System.Threading.Tasks.Task<CountLeadModel> CountLead(MyADA.Common.Services.Requests.CountLeadRequest p_CountLeadRequest);
        System.Threading.Tasks.Task<LeadInfos> GetLead(string p_LeadId);
        System.Threading.Tasks.Task<IEnumerable<Lead>> GetLeads(string p_Parameter = "");
        System.Threading.Tasks.Task<bool> SetLeadStatus(string p_LeadId, MyADA.Common.Services.Requests.SetLeadStatusRequest p_SetLeadStatusRequest);
        System.Threading.Tasks.Task UpdateLead(string p_LeadID, MyADA.Common.Services.Requests.UpdateLeadRequest p_UpdateLeadRequest);
    }
}
