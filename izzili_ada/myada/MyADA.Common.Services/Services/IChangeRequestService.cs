﻿using System;
namespace MyADA.Common.Services.Services
{
   public  interface IChangeRequestService
    {
       System.Threading.Tasks.Task<bool> Create(MyADA.Common.Services.Requests.ChangeRequest p_ChangeRequest);
    }
}
