﻿using MyADA.Common.DAOServices.Models;
using CoreSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.DAOServices.Services
{
    internal class GlobalConfigService : MyADA.Common.DAOServices.Services.IGlobalConfigService 
    {
        #region Champs
        private string ConfigFileName = "CONFIG_06D6969D91DA43D2801688D2AC9650D2.config";
        private IFileService FileService;
        #endregion

        private GlobalConfigModel _Data;
        
        public GlobalConfigModel Data
        {
            get { return _Data; }
        }

        public GlobalConfigService()
        {
            FileService = CoreSDK.Framework.Services.Container().Resolve<IFileService>();
            LoadConfig();
        }

        public void LoadConfig()
        {
            if (FileService.Exist(ConfigFileName))
            {
                _Data = FileService.LoadObject<GlobalConfigModel>(ConfigFileName);
              
            }
            else
            {
                _Data = new GlobalConfigModel();
                _Data.ViewedOrdersIds = new List<int>();
                _Data.BookedRSSList = new List<string>();
             
            }
        }

        public void SaveConfig()
        {
            FileService.SaveObject<GlobalConfigModel>(ConfigFileName, _Data);
        }
    }
}

namespace CoreSDK
{
    public static class GlobalConfigServiceExtension
    {
        public static MyADA.Common.DAOServices.Services.IGlobalConfigService GlobalConfigService(this CoreSDK.IService p_Service)
        {
            return CoreSDK.Framework.Services.Container().Resolve<MyADA.Common.DAOServices.Services.IGlobalConfigService>();
        }
    }
}
