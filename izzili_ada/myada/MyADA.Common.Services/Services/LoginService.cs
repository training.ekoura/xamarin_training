﻿using MyADA.Common.DAOServices.Services;
using MyADA.Common.Services.Models;
using MyADA.Common.Services.Requests;
using MyADA.Common.Services.Tools;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;

namespace MyADA.Common.Services.Services
{
    public class LoginService : ILoginService
    {
        public IHelper Helper = new Helper();
        private IGlobalConfigService GlobalConfigService = CoreSDK.Framework.Services.GlobalConfigService();
        /// <summary>
        /// For signin a user.
        /// </summary>
        /// <param name="p_LoginRequest"></param>
        /// <returns></returns>
        public async Task<SignInResponse> SignIn(LoginRequest p_LoginRequest)
        {
            using (HttpClient client = await Helper.GetClient())
            {
                var response = await client.PostAsync(Constants.DOMAIN + Constants.SigninService,
                    new StringContent(
                        JsonConvert.SerializeObject(p_LoginRequest),
                        Encoding.UTF8, Constants.DefaultServicesFormat));

                if (response.IsSuccessStatusCode)
                {

                    var l_Data = JsonConvert.DeserializeObject<SignInResponse>(await response.Content.ReadAsStringAsync());
                    GlobalConfigService.Data.Secret = l_Data.Secret;
                    GlobalConfigService.Data.Token = l_Data.Token;
                    GlobalConfigService.Data.DeviceToken = CoreSDK.Framework.Services.InteractionService().DeviceToken;
                    GlobalConfigService.Data.DevicePlatform = CoreSDK.Framework.Services.InteractionService().DevicePlatform;
                    GlobalConfigService.SaveConfig();

                    if (GlobalConfigService.Data.Token == null && GlobalConfigService.Data.Secret != null)
                        await CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Validate_Email_Message, Properties.Resources.Validate_Email_Message, 3, AlertType.Success);

                    return l_Data;
                }
                else
                {
                    if (GlobalConfigService.Data.Token == null && GlobalConfigService.Data.Secret != null)
                        await CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Validate_Email_Message, Properties.Resources.Validate_Email_Message, 3, AlertType.Success);
                    else
                        await CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error_Email_Message, Properties.Resources.Error_Email_Message);
                    return null;
                }
            }
        }

    }
}
