﻿using System;
namespace MyADA.Common.DAOServices.Services
{
    public interface IGlobalConfigService
    {
        MyADA.Common.DAOServices.Models.GlobalConfigModel Data { get; }
        void LoadConfig();
        void SaveConfig();
    }
}
