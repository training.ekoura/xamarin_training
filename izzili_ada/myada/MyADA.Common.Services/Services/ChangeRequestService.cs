﻿using MyADA.Common.Services.Models;
using MyADA.Common.Services.Requests;
using MyADA.Common.Services.Tools;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;

namespace MyADA.Common.Services.Services
{
    public class ChangeRequestService : MyADA.Common.Services.Services.IChangeRequestService
    {
        public IHelper Helper = new Helper();

        public async Task<bool> Create(ChangeRequest p_ChangeRequest)
        {
            using (HttpClient client = await Helper.GetClient())
            {
                var response = await client.PostAsync(Constants.DOMAIN + Constants.ChangeRequest,
                    new StringContent(
                        JsonConvert.SerializeObject(p_ChangeRequest),
                        Encoding.UTF8, Constants.DefaultServicesFormat));

                if (response.IsSuccessStatusCode)
                {
                   return true;
                }
                else
                {
                    await CoreSDK.Framework.Services.InteractionService().Alert("", "ChangeRequestService - Create: "+response.ReasonPhrase);
                    return false;
                }
            }
        }
    }
}
