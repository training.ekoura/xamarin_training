﻿using MyADA.Common.Services.Models;
using MyADA.Common.Services.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyADA.Common.DAOServices.Services
{
    public interface IUserService
    {
        Task CreateUser(MyADA.Common.Services.Requests.UpdateUserRequest p_CreateUserRequest);
        Task DeleteUser(string p_UserId);
        Task<CurrentUserProfile> GetProfile();
        Task<CurrentUserProfile> GetUserInfos(string p_UserId);
        Task<IEnumerable<RssFeed>> GetUserRssFeeds(string p_UserId);
        Task<IEnumerable<RssFeed>> GetRssfeeds();
        Task<IEnumerable<MyADA.Common.Services.Models.UserInfos>> GetUsers(string p_Parameter = "");
        Task GetUsersCount(MyADA.Common.Services.Requests.UserCountRequest p_UserCountRequest);
        Task<bool> SubscribeRssFeed(string p_UserId, UserRssRequest p_Request);
        Task<bool> UnsubcribeRssFeed(string p_UserId, UserRssRequest p_Request);
        Task UpdateUser(string p_UserId, MyADA.Common.Services.Requests.UpdateUserRequest p_UpdateUserRequest);
        List<DomainOfActionModel> GetDomainOfActionsList();
        List<OrganisationModel> GetOrganizationsList();
        List<PositionModel> GetPositionsList();
        List<UserTitle> GetTitlesList();
        Task<IEnumerable<Post>> ReadItem(string url);
    }
}