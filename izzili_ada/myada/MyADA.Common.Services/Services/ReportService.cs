﻿using MyADA.Common.Services.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.Services.Services
{
    public class ReportService : IReportService
    {
        public IHelper Helper = new Helper();

        /// <summary>
        /// Get single user report
        /// </summary>
        /// <param name="p_UserId"></param>
        /// <returns></returns>
        public async Task<string> GetMyReport(string p_UserId)
        {
            using (HttpClient client = await Helper.GetClient())
            {
                var response = await client.GetAsync(Constants.DOMAIN + String.Format(Constants.GetMyReport, p_UserId)).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    //var result = response.Content.ReadAsStringAsync().Result;
                    return response.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Get Showroom consolidated report
        /// </summary>
        /// <param name="p_ShowroomId"></param>
        /// <returns></returns>
        public async Task<string> GetConsolidatedReport(string p_ShowroomId)
        {
            using (HttpClient client = await Helper.GetClient())
            {
                var response = await client.GetAsync(Constants.DOMAIN + String.Format(Constants.GetConsolidatedReport, p_ShowroomId)).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    return null;
                }
            }
        }


        public async Task<string> GetOverAll()
        {
            using (HttpClient client = await Helper.GetClient())
            {
                var response = await client.GetAsync(Constants.DOMAIN + Constants.Users + Constants.GetOverAll).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    return null;
                }
            }
        }

    }
}
