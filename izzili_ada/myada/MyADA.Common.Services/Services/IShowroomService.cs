﻿using MyADA.Common.Services.Models;
using System;
using System.Collections.Generic;
namespace MyADA.Common.Services.Services
{
    public interface IShowroomService
    {
        System.Threading.Tasks.Task<IEnumerable<ShowroomModels>> GetShowroom();
    }
}
