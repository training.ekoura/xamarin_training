﻿using System;
namespace MyADA.Common.DAOServices.Services
{
    public interface INotificationService
    {
        System.Threading.Tasks.Task<CoreSDK.RemoteCallResult> AddUserSolAsync(string p_Token);
        System.Threading.Tasks.Task<CoreSDK.RemoteCallResult<MyADA.Common.DAOServices.Models.NotificationBatchConfig>> GetNotificationBatchConfigAsync(MyADA.Common.DAOServices.Requests.GetNotificationBatchConfigRequest p_Request);
    }
}
