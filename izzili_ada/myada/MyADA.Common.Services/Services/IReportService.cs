﻿using System;
namespace MyADA.Common.Services.Services
{
    public interface IReportService
    {
        System.Threading.Tasks.Task<string> GetConsolidatedReport(string p_ShowroomId);
        System.Threading.Tasks.Task<string> GetMyReport(string p_UserId);
        System.Threading.Tasks.Task<string> GetOverAll();
    }
}
