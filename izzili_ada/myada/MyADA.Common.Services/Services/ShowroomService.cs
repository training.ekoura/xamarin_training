﻿using MyADA.Common.Services.Models;
using MyADA.Common.Services.Tools;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;

namespace MyADA.Common.Services.Services
{
    public class ShowroomService : MyADA.Common.Services.Services.IShowroomService
    {
        public IHelper Helper = new Helper();

        public async Task<IEnumerable<ShowroomModels>> GetShowroom ()
        {
            using (HttpClient client = await Helper.GetClient())
            {
                var response = await client.GetAsync(Constants.DOMAIN + Constants.GetShowrooms );

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<IEnumerable<ShowroomModels>>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    //var l_ErrorObject = JsonConvert.DeserializeObject<DefaultReturnResponse>(await response.Content.ReadAsStringAsync());
                    await CoreSDK.Framework.Services.InteractionService().Alert("", response.ReasonPhrase);
                    return null;
                }
            }
        }
    }
}
