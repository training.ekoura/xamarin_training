﻿using MyADA.Common.Services.Models;
using System;
namespace MyADA.Common.DAOServices.Services
{
    public interface ILoginService
    {
        System.Threading.Tasks.Task<SignInResponse> SignIn(MyADA.Common.Services.Requests.LoginRequest p_LoginRequest);
    }
}
