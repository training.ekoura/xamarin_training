﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common
{
    public static class Constants
    {

        public const string DefaultServicesFormat = "application/json";
        //Domain       
#if DEBUG
        //QUAL
        public const string DOMAIN = "https://us46ac2u4c.execute-api.ap-northeast-1.amazonaws.com/test/";
        //public const string DOMAIN = "https://40y97ui101.execute-api.eu-central-1.amazonaws.com/demo/";
        public const string GeneralConditionsLink = "https://s3.ap-northeast-2.amazonaws.com/www.ada.myizzili.com/assets/test/public/legalNotice.html";
#else
        //prod
        //public const string DOMAIN = "https://us46ac2u4c.execute-api.ap-northeast-1.amazonaws.com/prod/";
        public const string DOMAIN = "https://40y97ui101.execute-api.eu-central-1.amazonaws.com/demo/";
        public const string GeneralConditionsLink = "https://s3.ap-northeast-2.amazonaws.com/www.ada.myizzili.com/assets/demo/public/legalNotice.html";
        //public const string GeneralConditionsLink = "https://s3.ap-northeast-2.amazonaws.com/www.ada.myizzili.com/assets/prod/public/legalNotice.html";
#endif
        /***************************************************************************************************************
        //WebService Names
        ***************************************************************************************************************/
        /////////////////////////////////////////////////
        // Authentication 
        /////////////////////////////////////////////////
        public const string SigninService = "signin";

        /////////////////////////////////////////////////
        // User
        /////////////////////////////////////////////////
        public const string InfosUser = "users/{0}";
        public const string Users = "users";
        public const string CreateUSer = "users";
        public const string DeleteUser = "users/{0}";
        public const string UpdateUser = "users/{0}";
        public const string Profile = "users/profile";
        public const string CountUsers = "users/count";
        //public const string GetUserRssFeeds = "users/{0}/rssfeeds";
        public const string GetUserRssFeeds = "users/{0}/rssfeeds";
        public const string SubscribeRssFeed = "users/{0}/rssfeeds/subscribe";
        public const string UnsubscribeRssFeed = "users/{0}/rssfeeds/unsubscribe";
        public const string GetRssfeeds = "rssfeeds";
        

        /////////////////////////////////////////////////
        // Change Request
        /////////////////////////////////////////////////
        public const string ChangeRequest = "changerequests";

        /////////////////////////////////////////////////
        // LEAD
        /////////////////////////////////////////////////
        public const string GetLead = "leads/{0}";
        public const string GetLeads = "leads";
        public const string UpdateLead = "leads/{0}";
        public const string SetLeadStatus = "leads/{0}/status";
        public const string AssignLeadToUser = "leads/user";
        public const string CountLead = "leads/count";
        public const string GetLeadNotifications = "leads/{0}/notifications";

        /////////////////////////////////////////////////
        // SHOWROOM
        /////////////////////////////////////////////////
        public const string GetShowroom = "showroom";
        public const string GetShowrooms = "showrooms";


        /////////////////////////////////////////////////
        // REPORT
        /////////////////////////////////////////////////
        public const string GetMyReport = "reports/my/{0}";
        public const string GetConsolidatedReport = "reports/consolidated/{0}";
        public const string GetOverAll = "reports/overall/";
        

        public const string NotificationService = "V3_00/Notification.cfc";

        public const int CacheService = 60;
        public const int CacheServiceFluidite = 5;

        public const string AppName = "myresto";

    }
}
