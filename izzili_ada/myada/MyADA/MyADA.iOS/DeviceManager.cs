﻿using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(MyADA.iOS.DeviceManager))]

namespace MyADA.iOS
{
    class DeviceManager  : IDeviceManager
    {
        public string GetDeviceIMEI()
        {
            return UIDevice.CurrentDevice.IdentifierForVendor.AsString();
        }
    }
}
