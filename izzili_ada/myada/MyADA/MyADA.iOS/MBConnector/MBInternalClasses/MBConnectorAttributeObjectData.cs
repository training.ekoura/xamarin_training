﻿using System;
using MobileBridge;
using Foundation;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections;
using MyADA.Common;
using MyADA.Common.Views;


namespace MyADA.iOS
{
	public class MBConnectorAttributeObjectData
	{
		MobileBridgeDelegate mbDelegate;
		public static MBXamarinFormsTrainingAppPage.CustomerAttributeDelegate attributeDelegate;
		public MBConnectorAttributeObjectData()
		{
		}

		public void MB_GetCustomerAttribute(MBXamarinFormsTrainingAppPage.CustomerAttributeDelegate _delegate)
		{
			attributeDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_GetCustomerAttribute(mbDelegate);
			}
		}


		public void MB_SetCustomerAttribute(MBXamarinFormsTrainingAppPage.CustomerAttributeDelegate _delegate)
		{
			attributeDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_SetCustomerAttribute(MBConnectorInitLibrary.attributeObject, mbDelegate);
					
			}
		}

		public void MB_ClearCustomerAttribute(MBXamarinFormsTrainingAppPage.CustomerAttributeDelegate _delegate)
		{
			attributeDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_ClearCustomerAttribute(mbDelegate);

			}
		}

		//DELEGATE//
		class MobileBridgeDelegate : MB_AttributeDelegate
		{
			public override void Data(AttributeRequestType attributeRequestType, Foundation.NSDictionary data)
			{
				NSError err;
				var jsonData = NSJsonSerialization.Serialize(data, NSJsonWritingOptions.PrettyPrinted, out err).ToString();

				if (attributeRequestType == AttributeRequestType.get_attribute)
				{
					attributeDelegate.MB_GetCustomerAttributeDataFinished(jsonData);
				}else if (attributeRequestType == AttributeRequestType.clear_attribute)
				{
					attributeDelegate.MB_CustomerAttributeDeleted(jsonData);

				} else if (attributeRequestType == AttributeRequestType.set_attribute)
				{
					attributeDelegate.MB_SetCustomerAttributeDataFinished(jsonData);
				}

			}

		}
	}
}

