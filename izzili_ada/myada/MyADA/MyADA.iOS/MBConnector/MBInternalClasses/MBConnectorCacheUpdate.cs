﻿using System;
using MobileBridge;
using UIKit;
using Foundation;
using MyADA.Common;
using MyADA.Common.Views;
namespace MyADA.iOS
{
	public class MBConnectorCacheUpdate
	{
		MobileBridgeDelegate mbDelegate;
		public static MBXamarinFormsTrainingAppPage.CacheUpdateDelegate cacheDelegate;

		public void MB_UpdateCacheCustomerAtribute(MBXamarinFormsTrainingAppPage.CacheUpdateDelegate _delegate)
		{

			cacheDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_UpdateCachedAssets(UpdateCacheType.customer_attribute, mbDelegate);
			}
		}



		class MobileBridgeDelegate : MB_UpdateCachingDataDelegate
		{
			public override void MB_UpdateCachingDataFinished()
			{
				cacheDelegate.MB_UpdateCacheFinished();
			}
		}
	}
}

