﻿using System;
using MobileBridge;
using UIKit;
using Foundation;
using MyADA.Common;

namespace MyADA.iOS
{
	public class MBConnectorInitLibrary
	{
		public InitMBDelegate initDelegate;
		public static MobileBridgeLibrary mbLib;
		public static MB_AttributeObject attributeObject;

		public MBConnectorInitLibrary()
		{
		}


		public void MB_InitLibrary(string clientKey, string clientSecret)
		{
			this.initDelegate = new InitMBDelegate(this);
			/*
			 * Create credentails object with key/secret you obtained during registration with MobileBridge
			 */
			var credentials = new MB_ClientCredentials(clientKey, clientSecret);

			/*
		 	* Initializes MobileBridge library
		 	* Once library is initialized - "MB_LibraryInitialized:" is triggered 
		 	*/
			MobileBridgeLibrary.MB_InitLibrary(credentials, this.initDelegate);
		}



		public void libraryLoaded(MobileBridgeLibrary mbLib, string description)
		{
			if (mbLib != null)
			{
				MBConnectorInitLibrary.attributeObject = new MB_AttributeObject();
				MBConnectorInitLibrary.mbLib = mbLib;
				mbLib.MB_PrintSQSMessages(true);
				//var d = new MBConnectorPromotionsData();
				//d.MB_GetPublicPromotionsData
			}
			else {
				Console.WriteLine("Oops, something went wrong with initialization of MobileBridgeLibrary" + description);
			}
		}


		public class InitMBDelegate : MobileBridgeLibraryDelegate
		{
			
			public MBConnectorInitLibrary mb_connector;

			public InitMBDelegate(MBConnectorInitLibrary connector)
			{				
				mb_connector = connector;
			}




			public override void MB_LibraryInitialized(MobileBridgeLibrary mbLib, string description)
			{
				mb_connector.libraryLoaded(mbLib, description);
			}

			public override void MB_SettingEventReply(NSDictionary response)
			{

			}

			public override void MB_NetworkAlertPrompt(int alertId)
			{

			}

			public override void MB_LocationUpdate(int position, int max)
			{

			}

			public override void MB_UpdateCachingDataFinished()
			{


			}
		}
	}
}

