﻿using System;
using MobileBridge;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections;
using Foundation;
using MyADA.Common.Views;

namespace MyADA.iOS
{
	public class MBConnectorRichMessageData
	{
		MobileBridgeDelegate mbDelegate;
		public static MBXamarinFormsTrainingAppPage.RichMessageDelegate rmDelegate;


		public MBConnectorRichMessageData()
		{
		}

		public void MB_GetRichMessageData(string assetInfo, MBXamarinFormsTrainingAppPage.RichMessageDelegate _delegate)
		{

			rmDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{

				var str = new NSString(assetInfo);

				NSData data = str.Encode(NSStringEncoding.UTF8);
				NSError err;
				var jsonData = NSJsonSerialization.Deserialize(data, NSJsonReadingOptions.MutableContainers, out err);

				if (jsonData != null)
				{
					MBConnectorInitLibrary.mbLib.MB_GetRichMessageData((NSDictionary)jsonData, mbDelegate);
				}
				else {
					Console.WriteLine("can't convert asset info to nsdictionary");
				}


			}
		}
		public void MB_GetAllRichMessages(MBXamarinFormsTrainingAppPage.RichMessageDelegate _delegate)
		{
			Console.WriteLine("Enter native MB_GetAllRichMessages in MBConnectorRichMessageData ==> OK");
			rmDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			var tags = new NSObject[0];
			MBConnectorInitLibrary.mbLib.MB_GetAllRichMessages(tags, mbDelegate);
		}

		class MobileBridgeDelegate : MB_GetRichMessageDataDelegate
		{
			public override void MB_GetRichMessageDataFinished(Foundation.NSDictionary data)
			{
				NSError err;
				var jsonData = NSJsonSerialization.Serialize(data, NSJsonWritingOptions.PrettyPrinted, out err).ToString();
				rmDelegate.MB_GetRichMessageData(jsonData);

			}

			public override void MB_GetAllRichMessagesFinished(Foundation.NSObject[] data)
			{
				//var result = new T();
				//foreach (var item in data)
				//{
				//	if (item != null)
				//	{
				//		NSError err;
				//		var jsonData = NSJsonSerialization.Serialize(item, NSJsonWritingOptions.PrettyPrinted, out err).ToString();
				//		result.Add(Newtonsoft.Json.JsonConvert.DeserializeObject<T2>(jsonData));
				//	}
				//}

				//String strJson = JsonConvert.SerializeObject(result);

				//rmDelegate.MB_GetAllRichMessages("");
			}
		}


	}
}

