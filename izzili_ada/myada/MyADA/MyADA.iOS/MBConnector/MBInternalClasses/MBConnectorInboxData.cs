﻿using System;
using MobileBridge;
using Foundation;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections;
using MyADA.Common;
using MyADA.Common.Views;
using MyADA.Common.DAOServices.Models;

namespace MyADA.iOS
{
	public class MBConnectorInboxData
	{
		MobileBridgeDelegate mbDelegate;
		public static MBXamarinFormsTrainingAppPage.InboxDataFinished inboxDelegate;
		public static TaskCompletionSource<List<PublicPromotion>> _taskCompletionSource;

		public MBConnectorInboxData()
		{
			_taskCompletionSource = new TaskCompletionSource<List<PublicPromotion>>();
		}

		public void MB_GetInboxData(MBXamarinFormsTrainingAppPage.InboxDataFinished _delegate)
		{
			inboxDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_GetInboxData(MB_InboxDataType.get_inbox_data, "", mbDelegate);
			}
		}

		public void MB_DeleteInboxData(MBXamarinFormsTrainingAppPage.InboxDataFinished _delegate)
		{
			inboxDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_GetInboxData(MB_InboxDataType.delete_all_data, "", mbDelegate);
			}
		}

		public void MB_DeleteInboxItem(string inbox_id, MBXamarinFormsTrainingAppPage.InboxDataFinished _delegate)
		{
			inboxDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_GetInboxData(MB_InboxDataType.delete_item_data, inbox_id, mbDelegate);
			}
		}



		//DELEGATE//
		class MobileBridgeDelegate : MB_GetInboxDataDelegate
		{
			public override void Data(MB_InboxDataType identifier, NSDictionary data)
			{
				if (identifier == MB_InboxDataType.get_inbox_data)
				{
					NSError err;
					string jsonData = NSJsonSerialization.Serialize(data, NSJsonWritingOptions.PrettyPrinted, out err).ToString();
					inboxDelegate.MB_GetInboxDataFinished(jsonData);
				}	
				else if (identifier == MB_InboxDataType.delete_all_data)
				{
					NSError err;
					string jsonData = NSJsonSerialization.Serialize(data, NSJsonWritingOptions.PrettyPrinted, out err).ToString();
					inboxDelegate.MB_InboxDataDeleted(jsonData);
				}
				else if (identifier == MB_InboxDataType.delete_item_data)
				{
					NSError err;
					string jsonData = NSJsonSerialization.Serialize(data, NSJsonWritingOptions.PrettyPrinted, out err).ToString();

					inboxDelegate.MB_InboxItemDeleted(jsonData);
				}
			}

		}

	}
}

