﻿using System;
using System.Collections.Generic;

namespace MyADA.iOS
{
	public class _Id
	{
		public int id { get; set; }
		public string type { get; set; }
	}

	public class RootObject
	{
		public _Id id { get; set; }
		public string title { get; set; }
		public string description { get; set; }
		public string image_url { get; set; }
		public string image_name { get; set; }
		public int end_date { get; set; }
		public int loyalty_cost { get; set; }
		public List<object> tags { get; set; }
	}

}

