﻿using System;
using MobileBridge;
using Foundation;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections;
using MyADA.Common;
using MyADA.Common.Views;
using MyADA.Common.DAOServices.Models;

namespace MyADA.iOS
{
	public class MBConnectorPromotionsData
	{
		MobileBridgeDelegate mbDelegate;
		public static MBXamarinFormsTrainingAppPage.MyDelegate myDelegateDelegate;
		public static TaskCompletionSource<List<PublicPromotion>> _taskCompletionSource;

		public MBConnectorPromotionsData()
		{
			_taskCompletionSource = new TaskCompletionSource<List<PublicPromotion>>();
		}

		public void MB_GetPublicPromotionsData(MBXamarinFormsTrainingAppPage.MyDelegate _delegate)
		{

			myDelegateDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_GetPublicPromotionData(mbDelegate);
			}
		}

		private static void HandleCallback<T, T2>(TaskCompletionSource<T> taskCompletionSource, NSObject[] data) where T : IList, new()
		{
			var result = new T();
			foreach (var item in data)
			{
				if (item != null)
				{
					NSError err;
					var jsonData = NSJsonSerialization.Serialize(item, NSJsonWritingOptions.PrettyPrinted, out err).ToString();
					result.Add(Newtonsoft.Json.JsonConvert.DeserializeObject<T2>(jsonData));
				}
			}

			String strJson = JsonConvert.SerializeObject(result);
			myDelegateDelegate.MB_GetPublicPromotionDataFinished(strJson);
		}

		class MobileBridgeDelegate : MB_GetPublicPromotionDataDelegate
		{
			public override void MB_GetPublicPromotionDataFinished(NSObject[] data)
			{
				HandleCallback<List<PublicPromotion>, PublicPromotion>(_taskCompletionSource, data);
			}
		}
	}
}

