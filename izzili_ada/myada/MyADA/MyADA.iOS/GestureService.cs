﻿using CoreSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
//using Xamarin.Forms.Platform.iOs;
using UIKit;
using System.Diagnostics;

namespace MyADA
{
    class GestureService : IGestureService
    {
        public void Hook(View p_Object)
        {
            new iOSGestureManager(p_Object);
        }
    }

    class iOSGestureManager : GestureManager
    {
        UIView _NativeView;

        public iOSGestureManager(View p_View)
            : base(p_View)
        {

        }

        protected override void OnLoaded()
        {
            var _platformType = Type.GetType("Xamarin.Forms.Platform.iOS.Platform, Xamarin.Forms.Platform.iOS", true);
            var _rendererProperty = (BindableProperty)_platformType.GetField("RendererProperty", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static).GetValue(null);
            var l_Renderer = View.GetValue(_rendererProperty as BindableProperty) as Xamarin.Forms.Platform.iOS.IVisualElementRenderer;

            _NativeView = l_Renderer.NativeView;
            _NativeView.AddGestureRecognizer(new Test(TouchesBegan, TouchesMoved, TouchesEnded, TouchesCancel));
        }

        double _StartX;

        double _StartY;

        double _LastX;

        double _LastY;

        void TouchesBegan(Foundation.NSSet touches, UIEvent evt)
        {
            Debug.WriteLine("TOUCHES BEGIN");

            var t = touches.AnyObject as UITouch;
            var point = t.LocationInView(_NativeView);
            _StartX = point.X;
            _StartY = point.Y;
            _LastX = _StartX;
            _LastY = _StartY;
            NotifyManipulationStart();
        }

        void TouchesMoved(Foundation.NSSet touches, UIEvent evt)
        {
            Debug.WriteLine("TOUCHES MOVED");

            var t = touches.AnyObject as UITouch;
            var point = t.LocationInView(_NativeView);
            var dx = point.X - _LastX;
            var dy = point.Y - _LastY;
            var tx = point.X - _StartX;
            var ty = point.Y - _StartY;
            _LastX = point.X;
            _LastY = point.Y;
            var l_Args = new GestureManipulationInfo(dx, dy, tx, ty);
            NotifyManipulationDelta(l_Args);
        }

        void TouchesEnded(Foundation.NSSet touches, UIEvent evt)
        {
            Debug.WriteLine("TOUCHES END");
            NotifyManipulationStop();
        }

        void TouchesCancel(Foundation.NSSet touches, UIEvent evt)
        {
            Debug.WriteLine("TOUCHES CANCEL");
            NotifyManipulationStop();
        }
    }

    public class Test : UIGestureRecognizer
    {
        Action<Foundation.NSSet, UIEvent> _StartCallback;
        Action<Foundation.NSSet, UIEvent> _MoveCallback;
        Action<Foundation.NSSet, UIEvent> _StopCallback;
        Action<Foundation.NSSet, UIEvent> _CancelCallback;

        public Test(Action<Foundation.NSSet, UIEvent> p_StartCallback,
            Action<Foundation.NSSet, UIEvent> p_MoveCallback,
            Action<Foundation.NSSet, UIEvent> p_StopCallback,
            Action<Foundation.NSSet, UIEvent> p_CancelCallback)
        {
            _StartCallback = p_StartCallback;
            _MoveCallback = p_MoveCallback;
            _StopCallback = p_StopCallback;
            _CancelCallback = p_CancelCallback;
        }

        public override void TouchesBegan(Foundation.NSSet touches, UIEvent evt)
        {
            _StartCallback(touches, evt);
        }

        public override void TouchesMoved(Foundation.NSSet touches, UIEvent evt)
        {
            _MoveCallback(touches, evt);
        }

        public override void TouchesEnded(Foundation.NSSet touches, UIEvent evt)
        {
            _StopCallback(touches, evt);
        }

        public override void TouchesCancelled(Foundation.NSSet touches, UIEvent evt)
        {
            _CancelCallback(touches, evt);
        }
    }
}
