﻿using CoreSDK.Controls;
using Foundation;
using MyADA.iOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ExtendedBoldFontLabel), typeof(ExtendedBoldFontLabelRenderer))]
namespace MyADA.iOS
{
	public class ExtendedBoldFontLabelRenderer : LabelRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
            base.OnElementChanged(e);
            //UILabel label = (UILabel)Control;
            //label.Font = UIFont.FromName("Verdana-Bold", 18);
		}
	}
}
