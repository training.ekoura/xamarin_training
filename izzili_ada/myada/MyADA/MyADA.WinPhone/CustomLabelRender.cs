﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using Xamarin.Forms;
using Xamarin.Forms.Platform.WinPhone;
using CoreSDK.Controls;
using MyADA;

[assembly: ExportRenderer(typeof(CustomLabel), typeof(CustomLabelRender))]
namespace MyADA
{
    /// <summary>
    /// The Custom label renderer.
    /// </summary>
    public class CustomLabelRender : LabelRenderer
    {
        /// <summary>
        /// The on element changed callback.
        /// </summary>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
              
                //Windows Phone to do later
               Control.Text =  System.Text.RegularExpressions.Regex.Replace(Control.Text.Replace("<br />", Environment.NewLine), "<[^>]*>","");
                 //new Microsoft.Phone.Controls.WebBrowser().NavigateToString(Control.Text);
               
            }
        }
    }
}
