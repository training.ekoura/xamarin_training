﻿using System;
using Xamarin.Forms.Platform.WinPhone;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using CoreSDK.Controls;
using MyADA;



[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]

namespace MyADA
{
    public class CustomEditorRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                Control.BorderThickness = new System.Windows.Thickness(1);
                Control.BorderBrush = Helper.ColorToBrush("#cccccc");
                Control.Foreground = Helper.ColorToBrush("#000000");

                var editor = e.NewElement as CustomEditor;
                Control.FontSize = editor.FontSize;
            }
        }
    }
}
