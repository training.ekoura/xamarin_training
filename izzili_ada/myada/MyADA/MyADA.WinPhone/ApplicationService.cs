using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyADA
{
    class ApplicationService : CoreSDK.IApplicationService
    {
        public void Close()
        {
            System.Windows.Application.Current.Terminate();
        }
    }
}