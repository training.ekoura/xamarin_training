﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreSDK;
using System.Net;
using Windows.Networking.Connectivity;
using System.Threading.Tasks;
using Windows.Networking.Sockets;

namespace MyADA
{
    internal class NetworkService : ObservableObject, INetworkService
    {
        public NetworkService()
        {
            //Microsoft.Phone.Net.NetworkInformation.DeviceNetworkInformation.NetworkAvailabilityChanged += (s, e) => Update();
            Update();
        }

        private bool _IsConnected;
        public bool IsConnected
        {
            get
            {
                Update();
                return GetValue<bool>(() => _IsConnected);
            }

        }

        private void NotifyIsConnectedChanged(string s, bool p_OldValue, bool p_NewValue)
        {
            if (StateChanged != null)
            {
                StateChanged(this, new EventArgs());
            }
        }

        public event EventHandler<EventArgs> StateChanged;

        private void Update()
        {
            _IsConnected = false;
            try
            {
                _IsConnected = Windows.Networking.Connectivity.NetworkInformation.GetInternetConnectionProfile().GetNetworkConnectivityLevel() ==
                     NetworkConnectivityLevel.InternetAccess;
            }
            catch (Exception)
            {

                _IsConnected = false;
            }
            //IsConnected = Microsoft.Phone.Net.NetworkInformation.DeviceNetworkInformation.IsNetworkAvailable;
        }

    }
}
