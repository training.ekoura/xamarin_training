﻿using MyADA;
using CoreSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA
{
    internal class Initializer
    {
        public static void RegisterAll()
        {
            CoreSDK.Framework.Services.Container().Register<IApplicationService, ApplicationService>(new SingletonInstanceManager());
            CoreSDK.Framework.Services.Container().Register<INetworkService, NetworkService>(new SingletonInstanceManager());
            CoreSDK.Framework.Services.Container().Register<IFileService, FileService>(new SingletonInstanceManager());
            CoreSDK.Framework.Services.Container().Register<IGestureService, GestureService>(new SingletonInstanceManager());
            CoreSDK.Framework.Services.Container().Register<ILocalizeService, LocalizeService>(new SingletonInstanceManager());
        }
    }
}
