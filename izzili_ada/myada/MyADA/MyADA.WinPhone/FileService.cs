﻿using CoreSDK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace MyADA
{
    public class FileService : IFileService
    {

        private string path;

        public FileService()
        {
            StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;
            path = local.Path;
        }


        public string GetFileWPath(string p_FileName)
        {
            return System.IO.Path.Combine(path, p_FileName);
        }


        public string[] GetFilesInPath()
        {
            return Directory.GetFiles(path);
        }

        public bool Exist(string p_FileName)
        {
            return System.IO.File.Exists(System.IO.Path.Combine(path, p_FileName));
        }

        public void Delete(string p_FileName)
        {

            System.IO.File.Delete(System.IO.Path.Combine(path, p_FileName));
        }

        public void SaveImage(MemoryStream p_Stream, string p_FileName)
        {
            using (FileStream file = new FileStream(System.IO.Path.Combine(path, p_FileName), FileMode.Create, System.IO.FileAccess.Write))
            {
                byte[] bytes = new byte[p_Stream.Length];
                p_Stream.Read(bytes, 0, (int)p_Stream.Length);
                file.Write(bytes, 0, bytes.Length);
                p_Stream.Close();
            }
        }


        public MemoryStream LoadImage(string p_FileName)
        {
            MemoryStream ms = new MemoryStream();
            using (FileStream file = new FileStream(System.IO.Path.Combine(path, p_FileName), FileMode.Open, FileAccess.Read))
            {
                byte[] bytes = new byte[file.Length];
                file.Read(bytes, 0, (int)file.Length);
                ms.Write(bytes, 0, (int)file.Length);
            }
            return ms;
        }

        public StringBuilder LoadText(string p_FileName)
        {
            var l_FileName = System.IO.Path.Combine(path, p_FileName);
            if (System.IO.File.Exists(l_FileName))
            {
                using (var l_StreamReader = new System.IO.StreamReader(l_FileName))
                //using (var l_Stream = System.IO.File.OpenText(System.IO.Path.Combine(path, p_FileName)))
                {
                    return new StringBuilder(l_StreamReader.ReadToEnd());
                }
            }
            return new StringBuilder();
        }

        public void SaveText(string p_FileName, StringBuilder p_Text)
        {

            if (!System.IO.Directory.Exists(path))
                System.IO.Directory.CreateDirectory(path);
            var l_FileName = System.IO.Path.Combine(path, p_FileName);
            using (var l_StreamWriter = new System.IO.StreamWriter(l_FileName))
            {
                l_StreamWriter.Write(p_Text.ToString());
            }
        }

        public void AppendText(string p_FileName, StringBuilder p_Text)
        {
            var l_FileName = System.IO.Path.Combine(path, p_FileName);
            using (var l_StreamWriter = new System.IO.StreamWriter(l_FileName, true))
            {
                l_StreamWriter.Write(p_Text.ToString());
            }
            //using (var l_StreamWriter = System.IO.File.AppendText(System.IO.Path.Combine(path, p_FileName)))
            //{
            //    l_StreamWriter.Write(p_Text.ToString());
            //}
        }

        public T LoadObject<T>(string p_FileName, T p_DefaultValue = default(T))
        {

            var l_SerializerService = CoreSDK.Framework.Services.SerializerService();
            if (Exist(p_FileName))
            {
                return l_SerializerService.Deserialize<T>(LoadText(p_FileName));
            }
            else
            {
                return p_DefaultValue;
            }
        }

        public void SaveObject<T>(string p_FileName, T p_Object)
        {
            var l_SerializerService = CoreSDK.Framework.Services.SerializerService();
            SaveText(p_FileName, l_SerializerService.Serialize<T>(p_Object));
        }

        public DateTime GetLastWriteTime(string p_FileName)
        {
            var l_FileName = System.IO.Path.Combine(path, p_FileName);
            return System.IO.File.GetLastWriteTime(l_FileName);
        }
    }
}
