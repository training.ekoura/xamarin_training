﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using Xamarin.Forms;
using Xamarin.Forms.Platform.WinPhone;
using CoreSDK.Controls;
using MyADA;

[assembly: ExportRenderer(typeof(Xamarin.Forms.Image), typeof(CustomImageRender))]
namespace MyADA
{
    /// <summary>
    /// The Custom label renderer.
    /// </summary>
    public class CustomImageRender : ImageRenderer
    {
        /// <summary>
        /// The on element changed callback.
        /// </summary>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Image> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                //var a = Control.GetType().ToString();
                try
                {
                    if( ((System.Windows.Media.Imaging.BitmapImage)Control.Source!=null))
                    {
                        ((System.Windows.Media.Imaging.BitmapImage)Control.Source).CreateOptions = System.Windows.Media.Imaging.BitmapCreateOptions.None;
                    }
                    
                }
                catch (System.Exception f)
                {

                    var a = f.Message;
                }
                //
            }
        }
    }
}

