﻿using CoreSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Platform.WinPhone;


namespace MyADA
{
    class GestureService : IGestureService
    {
        public void Hook(View p_Object)
        {
            new WinPhoneGestureManager(p_Object);
        }
    }

    class WinPhoneGestureManager : GestureManager
    {
        public WinPhoneGestureManager(View p_View)
            : base(p_View)
        {

        }

        protected override void OnLoaded()
        {
            var l_Renderer = View.GetRenderer();
            l_Renderer.ContainerElement.ManipulationStarted += ContainerElement_ManipulationStarted;
            l_Renderer.ContainerElement.ManipulationCompleted += ContainerElement_ManipulationCompleted;
            l_Renderer.ContainerElement.ManipulationDelta += ContainerElement_ManipulationDelta;
        }

        void ContainerElement_ManipulationDelta(object sender, System.Windows.Input.ManipulationDeltaEventArgs e)
        {
            var l_Args = new GestureManipulationInfo(e.DeltaManipulation.Translation.X, e.DeltaManipulation.Translation.Y, e.CumulativeManipulation.Translation.X, e.CumulativeManipulation.Translation.Y);
            NotifyManipulationDelta(l_Args);
        }

        void ContainerElement_ManipulationStarted(object sender, System.Windows.Input.ManipulationStartedEventArgs e)
        {
            NotifyManipulationStart();
        }

        void ContainerElement_ManipulationCompleted(object sender, System.Windows.Input.ManipulationCompletedEventArgs e)
        {
            NotifyManipulationStop();
        }
    }
}
