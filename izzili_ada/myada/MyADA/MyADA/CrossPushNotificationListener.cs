﻿using PushNotification.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using CoreSDK;

namespace MyADA
{
    public class CrossPushNotificationListener : IPushNotificationListener
    {
        public void OnError(string message, PushNotification.Plugin.Abstractions.DeviceType deviceType)
        {

        }

        public void OnMessage(Newtonsoft.Json.Linq.JObject values, PushNotification.Plugin.Abstractions.DeviceType deviceType)
        {

        }

        public void OnRegistered(string token, PushNotification.Plugin.Abstractions.DeviceType deviceType)
        {
            //MyADA.Common.Views.IMobileBridge iMobileBridge = DependencyService.Get<MyADA.Common.Views.IMobileBridge>();
            //iMobileBridge.MB_SetToken(token);
            CoreSDK.Framework.Services.InteractionService().DeviceToken = token;
            CoreSDK.Framework.Services.InteractionService().DevicePlatform = Device.OS.ToString().ToLower();
        }

        public void OnUnregistered(PushNotification.Plugin.Abstractions.DeviceType deviceType)
        {
        }

        public bool ShouldShowNotification()
        {
            return true;
        }
    }
}
