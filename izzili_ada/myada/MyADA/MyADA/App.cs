﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreSDK;
using Xamarin.Forms;
using MyADA.Common.Views;
using MyADA.Common.ViewModels;
using MyADA.Common.Views.Interfaces;
using PushNotification.Plugin;
using System.Threading.Tasks;
using MyADA.Common.DAOServices.Services;
using MyADA.Common.Services.Services;
using MyADA.Common.Services.Requests;

namespace MyADA.Common
{
    public class App : Application
    {
        // Properties
        static string Theme;
        static string Version = "1.0";
        static string VersionForWs = "1.0"; // format x.y.z

        public App(string p_Theme)
        {
            Theme = p_Theme;

            #region ==================================== Mobile bridge initialisation ====================================
            //IMobileBridge iMobileBridge = DependencyService.Get<IMobileBridge>();
            //iMobileBridge.MB_InitLibrary("hkYBL7uNFjhmm6sxWd50F5wHw6tzrjwKTAIAcoUm", "f5foFIQjBokGi5IyPMdO2ilPJ9NjdlWjULW8MKyI");
            #endregion
        }

        protected override async void OnStart()
        {
            // Ini
            CoreSDK.Framework.Initialize();
            CoreSDK.FrameworkXamarinForms.Initialize();

            #region splashScreen
            //  if (Device.OS == TargetPlatform.Android)
            //  {
            var l_Grid = new Grid()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                //Padding = new Thickness(0,10,0,0)
            };

            l_Grid.SizeChanged += l_Grid_SizeChanged;

            var l_SplashImageName = Device.OnPlatform("SplashScreen.jpg", "SplashScreen.jpg", "Assets/SplashScreen.jpg");

            var l_Photo = new Image()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Aspect = Xamarin.Forms.Aspect.AspectFill,
                Source = ImageSource.FromFile(l_SplashImageName)
            };

            var l_Version = new Label()
            {
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.End,
                Text = "Version " + Version,
                TranslationY = -10
            };

            l_Grid.Children.Add(l_Photo);
            l_Grid.Children.Add(l_Version);

            var l_Page = new ContentPage()
            {
                Content = l_Grid
            };

            NavigationPage.SetHasBackButton(l_Page, false);
            NavigationPage.SetHasNavigationBar(l_Page, false);

            CoreSDK.Framework.Services.InteractionService().Set(l_Page);
            // }
            #endregion

            // Initialisation des DAOServices
            MyADA.Common.DAOServices.Initializer.RegisterAll();

            // Initilisation des views
            MyADA.Common.Views.Initializer.RegisterAll();

            // Initialisation des ViewModels
            MyADA.Common.ViewModels.Initializer.RegisterAll();

            var GlobalConfigService = CoreSDK.Framework.Services.GlobalConfigService();

            #region ============================= Gestion du thème =============================
            // Gestion du Thême
            //var GlobalConfigService = CoreSDK.Framework.Services.;
            //GlobalConfigService.ThemeName = Theme;
            GlobalConfigService.Data.DeviceOs = Device.OS == TargetPlatform.Android ? "Android" : (Device.OS == TargetPlatform.iOS ? "iOS" : "WindowsPhone");
            GlobalConfigService.Data.AppVersion = VersionForWs;
            CheckBookingIsValide();
            


            var l_ThemeService = CoreSDK.Framework.Services.ThemeService() as CoreSDK.Themes.IThemeServiceXamarin;
            CoreSDK.Framework.Services.InteractionService().ThemeNameValue = MyADA.Common.ViewModels.Helper.ToolsBox.ThemeNameToImage(Theme);

            // On enregistre tous les thèmes possible dans l'application.
            l_ThemeService.Register(ThemeName.MyADA, Themes.ThemeAudi.ThemeCallback);
            l_ThemeService.Apply(ThemeName.MyADA);

            #endregion

            #region culture(langage)

            //////////////////////////////////////////////////////////////////
            // On ne gère pas de langues différentes pour cette application //
            //////////////////////////////////////////////////////////////////


            //var cultureDevice = CoreSDK.Framework.Services.Container().Resolve<ILocalizeService>().GetCurrentCultureInfo();
            //if (cultureDevice.Name.Contains("en"))
            //{

            //    Views.Properties.Resources.Culture = new System.Globalization.CultureInfo("en-US");
            //    ViewModels.Properties.Resources.Culture = new System.Globalization.CultureInfo("en-US");
            //}
            //else
            //{
            //    Views.Properties.Resources.Culture = new System.Globalization.CultureInfo("fr-FR");
            //    ViewModels.Properties.Resources.Culture = new System.Globalization.CultureInfo("fr-FR");
            //}

            if (GlobalConfigService.Data.CultureInfo != null)
            {
                Views.Properties.Resources.Culture = new System.Globalization.CultureInfo(GlobalConfigService.Data.CultureInfo);
                ViewModels.Properties.Resources.Culture = new System.Globalization.CultureInfo(GlobalConfigService.Data.CultureInfo);
            }

            #endregion



            if (GlobalConfigService.Data != null && GlobalConfigService.Data.Token != null && GlobalConfigService.Data.Secret != null)
            {
                // Prepare user data.
                var l_HomeData = CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>();
                await l_HomeData.PrepareVM();

                // If user already login on time on the project, we redirect himself directly to the homepage.
                var l_ChangeStatusBarColor = false;
                CoreSDK.Framework.Services.InteractionService().Set<IMasterDetailView>(null, l_ChangeStatusBarColor);
            }
            else
            {
                // Actuellement le SplashScreen est affiché. L'opération suivante est rapide et aboutie à un Set de ConnectionPage.
                // Comme c'est rapide, histoire de laisser une peu le SplashScreen, on diffère (on peut car il y a déjà une UI d'afficher donc cela ne va pas planter).
                System.Threading.Tasks.Task.Delay(2000).ContinueWith((t) =>
                {
                    var l_ChangeStatusBarColor = false;

                    // On applique la couleur du statusBar sur IOS
                    CoreSDK.Framework.Services.InteractionService().Set<ILoginView>(null, l_ChangeStatusBarColor);
                }, System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());
            }
            

            #region ==================================== Notification initialisation ====================================
            //TODO-GAV :  Remove Task Delay
            await Task.Delay(3000);
            if(GlobalConfigService.Data.DeviceToken == null && GlobalConfigService.Data.DevicePlatform == null)
                CrossPushNotification.Current.Register();
            CoreSDK.Framework.Services.InteractionService().DeviceIMEI = DependencyService.Get<IDeviceManager>().GetDeviceIMEI(); 
            #endregion
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            var GlobalConfigService = CoreSDK.Framework.Services.GlobalConfigService();
            GlobalConfigService.Data.DateWhenOnSleep = DateTime.Now;
        }

        protected override async void OnResume()
        {
            // Handle when your app resumes
            var GlobalConfigService = CoreSDK.Framework.Services.GlobalConfigService();

            /*if (GlobalConfigService.Data != null && GlobalConfigService.Data.OldToken != null)
            {
                if ((DateTime.Now - GlobalConfigService.Data.DateWhenOnSleep).TotalMinutes >= 30 ? true : false)
                {

                    #region splashScreen
                    var l_Grid = new Grid()
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        Padding = new Thickness(0)
                    };

                    l_Grid.SizeChanged += l_Grid_SizeChanged;

                    var l_SplashImageName = Device.OnPlatform("SplashScreen.jpg", "SplashScreen.jpg", "Assets/SplashScreen.jpg");

                    var l_Photo = new Image()
                    {
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        Aspect = Xamarin.Forms.Aspect.AspectFill,
                        Source = ImageSource.FromFile(l_SplashImageName)
                    };

                    var l_Version = new Label()
                    {
                        TextColor = Color.Black,
                        HorizontalOptions = LayoutOptions.Center,
                        VerticalOptions = LayoutOptions.End,
                        Text = "Version " + Version,
                        TranslationY = -10
                    };

                    l_Grid.Children.Add(l_Photo);
                    l_Grid.Children.Add(l_Version);

                    var l_Page = new ContentPage()
                    {
                        Content = l_Grid
                    };

                    NavigationPage.SetHasBackButton(l_Page, false);
                    NavigationPage.SetHasNavigationBar(l_Page, false);

                    CoreSDK.Framework.Services.InteractionService().Set(l_Page);

                    #endregion

                    await CoreSDK.Framework.Services.Container().Resolve<MyADA.Common.ViewModels.IContextViewModel>().AppStarter();
                }
                else
                {
                    // Mise à jour de la DashboardViewModel
                    var dashboardVM = CoreSDK.Framework.Services.Container().Resolve<MyADA.Common.ViewModels.IDashboardViewModel>();
                    if (!string.IsNullOrEmpty(dashboardVM.DateDashBoard))
                    {
                        var resultDashboardVMRefresh = await dashboardVM.refreshOnlyDashborad();
                    }
                };
            }
            else
            {
                System.Threading.Tasks.Task.Delay(2000).ContinueWith((t) =>
                {
                    CoreSDK.Framework.Services.InteractionService().Set(ViewName.ConnexionView);
                }, System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());
            }*/

        }

        #region ============================= Useful fonctions =============================
        void l_Grid_SizeChanged(object sender, EventArgs e)
        {
            CoreSDK.Framework.Services.InteractionService().DeviceHeight = MainPage.Height;
            CoreSDK.Framework.Services.InteractionService().DeviceWidth = MainPage.Width;
        }

        void CheckBookingIsValide()
        {
            
        }
        #endregion
    }
}
