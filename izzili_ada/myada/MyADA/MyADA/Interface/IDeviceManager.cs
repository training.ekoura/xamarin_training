﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA
{
    public interface IDeviceManager
    {
        /// <summary>
        /// Get Mobile unique code
        /// </summary>
        /// <returns></returns>
        string GetDeviceIMEI();
    }
}
