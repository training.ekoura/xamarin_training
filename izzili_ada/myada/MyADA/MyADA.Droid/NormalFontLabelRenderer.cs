using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CoreSDK.Controls;
using MyADA.Droid;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(NormalFontLabel), typeof(NormalFontLabelRenderer))]
namespace MyADA.Droid
{
    public class NormalFontLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            var label = (TextView)Control; // for example
            Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "AudiTypeV02-ExtendedNormal.otf");  // font name specified here
            label.Typeface = font;
        }
    }
}