using System;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.Widget;
using Android.Views;
using CoreSDK.Controls;
using Android.Views.InputMethods;
using MyADA;
using Android.Text;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]

namespace MyADA
{
    public class CustomEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            CustomEntry base_entry = (CustomEntry)this.Element;

            base.OnElementChanged(e);

            if (Control != null && base_entry != null && e.NewElement != null)
            {
                SetReturnType(base_entry);

                var nativeEditText = Control;
                //nativeEditText.SetFilters(new IInputFilter[] { new InputFilterLengthFilter(base_entry.MaxLength) });
                var shape = new Android.Graphics.Drawables.ShapeDrawable(new Android.Graphics.Drawables.Shapes.RectShape());
                shape.Paint.Color = Color.FromHex("#cccccc").ToAndroid();
                //used to hide border for StockEntry in ModifierStockContentView
                if (base_entry.StyleId == "StockEntry" || base_entry.StyleId == "TransparentBorderEntry")
                {
                    shape.Paint.Color = Color.Transparent.ToAndroid();

                    if (base_entry.StyleId == "TransparentBorderEntry")
                    {
                        base_entry.SetDynamicResource(CustomEntry.PlaceholderColorProperty, "BoxItemTextColor");
                    }
                    else
                    {
                        base_entry.SetDynamicResource(CustomEntry.PlaceholderColorProperty, "EntryPlaceholderColor");
                    }
                }
                shape.Paint.StrokeWidth = 5;
                shape.Paint.SetStyle(Android.Graphics.Paint.Style.Stroke);

                nativeEditText.SetBackgroundDrawable(shape);

                // Editor Action is called when the return button is pressed
                Control.EditorAction += (object sender, TextView.EditorActionEventArgs args) =>
                {
                    if (base_entry.ReturnType != ReturnType.Next)
                        base_entry.Unfocus();

                    // Call all the methods attached to base_entry event handler Completed
                    base_entry.InvokeCompleted();
                };
            }
        }

        private void SetReturnType(CustomEntry entry)
        {
            ReturnType type = entry.ReturnType;

            switch (type)
            {
                case ReturnType.Go:
                    Control.ImeOptions = ImeAction.Go;
                    Control.SetImeActionLabel("Go", ImeAction.Go);
                    break;
                case ReturnType.Next:
                    Control.ImeOptions = ImeAction.Next;
                    Control.SetImeActionLabel("Next", ImeAction.Next);
                    break;
                case ReturnType.Send:
                    Control.ImeOptions = ImeAction.Send;
                    Control.SetImeActionLabel("Send", ImeAction.Send);
                    break;
                case ReturnType.Search:
                    Control.ImeOptions = ImeAction.Search;
                    Control.SetImeActionLabel("Search", ImeAction.Search);
                    break;
                default:
                    Control.ImeOptions = ImeAction.Done;
                    Control.SetImeActionLabel("Done", ImeAction.Done);
                    break;
            }
        }
    }
}