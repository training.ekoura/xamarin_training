using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using PushNotification.Plugin;
using Android.Telephony;


[assembly: Xamarin.Forms.Dependency(typeof(MyADA.Droid.DeviceManager))]

namespace MyADA.Droid
{
    public class DeviceManager : IDeviceManager
    {
        public string GetDeviceIMEI()
        {
            /*var telephonyManager = (TelephonyManager)Application.Context.GetSystemService(Application.TelephonyService);
            return telephonyManager.DeviceId;*/
            return Android.OS.Build.Serial;
        }
    }
}