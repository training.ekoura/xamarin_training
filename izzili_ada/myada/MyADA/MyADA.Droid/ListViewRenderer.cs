using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using System;
using MyADA;

[assembly: ExportRenderer (typeof (ListView), typeof (ScrollListViewRenderer))]

namespace MyADA
{
    class ScrollListViewRenderer : ListViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);

            Control.Focusable = false;
        }
    }
}
