using System;
using Xamarin.Forms.Platform.Android;
using Android.Widget;
using Xamarin.Forms;
using CoreSDK.Controls;
using MyADA;


[assembly: ExportRenderer(typeof(CustomLabel), typeof(CustomLabelRenderer))]
namespace MyADA
{
    public class CustomLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Label> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.TextFormatted = new Android.Widget.TextView(this.Context)
                {
                    TextFormatted = Android.Text.Html.FromHtml(Control.Text)
                }.TextFormatted;
            }
        }
    }
}