﻿using Android.Content.Res;
using Android.Views;
using CoreSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Platform;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CoreSDK.Controls.TouchContentView), typeof(MyADA.TouchContentViewRenderer))]

namespace MyADA
{
    class GestureService : IGestureService
    {
        public void Hook(Xamarin.Forms.View p_Object)
        {
            new AndroidGestureManager(p_Object);
        }
    }

    class AndroidGestureManager : GestureManager
    {
        static Type _platformType;
        static BindableProperty _rendererProperty;

        static AndroidGestureManager()
        {
            _platformType = Type.GetType("Xamarin.Forms.Platform.Android.Platform, Xamarin.Forms.Platform.Android", true);
            _rendererProperty = (BindableProperty)_platformType.GetField("RendererProperty", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
        }

        public AndroidGestureManager(Xamarin.Forms.View p_View)
            : base(p_View)
        {
            //if (Application.Current.MainPage != null)
            //{
            //    try
            //    {
            //        _platformType = Type.GetType("Xamarin.Forms.Platform.Android.Platform, Xamarin.Forms.Platform.Android", true);
            //        _rendererProperty = (BindableProperty)_platformType.GetField("RendererProperty", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
            //        var l_Renderer = Application.Current.MainPage.GetValue(_rendererProperty as BindableProperty) as IVisualElementRenderer;
            //    }
            //    catch (Exception l_Exception)
            //    {
            //        System.Diagnostics.Debug.WriteLine("===> AndroidGestureManager Exception : {0}", l_Exception);
            //    }
            //}
            //else
            //{
            //    System.Diagnostics.Debug.WriteLine("===> AndroidGestureManager. The MainPage is not set.");
            //}
        }

        protected override void OnLoaded()
        {
            var l_Renderer = View.GetValue(_rendererProperty as BindableProperty) as IVisualElementRenderer;

            if (l_Renderer is TouchContentViewRenderer)
            {
                (l_Renderer as TouchContentViewRenderer).SetManager(this);
            }

            var l_UI = l_Renderer.ViewGroup;
            var l_View = l_UI.RootView;
            l_UI.Background = new Android.Graphics.Drawables.ColorDrawable(Android.Graphics.Color.Transparent);
            l_UI.Touch += l_UI_Touch;
        }

        double _StartX;

        double _StartY;

        double _LastX;

        double _LastY;

        //void l_UI_Touch(object sender, Android.Views.View.TouchEventArgs e)
        //{
        //    System.Diagnostics.Debug.WriteLine("Touch {0}", e.Event.Action);

        //    if (e.Event.Action == MotionEventActions.Down)
        //    {
        //        var l_X = e.Event.GetX() / Resources.System.DisplayMetrics.Density;
        //        var l_Y = e.Event.GetY() / Resources.System.DisplayMetrics.Density;
        //        _StartX = l_X;
        //        _StartY = l_Y;
        //        _LastX = _StartX;
        //        _LastY = _StartY;
        //        NotifyManipulationStart();
        //        e.Handled = true;
        //    }
        //    else if (e.Event.Action == MotionEventActions.Move)
        //    {
        //        var l_X = e.Event.GetX() / Resources.System.DisplayMetrics.Density;
        //        var l_Y = e.Event.GetY() / Resources.System.DisplayMetrics.Density;
        //        var l_Args = new GestureManipulationInfo(l_X - _LastX, l_Y - _LastY, l_X - _StartX, l_Y - _StartY);
        //        _LastX = l_X;
        //        _LastY = l_Y;
        //        NotifyManipulationDelta(l_Args);
        //        e.Handled = true;
        //    }
        //    else if (e.Event.Action == MotionEventActions.Up)
        //    {
        //        NotifyManipulationStop();
        //        e.Handled = true;
        //    }
        //    else
        //    {
        //        e.Handled = false;
        //    }
        //}

        void l_UI_Touch(object sender, Android.Views.View.TouchEventArgs e)
        {
            var l_X = e.Event.GetX();
            var l_Y = e.Event.GetY();
            DoTouch(e.Event.Action, l_X, l_Y);
        }

        public void DoTouch(MotionEventActions p_Action, double X, double Y)
        {
            System.Diagnostics.Debug.WriteLine("Touch {0}", p_Action);

            if (p_Action == MotionEventActions.Down)
            {
                var l_X = X / Resources.System.DisplayMetrics.Density;
                var l_Y = Y / Resources.System.DisplayMetrics.Density;
                _StartX = l_X;
                _StartY = l_Y;
                _LastX = _StartX;
                _LastY = _StartY;
                NotifyManipulationStart();
            }
            else if (p_Action == MotionEventActions.Move)
            {
                var l_X = X / Resources.System.DisplayMetrics.Density;
                var l_Y = Y / Resources.System.DisplayMetrics.Density;
                var l_Args = new GestureManipulationInfo(l_X - _LastX, l_Y - _LastY, l_X - _StartX, l_Y - _StartY);
                _LastX = l_X;
                _LastY = l_Y;
                NotifyManipulationDelta(l_Args);
            }
            else if (p_Action == MotionEventActions.Up)
            {
                NotifyManipulationStop();
            }
            else
            {

            }
        }
    }

    public class TouchContentViewRenderer : ViewRenderer<CoreSDK.Controls.TouchContentView, Android.Widget.GridView>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<CoreSDK.Controls.TouchContentView> e)
        {
            base.OnElementChanged(e);
        }

        public override bool OnInterceptTouchEvent(Android.Views.MotionEvent ev)
        {
            _Manager.DoTouch(ev.Action, ev.GetX(), ev.GetY());
            return false;
        }

        private AndroidGestureManager _Manager;

        internal void SetManager(AndroidGestureManager p_Manager)
        {
            _Manager = p_Manager;
        }
    }
}
