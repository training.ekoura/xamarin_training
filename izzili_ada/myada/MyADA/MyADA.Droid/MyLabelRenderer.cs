using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using CoreSDK.Controls;
using Xamarin.Forms;
using Android.Graphics;
using MyADA.Droid;

[assembly: ExportRenderer(typeof(MyLabel), typeof(MyLabelRenderer))]
namespace MyADA.Droid
{
    public class MyLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            var label = (TextView)Control; // for example
            Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "AudiTypeV02-ExtendedNormal.otf");  // font name specified here
            label.Typeface = font;
        }
    }
}