//using Xamarin.Forms.Platform.Android;
//using Xamarin.Forms;
//using MySodexo;
//using System;
//using CoreSDK.Controls;
//using Android.Graphics.Drawables;
//using System.ComponentModel;

//[assembly: ExportRenderer(typeof(RoundedButton), typeof(RoundedButtonRenderer))]

//namespace MySodexo
//{
//    public class RoundedButtonRenderer : ButtonRenderer
//    {
//        private GradientDrawable _normal, _pressed;

//        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
//        {
//            base.OnElementChanged(e);

//            if (Control != null)
//            {
//                var button = (RoundedButton)e.NewElement;

                
//                button.SizeChanged += (s, args) =>
//                {
//                    var radius = (float)Math.Min(button.Width, button.Height);

//                    // Create a drawable for the button's normal state
//                    _normal = new Android.Graphics.Drawables.GradientDrawable();

//                    if (button.BackgroundColor.R == -1.0 && button.BackgroundColor.G == -1.0 && button.BackgroundColor.B == -1.0)
//                        _normal.SetColor(Android.Graphics.Color.ParseColor("#ff2c2e2f"));
//                    else
//                        _normal.SetColor(button.BackgroundColor.ToAndroid());

//                    _normal.SetCornerRadius(radius);

//                    // Create a drawable for the button's pressed state
//                    _pressed = new Android.Graphics.Drawables.GradientDrawable();
//                    var highlight = Context.ObtainStyledAttributes(new int[] { Android.Resource.Attribute.ColorActivatedHighlight }).GetColor(0, Android.Graphics.Color.Gray);
//                    _pressed.SetColor(highlight);
//                    _pressed.SetCornerRadius(radius);

//                    // Add the drawables to a state list and assign the state list to the button
//                    var sld = new StateListDrawable();
//                    sld.AddState(new int[] { Android.Resource.Attribute.StatePressed }, _pressed);
//                    sld.AddState(new int[] { }, _normal);
//                    Control.SetBackgroundDrawable(sld);
//                };

//                button.BorderWidth = 1;
//                button.BorderColor = Color.Red;
//            }
//        }

//        //protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
//        //{
//        //    base.OnElementPropertyChanged(sender, e);
//        //    var button = (Xamarin.Forms.Button)sender;

//        //    if (_normal != null && _pressed != null)
//        //    {
//        //        if (e.PropertyName == "BorderRadius")
//        //        {
//        //            _normal.SetCornerRadius(button.BorderRadius);
//        //            _pressed.SetCornerRadius(button.BorderRadius);
//        //        }
//        //        if (e.PropertyName == "BorderWidth" || e.PropertyName == "BorderColor")
//        //        {
//        //            _normal.SetStroke((int)button.BorderWidth, button.BorderColor.ToAndroid(Android.Graphics.Color.Teal));
//        //            //_normal.SetStroke((int)button.BorderColor, button.BorderColor.ToAndroid());
//        //            _pressed.SetStroke((int)button.BorderWidth, button.BorderColor.ToAndroid());
//        //        }
//        //    }
//        //}
//    }
//}
