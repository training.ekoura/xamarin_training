using System;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using MyADA;
using Android.Widget;
using Android.Views;


[assembly: ExportRenderer(typeof(SearchBar), typeof(CustomSearchBarRenderer))]

namespace MyADA
{
    public class CustomSearchBarRenderer : SearchBarRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<SearchBar> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                AutoCompleteTextView objAutoTextView = (AutoCompleteTextView)(((this.Control.GetChildAt(0) as ViewGroup).GetChildAt(2) as ViewGroup).GetChildAt(1) as ViewGroup).GetChildAt(0);
                objAutoTextView.SetTextColor(Android.Graphics.Color.Black);

                var nativeEditText = Control;

                var shape = new Android.Graphics.Drawables.ShapeDrawable(new Android.Graphics.Drawables.Shapes.RectShape());
                shape.Paint.Color = Color.FromHex("#cccccc").ToAndroid();
                shape.Paint.StrokeWidth = 5;
                shape.Paint.SetStyle(Android.Graphics.Paint.Style.Stroke);

                nativeEditText.SetBackgroundDrawable(shape);
            }
        }

    }
}