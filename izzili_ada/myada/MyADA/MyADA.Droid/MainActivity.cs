﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using PushNotification.Plugin;
using Android.Telephony;
using CoreSDK;

namespace MyADA.Droid
{
    [Activity(Label = "My ADA", Icon = "@drawable/icon", MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait, LaunchMode = LaunchMode.SingleTask)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        internal static Context context; 
        protected override void OnCreate(Bundle bundle)
        {
            // TODO-GAV :  Décommenter ces deux lignes
            //TabLayoutResource = Resource.Layout.Tabbar;
            //ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            global::MyADA.Initializer.RegisterAll();
            // Desable Navigation bar on Android
            global::Xamarin.Forms.Forms.SetTitleBarVisibility(Xamarin.Forms.AndroidTitleBarVisibility.Never);

            // We don't nedd this part.
            if ((int)Android.OS.Build.VERSION.SdkInt >= (int)Android.OS.BuildVersionCodes.Lollipop)
            {
                Window w = Window;
                w.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
                //Window.SetStatusBarColor(Android.Graphics.Color.Rgb(193, 4, 24));
            }

            context = this;

            global::Xamarin.Forms.Forms.Init(this, bundle);

            // Initialize CrossPlateform notification
            CrossPushNotification.Initialize<CrossPushNotificationListener>(Keys.GoogleProjectNumber);

            // Load application
            LoadApplication(new MyADA.Common.App(""));
            
        }
    }
}

