using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using CoreSDK.Controls;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;

[assembly: ExportRenderer(typeof(ExtendedBoldFontLabel), typeof(MyADA.Droid.ExtendedBoldFontLabelRenderer))]
namespace MyADA.Droid
{
    public class ExtendedBoldFontLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            var label = (TextView)Control; // for example
            Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "AudiTypeV02-ExtendedBold.otf");  // font name specified here
            label.Typeface = font;
        }
    }
}