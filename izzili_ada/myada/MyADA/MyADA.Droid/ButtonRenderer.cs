using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CoreSDK.Controls;
using MyADA.Droid;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ListButton), typeof(MyADA.Droid.ListButtonRenderer))]
namespace MyADA.Droid
{
	public class ListButtonRenderer : ButtonRenderer
	{
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
		{
			base.OnElementChanged (e);

			Control.Focusable = false;

            var button = (Android.Widget.Button)Control;
            Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "AudiTypeV02-ExtendedNormal.otf");  // font name specified here
            button.Typeface = font;
        }
	}
}