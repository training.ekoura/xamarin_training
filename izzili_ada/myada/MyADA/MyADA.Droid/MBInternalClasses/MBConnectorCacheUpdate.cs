﻿using System;
using Com.Mobilebridge.Library;
using Org.Json;
using Android.Content;
using MyADA.Common;
using MyADA.Common.Views;

namespace MyADA.Droid
{
	public class MBConnectorCacheUpdate
	{
		MobileBridgeDelegate mbDelegate;
		public static MBXamarinFormsTrainingAppPage.CacheUpdateDelegate cacheDelegate;

		public void MB_UpdateCacheCustomerAtribute(Context context, MBXamarinFormsTrainingAppPage.CacheUpdateDelegate _delegate)
		{
			cacheDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_UpdateCachedAssets(context, MobileBridgeLibrary.UpdateCacheType.UpdateCacheCustomerAttribute, mbDelegate);
			}
		}



		class MobileBridgeDelegate : Java.Lang.Object, MobileBridgeLibrary.IMB_UpdateCachingDataInterface
		{
			public void MB_UpdateCachingDataFinished()
			{
				cacheDelegate.MB_UpdateCacheFinished();
			}
		}

	}
}

