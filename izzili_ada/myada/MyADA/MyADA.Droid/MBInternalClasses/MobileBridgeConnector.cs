﻿using System;
using Com.Mobilebridge.Library;
using Android.Widget;
using Xamarin.Forms;
using System.Collections.Generic;
using MyADA.Common;
using MyADA.Common.Views;

[assembly: Xamarin.Forms.Dependency(typeof(MyADA.Droid.MobileBridgeConnector))]

namespace MyADA.Droid
{
	public class MobileBridgeConnector : IMobileBridge
	{
		/*
			* Init Library with key/secret you obtained during registration with MobileBridge
			*/
		public void MB_InitLibrary(string clientKey, string clientSecret)
		{
			MBConnectorInitLibrary mbConnectorInitLibrary = new MBConnectorInitLibrary();
			mbConnectorInitLibrary.MB_InitLibrary(clientKey, clientSecret);

		}


		////////////////////////////// Data //////////////////////////////

		public void MB_CustomerAttributeSetInt(int value, string key)
		{
			if (MBConnectorInitLibrary.attributeObject != null)
			{
				MBConnectorInitLibrary.attributeObject.SetInt(value, key);
			}
		}

		public void MB_CustomerAttributeSetTime(long value, string key)
		{
			if (MBConnectorInitLibrary.attributeObject != null)
			{
				MBConnectorInitLibrary.attributeObject.SetTime(value, key);
			}
		}

		public void MB_CustomerAttributeSetDate(long value, string key)
		{
			if (MBConnectorInitLibrary.attributeObject != null)
			{
				MBConnectorInitLibrary.attributeObject.SetDate(value, key);
			}
		}

		public void MB_CustomerAttributeSetText(string value, string key)
		{
			if (MBConnectorInitLibrary.attributeObject != null)
			{
				MBConnectorInitLibrary.attributeObject.SetString(value, key);
			}
		}

		public void MB_CustomerAttributeSetMultiSelect(List<string> value, string key)
		{
			if (MBConnectorInitLibrary.attributeObject != null)
			{
/*				
				var array = new NSMutableArray();
				foreach (var item in value)
				{
					var str = new NSString(item);
					array.Add(str);
				}
				MBConnectorInitLibrary.attributeObject.SetMultiSelect(array, key);
*/				
			}
		}

		public void MB_CustomerAttributeSetSingleSelect(string value, string key)
		{
			if (MBConnectorInitLibrary.attributeObject != null)
			{
				MBConnectorInitLibrary.attributeObject.SetSingleSelect(value, key);
			}
		}

		public void MB_CustomerAttributeSetBool(Boolean value, string key)
		{
			if (MBConnectorInitLibrary.attributeObject != null)
			{
				MBConnectorInitLibrary.attributeObject.SetBool(value, key);
			}
		}

		public void MB_CustomerAttributeSetList(List<string> value, string key)
		{
			if (MBConnectorInitLibrary.attributeObject != null)
			{
//				MBConnectorInitLibrary.attributeObject.SetList(value, key);
			}
		}

		public void MB_GetCustomerAttribute(MBXamarinFormsTrainingAppPage.CustomerAttributeDelegate _delegate)
		{
			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorAttributeObjectData mbConnectorCustomAttribute = new MBConnectorAttributeObjectData();
				mbConnectorCustomAttribute.MB_GetCustomerAttribute(_delegate);
			}
		}

		public void MB_SetCustomerAttribute(MBXamarinFormsTrainingAppPage.CustomerAttributeDelegate _delegate)
		{
			if (MBConnectorInitLibrary.attributeObject != null)
			{
				if (MBConnectorInitLibrary.mbLib != null)
				{
					MBConnectorAttributeObjectData mbConnectorCustomAttribute = new MBConnectorAttributeObjectData();
					mbConnectorCustomAttribute.MB_SetCustomerAttribute(_delegate);
				}
			}
		}

		public void MB_ClearCustomerAttribute(MBXamarinFormsTrainingAppPage.CustomerAttributeDelegate _delegate)
		{
			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorAttributeObjectData mbConnectorCustomAttribute = new MBConnectorAttributeObjectData();
				mbConnectorCustomAttribute.MB_ClearCustomerAttribute(_delegate);
			}
		}


		public void MB_GetInboxData(MBXamarinFormsTrainingAppPage.InboxDataFinished _delegate)
		{
			MBConnectorInboxData mbConnectorInboxData = new MBConnectorInboxData();
			mbConnectorInboxData.MB_GetInboxData(_delegate);
		}

		public void MB_DeleteInboxData(MBXamarinFormsTrainingAppPage.InboxDataFinished _delegate)
		{
			MBConnectorInboxData mbConnectorInboxData = new MBConnectorInboxData();
			mbConnectorInboxData.MB_DeleteInboxData(_delegate);
		}

		public void MB_DeleteInboxItem(string inbox_id, MBXamarinFormsTrainingAppPage.InboxDataFinished _delegate)
		{
			MBConnectorInboxData mbConnectorInboxData = new MBConnectorInboxData();
			mbConnectorInboxData.MB_DeleteInboxItem(inbox_id, _delegate);
		}

		public string MB_GetLocationsData(string tagFilter)
		{
			return null;
		}

		public void MB_GetPublicPromotionsData(MBXamarinFormsTrainingAppPage.MyDelegate _delegate)
		{
			MBConnectorPromotionsData mbConnectorPromotionData = new MBConnectorPromotionsData();
			mbConnectorPromotionData.MB_GetPublicPromotionsData(_delegate);
		}


		public string MB_GetSavedPromotionsData(int type)
		{
			return null;
		}

		/**
 		* Requests rich message data
 		*
 		* @param 
 		*/
		public void MB_GetRichMessageData(string assetInfo, MBXamarinFormsTrainingAppPage.RichMessageDelegate _delegate)
		{
			MBConnectorRichMessageData mbConnectorRMData = new MBConnectorRichMessageData();
			mbConnectorRMData.MB_GetRichMessageData(assetInfo, _delegate);
		}

		public void MB_GetAllRichMessages(MBXamarinFormsTrainingAppPage.RichMessageDelegate _delegate)
		{
			MBConnectorRichMessageData mbConnectorRMData = new MBConnectorRichMessageData();
			mbConnectorRMData.MB_GetAllRichMessages(_delegate);
		}

		public void MB_UpdateCacheCustomerAtribute(MBXamarinFormsTrainingAppPage.CacheUpdateDelegate _delegate)
		{
			MBConnectorCacheUpdate mbConnectorCacheUpdate = new MBConnectorCacheUpdate();
			mbConnectorCacheUpdate.MB_UpdateCacheCustomerAtribute(MBConnectorInitLibrary.context, _delegate);
		}

		////////////////////////////// Loyalty  //////////////////////////////

		/**
		 * Requests points
		 */
		public string MB_GetPoints()
		{
			return null;
		}

		/**
		 * Sets loyalty points
		 *
		 * @param points: amount of loyalty points to set
		 */
		public string MB_SetPoints(string points)
		{
			return null;
		}

		/**
		 * Requests loyalty points history
		 */
		public string MB_GetPointsHistory()
		{
			return null;
		}

		/**
		 * Triggers custom point event
		 *
		 * @param customEvent: event name
		 */
		public bool MB_CustomPointEvent(string customEvent)
		{
			return false;
		}

		/**
		 * Gets location notifications status (used for geo-targeting)
		 */

		////////////////////////////// Settings / Utilities //////////////////////////////

		public bool MB_GetLocationReminderStatus()
		{
			return true;
		}

		/**
		 * Enables or disables location notifications (used for geo-targeting)
		 *
		 * @param enabled Notifications triggered:
		 *                com.mobilebridge.MB_SettingEventReply - notification for passing messages and status changes.
		 */
		public void MB_SetLocationReminderEnabled(bool enabled)
		{
		}


		/**
		 * Gets beacon notifications status (used for geo-targeting)
		 */
		public bool MB_GetBeaconReminderStatus()
		{
			return true;
		}

		/**
		 * Enables or disables location notifications (used for geo-targeting)
		 *
		 * @param enabled Notifications triggered:
		 *                com.mobilebridge.MB_SettingEventReply - notification for passing messages and status changes.
		 */
		public void MB_SetBeaconReminderEnabled(bool enabled)
		{

		}

		/**
		 * Enables or disables notifications
		 *
		 * @param enable
		 * @param showAlert Notifications triggered:
		 *                  com.mobilebridge.MB_SettingEventReply - notification for passing messages and status changes.
		 */
		public void MB_SetNotificationEnabled(bool enabled)
		{

		}

		/**
		 * Retrieves the app's notification status.
		 */
		public bool MB_GetNotificationStatus()
		{
			return true;
		}


		////////////////////////////// Test Purposes //////////////////////////////

		/**
		 * Adds users to a test group.
		 * This group can be used to send notifications to users in the group.
		 *
		 * @params firstName, lastName
		 */
		public void MB_JoinTestGroup(String firstName, String lastName)
		{
		}


		/**
		 * Removes user from test group
		 *
		 * @params firstName, lastName
		 */
		public void MB_LeaveTestGroup(string firstName, string lastName)
		{

		}

		/**
		 * Clears user history
		 * Deletes the user history on the MobileBridge server. This includes, for example, the promotions viewed or the polls that were answered.
		 * This methods enables you to clear the history programmatically, as part of the process of debugging your app.
		 * IMPORTANT: aims to be used ONLY for test purposes, don't use on "production"
		 */
		public bool MB_ClearUserHistory()
		{
			return true;
		}


		////////////////////////////// Analytics //////////////////////////////

		/**
		 * Logs the event of a user click, including the time-stamp of the event.
		 *
		 * @params key: the key of your choice, identifying the click event.
		 */
		public void MB_ClickTraceLog(string key)
		{

		}

		/**
		 * Starts logging view trace, date and time that the user enters a screen.
		 *
		 * @params key: the key of your choice, identifying the page.
		 */
		public void MB_ViewTraceLog_Start(string key)
		{

		}

		/**
		 * Stops logging view trace
		 *
		 * @params key: unique string identifier you used to start view trace
		 */
		public void MB_ViewTraceLog_Stop(string key)
		{
		}


		////////////////////////////// Assets //////////////////////////////

		/**
		 * Return asset view:
		 * Promotion, rich message, survey, poll
		 *
		 * @params data: asset's data. Example {"id": "123"}
		 */
		public Page MB_GetAssetView(string data)
		{
			return null;
		}

		/*
	* Shows banner view
	* TODO: @param viewName, tag
	*/
		public Page MB_ShowBannerView()
		{
			return null;
		}

		////////////////////////////// TOKEN //////////////////////////////
		public void MB_SetToken(string token)
		{
			MBConnectorInitLibrary.mbLib.MB_SetToken(token, Com.Mobilebridge.Settoken.SetToken.PushType.Gcm);
		}


		/// //////////////// DEVICE CONTEXT  ///////////////
		public string MB_GetDeviceContext()
		{
			return MBConnectorInitLibrary.mbLib.MB_GetDeviceId();
		}
	}

}

