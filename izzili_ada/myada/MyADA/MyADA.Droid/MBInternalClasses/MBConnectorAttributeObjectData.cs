﻿using System;
using Com.Mobilebridge.Library;
using Org.Json;
using Com.Mobilebridge.AttributeManager;
using MyADA.Common;
using MyADA.Common.Views;

namespace MyADA.Droid
{
	public class MBConnectorAttributeObjectData
	{
		MobileBridgeDelegate mbDelegate;
		public static MBXamarinFormsTrainingAppPage.CustomerAttributeDelegate attributeDelegate;

		public MBConnectorAttributeObjectData()
		{
		}

		public void MB_GetCustomerAttribute(MBXamarinFormsTrainingAppPage.CustomerAttributeDelegate _interface)
		{
			attributeDelegate = _interface;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_GetCustomerAttribute(mbDelegate);
			}
		}


		public void MB_SetCustomerAttribute(MBXamarinFormsTrainingAppPage.CustomerAttributeDelegate _interface)
		{
			attributeDelegate = _interface;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MB_AttributeObject aobj = MBConnectorInitLibrary.attributeObject;
				MBConnectorInitLibrary.mbLib.MB_SetCustomerAttribute(aobj , mbDelegate);
			}
		}

		public void MB_ClearCustomerAttribute(MBXamarinFormsTrainingAppPage.CustomerAttributeDelegate _interface)
		{
			attributeDelegate = _interface;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_ClearCustomerAttribute(mbDelegate);
			}
		}

		//DELEGATE//
		public class MobileBridgeDelegate : Java.Lang.Object, MobileBridgeLibrary.IMB_AttributeInterface 
		{
			public void MB_CustomerAttributeRequestFinished(AttributeManager.AttributeRequestType attributeRequestType, JSONObject data)
			{
				if (attributeRequestType == AttributeManager.AttributeRequestType.GetAttribute)
				{
					attributeDelegate.MB_GetCustomerAttributeDataFinished(data.ToString());
				}
				else if (attributeRequestType == AttributeManager.AttributeRequestType.ClearAttribute)
				{
					attributeDelegate.MB_CustomerAttributeDeleted(data.ToString());
				}
				else if (attributeRequestType == AttributeManager.AttributeRequestType.SetAttribute)
				{
					attributeDelegate.MB_SetCustomerAttributeDataFinished(data.ToString());
				}
			}
		}

	}
}

