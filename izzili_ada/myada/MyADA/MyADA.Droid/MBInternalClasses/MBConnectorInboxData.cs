﻿using System;
using Com.Mobilebridge.Library;
using Org.Json;
using MyADA.Common;
using MyADA.Common.Views;

namespace MyADA.Droid
{
	public class MBConnectorInboxData
	{
		MobileBridgeDelegate mbDelegate;
		public static MBXamarinFormsTrainingAppPage.InboxDataFinished inboxDelegate;

		public MBConnectorInboxData()
		{
		}

		public void MB_GetInboxData(MBXamarinFormsTrainingAppPage.InboxDataFinished _delegate)
		{
			inboxDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_GetInboxData(MobileBridgeLibrary.MB_InboxDataType.GetInboxData , "", mbDelegate);
			}
		}

		public void MB_DeleteInboxData(MBXamarinFormsTrainingAppPage.InboxDataFinished _delegate)
		{
			inboxDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_GetInboxData(MobileBridgeLibrary.MB_InboxDataType.DeleteAllData, "", mbDelegate);
			}
		}

		public void MB_DeleteInboxItem(string inbox_id, MBXamarinFormsTrainingAppPage.InboxDataFinished _delegate)
		{
			inboxDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_GetInboxData(MobileBridgeLibrary.MB_InboxDataType.DeleteItemData, inbox_id, mbDelegate);
			}
		}

		public class MobileBridgeDelegate : Java.Lang.Object, MobileBridgeLibrary.IMB_GetInboxDataInterface
		{
			public void MB_GetInboxDataFinished(MobileBridgeLibrary.MB_InboxDataType identifier, JSONObject data)
			{
				if (identifier == MobileBridgeLibrary.MB_InboxDataType.GetInboxData)
				{
					string strJson = data.ToString();
					inboxDelegate.MB_GetInboxDataFinished(strJson);
				}
				else if (identifier == MobileBridgeLibrary.MB_InboxDataType.DeleteAllData)
				{
					string strJson = data.ToString();
					inboxDelegate.MB_InboxDataDeleted(strJson);
				}
				else if (identifier == MobileBridgeLibrary.MB_InboxDataType.DeleteItemData)
				{
					string strJson = data.ToString();
					inboxDelegate.MB_InboxItemDeleted(strJson);
				}
			}
		}

	}
}

