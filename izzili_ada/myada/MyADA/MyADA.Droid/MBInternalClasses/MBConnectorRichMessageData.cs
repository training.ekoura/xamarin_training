﻿using System;
using Com.Mobilebridge.Library;
using Org.Json;
using MyADA.Common;
using MyADA.Common.Views;

namespace MyADA.Droid
{
	public class MBConnectorRichMessageData
	{
		private static MBXamarinFormsTrainingAppPage.RichMessageDelegate rmDelegate;
	    MobileBridgeDelegate mbDelegate;

		public MBConnectorRichMessageData()
		{
		}

		public void MB_GetRichMessageData(string assetInfo, MBXamarinFormsTrainingAppPage.RichMessageDelegate _delegate)
		{
			rmDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				JSONObject pId = new JSONObject(assetInfo);
				if (pId != null)
				{
					MBConnectorInitLibrary.mbLib.MB_GetRichMessageData(pId, mbDelegate);
				}
			}
		}

		public void MB_GetAllRichMessages(MBXamarinFormsTrainingAppPage.RichMessageDelegate _delegate)
		{
			rmDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_GetAllRichMessages(mbDelegate);
			}
		}

		class MobileBridgeDelegate : Java.Lang.Object, MobileBridgeLibrary.IMB_GetRichMessageDataInterface
		{
			public void MB_GetAllRichMessagesFinished(JSONArray data)
			{
				string strJson = data.ToString();
				rmDelegate.MB_GetAllRichMessages(strJson);
			}

			public void MB_GetRichMessageDataFinished(JSONObject data)
			{
				string strJson = data.ToString();
				rmDelegate.MB_GetRichMessageData(strJson);
			}
		}
	}
}

