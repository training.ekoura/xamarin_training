﻿using System;
using Com.Mobilebridge.Library;
using Org.Json;
using MyADA.Common;
using MyADA.Common.Views;

namespace MyADA.Droid
{
	public class MBConnectorPromotionsData
	{
		//IPromotionData delegate;
		MobileBridgeDelegate mbDelegate;
		public static MBXamarinFormsTrainingAppPage.MyDelegate myDelegateDelegate;

		public MBConnectorPromotionsData()
		{
		}

		public void MB_GetPublicPromotionsData(MBXamarinFormsTrainingAppPage.MyDelegate _delegate)
		{
			myDelegateDelegate = _delegate;
			mbDelegate = new MobileBridgeDelegate();

			if (MBConnectorInitLibrary.mbLib != null)
			{
				MBConnectorInitLibrary.mbLib.MB_GetPublicPromotionData(mbDelegate);
			}
		}

		public class MobileBridgeDelegate : Java.Lang.Object, MobileBridgeLibrary.IMB_GetPublicPromotionDataInterface
		{
			public void MB_GetPublicPromotionDataFinished(JSONArray data)
			{
				string strJson = data.ToString();

				myDelegateDelegate.MB_GetPublicPromotionDataFinished(strJson);
			}
		}
	}
}

