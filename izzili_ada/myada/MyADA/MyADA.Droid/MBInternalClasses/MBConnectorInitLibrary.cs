﻿using System;
using Com.Mobilebridge.Library;
using Android.Widget;
using Com.Mobilebridge.AttributeManager;
using Android.Content;

namespace MyADA.Droid
{
	public class MBConnectorInitLibrary
	{
		public static Context context;

		MobileBridgeDelegate mbDelegate;
		public static MobileBridgeLibrary mbLib;
		public static MB_AttributeObject attributeObject;

		public MBConnectorInitLibrary()
		{

		}

		public void MB_InitLibrary( string clientKey, string clientSecret)
		{
			context = MainActivity.context;
			mbDelegate = new MobileBridgeDelegate(this);
			MBConnectorInitLibrary.attributeObject = new MB_AttributeObject();
			Com.Mobilebridge.MB_ClientCredentials cc = new Com.Mobilebridge.MB_ClientCredentials(MainActivity.context, clientKey, clientSecret);
			MobileBridgeLibrary.MB_InitLibrary(context, cc, mbDelegate);
		}

		public class MobileBridgeDelegate : Java.Lang.Object, MobileBridgeLibrary.IMobileBridgeLibraryInterface
		{
			MBConnectorInitLibrary mMobileBridgeConnector;

			public MobileBridgeDelegate(MBConnectorInitLibrary mobileBridgeConnector)
			{
				mMobileBridgeConnector = mobileBridgeConnector;
			}

			public void MB_LibraryInitialized(MobileBridgeLibrary mblib, string description)
			{
				if (mblib != null)
				{
					MBConnectorInitLibrary.mbLib = mblib;
					mbLib.MB_PrintSQSMessages(true);
					Toast.MakeText(Android.App.Application.Context, "Library Initialized", ToastLength.Short).Show();
				}
				else
				{
					Toast.MakeText(Android.App.Application.Context, "Library Initialized - error", ToastLength.Short).Show();
				}

			}

			public void MB_LocationUpdate(int position, int max)
			{
				int i = 0;
				i++;
			}

			public void MB_NetworkAlertPrompt(int alertId)
			{
				int i = 0;
				i++;
			}

			public void MB_SettingEventReply(Org.Json.JSONObject p0)
			{
				int i = 0;
				i++;
			}

			public void MB_UpdateCachingDataFinished()
			{
				int i = 0;
				i++;
			}

		}

		public class Person
		{
			public string Name { get; set; }
			public DateTime Birthday { get; set; }
		}
	}
}

