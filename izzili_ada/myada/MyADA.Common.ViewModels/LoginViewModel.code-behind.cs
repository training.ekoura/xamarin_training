
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;
using MyADA.Common.Services.Requests;
using MyADA.Common.DAOServices.Services;
using MyADA.Common.Services.Models;
using System.Text.RegularExpressions;

namespace MyADA.Common.ViewModels
{
	public partial class LoginViewModel : LoginViewModelBase, ILoginViewModel
    {
        #region ==================================== Properties ====================================
        private string Locale = Properties.Resources.CultureInfoCurrentCultureName;
        private IGlobalConfigService GlobalConfigService;
        private static Regex EmailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        #endregion

        #region ==================================== Initialisation ====================================
        protected override void OnInitialize()
        {
            base.OnInitialize();
            ResolveInitialisation();
        }

        private void ResolveInitialisation()
        {
            GlobalConfigService = CoreSDK.Framework.Services.GlobalConfigService();
        }
        #endregion

        /// <summary>
        /// Command when the user click on login Button
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override async void OnLoginCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                var l_Isvalid = this.Validate();
                if (l_Isvalid)
                    if (!string.IsNullOrWhiteSpace(this.Email))
                    {
                        var l_SignInRequest = new LoginRequest()
                        { 
                            Email                 = this.Email.Trim(),
                            Secret                = GlobalConfigService.Data.Secret,
                            DeviceId              = CoreSDK.Framework.Services.InteractionService().DeviceIMEI,
                            DeviceOs              = CoreSDK.Framework.Services.InteractionService().DevicePlatform,
                            PushNotificationToken = CoreSDK.Framework.Services.InteractionService().DeviceToken,
                            MBTempUserKey         = Constants.MBTempUserKey
                        };

                        var l_LoginService = CoreSDK.Framework.Services.Container().Resolve<ILoginService>();
                        var l_SignInResponse = await l_LoginService.SignIn(l_SignInRequest);
                        if(l_SignInResponse != null)
                        {
                                // We save theses Keys for later.
                                GlobalConfigService.Data.Secret = l_SignInResponse.Secret;
                                GlobalConfigService.Data.Token = l_SignInResponse.Token;
                                GlobalConfigService.Data.DeviceToken = CoreSDK.Framework.Services.InteractionService().DeviceToken;
                                GlobalConfigService.Data.DevicePlatform = CoreSDK.Framework.Services.InteractionService().DevicePlatform;
                                GlobalConfigService.SaveConfig();
                            /*}
                            else
                                await CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.NoTokenOrSecret);*/
                        }

                        // Redirect user on homepage if everything is right.
                        if (GlobalConfigService.Data.Token != null && GlobalConfigService.Data.Secret != null){

                            // Only if AuthorizationKey is null or empty.
                            if (string.IsNullOrEmpty(GlobalConfigService.Data.AuthorizationKey))
                            {
                                // Save authorizationKey before redirect to HomePage.
                                GlobalConfigService.Data.AuthorizationKey = Constants.Bearer + GlobalConfigService.Data.Token;
                                GlobalConfigService.SaveConfig();
                            }

                            // Prepare user data for the homeview.
                            var l_HomeData = CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>();
                            await l_HomeData.PrepareVM();

                            // Redirection to the new page
                            await CoreSDK.Framework.Services.InteractionService().Open(ViewName.MasterDetailView, null);
                            
                            
                        }
                        
                    }
            }
            catch (Exception ex) {
                CoreSDK.Framework.Services.InteractionService().Alert("", ex.Message);
            }
            finally
            {
                EndWork();
            }
        }

        /// <summary>
        /// Command when user click on "Conditions label".
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override void OnOpenGeneralConditionsCommand_Execute(object p_Parameter)
        {
        }


        protected override void OnValidate(IValidationContext p_Context)
        {
            base.OnValidate(p_Context);
            if (string.IsNullOrWhiteSpace(Email))
            {
                p_Context.AddError(Properties.Resources.Required, Email_PROPERTYNAME);
            }
            else if (!EmailRegex.Match(Email).Success)
            {
                p_Context.AddError(Properties.Resources.MailNotValid, Email_PROPERTYNAME);
            }
        }

        protected override void OnLangugeSelectionCommand_Execute(object p_Parameter)
        {
            
        }
    }
}
