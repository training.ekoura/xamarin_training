﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.ViewModels
{
    public static class Constants
    {
        public const string MBTempUserKey = "lvuqvhfuvhfibhfubhfqubf";
        public const string Bearer = "Bearer ";

        public const string StringThemeSodexo = "Sodexo";
        public const string ONRESUME = "OnResume";

        public const string StringExceptionTypeNoSite = "AccountWithoutOpenPortailException";

        // CLEAR OU MD5
        public const string PwdMode = "CLEAR";

        // Liste des thèmes
        public const string ThemeMySodexo = "my-sodexo";
        public const int HoursValidToken = 12;

        public const string CancelLogicValue = "ANNULEE";

        public const string KoreanCultureInfo = "ko-KR";
        public const string EnglishCultureInfo = "";

        public const string _LeadsStatus = "allocated,closed,new";

        // Mobile bridge 
        public const string RichMessageTagShowroom = "showroom";
        public const string RichMessageTagNews = "news";
        public const string RichMessageTagSalesTool = "sales-tool";
        public const string RichMessageTagTraining = "training";



    }
  
}
