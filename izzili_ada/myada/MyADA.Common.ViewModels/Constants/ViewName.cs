﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.ViewModels
{
    public static class ViewName
    {
        public const string BurgerMenuView = "IBurgerMenuView";
        public const string ConnexionView = "IConnexionView";
        public const string DashboardView = "IDashboardView";
        public const string MasterDetailView = "IMasterDetailView";
        public const string OrderDetailView = "IOrderDetailView";
        public const string OrderListView = "IOrderListView";
        public const string LoginView = "ILoginView";
        public const string BurgerView = "IBurgerView";
        public const string HomeView = "IHomeView";
        public const string SettingsProfileView = "ISettingsProfileView";
        public const string SettingsChangeView = "ISettingsChangeView";
        public const string ChangeRequestView = "IChangeRequestView";
        public const string MyLeadsView = "IMyLeadsView";
        public const string MyReportView = "IMyReportView";
        public const string MyRSSView = "IMyRSSView";
        public const string MBXamarinFormsTrainingAppPage = "IMBXamarinFormsTrainingAppPage";
        public const string InboxDetailView = "IInboxDetailView";
        public const string CommercialLeadsView = "ICommercialLeadsView";
        public const string BookLeadsView = "IBookLeadsView";
        public const string MyOwnLeadsView = "IMyOwnLeadsView";
        public const string LeadsDetailsView = "ILeadsDetailsView";
        //CoreSDK.Framework.Services.Container().Register<IFilterView, FilterView>();
        public const string AvailableRSSView = "IAvailableRSSView";
        public const string NavigateLeadsView = "INavigateLeadsView";
        public const string LeadsDetailsWorkflowView = "ILeadsDetailsWorkflowView";
        public const string ReadDocumentView = "IReadDocumentView";
        public const string EnterRssUrlView = "IEnterRssUrlView";
        public const string ManageRSSView = "IManageRSSView";
        
        

        //PopInViews
        public const string ModifierStockContentView = "ModifierStockContentView";
        public const string PlatstatusContentView = "PlatstatusContentView";
        public const string CalndarContentView = "CalndarContentView";
        public const string YesNoContentView = "YesNoContentView";
        public const string StatusFilterContentView = "StatusFilterContentView";

        // Mobile Bridge Views
        public const string AudiNewsView = "AudiNewsView";
        public const string SelfTraining = "SelfTraining";
        public const string SalesTools = "SalesTools";
        public const string MBNewsDetails = "IMBNewsDetails";
        
    }
}
