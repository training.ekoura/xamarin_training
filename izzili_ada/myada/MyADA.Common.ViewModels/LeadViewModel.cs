﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML LeadViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  CoreSDK.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;

namespace MyADA.Common.ViewModels
{
	public abstract partial class LeadViewModelBase : CoreSDK.ViewModel
	{
		protected override void OnInitialize()
		{
			base.OnInitialize();

			// Initialisation de la collection LeadsCollection.
			LeadsCollection = new System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>();
			// Initialisation de la collection LeadsForFilterCollection.
			LeadsForFilterCollection = new System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>();
			// Initialisation de la collection OnUseLeadsCollection.
			OnUseLeadsCollection = new System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>();
			// Initialisation de la collection OnUseDayLeadsCollection.
			OnUseDayLeadsCollection = new System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>();
			// Initialisation de la collection SaveOnUseDayLeadsCollection.
			SaveOnUseDayLeadsCollection = new System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>();
			// Initialisation de la collection LeadsOfTheDateForFilterCollection.
			LeadsOfTheDateForFilterCollection = new System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>();
			// Initialisation de la collection OnUseOthersDayLeadsCollection.
			OnUseOthersDayLeadsCollection = new System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>();
			// Initialisation de la collection SaveOnUseOthersDayLeadsCollection.
			SaveOnUseOthersDayLeadsCollection = new System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>();
			// Initialisation de la collection LeadsOnGoingForFilterCollection.
			LeadsOnGoingForFilterCollection = new System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>();
			// Initialisation de la collection UsersCollections.
			UsersCollections = new System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos>();
			// Initialisation de la collection ManagedShowRoomLeadCollections.
			ManagedShowRoomLeadCollections = new System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ManagedShowRoomLead>();
			// Initialisation de la collection LeadFilterModelsCollections.
			LeadFilterModelsCollections = new System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.LeadFilterModels>();
			// Initialisation de la commande ShowAllocatedLeadCommand.
			ShowAllocatedLeadCommand = new DelegateCommand(ShowAllocatedLeadCommand_CanExecute, ShowAllocatedLeadCommand_Execute);
			// Initialisation de la commande ShowLeadDetailsCommand.
			ShowLeadDetailsCommand = new DelegateCommand(ShowLeadDetailsCommand_CanExecute, ShowLeadDetailsCommand_Execute);
			// Initialisation de la commande ShowMyLeadDetailsCommand.
			ShowMyLeadDetailsCommand = new DelegateCommand(ShowMyLeadDetailsCommand_CanExecute, ShowMyLeadDetailsCommand_Execute);
			// Initialisation de la commande SelectRepresentativeCommand.
			SelectRepresentativeCommand = new DelegateCommand(SelectRepresentativeCommand_CanExecute, SelectRepresentativeCommand_Execute);
			// Initialisation de la commande AddOrRemoveAffectationCommand.
			AddOrRemoveAffectationCommand = new DelegateCommand(AddOrRemoveAffectationCommand_CanExecute, AddOrRemoveAffectationCommand_Execute);
			// Initialisation de la commande SetLeadStatusCommand.
			SetLeadStatusCommand = new DelegateCommand(SetLeadStatusCommand_CanExecute, SetLeadStatusCommand_Execute);
			// Initialisation de la commande AllocateCommand.
			AllocateCommand = new DelegateCommand(AllocateCommand_CanExecute, AllocateCommand_Execute);
			// Initialisation de la commande LiveChangeLeadStatusOnDoneCommand.
			LiveChangeLeadStatusOnDoneCommand = new DelegateCommand(LiveChangeLeadStatusOnDoneCommand_CanExecute, LiveChangeLeadStatusOnDoneCommand_Execute);
			// Initialisation de la commande LiveChangeLeadStatusOnRecontactCommand.
			LiveChangeLeadStatusOnRecontactCommand = new DelegateCommand(LiveChangeLeadStatusOnRecontactCommand_CanExecute, LiveChangeLeadStatusOnRecontactCommand_Execute);
			// Initialisation de la commande LiveChangeLeadStatusOnNotInterestCommand.
			LiveChangeLeadStatusOnNotInterestCommand = new DelegateCommand(LiveChangeLeadStatusOnNotInterestCommand_CanExecute, LiveChangeLeadStatusOnNotInterestCommand_Execute);
			// Initialisation de la commande SimpleUserAllocateLeadCommand.
			SimpleUserAllocateLeadCommand = new DelegateCommand(SimpleUserAllocateLeadCommand_CanExecute, SimpleUserAllocateLeadCommand_Execute);
			// Initialisation de la commande ShowMyLeadsCommand.
			ShowMyLeadsCommand = new DelegateCommand(ShowMyLeadsCommand_CanExecute, ShowMyLeadsCommand_Execute);
			// Initialisation de la commande NextCommand.
			NextCommand = new DelegateCommand(NextCommand_CanExecute, NextCommand_Execute);
			// Initialisation de la commande BackCommand.
			BackCommand = new DelegateCommand(BackCommand_CanExecute, BackCommand_Execute);
			// Initialisation de la commande DoFilterCommand.
			DoFilterCommand = new DelegateCommand(DoFilterCommand_CanExecute, DoFilterCommand_Execute);
			// Initialisation de la commande MyOwnLeadsPopinVisibilityCommand.
			MyOwnLeadsPopinVisibilityCommand = new DelegateCommand(MyOwnLeadsPopinVisibilityCommand_CanExecute, MyOwnLeadsPopinVisibilityCommand_Execute);
			// Initialisation de la commande LoadMoreCommand.
			LoadMoreCommand = new DelegateCommand(LoadMoreCommand_CanExecute, LoadMoreCommand_Execute);
			// Initialisation de la commande LoadMoreLeadOfTheDateCommand.
			LoadMoreLeadOfTheDateCommand = new DelegateCommand(LoadMoreLeadOfTheDateCommand_CanExecute, LoadMoreLeadOfTheDateCommand_Execute);
			// Initialisation de la commande LoadMoreLeadOnGoingCommand.
			LoadMoreLeadOnGoingCommand = new DelegateCommand(LoadMoreLeadOnGoingCommand_CanExecute, LoadMoreLeadOnGoingCommand_Execute);
			// Initialisation de la commande CloseFilterViewAndLoadDataCommand.
			CloseFilterViewAndLoadDataCommand = new DelegateCommand(CloseFilterViewAndLoadDataCommand_CanExecute, CloseFilterViewAndLoadDataCommand_Execute);
		}

		#region === Propriétés ===

			#region === Propriété : CurrentUserProfileInfos ===

				public const string CurrentUserProfileInfos_PROPERTYNAME = "CurrentUserProfileInfos";

				private MyADA.Common.Services.Models.CurrentUserProfile _CurrentUserProfileInfos;
				///<summary>
				/// Propriété : CurrentUserProfileInfos
				///</summary>
				public MyADA.Common.Services.Models.CurrentUserProfile CurrentUserProfileInfos
				{
					get
					{
						return GetValue<MyADA.Common.Services.Models.CurrentUserProfile>(() => _CurrentUserProfileInfos);
					}
					set
					{
						SetValue<MyADA.Common.Services.Models.CurrentUserProfile>(() => _CurrentUserProfileInfos, (v) => _CurrentUserProfileInfos = v, value, CurrentUserProfileInfos_PROPERTYNAME,  DoCurrentUserProfileInfosBeforeSet, DoCurrentUserProfileInfosAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCurrentUserProfileInfosBeforeSet(string p_PropertyName, MyADA.Common.Services.Models.CurrentUserProfile p_OldValue, MyADA.Common.Services.Models.CurrentUserProfile p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCurrentUserProfileInfosAfterSet(string p_PropertyName, MyADA.Common.Services.Models.CurrentUserProfile p_OldValue, MyADA.Common.Services.Models.CurrentUserProfile p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCurrentUserProfileInfosDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : TotalShowLeadCount ===

				public const string TotalShowLeadCount_PROPERTYNAME = "TotalShowLeadCount";

				private int _TotalShowLeadCount;
				///<summary>
				/// Propriété : TotalShowLeadCount
				///</summary>
				public int TotalShowLeadCount
				{
					get
					{
						return GetValue<int>(() => _TotalShowLeadCount);
					}
					set
					{
						SetValue<int>(() => _TotalShowLeadCount, (v) => _TotalShowLeadCount = v, value, TotalShowLeadCount_PROPERTYNAME,  DoTotalShowLeadCountBeforeSet, DoTotalShowLeadCountAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTotalShowLeadCountBeforeSet(string p_PropertyName, int p_OldValue, int p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTotalShowLeadCountAfterSet(string p_PropertyName, int p_OldValue, int p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTotalShowLeadCountDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CurrentPageCount ===

				public const string CurrentPageCount_PROPERTYNAME = "CurrentPageCount";

				private int _CurrentPageCount;
				///<summary>
				/// Propriété : CurrentPageCount
				///</summary>
				public int CurrentPageCount
				{
					get
					{
						return GetValue<int>(() => _CurrentPageCount);
					}
					set
					{
						SetValue<int>(() => _CurrentPageCount, (v) => _CurrentPageCount = v, value, CurrentPageCount_PROPERTYNAME,  DoCurrentPageCountBeforeSet, DoCurrentPageCountAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCurrentPageCountBeforeSet(string p_PropertyName, int p_OldValue, int p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCurrentPageCountAfterSet(string p_PropertyName, int p_OldValue, int p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCurrentPageCountDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowNoLeadMessage ===

				public const string ShowNoLeadMessage_PROPERTYNAME = "ShowNoLeadMessage";

				private bool _ShowNoLeadMessage;
				///<summary>
				/// Propriété : ShowNoLeadMessage
				///</summary>
				public bool ShowNoLeadMessage
				{
					get
					{
						return GetValue<bool>(() => _ShowNoLeadMessage);
					}
					set
					{
						SetValue<bool>(() => _ShowNoLeadMessage, (v) => _ShowNoLeadMessage = v, value, ShowNoLeadMessage_PROPERTYNAME,  DoShowNoLeadMessageBeforeSet, DoShowNoLeadMessageAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowNoLeadMessageBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowNoLeadMessageAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowNoLeadMessageDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectedUser ===

				public const string SelectedUser_PROPERTYNAME = "SelectedUser";

				private MyADA.Common.Services.Models.UserInfos _SelectedUser;
				///<summary>
				/// Propriété : SelectedUser
				///</summary>
				public MyADA.Common.Services.Models.UserInfos SelectedUser
				{
					get
					{
						return GetValue<MyADA.Common.Services.Models.UserInfos>(() => _SelectedUser);
					}
					set
					{
						SetValue<MyADA.Common.Services.Models.UserInfos>(() => _SelectedUser, (v) => _SelectedUser = v, value, SelectedUser_PROPERTYNAME,  DoSelectedUserBeforeSet, DoSelectedUserAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectedUserBeforeSet(string p_PropertyName, MyADA.Common.Services.Models.UserInfos p_OldValue, MyADA.Common.Services.Models.UserInfos p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectedUserAfterSet(string p_PropertyName, MyADA.Common.Services.Models.UserInfos p_OldValue, MyADA.Common.Services.Models.UserInfos p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectedUserDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsAllocatedVisible ===

				public const string IsAllocatedVisible_PROPERTYNAME = "IsAllocatedVisible";

				private bool _IsAllocatedVisible;
				///<summary>
				/// Propriété : IsAllocatedVisible
				///</summary>
				public bool IsAllocatedVisible
				{
					get
					{
						return GetValue<bool>(() => _IsAllocatedVisible);
					}
					set
					{
						SetValue<bool>(() => _IsAllocatedVisible, (v) => _IsAllocatedVisible = v, value, IsAllocatedVisible_PROPERTYNAME,  DoIsAllocatedVisibleBeforeSet, DoIsAllocatedVisibleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsAllocatedVisibleBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsAllocatedVisibleAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsAllocatedVisibleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsAllocateButtonVisible ===

				public const string IsAllocateButtonVisible_PROPERTYNAME = "IsAllocateButtonVisible";

				private bool _IsAllocateButtonVisible;
				///<summary>
				/// Propriété : IsAllocateButtonVisible
				///</summary>
				public bool IsAllocateButtonVisible
				{
					get
					{
						return GetValue<bool>(() => _IsAllocateButtonVisible);
					}
					set
					{
						SetValue<bool>(() => _IsAllocateButtonVisible, (v) => _IsAllocateButtonVisible = v, value, IsAllocateButtonVisible_PROPERTYNAME,  DoIsAllocateButtonVisibleBeforeSet, DoIsAllocateButtonVisibleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsAllocateButtonVisibleBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsAllocateButtonVisibleAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsAllocateButtonVisibleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectedLead ===

				public const string SelectedLead_PROPERTYNAME = "SelectedLead";

				private LeadsItemViewModel _SelectedLead;
				///<summary>
				/// Propriété : SelectedLead
				///</summary>
				public LeadsItemViewModel SelectedLead
				{
					get
					{
						return GetValue<LeadsItemViewModel>(() => _SelectedLead);
					}
					set
					{
						SetValue<LeadsItemViewModel>(() => _SelectedLead, (v) => _SelectedLead = v, value, SelectedLead_PROPERTYNAME,  DoSelectedLeadBeforeSet, DoSelectedLeadAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectedLeadBeforeSet(string p_PropertyName, LeadsItemViewModel p_OldValue, LeadsItemViewModel p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectedLeadAfterSet(string p_PropertyName, LeadsItemViewModel p_OldValue, LeadsItemViewModel p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectedLeadDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowNextButton ===

				public const string ShowNextButton_PROPERTYNAME = "ShowNextButton";

				private bool _ShowNextButton;
				///<summary>
				/// Propriété : ShowNextButton
				///</summary>
				public bool ShowNextButton
				{
					get
					{
						return GetValue<bool>(() => _ShowNextButton);
					}
					set
					{
						SetValue<bool>(() => _ShowNextButton, (v) => _ShowNextButton = v, value, ShowNextButton_PROPERTYNAME,  DoShowNextButtonBeforeSet, DoShowNextButtonAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowNextButtonBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowNextButtonAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowNextButtonDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowAllocateButton ===

				public const string ShowAllocateButton_PROPERTYNAME = "ShowAllocateButton";

				private bool _ShowAllocateButton;
				///<summary>
				/// Propriété : ShowAllocateButton
				///</summary>
				public bool ShowAllocateButton
				{
					get
					{
						return GetValue<bool>(() => _ShowAllocateButton);
					}
					set
					{
						SetValue<bool>(() => _ShowAllocateButton, (v) => _ShowAllocateButton = v, value, ShowAllocateButton_PROPERTYNAME,  DoShowAllocateButtonBeforeSet, DoShowAllocateButtonAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowAllocateButtonBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowAllocateButtonAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowAllocateButtonDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowSaveButton ===

				public const string ShowSaveButton_PROPERTYNAME = "ShowSaveButton";

				private bool _ShowSaveButton;
				///<summary>
				/// Propriété : ShowSaveButton
				///</summary>
				public bool ShowSaveButton
				{
					get
					{
						return GetValue<bool>(() => _ShowSaveButton);
					}
					set
					{
						SetValue<bool>(() => _ShowSaveButton, (v) => _ShowSaveButton = v, value, ShowSaveButton_PROPERTYNAME,  DoShowSaveButtonBeforeSet, DoShowSaveButtonAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowSaveButtonBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowSaveButtonAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowSaveButtonDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : PreviousViewName ===

				public const string PreviousViewName_PROPERTYNAME = "PreviousViewName";

				private string _PreviousViewName;
				///<summary>
				/// Propriété : PreviousViewName
				///</summary>
				public string PreviousViewName
				{
					get
					{
						return GetValue<string>(() => _PreviousViewName);
					}
					set
					{
						SetValue<string>(() => _PreviousViewName, (v) => _PreviousViewName = v, value, PreviousViewName_PROPERTYNAME,  DoPreviousViewNameBeforeSet, DoPreviousViewNameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoPreviousViewNameBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoPreviousViewNameAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyPreviousViewNameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : NotifUseDayLeadsCollection ===

				public const string NotifUseDayLeadsCollection_PROPERTYNAME = "NotifUseDayLeadsCollection";

				private string _NotifUseDayLeadsCollection;
				///<summary>
				/// Propriété : NotifUseDayLeadsCollection
				///</summary>
				public string NotifUseDayLeadsCollection
				{
					get
					{
						return GetValue<string>(() => _NotifUseDayLeadsCollection);
					}
					set
					{
						SetValue<string>(() => _NotifUseDayLeadsCollection, (v) => _NotifUseDayLeadsCollection = v, value, NotifUseDayLeadsCollection_PROPERTYNAME,  DoNotifUseDayLeadsCollectionBeforeSet, DoNotifUseDayLeadsCollectionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoNotifUseDayLeadsCollectionBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoNotifUseDayLeadsCollectionAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyNotifUseDayLeadsCollectionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : NotifUseOthersDayLeadsCollection ===

				public const string NotifUseOthersDayLeadsCollection_PROPERTYNAME = "NotifUseOthersDayLeadsCollection";

				private string _NotifUseOthersDayLeadsCollection;
				///<summary>
				/// Propriété : NotifUseOthersDayLeadsCollection
				///</summary>
				public string NotifUseOthersDayLeadsCollection
				{
					get
					{
						return GetValue<string>(() => _NotifUseOthersDayLeadsCollection);
					}
					set
					{
						SetValue<string>(() => _NotifUseOthersDayLeadsCollection, (v) => _NotifUseOthersDayLeadsCollection = v, value, NotifUseOthersDayLeadsCollection_PROPERTYNAME,  DoNotifUseOthersDayLeadsCollectionBeforeSet, DoNotifUseOthersDayLeadsCollectionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoNotifUseOthersDayLeadsCollectionBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoNotifUseOthersDayLeadsCollectionAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyNotifUseOthersDayLeadsCollectionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsBusy ===

				public const string IsBusy_PROPERTYNAME = "IsBusy";

				private bool _IsBusy;
				///<summary>
				/// Propriété : IsBusy
				///</summary>
				public bool IsBusy
				{
					get
					{
						return GetValue<bool>(() => _IsBusy);
					}
					set
					{
						SetValue<bool>(() => _IsBusy, (v) => _IsBusy = v, value, IsBusy_PROPERTYNAME,  DoIsBusyBeforeSet, DoIsBusyAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsBusyBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsBusyAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsBusyDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsDefaultLeadCollectionFilter ===

				public const string IsDefaultLeadCollectionFilter_PROPERTYNAME = "IsDefaultLeadCollectionFilter";

				private bool _IsDefaultLeadCollectionFilter;
				///<summary>
				/// Propriété : IsDefaultLeadCollectionFilter
				///</summary>
				public bool IsDefaultLeadCollectionFilter
				{
					get
					{
						return GetValue<bool>(() => _IsDefaultLeadCollectionFilter);
					}
					set
					{
						SetValue<bool>(() => _IsDefaultLeadCollectionFilter, (v) => _IsDefaultLeadCollectionFilter = v, value, IsDefaultLeadCollectionFilter_PROPERTYNAME,  DoIsDefaultLeadCollectionFilterBeforeSet, DoIsDefaultLeadCollectionFilterAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsDefaultLeadCollectionFilterBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsDefaultLeadCollectionFilterAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsDefaultLeadCollectionFilterDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : MyOwnLeadsPopinIsVisible ===

				public const string MyOwnLeadsPopinIsVisible_PROPERTYNAME = "MyOwnLeadsPopinIsVisible";

				private bool _MyOwnLeadsPopinIsVisible;
				///<summary>
				/// Propriété : MyOwnLeadsPopinIsVisible
				///</summary>
				public bool MyOwnLeadsPopinIsVisible
				{
					get
					{
						return GetValue<bool>(() => _MyOwnLeadsPopinIsVisible);
					}
					set
					{
						SetValue<bool>(() => _MyOwnLeadsPopinIsVisible, (v) => _MyOwnLeadsPopinIsVisible = v, value, MyOwnLeadsPopinIsVisible_PROPERTYNAME,  DoMyOwnLeadsPopinIsVisibleBeforeSet, DoMyOwnLeadsPopinIsVisibleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoMyOwnLeadsPopinIsVisibleBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoMyOwnLeadsPopinIsVisibleAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyMyOwnLeadsPopinIsVisibleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : NavigateLeadHeaderTitle ===

				public const string NavigateLeadHeaderTitle_PROPERTYNAME = "NavigateLeadHeaderTitle";

				private string _NavigateLeadHeaderTitle;
				///<summary>
				/// Propriété : NavigateLeadHeaderTitle
				///</summary>
				public string NavigateLeadHeaderTitle
				{
					get
					{
						return GetValue<string>(() => _NavigateLeadHeaderTitle);
					}
					set
					{
						SetValue<string>(() => _NavigateLeadHeaderTitle, (v) => _NavigateLeadHeaderTitle = v, value, NavigateLeadHeaderTitle_PROPERTYNAME,  DoNavigateLeadHeaderTitleBeforeSet, DoNavigateLeadHeaderTitleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoNavigateLeadHeaderTitleBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoNavigateLeadHeaderTitleAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyNavigateLeadHeaderTitleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : PickerDateValue ===

				public const string PickerDateValue_PROPERTYNAME = "PickerDateValue";

				private string _PickerDateValue;
				///<summary>
				/// Propriété : PickerDateValue
				///</summary>
				public string PickerDateValue
				{
					get
					{
						return GetValue<string>(() => _PickerDateValue);
					}
					set
					{
						SetValue<string>(() => _PickerDateValue, (v) => _PickerDateValue = v, value, PickerDateValue_PROPERTYNAME,  DoPickerDateValueBeforeSet, DoPickerDateValueAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoPickerDateValueBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoPickerDateValueAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyPickerDateValueDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsAvailableShowroom ===

				public const string IsAvailableShowroom_PROPERTYNAME = "IsAvailableShowroom";

				private bool _IsAvailableShowroom;
				///<summary>
				/// Propriété : IsAvailableShowroom
				///</summary>
				public bool IsAvailableShowroom
				{
					get
					{
						return GetValue<bool>(() => _IsAvailableShowroom);
					}
					set
					{
						SetValue<bool>(() => _IsAvailableShowroom, (v) => _IsAvailableShowroom = v, value, IsAvailableShowroom_PROPERTYNAME,  DoIsAvailableShowroomBeforeSet, DoIsAvailableShowroomAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsAvailableShowroomBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsAvailableShowroomAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsAvailableShowroomDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FilterStyleRoot ===

				public const string FilterStyleRoot_PROPERTYNAME = "FilterStyleRoot";

				private string _FilterStyleRoot;
				///<summary>
				/// Propriété : FilterStyleRoot
				///</summary>
				public string FilterStyleRoot
				{
					get
					{
						return GetValue<string>(() => _FilterStyleRoot);
					}
					set
					{
						SetValue<string>(() => _FilterStyleRoot, (v) => _FilterStyleRoot = v, value, FilterStyleRoot_PROPERTYNAME,  DoFilterStyleRootBeforeSet, DoFilterStyleRootAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFilterStyleRootBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFilterStyleRootAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFilterStyleRootDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FilterStyleDetails ===

				public const string FilterStyleDetails_PROPERTYNAME = "FilterStyleDetails";

				private string _FilterStyleDetails;
				///<summary>
				/// Propriété : FilterStyleDetails
				///</summary>
				public string FilterStyleDetails
				{
					get
					{
						return GetValue<string>(() => _FilterStyleDetails);
					}
					set
					{
						SetValue<string>(() => _FilterStyleDetails, (v) => _FilterStyleDetails = v, value, FilterStyleDetails_PROPERTYNAME,  DoFilterStyleDetailsBeforeSet, DoFilterStyleDetailsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFilterStyleDetailsBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFilterStyleDetailsAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFilterStyleDetailsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FilterDoUpdate ===

				public const string FilterDoUpdate_PROPERTYNAME = "FilterDoUpdate";

				private bool _FilterDoUpdate;
				///<summary>
				/// Propriété : FilterDoUpdate
				///</summary>
				public bool FilterDoUpdate
				{
					get
					{
						return GetValue<bool>(() => _FilterDoUpdate);
					}
					set
					{
						SetValue<bool>(() => _FilterDoUpdate, (v) => _FilterDoUpdate = v, value, FilterDoUpdate_PROPERTYNAME,  DoFilterDoUpdateBeforeSet, DoFilterDoUpdateAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFilterDoUpdateBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFilterDoUpdateAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFilterDoUpdateDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectedLeadDetails ===

				public const string SelectedLeadDetails_PROPERTYNAME = "SelectedLeadDetails";

				private LeadsInfosItemViewModel _SelectedLeadDetails;
				///<summary>
				/// Propriété : SelectedLeadDetails
				///</summary>
				public LeadsInfosItemViewModel SelectedLeadDetails
				{
					get
					{
						return GetValue<LeadsInfosItemViewModel>(() => _SelectedLeadDetails);
					}
					set
					{
						SetValue<LeadsInfosItemViewModel>(() => _SelectedLeadDetails, (v) => _SelectedLeadDetails = v, value, SelectedLeadDetails_PROPERTYNAME,  DoSelectedLeadDetailsBeforeSet, DoSelectedLeadDetailsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectedLeadDetailsBeforeSet(string p_PropertyName, LeadsInfosItemViewModel p_OldValue, LeadsInfosItemViewModel p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectedLeadDetailsAfterSet(string p_PropertyName, LeadsInfosItemViewModel p_OldValue, LeadsInfosItemViewModel p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectedLeadDetailsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : PlaceHolderForLeadsPicker ===

				public const string PlaceHolderForLeadsPicker_PROPERTYNAME = "PlaceHolderForLeadsPicker";

				private string _PlaceHolderForLeadsPicker;
				///<summary>
				/// Propriété : PlaceHolderForLeadsPicker
				///</summary>
				public string PlaceHolderForLeadsPicker
				{
					get
					{
						return GetValue<string>(() => _PlaceHolderForLeadsPicker);
					}
					set
					{
						SetValue<string>(() => _PlaceHolderForLeadsPicker, (v) => _PlaceHolderForLeadsPicker = v, value, PlaceHolderForLeadsPicker_PROPERTYNAME,  DoPlaceHolderForLeadsPickerBeforeSet, DoPlaceHolderForLeadsPickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoPlaceHolderForLeadsPickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoPlaceHolderForLeadsPickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyPlaceHolderForLeadsPickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : HeaderTextPicker ===

				public const string HeaderTextPicker_PROPERTYNAME = "HeaderTextPicker";

				private string _HeaderTextPicker;
				///<summary>
				/// Propriété : HeaderTextPicker
				///</summary>
				public string HeaderTextPicker
				{
					get
					{
						return GetValue<string>(() => _HeaderTextPicker);
					}
					set
					{
						SetValue<string>(() => _HeaderTextPicker, (v) => _HeaderTextPicker = v, value, HeaderTextPicker_PROPERTYNAME,  DoHeaderTextPickerBeforeSet, DoHeaderTextPickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoHeaderTextPickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoHeaderTextPickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyHeaderTextPickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LeadsCollection ===

				public const string LeadsCollection_PROPERTYNAME = "LeadsCollection";

				private System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> _LeadsCollection;
				///<summary>
				/// Propriété : LeadsCollection
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> LeadsCollection
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _LeadsCollection);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _LeadsCollection, (v) => _LeadsCollection = v, value, LeadsCollection_PROPERTYNAME,  DoLeadsCollectionBeforeSet, DoLeadsCollectionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLeadsCollectionBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionLeadsCollection_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLeadsCollectionAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionLeadsCollection_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLeadsCollectionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LeadsForFilterCollection ===

				public const string LeadsForFilterCollection_PROPERTYNAME = "LeadsForFilterCollection";

				private System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> _LeadsForFilterCollection;
				///<summary>
				/// Propriété : LeadsForFilterCollection
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> LeadsForFilterCollection
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _LeadsForFilterCollection);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _LeadsForFilterCollection, (v) => _LeadsForFilterCollection = v, value, LeadsForFilterCollection_PROPERTYNAME,  DoLeadsForFilterCollectionBeforeSet, DoLeadsForFilterCollectionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLeadsForFilterCollectionBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionLeadsForFilterCollection_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLeadsForFilterCollectionAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionLeadsForFilterCollection_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLeadsForFilterCollectionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : OnUseLeadsCollection ===

				public const string OnUseLeadsCollection_PROPERTYNAME = "OnUseLeadsCollection";

				private System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> _OnUseLeadsCollection;
				///<summary>
				/// Propriété : OnUseLeadsCollection
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> OnUseLeadsCollection
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _OnUseLeadsCollection);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _OnUseLeadsCollection, (v) => _OnUseLeadsCollection = v, value, OnUseLeadsCollection_PROPERTYNAME,  DoOnUseLeadsCollectionBeforeSet, DoOnUseLeadsCollectionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoOnUseLeadsCollectionBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionOnUseLeadsCollection_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoOnUseLeadsCollectionAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionOnUseLeadsCollection_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyOnUseLeadsCollectionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : OnUseDayLeadsCollection ===

				public const string OnUseDayLeadsCollection_PROPERTYNAME = "OnUseDayLeadsCollection";

				private System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> _OnUseDayLeadsCollection;
				///<summary>
				/// Propriété : OnUseDayLeadsCollection
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> OnUseDayLeadsCollection
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _OnUseDayLeadsCollection);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _OnUseDayLeadsCollection, (v) => _OnUseDayLeadsCollection = v, value, OnUseDayLeadsCollection_PROPERTYNAME,  DoOnUseDayLeadsCollectionBeforeSet, DoOnUseDayLeadsCollectionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoOnUseDayLeadsCollectionBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionOnUseDayLeadsCollection_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoOnUseDayLeadsCollectionAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionOnUseDayLeadsCollection_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyOnUseDayLeadsCollectionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SaveOnUseDayLeadsCollection ===

				public const string SaveOnUseDayLeadsCollection_PROPERTYNAME = "SaveOnUseDayLeadsCollection";

				private System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> _SaveOnUseDayLeadsCollection;
				///<summary>
				/// Propriété : SaveOnUseDayLeadsCollection
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> SaveOnUseDayLeadsCollection
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _SaveOnUseDayLeadsCollection);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _SaveOnUseDayLeadsCollection, (v) => _SaveOnUseDayLeadsCollection = v, value, SaveOnUseDayLeadsCollection_PROPERTYNAME,  DoSaveOnUseDayLeadsCollectionBeforeSet, DoSaveOnUseDayLeadsCollectionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSaveOnUseDayLeadsCollectionBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionSaveOnUseDayLeadsCollection_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSaveOnUseDayLeadsCollectionAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionSaveOnUseDayLeadsCollection_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySaveOnUseDayLeadsCollectionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LeadsOfTheDateForFilterCollection ===

				public const string LeadsOfTheDateForFilterCollection_PROPERTYNAME = "LeadsOfTheDateForFilterCollection";

				private System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> _LeadsOfTheDateForFilterCollection;
				///<summary>
				/// Propriété : LeadsOfTheDateForFilterCollection
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> LeadsOfTheDateForFilterCollection
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _LeadsOfTheDateForFilterCollection);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _LeadsOfTheDateForFilterCollection, (v) => _LeadsOfTheDateForFilterCollection = v, value, LeadsOfTheDateForFilterCollection_PROPERTYNAME,  DoLeadsOfTheDateForFilterCollectionBeforeSet, DoLeadsOfTheDateForFilterCollectionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLeadsOfTheDateForFilterCollectionBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionLeadsOfTheDateForFilterCollection_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLeadsOfTheDateForFilterCollectionAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionLeadsOfTheDateForFilterCollection_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLeadsOfTheDateForFilterCollectionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : OnUseOthersDayLeadsCollection ===

				public const string OnUseOthersDayLeadsCollection_PROPERTYNAME = "OnUseOthersDayLeadsCollection";

				private System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> _OnUseOthersDayLeadsCollection;
				///<summary>
				/// Propriété : OnUseOthersDayLeadsCollection
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> OnUseOthersDayLeadsCollection
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _OnUseOthersDayLeadsCollection);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _OnUseOthersDayLeadsCollection, (v) => _OnUseOthersDayLeadsCollection = v, value, OnUseOthersDayLeadsCollection_PROPERTYNAME,  DoOnUseOthersDayLeadsCollectionBeforeSet, DoOnUseOthersDayLeadsCollectionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoOnUseOthersDayLeadsCollectionBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionOnUseOthersDayLeadsCollection_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoOnUseOthersDayLeadsCollectionAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionOnUseOthersDayLeadsCollection_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyOnUseOthersDayLeadsCollectionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SaveOnUseOthersDayLeadsCollection ===

				public const string SaveOnUseOthersDayLeadsCollection_PROPERTYNAME = "SaveOnUseOthersDayLeadsCollection";

				private System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> _SaveOnUseOthersDayLeadsCollection;
				///<summary>
				/// Propriété : SaveOnUseOthersDayLeadsCollection
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> SaveOnUseOthersDayLeadsCollection
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _SaveOnUseOthersDayLeadsCollection);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _SaveOnUseOthersDayLeadsCollection, (v) => _SaveOnUseOthersDayLeadsCollection = v, value, SaveOnUseOthersDayLeadsCollection_PROPERTYNAME,  DoSaveOnUseOthersDayLeadsCollectionBeforeSet, DoSaveOnUseOthersDayLeadsCollectionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSaveOnUseOthersDayLeadsCollectionBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionSaveOnUseOthersDayLeadsCollection_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSaveOnUseOthersDayLeadsCollectionAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionSaveOnUseOthersDayLeadsCollection_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySaveOnUseOthersDayLeadsCollectionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LeadsOnGoingForFilterCollection ===

				public const string LeadsOnGoingForFilterCollection_PROPERTYNAME = "LeadsOnGoingForFilterCollection";

				private System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> _LeadsOnGoingForFilterCollection;
				///<summary>
				/// Propriété : LeadsOnGoingForFilterCollection
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> LeadsOnGoingForFilterCollection
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _LeadsOnGoingForFilterCollection);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel>>(() => _LeadsOnGoingForFilterCollection, (v) => _LeadsOnGoingForFilterCollection = v, value, LeadsOnGoingForFilterCollection_PROPERTYNAME,  DoLeadsOnGoingForFilterCollectionBeforeSet, DoLeadsOnGoingForFilterCollectionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLeadsOnGoingForFilterCollectionBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionLeadsOnGoingForFilterCollection_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLeadsOnGoingForFilterCollectionAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionLeadsOnGoingForFilterCollection_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLeadsOnGoingForFilterCollectionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UsersCollections ===

				public const string UsersCollections_PROPERTYNAME = "UsersCollections";

				private System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> _UsersCollections;
				///<summary>
				/// Propriété : UsersCollections
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> UsersCollections
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos>>(() => _UsersCollections);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos>>(() => _UsersCollections, (v) => _UsersCollections = v, value, UsersCollections_PROPERTYNAME,  DoUsersCollectionsBeforeSet, DoUsersCollectionsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUsersCollectionsBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionUsersCollections_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUsersCollectionsAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionUsersCollections_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUsersCollectionsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ManagedShowRoomLeadCollections ===

				public const string ManagedShowRoomLeadCollections_PROPERTYNAME = "ManagedShowRoomLeadCollections";

				private System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ManagedShowRoomLead> _ManagedShowRoomLeadCollections;
				///<summary>
				/// Propriété : ManagedShowRoomLeadCollections
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ManagedShowRoomLead> ManagedShowRoomLeadCollections
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ManagedShowRoomLead>>(() => _ManagedShowRoomLeadCollections);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ManagedShowRoomLead>>(() => _ManagedShowRoomLeadCollections, (v) => _ManagedShowRoomLeadCollections = v, value, ManagedShowRoomLeadCollections_PROPERTYNAME,  DoManagedShowRoomLeadCollectionsBeforeSet, DoManagedShowRoomLeadCollectionsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoManagedShowRoomLeadCollectionsBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ManagedShowRoomLead> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ManagedShowRoomLead> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionManagedShowRoomLeadCollections_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoManagedShowRoomLeadCollectionsAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ManagedShowRoomLead> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ManagedShowRoomLead> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionManagedShowRoomLeadCollections_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyManagedShowRoomLeadCollectionsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LeadFilterModelsCollections ===

				public const string LeadFilterModelsCollections_PROPERTYNAME = "LeadFilterModelsCollections";

				private System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.LeadFilterModels> _LeadFilterModelsCollections;
				///<summary>
				/// Propriété : LeadFilterModelsCollections
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.LeadFilterModels> LeadFilterModelsCollections
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.LeadFilterModels>>(() => _LeadFilterModelsCollections);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.LeadFilterModels>>(() => _LeadFilterModelsCollections, (v) => _LeadFilterModelsCollections = v, value, LeadFilterModelsCollections_PROPERTYNAME,  DoLeadFilterModelsCollectionsBeforeSet, DoLeadFilterModelsCollectionsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLeadFilterModelsCollectionsBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.LeadFilterModels> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.LeadFilterModels> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionLeadFilterModelsCollections_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLeadFilterModelsCollectionsAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.LeadFilterModels> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.LeadFilterModels> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionLeadFilterModelsCollections_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLeadFilterModelsCollectionsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowAllocatedLeadCommand ===

				public const string ShowAllocatedLeadCommand_PROPERTYNAME = "ShowAllocatedLeadCommand";

				private IDelegateCommand _ShowAllocatedLeadCommand;
				///<summary>
				/// Propriété : ShowAllocatedLeadCommand
				///</summary>
				public IDelegateCommand ShowAllocatedLeadCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowAllocatedLeadCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowAllocatedLeadCommand, (v) => _ShowAllocatedLeadCommand = v, value, ShowAllocatedLeadCommand_PROPERTYNAME,  DoShowAllocatedLeadCommandBeforeSet, DoShowAllocatedLeadCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowAllocatedLeadCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowAllocatedLeadCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowAllocatedLeadCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowLeadDetailsCommand ===

				public const string ShowLeadDetailsCommand_PROPERTYNAME = "ShowLeadDetailsCommand";

				private IDelegateCommand _ShowLeadDetailsCommand;
				///<summary>
				/// Propriété : ShowLeadDetailsCommand
				///</summary>
				public IDelegateCommand ShowLeadDetailsCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowLeadDetailsCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowLeadDetailsCommand, (v) => _ShowLeadDetailsCommand = v, value, ShowLeadDetailsCommand_PROPERTYNAME,  DoShowLeadDetailsCommandBeforeSet, DoShowLeadDetailsCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowLeadDetailsCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowLeadDetailsCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowLeadDetailsCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowMyLeadDetailsCommand ===

				public const string ShowMyLeadDetailsCommand_PROPERTYNAME = "ShowMyLeadDetailsCommand";

				private IDelegateCommand _ShowMyLeadDetailsCommand;
				///<summary>
				/// Propriété : ShowMyLeadDetailsCommand
				///</summary>
				public IDelegateCommand ShowMyLeadDetailsCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowMyLeadDetailsCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowMyLeadDetailsCommand, (v) => _ShowMyLeadDetailsCommand = v, value, ShowMyLeadDetailsCommand_PROPERTYNAME,  DoShowMyLeadDetailsCommandBeforeSet, DoShowMyLeadDetailsCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowMyLeadDetailsCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowMyLeadDetailsCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowMyLeadDetailsCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectRepresentativeCommand ===

				public const string SelectRepresentativeCommand_PROPERTYNAME = "SelectRepresentativeCommand";

				private IDelegateCommand _SelectRepresentativeCommand;
				///<summary>
				/// Propriété : SelectRepresentativeCommand
				///</summary>
				public IDelegateCommand SelectRepresentativeCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _SelectRepresentativeCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _SelectRepresentativeCommand, (v) => _SelectRepresentativeCommand = v, value, SelectRepresentativeCommand_PROPERTYNAME,  DoSelectRepresentativeCommandBeforeSet, DoSelectRepresentativeCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectRepresentativeCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectRepresentativeCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectRepresentativeCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : AddOrRemoveAffectationCommand ===

				public const string AddOrRemoveAffectationCommand_PROPERTYNAME = "AddOrRemoveAffectationCommand";

				private IDelegateCommand _AddOrRemoveAffectationCommand;
				///<summary>
				/// Propriété : AddOrRemoveAffectationCommand
				///</summary>
				public IDelegateCommand AddOrRemoveAffectationCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _AddOrRemoveAffectationCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _AddOrRemoveAffectationCommand, (v) => _AddOrRemoveAffectationCommand = v, value, AddOrRemoveAffectationCommand_PROPERTYNAME,  DoAddOrRemoveAffectationCommandBeforeSet, DoAddOrRemoveAffectationCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoAddOrRemoveAffectationCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoAddOrRemoveAffectationCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyAddOrRemoveAffectationCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SetLeadStatusCommand ===

				public const string SetLeadStatusCommand_PROPERTYNAME = "SetLeadStatusCommand";

				private IDelegateCommand _SetLeadStatusCommand;
				///<summary>
				/// Propriété : SetLeadStatusCommand
				///</summary>
				public IDelegateCommand SetLeadStatusCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _SetLeadStatusCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _SetLeadStatusCommand, (v) => _SetLeadStatusCommand = v, value, SetLeadStatusCommand_PROPERTYNAME,  DoSetLeadStatusCommandBeforeSet, DoSetLeadStatusCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSetLeadStatusCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSetLeadStatusCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySetLeadStatusCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : AllocateCommand ===

				public const string AllocateCommand_PROPERTYNAME = "AllocateCommand";

				private IDelegateCommand _AllocateCommand;
				///<summary>
				/// Propriété : AllocateCommand
				///</summary>
				public IDelegateCommand AllocateCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _AllocateCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _AllocateCommand, (v) => _AllocateCommand = v, value, AllocateCommand_PROPERTYNAME,  DoAllocateCommandBeforeSet, DoAllocateCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoAllocateCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoAllocateCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyAllocateCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LiveChangeLeadStatusOnDoneCommand ===

				public const string LiveChangeLeadStatusOnDoneCommand_PROPERTYNAME = "LiveChangeLeadStatusOnDoneCommand";

				private IDelegateCommand _LiveChangeLeadStatusOnDoneCommand;
				///<summary>
				/// Propriété : LiveChangeLeadStatusOnDoneCommand
				///</summary>
				public IDelegateCommand LiveChangeLeadStatusOnDoneCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _LiveChangeLeadStatusOnDoneCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _LiveChangeLeadStatusOnDoneCommand, (v) => _LiveChangeLeadStatusOnDoneCommand = v, value, LiveChangeLeadStatusOnDoneCommand_PROPERTYNAME,  DoLiveChangeLeadStatusOnDoneCommandBeforeSet, DoLiveChangeLeadStatusOnDoneCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLiveChangeLeadStatusOnDoneCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLiveChangeLeadStatusOnDoneCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLiveChangeLeadStatusOnDoneCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LiveChangeLeadStatusOnRecontactCommand ===

				public const string LiveChangeLeadStatusOnRecontactCommand_PROPERTYNAME = "LiveChangeLeadStatusOnRecontactCommand";

				private IDelegateCommand _LiveChangeLeadStatusOnRecontactCommand;
				///<summary>
				/// Propriété : LiveChangeLeadStatusOnRecontactCommand
				///</summary>
				public IDelegateCommand LiveChangeLeadStatusOnRecontactCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _LiveChangeLeadStatusOnRecontactCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _LiveChangeLeadStatusOnRecontactCommand, (v) => _LiveChangeLeadStatusOnRecontactCommand = v, value, LiveChangeLeadStatusOnRecontactCommand_PROPERTYNAME,  DoLiveChangeLeadStatusOnRecontactCommandBeforeSet, DoLiveChangeLeadStatusOnRecontactCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLiveChangeLeadStatusOnRecontactCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLiveChangeLeadStatusOnRecontactCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLiveChangeLeadStatusOnRecontactCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LiveChangeLeadStatusOnNotInterestCommand ===

				public const string LiveChangeLeadStatusOnNotInterestCommand_PROPERTYNAME = "LiveChangeLeadStatusOnNotInterestCommand";

				private IDelegateCommand _LiveChangeLeadStatusOnNotInterestCommand;
				///<summary>
				/// Propriété : LiveChangeLeadStatusOnNotInterestCommand
				///</summary>
				public IDelegateCommand LiveChangeLeadStatusOnNotInterestCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _LiveChangeLeadStatusOnNotInterestCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _LiveChangeLeadStatusOnNotInterestCommand, (v) => _LiveChangeLeadStatusOnNotInterestCommand = v, value, LiveChangeLeadStatusOnNotInterestCommand_PROPERTYNAME,  DoLiveChangeLeadStatusOnNotInterestCommandBeforeSet, DoLiveChangeLeadStatusOnNotInterestCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLiveChangeLeadStatusOnNotInterestCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLiveChangeLeadStatusOnNotInterestCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLiveChangeLeadStatusOnNotInterestCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SimpleUserAllocateLeadCommand ===

				public const string SimpleUserAllocateLeadCommand_PROPERTYNAME = "SimpleUserAllocateLeadCommand";

				private IDelegateCommand _SimpleUserAllocateLeadCommand;
				///<summary>
				/// Propriété : SimpleUserAllocateLeadCommand
				///</summary>
				public IDelegateCommand SimpleUserAllocateLeadCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _SimpleUserAllocateLeadCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _SimpleUserAllocateLeadCommand, (v) => _SimpleUserAllocateLeadCommand = v, value, SimpleUserAllocateLeadCommand_PROPERTYNAME,  DoSimpleUserAllocateLeadCommandBeforeSet, DoSimpleUserAllocateLeadCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSimpleUserAllocateLeadCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSimpleUserAllocateLeadCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySimpleUserAllocateLeadCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowMyLeadsCommand ===

				public const string ShowMyLeadsCommand_PROPERTYNAME = "ShowMyLeadsCommand";

				private IDelegateCommand _ShowMyLeadsCommand;
				///<summary>
				/// Propriété : ShowMyLeadsCommand
				///</summary>
				public IDelegateCommand ShowMyLeadsCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowMyLeadsCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowMyLeadsCommand, (v) => _ShowMyLeadsCommand = v, value, ShowMyLeadsCommand_PROPERTYNAME,  DoShowMyLeadsCommandBeforeSet, DoShowMyLeadsCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowMyLeadsCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowMyLeadsCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowMyLeadsCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : NextCommand ===

				public const string NextCommand_PROPERTYNAME = "NextCommand";

				private IDelegateCommand _NextCommand;
				///<summary>
				/// Propriété : NextCommand
				///</summary>
				public IDelegateCommand NextCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _NextCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _NextCommand, (v) => _NextCommand = v, value, NextCommand_PROPERTYNAME,  DoNextCommandBeforeSet, DoNextCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoNextCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoNextCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyNextCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : BackCommand ===

				public const string BackCommand_PROPERTYNAME = "BackCommand";

				private IDelegateCommand _BackCommand;
				///<summary>
				/// Propriété : BackCommand
				///</summary>
				public IDelegateCommand BackCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _BackCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _BackCommand, (v) => _BackCommand = v, value, BackCommand_PROPERTYNAME,  DoBackCommandBeforeSet, DoBackCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoBackCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoBackCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyBackCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DoFilterCommand ===

				public const string DoFilterCommand_PROPERTYNAME = "DoFilterCommand";

				private IDelegateCommand _DoFilterCommand;
				///<summary>
				/// Propriété : DoFilterCommand
				///</summary>
				public IDelegateCommand DoFilterCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _DoFilterCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _DoFilterCommand, (v) => _DoFilterCommand = v, value, DoFilterCommand_PROPERTYNAME,  DoDoFilterCommandBeforeSet, DoDoFilterCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDoFilterCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDoFilterCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDoFilterCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : MyOwnLeadsPopinVisibilityCommand ===

				public const string MyOwnLeadsPopinVisibilityCommand_PROPERTYNAME = "MyOwnLeadsPopinVisibilityCommand";

				private IDelegateCommand _MyOwnLeadsPopinVisibilityCommand;
				///<summary>
				/// Propriété : MyOwnLeadsPopinVisibilityCommand
				///</summary>
				public IDelegateCommand MyOwnLeadsPopinVisibilityCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _MyOwnLeadsPopinVisibilityCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _MyOwnLeadsPopinVisibilityCommand, (v) => _MyOwnLeadsPopinVisibilityCommand = v, value, MyOwnLeadsPopinVisibilityCommand_PROPERTYNAME,  DoMyOwnLeadsPopinVisibilityCommandBeforeSet, DoMyOwnLeadsPopinVisibilityCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoMyOwnLeadsPopinVisibilityCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoMyOwnLeadsPopinVisibilityCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyMyOwnLeadsPopinVisibilityCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LoadMoreCommand ===

				public const string LoadMoreCommand_PROPERTYNAME = "LoadMoreCommand";

				private IDelegateCommand _LoadMoreCommand;
				///<summary>
				/// Propriété : LoadMoreCommand
				///</summary>
				public IDelegateCommand LoadMoreCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _LoadMoreCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _LoadMoreCommand, (v) => _LoadMoreCommand = v, value, LoadMoreCommand_PROPERTYNAME,  DoLoadMoreCommandBeforeSet, DoLoadMoreCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLoadMoreCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLoadMoreCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLoadMoreCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LoadMoreLeadOfTheDateCommand ===

				public const string LoadMoreLeadOfTheDateCommand_PROPERTYNAME = "LoadMoreLeadOfTheDateCommand";

				private IDelegateCommand _LoadMoreLeadOfTheDateCommand;
				///<summary>
				/// Propriété : LoadMoreLeadOfTheDateCommand
				///</summary>
				public IDelegateCommand LoadMoreLeadOfTheDateCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _LoadMoreLeadOfTheDateCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _LoadMoreLeadOfTheDateCommand, (v) => _LoadMoreLeadOfTheDateCommand = v, value, LoadMoreLeadOfTheDateCommand_PROPERTYNAME,  DoLoadMoreLeadOfTheDateCommandBeforeSet, DoLoadMoreLeadOfTheDateCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLoadMoreLeadOfTheDateCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLoadMoreLeadOfTheDateCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLoadMoreLeadOfTheDateCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LoadMoreLeadOnGoingCommand ===

				public const string LoadMoreLeadOnGoingCommand_PROPERTYNAME = "LoadMoreLeadOnGoingCommand";

				private IDelegateCommand _LoadMoreLeadOnGoingCommand;
				///<summary>
				/// Propriété : LoadMoreLeadOnGoingCommand
				///</summary>
				public IDelegateCommand LoadMoreLeadOnGoingCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _LoadMoreLeadOnGoingCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _LoadMoreLeadOnGoingCommand, (v) => _LoadMoreLeadOnGoingCommand = v, value, LoadMoreLeadOnGoingCommand_PROPERTYNAME,  DoLoadMoreLeadOnGoingCommandBeforeSet, DoLoadMoreLeadOnGoingCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLoadMoreLeadOnGoingCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLoadMoreLeadOnGoingCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLoadMoreLeadOnGoingCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CloseFilterViewAndLoadDataCommand ===

				public const string CloseFilterViewAndLoadDataCommand_PROPERTYNAME = "CloseFilterViewAndLoadDataCommand";

				private IDelegateCommand _CloseFilterViewAndLoadDataCommand;
				///<summary>
				/// Propriété : CloseFilterViewAndLoadDataCommand
				///</summary>
				public IDelegateCommand CloseFilterViewAndLoadDataCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _CloseFilterViewAndLoadDataCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _CloseFilterViewAndLoadDataCommand, (v) => _CloseFilterViewAndLoadDataCommand = v, value, CloseFilterViewAndLoadDataCommand_PROPERTYNAME,  DoCloseFilterViewAndLoadDataCommandBeforeSet, DoCloseFilterViewAndLoadDataCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCloseFilterViewAndLoadDataCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCloseFilterViewAndLoadDataCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCloseFilterViewAndLoadDataCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux collections ===

			// Notez que les propriétés de collection sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Collection : LeadsCollection ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection LeadsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionLeadsCollection_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionLeadsCollection_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionLeadsCollection_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection LeadsCollection (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionLeadsCollection_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionLeadsCollection_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionLeadsCollection_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection LeadsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionLeadsCollection_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionLeadsCollection_ItemPropertyChanged;
							OnLeadsCollectionItemAdded((LeadsItemViewModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection LeadsCollection (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionLeadsCollection_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnLeadsCollectionItemRemoved((LeadsItemViewModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionLeadsCollection_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionLeadsCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionLeadsCollection_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionLeadsCollection_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LeadsCollection_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyLeadsCollectionDependencies();
				}

				private void WatchCollectionLeadsCollection_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LeadsCollection_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyLeadsCollectionDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection LeadsCollection.
				///</summary>
				protected virtual void OnLeadsCollectionItemAdded(LeadsItemViewModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection LeadsCollection.
				///</summary>
				protected virtual void OnLeadsCollectionItemRemoved(LeadsItemViewModel p_Item)
				{
				}

			#endregion

			#region === Collection : LeadsForFilterCollection ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection LeadsForFilterCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionLeadsForFilterCollection_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionLeadsForFilterCollection_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionLeadsForFilterCollection_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection LeadsForFilterCollection (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionLeadsForFilterCollection_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionLeadsForFilterCollection_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionLeadsForFilterCollection_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection LeadsForFilterCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionLeadsForFilterCollection_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionLeadsForFilterCollection_ItemPropertyChanged;
							OnLeadsForFilterCollectionItemAdded((LeadsItemViewModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection LeadsForFilterCollection (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionLeadsForFilterCollection_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnLeadsForFilterCollectionItemRemoved((LeadsItemViewModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionLeadsForFilterCollection_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionLeadsForFilterCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionLeadsForFilterCollection_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionLeadsForFilterCollection_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LeadsForFilterCollection_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyLeadsForFilterCollectionDependencies();
				}

				private void WatchCollectionLeadsForFilterCollection_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LeadsForFilterCollection_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyLeadsForFilterCollectionDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection LeadsForFilterCollection.
				///</summary>
				protected virtual void OnLeadsForFilterCollectionItemAdded(LeadsItemViewModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection LeadsForFilterCollection.
				///</summary>
				protected virtual void OnLeadsForFilterCollectionItemRemoved(LeadsItemViewModel p_Item)
				{
				}

			#endregion

			#region === Collection : OnUseLeadsCollection ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection OnUseLeadsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionOnUseLeadsCollection_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionOnUseLeadsCollection_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionOnUseLeadsCollection_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection OnUseLeadsCollection (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionOnUseLeadsCollection_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionOnUseLeadsCollection_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionOnUseLeadsCollection_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection OnUseLeadsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionOnUseLeadsCollection_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionOnUseLeadsCollection_ItemPropertyChanged;
							OnOnUseLeadsCollectionItemAdded((LeadsItemViewModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection OnUseLeadsCollection (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionOnUseLeadsCollection_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnOnUseLeadsCollectionItemRemoved((LeadsItemViewModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionOnUseLeadsCollection_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionOnUseLeadsCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionOnUseLeadsCollection_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionOnUseLeadsCollection_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(OnUseLeadsCollection_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyOnUseLeadsCollectionDependencies();
				}

				private void WatchCollectionOnUseLeadsCollection_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(OnUseLeadsCollection_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyOnUseLeadsCollectionDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection OnUseLeadsCollection.
				///</summary>
				protected virtual void OnOnUseLeadsCollectionItemAdded(LeadsItemViewModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection OnUseLeadsCollection.
				///</summary>
				protected virtual void OnOnUseLeadsCollectionItemRemoved(LeadsItemViewModel p_Item)
				{
				}

			#endregion

			#region === Collection : OnUseDayLeadsCollection ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection OnUseDayLeadsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionOnUseDayLeadsCollection_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionOnUseDayLeadsCollection_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionOnUseDayLeadsCollection_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection OnUseDayLeadsCollection (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionOnUseDayLeadsCollection_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionOnUseDayLeadsCollection_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionOnUseDayLeadsCollection_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection OnUseDayLeadsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionOnUseDayLeadsCollection_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionOnUseDayLeadsCollection_ItemPropertyChanged;
							OnOnUseDayLeadsCollectionItemAdded((LeadsItemViewModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection OnUseDayLeadsCollection (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionOnUseDayLeadsCollection_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnOnUseDayLeadsCollectionItemRemoved((LeadsItemViewModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionOnUseDayLeadsCollection_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionOnUseDayLeadsCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionOnUseDayLeadsCollection_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionOnUseDayLeadsCollection_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(OnUseDayLeadsCollection_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyOnUseDayLeadsCollectionDependencies();
				}

				private void WatchCollectionOnUseDayLeadsCollection_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(OnUseDayLeadsCollection_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyOnUseDayLeadsCollectionDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection OnUseDayLeadsCollection.
				///</summary>
				protected virtual void OnOnUseDayLeadsCollectionItemAdded(LeadsItemViewModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection OnUseDayLeadsCollection.
				///</summary>
				protected virtual void OnOnUseDayLeadsCollectionItemRemoved(LeadsItemViewModel p_Item)
				{
				}

			#endregion

			#region === Collection : SaveOnUseDayLeadsCollection ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection SaveOnUseDayLeadsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionSaveOnUseDayLeadsCollection_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionSaveOnUseDayLeadsCollection_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionSaveOnUseDayLeadsCollection_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection SaveOnUseDayLeadsCollection (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionSaveOnUseDayLeadsCollection_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionSaveOnUseDayLeadsCollection_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionSaveOnUseDayLeadsCollection_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection SaveOnUseDayLeadsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionSaveOnUseDayLeadsCollection_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionSaveOnUseDayLeadsCollection_ItemPropertyChanged;
							OnSaveOnUseDayLeadsCollectionItemAdded((LeadsItemViewModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection SaveOnUseDayLeadsCollection (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionSaveOnUseDayLeadsCollection_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnSaveOnUseDayLeadsCollectionItemRemoved((LeadsItemViewModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionSaveOnUseDayLeadsCollection_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionSaveOnUseDayLeadsCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionSaveOnUseDayLeadsCollection_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionSaveOnUseDayLeadsCollection_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(SaveOnUseDayLeadsCollection_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifySaveOnUseDayLeadsCollectionDependencies();
				}

				private void WatchCollectionSaveOnUseDayLeadsCollection_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(SaveOnUseDayLeadsCollection_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifySaveOnUseDayLeadsCollectionDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection SaveOnUseDayLeadsCollection.
				///</summary>
				protected virtual void OnSaveOnUseDayLeadsCollectionItemAdded(LeadsItemViewModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection SaveOnUseDayLeadsCollection.
				///</summary>
				protected virtual void OnSaveOnUseDayLeadsCollectionItemRemoved(LeadsItemViewModel p_Item)
				{
				}

			#endregion

			#region === Collection : LeadsOfTheDateForFilterCollection ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection LeadsOfTheDateForFilterCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionLeadsOfTheDateForFilterCollection_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionLeadsOfTheDateForFilterCollection_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionLeadsOfTheDateForFilterCollection_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection LeadsOfTheDateForFilterCollection (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionLeadsOfTheDateForFilterCollection_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionLeadsOfTheDateForFilterCollection_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionLeadsOfTheDateForFilterCollection_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection LeadsOfTheDateForFilterCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionLeadsOfTheDateForFilterCollection_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionLeadsOfTheDateForFilterCollection_ItemPropertyChanged;
							OnLeadsOfTheDateForFilterCollectionItemAdded((LeadsItemViewModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection LeadsOfTheDateForFilterCollection (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionLeadsOfTheDateForFilterCollection_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnLeadsOfTheDateForFilterCollectionItemRemoved((LeadsItemViewModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionLeadsOfTheDateForFilterCollection_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionLeadsOfTheDateForFilterCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionLeadsOfTheDateForFilterCollection_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionLeadsOfTheDateForFilterCollection_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LeadsOfTheDateForFilterCollection_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyLeadsOfTheDateForFilterCollectionDependencies();
				}

				private void WatchCollectionLeadsOfTheDateForFilterCollection_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LeadsOfTheDateForFilterCollection_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyLeadsOfTheDateForFilterCollectionDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection LeadsOfTheDateForFilterCollection.
				///</summary>
				protected virtual void OnLeadsOfTheDateForFilterCollectionItemAdded(LeadsItemViewModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection LeadsOfTheDateForFilterCollection.
				///</summary>
				protected virtual void OnLeadsOfTheDateForFilterCollectionItemRemoved(LeadsItemViewModel p_Item)
				{
				}

			#endregion

			#region === Collection : OnUseOthersDayLeadsCollection ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection OnUseOthersDayLeadsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionOnUseOthersDayLeadsCollection_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionOnUseOthersDayLeadsCollection_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionOnUseOthersDayLeadsCollection_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection OnUseOthersDayLeadsCollection (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionOnUseOthersDayLeadsCollection_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionOnUseOthersDayLeadsCollection_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionOnUseOthersDayLeadsCollection_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection OnUseOthersDayLeadsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionOnUseOthersDayLeadsCollection_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionOnUseOthersDayLeadsCollection_ItemPropertyChanged;
							OnOnUseOthersDayLeadsCollectionItemAdded((LeadsItemViewModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection OnUseOthersDayLeadsCollection (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionOnUseOthersDayLeadsCollection_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnOnUseOthersDayLeadsCollectionItemRemoved((LeadsItemViewModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionOnUseOthersDayLeadsCollection_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionOnUseOthersDayLeadsCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionOnUseOthersDayLeadsCollection_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionOnUseOthersDayLeadsCollection_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(OnUseOthersDayLeadsCollection_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyOnUseOthersDayLeadsCollectionDependencies();
				}

				private void WatchCollectionOnUseOthersDayLeadsCollection_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(OnUseOthersDayLeadsCollection_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyOnUseOthersDayLeadsCollectionDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection OnUseOthersDayLeadsCollection.
				///</summary>
				protected virtual void OnOnUseOthersDayLeadsCollectionItemAdded(LeadsItemViewModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection OnUseOthersDayLeadsCollection.
				///</summary>
				protected virtual void OnOnUseOthersDayLeadsCollectionItemRemoved(LeadsItemViewModel p_Item)
				{
				}

			#endregion

			#region === Collection : SaveOnUseOthersDayLeadsCollection ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection SaveOnUseOthersDayLeadsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionSaveOnUseOthersDayLeadsCollection_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionSaveOnUseOthersDayLeadsCollection_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionSaveOnUseOthersDayLeadsCollection_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection SaveOnUseOthersDayLeadsCollection (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionSaveOnUseOthersDayLeadsCollection_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionSaveOnUseOthersDayLeadsCollection_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionSaveOnUseOthersDayLeadsCollection_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection SaveOnUseOthersDayLeadsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionSaveOnUseOthersDayLeadsCollection_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionSaveOnUseOthersDayLeadsCollection_ItemPropertyChanged;
							OnSaveOnUseOthersDayLeadsCollectionItemAdded((LeadsItemViewModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection SaveOnUseOthersDayLeadsCollection (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionSaveOnUseOthersDayLeadsCollection_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnSaveOnUseOthersDayLeadsCollectionItemRemoved((LeadsItemViewModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionSaveOnUseOthersDayLeadsCollection_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionSaveOnUseOthersDayLeadsCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionSaveOnUseOthersDayLeadsCollection_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionSaveOnUseOthersDayLeadsCollection_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(SaveOnUseOthersDayLeadsCollection_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifySaveOnUseOthersDayLeadsCollectionDependencies();
				}

				private void WatchCollectionSaveOnUseOthersDayLeadsCollection_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(SaveOnUseOthersDayLeadsCollection_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifySaveOnUseOthersDayLeadsCollectionDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection SaveOnUseOthersDayLeadsCollection.
				///</summary>
				protected virtual void OnSaveOnUseOthersDayLeadsCollectionItemAdded(LeadsItemViewModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection SaveOnUseOthersDayLeadsCollection.
				///</summary>
				protected virtual void OnSaveOnUseOthersDayLeadsCollectionItemRemoved(LeadsItemViewModel p_Item)
				{
				}

			#endregion

			#region === Collection : LeadsOnGoingForFilterCollection ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection LeadsOnGoingForFilterCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionLeadsOnGoingForFilterCollection_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionLeadsOnGoingForFilterCollection_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionLeadsOnGoingForFilterCollection_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection LeadsOnGoingForFilterCollection (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionLeadsOnGoingForFilterCollection_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionLeadsOnGoingForFilterCollection_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionLeadsOnGoingForFilterCollection_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection LeadsOnGoingForFilterCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionLeadsOnGoingForFilterCollection_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionLeadsOnGoingForFilterCollection_ItemPropertyChanged;
							OnLeadsOnGoingForFilterCollectionItemAdded((LeadsItemViewModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection LeadsOnGoingForFilterCollection (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionLeadsOnGoingForFilterCollection_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnLeadsOnGoingForFilterCollectionItemRemoved((LeadsItemViewModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionLeadsOnGoingForFilterCollection_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionLeadsOnGoingForFilterCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionLeadsOnGoingForFilterCollection_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionLeadsOnGoingForFilterCollection_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LeadsOnGoingForFilterCollection_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyLeadsOnGoingForFilterCollectionDependencies();
				}

				private void WatchCollectionLeadsOnGoingForFilterCollection_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LeadsOnGoingForFilterCollection_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyLeadsOnGoingForFilterCollectionDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection LeadsOnGoingForFilterCollection.
				///</summary>
				protected virtual void OnLeadsOnGoingForFilterCollectionItemAdded(LeadsItemViewModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection LeadsOnGoingForFilterCollection.
				///</summary>
				protected virtual void OnLeadsOnGoingForFilterCollectionItemRemoved(LeadsItemViewModel p_Item)
				{
				}

			#endregion

			#region === Collection : UsersCollections ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection UsersCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionUsersCollections_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionUsersCollections_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionUsersCollections_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection UsersCollections (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionUsersCollections_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionUsersCollections_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionUsersCollections_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection UsersCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionUsersCollections_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionUsersCollections_ItemPropertyChanged;
							OnUsersCollectionsItemAdded((MyADA.Common.Services.Models.UserInfos)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection UsersCollections (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionUsersCollections_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnUsersCollectionsItemRemoved((MyADA.Common.Services.Models.UserInfos)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionUsersCollections_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionUsersCollections_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionUsersCollections_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionUsersCollections_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(UsersCollections_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyUsersCollectionsDependencies();
				}

				private void WatchCollectionUsersCollections_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(UsersCollections_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyUsersCollectionsDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection UsersCollections.
				///</summary>
				protected virtual void OnUsersCollectionsItemAdded(MyADA.Common.Services.Models.UserInfos p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection UsersCollections.
				///</summary>
				protected virtual void OnUsersCollectionsItemRemoved(MyADA.Common.Services.Models.UserInfos p_Item)
				{
				}

			#endregion

			#region === Collection : ManagedShowRoomLeadCollections ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection ManagedShowRoomLeadCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionManagedShowRoomLeadCollections_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionManagedShowRoomLeadCollections_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionManagedShowRoomLeadCollections_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection ManagedShowRoomLeadCollections (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionManagedShowRoomLeadCollections_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionManagedShowRoomLeadCollections_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionManagedShowRoomLeadCollections_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection ManagedShowRoomLeadCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionManagedShowRoomLeadCollections_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionManagedShowRoomLeadCollections_ItemPropertyChanged;
							OnManagedShowRoomLeadCollectionsItemAdded((MyADA.Common.Services.Models.ManagedShowRoomLead)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection ManagedShowRoomLeadCollections (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionManagedShowRoomLeadCollections_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnManagedShowRoomLeadCollectionsItemRemoved((MyADA.Common.Services.Models.ManagedShowRoomLead)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionManagedShowRoomLeadCollections_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionManagedShowRoomLeadCollections_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionManagedShowRoomLeadCollections_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionManagedShowRoomLeadCollections_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ManagedShowRoomLeadCollections_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyManagedShowRoomLeadCollectionsDependencies();
				}

				private void WatchCollectionManagedShowRoomLeadCollections_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ManagedShowRoomLeadCollections_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyManagedShowRoomLeadCollectionsDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection ManagedShowRoomLeadCollections.
				///</summary>
				protected virtual void OnManagedShowRoomLeadCollectionsItemAdded(MyADA.Common.Services.Models.ManagedShowRoomLead p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection ManagedShowRoomLeadCollections.
				///</summary>
				protected virtual void OnManagedShowRoomLeadCollectionsItemRemoved(MyADA.Common.Services.Models.ManagedShowRoomLead p_Item)
				{
				}

			#endregion

			#region === Collection : LeadFilterModelsCollections ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection LeadFilterModelsCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionLeadFilterModelsCollections_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionLeadFilterModelsCollections_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionLeadFilterModelsCollections_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection LeadFilterModelsCollections (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionLeadFilterModelsCollections_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionLeadFilterModelsCollections_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionLeadFilterModelsCollections_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection LeadFilterModelsCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionLeadFilterModelsCollections_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionLeadFilterModelsCollections_ItemPropertyChanged;
							OnLeadFilterModelsCollectionsItemAdded((MyADA.Common.Services.Models.LeadFilterModels)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection LeadFilterModelsCollections (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionLeadFilterModelsCollections_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnLeadFilterModelsCollectionsItemRemoved((MyADA.Common.Services.Models.LeadFilterModels)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionLeadFilterModelsCollections_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionLeadFilterModelsCollections_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionLeadFilterModelsCollections_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionLeadFilterModelsCollections_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LeadFilterModelsCollections_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyLeadFilterModelsCollectionsDependencies();
				}

				private void WatchCollectionLeadFilterModelsCollections_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LeadFilterModelsCollections_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyLeadFilterModelsCollectionsDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection LeadFilterModelsCollections.
				///</summary>
				protected virtual void OnLeadFilterModelsCollectionsItemAdded(MyADA.Common.Services.Models.LeadFilterModels p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection LeadFilterModelsCollections.
				///</summary>
				protected virtual void OnLeadFilterModelsCollectionsItemRemoved(MyADA.Common.Services.Models.LeadFilterModels p_Item)
				{
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux commandes ===

			// Notez que les propriétés de commandes sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Commande : ShowAllocatedLeadCommand ===


				private bool ShowAllocatedLeadCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowAllocatedLeadCommand_CanExecute(l_Parameter);
				}

				private void ShowAllocatedLeadCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowAllocatedLeadCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowAllocatedLeadCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowAllocatedLeadCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowLeadDetailsCommand ===


				private bool ShowLeadDetailsCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowLeadDetailsCommand_CanExecute(l_Parameter);
				}

				private void ShowLeadDetailsCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowLeadDetailsCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowLeadDetailsCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowLeadDetailsCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowMyLeadDetailsCommand ===


				private bool ShowMyLeadDetailsCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowMyLeadDetailsCommand_CanExecute(l_Parameter);
				}

				private void ShowMyLeadDetailsCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowMyLeadDetailsCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowMyLeadDetailsCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowMyLeadDetailsCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : SelectRepresentativeCommand ===


				private bool SelectRepresentativeCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnSelectRepresentativeCommand_CanExecute(l_Parameter);
				}

				private void SelectRepresentativeCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnSelectRepresentativeCommand_Execute(l_Parameter);
				}

				protected virtual bool OnSelectRepresentativeCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnSelectRepresentativeCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : AddOrRemoveAffectationCommand ===


				private bool AddOrRemoveAffectationCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnAddOrRemoveAffectationCommand_CanExecute(l_Parameter);
				}

				private void AddOrRemoveAffectationCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnAddOrRemoveAffectationCommand_Execute(l_Parameter);
				}

				protected virtual bool OnAddOrRemoveAffectationCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnAddOrRemoveAffectationCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : SetLeadStatusCommand ===


				private bool SetLeadStatusCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnSetLeadStatusCommand_CanExecute(l_Parameter);
				}

				private void SetLeadStatusCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnSetLeadStatusCommand_Execute(l_Parameter);
				}

				protected virtual bool OnSetLeadStatusCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnSetLeadStatusCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : AllocateCommand ===


				private bool AllocateCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnAllocateCommand_CanExecute(l_Parameter);
				}

				private void AllocateCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnAllocateCommand_Execute(l_Parameter);
				}

				protected virtual bool OnAllocateCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnAllocateCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : LiveChangeLeadStatusOnDoneCommand ===


				private bool LiveChangeLeadStatusOnDoneCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnLiveChangeLeadStatusOnDoneCommand_CanExecute(l_Parameter);
				}

				private void LiveChangeLeadStatusOnDoneCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnLiveChangeLeadStatusOnDoneCommand_Execute(l_Parameter);
				}

				protected virtual bool OnLiveChangeLeadStatusOnDoneCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnLiveChangeLeadStatusOnDoneCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : LiveChangeLeadStatusOnRecontactCommand ===


				private bool LiveChangeLeadStatusOnRecontactCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnLiveChangeLeadStatusOnRecontactCommand_CanExecute(l_Parameter);
				}

				private void LiveChangeLeadStatusOnRecontactCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnLiveChangeLeadStatusOnRecontactCommand_Execute(l_Parameter);
				}

				protected virtual bool OnLiveChangeLeadStatusOnRecontactCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnLiveChangeLeadStatusOnRecontactCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : LiveChangeLeadStatusOnNotInterestCommand ===


				private bool LiveChangeLeadStatusOnNotInterestCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnLiveChangeLeadStatusOnNotInterestCommand_CanExecute(l_Parameter);
				}

				private void LiveChangeLeadStatusOnNotInterestCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnLiveChangeLeadStatusOnNotInterestCommand_Execute(l_Parameter);
				}

				protected virtual bool OnLiveChangeLeadStatusOnNotInterestCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnLiveChangeLeadStatusOnNotInterestCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : SimpleUserAllocateLeadCommand ===


				private bool SimpleUserAllocateLeadCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnSimpleUserAllocateLeadCommand_CanExecute(l_Parameter);
				}

				private void SimpleUserAllocateLeadCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnSimpleUserAllocateLeadCommand_Execute(l_Parameter);
				}

				protected virtual bool OnSimpleUserAllocateLeadCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnSimpleUserAllocateLeadCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowMyLeadsCommand ===


				private bool ShowMyLeadsCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowMyLeadsCommand_CanExecute(l_Parameter);
				}

				private void ShowMyLeadsCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowMyLeadsCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowMyLeadsCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowMyLeadsCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : NextCommand ===


				private bool NextCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnNextCommand_CanExecute(l_Parameter);
				}

				private void NextCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnNextCommand_Execute(l_Parameter);
				}

				protected virtual bool OnNextCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnNextCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : BackCommand ===


				private bool BackCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnBackCommand_CanExecute(l_Parameter);
				}

				private void BackCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnBackCommand_Execute(l_Parameter);
				}

				protected virtual bool OnBackCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnBackCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : DoFilterCommand ===


				private bool DoFilterCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnDoFilterCommand_CanExecute(l_Parameter);
				}

				private void DoFilterCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnDoFilterCommand_Execute(l_Parameter);
				}

				protected virtual bool OnDoFilterCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnDoFilterCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : MyOwnLeadsPopinVisibilityCommand ===


				private bool MyOwnLeadsPopinVisibilityCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnMyOwnLeadsPopinVisibilityCommand_CanExecute(l_Parameter);
				}

				private void MyOwnLeadsPopinVisibilityCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnMyOwnLeadsPopinVisibilityCommand_Execute(l_Parameter);
				}

				protected virtual bool OnMyOwnLeadsPopinVisibilityCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnMyOwnLeadsPopinVisibilityCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : LoadMoreCommand ===


				private bool LoadMoreCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnLoadMoreCommand_CanExecute(l_Parameter);
				}

				private void LoadMoreCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnLoadMoreCommand_Execute(l_Parameter);
				}

				protected virtual bool OnLoadMoreCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnLoadMoreCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : LoadMoreLeadOfTheDateCommand ===


				private bool LoadMoreLeadOfTheDateCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnLoadMoreLeadOfTheDateCommand_CanExecute(l_Parameter);
				}

				private void LoadMoreLeadOfTheDateCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnLoadMoreLeadOfTheDateCommand_Execute(l_Parameter);
				}

				protected virtual bool OnLoadMoreLeadOfTheDateCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnLoadMoreLeadOfTheDateCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : LoadMoreLeadOnGoingCommand ===


				private bool LoadMoreLeadOnGoingCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnLoadMoreLeadOnGoingCommand_CanExecute(l_Parameter);
				}

				private void LoadMoreLeadOnGoingCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnLoadMoreLeadOnGoingCommand_Execute(l_Parameter);
				}

				protected virtual bool OnLoadMoreLeadOnGoingCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnLoadMoreLeadOnGoingCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : CloseFilterViewAndLoadDataCommand ===


				private bool CloseFilterViewAndLoadDataCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnCloseFilterViewAndLoadDataCommand_CanExecute(l_Parameter);
				}

				private void CloseFilterViewAndLoadDataCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnCloseFilterViewAndLoadDataCommand_Execute(l_Parameter);
				}

				protected virtual bool OnCloseFilterViewAndLoadDataCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnCloseFilterViewAndLoadDataCommand_Execute(object p_Parameter);

			#endregion


		#endregion

	}

	public partial class LeadViewModel : LeadViewModelBase
	{
	}
}
