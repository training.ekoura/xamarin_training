using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;
using MyADA.Common.Services.Services;
using System.Threading.Tasks;
using MyADA.Common.DAOServices.Services;
using MyADA.Common.Services.Models;
using MyADA.Common.Services.Requests;

namespace MyADA.Common.ViewModels
{
    public partial class BurgerViewModel : BurgerViewModelBase, IBurgerViewModel
    {
        private IHomeViewModel _HomeViewModel = CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>();

        /// <summary>
        /// Show lead command
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override void OnShowLeadCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBusy = true;
                IsBurgerPresented = true;

                var l_LeadViewModel = CoreSDK.Framework.Services.Container().Resolve<ILeadViewModel>();
                l_LeadViewModel.IsAllocatedVisible = true;
                l_LeadViewModel.LeadsCollection.Clear();
                l_LeadViewModel.OnUseLeadsCollection.Clear();
                l_LeadViewModel.IsDefaultLeadCollectionFilter = true;
                l_LeadViewModel.ShowAllocatedLeadCommand.Execute(1);

                if (_HomeViewModel.CurrentUserProfileInfos.Showroom.LeadConfig == Helper.ToolsBox.ADAShowroomLeadConfig.available.ToString())
                {
                    l_LeadViewModel.IsDefaultLeadCollectionFilter = true;
                    CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.BookLeadsView, l_LeadViewModel);
                }
                else
                {

                    if (_HomeViewModel.CurrentUserProfileInfos.Role != MyADA.Common.ViewModels.Helper.ToolsBox.ADARole.user.ToString())
                    {
                        l_LeadViewModel.IsDefaultLeadCollectionFilter = true;
                        CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MyLeadsView, l_LeadViewModel);
                    }
                    else
                    {
                        CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.CommercialLeadsView, l_LeadViewModel);
                    }
                }
                IsBurgerPresented = false;
                IsBusy = false;
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBurgerPresented = false;
                IsBusy = false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Show Audi news from Mobile Bridge.
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override void OnShowAudiNewsCommand_Execute(object p_Parameter)
        {
            //try
            //{
            //    IsBusy = true;
            //    IsBurgerPresented = true;

            //    _HomeViewModel.CurrentMBPage = ViewName.AudiNewsView;
            //    _HomeViewModel.IsHomeView = false;
            //    _HomeViewModel.CurrentPageHeaderTitle = Properties.Resources.MB_AudiNews;
            //    CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MBXamarinFormsTrainingAppPage, _HomeViewModel);

            //    //IsBurgerPresented = false;
            //    //IsBusy = false;
            //}
            //catch (Exception ex)
            //{
            //    CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            //    IsBurgerPresented = false;
            //    IsBusy = false;
            //}
            //finally
            //{
            //}
        }

        /// <summary>
        /// Show Sales tools MB
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override void OnShowSalesToolCommand_Execute(object p_Parameter)
        {
            //try
            //{
            //    IsBusy = true;
            //    IsBurgerPresented = true;

            //    //_HomeViewModel.CurrentMBPage = ViewName.SalesTools;
            //    _HomeViewModel.CurrentMBPage = ViewName.AudiNewsView;
            //    _HomeViewModel.IsHomeView = false;
            //    _HomeViewModel.CurrentPageHeaderTitle = Properties.Resources.MB_SalesTools;
            //    CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MBXamarinFormsTrainingAppPage, _HomeViewModel);

            //    //IsBurgerPresented = false;
            //    //IsBusy = false;
            //}
            //catch (Exception ex)
            //{
            //    CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            //    IsBurgerPresented = false;
            //    IsBusy = false;
            //}
            //finally
            //{
            //}
        }

        /// <summary>
        /// Show Self training
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override void OnShowSelfTrainingCommand_Execute(object p_Parameter)
        {
            //try
            //{
            //    IsBusy = true;
            //    IsBurgerPresented = true;

            //    //_HomeViewModel.CurrentMBPage = ViewName.SelfTraining;
            //    _HomeViewModel.CurrentMBPage = ViewName.AudiNewsView;
            //    _HomeViewModel.IsHomeView = false;
            //    _HomeViewModel.CurrentPageHeaderTitle = Properties.Resources.MB_SelfTraining;
            //    CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MBXamarinFormsTrainingAppPage, _HomeViewModel);

            //    //IsBurgerPresented = false;
            //    //IsBusy = false;
            //}
            //catch (Exception ex)
            //{
            //    CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            //    IsBurgerPresented = false;
            //    IsBusy = false;
            //}
            //finally
            //{
            //}
        }

        /// <summary>
        /// Show report views
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override async void OnShowViewReportCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBusy = true;
                IsBurgerPresented = true;
                if (_HomeViewModel.CurrentUserProfileInfos.Role == Helper.ToolsBox.ADARole.user.ToString())
                {
                    var l_ReportService = CoreSDK.Framework.Services.Container().Resolve<IReportService>();
                    _HomeViewModel.ReportSource = await l_ReportService.GetMyReport(_HomeViewModel.CurrentUserProfileInfos.UserId);

                    CoreSDK.Framework.Services.InteractionService().Open(ViewName.ReadDocumentView, this);
                }
                else
                {
                    var l_ShowRoomService = CoreSDK.Framework.Services.Container().Resolve<IShowroomService>();
                    _HomeViewModel.ShowRoomCollections.Clear();
                    _HomeViewModel.UsersCollections.Clear();
                    _HomeViewModel.UsersCollections.AddRange(CoreSDK.Framework.Services.Container().Resolve<ILeadViewModel>().UsersCollections);

                    if (_HomeViewModel.CurrentUserProfileInfos.Profile.Organization.employee)
                    {
                        _HomeViewModel.ShowReportForManager = false;
                        _HomeViewModel.ShowReportForSimpleUser = true;
                        var l_ListShowRoom = await l_ShowRoomService.GetShowroom();
                        _HomeViewModel.ShowRoomCollections.AddRange(l_ListShowRoom);
                        _HomeViewModel.ShowShowroomPicker = true;
                    }
                    else
                    {
                        _HomeViewModel.ShowReportForSimpleUser = true;
                        _HomeViewModel.ShowReportForManager = true;
                        _HomeViewModel.ShowShowroomPicker = false;
                    }
                    CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MyReportView, _HomeViewModel);
                }

                IsBusy = false;
                IsBurgerPresented = false;
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBurgerPresented = false;
                IsBusy = false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Show home view
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override async void OnShowHomeViewCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBurgerPresented = true;
                IsBusy = true;
                _HomeViewModel.CurrentMBPage = ViewName.MBXamarinFormsTrainingAppPage;
                _HomeViewModel.IsHomeView = true;

                await _HomeViewModel.RefreshLeadData();
                CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MBXamarinFormsTrainingAppPage);

                IsBusy = false;
                IsBurgerPresented = false;
                _HomeViewModel.IsBusy = false;
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBurgerPresented = false;
                IsBusy = false;
            }
        }

        /// <summary>
        /// Show my news
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override void OnShowMyNewsCommand_Execute(object p_Parameter)
        {
            //try
            //{
            //    IsBusy = true;
            //    IsBurgerPresented = true;

            //    IsBurgerPresented = false;
            //    IsBusy = false;

            //}
            //catch (Exception ex)
            //{
            //    CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            //    IsBurgerPresented = false;
            //    IsBusy = false;
            //}
            //finally
            //{
            //}
        }

        /// <summary>
        /// Show My RSS
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override async void OnShowMyRssCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBurgerPresented = true;
                IsBusy = true;

                var l_UserService = CoreSDK.Framework.Services.Container().Resolve<IUserService>();
                var l_GetRssFeeds = await l_UserService.GetUserRssFeeds(_HomeViewModel.CurrentUserProfileInfos.UserId);
                UserRssFeedsCollection.Clear();
                UserRssFeedsCollection.AddRange(l_GetRssFeeds.Select(a => new RssFeedItemViewModel().SetLeadsEncapsulation(a)));

                var GlobalConfigService = CoreSDK.Framework.Services.GlobalConfigService();
                GlobalConfigService.Data.BookedRSSList.Clear();
                GlobalConfigService.Data.BookedRSSList.AddRange(UserRssFeedsCollection.Select(y => y.Title).ToList());
                GlobalConfigService.SaveConfig();

                // Afficher le nombre de notification de chaque feed souscrit.
                foreach (var Feed in UserRssFeedsCollection)
                {
                    var Posts = await l_UserService.ReadItem(Feed.Url);
                    Feed.NbPost = (Posts != null) ? Posts.Count().ToString() : "0";
                }

                CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MyRSSView, this);

                IsBurgerPresented = false;
                IsBusy = false;
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBurgerPresented = false;
                IsBusy = false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Show Sales Tools Products
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override void OnShowSalesToolsProductsCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBurgerPresented = true;
                IsBusy = true;

                IsBurgerPresented = false;
                IsBusy = false;
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBurgerPresented = false;
                IsBusy = false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Show Sales Tools Services
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override void OnShowSalesToolsServiceCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBurgerPresented = true;
                IsBusy = true;

                IsBurgerPresented = false;
                IsBusy = false;

            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBurgerPresented = false;
                IsBusy = false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Show my Sales Tools Incentives
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override void OnShowSalesToolsIncentivesCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBurgerPresented = true;
                IsBusy = true;

                IsBurgerPresented = false;
                IsBusy = false;

            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBurgerPresented = false;
                IsBusy = false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Show Tools Promotion
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override void OnShowSalesToolsPromotionCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBurgerPresented = true;
                IsBusy = true;

                IsBurgerPresented = false;
                IsBusy = false;
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBurgerPresented = false;
                IsBusy = false;
            }
            finally
            {
            }
        }

        protected override async void OnShowMyLeadsCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBusy = true;
                IsBurgerPresented = true;
                var l_LeadViewModel = CoreSDK.Framework.Services.Container().Resolve<ILeadViewModel>();

                l_LeadViewModel.ShowMyLeadsCommandFunction();

            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBusy = false;
                IsBurgerPresented = true;
            }
        }

        protected override async void OnShowSettingsCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBurgerPresented = true;
                IsBusy = true;

                var l_SettingsViewModel = CoreSDK.Framework.Services.Container().Resolve<ISettingsViewModel>();

                var result = await l_SettingsViewModel.PrepareVM(_HomeViewModel.CurrentUserProfileInfos);

                if (result)
                    CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.SettingsProfileView, l_SettingsViewModel);

                IsBusy = false;
                IsBurgerPresented = false;
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBurgerPresented = false;
                IsBusy = false;
            }
        }

        protected override async void OnShowHomeViewFromHeaderCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBusy = true;
                _HomeViewModel.CurrentMBPage = ViewName.MBXamarinFormsTrainingAppPage;
                _HomeViewModel.IsHomeView = true;

                await _HomeViewModel.RefreshLeadData();
                CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MBXamarinFormsTrainingAppPage);

                IsBusy = false;
                _HomeViewModel.IsBusy = false;
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBurgerPresented = false;
                IsBusy = false;
            }
        }

        protected override void OnAddOrRemoveRssFeedCommand_Execute(object p_Parameter)
        {
            var l_RssFeedParameter = (RssFeedItemViewModel)p_Parameter;
            RssFeedsCollection.Where(x => x.FeedId == l_RssFeedParameter.FeedId).FirstOrDefault().IsSelected = (l_RssFeedParameter.IsSelected) ? false : true;
        }

        protected override async void OnSaveRssCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBusy = true;


                var l_UserService = CoreSDK.Framework.Services.Container().Resolve<IUserService>();
                var l_UserSubcribeRssRequest = new UserRssRequest()
                {
                    FeedIds = RssFeedsCollection.Where(x => x.IsSelected).Select(y => y.FeedId).ToArray()
                };

                var l_UserUnSubcribeRssRequest = new UserRssRequest()
                {
                    FeedIds = RssFeedsCollection.Where(x => !x.IsSelected).Select(y => y.FeedId).ToArray()
                };

                var l_CheckSubscribe = await l_UserService.SubscribeRssFeed(_HomeViewModel.CurrentUserProfileInfos.UserId, l_UserSubcribeRssRequest);
                var l_CheckUnsubcribe = await l_UserService.UnsubcribeRssFeed(_HomeViewModel.CurrentUserProfileInfos.UserId, l_UserUnSubcribeRssRequest);

                var l_GetRssFeeds = await l_UserService.GetUserRssFeeds(_HomeViewModel.CurrentUserProfileInfos.UserId);
                UserRssFeedsCollection.Clear();
                UserRssFeedsCollection.AddRange(l_GetRssFeeds.Select(a => new RssFeedItemViewModel().SetLeadsEncapsulation(a)));

                // Save selected RSS
                var GlobalConfigService = CoreSDK.Framework.Services.GlobalConfigService();
                GlobalConfigService.Data.BookedRSSList.Clear();
                GlobalConfigService.Data.BookedRSSList.AddRange(UserRssFeedsCollection.Select(y => y.Title).ToList());
                GlobalConfigService.SaveConfig();

                // Afficher le nombre de notification de chaque feed souscrit.
                foreach (var Feed in UserRssFeedsCollection)
                {
                    var Posts = await l_UserService.ReadItem(Feed.Url);
                    Feed.NbPost = (Posts != null) ? Posts.Count().ToString() : "0";
                }

                CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection("IMyRSSView", this);

                IsBusy = false;

            }
            catch (Exception ex)
            {
            }
        }

        protected override void OnBackCommand_Execute(object p_Parameter)
        {

        }

        protected override async void OnShowManageRssCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBusy = true;

                var l_UserService = CoreSDK.Framework.Services.Container().Resolve<IUserService>();
                var l_GetRssFeeds = await l_UserService.GetRssfeeds();
                var l_GlobalService = CoreSDK.Framework.Services.GlobalConfigService();
                RssFeedsCollection.Clear();
                RssFeedsCollection.AddRange(l_GetRssFeeds.Select(a => new RssFeedItemViewModel() { IsSelected = l_GlobalService.Data.BookedRSSList.Contains(a.Title) }.SetLeadsEncapsulation(a)));

                CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.EnterRssUrlView, this);

                IsBusy = false;
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBusy = false;
            }
            finally
            {
            }
        }

        protected async override void OnShowFeedDetailCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBusy = true;

                await Task.Delay(200);

                var l_RssFeed = (p_Parameter != null) ? (RssFeedItemViewModel)p_Parameter : new RssFeedItemViewModel();
                SelectedFeedName = l_RssFeed.Title;

                var l_UserService = CoreSDK.Framework.Services.Container().Resolve<IUserService>();
                var posts = await l_UserService.ReadItem(l_RssFeed.Url);
                RssFeedItemsCollection.Clear();
                RssFeedItemsCollection.AddRange(posts);

                CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection("IRSSFeedView", this);

                //IsBusy = false;
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBusy = false;
            }
        }

        protected override void OnShowFeedItemDetailCommand_Execute(object p_Parameter)
        {
            try
            {
                //IsBusy = true;
                SelectedItemFeed = (p_Parameter != null) ? (Post)p_Parameter : new Post();
                SelectedItemFeedUrl = SelectedItemFeed.Link;
                CoreSDK.Framework.Services.InteractionService().Open("IFeedRSSDetailView", this);

                //IsBusy = false;
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBusy = false;
            }
        }

        protected override void OnAddRssFeedCommand_Execute(object p_Parameter)
        {
            throw new NotImplementedException();
        }

        protected override async void OnShowAboutViewCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBusy = true;
                IsBurgerPresented = true;
                AboutViewUrl = MyADA.Common.Constants.GeneralConditionsLink;
                CoreSDK.Framework.Services.InteractionService().Open("IAboutView", this);
                IsBusy = false;
                IsBurgerPresented = false;

            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}