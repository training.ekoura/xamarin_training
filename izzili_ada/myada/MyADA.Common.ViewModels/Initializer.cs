﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;


namespace MyADA.Common.ViewModels
{
    public class Initializer 
    {
        public static void RegisterAll()
        {
            CoreSDK.Framework.Services.Container().Register<IContextViewModel, ContextViewModel>();
            CoreSDK.Framework.Services.Container().Register<ILoginViewModel, LoginViewModel>();
            CoreSDK.Framework.Services.Container().Register<IHomeViewModel, HomeViewModel>(new SingletonInstanceManager());
            CoreSDK.Framework.Services.Container().Register<ISettingsViewModel, SettingsViewModel>();
            CoreSDK.Framework.Services.Container().Register<ILeadViewModel, LeadViewModel>(new SingletonInstanceManager());
            CoreSDK.Framework.Services.Container().Register<IBurgerViewModel, BurgerViewModel>(new SingletonInstanceManager());
        }
    }
}
