﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML HomeViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  CoreSDK.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;

namespace MyADA.Common.ViewModels
{
	public abstract partial class HomeViewModelBase : CoreSDK.ViewModel
	{
		protected override void OnInitialize()
		{
			base.OnInitialize();

			// Initialisation de la collection MBAllRichMessage.
			MBAllRichMessage = new System.Collections.ObjectModel.ObservableCollection<RichMessageItem>();
			// Initialisation de la collection MBAllSalesToolsRichMessage.
			MBAllSalesToolsRichMessage = new System.Collections.ObjectModel.ObservableCollection<RichMessageItem>();
			// Initialisation de la collection MBAllSelfTrainingRichMessage.
			MBAllSelfTrainingRichMessage = new System.Collections.ObjectModel.ObservableCollection<RichMessageItem>();
			// Initialisation de la collection ShowRoomCollections.
			ShowRoomCollections = new System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels>();
			// Initialisation de la collection UsersCollections.
			UsersCollections = new System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos>();
			// Initialisation de la commande ShowLeadFromHomeCommand.
			ShowLeadFromHomeCommand = new DelegateCommand(ShowLeadFromHomeCommand_CanExecute, ShowLeadFromHomeCommand_Execute);
			// Initialisation de la commande ShowAudiNewsFromHomeCommand.
			ShowAudiNewsFromHomeCommand = new DelegateCommand(ShowAudiNewsFromHomeCommand_CanExecute, ShowAudiNewsFromHomeCommand_Execute);
			// Initialisation de la commande ShowSalesToolFromHomeCommand.
			ShowSalesToolFromHomeCommand = new DelegateCommand(ShowSalesToolFromHomeCommand_CanExecute, ShowSalesToolFromHomeCommand_Execute);
			// Initialisation de la commande ShowSelfTrainingFromHomeCommand.
			ShowSelfTrainingFromHomeCommand = new DelegateCommand(ShowSelfTrainingFromHomeCommand_CanExecute, ShowSelfTrainingFromHomeCommand_Execute);
			// Initialisation de la commande ShowViewReportFromHomeCommand.
			ShowViewReportFromHomeCommand = new DelegateCommand(ShowViewReportFromHomeCommand_CanExecute, ShowViewReportFromHomeCommand_Execute);
			// Initialisation de la commande ShowSettingsCommand.
			ShowSettingsCommand = new DelegateCommand(ShowSettingsCommand_CanExecute, ShowSettingsCommand_Execute);
			// Initialisation de la commande ShowRichMessageCommand.
			ShowRichMessageCommand = new DelegateCommand(ShowRichMessageCommand_CanExecute, ShowRichMessageCommand_Execute);
			// Initialisation de la commande SelectedUserCommand.
			SelectedUserCommand = new DelegateCommand(SelectedUserCommand_CanExecute, SelectedUserCommand_Execute);
			// Initialisation de la commande SelectedShowroomCommand.
			SelectedShowroomCommand = new DelegateCommand(SelectedShowroomCommand_CanExecute, SelectedShowroomCommand_Execute);
			// Initialisation de la commande ShowLeadReportCommand.
			ShowLeadReportCommand = new DelegateCommand(ShowLeadReportCommand_CanExecute, ShowLeadReportCommand_Execute);
			// Initialisation de la commande ShowConsolidatedMetricsCommand.
			ShowConsolidatedMetricsCommand = new DelegateCommand(ShowConsolidatedMetricsCommand_CanExecute, ShowConsolidatedMetricsCommand_Execute);
			// Initialisation de la commande ShowMyLeadsCommand.
			ShowMyLeadsCommand = new DelegateCommand(ShowMyLeadsCommand_CanExecute, ShowMyLeadsCommand_Execute);
			// Initialisation de la commande ShowOverAllCommand.
			ShowOverAllCommand = new DelegateCommand(ShowOverAllCommand_CanExecute, ShowOverAllCommand_Execute);
		}

		#region === Propriétés ===

			#region === Propriété : LeadNotif ===

				public const string LeadNotif_PROPERTYNAME = "LeadNotif";

				private string _LeadNotif;
				///<summary>
				/// Propriété : LeadNotif
				///</summary>
				public string LeadNotif
				{
					get
					{
						return GetValue<string>(() => _LeadNotif);
					}
					set
					{
						SetValue<string>(() => _LeadNotif, (v) => _LeadNotif = v, value, LeadNotif_PROPERTYNAME,  DoLeadNotifBeforeSet, DoLeadNotifAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLeadNotifBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLeadNotifAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLeadNotifDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsVisibleLeadNotif ===

				public const string IsVisibleLeadNotif_PROPERTYNAME = "IsVisibleLeadNotif";

				private bool _IsVisibleLeadNotif;
				///<summary>
				/// Propriété : IsVisibleLeadNotif
				///</summary>
				public bool IsVisibleLeadNotif
				{
					get
					{
						return GetValue<bool>(() => _IsVisibleLeadNotif);
					}
					set
					{
						SetValue<bool>(() => _IsVisibleLeadNotif, (v) => _IsVisibleLeadNotif = v, value, IsVisibleLeadNotif_PROPERTYNAME,  DoIsVisibleLeadNotifBeforeSet, DoIsVisibleLeadNotifAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsVisibleLeadNotifBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsVisibleLeadNotifAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsVisibleLeadNotifDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : OverHundredLeadNotif ===

				public const string OverHundredLeadNotif_PROPERTYNAME = "OverHundredLeadNotif";

				private bool _OverHundredLeadNotif;
				///<summary>
				/// Propriété : OverHundredLeadNotif
				///</summary>
				public bool OverHundredLeadNotif
				{
					get
					{
						return GetValue<bool>(() => _OverHundredLeadNotif);
					}
					set
					{
						SetValue<bool>(() => _OverHundredLeadNotif, (v) => _OverHundredLeadNotif = v, value, OverHundredLeadNotif_PROPERTYNAME,  DoOverHundredLeadNotifBeforeSet, DoOverHundredLeadNotifAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoOverHundredLeadNotifBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoOverHundredLeadNotifAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyOverHundredLeadNotifDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsBusy ===

				public const string IsBusy_PROPERTYNAME = "IsBusy";

				private bool _IsBusy;
				///<summary>
				/// Propriété : IsBusy
				///</summary>
				public bool IsBusy
				{
					get
					{
						return GetValue<bool>(() => _IsBusy);
					}
					set
					{
						SetValue<bool>(() => _IsBusy, (v) => _IsBusy = v, value, IsBusy_PROPERTYNAME,  DoIsBusyBeforeSet, DoIsBusyAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsBusyBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsBusyAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsBusyDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : AudiNewsNotif ===

				public const string AudiNewsNotif_PROPERTYNAME = "AudiNewsNotif";

				private string _AudiNewsNotif;
				///<summary>
				/// Propriété : AudiNewsNotif
				///</summary>
				public string AudiNewsNotif
				{
					get
					{
						return GetValue<string>(() => _AudiNewsNotif);
					}
					set
					{
						SetValue<string>(() => _AudiNewsNotif, (v) => _AudiNewsNotif = v, value, AudiNewsNotif_PROPERTYNAME,  DoAudiNewsNotifBeforeSet, DoAudiNewsNotifAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoAudiNewsNotifBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoAudiNewsNotifAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyAudiNewsNotifDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsVisibleAudiNewsNotif ===

				public const string IsVisibleAudiNewsNotif_PROPERTYNAME = "IsVisibleAudiNewsNotif";

				private bool _IsVisibleAudiNewsNotif;
				///<summary>
				/// Propriété : IsVisibleAudiNewsNotif
				///</summary>
				public bool IsVisibleAudiNewsNotif
				{
					get
					{
						return GetValue<bool>(() => _IsVisibleAudiNewsNotif);
					}
					set
					{
						SetValue<bool>(() => _IsVisibleAudiNewsNotif, (v) => _IsVisibleAudiNewsNotif = v, value, IsVisibleAudiNewsNotif_PROPERTYNAME,  DoIsVisibleAudiNewsNotifBeforeSet, DoIsVisibleAudiNewsNotifAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsVisibleAudiNewsNotifBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsVisibleAudiNewsNotifAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsVisibleAudiNewsNotifDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : OverHundredAudiNewsNotif ===

				public const string OverHundredAudiNewsNotif_PROPERTYNAME = "OverHundredAudiNewsNotif";

				private bool _OverHundredAudiNewsNotif;
				///<summary>
				/// Propriété : OverHundredAudiNewsNotif
				///</summary>
				public bool OverHundredAudiNewsNotif
				{
					get
					{
						return GetValue<bool>(() => _OverHundredAudiNewsNotif);
					}
					set
					{
						SetValue<bool>(() => _OverHundredAudiNewsNotif, (v) => _OverHundredAudiNewsNotif = v, value, OverHundredAudiNewsNotif_PROPERTYNAME,  DoOverHundredAudiNewsNotifBeforeSet, DoOverHundredAudiNewsNotifAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoOverHundredAudiNewsNotifBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoOverHundredAudiNewsNotifAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyOverHundredAudiNewsNotifDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SalesToolNotif ===

				public const string SalesToolNotif_PROPERTYNAME = "SalesToolNotif";

				private string _SalesToolNotif;
				///<summary>
				/// Propriété : SalesToolNotif
				///</summary>
				public string SalesToolNotif
				{
					get
					{
						return GetValue<string>(() => _SalesToolNotif);
					}
					set
					{
						SetValue<string>(() => _SalesToolNotif, (v) => _SalesToolNotif = v, value, SalesToolNotif_PROPERTYNAME,  DoSalesToolNotifBeforeSet, DoSalesToolNotifAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSalesToolNotifBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSalesToolNotifAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySalesToolNotifDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsVisibleSalesToolNotif ===

				public const string IsVisibleSalesToolNotif_PROPERTYNAME = "IsVisibleSalesToolNotif";

				private bool _IsVisibleSalesToolNotif;
				///<summary>
				/// Propriété : IsVisibleSalesToolNotif
				///</summary>
				public bool IsVisibleSalesToolNotif
				{
					get
					{
						return GetValue<bool>(() => _IsVisibleSalesToolNotif);
					}
					set
					{
						SetValue<bool>(() => _IsVisibleSalesToolNotif, (v) => _IsVisibleSalesToolNotif = v, value, IsVisibleSalesToolNotif_PROPERTYNAME,  DoIsVisibleSalesToolNotifBeforeSet, DoIsVisibleSalesToolNotifAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsVisibleSalesToolNotifBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsVisibleSalesToolNotifAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsVisibleSalesToolNotifDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : OverHundredSalesToolNotif ===

				public const string OverHundredSalesToolNotif_PROPERTYNAME = "OverHundredSalesToolNotif";

				private bool _OverHundredSalesToolNotif;
				///<summary>
				/// Propriété : OverHundredSalesToolNotif
				///</summary>
				public bool OverHundredSalesToolNotif
				{
					get
					{
						return GetValue<bool>(() => _OverHundredSalesToolNotif);
					}
					set
					{
						SetValue<bool>(() => _OverHundredSalesToolNotif, (v) => _OverHundredSalesToolNotif = v, value, OverHundredSalesToolNotif_PROPERTYNAME,  DoOverHundredSalesToolNotifBeforeSet, DoOverHundredSalesToolNotifAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoOverHundredSalesToolNotifBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoOverHundredSalesToolNotifAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyOverHundredSalesToolNotifDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelfTrainingNotif ===

				public const string SelfTrainingNotif_PROPERTYNAME = "SelfTrainingNotif";

				private string _SelfTrainingNotif;
				///<summary>
				/// Propriété : SelfTrainingNotif
				///</summary>
				public string SelfTrainingNotif
				{
					get
					{
						return GetValue<string>(() => _SelfTrainingNotif);
					}
					set
					{
						SetValue<string>(() => _SelfTrainingNotif, (v) => _SelfTrainingNotif = v, value, SelfTrainingNotif_PROPERTYNAME,  DoSelfTrainingNotifBeforeSet, DoSelfTrainingNotifAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelfTrainingNotifBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelfTrainingNotifAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelfTrainingNotifDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsVisibleSelfTrainingNotif ===

				public const string IsVisibleSelfTrainingNotif_PROPERTYNAME = "IsVisibleSelfTrainingNotif";

				private bool _IsVisibleSelfTrainingNotif;
				///<summary>
				/// Propriété : IsVisibleSelfTrainingNotif
				///</summary>
				public bool IsVisibleSelfTrainingNotif
				{
					get
					{
						return GetValue<bool>(() => _IsVisibleSelfTrainingNotif);
					}
					set
					{
						SetValue<bool>(() => _IsVisibleSelfTrainingNotif, (v) => _IsVisibleSelfTrainingNotif = v, value, IsVisibleSelfTrainingNotif_PROPERTYNAME,  DoIsVisibleSelfTrainingNotifBeforeSet, DoIsVisibleSelfTrainingNotifAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsVisibleSelfTrainingNotifBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsVisibleSelfTrainingNotifAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsVisibleSelfTrainingNotifDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : OverHundredSelfTrainingNotif ===

				public const string OverHundredSelfTrainingNotif_PROPERTYNAME = "OverHundredSelfTrainingNotif";

				private bool _OverHundredSelfTrainingNotif;
				///<summary>
				/// Propriété : OverHundredSelfTrainingNotif
				///</summary>
				public bool OverHundredSelfTrainingNotif
				{
					get
					{
						return GetValue<bool>(() => _OverHundredSelfTrainingNotif);
					}
					set
					{
						SetValue<bool>(() => _OverHundredSelfTrainingNotif, (v) => _OverHundredSelfTrainingNotif = v, value, OverHundredSelfTrainingNotif_PROPERTYNAME,  DoOverHundredSelfTrainingNotifBeforeSet, DoOverHundredSelfTrainingNotifAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoOverHundredSelfTrainingNotifBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoOverHundredSelfTrainingNotifAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyOverHundredSelfTrainingNotifDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowReportForManager ===

				public const string ShowReportForManager_PROPERTYNAME = "ShowReportForManager";

				private bool _ShowReportForManager;
				///<summary>
				/// Propriété : ShowReportForManager
				///</summary>
				public bool ShowReportForManager
				{
					get
					{
						return GetValue<bool>(() => _ShowReportForManager);
					}
					set
					{
						SetValue<bool>(() => _ShowReportForManager, (v) => _ShowReportForManager = v, value, ShowReportForManager_PROPERTYNAME,  DoShowReportForManagerBeforeSet, DoShowReportForManagerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowReportForManagerBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowReportForManagerAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowReportForManagerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowReportForSimpleUser ===

				public const string ShowReportForSimpleUser_PROPERTYNAME = "ShowReportForSimpleUser";

				private bool _ShowReportForSimpleUser;
				///<summary>
				/// Propriété : ShowReportForSimpleUser
				///</summary>
				public bool ShowReportForSimpleUser
				{
					get
					{
						return GetValue<bool>(() => _ShowReportForSimpleUser);
					}
					set
					{
						SetValue<bool>(() => _ShowReportForSimpleUser, (v) => _ShowReportForSimpleUser = v, value, ShowReportForSimpleUser_PROPERTYNAME,  DoShowReportForSimpleUserBeforeSet, DoShowReportForSimpleUserAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowReportForSimpleUserBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowReportForSimpleUserAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowReportForSimpleUserDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ReportSource ===

				public const string ReportSource_PROPERTYNAME = "ReportSource";

				private string _ReportSource;
				///<summary>
				/// Propriété : ReportSource
				///</summary>
				public string ReportSource
				{
					get
					{
						return GetValue<string>(() => _ReportSource);
					}
					set
					{
						SetValue<string>(() => _ReportSource, (v) => _ReportSource = v, value, ReportSource_PROPERTYNAME,  DoReportSourceBeforeSet, DoReportSourceAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoReportSourceBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoReportSourceAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyReportSourceDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CurrentUserProfileInfos ===

				public const string CurrentUserProfileInfos_PROPERTYNAME = "CurrentUserProfileInfos";

				private MyADA.Common.Services.Models.CurrentUserProfile _CurrentUserProfileInfos;
				///<summary>
				/// Propriété : CurrentUserProfileInfos
				///</summary>
				public MyADA.Common.Services.Models.CurrentUserProfile CurrentUserProfileInfos
				{
					get
					{
						return GetValue<MyADA.Common.Services.Models.CurrentUserProfile>(() => _CurrentUserProfileInfos);
					}
					set
					{
						SetValue<MyADA.Common.Services.Models.CurrentUserProfile>(() => _CurrentUserProfileInfos, (v) => _CurrentUserProfileInfos = v, value, CurrentUserProfileInfos_PROPERTYNAME,  DoCurrentUserProfileInfosBeforeSet, DoCurrentUserProfileInfosAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCurrentUserProfileInfosBeforeSet(string p_PropertyName, MyADA.Common.Services.Models.CurrentUserProfile p_OldValue, MyADA.Common.Services.Models.CurrentUserProfile p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCurrentUserProfileInfosAfterSet(string p_PropertyName, MyADA.Common.Services.Models.CurrentUserProfile p_OldValue, MyADA.Common.Services.Models.CurrentUserProfile p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCurrentUserProfileInfosDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CurrentMBPage ===

				public const string CurrentMBPage_PROPERTYNAME = "CurrentMBPage";

				private string _CurrentMBPage;
				///<summary>
				/// Propriété : CurrentMBPage
				///</summary>
				public string CurrentMBPage
				{
					get
					{
						return GetValue<string>(() => _CurrentMBPage);
					}
					set
					{
						SetValue<string>(() => _CurrentMBPage, (v) => _CurrentMBPage = v, value, CurrentMBPage_PROPERTYNAME,  DoCurrentMBPageBeforeSet, DoCurrentMBPageAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCurrentMBPageBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCurrentMBPageAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCurrentMBPageDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CurrentPageHeaderTitle ===

				public const string CurrentPageHeaderTitle_PROPERTYNAME = "CurrentPageHeaderTitle";

				private string _CurrentPageHeaderTitle;
				///<summary>
				/// Propriété : CurrentPageHeaderTitle
				///</summary>
				public string CurrentPageHeaderTitle
				{
					get
					{
						return GetValue<string>(() => _CurrentPageHeaderTitle);
					}
					set
					{
						SetValue<string>(() => _CurrentPageHeaderTitle, (v) => _CurrentPageHeaderTitle = v, value, CurrentPageHeaderTitle_PROPERTYNAME,  DoCurrentPageHeaderTitleBeforeSet, DoCurrentPageHeaderTitleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCurrentPageHeaderTitleBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCurrentPageHeaderTitleAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCurrentPageHeaderTitleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsHomeView ===

				public const string IsHomeView_PROPERTYNAME = "IsHomeView";

				private bool _IsHomeView;
				///<summary>
				/// Propriété : IsHomeView
				///</summary>
				public bool IsHomeView
				{
					get
					{
						return GetValue<bool>(() => _IsHomeView);
					}
					set
					{
						SetValue<bool>(() => _IsHomeView, (v) => _IsHomeView = v, value, IsHomeView_PROPERTYNAME,  DoIsHomeViewBeforeSet, DoIsHomeViewAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsHomeViewBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsHomeViewAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsHomeViewDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowRoomPickerHeaderText ===

				public const string ShowRoomPickerHeaderText_PROPERTYNAME = "ShowRoomPickerHeaderText";

				private string _ShowRoomPickerHeaderText;
				///<summary>
				/// Propriété : ShowRoomPickerHeaderText
				///</summary>
				public string ShowRoomPickerHeaderText
				{
					get
					{
						return GetValue<string>(() => _ShowRoomPickerHeaderText);
					}
					set
					{
						SetValue<string>(() => _ShowRoomPickerHeaderText, (v) => _ShowRoomPickerHeaderText = v, value, ShowRoomPickerHeaderText_PROPERTYNAME,  DoShowRoomPickerHeaderTextBeforeSet, DoShowRoomPickerHeaderTextAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowRoomPickerHeaderTextBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowRoomPickerHeaderTextAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowRoomPickerHeaderTextDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowRoomPickerPlaceHolder ===

				public const string ShowRoomPickerPlaceHolder_PROPERTYNAME = "ShowRoomPickerPlaceHolder";

				private string _ShowRoomPickerPlaceHolder;
				///<summary>
				/// Propriété : ShowRoomPickerPlaceHolder
				///</summary>
				public string ShowRoomPickerPlaceHolder
				{
					get
					{
						return GetValue<string>(() => _ShowRoomPickerPlaceHolder);
					}
					set
					{
						SetValue<string>(() => _ShowRoomPickerPlaceHolder, (v) => _ShowRoomPickerPlaceHolder = v, value, ShowRoomPickerPlaceHolder_PROPERTYNAME,  DoShowRoomPickerPlaceHolderBeforeSet, DoShowRoomPickerPlaceHolderAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowRoomPickerPlaceHolderBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowRoomPickerPlaceHolderAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowRoomPickerPlaceHolderDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UserPickerHeaderText ===

				public const string UserPickerHeaderText_PROPERTYNAME = "UserPickerHeaderText";

				private string _UserPickerHeaderText;
				///<summary>
				/// Propriété : UserPickerHeaderText
				///</summary>
				public string UserPickerHeaderText
				{
					get
					{
						return GetValue<string>(() => _UserPickerHeaderText);
					}
					set
					{
						SetValue<string>(() => _UserPickerHeaderText, (v) => _UserPickerHeaderText = v, value, UserPickerHeaderText_PROPERTYNAME,  DoUserPickerHeaderTextBeforeSet, DoUserPickerHeaderTextAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUserPickerHeaderTextBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUserPickerHeaderTextAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUserPickerHeaderTextDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UserPickerPickerPlaceHolder ===

				public const string UserPickerPickerPlaceHolder_PROPERTYNAME = "UserPickerPickerPlaceHolder";

				private string _UserPickerPickerPlaceHolder;
				///<summary>
				/// Propriété : UserPickerPickerPlaceHolder
				///</summary>
				public string UserPickerPickerPlaceHolder
				{
					get
					{
						return GetValue<string>(() => _UserPickerPickerPlaceHolder);
					}
					set
					{
						SetValue<string>(() => _UserPickerPickerPlaceHolder, (v) => _UserPickerPickerPlaceHolder = v, value, UserPickerPickerPlaceHolder_PROPERTYNAME,  DoUserPickerPickerPlaceHolderBeforeSet, DoUserPickerPickerPlaceHolderAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUserPickerPickerPlaceHolderBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUserPickerPickerPlaceHolderAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUserPickerPickerPlaceHolderDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectedShowRoom ===

				public const string SelectedShowRoom_PROPERTYNAME = "SelectedShowRoom";

				private MyADA.Common.Services.Models.ShowroomModels _SelectedShowRoom;
				///<summary>
				/// Propriété : SelectedShowRoom
				///</summary>
				public MyADA.Common.Services.Models.ShowroomModels SelectedShowRoom
				{
					get
					{
						return GetValue<MyADA.Common.Services.Models.ShowroomModels>(() => _SelectedShowRoom);
					}
					set
					{
						SetValue<MyADA.Common.Services.Models.ShowroomModels>(() => _SelectedShowRoom, (v) => _SelectedShowRoom = v, value, SelectedShowRoom_PROPERTYNAME,  DoSelectedShowRoomBeforeSet, DoSelectedShowRoomAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectedShowRoomBeforeSet(string p_PropertyName, MyADA.Common.Services.Models.ShowroomModels p_OldValue, MyADA.Common.Services.Models.ShowroomModels p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectedShowRoomAfterSet(string p_PropertyName, MyADA.Common.Services.Models.ShowroomModels p_OldValue, MyADA.Common.Services.Models.ShowroomModels p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectedShowRoomDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectedUser ===

				public const string SelectedUser_PROPERTYNAME = "SelectedUser";

				private MyADA.Common.Services.Models.UserInfos _SelectedUser;
				///<summary>
				/// Propriété : SelectedUser
				///</summary>
				public MyADA.Common.Services.Models.UserInfos SelectedUser
				{
					get
					{
						return GetValue<MyADA.Common.Services.Models.UserInfos>(() => _SelectedUser);
					}
					set
					{
						SetValue<MyADA.Common.Services.Models.UserInfos>(() => _SelectedUser, (v) => _SelectedUser = v, value, SelectedUser_PROPERTYNAME,  DoSelectedUserBeforeSet, DoSelectedUserAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectedUserBeforeSet(string p_PropertyName, MyADA.Common.Services.Models.UserInfos p_OldValue, MyADA.Common.Services.Models.UserInfos p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectedUserAfterSet(string p_PropertyName, MyADA.Common.Services.Models.UserInfos p_OldValue, MyADA.Common.Services.Models.UserInfos p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectedUserDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowShowroomPicker ===

				public const string ShowShowroomPicker_PROPERTYNAME = "ShowShowroomPicker";

				private bool _ShowShowroomPicker;
				///<summary>
				/// Propriété : ShowShowroomPicker
				///</summary>
				public bool ShowShowroomPicker
				{
					get
					{
						return GetValue<bool>(() => _ShowShowroomPicker);
					}
					set
					{
						SetValue<bool>(() => _ShowShowroomPicker, (v) => _ShowShowroomPicker = v, value, ShowShowroomPicker_PROPERTYNAME,  DoShowShowroomPickerBeforeSet, DoShowShowroomPickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowShowroomPickerBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowShowroomPickerAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowShowroomPickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : MBAllRichMessage ===

				public const string MBAllRichMessage_PROPERTYNAME = "MBAllRichMessage";

				private System.Collections.ObjectModel.ObservableCollection<RichMessageItem> _MBAllRichMessage;
				///<summary>
				/// Propriété : MBAllRichMessage
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<RichMessageItem> MBAllRichMessage
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<RichMessageItem>>(() => _MBAllRichMessage);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<RichMessageItem>>(() => _MBAllRichMessage, (v) => _MBAllRichMessage = v, value, MBAllRichMessage_PROPERTYNAME,  DoMBAllRichMessageBeforeSet, DoMBAllRichMessageAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoMBAllRichMessageBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<RichMessageItem> p_OldValue, System.Collections.ObjectModel.ObservableCollection<RichMessageItem> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionMBAllRichMessage_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoMBAllRichMessageAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<RichMessageItem> p_OldValue, System.Collections.ObjectModel.ObservableCollection<RichMessageItem> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionMBAllRichMessage_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyMBAllRichMessageDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : MBAllSalesToolsRichMessage ===

				public const string MBAllSalesToolsRichMessage_PROPERTYNAME = "MBAllSalesToolsRichMessage";

				private System.Collections.ObjectModel.ObservableCollection<RichMessageItem> _MBAllSalesToolsRichMessage;
				///<summary>
				/// Propriété : MBAllSalesToolsRichMessage
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<RichMessageItem> MBAllSalesToolsRichMessage
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<RichMessageItem>>(() => _MBAllSalesToolsRichMessage);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<RichMessageItem>>(() => _MBAllSalesToolsRichMessage, (v) => _MBAllSalesToolsRichMessage = v, value, MBAllSalesToolsRichMessage_PROPERTYNAME,  DoMBAllSalesToolsRichMessageBeforeSet, DoMBAllSalesToolsRichMessageAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoMBAllSalesToolsRichMessageBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<RichMessageItem> p_OldValue, System.Collections.ObjectModel.ObservableCollection<RichMessageItem> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionMBAllSalesToolsRichMessage_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoMBAllSalesToolsRichMessageAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<RichMessageItem> p_OldValue, System.Collections.ObjectModel.ObservableCollection<RichMessageItem> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionMBAllSalesToolsRichMessage_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyMBAllSalesToolsRichMessageDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : MBAllSelfTrainingRichMessage ===

				public const string MBAllSelfTrainingRichMessage_PROPERTYNAME = "MBAllSelfTrainingRichMessage";

				private System.Collections.ObjectModel.ObservableCollection<RichMessageItem> _MBAllSelfTrainingRichMessage;
				///<summary>
				/// Propriété : MBAllSelfTrainingRichMessage
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<RichMessageItem> MBAllSelfTrainingRichMessage
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<RichMessageItem>>(() => _MBAllSelfTrainingRichMessage);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<RichMessageItem>>(() => _MBAllSelfTrainingRichMessage, (v) => _MBAllSelfTrainingRichMessage = v, value, MBAllSelfTrainingRichMessage_PROPERTYNAME,  DoMBAllSelfTrainingRichMessageBeforeSet, DoMBAllSelfTrainingRichMessageAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoMBAllSelfTrainingRichMessageBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<RichMessageItem> p_OldValue, System.Collections.ObjectModel.ObservableCollection<RichMessageItem> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionMBAllSelfTrainingRichMessage_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoMBAllSelfTrainingRichMessageAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<RichMessageItem> p_OldValue, System.Collections.ObjectModel.ObservableCollection<RichMessageItem> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionMBAllSelfTrainingRichMessage_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyMBAllSelfTrainingRichMessageDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowRoomCollections ===

				public const string ShowRoomCollections_PROPERTYNAME = "ShowRoomCollections";

				private System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels> _ShowRoomCollections;
				///<summary>
				/// Propriété : ShowRoomCollections
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels> ShowRoomCollections
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels>>(() => _ShowRoomCollections);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels>>(() => _ShowRoomCollections, (v) => _ShowRoomCollections = v, value, ShowRoomCollections_PROPERTYNAME,  DoShowRoomCollectionsBeforeSet, DoShowRoomCollectionsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowRoomCollectionsBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionShowRoomCollections_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowRoomCollectionsAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionShowRoomCollections_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowRoomCollectionsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UsersCollections ===

				public const string UsersCollections_PROPERTYNAME = "UsersCollections";

				private System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> _UsersCollections;
				///<summary>
				/// Propriété : UsersCollections
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> UsersCollections
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos>>(() => _UsersCollections);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos>>(() => _UsersCollections, (v) => _UsersCollections = v, value, UsersCollections_PROPERTYNAME,  DoUsersCollectionsBeforeSet, DoUsersCollectionsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUsersCollectionsBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionUsersCollections_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUsersCollectionsAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionUsersCollections_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUsersCollectionsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowLeadFromHomeCommand ===

				public const string ShowLeadFromHomeCommand_PROPERTYNAME = "ShowLeadFromHomeCommand";

				private IDelegateCommand _ShowLeadFromHomeCommand;
				///<summary>
				/// Propriété : ShowLeadFromHomeCommand
				///</summary>
				public IDelegateCommand ShowLeadFromHomeCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowLeadFromHomeCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowLeadFromHomeCommand, (v) => _ShowLeadFromHomeCommand = v, value, ShowLeadFromHomeCommand_PROPERTYNAME,  DoShowLeadFromHomeCommandBeforeSet, DoShowLeadFromHomeCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowLeadFromHomeCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowLeadFromHomeCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowLeadFromHomeCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowAudiNewsFromHomeCommand ===

				public const string ShowAudiNewsFromHomeCommand_PROPERTYNAME = "ShowAudiNewsFromHomeCommand";

				private IDelegateCommand _ShowAudiNewsFromHomeCommand;
				///<summary>
				/// Propriété : ShowAudiNewsFromHomeCommand
				///</summary>
				public IDelegateCommand ShowAudiNewsFromHomeCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowAudiNewsFromHomeCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowAudiNewsFromHomeCommand, (v) => _ShowAudiNewsFromHomeCommand = v, value, ShowAudiNewsFromHomeCommand_PROPERTYNAME,  DoShowAudiNewsFromHomeCommandBeforeSet, DoShowAudiNewsFromHomeCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowAudiNewsFromHomeCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowAudiNewsFromHomeCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowAudiNewsFromHomeCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowSalesToolFromHomeCommand ===

				public const string ShowSalesToolFromHomeCommand_PROPERTYNAME = "ShowSalesToolFromHomeCommand";

				private IDelegateCommand _ShowSalesToolFromHomeCommand;
				///<summary>
				/// Propriété : ShowSalesToolFromHomeCommand
				///</summary>
				public IDelegateCommand ShowSalesToolFromHomeCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowSalesToolFromHomeCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowSalesToolFromHomeCommand, (v) => _ShowSalesToolFromHomeCommand = v, value, ShowSalesToolFromHomeCommand_PROPERTYNAME,  DoShowSalesToolFromHomeCommandBeforeSet, DoShowSalesToolFromHomeCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowSalesToolFromHomeCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowSalesToolFromHomeCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowSalesToolFromHomeCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowSelfTrainingFromHomeCommand ===

				public const string ShowSelfTrainingFromHomeCommand_PROPERTYNAME = "ShowSelfTrainingFromHomeCommand";

				private IDelegateCommand _ShowSelfTrainingFromHomeCommand;
				///<summary>
				/// Propriété : ShowSelfTrainingFromHomeCommand
				///</summary>
				public IDelegateCommand ShowSelfTrainingFromHomeCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowSelfTrainingFromHomeCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowSelfTrainingFromHomeCommand, (v) => _ShowSelfTrainingFromHomeCommand = v, value, ShowSelfTrainingFromHomeCommand_PROPERTYNAME,  DoShowSelfTrainingFromHomeCommandBeforeSet, DoShowSelfTrainingFromHomeCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowSelfTrainingFromHomeCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowSelfTrainingFromHomeCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowSelfTrainingFromHomeCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowViewReportFromHomeCommand ===

				public const string ShowViewReportFromHomeCommand_PROPERTYNAME = "ShowViewReportFromHomeCommand";

				private IDelegateCommand _ShowViewReportFromHomeCommand;
				///<summary>
				/// Propriété : ShowViewReportFromHomeCommand
				///</summary>
				public IDelegateCommand ShowViewReportFromHomeCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowViewReportFromHomeCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowViewReportFromHomeCommand, (v) => _ShowViewReportFromHomeCommand = v, value, ShowViewReportFromHomeCommand_PROPERTYNAME,  DoShowViewReportFromHomeCommandBeforeSet, DoShowViewReportFromHomeCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowViewReportFromHomeCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowViewReportFromHomeCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowViewReportFromHomeCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowSettingsCommand ===

				public const string ShowSettingsCommand_PROPERTYNAME = "ShowSettingsCommand";

				private IDelegateCommand _ShowSettingsCommand;
				///<summary>
				/// Propriété : ShowSettingsCommand
				///</summary>
				public IDelegateCommand ShowSettingsCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowSettingsCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowSettingsCommand, (v) => _ShowSettingsCommand = v, value, ShowSettingsCommand_PROPERTYNAME,  DoShowSettingsCommandBeforeSet, DoShowSettingsCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowSettingsCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowSettingsCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowSettingsCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowRichMessageCommand ===

				public const string ShowRichMessageCommand_PROPERTYNAME = "ShowRichMessageCommand";

				private IDelegateCommand _ShowRichMessageCommand;
				///<summary>
				/// Propriété : ShowRichMessageCommand
				///</summary>
				public IDelegateCommand ShowRichMessageCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowRichMessageCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowRichMessageCommand, (v) => _ShowRichMessageCommand = v, value, ShowRichMessageCommand_PROPERTYNAME,  DoShowRichMessageCommandBeforeSet, DoShowRichMessageCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowRichMessageCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowRichMessageCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowRichMessageCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectedUserCommand ===

				public const string SelectedUserCommand_PROPERTYNAME = "SelectedUserCommand";

				private IDelegateCommand _SelectedUserCommand;
				///<summary>
				/// Propriété : SelectedUserCommand
				///</summary>
				public IDelegateCommand SelectedUserCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _SelectedUserCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _SelectedUserCommand, (v) => _SelectedUserCommand = v, value, SelectedUserCommand_PROPERTYNAME,  DoSelectedUserCommandBeforeSet, DoSelectedUserCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectedUserCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectedUserCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectedUserCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectedShowroomCommand ===

				public const string SelectedShowroomCommand_PROPERTYNAME = "SelectedShowroomCommand";

				private IDelegateCommand _SelectedShowroomCommand;
				///<summary>
				/// Propriété : SelectedShowroomCommand
				///</summary>
				public IDelegateCommand SelectedShowroomCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _SelectedShowroomCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _SelectedShowroomCommand, (v) => _SelectedShowroomCommand = v, value, SelectedShowroomCommand_PROPERTYNAME,  DoSelectedShowroomCommandBeforeSet, DoSelectedShowroomCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectedShowroomCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectedShowroomCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectedShowroomCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowLeadReportCommand ===

				public const string ShowLeadReportCommand_PROPERTYNAME = "ShowLeadReportCommand";

				private IDelegateCommand _ShowLeadReportCommand;
				///<summary>
				/// Propriété : ShowLeadReportCommand
				///</summary>
				public IDelegateCommand ShowLeadReportCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowLeadReportCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowLeadReportCommand, (v) => _ShowLeadReportCommand = v, value, ShowLeadReportCommand_PROPERTYNAME,  DoShowLeadReportCommandBeforeSet, DoShowLeadReportCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowLeadReportCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowLeadReportCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowLeadReportCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowConsolidatedMetricsCommand ===

				public const string ShowConsolidatedMetricsCommand_PROPERTYNAME = "ShowConsolidatedMetricsCommand";

				private IDelegateCommand _ShowConsolidatedMetricsCommand;
				///<summary>
				/// Propriété : ShowConsolidatedMetricsCommand
				///</summary>
				public IDelegateCommand ShowConsolidatedMetricsCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowConsolidatedMetricsCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowConsolidatedMetricsCommand, (v) => _ShowConsolidatedMetricsCommand = v, value, ShowConsolidatedMetricsCommand_PROPERTYNAME,  DoShowConsolidatedMetricsCommandBeforeSet, DoShowConsolidatedMetricsCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowConsolidatedMetricsCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowConsolidatedMetricsCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowConsolidatedMetricsCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowMyLeadsCommand ===

				public const string ShowMyLeadsCommand_PROPERTYNAME = "ShowMyLeadsCommand";

				private IDelegateCommand _ShowMyLeadsCommand;
				///<summary>
				/// Propriété : ShowMyLeadsCommand
				///</summary>
				public IDelegateCommand ShowMyLeadsCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowMyLeadsCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowMyLeadsCommand, (v) => _ShowMyLeadsCommand = v, value, ShowMyLeadsCommand_PROPERTYNAME,  DoShowMyLeadsCommandBeforeSet, DoShowMyLeadsCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowMyLeadsCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowMyLeadsCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowMyLeadsCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowOverAllCommand ===

				public const string ShowOverAllCommand_PROPERTYNAME = "ShowOverAllCommand";

				private IDelegateCommand _ShowOverAllCommand;
				///<summary>
				/// Propriété : ShowOverAllCommand
				///</summary>
				public IDelegateCommand ShowOverAllCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowOverAllCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowOverAllCommand, (v) => _ShowOverAllCommand = v, value, ShowOverAllCommand_PROPERTYNAME,  DoShowOverAllCommandBeforeSet, DoShowOverAllCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowOverAllCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowOverAllCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowOverAllCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux collections ===

			// Notez que les propriétés de collection sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Collection : MBAllRichMessage ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection MBAllRichMessage aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionMBAllRichMessage_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionMBAllRichMessage_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionMBAllRichMessage_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection MBAllRichMessage (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionMBAllRichMessage_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionMBAllRichMessage_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionMBAllRichMessage_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection MBAllRichMessage aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionMBAllRichMessage_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionMBAllRichMessage_ItemPropertyChanged;
							OnMBAllRichMessageItemAdded((RichMessageItem)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection MBAllRichMessage (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionMBAllRichMessage_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnMBAllRichMessageItemRemoved((RichMessageItem)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionMBAllRichMessage_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionMBAllRichMessage_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionMBAllRichMessage_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionMBAllRichMessage_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(MBAllRichMessage_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyMBAllRichMessageDependencies();
				}

				private void WatchCollectionMBAllRichMessage_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(MBAllRichMessage_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyMBAllRichMessageDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection MBAllRichMessage.
				///</summary>
				protected virtual void OnMBAllRichMessageItemAdded(RichMessageItem p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection MBAllRichMessage.
				///</summary>
				protected virtual void OnMBAllRichMessageItemRemoved(RichMessageItem p_Item)
				{
				}

			#endregion

			#region === Collection : MBAllSalesToolsRichMessage ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection MBAllSalesToolsRichMessage aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionMBAllSalesToolsRichMessage_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionMBAllSalesToolsRichMessage_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionMBAllSalesToolsRichMessage_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection MBAllSalesToolsRichMessage (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionMBAllSalesToolsRichMessage_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionMBAllSalesToolsRichMessage_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionMBAllSalesToolsRichMessage_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection MBAllSalesToolsRichMessage aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionMBAllSalesToolsRichMessage_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionMBAllSalesToolsRichMessage_ItemPropertyChanged;
							OnMBAllSalesToolsRichMessageItemAdded((RichMessageItem)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection MBAllSalesToolsRichMessage (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionMBAllSalesToolsRichMessage_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnMBAllSalesToolsRichMessageItemRemoved((RichMessageItem)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionMBAllSalesToolsRichMessage_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionMBAllSalesToolsRichMessage_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionMBAllSalesToolsRichMessage_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionMBAllSalesToolsRichMessage_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(MBAllSalesToolsRichMessage_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyMBAllSalesToolsRichMessageDependencies();
				}

				private void WatchCollectionMBAllSalesToolsRichMessage_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(MBAllSalesToolsRichMessage_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyMBAllSalesToolsRichMessageDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection MBAllSalesToolsRichMessage.
				///</summary>
				protected virtual void OnMBAllSalesToolsRichMessageItemAdded(RichMessageItem p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection MBAllSalesToolsRichMessage.
				///</summary>
				protected virtual void OnMBAllSalesToolsRichMessageItemRemoved(RichMessageItem p_Item)
				{
				}

			#endregion

			#region === Collection : MBAllSelfTrainingRichMessage ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection MBAllSelfTrainingRichMessage aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionMBAllSelfTrainingRichMessage_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionMBAllSelfTrainingRichMessage_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionMBAllSelfTrainingRichMessage_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection MBAllSelfTrainingRichMessage (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionMBAllSelfTrainingRichMessage_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionMBAllSelfTrainingRichMessage_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionMBAllSelfTrainingRichMessage_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection MBAllSelfTrainingRichMessage aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionMBAllSelfTrainingRichMessage_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionMBAllSelfTrainingRichMessage_ItemPropertyChanged;
							OnMBAllSelfTrainingRichMessageItemAdded((RichMessageItem)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection MBAllSelfTrainingRichMessage (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionMBAllSelfTrainingRichMessage_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnMBAllSelfTrainingRichMessageItemRemoved((RichMessageItem)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionMBAllSelfTrainingRichMessage_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionMBAllSelfTrainingRichMessage_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionMBAllSelfTrainingRichMessage_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionMBAllSelfTrainingRichMessage_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(MBAllSelfTrainingRichMessage_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyMBAllSelfTrainingRichMessageDependencies();
				}

				private void WatchCollectionMBAllSelfTrainingRichMessage_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(MBAllSelfTrainingRichMessage_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyMBAllSelfTrainingRichMessageDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection MBAllSelfTrainingRichMessage.
				///</summary>
				protected virtual void OnMBAllSelfTrainingRichMessageItemAdded(RichMessageItem p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection MBAllSelfTrainingRichMessage.
				///</summary>
				protected virtual void OnMBAllSelfTrainingRichMessageItemRemoved(RichMessageItem p_Item)
				{
				}

			#endregion

			#region === Collection : ShowRoomCollections ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection ShowRoomCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionShowRoomCollections_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionShowRoomCollections_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionShowRoomCollections_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection ShowRoomCollections (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionShowRoomCollections_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionShowRoomCollections_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionShowRoomCollections_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection ShowRoomCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionShowRoomCollections_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionShowRoomCollections_ItemPropertyChanged;
							OnShowRoomCollectionsItemAdded((MyADA.Common.Services.Models.ShowroomModels)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection ShowRoomCollections (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionShowRoomCollections_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnShowRoomCollectionsItemRemoved((MyADA.Common.Services.Models.ShowroomModels)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionShowRoomCollections_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionShowRoomCollections_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionShowRoomCollections_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionShowRoomCollections_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ShowRoomCollections_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyShowRoomCollectionsDependencies();
				}

				private void WatchCollectionShowRoomCollections_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ShowRoomCollections_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyShowRoomCollectionsDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection ShowRoomCollections.
				///</summary>
				protected virtual void OnShowRoomCollectionsItemAdded(MyADA.Common.Services.Models.ShowroomModels p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection ShowRoomCollections.
				///</summary>
				protected virtual void OnShowRoomCollectionsItemRemoved(MyADA.Common.Services.Models.ShowroomModels p_Item)
				{
				}

			#endregion

			#region === Collection : UsersCollections ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection UsersCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionUsersCollections_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionUsersCollections_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionUsersCollections_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection UsersCollections (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionUsersCollections_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionUsersCollections_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionUsersCollections_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection UsersCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionUsersCollections_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionUsersCollections_ItemPropertyChanged;
							OnUsersCollectionsItemAdded((MyADA.Common.Services.Models.UserInfos)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection UsersCollections (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionUsersCollections_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnUsersCollectionsItemRemoved((MyADA.Common.Services.Models.UserInfos)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionUsersCollections_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionUsersCollections_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionUsersCollections_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionUsersCollections_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(UsersCollections_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyUsersCollectionsDependencies();
				}

				private void WatchCollectionUsersCollections_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(UsersCollections_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyUsersCollectionsDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection UsersCollections.
				///</summary>
				protected virtual void OnUsersCollectionsItemAdded(MyADA.Common.Services.Models.UserInfos p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection UsersCollections.
				///</summary>
				protected virtual void OnUsersCollectionsItemRemoved(MyADA.Common.Services.Models.UserInfos p_Item)
				{
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux commandes ===

			// Notez que les propriétés de commandes sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Commande : ShowLeadFromHomeCommand ===


				private bool ShowLeadFromHomeCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowLeadFromHomeCommand_CanExecute(l_Parameter);
				}

				private void ShowLeadFromHomeCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowLeadFromHomeCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowLeadFromHomeCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowLeadFromHomeCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowAudiNewsFromHomeCommand ===


				private bool ShowAudiNewsFromHomeCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowAudiNewsFromHomeCommand_CanExecute(l_Parameter);
				}

				private void ShowAudiNewsFromHomeCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowAudiNewsFromHomeCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowAudiNewsFromHomeCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowAudiNewsFromHomeCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowSalesToolFromHomeCommand ===


				private bool ShowSalesToolFromHomeCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowSalesToolFromHomeCommand_CanExecute(l_Parameter);
				}

				private void ShowSalesToolFromHomeCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowSalesToolFromHomeCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowSalesToolFromHomeCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowSalesToolFromHomeCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowSelfTrainingFromHomeCommand ===


				private bool ShowSelfTrainingFromHomeCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowSelfTrainingFromHomeCommand_CanExecute(l_Parameter);
				}

				private void ShowSelfTrainingFromHomeCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowSelfTrainingFromHomeCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowSelfTrainingFromHomeCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowSelfTrainingFromHomeCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowViewReportFromHomeCommand ===


				private bool ShowViewReportFromHomeCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowViewReportFromHomeCommand_CanExecute(l_Parameter);
				}

				private void ShowViewReportFromHomeCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowViewReportFromHomeCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowViewReportFromHomeCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowViewReportFromHomeCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowSettingsCommand ===


				private bool ShowSettingsCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowSettingsCommand_CanExecute(l_Parameter);
				}

				private void ShowSettingsCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowSettingsCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowSettingsCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowSettingsCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowRichMessageCommand ===


				private bool ShowRichMessageCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowRichMessageCommand_CanExecute(l_Parameter);
				}

				private void ShowRichMessageCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowRichMessageCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowRichMessageCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowRichMessageCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : SelectedUserCommand ===


				private bool SelectedUserCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnSelectedUserCommand_CanExecute(l_Parameter);
				}

				private void SelectedUserCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnSelectedUserCommand_Execute(l_Parameter);
				}

				protected virtual bool OnSelectedUserCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnSelectedUserCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : SelectedShowroomCommand ===


				private bool SelectedShowroomCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnSelectedShowroomCommand_CanExecute(l_Parameter);
				}

				private void SelectedShowroomCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnSelectedShowroomCommand_Execute(l_Parameter);
				}

				protected virtual bool OnSelectedShowroomCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnSelectedShowroomCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowLeadReportCommand ===


				private bool ShowLeadReportCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowLeadReportCommand_CanExecute(l_Parameter);
				}

				private void ShowLeadReportCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowLeadReportCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowLeadReportCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowLeadReportCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowConsolidatedMetricsCommand ===


				private bool ShowConsolidatedMetricsCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowConsolidatedMetricsCommand_CanExecute(l_Parameter);
				}

				private void ShowConsolidatedMetricsCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowConsolidatedMetricsCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowConsolidatedMetricsCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowConsolidatedMetricsCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowMyLeadsCommand ===


				private bool ShowMyLeadsCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowMyLeadsCommand_CanExecute(l_Parameter);
				}

				private void ShowMyLeadsCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowMyLeadsCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowMyLeadsCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowMyLeadsCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowOverAllCommand ===


				private bool ShowOverAllCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowOverAllCommand_CanExecute(l_Parameter);
				}

				private void ShowOverAllCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowOverAllCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowOverAllCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowOverAllCommand_Execute(object p_Parameter);

			#endregion


		#endregion

	}

	public partial class HomeViewModel : HomeViewModelBase
	{
	}
}
