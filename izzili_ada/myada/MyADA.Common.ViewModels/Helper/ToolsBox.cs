﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADA.Common.ViewModels.Helper
{
    public static class ToolsBox
    {
        /// <summary>
        /// Showroom config
        /// </summary>
        public enum ADAShowroomLeadConfig
        {
            managed,
            available
        }

        /// <summary>
        /// List of role in the application
        /// </summary>
        public enum ADARole {
            admin,
            manager,
            user
        }

        /// <summary>
        /// Leads status
        /// </summary>
        public enum ADALeadStatus
        {
            allocated,
            closed,
            New
        }

        /// <summary>
        /// Leads status color
        /// </summary>
        public enum ADALeadStatusColor
        {
            red,
            orange,
            gray
        }

        public static String SeletedListViewItemColor(string theme) {

            switch (theme)
            {
                case Constants.ThemeMySodexo:
                    return "#F07F13";
                default:
                    return "";
            }
        }


        public static String SeletedThemeName(string theme)
        {

            switch (theme)
            {
                case Constants.ThemeMySodexo:
                    return "Sodexo ";

                default:
                    return "";
            }

        }

        public static String ThemeNameToImage(string theme)
        {

            switch (theme)
            {
                case Constants.ThemeMySodexo:
                    return "";
                default:
                    return "";
            }

        }

    }
}
