﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML SettingsViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  CoreSDK.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;

namespace MyADA.Common.ViewModels
{
	public abstract partial class SettingsViewModelBase : CoreSDK.ViewModel
	{
		protected override void OnInitialize()
		{
			base.OnInitialize();

			// Initialisation de la collection UsersCollections.
			UsersCollections = new System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos>();
			// Initialisation de la collection PositionCollections.
			PositionCollections = new System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.PositionModel>();
			// Initialisation de la collection ShowRoomCollections.
			ShowRoomCollections = new System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels>();
			// Initialisation de la collection UserTitleCollections.
			UserTitleCollections = new System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserTitle>();
			// Initialisation de la collection DomainOfActionsCollections.
			DomainOfActionsCollections = new System.Collections.ObjectModel.ObservableCollection<DomainOfActionViewModelItem>();
			// Initialisation de la commande ShowMyInfosCommand.
			ShowMyInfosCommand = new DelegateCommand(ShowMyInfosCommand_CanExecute, ShowMyInfosCommand_Execute);
			// Initialisation de la commande ShowChangeRequestViewCommand.
			ShowChangeRequestViewCommand = new DelegateCommand(ShowChangeRequestViewCommand_CanExecute, ShowChangeRequestViewCommand_Execute);
			// Initialisation de la commande SelectUserCommand.
			SelectUserCommand = new DelegateCommand(SelectUserCommand_CanExecute, SelectUserCommand_Execute);
			// Initialisation de la commande SelectTitleCommand.
			SelectTitleCommand = new DelegateCommand(SelectTitleCommand_CanExecute, SelectTitleCommand_Execute);
			// Initialisation de la commande SelectUserPositionCommand.
			SelectUserPositionCommand = new DelegateCommand(SelectUserPositionCommand_CanExecute, SelectUserPositionCommand_Execute);
			// Initialisation de la commande SelectUserOrganizationCommand.
			SelectUserOrganizationCommand = new DelegateCommand(SelectUserOrganizationCommand_CanExecute, SelectUserOrganizationCommand_Execute);
			// Initialisation de la commande SelectUserDomainOfActionsCommand.
			SelectUserDomainOfActionsCommand = new DelegateCommand(SelectUserDomainOfActionsCommand_CanExecute, SelectUserDomainOfActionsCommand_Execute);
			// Initialisation de la commande SendRequestionCommand.
			SendRequestionCommand = new DelegateCommand(SendRequestionCommand_CanExecute, SendRequestionCommand_Execute);
			// Initialisation de la commande ChangeCommand.
			ChangeCommand = new DelegateCommand(ChangeCommand_CanExecute, ChangeCommand_Execute);
			// Initialisation de la commande CreateCommand.
			CreateCommand = new DelegateCommand(CreateCommand_CanExecute, CreateCommand_Execute);
			// Initialisation de la commande DeleteCommand.
			DeleteCommand = new DelegateCommand(DeleteCommand_CanExecute, DeleteCommand_Execute);
			// Initialisation de la commande ChangeRequestCommand.
			ChangeRequestCommand = new DelegateCommand(ChangeRequestCommand_CanExecute, ChangeRequestCommand_Execute);
			// Initialisation de la commande CreateRequestCommand.
			CreateRequestCommand = new DelegateCommand(CreateRequestCommand_CanExecute, CreateRequestCommand_Execute);
			// Initialisation de la commande DeleteRequestCommand.
			DeleteRequestCommand = new DelegateCommand(DeleteRequestCommand_CanExecute, DeleteRequestCommand_Execute);
		}

		#region === Propriétés ===

			#region === Propriété : UserId ===

				public const string UserId_PROPERTYNAME = "UserId";

				private string _UserId;
				///<summary>
				/// Propriété : UserId
				///</summary>
				public string UserId
				{
					get
					{
						return GetValue<string>(() => _UserId);
					}
					set
					{
						SetValue<string>(() => _UserId, (v) => _UserId = v, value, UserId_PROPERTYNAME,  DoUserIdBeforeSet, DoUserIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUserIdBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUserIdAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUserIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Title ===

				public const string Title_PROPERTYNAME = "Title";

				private string _Title;
				///<summary>
				/// Propriété : Title
				///</summary>
				public string Title
				{
					get
					{
						return GetValue<string>(() => _Title);
					}
					set
					{
						SetValue<string>(() => _Title, (v) => _Title = v, value, Title_PROPERTYNAME,  DoTitleBeforeSet, DoTitleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTitleBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTitleAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTitleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Name ===

				public const string Name_PROPERTYNAME = "Name";

				private string _Name;
				///<summary>
				/// Propriété : Name
				///</summary>
				public string Name
				{
					get
					{
						return GetValue<string>(() => _Name);
					}
					set
					{
						SetValue<string>(() => _Name, (v) => _Name = v, value, Name_PROPERTYNAME,  DoNameBeforeSet, DoNameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoNameBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoNameAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyNameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SurName ===

				public const string SurName_PROPERTYNAME = "SurName";

				private string _SurName;
				///<summary>
				/// Propriété : SurName
				///</summary>
				public string SurName
				{
					get
					{
						return GetValue<string>(() => _SurName);
					}
					set
					{
						SetValue<string>(() => _SurName, (v) => _SurName = v, value, SurName_PROPERTYNAME,  DoSurNameBeforeSet, DoSurNameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSurNameBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSurNameAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySurNameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Email ===

				public const string Email_PROPERTYNAME = "Email";

				private string _Email;
				///<summary>
				/// Propriété : Email
				///</summary>
				public string Email
				{
					get
					{
						return GetValue<string>(() => _Email);
					}
					set
					{
						SetValue<string>(() => _Email, (v) => _Email = v, value, Email_PROPERTYNAME,  DoEmailBeforeSet, DoEmailAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoEmailBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoEmailAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyEmailDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsChangeChecked ===

				public const string IsChangeChecked_PROPERTYNAME = "IsChangeChecked";

				private bool _IsChangeChecked;
				///<summary>
				/// Propriété : IsChangeChecked
				///</summary>
				public bool IsChangeChecked
				{
					get
					{
						return GetValue<bool>(() => _IsChangeChecked);
					}
					set
					{
						SetValue<bool>(() => _IsChangeChecked, (v) => _IsChangeChecked = v, value, IsChangeChecked_PROPERTYNAME,  DoIsChangeCheckedBeforeSet, DoIsChangeCheckedAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsChangeCheckedBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsChangeCheckedAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsChangeCheckedDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsCreateChecked ===

				public const string IsCreateChecked_PROPERTYNAME = "IsCreateChecked";

				private bool _IsCreateChecked;
				///<summary>
				/// Propriété : IsCreateChecked
				///</summary>
				public bool IsCreateChecked
				{
					get
					{
						return GetValue<bool>(() => _IsCreateChecked);
					}
					set
					{
						SetValue<bool>(() => _IsCreateChecked, (v) => _IsCreateChecked = v, value, IsCreateChecked_PROPERTYNAME,  DoIsCreateCheckedBeforeSet, DoIsCreateCheckedAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsCreateCheckedBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsCreateCheckedAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsCreateCheckedDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsDeleteChecked ===

				public const string IsDeleteChecked_PROPERTYNAME = "IsDeleteChecked";

				private bool _IsDeleteChecked;
				///<summary>
				/// Propriété : IsDeleteChecked
				///</summary>
				public bool IsDeleteChecked
				{
					get
					{
						return GetValue<bool>(() => _IsDeleteChecked);
					}
					set
					{
						SetValue<bool>(() => _IsDeleteChecked, (v) => _IsDeleteChecked = v, value, IsDeleteChecked_PROPERTYNAME,  DoIsDeleteCheckedBeforeSet, DoIsDeleteCheckedAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsDeleteCheckedBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsDeleteCheckedAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsDeleteCheckedDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsChanging ===

				public const string IsChanging_PROPERTYNAME = "IsChanging";

				private bool _IsChanging;
				///<summary>
				/// Propriété : IsChanging
				///</summary>
				public bool IsChanging
				{
					get
					{
						return GetValue<bool>(() => _IsChanging);
					}
					set
					{
						SetValue<bool>(() => _IsChanging, (v) => _IsChanging = v, value, IsChanging_PROPERTYNAME,  DoIsChangingBeforeSet, DoIsChangingAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsChangingBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsChangingAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsChangingDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsCreation ===

				public const string IsCreation_PROPERTYNAME = "IsCreation";

				private bool _IsCreation;
				///<summary>
				/// Propriété : IsCreation
				///</summary>
				public bool IsCreation
				{
					get
					{
						return GetValue<bool>(() => _IsCreation);
					}
					set
					{
						SetValue<bool>(() => _IsCreation, (v) => _IsCreation = v, value, IsCreation_PROPERTYNAME,  DoIsCreationBeforeSet, DoIsCreationAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsCreationBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsCreationAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsCreationDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : EnableMe ===

				public const string EnableMe_PROPERTYNAME = "EnableMe";

				private bool _EnableMe;
				///<summary>
				/// Propriété : EnableMe
				///</summary>
				public bool EnableMe
				{
					get
					{
						return GetValue<bool>(() => _EnableMe);
					}
					set
					{
						SetValue<bool>(() => _EnableMe, (v) => _EnableMe = v, value, EnableMe_PROPERTYNAME,  DoEnableMeBeforeSet, DoEnableMeAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoEnableMeBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoEnableMeAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyEnableMeDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DisableMe ===

				public const string DisableMe_PROPERTYNAME = "DisableMe";

				private bool _DisableMe;
				///<summary>
				/// Propriété : DisableMe
				///</summary>
				public bool DisableMe
				{
					get
					{
						return GetValue<bool>(() => _DisableMe);
					}
					set
					{
						SetValue<bool>(() => _DisableMe, (v) => _DisableMe = v, value, DisableMe_PROPERTYNAME,  DoDisableMeBeforeSet, DoDisableMeAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDisableMeBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDisableMeAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDisableMeDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : EnableSomeoneElse ===

				public const string EnableSomeoneElse_PROPERTYNAME = "EnableSomeoneElse";

				private bool _EnableSomeoneElse;
				///<summary>
				/// Propriété : EnableSomeoneElse
				///</summary>
				public bool EnableSomeoneElse
				{
					get
					{
						return GetValue<bool>(() => _EnableSomeoneElse);
					}
					set
					{
						SetValue<bool>(() => _EnableSomeoneElse, (v) => _EnableSomeoneElse = v, value, EnableSomeoneElse_PROPERTYNAME,  DoEnableSomeoneElseBeforeSet, DoEnableSomeoneElseAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoEnableSomeoneElseBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoEnableSomeoneElseAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyEnableSomeoneElseDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DisableSomeOneElse ===

				public const string DisableSomeOneElse_PROPERTYNAME = "DisableSomeOneElse";

				private bool _DisableSomeOneElse;
				///<summary>
				/// Propriété : DisableSomeOneElse
				///</summary>
				public bool DisableSomeOneElse
				{
					get
					{
						return GetValue<bool>(() => _DisableSomeOneElse);
					}
					set
					{
						SetValue<bool>(() => _DisableSomeOneElse, (v) => _DisableSomeOneElse = v, value, DisableSomeOneElse_PROPERTYNAME,  DoDisableSomeOneElseBeforeSet, DoDisableSomeOneElseAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDisableSomeOneElseBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDisableSomeOneElseAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDisableSomeOneElseDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsDeleting ===

				public const string IsDeleting_PROPERTYNAME = "IsDeleting";

				private bool _IsDeleting;
				///<summary>
				/// Propriété : IsDeleting
				///</summary>
				public bool IsDeleting
				{
					get
					{
						return GetValue<bool>(() => _IsDeleting);
					}
					set
					{
						SetValue<bool>(() => _IsDeleting, (v) => _IsDeleting = v, value, IsDeleting_PROPERTYNAME,  DoIsDeletingBeforeSet, DoIsDeletingAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsDeletingBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsDeletingAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsDeletingDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowOrganizationPicker ===

				public const string ShowOrganizationPicker_PROPERTYNAME = "ShowOrganizationPicker";

				private bool _ShowOrganizationPicker;
				///<summary>
				/// Propriété : ShowOrganizationPicker
				///</summary>
				public bool ShowOrganizationPicker
				{
					get
					{
						return GetValue<bool>(() => _ShowOrganizationPicker);
					}
					set
					{
						SetValue<bool>(() => _ShowOrganizationPicker, (v) => _ShowOrganizationPicker = v, value, ShowOrganizationPicker_PROPERTYNAME,  DoShowOrganizationPickerBeforeSet, DoShowOrganizationPickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowOrganizationPickerBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowOrganizationPickerAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowOrganizationPickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowEmail ===

				public const string ShowEmail_PROPERTYNAME = "ShowEmail";

				private bool _ShowEmail;
				///<summary>
				/// Propriété : ShowEmail
				///</summary>
				public bool ShowEmail
				{
					get
					{
						return GetValue<bool>(() => _ShowEmail);
					}
					set
					{
						SetValue<bool>(() => _ShowEmail, (v) => _ShowEmail = v, value, ShowEmail_PROPERTYNAME,  DoShowEmailBeforeSet, DoShowEmailAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowEmailBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowEmailAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowEmailDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CurrentUserProfileInfos ===

				public const string CurrentUserProfileInfos_PROPERTYNAME = "CurrentUserProfileInfos";

				private MyADA.Common.Services.Models.CurrentUserProfile _CurrentUserProfileInfos;
				///<summary>
				/// Propriété : CurrentUserProfileInfos
				///</summary>
				public MyADA.Common.Services.Models.CurrentUserProfile CurrentUserProfileInfos
				{
					get
					{
						return GetValue<MyADA.Common.Services.Models.CurrentUserProfile>(() => _CurrentUserProfileInfos);
					}
					set
					{
						SetValue<MyADA.Common.Services.Models.CurrentUserProfile>(() => _CurrentUserProfileInfos, (v) => _CurrentUserProfileInfos = v, value, CurrentUserProfileInfos_PROPERTYNAME,  DoCurrentUserProfileInfosBeforeSet, DoCurrentUserProfileInfosAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCurrentUserProfileInfosBeforeSet(string p_PropertyName, MyADA.Common.Services.Models.CurrentUserProfile p_OldValue, MyADA.Common.Services.Models.CurrentUserProfile p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCurrentUserProfileInfosAfterSet(string p_PropertyName, MyADA.Common.Services.Models.CurrentUserProfile p_OldValue, MyADA.Common.Services.Models.CurrentUserProfile p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCurrentUserProfileInfosDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : PlaceHolderForSomeOneElsePicker ===

				public const string PlaceHolderForSomeOneElsePicker_PROPERTYNAME = "PlaceHolderForSomeOneElsePicker";

				private string _PlaceHolderForSomeOneElsePicker;
				///<summary>
				/// Propriété : PlaceHolderForSomeOneElsePicker
				///</summary>
				public string PlaceHolderForSomeOneElsePicker
				{
					get
					{
						return GetValue<string>(() => _PlaceHolderForSomeOneElsePicker);
					}
					set
					{
						SetValue<string>(() => _PlaceHolderForSomeOneElsePicker, (v) => _PlaceHolderForSomeOneElsePicker = v, value, PlaceHolderForSomeOneElsePicker_PROPERTYNAME,  DoPlaceHolderForSomeOneElsePickerBeforeSet, DoPlaceHolderForSomeOneElsePickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoPlaceHolderForSomeOneElsePickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoPlaceHolderForSomeOneElsePickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyPlaceHolderForSomeOneElsePickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DefaultPlaceHolderForSomeOneElsePicker ===

				public const string DefaultPlaceHolderForSomeOneElsePicker_PROPERTYNAME = "DefaultPlaceHolderForSomeOneElsePicker";

				private string _DefaultPlaceHolderForSomeOneElsePicker;
				///<summary>
				/// Propriété : DefaultPlaceHolderForSomeOneElsePicker
				///</summary>
				public string DefaultPlaceHolderForSomeOneElsePicker
				{
					get
					{
						return GetValue<string>(() => _DefaultPlaceHolderForSomeOneElsePicker);
					}
					set
					{
						SetValue<string>(() => _DefaultPlaceHolderForSomeOneElsePicker, (v) => _DefaultPlaceHolderForSomeOneElsePicker = v, value, DefaultPlaceHolderForSomeOneElsePicker_PROPERTYNAME,  DoDefaultPlaceHolderForSomeOneElsePickerBeforeSet, DoDefaultPlaceHolderForSomeOneElsePickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDefaultPlaceHolderForSomeOneElsePickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDefaultPlaceHolderForSomeOneElsePickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDefaultPlaceHolderForSomeOneElsePickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : HeaderTextHolderForSomeOneElsePicker ===

				public const string HeaderTextHolderForSomeOneElsePicker_PROPERTYNAME = "HeaderTextHolderForSomeOneElsePicker";

				private string _HeaderTextHolderForSomeOneElsePicker;
				///<summary>
				/// Propriété : HeaderTextHolderForSomeOneElsePicker
				///</summary>
				public string HeaderTextHolderForSomeOneElsePicker
				{
					get
					{
						return GetValue<string>(() => _HeaderTextHolderForSomeOneElsePicker);
					}
					set
					{
						SetValue<string>(() => _HeaderTextHolderForSomeOneElsePicker, (v) => _HeaderTextHolderForSomeOneElsePicker = v, value, HeaderTextHolderForSomeOneElsePicker_PROPERTYNAME,  DoHeaderTextHolderForSomeOneElsePickerBeforeSet, DoHeaderTextHolderForSomeOneElsePickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoHeaderTextHolderForSomeOneElsePickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoHeaderTextHolderForSomeOneElsePickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyHeaderTextHolderForSomeOneElsePickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : PlaceHolderForTitlePicker ===

				public const string PlaceHolderForTitlePicker_PROPERTYNAME = "PlaceHolderForTitlePicker";

				private string _PlaceHolderForTitlePicker;
				///<summary>
				/// Propriété : PlaceHolderForTitlePicker
				///</summary>
				public string PlaceHolderForTitlePicker
				{
					get
					{
						return GetValue<string>(() => _PlaceHolderForTitlePicker);
					}
					set
					{
						SetValue<string>(() => _PlaceHolderForTitlePicker, (v) => _PlaceHolderForTitlePicker = v, value, PlaceHolderForTitlePicker_PROPERTYNAME,  DoPlaceHolderForTitlePickerBeforeSet, DoPlaceHolderForTitlePickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoPlaceHolderForTitlePickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoPlaceHolderForTitlePickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyPlaceHolderForTitlePickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DefaultPlaceHolderForTitlePicker ===

				public const string DefaultPlaceHolderForTitlePicker_PROPERTYNAME = "DefaultPlaceHolderForTitlePicker";

				private string _DefaultPlaceHolderForTitlePicker;
				///<summary>
				/// Propriété : DefaultPlaceHolderForTitlePicker
				///</summary>
				public string DefaultPlaceHolderForTitlePicker
				{
					get
					{
						return GetValue<string>(() => _DefaultPlaceHolderForTitlePicker);
					}
					set
					{
						SetValue<string>(() => _DefaultPlaceHolderForTitlePicker, (v) => _DefaultPlaceHolderForTitlePicker = v, value, DefaultPlaceHolderForTitlePicker_PROPERTYNAME,  DoDefaultPlaceHolderForTitlePickerBeforeSet, DoDefaultPlaceHolderForTitlePickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDefaultPlaceHolderForTitlePickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDefaultPlaceHolderForTitlePickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDefaultPlaceHolderForTitlePickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : HeaderTextHolderForTitlePicker ===

				public const string HeaderTextHolderForTitlePicker_PROPERTYNAME = "HeaderTextHolderForTitlePicker";

				private string _HeaderTextHolderForTitlePicker;
				///<summary>
				/// Propriété : HeaderTextHolderForTitlePicker
				///</summary>
				public string HeaderTextHolderForTitlePicker
				{
					get
					{
						return GetValue<string>(() => _HeaderTextHolderForTitlePicker);
					}
					set
					{
						SetValue<string>(() => _HeaderTextHolderForTitlePicker, (v) => _HeaderTextHolderForTitlePicker = v, value, HeaderTextHolderForTitlePicker_PROPERTYNAME,  DoHeaderTextHolderForTitlePickerBeforeSet, DoHeaderTextHolderForTitlePickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoHeaderTextHolderForTitlePickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoHeaderTextHolderForTitlePickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyHeaderTextHolderForTitlePickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : PlaceHolderForPositionPicker ===

				public const string PlaceHolderForPositionPicker_PROPERTYNAME = "PlaceHolderForPositionPicker";

				private string _PlaceHolderForPositionPicker;
				///<summary>
				/// Propriété : PlaceHolderForPositionPicker
				///</summary>
				public string PlaceHolderForPositionPicker
				{
					get
					{
						return GetValue<string>(() => _PlaceHolderForPositionPicker);
					}
					set
					{
						SetValue<string>(() => _PlaceHolderForPositionPicker, (v) => _PlaceHolderForPositionPicker = v, value, PlaceHolderForPositionPicker_PROPERTYNAME,  DoPlaceHolderForPositionPickerBeforeSet, DoPlaceHolderForPositionPickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoPlaceHolderForPositionPickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoPlaceHolderForPositionPickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyPlaceHolderForPositionPickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DefaultPlaceHolderForPositionPicker ===

				public const string DefaultPlaceHolderForPositionPicker_PROPERTYNAME = "DefaultPlaceHolderForPositionPicker";

				private string _DefaultPlaceHolderForPositionPicker;
				///<summary>
				/// Propriété : DefaultPlaceHolderForPositionPicker
				///</summary>
				public string DefaultPlaceHolderForPositionPicker
				{
					get
					{
						return GetValue<string>(() => _DefaultPlaceHolderForPositionPicker);
					}
					set
					{
						SetValue<string>(() => _DefaultPlaceHolderForPositionPicker, (v) => _DefaultPlaceHolderForPositionPicker = v, value, DefaultPlaceHolderForPositionPicker_PROPERTYNAME,  DoDefaultPlaceHolderForPositionPickerBeforeSet, DoDefaultPlaceHolderForPositionPickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDefaultPlaceHolderForPositionPickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDefaultPlaceHolderForPositionPickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDefaultPlaceHolderForPositionPickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : HeaderTextHolderForPositionPicker ===

				public const string HeaderTextHolderForPositionPicker_PROPERTYNAME = "HeaderTextHolderForPositionPicker";

				private string _HeaderTextHolderForPositionPicker;
				///<summary>
				/// Propriété : HeaderTextHolderForPositionPicker
				///</summary>
				public string HeaderTextHolderForPositionPicker
				{
					get
					{
						return GetValue<string>(() => _HeaderTextHolderForPositionPicker);
					}
					set
					{
						SetValue<string>(() => _HeaderTextHolderForPositionPicker, (v) => _HeaderTextHolderForPositionPicker = v, value, HeaderTextHolderForPositionPicker_PROPERTYNAME,  DoHeaderTextHolderForPositionPickerBeforeSet, DoHeaderTextHolderForPositionPickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoHeaderTextHolderForPositionPickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoHeaderTextHolderForPositionPickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyHeaderTextHolderForPositionPickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : PlaceHolderForOrganizationPicker ===

				public const string PlaceHolderForOrganizationPicker_PROPERTYNAME = "PlaceHolderForOrganizationPicker";

				private string _PlaceHolderForOrganizationPicker;
				///<summary>
				/// Propriété : PlaceHolderForOrganizationPicker
				///</summary>
				public string PlaceHolderForOrganizationPicker
				{
					get
					{
						return GetValue<string>(() => _PlaceHolderForOrganizationPicker);
					}
					set
					{
						SetValue<string>(() => _PlaceHolderForOrganizationPicker, (v) => _PlaceHolderForOrganizationPicker = v, value, PlaceHolderForOrganizationPicker_PROPERTYNAME,  DoPlaceHolderForOrganizationPickerBeforeSet, DoPlaceHolderForOrganizationPickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoPlaceHolderForOrganizationPickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoPlaceHolderForOrganizationPickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyPlaceHolderForOrganizationPickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DefaultPlaceHolderForOrganizationPicker ===

				public const string DefaultPlaceHolderForOrganizationPicker_PROPERTYNAME = "DefaultPlaceHolderForOrganizationPicker";

				private string _DefaultPlaceHolderForOrganizationPicker;
				///<summary>
				/// Propriété : DefaultPlaceHolderForOrganizationPicker
				///</summary>
				public string DefaultPlaceHolderForOrganizationPicker
				{
					get
					{
						return GetValue<string>(() => _DefaultPlaceHolderForOrganizationPicker);
					}
					set
					{
						SetValue<string>(() => _DefaultPlaceHolderForOrganizationPicker, (v) => _DefaultPlaceHolderForOrganizationPicker = v, value, DefaultPlaceHolderForOrganizationPicker_PROPERTYNAME,  DoDefaultPlaceHolderForOrganizationPickerBeforeSet, DoDefaultPlaceHolderForOrganizationPickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDefaultPlaceHolderForOrganizationPickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDefaultPlaceHolderForOrganizationPickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDefaultPlaceHolderForOrganizationPickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : HeaderTextHolderForOrganizationPicker ===

				public const string HeaderTextHolderForOrganizationPicker_PROPERTYNAME = "HeaderTextHolderForOrganizationPicker";

				private string _HeaderTextHolderForOrganizationPicker;
				///<summary>
				/// Propriété : HeaderTextHolderForOrganizationPicker
				///</summary>
				public string HeaderTextHolderForOrganizationPicker
				{
					get
					{
						return GetValue<string>(() => _HeaderTextHolderForOrganizationPicker);
					}
					set
					{
						SetValue<string>(() => _HeaderTextHolderForOrganizationPicker, (v) => _HeaderTextHolderForOrganizationPicker = v, value, HeaderTextHolderForOrganizationPicker_PROPERTYNAME,  DoHeaderTextHolderForOrganizationPickerBeforeSet, DoHeaderTextHolderForOrganizationPickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoHeaderTextHolderForOrganizationPickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoHeaderTextHolderForOrganizationPickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyHeaderTextHolderForOrganizationPickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : PlaceHolderForDomainOfActionsPicker ===

				public const string PlaceHolderForDomainOfActionsPicker_PROPERTYNAME = "PlaceHolderForDomainOfActionsPicker";

				private string _PlaceHolderForDomainOfActionsPicker;
				///<summary>
				/// Propriété : PlaceHolderForDomainOfActionsPicker
				///</summary>
				public string PlaceHolderForDomainOfActionsPicker
				{
					get
					{
						return GetValue<string>(() => _PlaceHolderForDomainOfActionsPicker);
					}
					set
					{
						SetValue<string>(() => _PlaceHolderForDomainOfActionsPicker, (v) => _PlaceHolderForDomainOfActionsPicker = v, value, PlaceHolderForDomainOfActionsPicker_PROPERTYNAME,  DoPlaceHolderForDomainOfActionsPickerBeforeSet, DoPlaceHolderForDomainOfActionsPickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoPlaceHolderForDomainOfActionsPickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoPlaceHolderForDomainOfActionsPickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyPlaceHolderForDomainOfActionsPickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DefaultPlaceHolderForDomainOfActionsPicker ===

				public const string DefaultPlaceHolderForDomainOfActionsPicker_PROPERTYNAME = "DefaultPlaceHolderForDomainOfActionsPicker";

				private string _DefaultPlaceHolderForDomainOfActionsPicker;
				///<summary>
				/// Propriété : DefaultPlaceHolderForDomainOfActionsPicker
				///</summary>
				public string DefaultPlaceHolderForDomainOfActionsPicker
				{
					get
					{
						return GetValue<string>(() => _DefaultPlaceHolderForDomainOfActionsPicker);
					}
					set
					{
						SetValue<string>(() => _DefaultPlaceHolderForDomainOfActionsPicker, (v) => _DefaultPlaceHolderForDomainOfActionsPicker = v, value, DefaultPlaceHolderForDomainOfActionsPicker_PROPERTYNAME,  DoDefaultPlaceHolderForDomainOfActionsPickerBeforeSet, DoDefaultPlaceHolderForDomainOfActionsPickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDefaultPlaceHolderForDomainOfActionsPickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDefaultPlaceHolderForDomainOfActionsPickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDefaultPlaceHolderForDomainOfActionsPickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : HeaderTextHolderForDomainOfActionsPicker ===

				public const string HeaderTextHolderForDomainOfActionsPicker_PROPERTYNAME = "HeaderTextHolderForDomainOfActionsPicker";

				private string _HeaderTextHolderForDomainOfActionsPicker;
				///<summary>
				/// Propriété : HeaderTextHolderForDomainOfActionsPicker
				///</summary>
				public string HeaderTextHolderForDomainOfActionsPicker
				{
					get
					{
						return GetValue<string>(() => _HeaderTextHolderForDomainOfActionsPicker);
					}
					set
					{
						SetValue<string>(() => _HeaderTextHolderForDomainOfActionsPicker, (v) => _HeaderTextHolderForDomainOfActionsPicker = v, value, HeaderTextHolderForDomainOfActionsPicker_PROPERTYNAME,  DoHeaderTextHolderForDomainOfActionsPickerBeforeSet, DoHeaderTextHolderForDomainOfActionsPickerAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoHeaderTextHolderForDomainOfActionsPickerBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoHeaderTextHolderForDomainOfActionsPickerAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyHeaderTextHolderForDomainOfActionsPickerDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SeletedShowroom ===

				public const string SeletedShowroom_PROPERTYNAME = "SeletedShowroom";

				private MyADA.Common.Services.Models.ShowroomModels _SeletedShowroom;
				///<summary>
				/// Propriété : SeletedShowroom
				///</summary>
				public MyADA.Common.Services.Models.ShowroomModels SeletedShowroom
				{
					get
					{
						return GetValue<MyADA.Common.Services.Models.ShowroomModels>(() => _SeletedShowroom);
					}
					set
					{
						SetValue<MyADA.Common.Services.Models.ShowroomModels>(() => _SeletedShowroom, (v) => _SeletedShowroom = v, value, SeletedShowroom_PROPERTYNAME,  DoSeletedShowroomBeforeSet, DoSeletedShowroomAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSeletedShowroomBeforeSet(string p_PropertyName, MyADA.Common.Services.Models.ShowroomModels p_OldValue, MyADA.Common.Services.Models.ShowroomModels p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSeletedShowroomAfterSet(string p_PropertyName, MyADA.Common.Services.Models.ShowroomModels p_OldValue, MyADA.Common.Services.Models.ShowroomModels p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySeletedShowroomDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UsersCollections ===

				public const string UsersCollections_PROPERTYNAME = "UsersCollections";

				private System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> _UsersCollections;
				///<summary>
				/// Propriété : UsersCollections
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> UsersCollections
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos>>(() => _UsersCollections);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos>>(() => _UsersCollections, (v) => _UsersCollections = v, value, UsersCollections_PROPERTYNAME,  DoUsersCollectionsBeforeSet, DoUsersCollectionsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUsersCollectionsBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionUsersCollections_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUsersCollectionsAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionUsersCollections_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUsersCollectionsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : PositionCollections ===

				public const string PositionCollections_PROPERTYNAME = "PositionCollections";

				private System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.PositionModel> _PositionCollections;
				///<summary>
				/// Propriété : PositionCollections
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.PositionModel> PositionCollections
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.PositionModel>>(() => _PositionCollections);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.PositionModel>>(() => _PositionCollections, (v) => _PositionCollections = v, value, PositionCollections_PROPERTYNAME,  DoPositionCollectionsBeforeSet, DoPositionCollectionsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoPositionCollectionsBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.PositionModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.PositionModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionPositionCollections_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoPositionCollectionsAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.PositionModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.PositionModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionPositionCollections_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyPositionCollectionsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowRoomCollections ===

				public const string ShowRoomCollections_PROPERTYNAME = "ShowRoomCollections";

				private System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels> _ShowRoomCollections;
				///<summary>
				/// Propriété : ShowRoomCollections
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels> ShowRoomCollections
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels>>(() => _ShowRoomCollections);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels>>(() => _ShowRoomCollections, (v) => _ShowRoomCollections = v, value, ShowRoomCollections_PROPERTYNAME,  DoShowRoomCollectionsBeforeSet, DoShowRoomCollectionsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowRoomCollectionsBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionShowRoomCollections_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowRoomCollectionsAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.ShowroomModels> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionShowRoomCollections_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowRoomCollectionsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UserTitleCollections ===

				public const string UserTitleCollections_PROPERTYNAME = "UserTitleCollections";

				private System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserTitle> _UserTitleCollections;
				///<summary>
				/// Propriété : UserTitleCollections
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserTitle> UserTitleCollections
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserTitle>>(() => _UserTitleCollections);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserTitle>>(() => _UserTitleCollections, (v) => _UserTitleCollections = v, value, UserTitleCollections_PROPERTYNAME,  DoUserTitleCollectionsBeforeSet, DoUserTitleCollectionsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUserTitleCollectionsBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserTitle> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserTitle> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionUserTitleCollections_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUserTitleCollectionsAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserTitle> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserTitle> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionUserTitleCollections_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUserTitleCollectionsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DomainOfActionsCollections ===

				public const string DomainOfActionsCollections_PROPERTYNAME = "DomainOfActionsCollections";

				private System.Collections.ObjectModel.ObservableCollection<DomainOfActionViewModelItem> _DomainOfActionsCollections;
				///<summary>
				/// Propriété : DomainOfActionsCollections
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<DomainOfActionViewModelItem> DomainOfActionsCollections
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<DomainOfActionViewModelItem>>(() => _DomainOfActionsCollections);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<DomainOfActionViewModelItem>>(() => _DomainOfActionsCollections, (v) => _DomainOfActionsCollections = v, value, DomainOfActionsCollections_PROPERTYNAME,  DoDomainOfActionsCollectionsBeforeSet, DoDomainOfActionsCollectionsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDomainOfActionsCollectionsBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<DomainOfActionViewModelItem> p_OldValue, System.Collections.ObjectModel.ObservableCollection<DomainOfActionViewModelItem> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionDomainOfActionsCollections_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDomainOfActionsCollectionsAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<DomainOfActionViewModelItem> p_OldValue, System.Collections.ObjectModel.ObservableCollection<DomainOfActionViewModelItem> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionDomainOfActionsCollections_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDomainOfActionsCollectionsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowMyInfosCommand ===

				public const string ShowMyInfosCommand_PROPERTYNAME = "ShowMyInfosCommand";

				private IDelegateCommand _ShowMyInfosCommand;
				///<summary>
				/// Propriété : ShowMyInfosCommand
				///</summary>
				public IDelegateCommand ShowMyInfosCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowMyInfosCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowMyInfosCommand, (v) => _ShowMyInfosCommand = v, value, ShowMyInfosCommand_PROPERTYNAME,  DoShowMyInfosCommandBeforeSet, DoShowMyInfosCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowMyInfosCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowMyInfosCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowMyInfosCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowChangeRequestViewCommand ===

				public const string ShowChangeRequestViewCommand_PROPERTYNAME = "ShowChangeRequestViewCommand";

				private IDelegateCommand _ShowChangeRequestViewCommand;
				///<summary>
				/// Propriété : ShowChangeRequestViewCommand
				///</summary>
				public IDelegateCommand ShowChangeRequestViewCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowChangeRequestViewCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowChangeRequestViewCommand, (v) => _ShowChangeRequestViewCommand = v, value, ShowChangeRequestViewCommand_PROPERTYNAME,  DoShowChangeRequestViewCommandBeforeSet, DoShowChangeRequestViewCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowChangeRequestViewCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowChangeRequestViewCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowChangeRequestViewCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectUserCommand ===

				public const string SelectUserCommand_PROPERTYNAME = "SelectUserCommand";

				private IDelegateCommand _SelectUserCommand;
				///<summary>
				/// Propriété : SelectUserCommand
				///</summary>
				public IDelegateCommand SelectUserCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _SelectUserCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _SelectUserCommand, (v) => _SelectUserCommand = v, value, SelectUserCommand_PROPERTYNAME,  DoSelectUserCommandBeforeSet, DoSelectUserCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectUserCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectUserCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectUserCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectTitleCommand ===

				public const string SelectTitleCommand_PROPERTYNAME = "SelectTitleCommand";

				private IDelegateCommand _SelectTitleCommand;
				///<summary>
				/// Propriété : SelectTitleCommand
				///</summary>
				public IDelegateCommand SelectTitleCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _SelectTitleCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _SelectTitleCommand, (v) => _SelectTitleCommand = v, value, SelectTitleCommand_PROPERTYNAME,  DoSelectTitleCommandBeforeSet, DoSelectTitleCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectTitleCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectTitleCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectTitleCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectUserPositionCommand ===

				public const string SelectUserPositionCommand_PROPERTYNAME = "SelectUserPositionCommand";

				private IDelegateCommand _SelectUserPositionCommand;
				///<summary>
				/// Propriété : SelectUserPositionCommand
				///</summary>
				public IDelegateCommand SelectUserPositionCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _SelectUserPositionCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _SelectUserPositionCommand, (v) => _SelectUserPositionCommand = v, value, SelectUserPositionCommand_PROPERTYNAME,  DoSelectUserPositionCommandBeforeSet, DoSelectUserPositionCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectUserPositionCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectUserPositionCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectUserPositionCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectUserOrganizationCommand ===

				public const string SelectUserOrganizationCommand_PROPERTYNAME = "SelectUserOrganizationCommand";

				private IDelegateCommand _SelectUserOrganizationCommand;
				///<summary>
				/// Propriété : SelectUserOrganizationCommand
				///</summary>
				public IDelegateCommand SelectUserOrganizationCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _SelectUserOrganizationCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _SelectUserOrganizationCommand, (v) => _SelectUserOrganizationCommand = v, value, SelectUserOrganizationCommand_PROPERTYNAME,  DoSelectUserOrganizationCommandBeforeSet, DoSelectUserOrganizationCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectUserOrganizationCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectUserOrganizationCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectUserOrganizationCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectUserDomainOfActionsCommand ===

				public const string SelectUserDomainOfActionsCommand_PROPERTYNAME = "SelectUserDomainOfActionsCommand";

				private IDelegateCommand _SelectUserDomainOfActionsCommand;
				///<summary>
				/// Propriété : SelectUserDomainOfActionsCommand
				///</summary>
				public IDelegateCommand SelectUserDomainOfActionsCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _SelectUserDomainOfActionsCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _SelectUserDomainOfActionsCommand, (v) => _SelectUserDomainOfActionsCommand = v, value, SelectUserDomainOfActionsCommand_PROPERTYNAME,  DoSelectUserDomainOfActionsCommandBeforeSet, DoSelectUserDomainOfActionsCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectUserDomainOfActionsCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectUserDomainOfActionsCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectUserDomainOfActionsCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SendRequestionCommand ===

				public const string SendRequestionCommand_PROPERTYNAME = "SendRequestionCommand";

				private IDelegateCommand _SendRequestionCommand;
				///<summary>
				/// Propriété : SendRequestionCommand
				///</summary>
				public IDelegateCommand SendRequestionCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _SendRequestionCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _SendRequestionCommand, (v) => _SendRequestionCommand = v, value, SendRequestionCommand_PROPERTYNAME,  DoSendRequestionCommandBeforeSet, DoSendRequestionCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSendRequestionCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSendRequestionCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySendRequestionCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ChangeCommand ===

				public const string ChangeCommand_PROPERTYNAME = "ChangeCommand";

				private IDelegateCommand _ChangeCommand;
				///<summary>
				/// Propriété : ChangeCommand
				///</summary>
				public IDelegateCommand ChangeCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ChangeCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ChangeCommand, (v) => _ChangeCommand = v, value, ChangeCommand_PROPERTYNAME,  DoChangeCommandBeforeSet, DoChangeCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoChangeCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoChangeCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyChangeCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CreateCommand ===

				public const string CreateCommand_PROPERTYNAME = "CreateCommand";

				private IDelegateCommand _CreateCommand;
				///<summary>
				/// Propriété : CreateCommand
				///</summary>
				public IDelegateCommand CreateCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _CreateCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _CreateCommand, (v) => _CreateCommand = v, value, CreateCommand_PROPERTYNAME,  DoCreateCommandBeforeSet, DoCreateCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCreateCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCreateCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCreateCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DeleteCommand ===

				public const string DeleteCommand_PROPERTYNAME = "DeleteCommand";

				private IDelegateCommand _DeleteCommand;
				///<summary>
				/// Propriété : DeleteCommand
				///</summary>
				public IDelegateCommand DeleteCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _DeleteCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _DeleteCommand, (v) => _DeleteCommand = v, value, DeleteCommand_PROPERTYNAME,  DoDeleteCommandBeforeSet, DoDeleteCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDeleteCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDeleteCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDeleteCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ChangeRequestCommand ===

				public const string ChangeRequestCommand_PROPERTYNAME = "ChangeRequestCommand";

				private IDelegateCommand _ChangeRequestCommand;
				///<summary>
				/// Propriété : ChangeRequestCommand
				///</summary>
				public IDelegateCommand ChangeRequestCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ChangeRequestCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ChangeRequestCommand, (v) => _ChangeRequestCommand = v, value, ChangeRequestCommand_PROPERTYNAME,  DoChangeRequestCommandBeforeSet, DoChangeRequestCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoChangeRequestCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoChangeRequestCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyChangeRequestCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CreateRequestCommand ===

				public const string CreateRequestCommand_PROPERTYNAME = "CreateRequestCommand";

				private IDelegateCommand _CreateRequestCommand;
				///<summary>
				/// Propriété : CreateRequestCommand
				///</summary>
				public IDelegateCommand CreateRequestCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _CreateRequestCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _CreateRequestCommand, (v) => _CreateRequestCommand = v, value, CreateRequestCommand_PROPERTYNAME,  DoCreateRequestCommandBeforeSet, DoCreateRequestCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCreateRequestCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCreateRequestCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCreateRequestCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DeleteRequestCommand ===

				public const string DeleteRequestCommand_PROPERTYNAME = "DeleteRequestCommand";

				private IDelegateCommand _DeleteRequestCommand;
				///<summary>
				/// Propriété : DeleteRequestCommand
				///</summary>
				public IDelegateCommand DeleteRequestCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _DeleteRequestCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _DeleteRequestCommand, (v) => _DeleteRequestCommand = v, value, DeleteRequestCommand_PROPERTYNAME,  DoDeleteRequestCommandBeforeSet, DoDeleteRequestCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDeleteRequestCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDeleteRequestCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDeleteRequestCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux collections ===

			// Notez que les propriétés de collection sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Collection : UsersCollections ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection UsersCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionUsersCollections_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionUsersCollections_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionUsersCollections_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection UsersCollections (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionUsersCollections_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionUsersCollections_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionUsersCollections_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection UsersCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionUsersCollections_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionUsersCollections_ItemPropertyChanged;
							OnUsersCollectionsItemAdded((MyADA.Common.Services.Models.UserInfos)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection UsersCollections (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionUsersCollections_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnUsersCollectionsItemRemoved((MyADA.Common.Services.Models.UserInfos)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionUsersCollections_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionUsersCollections_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionUsersCollections_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionUsersCollections_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(UsersCollections_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyUsersCollectionsDependencies();
				}

				private void WatchCollectionUsersCollections_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(UsersCollections_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyUsersCollectionsDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection UsersCollections.
				///</summary>
				protected virtual void OnUsersCollectionsItemAdded(MyADA.Common.Services.Models.UserInfos p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection UsersCollections.
				///</summary>
				protected virtual void OnUsersCollectionsItemRemoved(MyADA.Common.Services.Models.UserInfos p_Item)
				{
				}

			#endregion

			#region === Collection : PositionCollections ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection PositionCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionPositionCollections_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionPositionCollections_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionPositionCollections_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection PositionCollections (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionPositionCollections_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionPositionCollections_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionPositionCollections_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection PositionCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionPositionCollections_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionPositionCollections_ItemPropertyChanged;
							OnPositionCollectionsItemAdded((MyADA.Common.Services.Models.PositionModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection PositionCollections (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionPositionCollections_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnPositionCollectionsItemRemoved((MyADA.Common.Services.Models.PositionModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionPositionCollections_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionPositionCollections_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionPositionCollections_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionPositionCollections_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(PositionCollections_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyPositionCollectionsDependencies();
				}

				private void WatchCollectionPositionCollections_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(PositionCollections_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyPositionCollectionsDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection PositionCollections.
				///</summary>
				protected virtual void OnPositionCollectionsItemAdded(MyADA.Common.Services.Models.PositionModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection PositionCollections.
				///</summary>
				protected virtual void OnPositionCollectionsItemRemoved(MyADA.Common.Services.Models.PositionModel p_Item)
				{
				}

			#endregion

			#region === Collection : ShowRoomCollections ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection ShowRoomCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionShowRoomCollections_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionShowRoomCollections_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionShowRoomCollections_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection ShowRoomCollections (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionShowRoomCollections_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionShowRoomCollections_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionShowRoomCollections_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection ShowRoomCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionShowRoomCollections_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionShowRoomCollections_ItemPropertyChanged;
							OnShowRoomCollectionsItemAdded((MyADA.Common.Services.Models.ShowroomModels)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection ShowRoomCollections (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionShowRoomCollections_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnShowRoomCollectionsItemRemoved((MyADA.Common.Services.Models.ShowroomModels)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionShowRoomCollections_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionShowRoomCollections_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionShowRoomCollections_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionShowRoomCollections_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ShowRoomCollections_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyShowRoomCollectionsDependencies();
				}

				private void WatchCollectionShowRoomCollections_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ShowRoomCollections_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyShowRoomCollectionsDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection ShowRoomCollections.
				///</summary>
				protected virtual void OnShowRoomCollectionsItemAdded(MyADA.Common.Services.Models.ShowroomModels p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection ShowRoomCollections.
				///</summary>
				protected virtual void OnShowRoomCollectionsItemRemoved(MyADA.Common.Services.Models.ShowroomModels p_Item)
				{
				}

			#endregion

			#region === Collection : UserTitleCollections ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection UserTitleCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionUserTitleCollections_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionUserTitleCollections_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionUserTitleCollections_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection UserTitleCollections (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionUserTitleCollections_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionUserTitleCollections_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionUserTitleCollections_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection UserTitleCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionUserTitleCollections_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionUserTitleCollections_ItemPropertyChanged;
							OnUserTitleCollectionsItemAdded((MyADA.Common.Services.Models.UserTitle)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection UserTitleCollections (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionUserTitleCollections_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnUserTitleCollectionsItemRemoved((MyADA.Common.Services.Models.UserTitle)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionUserTitleCollections_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionUserTitleCollections_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionUserTitleCollections_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionUserTitleCollections_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(UserTitleCollections_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyUserTitleCollectionsDependencies();
				}

				private void WatchCollectionUserTitleCollections_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(UserTitleCollections_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyUserTitleCollectionsDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection UserTitleCollections.
				///</summary>
				protected virtual void OnUserTitleCollectionsItemAdded(MyADA.Common.Services.Models.UserTitle p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection UserTitleCollections.
				///</summary>
				protected virtual void OnUserTitleCollectionsItemRemoved(MyADA.Common.Services.Models.UserTitle p_Item)
				{
				}

			#endregion

			#region === Collection : DomainOfActionsCollections ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection DomainOfActionsCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionDomainOfActionsCollections_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionDomainOfActionsCollections_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionDomainOfActionsCollections_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection DomainOfActionsCollections (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionDomainOfActionsCollections_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionDomainOfActionsCollections_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionDomainOfActionsCollections_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection DomainOfActionsCollections aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionDomainOfActionsCollections_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionDomainOfActionsCollections_ItemPropertyChanged;
							OnDomainOfActionsCollectionsItemAdded((DomainOfActionViewModelItem)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection DomainOfActionsCollections (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionDomainOfActionsCollections_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnDomainOfActionsCollectionsItemRemoved((DomainOfActionViewModelItem)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionDomainOfActionsCollections_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionDomainOfActionsCollections_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionDomainOfActionsCollections_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionDomainOfActionsCollections_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(DomainOfActionsCollections_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyDomainOfActionsCollectionsDependencies();
				}

				private void WatchCollectionDomainOfActionsCollections_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(DomainOfActionsCollections_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyDomainOfActionsCollectionsDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection DomainOfActionsCollections.
				///</summary>
				protected virtual void OnDomainOfActionsCollectionsItemAdded(DomainOfActionViewModelItem p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection DomainOfActionsCollections.
				///</summary>
				protected virtual void OnDomainOfActionsCollectionsItemRemoved(DomainOfActionViewModelItem p_Item)
				{
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux commandes ===

			// Notez que les propriétés de commandes sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Commande : ShowMyInfosCommand ===


				private bool ShowMyInfosCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowMyInfosCommand_CanExecute(l_Parameter);
				}

				private void ShowMyInfosCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowMyInfosCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowMyInfosCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowMyInfosCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowChangeRequestViewCommand ===


				private bool ShowChangeRequestViewCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowChangeRequestViewCommand_CanExecute(l_Parameter);
				}

				private void ShowChangeRequestViewCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowChangeRequestViewCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowChangeRequestViewCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowChangeRequestViewCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : SelectUserCommand ===


				private bool SelectUserCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnSelectUserCommand_CanExecute(l_Parameter);
				}

				private void SelectUserCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnSelectUserCommand_Execute(l_Parameter);
				}

				protected virtual bool OnSelectUserCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnSelectUserCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : SelectTitleCommand ===


				private bool SelectTitleCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnSelectTitleCommand_CanExecute(l_Parameter);
				}

				private void SelectTitleCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnSelectTitleCommand_Execute(l_Parameter);
				}

				protected virtual bool OnSelectTitleCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnSelectTitleCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : SelectUserPositionCommand ===


				private bool SelectUserPositionCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnSelectUserPositionCommand_CanExecute(l_Parameter);
				}

				private void SelectUserPositionCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnSelectUserPositionCommand_Execute(l_Parameter);
				}

				protected virtual bool OnSelectUserPositionCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnSelectUserPositionCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : SelectUserOrganizationCommand ===


				private bool SelectUserOrganizationCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnSelectUserOrganizationCommand_CanExecute(l_Parameter);
				}

				private void SelectUserOrganizationCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnSelectUserOrganizationCommand_Execute(l_Parameter);
				}

				protected virtual bool OnSelectUserOrganizationCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnSelectUserOrganizationCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : SelectUserDomainOfActionsCommand ===


				private bool SelectUserDomainOfActionsCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnSelectUserDomainOfActionsCommand_CanExecute(l_Parameter);
				}

				private void SelectUserDomainOfActionsCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnSelectUserDomainOfActionsCommand_Execute(l_Parameter);
				}

				protected virtual bool OnSelectUserDomainOfActionsCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnSelectUserDomainOfActionsCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : SendRequestionCommand ===


				private bool SendRequestionCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnSendRequestionCommand_CanExecute(l_Parameter);
				}

				private void SendRequestionCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnSendRequestionCommand_Execute(l_Parameter);
				}

				protected virtual bool OnSendRequestionCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnSendRequestionCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ChangeCommand ===


				private bool ChangeCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnChangeCommand_CanExecute(l_Parameter);
				}

				private void ChangeCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnChangeCommand_Execute(l_Parameter);
				}

				protected virtual bool OnChangeCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnChangeCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : CreateCommand ===


				private bool CreateCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnCreateCommand_CanExecute(l_Parameter);
				}

				private void CreateCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnCreateCommand_Execute(l_Parameter);
				}

				protected virtual bool OnCreateCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnCreateCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : DeleteCommand ===


				private bool DeleteCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnDeleteCommand_CanExecute(l_Parameter);
				}

				private void DeleteCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnDeleteCommand_Execute(l_Parameter);
				}

				protected virtual bool OnDeleteCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnDeleteCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ChangeRequestCommand ===


				private bool ChangeRequestCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnChangeRequestCommand_CanExecute(l_Parameter);
				}

				private void ChangeRequestCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnChangeRequestCommand_Execute(l_Parameter);
				}

				protected virtual bool OnChangeRequestCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnChangeRequestCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : CreateRequestCommand ===


				private bool CreateRequestCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnCreateRequestCommand_CanExecute(l_Parameter);
				}

				private void CreateRequestCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnCreateRequestCommand_Execute(l_Parameter);
				}

				protected virtual bool OnCreateRequestCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnCreateRequestCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : DeleteRequestCommand ===


				private bool DeleteRequestCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnDeleteRequestCommand_CanExecute(l_Parameter);
				}

				private void DeleteRequestCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnDeleteRequestCommand_Execute(l_Parameter);
				}

				protected virtual bool OnDeleteRequestCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnDeleteRequestCommand_Execute(object p_Parameter);

			#endregion


		#endregion

	}

	public partial class SettingsViewModel : SettingsViewModelBase
	{
	}
}
