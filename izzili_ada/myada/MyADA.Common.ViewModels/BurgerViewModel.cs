﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML BurgerViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  CoreSDK.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;

namespace MyADA.Common.ViewModels
{
	public abstract partial class BurgerViewModelBase : CoreSDK.ViewModel
	{
		protected override void OnInitialize()
		{
			base.OnInitialize();

			// Initialisation de la collection RssFeedsCollection.
			RssFeedsCollection = new System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel>();
			// Initialisation de la collection UserRssFeedsCollection.
			UserRssFeedsCollection = new System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel>();
			// Initialisation de la collection RssFeedItemsCollection.
			RssFeedItemsCollection = new System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.Post>();
			// Initialisation de la commande ShowLeadCommand.
			ShowLeadCommand = new DelegateCommand(ShowLeadCommand_CanExecute, ShowLeadCommand_Execute);
			// Initialisation de la commande ShowAudiNewsCommand.
			ShowAudiNewsCommand = new DelegateCommand(ShowAudiNewsCommand_CanExecute, ShowAudiNewsCommand_Execute);
			// Initialisation de la commande ShowSalesToolCommand.
			ShowSalesToolCommand = new DelegateCommand(ShowSalesToolCommand_CanExecute, ShowSalesToolCommand_Execute);
			// Initialisation de la commande ShowSelfTrainingCommand.
			ShowSelfTrainingCommand = new DelegateCommand(ShowSelfTrainingCommand_CanExecute, ShowSelfTrainingCommand_Execute);
			// Initialisation de la commande ShowViewReportCommand.
			ShowViewReportCommand = new DelegateCommand(ShowViewReportCommand_CanExecute, ShowViewReportCommand_Execute);
			// Initialisation de la commande ShowHomeViewCommand.
			ShowHomeViewCommand = new DelegateCommand(ShowHomeViewCommand_CanExecute, ShowHomeViewCommand_Execute);
			// Initialisation de la commande ShowMyNewsCommand.
			ShowMyNewsCommand = new DelegateCommand(ShowMyNewsCommand_CanExecute, ShowMyNewsCommand_Execute);
			// Initialisation de la commande ShowMyRssCommand.
			ShowMyRssCommand = new DelegateCommand(ShowMyRssCommand_CanExecute, ShowMyRssCommand_Execute);
			// Initialisation de la commande ShowSalesToolsProductsCommand.
			ShowSalesToolsProductsCommand = new DelegateCommand(ShowSalesToolsProductsCommand_CanExecute, ShowSalesToolsProductsCommand_Execute);
			// Initialisation de la commande ShowSalesToolsServiceCommand.
			ShowSalesToolsServiceCommand = new DelegateCommand(ShowSalesToolsServiceCommand_CanExecute, ShowSalesToolsServiceCommand_Execute);
			// Initialisation de la commande ShowSalesToolsIncentivesCommand.
			ShowSalesToolsIncentivesCommand = new DelegateCommand(ShowSalesToolsIncentivesCommand_CanExecute, ShowSalesToolsIncentivesCommand_Execute);
			// Initialisation de la commande ShowSalesToolsPromotionCommand.
			ShowSalesToolsPromotionCommand = new DelegateCommand(ShowSalesToolsPromotionCommand_CanExecute, ShowSalesToolsPromotionCommand_Execute);
			// Initialisation de la commande ShowMyLeadsCommand.
			ShowMyLeadsCommand = new DelegateCommand(ShowMyLeadsCommand_CanExecute, ShowMyLeadsCommand_Execute);
			// Initialisation de la commande ShowSettingsCommand.
			ShowSettingsCommand = new DelegateCommand(ShowSettingsCommand_CanExecute, ShowSettingsCommand_Execute);
			// Initialisation de la commande ShowHomeViewFromHeaderCommand.
			ShowHomeViewFromHeaderCommand = new DelegateCommand(ShowHomeViewFromHeaderCommand_CanExecute, ShowHomeViewFromHeaderCommand_Execute);
			// Initialisation de la commande ShowManageRssCommand.
			ShowManageRssCommand = new DelegateCommand(ShowManageRssCommand_CanExecute, ShowManageRssCommand_Execute);
			// Initialisation de la commande ShowFeedDetailCommand.
			ShowFeedDetailCommand = new DelegateCommand(ShowFeedDetailCommand_CanExecute, ShowFeedDetailCommand_Execute);
			// Initialisation de la commande ShowFeedItemDetailCommand.
			ShowFeedItemDetailCommand = new DelegateCommand(ShowFeedItemDetailCommand_CanExecute, ShowFeedItemDetailCommand_Execute);
			// Initialisation de la commande AddRssFeedCommand.
			AddRssFeedCommand = new DelegateCommand(AddRssFeedCommand_CanExecute, AddRssFeedCommand_Execute);
			// Initialisation de la commande AddOrRemoveRssFeedCommand.
			AddOrRemoveRssFeedCommand = new DelegateCommand(AddOrRemoveRssFeedCommand_CanExecute, AddOrRemoveRssFeedCommand_Execute);
			// Initialisation de la commande SaveRssCommand.
			SaveRssCommand = new DelegateCommand(SaveRssCommand_CanExecute, SaveRssCommand_Execute);
			// Initialisation de la commande BackCommand.
			BackCommand = new DelegateCommand(BackCommand_CanExecute, BackCommand_Execute);
			// Initialisation de la commande ShowAboutViewCommand.
			ShowAboutViewCommand = new DelegateCommand(ShowAboutViewCommand_CanExecute, ShowAboutViewCommand_Execute);
		}

		#region === Propriétés ===

			#region === Propriété : IsBusy ===

				public const string IsBusy_PROPERTYNAME = "IsBusy";

				private bool _IsBusy;
				///<summary>
				/// Propriété : IsBusy
				///</summary>
				public bool IsBusy
				{
					get
					{
						return GetValue<bool>(() => _IsBusy);
					}
					set
					{
						SetValue<bool>(() => _IsBusy, (v) => _IsBusy = v, value, IsBusy_PROPERTYNAME,  DoIsBusyBeforeSet, DoIsBusyAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsBusyBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsBusyAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsBusyDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsBurgerPresented ===

				public const string IsBurgerPresented_PROPERTYNAME = "IsBurgerPresented";

				private bool _IsBurgerPresented;
				///<summary>
				/// Propriété : IsBurgerPresented
				///</summary>
				public bool IsBurgerPresented
				{
					get
					{
						return GetValue<bool>(() => _IsBurgerPresented);
					}
					set
					{
						SetValue<bool>(() => _IsBurgerPresented, (v) => _IsBurgerPresented = v, value, IsBurgerPresented_PROPERTYNAME,  DoIsBurgerPresentedBeforeSet, DoIsBurgerPresentedAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsBurgerPresentedBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsBurgerPresentedAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsBurgerPresentedDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LastPage ===

				public const string LastPage_PROPERTYNAME = "LastPage";

				private string _LastPage;
				///<summary>
				/// Propriété : LastPage
				///</summary>
				public string LastPage
				{
					get
					{
						return GetValue<string>(() => _LastPage);
					}
					set
					{
						SetValue<string>(() => _LastPage, (v) => _LastPage = v, value, LastPage_PROPERTYNAME,  DoLastPageBeforeSet, DoLastPageAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLastPageBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLastPageAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLastPageDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectedFeedUrl ===

				public const string SelectedFeedUrl_PROPERTYNAME = "SelectedFeedUrl";

				private string _SelectedFeedUrl;
				///<summary>
				/// Propriété : SelectedFeedUrl
				///</summary>
				public string SelectedFeedUrl
				{
					get
					{
						return GetValue<string>(() => _SelectedFeedUrl);
					}
					set
					{
						SetValue<string>(() => _SelectedFeedUrl, (v) => _SelectedFeedUrl = v, value, SelectedFeedUrl_PROPERTYNAME,  DoSelectedFeedUrlBeforeSet, DoSelectedFeedUrlAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectedFeedUrlBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectedFeedUrlAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectedFeedUrlDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectedItemFeedUrl ===

				public const string SelectedItemFeedUrl_PROPERTYNAME = "SelectedItemFeedUrl";

				private string _SelectedItemFeedUrl;
				///<summary>
				/// Propriété : SelectedItemFeedUrl
				///</summary>
				public string SelectedItemFeedUrl
				{
					get
					{
						return GetValue<string>(() => _SelectedItemFeedUrl);
					}
					set
					{
						SetValue<string>(() => _SelectedItemFeedUrl, (v) => _SelectedItemFeedUrl = v, value, SelectedItemFeedUrl_PROPERTYNAME,  DoSelectedItemFeedUrlBeforeSet, DoSelectedItemFeedUrlAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectedItemFeedUrlBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectedItemFeedUrlAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectedItemFeedUrlDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : AboutViewUrl ===

				public const string AboutViewUrl_PROPERTYNAME = "AboutViewUrl";

				private string _AboutViewUrl;
				///<summary>
				/// Propriété : AboutViewUrl
				///</summary>
				public string AboutViewUrl
				{
					get
					{
						return GetValue<string>(() => _AboutViewUrl);
					}
					set
					{
						SetValue<string>(() => _AboutViewUrl, (v) => _AboutViewUrl = v, value, AboutViewUrl_PROPERTYNAME,  DoAboutViewUrlBeforeSet, DoAboutViewUrlAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoAboutViewUrlBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoAboutViewUrlAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyAboutViewUrlDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectedItemFeed ===

				public const string SelectedItemFeed_PROPERTYNAME = "SelectedItemFeed";

				private MyADA.Common.Services.Models.Post _SelectedItemFeed;
				///<summary>
				/// Propriété : SelectedItemFeed
				///</summary>
				public MyADA.Common.Services.Models.Post SelectedItemFeed
				{
					get
					{
						return GetValue<MyADA.Common.Services.Models.Post>(() => _SelectedItemFeed);
					}
					set
					{
						SetValue<MyADA.Common.Services.Models.Post>(() => _SelectedItemFeed, (v) => _SelectedItemFeed = v, value, SelectedItemFeed_PROPERTYNAME,  DoSelectedItemFeedBeforeSet, DoSelectedItemFeedAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectedItemFeedBeforeSet(string p_PropertyName, MyADA.Common.Services.Models.Post p_OldValue, MyADA.Common.Services.Models.Post p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectedItemFeedAfterSet(string p_PropertyName, MyADA.Common.Services.Models.Post p_OldValue, MyADA.Common.Services.Models.Post p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectedItemFeedDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectedFeedName ===

				public const string SelectedFeedName_PROPERTYNAME = "SelectedFeedName";

				private string _SelectedFeedName;
				///<summary>
				/// Propriété : SelectedFeedName
				///</summary>
				public string SelectedFeedName
				{
					get
					{
						return GetValue<string>(() => _SelectedFeedName);
					}
					set
					{
						SetValue<string>(() => _SelectedFeedName, (v) => _SelectedFeedName = v, value, SelectedFeedName_PROPERTYNAME,  DoSelectedFeedNameBeforeSet, DoSelectedFeedNameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectedFeedNameBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectedFeedNameAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectedFeedNameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : RssFeedsCollection ===

				public const string RssFeedsCollection_PROPERTYNAME = "RssFeedsCollection";

				private System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel> _RssFeedsCollection;
				///<summary>
				/// Propriété : RssFeedsCollection
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel> RssFeedsCollection
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel>>(() => _RssFeedsCollection);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel>>(() => _RssFeedsCollection, (v) => _RssFeedsCollection = v, value, RssFeedsCollection_PROPERTYNAME,  DoRssFeedsCollectionBeforeSet, DoRssFeedsCollectionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoRssFeedsCollectionBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionRssFeedsCollection_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoRssFeedsCollectionAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionRssFeedsCollection_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyRssFeedsCollectionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UserRssFeedsCollection ===

				public const string UserRssFeedsCollection_PROPERTYNAME = "UserRssFeedsCollection";

				private System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel> _UserRssFeedsCollection;
				///<summary>
				/// Propriété : UserRssFeedsCollection
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel> UserRssFeedsCollection
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel>>(() => _UserRssFeedsCollection);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel>>(() => _UserRssFeedsCollection, (v) => _UserRssFeedsCollection = v, value, UserRssFeedsCollection_PROPERTYNAME,  DoUserRssFeedsCollectionBeforeSet, DoUserRssFeedsCollectionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUserRssFeedsCollectionBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionUserRssFeedsCollection_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUserRssFeedsCollectionAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<RssFeedItemViewModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionUserRssFeedsCollection_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUserRssFeedsCollectionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : RssFeedItemsCollection ===

				public const string RssFeedItemsCollection_PROPERTYNAME = "RssFeedItemsCollection";

				private System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.Post> _RssFeedItemsCollection;
				///<summary>
				/// Propriété : RssFeedItemsCollection
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.Post> RssFeedItemsCollection
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.Post>>(() => _RssFeedItemsCollection);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.Post>>(() => _RssFeedItemsCollection, (v) => _RssFeedItemsCollection = v, value, RssFeedItemsCollection_PROPERTYNAME,  DoRssFeedItemsCollectionBeforeSet, DoRssFeedItemsCollectionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoRssFeedItemsCollectionBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.Post> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.Post> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionRssFeedItemsCollection_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoRssFeedItemsCollectionAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.Post> p_OldValue, System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.Post> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionRssFeedItemsCollection_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyRssFeedItemsCollectionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowLeadCommand ===

				public const string ShowLeadCommand_PROPERTYNAME = "ShowLeadCommand";

				private IDelegateCommand _ShowLeadCommand;
				///<summary>
				/// Propriété : ShowLeadCommand
				///</summary>
				public IDelegateCommand ShowLeadCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowLeadCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowLeadCommand, (v) => _ShowLeadCommand = v, value, ShowLeadCommand_PROPERTYNAME,  DoShowLeadCommandBeforeSet, DoShowLeadCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowLeadCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowLeadCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowLeadCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowAudiNewsCommand ===

				public const string ShowAudiNewsCommand_PROPERTYNAME = "ShowAudiNewsCommand";

				private IDelegateCommand _ShowAudiNewsCommand;
				///<summary>
				/// Propriété : ShowAudiNewsCommand
				///</summary>
				public IDelegateCommand ShowAudiNewsCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowAudiNewsCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowAudiNewsCommand, (v) => _ShowAudiNewsCommand = v, value, ShowAudiNewsCommand_PROPERTYNAME,  DoShowAudiNewsCommandBeforeSet, DoShowAudiNewsCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowAudiNewsCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowAudiNewsCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowAudiNewsCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowSalesToolCommand ===

				public const string ShowSalesToolCommand_PROPERTYNAME = "ShowSalesToolCommand";

				private IDelegateCommand _ShowSalesToolCommand;
				///<summary>
				/// Propriété : ShowSalesToolCommand
				///</summary>
				public IDelegateCommand ShowSalesToolCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowSalesToolCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowSalesToolCommand, (v) => _ShowSalesToolCommand = v, value, ShowSalesToolCommand_PROPERTYNAME,  DoShowSalesToolCommandBeforeSet, DoShowSalesToolCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowSalesToolCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowSalesToolCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowSalesToolCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowSelfTrainingCommand ===

				public const string ShowSelfTrainingCommand_PROPERTYNAME = "ShowSelfTrainingCommand";

				private IDelegateCommand _ShowSelfTrainingCommand;
				///<summary>
				/// Propriété : ShowSelfTrainingCommand
				///</summary>
				public IDelegateCommand ShowSelfTrainingCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowSelfTrainingCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowSelfTrainingCommand, (v) => _ShowSelfTrainingCommand = v, value, ShowSelfTrainingCommand_PROPERTYNAME,  DoShowSelfTrainingCommandBeforeSet, DoShowSelfTrainingCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowSelfTrainingCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowSelfTrainingCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowSelfTrainingCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowViewReportCommand ===

				public const string ShowViewReportCommand_PROPERTYNAME = "ShowViewReportCommand";

				private IDelegateCommand _ShowViewReportCommand;
				///<summary>
				/// Propriété : ShowViewReportCommand
				///</summary>
				public IDelegateCommand ShowViewReportCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowViewReportCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowViewReportCommand, (v) => _ShowViewReportCommand = v, value, ShowViewReportCommand_PROPERTYNAME,  DoShowViewReportCommandBeforeSet, DoShowViewReportCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowViewReportCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowViewReportCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowViewReportCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowHomeViewCommand ===

				public const string ShowHomeViewCommand_PROPERTYNAME = "ShowHomeViewCommand";

				private IDelegateCommand _ShowHomeViewCommand;
				///<summary>
				/// Propriété : ShowHomeViewCommand
				///</summary>
				public IDelegateCommand ShowHomeViewCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowHomeViewCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowHomeViewCommand, (v) => _ShowHomeViewCommand = v, value, ShowHomeViewCommand_PROPERTYNAME,  DoShowHomeViewCommandBeforeSet, DoShowHomeViewCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowHomeViewCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowHomeViewCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowHomeViewCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowMyNewsCommand ===

				public const string ShowMyNewsCommand_PROPERTYNAME = "ShowMyNewsCommand";

				private IDelegateCommand _ShowMyNewsCommand;
				///<summary>
				/// Propriété : ShowMyNewsCommand
				///</summary>
				public IDelegateCommand ShowMyNewsCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowMyNewsCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowMyNewsCommand, (v) => _ShowMyNewsCommand = v, value, ShowMyNewsCommand_PROPERTYNAME,  DoShowMyNewsCommandBeforeSet, DoShowMyNewsCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowMyNewsCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowMyNewsCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowMyNewsCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowMyRssCommand ===

				public const string ShowMyRssCommand_PROPERTYNAME = "ShowMyRssCommand";

				private IDelegateCommand _ShowMyRssCommand;
				///<summary>
				/// Propriété : ShowMyRssCommand
				///</summary>
				public IDelegateCommand ShowMyRssCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowMyRssCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowMyRssCommand, (v) => _ShowMyRssCommand = v, value, ShowMyRssCommand_PROPERTYNAME,  DoShowMyRssCommandBeforeSet, DoShowMyRssCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowMyRssCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowMyRssCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowMyRssCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowSalesToolsProductsCommand ===

				public const string ShowSalesToolsProductsCommand_PROPERTYNAME = "ShowSalesToolsProductsCommand";

				private IDelegateCommand _ShowSalesToolsProductsCommand;
				///<summary>
				/// Propriété : ShowSalesToolsProductsCommand
				///</summary>
				public IDelegateCommand ShowSalesToolsProductsCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowSalesToolsProductsCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowSalesToolsProductsCommand, (v) => _ShowSalesToolsProductsCommand = v, value, ShowSalesToolsProductsCommand_PROPERTYNAME,  DoShowSalesToolsProductsCommandBeforeSet, DoShowSalesToolsProductsCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowSalesToolsProductsCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowSalesToolsProductsCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowSalesToolsProductsCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowSalesToolsServiceCommand ===

				public const string ShowSalesToolsServiceCommand_PROPERTYNAME = "ShowSalesToolsServiceCommand";

				private IDelegateCommand _ShowSalesToolsServiceCommand;
				///<summary>
				/// Propriété : ShowSalesToolsServiceCommand
				///</summary>
				public IDelegateCommand ShowSalesToolsServiceCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowSalesToolsServiceCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowSalesToolsServiceCommand, (v) => _ShowSalesToolsServiceCommand = v, value, ShowSalesToolsServiceCommand_PROPERTYNAME,  DoShowSalesToolsServiceCommandBeforeSet, DoShowSalesToolsServiceCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowSalesToolsServiceCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowSalesToolsServiceCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowSalesToolsServiceCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowSalesToolsIncentivesCommand ===

				public const string ShowSalesToolsIncentivesCommand_PROPERTYNAME = "ShowSalesToolsIncentivesCommand";

				private IDelegateCommand _ShowSalesToolsIncentivesCommand;
				///<summary>
				/// Propriété : ShowSalesToolsIncentivesCommand
				///</summary>
				public IDelegateCommand ShowSalesToolsIncentivesCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowSalesToolsIncentivesCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowSalesToolsIncentivesCommand, (v) => _ShowSalesToolsIncentivesCommand = v, value, ShowSalesToolsIncentivesCommand_PROPERTYNAME,  DoShowSalesToolsIncentivesCommandBeforeSet, DoShowSalesToolsIncentivesCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowSalesToolsIncentivesCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowSalesToolsIncentivesCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowSalesToolsIncentivesCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowSalesToolsPromotionCommand ===

				public const string ShowSalesToolsPromotionCommand_PROPERTYNAME = "ShowSalesToolsPromotionCommand";

				private IDelegateCommand _ShowSalesToolsPromotionCommand;
				///<summary>
				/// Propriété : ShowSalesToolsPromotionCommand
				///</summary>
				public IDelegateCommand ShowSalesToolsPromotionCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowSalesToolsPromotionCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowSalesToolsPromotionCommand, (v) => _ShowSalesToolsPromotionCommand = v, value, ShowSalesToolsPromotionCommand_PROPERTYNAME,  DoShowSalesToolsPromotionCommandBeforeSet, DoShowSalesToolsPromotionCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowSalesToolsPromotionCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowSalesToolsPromotionCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowSalesToolsPromotionCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowMyLeadsCommand ===

				public const string ShowMyLeadsCommand_PROPERTYNAME = "ShowMyLeadsCommand";

				private IDelegateCommand _ShowMyLeadsCommand;
				///<summary>
				/// Propriété : ShowMyLeadsCommand
				///</summary>
				public IDelegateCommand ShowMyLeadsCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowMyLeadsCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowMyLeadsCommand, (v) => _ShowMyLeadsCommand = v, value, ShowMyLeadsCommand_PROPERTYNAME,  DoShowMyLeadsCommandBeforeSet, DoShowMyLeadsCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowMyLeadsCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowMyLeadsCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowMyLeadsCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowSettingsCommand ===

				public const string ShowSettingsCommand_PROPERTYNAME = "ShowSettingsCommand";

				private IDelegateCommand _ShowSettingsCommand;
				///<summary>
				/// Propriété : ShowSettingsCommand
				///</summary>
				public IDelegateCommand ShowSettingsCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowSettingsCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowSettingsCommand, (v) => _ShowSettingsCommand = v, value, ShowSettingsCommand_PROPERTYNAME,  DoShowSettingsCommandBeforeSet, DoShowSettingsCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowSettingsCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowSettingsCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowSettingsCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowHomeViewFromHeaderCommand ===

				public const string ShowHomeViewFromHeaderCommand_PROPERTYNAME = "ShowHomeViewFromHeaderCommand";

				private IDelegateCommand _ShowHomeViewFromHeaderCommand;
				///<summary>
				/// Propriété : ShowHomeViewFromHeaderCommand
				///</summary>
				public IDelegateCommand ShowHomeViewFromHeaderCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowHomeViewFromHeaderCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowHomeViewFromHeaderCommand, (v) => _ShowHomeViewFromHeaderCommand = v, value, ShowHomeViewFromHeaderCommand_PROPERTYNAME,  DoShowHomeViewFromHeaderCommandBeforeSet, DoShowHomeViewFromHeaderCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowHomeViewFromHeaderCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowHomeViewFromHeaderCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowHomeViewFromHeaderCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowManageRssCommand ===

				public const string ShowManageRssCommand_PROPERTYNAME = "ShowManageRssCommand";

				private IDelegateCommand _ShowManageRssCommand;
				///<summary>
				/// Propriété : ShowManageRssCommand
				///</summary>
				public IDelegateCommand ShowManageRssCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowManageRssCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowManageRssCommand, (v) => _ShowManageRssCommand = v, value, ShowManageRssCommand_PROPERTYNAME,  DoShowManageRssCommandBeforeSet, DoShowManageRssCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowManageRssCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowManageRssCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowManageRssCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowFeedDetailCommand ===

				public const string ShowFeedDetailCommand_PROPERTYNAME = "ShowFeedDetailCommand";

				private IDelegateCommand _ShowFeedDetailCommand;
				///<summary>
				/// Propriété : ShowFeedDetailCommand
				///</summary>
				public IDelegateCommand ShowFeedDetailCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowFeedDetailCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowFeedDetailCommand, (v) => _ShowFeedDetailCommand = v, value, ShowFeedDetailCommand_PROPERTYNAME,  DoShowFeedDetailCommandBeforeSet, DoShowFeedDetailCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowFeedDetailCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowFeedDetailCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowFeedDetailCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowFeedItemDetailCommand ===

				public const string ShowFeedItemDetailCommand_PROPERTYNAME = "ShowFeedItemDetailCommand";

				private IDelegateCommand _ShowFeedItemDetailCommand;
				///<summary>
				/// Propriété : ShowFeedItemDetailCommand
				///</summary>
				public IDelegateCommand ShowFeedItemDetailCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowFeedItemDetailCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowFeedItemDetailCommand, (v) => _ShowFeedItemDetailCommand = v, value, ShowFeedItemDetailCommand_PROPERTYNAME,  DoShowFeedItemDetailCommandBeforeSet, DoShowFeedItemDetailCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowFeedItemDetailCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowFeedItemDetailCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowFeedItemDetailCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : AddRssFeedCommand ===

				public const string AddRssFeedCommand_PROPERTYNAME = "AddRssFeedCommand";

				private IDelegateCommand _AddRssFeedCommand;
				///<summary>
				/// Propriété : AddRssFeedCommand
				///</summary>
				public IDelegateCommand AddRssFeedCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _AddRssFeedCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _AddRssFeedCommand, (v) => _AddRssFeedCommand = v, value, AddRssFeedCommand_PROPERTYNAME,  DoAddRssFeedCommandBeforeSet, DoAddRssFeedCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoAddRssFeedCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoAddRssFeedCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyAddRssFeedCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : AddOrRemoveRssFeedCommand ===

				public const string AddOrRemoveRssFeedCommand_PROPERTYNAME = "AddOrRemoveRssFeedCommand";

				private IDelegateCommand _AddOrRemoveRssFeedCommand;
				///<summary>
				/// Propriété : AddOrRemoveRssFeedCommand
				///</summary>
				public IDelegateCommand AddOrRemoveRssFeedCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _AddOrRemoveRssFeedCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _AddOrRemoveRssFeedCommand, (v) => _AddOrRemoveRssFeedCommand = v, value, AddOrRemoveRssFeedCommand_PROPERTYNAME,  DoAddOrRemoveRssFeedCommandBeforeSet, DoAddOrRemoveRssFeedCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoAddOrRemoveRssFeedCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoAddOrRemoveRssFeedCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyAddOrRemoveRssFeedCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SaveRssCommand ===

				public const string SaveRssCommand_PROPERTYNAME = "SaveRssCommand";

				private IDelegateCommand _SaveRssCommand;
				///<summary>
				/// Propriété : SaveRssCommand
				///</summary>
				public IDelegateCommand SaveRssCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _SaveRssCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _SaveRssCommand, (v) => _SaveRssCommand = v, value, SaveRssCommand_PROPERTYNAME,  DoSaveRssCommandBeforeSet, DoSaveRssCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSaveRssCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSaveRssCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySaveRssCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : BackCommand ===

				public const string BackCommand_PROPERTYNAME = "BackCommand";

				private IDelegateCommand _BackCommand;
				///<summary>
				/// Propriété : BackCommand
				///</summary>
				public IDelegateCommand BackCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _BackCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _BackCommand, (v) => _BackCommand = v, value, BackCommand_PROPERTYNAME,  DoBackCommandBeforeSet, DoBackCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoBackCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoBackCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyBackCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowAboutViewCommand ===

				public const string ShowAboutViewCommand_PROPERTYNAME = "ShowAboutViewCommand";

				private IDelegateCommand _ShowAboutViewCommand;
				///<summary>
				/// Propriété : ShowAboutViewCommand
				///</summary>
				public IDelegateCommand ShowAboutViewCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ShowAboutViewCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ShowAboutViewCommand, (v) => _ShowAboutViewCommand = v, value, ShowAboutViewCommand_PROPERTYNAME,  DoShowAboutViewCommandBeforeSet, DoShowAboutViewCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowAboutViewCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowAboutViewCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowAboutViewCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux collections ===

			// Notez que les propriétés de collection sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Collection : RssFeedsCollection ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection RssFeedsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionRssFeedsCollection_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionRssFeedsCollection_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionRssFeedsCollection_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection RssFeedsCollection (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionRssFeedsCollection_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionRssFeedsCollection_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionRssFeedsCollection_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection RssFeedsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionRssFeedsCollection_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionRssFeedsCollection_ItemPropertyChanged;
							OnRssFeedsCollectionItemAdded((RssFeedItemViewModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection RssFeedsCollection (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionRssFeedsCollection_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnRssFeedsCollectionItemRemoved((RssFeedItemViewModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionRssFeedsCollection_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionRssFeedsCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionRssFeedsCollection_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionRssFeedsCollection_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(RssFeedsCollection_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyRssFeedsCollectionDependencies();
				}

				private void WatchCollectionRssFeedsCollection_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(RssFeedsCollection_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyRssFeedsCollectionDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection RssFeedsCollection.
				///</summary>
				protected virtual void OnRssFeedsCollectionItemAdded(RssFeedItemViewModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection RssFeedsCollection.
				///</summary>
				protected virtual void OnRssFeedsCollectionItemRemoved(RssFeedItemViewModel p_Item)
				{
				}

			#endregion

			#region === Collection : UserRssFeedsCollection ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection UserRssFeedsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionUserRssFeedsCollection_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionUserRssFeedsCollection_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionUserRssFeedsCollection_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection UserRssFeedsCollection (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionUserRssFeedsCollection_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionUserRssFeedsCollection_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionUserRssFeedsCollection_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection UserRssFeedsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionUserRssFeedsCollection_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionUserRssFeedsCollection_ItemPropertyChanged;
							OnUserRssFeedsCollectionItemAdded((RssFeedItemViewModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection UserRssFeedsCollection (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionUserRssFeedsCollection_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnUserRssFeedsCollectionItemRemoved((RssFeedItemViewModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionUserRssFeedsCollection_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionUserRssFeedsCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionUserRssFeedsCollection_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionUserRssFeedsCollection_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(UserRssFeedsCollection_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyUserRssFeedsCollectionDependencies();
				}

				private void WatchCollectionUserRssFeedsCollection_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(UserRssFeedsCollection_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyUserRssFeedsCollectionDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection UserRssFeedsCollection.
				///</summary>
				protected virtual void OnUserRssFeedsCollectionItemAdded(RssFeedItemViewModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection UserRssFeedsCollection.
				///</summary>
				protected virtual void OnUserRssFeedsCollectionItemRemoved(RssFeedItemViewModel p_Item)
				{
				}

			#endregion

			#region === Collection : RssFeedItemsCollection ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection RssFeedItemsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionRssFeedItemsCollection_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionRssFeedItemsCollection_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionRssFeedItemsCollection_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection RssFeedItemsCollection (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionRssFeedItemsCollection_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionRssFeedItemsCollection_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionRssFeedItemsCollection_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection RssFeedItemsCollection aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionRssFeedItemsCollection_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionRssFeedItemsCollection_ItemPropertyChanged;
							OnRssFeedItemsCollectionItemAdded((MyADA.Common.Services.Models.Post)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection RssFeedItemsCollection (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionRssFeedItemsCollection_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnRssFeedItemsCollectionItemRemoved((MyADA.Common.Services.Models.Post)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionRssFeedItemsCollection_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionRssFeedItemsCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionRssFeedItemsCollection_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionRssFeedItemsCollection_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(RssFeedItemsCollection_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyRssFeedItemsCollectionDependencies();
				}

				private void WatchCollectionRssFeedItemsCollection_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(RssFeedItemsCollection_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyRssFeedItemsCollectionDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection RssFeedItemsCollection.
				///</summary>
				protected virtual void OnRssFeedItemsCollectionItemAdded(MyADA.Common.Services.Models.Post p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection RssFeedItemsCollection.
				///</summary>
				protected virtual void OnRssFeedItemsCollectionItemRemoved(MyADA.Common.Services.Models.Post p_Item)
				{
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux commandes ===

			// Notez que les propriétés de commandes sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Commande : ShowLeadCommand ===


				private bool ShowLeadCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowLeadCommand_CanExecute(l_Parameter);
				}

				private void ShowLeadCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowLeadCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowLeadCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowLeadCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowAudiNewsCommand ===


				private bool ShowAudiNewsCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowAudiNewsCommand_CanExecute(l_Parameter);
				}

				private void ShowAudiNewsCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowAudiNewsCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowAudiNewsCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowAudiNewsCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowSalesToolCommand ===


				private bool ShowSalesToolCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowSalesToolCommand_CanExecute(l_Parameter);
				}

				private void ShowSalesToolCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowSalesToolCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowSalesToolCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowSalesToolCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowSelfTrainingCommand ===


				private bool ShowSelfTrainingCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowSelfTrainingCommand_CanExecute(l_Parameter);
				}

				private void ShowSelfTrainingCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowSelfTrainingCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowSelfTrainingCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowSelfTrainingCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowViewReportCommand ===


				private bool ShowViewReportCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowViewReportCommand_CanExecute(l_Parameter);
				}

				private void ShowViewReportCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowViewReportCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowViewReportCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowViewReportCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowHomeViewCommand ===


				private bool ShowHomeViewCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowHomeViewCommand_CanExecute(l_Parameter);
				}

				private void ShowHomeViewCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowHomeViewCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowHomeViewCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowHomeViewCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowMyNewsCommand ===


				private bool ShowMyNewsCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowMyNewsCommand_CanExecute(l_Parameter);
				}

				private void ShowMyNewsCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowMyNewsCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowMyNewsCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowMyNewsCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowMyRssCommand ===


				private bool ShowMyRssCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowMyRssCommand_CanExecute(l_Parameter);
				}

				private void ShowMyRssCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowMyRssCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowMyRssCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowMyRssCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowSalesToolsProductsCommand ===


				private bool ShowSalesToolsProductsCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowSalesToolsProductsCommand_CanExecute(l_Parameter);
				}

				private void ShowSalesToolsProductsCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowSalesToolsProductsCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowSalesToolsProductsCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowSalesToolsProductsCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowSalesToolsServiceCommand ===


				private bool ShowSalesToolsServiceCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowSalesToolsServiceCommand_CanExecute(l_Parameter);
				}

				private void ShowSalesToolsServiceCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowSalesToolsServiceCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowSalesToolsServiceCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowSalesToolsServiceCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowSalesToolsIncentivesCommand ===


				private bool ShowSalesToolsIncentivesCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowSalesToolsIncentivesCommand_CanExecute(l_Parameter);
				}

				private void ShowSalesToolsIncentivesCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowSalesToolsIncentivesCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowSalesToolsIncentivesCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowSalesToolsIncentivesCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowSalesToolsPromotionCommand ===


				private bool ShowSalesToolsPromotionCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowSalesToolsPromotionCommand_CanExecute(l_Parameter);
				}

				private void ShowSalesToolsPromotionCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowSalesToolsPromotionCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowSalesToolsPromotionCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowSalesToolsPromotionCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowMyLeadsCommand ===


				private bool ShowMyLeadsCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowMyLeadsCommand_CanExecute(l_Parameter);
				}

				private void ShowMyLeadsCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowMyLeadsCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowMyLeadsCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowMyLeadsCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowSettingsCommand ===


				private bool ShowSettingsCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowSettingsCommand_CanExecute(l_Parameter);
				}

				private void ShowSettingsCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowSettingsCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowSettingsCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowSettingsCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowHomeViewFromHeaderCommand ===


				private bool ShowHomeViewFromHeaderCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowHomeViewFromHeaderCommand_CanExecute(l_Parameter);
				}

				private void ShowHomeViewFromHeaderCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowHomeViewFromHeaderCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowHomeViewFromHeaderCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowHomeViewFromHeaderCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowManageRssCommand ===


				private bool ShowManageRssCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowManageRssCommand_CanExecute(l_Parameter);
				}

				private void ShowManageRssCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowManageRssCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowManageRssCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowManageRssCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowFeedDetailCommand ===


				private bool ShowFeedDetailCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowFeedDetailCommand_CanExecute(l_Parameter);
				}

				private void ShowFeedDetailCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowFeedDetailCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowFeedDetailCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowFeedDetailCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowFeedItemDetailCommand ===


				private bool ShowFeedItemDetailCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowFeedItemDetailCommand_CanExecute(l_Parameter);
				}

				private void ShowFeedItemDetailCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowFeedItemDetailCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowFeedItemDetailCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowFeedItemDetailCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : AddRssFeedCommand ===


				private bool AddRssFeedCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnAddRssFeedCommand_CanExecute(l_Parameter);
				}

				private void AddRssFeedCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnAddRssFeedCommand_Execute(l_Parameter);
				}

				protected virtual bool OnAddRssFeedCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnAddRssFeedCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : AddOrRemoveRssFeedCommand ===


				private bool AddOrRemoveRssFeedCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnAddOrRemoveRssFeedCommand_CanExecute(l_Parameter);
				}

				private void AddOrRemoveRssFeedCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnAddOrRemoveRssFeedCommand_Execute(l_Parameter);
				}

				protected virtual bool OnAddOrRemoveRssFeedCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnAddOrRemoveRssFeedCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : SaveRssCommand ===


				private bool SaveRssCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnSaveRssCommand_CanExecute(l_Parameter);
				}

				private void SaveRssCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnSaveRssCommand_Execute(l_Parameter);
				}

				protected virtual bool OnSaveRssCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnSaveRssCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : BackCommand ===


				private bool BackCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnBackCommand_CanExecute(l_Parameter);
				}

				private void BackCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnBackCommand_Execute(l_Parameter);
				}

				protected virtual bool OnBackCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnBackCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ShowAboutViewCommand ===


				private bool ShowAboutViewCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnShowAboutViewCommand_CanExecute(l_Parameter);
				}

				private void ShowAboutViewCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnShowAboutViewCommand_Execute(l_Parameter);
				}

				protected virtual bool OnShowAboutViewCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnShowAboutViewCommand_Execute(object p_Parameter);

			#endregion


		#endregion

	}

	public partial class BurgerViewModel : BurgerViewModelBase
	{
	}
}
