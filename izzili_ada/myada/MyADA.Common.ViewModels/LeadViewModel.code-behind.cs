using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;
using MyADA.Common.DAOServices.Services;
using System.Threading.Tasks;
using MyADA.Common.Services.Models;
using MyADA.Common.Services.Services;
using System.Globalization;
using MyADA.Common.Services.Requests;
using Newtonsoft.Json;

namespace MyADA.Common.ViewModels
{
    public partial class LeadViewModel : LeadViewModelBase, MyADA.Common.ViewModels.ILeadViewModel
    {
        #region ==================================== Properties ====================================
        private string Locale = Properties.Resources.CultureInfoCurrentCultureName;
        private IGlobalConfigService GlobalConfigService;
        private const string _NewLeadStatus = "new";
        private const string _NotallocatedLeadStatus = "notallocated";
        private const string _AllocatedLeadStatus = "allocated";
        private const string _DoneLeadStatus = "closed";
        private const string _NotInterestLeadStatus = "nointerest";
        private const string _Ongoing = "ongoing";
        private const string _RecontactOn = "recontacton";
        private const string _FilterWhite = "file|FilterWhite.png";
        private const string _FilterRed = "file|FilterRed.png";
        private const string _FilterDarkGrey = "file|FilterDarkGrey.png";
        private const int _LimitValue = 15;

        private List<string> _NotConcernedStatus = new List<string>() { Helper.ToolsBox.ADALeadStatus.allocated.ToString(), _NewLeadStatus, _NotallocatedLeadStatus };
        private List<string> SelectedLeadsList = new List<string>();
        private List<string> _ListInterestLeadStatusForMyLead = new List<string>() { _NewLeadStatus, _AllocatedLeadStatus };
        private List<string> _ListInterestLeadStatusForMyOwnLead = new List<string>() { _AllocatedLeadStatus, _Ongoing };
        private List<string> LeadFilterModelsList = new List<string>();
        public enum FilterStep { BookLeadFilter, CommercialLeadFilter, MyLeadsFilter, MyOwnLeadFilter };
        private bool IsWorkingView = false;
        #endregion

        #region ==================================== Initialisation ====================================
        protected override void OnInitialize()
        {
            base.OnInitialize();
            ResolveInitialisation();
        }

        private void ResolveInitialisation()
        {
            GlobalConfigService = CoreSDK.Framework.Services.GlobalConfigService();
        }
        #endregion

        #region ==================================== Prepare ViewModel data ====================================
        /// <summary>
        /// Prepare ViewModel
        /// </summary>
        /// <returns></returns>
        public async Task<bool> PrepareVM(CurrentUserProfile p_CurrentUserProfile)
        {
            try
            {
                this.CurrentUserProfileInfos = p_CurrentUserProfile;
                MyOwnLeadsPopinIsVisible = false;
                PickerDateValue = DateTime.Now.ToString();

                FilterStyleDetails = _FilterWhite;
                FilterStyleRoot = _FilterDarkGrey;

                await Task.Run(async () =>
                {
                    await LoadData(true);
                }).ConfigureAwait(false);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                //EndWork();
            }
        }

        public async Task LoadData(bool IsPrepaData = true)
        {
            //BeginWork();

            this.PlaceHolderForLeadsPicker = Properties.Resources.AllocateTo;
            this.HeaderTextPicker = Properties.Resources.HeaderTextPicker;
            var l_Parameter = "?ShowroomId=" + CurrentUserProfileInfos.ShowroomId + "&Status=" + System.Net.WebUtility.UrlEncode(JsonConvert.SerializeObject(_ListInterestLeadStatusForMyLead.ToArray()));// +"Status=" + _NewLeadStatus;// +((IsPrepaData) ? "&Limit=15" : string.Empty);

            if (IsPrepaData)
            {
                LeadsCollection.Clear();
                UsersCollections.Clear();
                OnUseLeadsCollection.Clear();
                await LoadingData(l_Parameter);
            }
            else
                await RefreshingData(l_Parameter);
        }

        private async Task LoadingData(string p_Parameter)
        {
            #region ==================================== Leads collection ====================================

            var l_LeadService = CoreSDK.Framework.Services.Container().Resolve<ILeadService>();
            var l_LeadsList = await l_LeadService.GetLeads(p_Parameter);

            /*LeadsCollection.AddRange(l_LeadsList.OrderByDescending(a => a.CreatedDate).Where(b => b.Status == _NewLeadStatus).Select(x => new LeadsItemViewModel()
            {
                StatusColor = GetStatusColor(x.CreatedDate),
                IsSeleted = false,
                ShowAffectationName = (x.UserId != null) ? true : false,
                FormattedDate = String.Format("{0:g}", x.CreatedDate),
                IsLineVisble = true
            }.SetLeadsEncapsulation(x)));*/


            OnUseLeadsCollection.AddRange(l_LeadsList.OrderByDescending(a => a.CreatedDate).Where(b => b.Status == _NewLeadStatus).Select(x => new LeadsItemViewModel()
            {
                StatusColor = GetStatusColor(x.CreatedDate),
                IsSeleted = false,
                ShowAffectationName = (x.UserId != null) ? true : false,
                FormattedDate = String.Format("{0:g}", x.CreatedDate),
                IsLineVisble = true
            }.SetLeadsEncapsulation(x)));

            var l_Items = Load(DateTime.MaxValue);
            LeadsCollection.AddRange(l_Items);


            // Only if the showroom is managed and user role is simple user
            if (CurrentUserProfileInfos.Showroom.LeadConfig == Helper.ToolsBox.ADAShowroomLeadConfig.managed.ToString() && CurrentUserProfileInfos.Role == Helper.ToolsBox.ADARole.user.ToString())
                ManagedShowRoomLeadCollections.AddRange(
                    LeadsCollection.Where(y => y.UserId == CurrentUserProfileInfos.UserId).OrderBy(date => date.CreatedDate).GroupBy(x => x.FormattedDate).Select(group => new ManagedShowRoomLead
                    {
                        FormatedDate = group.Key,
                        LeadCount = group.Count()
                    }));

            LeadFilterModelsCollections.Clear();
            LeadFilterModelsCollections.AddRange(OnUseLeadsCollection.Where(x => !string.IsNullOrEmpty(x.Tag)).OrderBy(y => y.Tag).GroupBy(z => z.Tag).Select(group => new LeadFilterModels
            {
                Libelle = group.Key,
                IsSelected = false
            }));
            #endregion

            #region ==================================== Total Leads infos ====================================
            TotalShowLeadCount = OnUseLeadsCollection.Count;
            CurrentPageCount = (int)Math.Round((decimal)(TotalShowLeadCount / _LimitValue), 0, MidpointRounding.ToEven);
            #endregion

            #region ==================================== Users collection ====================================
            UsersCollections.Clear();
            if (this.CurrentUserProfileInfos.Role != Helper.ToolsBox.ADARole.user.ToString())
            {
                var l_UserService = CoreSDK.Framework.Services.Container().Resolve<IUserService>();
                var l_UsersList = await l_UserService.GetUsers("?filter=%7B%22Profile%22%3A%7B%22DomainsOfAction%22%3A%7B%22newcars%22%3Atrue%7D%7D%7D");
                UsersCollections.AddRange(l_UsersList);
            }
            #endregion
        }

        public async Task LoadMoreLeadData(LeadsItemViewModel p_Parameter)
        {
            if (LeadFilterModelsList.Count() == 0)
            {
                var l_CreatedDate = (p_Parameter != null) ? p_Parameter.CreatedDate : DateTime.MaxValue;
                var l_Items = Load(l_CreatedDate);
                LeadsCollection.AddRange(l_Items);
                if (SelectedUser == null)
                    await RazUserSelection(null, false);
                else
                    await RazUserSelection(SelectedUser, true);
            }
        }

        public async Task LoadMoreMyLeadOfTheDateData(LeadsItemViewModel p_Parameter)
        {
            var l_CreatedDate = (p_Parameter != null) ? p_Parameter.CreatedDate : DateTime.MaxValue;
            var l_Items = (LeadFilterModelsList.Count > 0) ? LoadOfTheDateDataWithFilter(l_CreatedDate) : LoadLeadOfTheDate(l_CreatedDate);
            OnUseDayLeadsCollection.AddRange(l_Items);
        }

        public async Task LoadMoreMyLeadOnGoingData(LeadsItemViewModel p_Parameter)
        {
            var l_CreatedDate = (p_Parameter != null) ? p_Parameter.CreatedDate : DateTime.MaxValue;
            var l_Items = (LeadFilterModelsList.Count > 0) ? LoadOnGoingDataWithFilter(l_CreatedDate) : LoadLeadOnGoing(l_CreatedDate);
            OnUseOthersDayLeadsCollection.AddRange(l_Items);
        }

        public IEnumerable<LeadsItemViewModel> Load(DateTime? fromDate)
        {
            if (fromDate == DateTime.MaxValue)
                return OnUseLeadsCollection.OrderByDescending(order => order.CreatedDate).Take(20);

            if (!OnUseLeadsCollection.Any(o => o.CreatedDate < fromDate))
                return new List<LeadsItemViewModel>();

            return OnUseLeadsCollection.Where(order => order.CreatedDate <= fromDate).OrderByDescending(order => order.CreatedDate).Take(20);

        }

        public IEnumerable<LeadsItemViewModel> LoadDataWithFilter(DateTime? fromDate)
        {
            if (fromDate == DateTime.MaxValue)
                return LeadsForFilterCollection.OrderByDescending(order => order.CreatedDate)
                                            .Take(20);

            if (!LeadsForFilterCollection.Any(o => o.CreatedDate < fromDate))
                return new List<LeadsItemViewModel>();

            return LeadsForFilterCollection.Where(filter => LeadFilterModelsList.Contains(filter.Tag)).Where(order => order.CreatedDate <= fromDate).OrderByDescending(order => order.CreatedDate)
                                        .Take(20);
        }

        public IEnumerable<LeadsItemViewModel> LoadLeadOfTheDate(DateTime? fromDate)
        {
            if (fromDate == DateTime.MaxValue)
                return SaveOnUseDayLeadsCollection.OrderByDescending(order => order.CreatedDate).Take(10);

            if (!SaveOnUseDayLeadsCollection.Any(o => o.CreatedDate < fromDate))
                return new List<LeadsItemViewModel>();

            return SaveOnUseDayLeadsCollection.Where(order => order.CreatedDate <= fromDate).OrderByDescending(order => order.CreatedDate).Take(10);

        }

        public IEnumerable<LeadsItemViewModel> LoadLeadOnGoing(DateTime? fromDate)
        {
            if (fromDate == DateTime.MaxValue)
                return SaveOnUseOthersDayLeadsCollection.OrderByDescending(order => order.CreatedDate).Take(10);

            if (!SaveOnUseOthersDayLeadsCollection.Any(o => o.CreatedDate < fromDate))
                return new List<LeadsItemViewModel>();

            return SaveOnUseOthersDayLeadsCollection.Where(order => order.CreatedDate <= fromDate).OrderByDescending(order => order.CreatedDate).Take(10);

        }

        public IEnumerable<LeadsItemViewModel> LoadOfTheDateDataWithFilter(DateTime? fromDate)
        {
            if (fromDate == DateTime.MaxValue)
                return LeadsOfTheDateForFilterCollection.OrderByDescending(order => order.CreatedDate)
                                            .Take(10);

            if (!LeadsOfTheDateForFilterCollection.Any(o => o.CreatedDate < fromDate))
                return new List<LeadsItemViewModel>();

            return LeadsOfTheDateForFilterCollection.Where(filter => LeadFilterModelsList.Contains(filter.Tag)).Where(order => order.CreatedDate <= fromDate).OrderByDescending(order => order.CreatedDate)
                                        .Take(10);
        }

        public IEnumerable<LeadsItemViewModel> LoadOnGoingDataWithFilter(DateTime? fromDate)
        {
            if (fromDate == DateTime.MaxValue)
                return LeadsOnGoingForFilterCollection.OrderByDescending(order => order.CreatedDate)
                                            .Take(10);

            if (!LeadsOnGoingForFilterCollection.Any(o => o.CreatedDate < fromDate))
                return new List<LeadsItemViewModel>();

            return LeadsOnGoingForFilterCollection.Where(filter => LeadFilterModelsList.Contains(filter.Tag)).Where(order => order.CreatedDate <= fromDate).OrderByDescending(order => order.CreatedDate)
                                        .Take(10);
        }

        private async Task RefreshingData(string p_Parameter)
        {
            #region ==================================== Leads collection ====================================

            var l_LeadService = CoreSDK.Framework.Services.Container().Resolve<ILeadService>();
            var l_LeadsList = await l_LeadService.GetLeads(p_Parameter);
            var l_CompareData = LeadsCollection.Select(x => x.LeadId).ToList();

            LeadsCollection.AddRange(l_LeadsList.Where(y => !l_CompareData.Contains(y.LeadId)).OrderByDescending(a => a.CreatedDate)
                .Select(x => new LeadsItemViewModel()
                {
                    StatusColor = GetStatusColor(x.CreatedDate),
                    //IsSeleted = (Constants._LeadsStatus.Contains(x.Status)) ? true : false,
                    ShowAffectationName = (x.UserId != null) ? true : false,
                    FormattedDate = String.Format("{0:g}", x.CreatedDate),
                    FormattedHeure = String.Format("{0:t}", x.CreatedDate),
                    IsLineVisble = (IsAllocateButtonVisible) ? true : (x.UserId != null) ? false : true
                }.SetLeadsEncapsulation(x)));

            // Only if the showroom is managed and user role is simple user
            var l_SaveManagedShowRoomLeadCollections = ManagedShowRoomLeadCollections.Select(z => z.FormatedDate).ToList();
            if (CurrentUserProfileInfos.Showroom.LeadConfig == Helper.ToolsBox.ADAShowroomLeadConfig.managed.ToString() && CurrentUserProfileInfos.Role == Helper.ToolsBox.ADARole.user.ToString())
                ManagedShowRoomLeadCollections.AddRange(
                    LeadsCollection.Where(a => !l_SaveManagedShowRoomLeadCollections.Contains(a.FormattedDate))
                                    .Where(y => y.UserId == CurrentUserProfileInfos.UserId).OrderBy(date => date.CreatedDate)
                                    .GroupBy(x => x.FormattedDate)
                                    .Select(group => new ManagedShowRoomLead
                                    {
                                        FormatedDate = group.Key,
                                        LeadCount = group.Count()
                                    }));

            var l_LeadFilterModelsCollections = LeadFilterModelsCollections.Select(a => a.Libelle).ToList();
            LeadFilterModelsCollections.Clear();
            LeadFilterModelsCollections.AddRange(
                LeadsCollection.Where(a => !l_LeadFilterModelsCollections.Contains(a.Tag))
                .Where(x => !string.IsNullOrEmpty(x.Tag))
                .OrderBy(y => y.Tag).
                GroupBy(z => z.Tag).
                Select(group => new LeadFilterModels
                {
                    Libelle = group.Key,
                    IsSelected = false
                }));

            #endregion

            #region ==================================== Users collection ====================================
            if (this.CurrentUserProfileInfos.Role != Helper.ToolsBox.ADARole.user.ToString())
            {
                var l_SaveUserCollections = UsersCollections.Select(a => a.UserId).ToList();
                var l_UserService = CoreSDK.Framework.Services.Container().Resolve<IUserService>();
                var l_UsersList = await l_UserService.GetUsers("filter=%7B%22Profile%22%3A%7B%22DomainsOfAction%22%3A%7B%22newcars%22%3Atrue%7D%7D%7D");
                UsersCollections.AddRange(l_UsersList.Where(a => !l_SaveUserCollections.Contains(a.UserId)));
            }
            #endregion
        }

        private string GetStatusColor(DateTime p_DateCreation)
        {
            var l_CompareData = (DateTime.Now - p_DateCreation).TotalHours;
            var l_RightColor = "";

            if (l_CompareData < 0)
                l_RightColor = string.Empty;
            if (l_CompareData > 0 && l_CompareData < 12)
                l_RightColor = MyADA.Common.ViewModels.Helper.ToolsBox.ADALeadStatusColor.gray.ToString();
            else if (12 < l_CompareData && l_CompareData < 24)
                l_RightColor = MyADA.Common.ViewModels.Helper.ToolsBox.ADALeadStatusColor.orange.ToString();
            else if (l_CompareData > 24)
                l_RightColor = MyADA.Common.ViewModels.Helper.ToolsBox.ADALeadStatusColor.red.ToString();

            return l_RightColor;
        }
        #endregion

        #region ==================================== Commands ====================================
        protected override async void OnShowAllocatedLeadCommand_Execute(object p_Parameter)
        {
            if (IsWorkingView)
                return;
            try
            {
                IsWorkingView = true;
                IsBusy = true;
                await Task.Delay(1000);

                // Remove filter
                if (LeadFilterModelsList.Count > 0) DoFilterCommand.Execute(null);

                var l_Parameter = int.Parse(p_Parameter.ToString());
                var l_ShowAllocated = (l_Parameter == 1) ? true : false;
                // Correction des erreurs de double appel.
                IsAllocatedVisible = (IsAllocatedVisible == l_ShowAllocated) ? !l_ShowAllocated : l_ShowAllocated;

                var l_Status = (this.IsAllocatedVisible) ? System.Net.WebUtility.UrlEncode(JsonConvert.SerializeObject(_ListInterestLeadStatusForMyLead.ToArray())) : _NewLeadStatus;
                var l_Url = "?ShowroomId=" + CurrentUserProfileInfos.ShowroomId + "&Status=" + l_Status;
                var l_LeadService = CoreSDK.Framework.Services.Container().Resolve<ILeadService>();
                var l_LeadsList = await l_LeadService.GetLeads(l_Url);
                LeadsCollection.Clear();
                OnUseLeadsCollection.Clear();
                OnUseLeadsCollection.AddRange(l_LeadsList.OrderByDescending(a => a.CreatedDate).Select(x => new LeadsItemViewModel()
                {
                    StatusColor = GetStatusColor(x.CreatedDate),
                    IsSeleted = false,
                    ShowAffectationName = (x.UserId != null) ? true : false,
                    FormattedDate = String.Format("{0:g}", x.CreatedDate),
                    IsLineVisble = true
                }.SetLeadsEncapsulation(x)));

                LeadFilterModelsCollections.Clear();
                LeadFilterModelsCollections.AddRange(OnUseLeadsCollection.Where(x => !string.IsNullOrEmpty(x.Tag)).OrderBy(y => y.Tag).GroupBy(z => z.Tag).Select(group => new LeadFilterModels
                {
                    Libelle = group.Key,
                    IsSelected = false
                }));

                await LoadMoreLeadData(null);

                IsBusy = false;
                IsWorkingView = false;
            }
            catch (Exception ex)
            {
                IsWorkingView = false;
                IsBusy = false;
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
                IsWorkingView = false;
            }
        }

        protected override async void OnShowLeadDetailsCommand_Execute(object p_Parameter)
        {
            if (IsWorkingView)
                return;
            if (SelectedUser != null || CurrentUserProfileInfos.Role == Helper.ToolsBox.ADARole.user.ToString() || CurrentUserProfileInfos.Showroom.LeadConfig == Helper.ToolsBox.ADAShowroomLeadConfig.available.ToString())
            {
                try
                {

                    IsWorkingView = true;
                    IsBusy = true;
                    SelectedLead = (p_Parameter != null) ? (LeadsItemViewModel)p_Parameter : SelectedLead;
                    var tes = SelectedLead;
                    var l_LeadService = CoreSDK.Framework.Services.Container().Resolve<ILeadService>();
                    var l_LeadDetails = await l_LeadService.GetLead(SelectedLead.LeadId);
                    SelectedLeadDetails = new LeadsInfosItemViewModel()
                    {
                        FullName = l_LeadDetails.FirstName + " " + l_LeadDetails.LastName,
                        FormattedDate = String.Format("{0:d}", l_LeadDetails.CreatedDate),
                        FormattedHeure = String.Format("{0:t}", l_LeadDetails.CreatedDate),
                        IsPhoneVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.PhoneNumber),
                        IsEmailVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.Email),
                        IsFacebookVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.FacebookId),
                        IsKakaoVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.KakaoId),
                        IsModelVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.Category),
                        IsCommentVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.Comment)
                    }.SetLeadsEncapsulation(l_LeadDetails);

                    var l_ActualIndexOfData = LeadsCollection.IndexOf(SelectedLead);
                    if (l_ActualIndexOfData == LeadsCollection.Count() - 1)
                    {
                        ShowNextButton = false;
                    }
                    else
                    {
                        ShowNextButton = true;
                    }

                    // Unselect all selected line
                    foreach (var item in LeadsCollection)
                    {
                        item.IsSeleted = false;
                    }


                    //LeadsCollection.Where(x => x.LeadId == SelectedLead.LeadId).FirstOrDefault().IsSeleted = true;

                    if (this.CurrentUserProfileInfos.Role == Helper.ToolsBox.ADARole.user.ToString() || CurrentUserProfileInfos.Showroom.LeadConfig == Helper.ToolsBox.ADAShowroomLeadConfig.available.ToString())
                    {
                        IsAvailableShowroom = true;
                        ShowSaveButton = true;
                        ShowAllocateButton = false;
                        ShowNextButton = false;
                        NavigateLeadHeaderTitle = Properties.Resources.NewLeads;
                        PreviousViewName = ViewName.BookLeadsView;
                    }
                    else
                    {
                        IsAvailableShowroom = false;
                        NavigateLeadHeaderTitle = Properties.Resources.AllocateLeadTo;
                        ShowSaveButton = false;
                        ShowAllocateButton = true;
                        PreviousViewName = ViewName.MyLeadsView;
                    }

                    CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.NavigateLeadsView, this);
                    //IsDefaultLeadCollectionFilter = false;
                    IsWorkingView = false;
                }
                catch (Exception ex)
                {
                    IsWorkingView = false;
                    CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                }
                finally
                {
                    IsWorkingView = false;
                    EndWork();
                }
            }
            else
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, "Please select a user.");
        }

        protected override async void OnSelectRepresentativeCommand_Execute(object p_Parameter)
        {
            if (IsWorkingView)
                return;
            try
            {
                IsWorkingView = true;
                await RazUserSelection(p_Parameter, true);

                CoreSDK.Framework.Services.InteractionService().Close();
            }
            catch (Exception ex)
            {
                IsWorkingView = false;
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
                IsWorkingView = false;
                EndWork();
            }
        }

        private async Task RazUserSelection(object p_Parameter, bool p_Boolean = true)
        {
            PlaceHolderForLeadsPicker = (p_Parameter != null) ? ((UserInfos)p_Parameter).FullName : Properties.Resources.AllocateTo;
            SelectedUser = (p_Parameter != null) ? ((UserInfos)p_Parameter) : null;
            // Traitement to show hidden columns.
            IsAllocateButtonVisible = p_Boolean;

            //TODO-GAV : Remove this part.
            foreach (var item in LeadsCollection)
            {
                item.ShowSelectionIcone = p_Boolean;
            }

            foreach (var item in OnUseLeadsCollection)
            {
                item.ShowSelectionIcone = p_Boolean;
            }

            /*foreach (var item in OnSaveAllocatedLeadsCollection)
            {
                item.ShowSelectionIcone = p_Boolean;
            }*/
        }

        protected override async void OnAddOrRemoveAffectationCommand_Execute(object p_Parameter)
        {
            var l_LeadParameter = (LeadsItemViewModel)p_Parameter;
            if (LeadsCollection.Where(x => x.LeadId == l_LeadParameter.LeadId).FirstOrDefault().User != null)
            {
                if (await CoreSDK.Framework.Services.InteractionService().Confirm("Confirm message", MyADA.Common.ViewModels.Properties.Resources.ReallocateLead_Message))
                {
                    LeadsCollection.Where(x => x.LeadId == l_LeadParameter.LeadId).FirstOrDefault().IsSeleted = (l_LeadParameter.IsSeleted) ? false : true;
                }
            }
            else
            {
                LeadsCollection.Where(x => x.LeadId == l_LeadParameter.LeadId).FirstOrDefault().IsSeleted = (l_LeadParameter.IsSeleted) ? false : true;
            }
        }

        protected override async void OnAllocateCommand_Execute(object p_Parameter)
        {
            if (IsWorkingView)
                return;
            try
            {
                IsWorkingView = true;
                IsBusy = true;
                BeginWork();
                if (p_Parameter != null)
                {

                    if (((LeadsItemViewModel)p_Parameter).User != null)
                    {
                        if (await CoreSDK.Framework.Services.InteractionService().Confirm("Confirm message", MyADA.Common.ViewModels.Properties.Resources.ReallocateLead_Message))
                        {
                            LeadsCollection.Where(x => x.LeadId == ((LeadsItemViewModel)p_Parameter).LeadId).FirstOrDefault().IsSeleted = true;
                        }
                        else
                        {
                            IsWorkingView = false;
                            IsBusy = false;
                            return;
                        }
                    }
                    else
                        LeadsCollection.Where(x => x.LeadId == ((LeadsItemViewModel)p_Parameter).LeadId).FirstOrDefault().IsSeleted = true;
                }

                var l_LeadsServices = CoreSDK.Framework.Services.Container().Resolve<ILeadService>();
                SelectedLeadsList.Clear();
                SelectedLeadsList.AddRange(LeadsCollection.Where(x => x.IsSeleted == true).ToList().Select(y => y.LeadId));
                if (SelectedLeadsList.Count == 0)
                {
                    CoreSDK.Framework.Services.InteractionService().Alert("", "Please select at least one lead.");
                    IsWorkingView = false;
                    IsBusy = false;
                }
                else
                {
                    await l_LeadsServices.AssignLeadToUser(new Services.Requests.AssignLeadToUserRequest()
                    {
                        LeadId = SelectedLeadsList.ToArray(),
                        UserId = SelectedUser.UserId
                    });

                    //var l_ListSaveDeleteItems = new ICollection<LeadsInfosItemViewModel>();
                    foreach (var item in LeadsCollection)
                    {
                        if (SelectedLeadsList.Contains(item.LeadId))
                        {
                            item.Status = Helper.ToolsBox.ADALeadStatus.allocated.ToString();
                            item.UserId = SelectedUser.UserId;
                            item.User = new User()
                            {
                                FirstName = SelectedUser.FirstName,
                                LastName = SelectedUser.LastName,
                                Title = SelectedUser.Title
                            };
                            item.IsSeleted = false;
                            item.ShowAffectationName = true;
                            item.IsLineVisble = true;
                        }
                    }
                    ShowAllocateButton = false;
                    IsBusy = false;

                    // RAZ grid selection
                    if (SelectedLead == null)
                        await RazUserSelection(null, false);

                    IsWorkingView = false;
                }

            }
            catch (Exception ex)
            {
                IsWorkingView = false;
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
                IsWorkingView = false;
                EndWork();
            }

        }

        protected override async void OnNextCommand_Execute(object p_Parameter)
        {
            if (IsWorkingView)
                return;
            try
            {
                IsWorkingView = true;
                IsBusy = true;

                BeginWork();
                var l_ActualIndexOfData = LeadsCollection.IndexOf(SelectedLead);
                var l_OldIndexOfData = l_ActualIndexOfData - 1;

                // Unselect first item
                if (l_OldIndexOfData > -1)
                    LeadsCollection.Where(x => x.LeadId == LeadsCollection.FirstOrDefault().LeadId).FirstOrDefault().IsSeleted = false;


                if (l_ActualIndexOfData == LeadsCollection.Count() - 1)
                {
                    ShowNextButton = false;
                    SelectedLead = LeadsCollection.LastOrDefault();
                }
                else
                {
                    ShowNextButton = true;
                    SelectedLead = LeadsCollection[l_ActualIndexOfData + 1];
                }

                LeadsCollection.Where(x => x.LeadId == SelectedLead.LeadId).FirstOrDefault().IsSeleted = true;
                var l_LeadService = CoreSDK.Framework.Services.Container().Resolve<ILeadService>();
                var l_LeadDetails = await l_LeadService.GetLead(SelectedLead.LeadId);
                SelectedLeadDetails = new LeadsInfosItemViewModel()
                {
                    FullName = l_LeadDetails.FirstName + " " + l_LeadDetails.LastName,
                    FormattedDate = String.Format("{0:d}", l_LeadDetails.CreatedDate),
                    FormattedHeure = String.Format("{0:t}", l_LeadDetails.CreatedDate),
                    IsPhoneVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.PhoneNumber),
                    IsEmailVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.Email),
                    IsFacebookVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.FacebookId),
                    IsKakaoVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.KakaoId),
                    IsModelVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.Category),
                    IsCommentVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.Comment)
                }.SetLeadsEncapsulation(l_LeadDetails);

                ShowAllocateButton = true;

                IsBusy = false;

            }
            catch (Exception ex)
            {
                IsWorkingView = false;
                IsBusy = false;
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
                IsWorkingView = false;
                EndWork();
            }


        }

        protected async override void OnSimpleUserAllocateLeadCommand_Execute(object p_Parameter)
        {
            if (IsWorkingView)
                return;
            try
            {
                IsWorkingView = true;
                IsBusy = true;

                var l_SelectedLine = (p_Parameter != null) ? (LeadsItemViewModel)p_Parameter : SelectedLead;

                var l_LeadsServices = CoreSDK.Framework.Services.Container().Resolve<ILeadService>();
                SelectedLeadsList.Clear();
                SelectedLeadsList.Add(l_SelectedLine.LeadId);

                ShowSaveButton = false;

                await l_LeadsServices.AssignLeadToUser(new Services.Requests.AssignLeadToUserRequest()
                {
                    LeadId = SelectedLeadsList.ToArray(),
                    UserId = CurrentUserProfileInfos.UserId
                });

                LeadsCollection.Remove(l_SelectedLine);

                ShowNoLeadMessage = (LeadsCollection.Count == 0) ? true : false;
                IsWorkingView = false;
                IsBusy = false;
            }
            catch (Exception ex)
            {
                IsWorkingView = false;
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
            }

        }

        protected override async void OnBackCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBusy = true;
                //var l_ListLineInvisible = LeadsCollection.Where(x => x.IsLineVisble == false);

                foreach (var item in LeadsCollection)
                {
                    item.IsSeleted = false;
                }

                await Task.Delay(1000);
                await CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(PreviousViewName, this).ConfigureAwait(false);
                //IsBusy = false;
            }
            finally
            {
            }
        }

        public async Task ShowMyLeadsCommandFunction()
        {
            OnShowMyLeadsCommand_Execute(null);
        }

        protected override async void OnShowMyLeadsCommand_Execute(object p_Parameter)
        {
            try
            {
                if (IsWorkingView)
                    return;

                IsWorkingView = true;

                IsBusy = true;
                OnUseDayLeadsCollection.Clear();
                SaveOnUseDayLeadsCollection.Clear();
                OnUseOthersDayLeadsCollection.Clear();
                SaveOnUseOthersDayLeadsCollection.Clear();
                var l_LeadService = CoreSDK.Framework.Services.Container().Resolve<ILeadService>();
                var l_Parameter = "?ShowroomId=" + CurrentUserProfileInfos.ShowroomId;
                //var l_GetLeadsAllocated = await l_LeadService.GetLeads(l_Parameter + "Status=" + Helper.ToolsBox.ADALeadStatus.allocated.ToString());
                //var l_GetLeadsOnGoing = await l_LeadService.GetLeads(l_Parameter + "Status=" + _Ongoing);

                var l_GetAllocatedAndOngoingLeads = await l_LeadService.GetLeads(l_Parameter + "&Status=" + System.Net.WebUtility.UrlEncode(JsonConvert.SerializeObject(_ListInterestLeadStatusForMyOwnLead.ToArray())));

                // List for the first code block
                SaveOnUseDayLeadsCollection.AddRange(
                    l_GetAllocatedAndOngoingLeads.Where(a => a.Status == _AllocatedLeadStatus)
                    .OrderByDescending(b => b.CreatedDate)
                    .Select(x => new LeadsItemViewModel()
                    {
                        StatusColor = GetStatusColor(x.CreatedDate),
                        ShowAffectationName = (x.UserId != null) ? true : false,
                        FormattedDate = String.Format("{0:d}", x.CreatedDate),
                        IsLineVisble = true
                    }.SetLeadsEncapsulation(x)));

                var l_Items = LoadLeadOfTheDate(DateTime.MaxValue);
                OnUseDayLeadsCollection.AddRange(l_Items);

                // List for the second code block.
                SaveOnUseOthersDayLeadsCollection.AddRange(l_GetAllocatedAndOngoingLeads.Where(a => a.Status == _Ongoing)
                    .OrderByDescending(b => b.RecontactOn ?? DateTime.MinValue.ToString())
                    .Select(x => new LeadsItemViewModel()
                    {
                        StatusColor = (x.RecontactOn != null) ? GetStatusColor(Convert.ToDateTime(x.RecontactOn)) : MyADA.Common.ViewModels.Helper.ToolsBox.ADALeadStatusColor.red.ToString(),
                        ShowAffectationName = (x.UserId != null) ? true : false,
                        FormattedDate = (x.RecontactOn != null) ? String.Format("{0:d}", Convert.ToDateTime(x.RecontactOn)) : string.Empty,
                        IsLineVisble = true
                    }.SetLeadsEncapsulation(x)));

                var l_SItems = LoadLeadOnGoing(DateTime.MaxValue);
                OnUseOthersDayLeadsCollection.AddRange(l_SItems);

                LeadFilterModelsCollections.Clear();
                LeadFilterModelsCollections.AddRange(l_GetAllocatedAndOngoingLeads.Where(x => !string.IsNullOrEmpty(x.Tag)).OrderBy(y => y.Tag).GroupBy(z => z.Tag).Select(group => new LeadFilterModels
                {
                    Libelle = group.Key,
                    IsSelected = false
                }));

                // Show notif number
                NotifUseDayLeadsCollection = SaveOnUseDayLeadsCollection.Count().ToString();
                NotifUseOthersDayLeadsCollection = SaveOnUseOthersDayLeadsCollection.Count().ToString();

                IsDefaultLeadCollectionFilter = false;
                RazFilter();
                CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MyOwnLeadsView, this);
                IsBusy = false;
                CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>().IsBusy = false;
                CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>().IsBusy = false;
                CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>().IsBurgerPresented = false;

                IsWorkingView = false;
            }
            finally
            {
            }

        }

        protected override async void OnDoFilterCommand_Execute(object p_Parameter)
        {
            IsBusy = true;

            if (p_Parameter != null)
            {
                var l_Parameter = (LeadFilterModels)p_Parameter;
                l_Parameter.IsSelected = !l_Parameter.IsSelected;

                if (l_Parameter.IsSelected)
                    LeadFilterModelsList.Add(l_Parameter.Libelle);
                else
                    LeadFilterModelsList.Remove(l_Parameter.Libelle);
            }
            else
            {
                LeadFilterModelsList.Clear();
                LeadFilterModelsCollections.Clear();
                LeadFilterModelsCollections.AddRange(OnUseLeadsCollection.Where(x => !string.IsNullOrEmpty(x.Tag)).OrderBy(y => y.Tag).GroupBy(z => z.Tag).Select(group => new LeadFilterModels
                {
                    Libelle = group.Key,
                    IsSelected = false
                }));
            }

            if (IsDefaultLeadCollectionFilter)
                LeadsCollection.Clear();
            else
            {
                OnUseDayLeadsCollection.Clear();
                OnUseOthersDayLeadsCollection.Clear();
            }

            if (LeadFilterModelsList.Count() > 0)
            {
                FilterStyleDetails = _FilterRed;
                FilterStyleRoot = _FilterRed;
            }
            else
            {
                FilterStyleDetails = _FilterWhite;
                FilterStyleRoot = _FilterDarkGrey;
            }

            await Task.Delay(200);

            FilterDoUpdate = true;
            IsBusy = false;
        }

        private async Task LoadFilterData(LeadsItemViewModel p_Parameter)
        {
            if (LeadFilterModelsList.Count() > 0)
            {

                FilterStyleDetails = _FilterRed;
                FilterStyleRoot = _FilterRed;

                if (IsDefaultLeadCollectionFilter)
                {
                    IsBusy = true;

                    LeadsForFilterCollection.Clear();
                    LeadsForFilterCollection.AddRange(OnUseLeadsCollection.Where(filter => LeadFilterModelsList.Contains(filter.Tag)));

                    var l_CreatedDate = (p_Parameter != null) ? p_Parameter.CreatedDate : DateTime.MaxValue;
                    var l_Items = LoadDataWithFilter(l_CreatedDate);
                    LeadsCollection.AddRange(l_Items);
                    if (SelectedUser == null)
                        RazUserSelection(null, false);
                    else
                        RazUserSelection(SelectedUser, true);
                    IsBusy = false;
                }
                else
                {
                    LeadsOfTheDateForFilterCollection.Clear();
                    LeadsOfTheDateForFilterCollection.AddRange(SaveOnUseDayLeadsCollection.Where(filter => LeadFilterModelsList.Contains(filter.Tag)));

                    LeadsOnGoingForFilterCollection.Clear();
                    LeadsOnGoingForFilterCollection.AddRange(SaveOnUseOthersDayLeadsCollection.Where(filter => LeadFilterModelsList.Contains(filter.Tag)));

                    var l_CreatedDate = (p_Parameter != null) ? p_Parameter.CreatedDate : DateTime.MaxValue;
                    var l_Items = LoadOfTheDateDataWithFilter(l_CreatedDate);
                    OnUseDayLeadsCollection.AddRange(l_Items);

                    var l_SItems = LoadOnGoingDataWithFilter(l_CreatedDate);
                    OnUseOthersDayLeadsCollection.AddRange(l_SItems);

                    /*foreach (var item in SaveOnUseDayLeadsCollection)
                    {
                        foreach (var child in LeadFilterModelsList)
                        {
                            if (child == item.Tag)
                                OnUseDayLeadsCollection.Add(item);
                        }
                    }*/

                    /*foreach (var item in SaveOnUseOthersDayLeadsCollection)
                    {
                        foreach (var child in LeadFilterModelsList)
                        {
                            if (child == item.Tag)
                                OnUseOthersDayLeadsCollection.Add(item);
                        }
                    }*/
                }
            }
            else
            {
                FilterStyleDetails = _FilterWhite;
                FilterStyleRoot = _FilterDarkGrey;

                if (IsDefaultLeadCollectionFilter)
                {
                    if (LeadsCollection.Count == 0)
                        await LoadMoreLeadData(null);
                }
                else
                {
                    /*OnUseDayLeadsCollection.Clear();
                    OnUseOthersDayLeadsCollection.Clear();
                    foreach (var item in SaveOnUseDayLeadsCollection)
                    {
                        OnUseDayLeadsCollection.Add(item);
                    }

                    foreach (var item in SaveOnUseOthersDayLeadsCollection)
                    {
                        OnUseOthersDayLeadsCollection.Add(item);
                    }*/
                    var l_Items = LoadLeadOfTheDate(DateTime.MaxValue);
                    OnUseDayLeadsCollection.AddRange(l_Items);

                    var l_SItems = LoadLeadOnGoing(DateTime.MaxValue);
                    OnUseOthersDayLeadsCollection.AddRange(l_SItems);
                }
            }
        }

        public void RazFilter(bool p_RemoveSelectedLine = true)
        {
            /*LeadFilterModelsList.Clear();

            foreach (var item in LeadFilterModelsCollections)
            {
                item.IsSelected = false;
            }

            if (p_RemoveSelectedLine)
                foreach (var item in LeadsCollection)
                {
                    item.IsLineVisble = true;
                }

            foreach (var item in OnUseDayLeadsCollection)
            {
                item.IsLineVisble = true;
            }

            foreach (var item in OnUseOthersDayLeadsCollection)
            {
                item.IsLineVisble = true;
            }*/
        }

        protected override async void OnShowMyLeadDetailsCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBusy = true;
                SelectedLead = (LeadsItemViewModel)p_Parameter;
                //var tes = SelectedLead;
                var l_LeadService = CoreSDK.Framework.Services.Container().Resolve<ILeadService>();
                var l_LeadDetails = await l_LeadService.GetLead(SelectedLead.LeadId);
                SelectedLeadDetails = new LeadsInfosItemViewModel()
                {
                    FullName = l_LeadDetails.FirstName + " " + l_LeadDetails.LastName,
                    FormattedDate = String.Format("{0:d}", l_LeadDetails.CreatedDate),
                    FormattedHeure = String.Format("{0:t}", l_LeadDetails.CreatedDate),
                    IsPhoneVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.PhoneNumber),
                    IsEmailVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.Email),
                    IsFacebookVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.FacebookId),
                    IsKakaoVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.KakaoId),
                    IsModelVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.Category),
                    IsCommentVisible = !String.IsNullOrWhiteSpace(l_LeadDetails.Comment)
                }.SetLeadsEncapsulation(l_LeadDetails);

                IsDefaultLeadCollectionFilter = false;

                PreviousViewName = ViewName.MyOwnLeadsView;
                //CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.LeadsDetailsWorkflowView, this);
                IsBusy = false;
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override async void OnSetLeadStatusCommand_Execute(object p_Parameter)
        {

            if (IsWorkingView)
                return;
            try
            {
                IsWorkingView = true;
                IsBusy = true;
                var l_LeadService = CoreSDK.Framework.Services.Container().Resolve<ILeadService>();

                // Manage Lead status
                var l_StatusValue = "";
                if (SelectedLeadDetails.IsDone)
                    l_StatusValue = _DoneLeadStatus;
                else if (SelectedLeadDetails.IsNotInterest)
                    l_StatusValue = _NotInterestLeadStatus;
                else if ((SelectedLeadDetails.IsRecontact))
                {
                    l_StatusValue = _Ongoing;

                }

                if (l_StatusValue.Length > 0)
                {
                    var l_Parameter = new SetLeadStatusRequest()
                    {
                        Status = l_StatusValue,
                        Comment = "",
                        RecontactOn = (l_StatusValue == _Ongoing) ? PickerDateValue : null
                    };

                    var l_SetLeadStatusService = await l_LeadService.SetLeadStatus(SelectedLeadDetails.LeadId, l_Parameter);

                    if (l_SetLeadStatusService)
                    {

                        CoreSDK.Framework.Services.InteractionService().Alert("", "Lead updated");
                        var l_SeletedLead = OnUseDayLeadsCollection.Where(a => a.LeadId == SelectedLeadDetails.LeadId).FirstOrDefault();
                        OnUseDayLeadsCollection.Remove(l_SeletedLead);
                        SaveOnUseDayLeadsCollection.Remove(l_SeletedLead);

                        //if (l_StatusValue == _Ongoing) {
                        OnUseOthersDayLeadsCollection.Clear();
                        SaveOnUseOthersDayLeadsCollection.Clear();
                        var l_Param = "?ShowroomId=" + CurrentUserProfileInfos.ShowroomId;
                        var l_GetLeadsOnGoing = await l_LeadService.GetLeads(l_Param + "&Status=" + _Ongoing);

                        // List for the second code block.
                        SaveOnUseOthersDayLeadsCollection.AddRange(l_GetLeadsOnGoing.Where(a => a.Status == _Ongoing)
                            .OrderByDescending(b => b.RecontactOn ?? DateTime.MinValue.ToString())
                            .Select(x => new LeadsItemViewModel()
                            {
                                StatusColor = (x.RecontactOn != null) ? GetStatusColor(Convert.ToDateTime(x.RecontactOn)) : MyADA.Common.ViewModels.Helper.ToolsBox.ADALeadStatusColor.red.ToString(),
                                ShowAffectationName = (x.UserId != null) ? true : false,
                                FormattedDate = (x.RecontactOn != null) ? String.Format("{0:d}", Convert.ToDateTime(x.RecontactOn)) : string.Empty,
                                IsLineVisble = true
                            }.SetLeadsEncapsulation(x)));

                        /*var l_SItems = LoadLeadOnGoing(DateTime.MaxValue);
                        OnUseOthersDayLeadsCollection.AddRange(l_SItems);*/
                        OnDoFilterCommand_Execute(null);
                        await LoadFilterData(null);

                        //OnUseOthersDayLeadsCollection.AddRange(SaveOnUseOthersDayLeadsCollection);
                        //}

                        // Update notif count
                        NotifUseDayLeadsCollection = SaveOnUseDayLeadsCollection.Count.ToString();
                        NotifUseOthersDayLeadsCollection = SaveOnUseOthersDayLeadsCollection.Count.ToString();

                        // Load the new page
                        //CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MyOwnLeadsView, this);
                        CoreSDK.Framework.Services.InteractionService().Close();
                    }
                    else
                    {
                        CoreSDK.Framework.Services.InteractionService().Alert("", "Error = Lead doesn't updated.");
                    }
                }
                else
                    CoreSDK.Framework.Services.InteractionService().Alert("", "Please select a status.");

                IsBusy = false;
                IsWorkingView = false;

            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        protected override void OnLiveChangeLeadStatusOnDoneCommand_Execute(object p_Parameter)
        {
            if (p_Parameter != null)
            {
                IsDefaultLeadCollectionFilter = false;

                if (Convert.ToBoolean(p_Parameter))
                {
                    if (SelectedLeadDetails.IsNotInterest)
                        SelectedLeadDetails.IsNotInterest = false;

                    if (SelectedLeadDetails.IsRecontact)
                        SelectedLeadDetails.IsRecontact = false;
                }
            }
        }

        protected override void OnLiveChangeLeadStatusOnRecontactCommand_Execute(object p_Parameter)
        {
            if (p_Parameter != null)
            {

                IsDefaultLeadCollectionFilter = false;
                if (Convert.ToBoolean(p_Parameter))
                {
                    if (SelectedLeadDetails.IsDone)
                        SelectedLeadDetails.IsDone = false;

                    if (SelectedLeadDetails.IsNotInterest)
                        SelectedLeadDetails.IsNotInterest = false;
                }
            }
        }

        protected override void OnLiveChangeLeadStatusOnNotInterestCommand_Execute(object p_Parameter)
        {
            if (p_Parameter != null)
            {
                IsDefaultLeadCollectionFilter = false;
                if (Convert.ToBoolean(p_Parameter))
                {
                    if (SelectedLeadDetails.IsRecontact)
                        SelectedLeadDetails.IsRecontact = false;

                    if (SelectedLeadDetails.IsDone)
                        SelectedLeadDetails.IsDone = false;
                }

            }
        }

        protected override void OnMyOwnLeadsPopinVisibilityCommand_Execute(object p_Parameter)
        {
            MyOwnLeadsPopinIsVisible = (MyOwnLeadsPopinIsVisible) ? false : true;
        }

        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMdd");
        }


        protected override async void OnLoadMoreCommand_Execute(object p_Parameter)
        {
            var l_Check = LeadsCollection.Count != 0 && LeadsCollection.OrderByDescending(o => o.CreatedDate).Last().CreatedDate == ((LeadsItemViewModel)p_Parameter).CreatedDate;
            if (l_Check)
            {
                if (LeadFilterModelsList.Count > 0)
                    await LoadFilterData((LeadsItemViewModel)p_Parameter);
                else
                    await LoadMoreLeadData((LeadsItemViewModel)p_Parameter);
            }
        }

        protected override async void OnCloseFilterViewAndLoadDataCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBusy = true;
                if (FilterDoUpdate)
                {
                    await LoadFilterData(null);
                    FilterDoUpdate = false;
                }
                IsBusy = false;
            }
            catch (Exception)
            {
            }
        }

        protected override async void OnLoadMoreLeadOfTheDateCommand_Execute(object p_Parameter)
        {
            var l_Check = OnUseDayLeadsCollection.Count != 0 && OnUseDayLeadsCollection.OrderByDescending(o => o.CreatedDate).Last().CreatedDate == ((LeadsItemViewModel)p_Parameter).CreatedDate;
            if (l_Check)
            {
                if (LeadFilterModelsList.Count > 0)
                    await LoadFilterData((LeadsItemViewModel)p_Parameter);
                else
                    await LoadMoreMyLeadOfTheDateData((LeadsItemViewModel)p_Parameter);
            }
        }

        protected override async void OnLoadMoreLeadOnGoingCommand_Execute(object p_Parameter)
        {
            var l_Check = OnUseOthersDayLeadsCollection.Count != 0 && OnUseOthersDayLeadsCollection.OrderByDescending(o => o.CreatedDate).Last().CreatedDate == ((LeadsItemViewModel)p_Parameter).CreatedDate;
            if (l_Check)
            {
                if (LeadFilterModelsList.Count > 0)
                    await LoadFilterData((LeadsItemViewModel)p_Parameter);
                else
                    await LoadMoreMyLeadOnGoingData((LeadsItemViewModel)p_Parameter);
            }
        }
    }
}