
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;
using MyADA.Common.DAOServices.Services;
using System.Threading.Tasks;
using MyADA.Common.Services.Services;
using MyADA.Common.Services.Requests;
using MyADA.Common.Services.Models;

namespace MyADA.Common.ViewModels
{
	public partial class HomeViewModel : HomeViewModelBase, IHomeViewModel
	{
        #region ==================================== Properties ====================================
        private string Locale = Properties.Resources.CultureInfoCurrentCultureName;
        private IGlobalConfigService GlobalConfigService;
        public bool IsWorkingView = false;
        #endregion

        #region ==================================== Initialisation ====================================
        protected override void OnInitialize()
        {
            base.OnInitialize();
            ResolveInitialisation();
        }

        private void ResolveInitialisation()
        {
            GlobalConfigService = CoreSDK.Framework.Services.GlobalConfigService();
        }
        #endregion

        #region ==================================== Prepare ViewModel data ====================================
        /// <summary>
        /// Prepare ViewModel
        /// </summary>
        /// <returns></returns>
        public async Task<bool> PrepareVM()
        {
            try
            {
                // By default we don't show any notification ic�ne.
                OverHundredLeadNotif = false;
                IsVisibleLeadNotif = false;
                IsVisibleAudiNewsNotif = false;
                IsVisibleSalesToolNotif = false;
                IsVisibleSelfTrainingNotif = false;
                CurrentMBPage = ViewName.MBXamarinFormsTrainingAppPage;
                IsHomeView = true;

                // Initiate Report Picker
                ShowRoomPickerHeaderText = Properties.Resources.GeneralPickerHeaderText;
                UserPickerHeaderText = Properties.Resources.GeneralPickerHeaderText;
                ShowRoomPickerPlaceHolder = Properties.Resources.Select_Showroom;
                UserPickerPickerPlaceHolder = Properties.Resources.Select_User;

                var l_CountLeadRequest = new CountLeadRequest()
                { 
                    ShowroomId = string.Empty,
                    status = string.Empty,
                    UserId = string.Empty
                };

                // Get user profile infos
                var l_UserService = CoreSDK.Framework.Services.Container().Resolve<IUserService>();
                CurrentUserProfileInfos = await l_UserService.GetProfile();

                await RefreshLeadData();

                return true;
            }
            catch(Exception ex)
            {
                return true;
            }
            finally
            {
            }
        }

        public async Task RefreshLeadData()
        {
            await Task.Run(async() =>
            {
                var l_LeadViewModel = CoreSDK.Framework.Services.Container().Resolve<ILeadViewModel>();
                await l_LeadViewModel.PrepareVM(CurrentUserProfileInfos);

                // Contr�le Lead notification ic�ne
                var l_CountLead = l_LeadViewModel.OnUseLeadsCollection.Count();
                if (l_CountLead > 0) IsVisibleLeadNotif = true;
                if (l_CountLead > 99) OverHundredLeadNotif = true;
                LeadNotif = (OverHundredLeadNotif) ? "99" : l_CountLead.ToString();

            }).ConfigureAwait(false);
        } 
        #endregion

        #region ==================================== View Commands ====================================
        #region ==================================== From Home ====================================
        
        protected override async void OnShowSettingsCommand_Execute(object p_Parameter)
        {
            if(IsWorkingView)
                    return;
            try
            {
                IsBusy = true;
                IsWorkingView = true;
                var l_SettingViewModel = CoreSDK.Framework.Services.Container().Resolve<ISettingsViewModel>();
                var result =  await l_SettingViewModel.PrepareVM(this.CurrentUserProfileInfos);
                IsWorkingView = false;
                if(result)
                    await CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.SettingsProfileView, l_SettingViewModel);
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override void OnShowLeadFromHomeCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBusy = true;
                var l_LeadViewModel = CoreSDK.Framework.Services.Container().Resolve<ILeadViewModel>();
                l_LeadViewModel.IsAllocatedVisible = false;
                //await l_LeadViewModel.PrepareVM(CurrentUserProfileInfos);

                await Task.Delay(1000);
                //l_LeadViewModel.ShowAllocatedLeadCommand.Execute(0);
                if (CurrentUserProfileInfos.Showroom.LeadConfig == Helper.ToolsBox.ADAShowroomLeadConfig.available.ToString())
                {
                    l_LeadViewModel.ShowNoLeadMessage = (l_LeadViewModel.OnUseLeadsCollection.Count == 0) ? true : false;
                    l_LeadViewModel.IsDefaultLeadCollectionFilter = true;
                    await CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.BookLeadsView, l_LeadViewModel);
                }
                else
                {

                    if (CurrentUserProfileInfos.Role != MyADA.Common.ViewModels.Helper.ToolsBox.ADARole.user.ToString())
                    {
                        l_LeadViewModel.IsDefaultLeadCollectionFilter = true;
                        await CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MyLeadsView, l_LeadViewModel);
                    }
                    else
                    {
                        await CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.CommercialLeadsView, l_LeadViewModel);
                    }
                }

                IsBusy = false;
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }
        
        protected override void OnShowAudiNewsFromHomeCommand_Execute(object p_Parameter)
        {
            //try
            //{
            //    IsBusy = true;

            //    CurrentMBPage = ViewName.AudiNewsView;
            //    IsHomeView = false;
            //    CurrentPageHeaderTitle = Properties.Resources.MB_AudiNews;
            //    CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MBXamarinFormsTrainingAppPage, this);

            //    //IsBusy = false;
            //}
            //catch (Exception ex)
            //{
            //    CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            //    IsBusy = false;
            //}
            //finally
            //{
            //}
            }

        protected override void OnShowSalesToolFromHomeCommand_Execute(object p_Parameter)
        {
            //try
            //{
            //    IsBusy = true;

            //    //CurrentMBPage = ViewName.SalesTools;
            //    CurrentMBPage = ViewName.AudiNewsView;
            //    IsHomeView = false;
            //    CurrentPageHeaderTitle = Properties.Resources.MB_SalesTools;
            //    CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MBXamarinFormsTrainingAppPage, this);

            //    //IsBusy = false;
            //}
            //catch (Exception ex)
            //{
            //    CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            //    IsBusy = false;
            //}
            //finally
            //{
            //}
        }
        
        protected override async void OnShowViewReportFromHomeCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBusy = true;

                if (CurrentUserProfileInfos.Role == Helper.ToolsBox.ADARole.user.ToString())
                {
                    var l_ReportService = CoreSDK.Framework.Services.Container().Resolve<IReportService>();
                    ReportSource = await l_ReportService.GetMyReport(CurrentUserProfileInfos.UserId);

                    CoreSDK.Framework.Services.InteractionService().Open(ViewName.ReadDocumentView, this);
                }
                else
                {
                    var l_ShowRoomService = CoreSDK.Framework.Services.Container().Resolve<IShowroomService>();
                    ShowRoomCollections.Clear();
                    UsersCollections.Clear();
                    UsersCollections.AddRange(CoreSDK.Framework.Services.Container().Resolve<ILeadViewModel>().UsersCollections);

                    if (CurrentUserProfileInfos.Profile.Organization.employee)
                    {
                        ShowReportForManager = false;
                        ShowReportForSimpleUser = true;
                        var l_ListShowRoom = await l_ShowRoomService.GetShowroom();
                        ShowRoomCollections.AddRange(l_ListShowRoom);
                        ShowShowroomPicker = true;
                    }
                    else
                    {
                        ShowReportForSimpleUser = true;
                        ShowReportForManager = true;
                        ShowShowroomPicker = false;
                    }
                    CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MyReportView, this);
                }

                IsBusy = false;
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
                
            }
        }

        protected override void OnShowSelfTrainingFromHomeCommand_Execute(object p_Parameter)
        {
            //try
            //{
            //    IsBusy = true;

            //    //CurrentMBPage = ViewName.SelfTraining;
            //    CurrentMBPage = ViewName.AudiNewsView;
            //    IsHomeView = false;
            //    CurrentPageHeaderTitle = Properties.Resources.MB_SelfTraining;
            //    CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MBXamarinFormsTrainingAppPage, this);

            //    //IsBusy = false;
            //}
            //catch (Exception ex)
            //{
            //    CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            //    IsBusy = false;
            //}
            //finally
            //{
            //}
        }

        #endregion


        /// <summary>
        /// Show Rich message details
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override void OnShowRichMessageCommand_Execute(object p_Parameter)
        {
            try
            {
                IsBusy = true;
                var l_Parameter = (RichMessageItem)p_Parameter;
               
                #region ==================================== Get url_image for the detail / Continue traitement if gets an error ====================================
                try
                {
                    var l_CheckImgPropertyIndex = l_Parameter.content.IndexOf("<img");
                    var l_CheckEndOfImgPropertyIndex = l_Parameter.content.IndexOf("\">");
                    var l_ImgProperty = l_Parameter.content.Substring(l_CheckImgPropertyIndex, l_CheckImgPropertyIndex + l_CheckEndOfImgPropertyIndex);
                    var l_ImgSrcPropertyIndex = (l_ImgProperty.Length > 0) ? l_ImgProperty.IndexOf("src=\"") : 0;
                    var l_NewImagePropertyValue = (l_ImgSrcPropertyIndex > 0) ? l_ImgProperty.Substring(l_ImgSrcPropertyIndex + 5, l_ImgProperty.Length - (l_ImgSrcPropertyIndex + 5)) : "";
                    var l_EndOfImgSrcProperty = (l_NewImagePropertyValue.Length > 0) ? l_NewImagePropertyValue.IndexOf("\"") : 0;
                    var l_ImgSrcValue = l_ImgProperty.Substring(l_ImgSrcPropertyIndex + 5, l_ImgSrcPropertyIndex + l_EndOfImgSrcProperty - 5);
                    l_Parameter.image_url = "uri|" + l_ImgSrcValue;
                    l_Parameter.content.Replace(l_ImgProperty, "");
                }
                catch(Exception ex)
                {
                    CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                }
                #endregion
                
                CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.MBNewsDetails, l_Parameter);
                IsBusy = false;
            }
            catch (Exception ex) 
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
            }
        }

        #endregion

        protected override void OnSelectedUserCommand_Execute(object p_Parameter)
        {
            SelectedUser = (p_Parameter != null) ? (UserInfos)p_Parameter : new UserInfos();
            UserPickerPickerPlaceHolder = (SelectedUser != null) ? SelectedUser.FullName : "";
            CoreSDK.Framework.Services.InteractionService().Close();
        }

        protected override void OnSelectedShowroomCommand_Execute(object p_Parameter)
        {
            SelectedShowRoom = (p_Parameter != null) ? (ShowroomModels)p_Parameter : new ShowroomModels();
            ShowRoomPickerPlaceHolder = (SelectedShowRoom != null) ? SelectedShowRoom.Name : "";
            CoreSDK.Framework.Services.InteractionService().Close();
        }

        protected override async void OnShowLeadReportCommand_Execute(object p_Parameter)
        {
            try
            {
                if (IsWorkingView) return;

                IsWorkingView = true;
                IsBusy = true;

                if (SelectedUser != null)
                {
                    var l_ReportService = CoreSDK.Framework.Services.Container().Resolve<IReportService>();
                    ReportSource = await l_ReportService.GetMyReport(SelectedUser.UserId);
                    CoreSDK.Framework.Services.InteractionService().Open(ViewName.ReadDocumentView, this);
                    ShowRoomPickerPlaceHolder = Properties.Resources.Select_Showroom;
                    UserPickerPickerPlaceHolder = Properties.Resources.Select_User;

                }
                else
                {
                    CoreSDK.Framework.Services.InteractionService().Alert("", "Please select a user.");
                }
                IsWorkingView = false;
                IsBusy = false;
            }
            catch (Exception ex )
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBusy = false;
            }
            
        }

        protected override async void OnShowConsolidatedMetricsCommand_Execute(object p_Parameter)
        {
            try
            {
                if (IsWorkingView) return;

                IsWorkingView = true;
                IsBusy = true;
                if (SelectedShowRoom != null || !ShowShowroomPicker)
                {
                    var l_ReportService = CoreSDK.Framework.Services.Container().Resolve<IReportService>();
                    var l_ShowRoomId = (ShowShowroomPicker) ? SelectedShowRoom.ShowroomId : CurrentUserProfileInfos.ShowroomId;
                    ReportSource = await l_ReportService.GetConsolidatedReport(l_ShowRoomId);
                    CoreSDK.Framework.Services.InteractionService().Open(ViewName.ReadDocumentView, this);
                    ShowRoomPickerPlaceHolder = Properties.Resources.Select_Showroom;
                    UserPickerPickerPlaceHolder = Properties.Resources.Select_User;
                }
                else
                {
                    CoreSDK.Framework.Services.InteractionService().Alert("", "Please select a showroom.");
                }
                IsBusy = false;
                IsWorkingView = false;

            }
            catch (Exception ex )
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBusy = false;
            }
        }

        protected override async void OnShowMyLeadsCommand_Execute(object p_Parameter)
        {
            try
            {
                if (IsWorkingView) return;

                IsWorkingView = true;
                IsBusy = true;
                
                var l_LeadViewModel = CoreSDK.Framework.Services.Container().Resolve<ILeadViewModel>();

                l_LeadViewModel.ShowMyLeadsCommandFunction();
                
                //IsBusy = false;
                
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBusy = false;
            }
        }

        protected override async void OnShowOverAllCommand_Execute(object p_Parameter)
        {
            try
            {
                if (IsWorkingView) return;

                IsWorkingView = true;
                IsBusy = true;
                var l_ReportService = CoreSDK.Framework.Services.Container().Resolve<IReportService>();
                ReportSource = await l_ReportService.GetOverAll();
                CoreSDK.Framework.Services.InteractionService().Open(ViewName.ReadDocumentView, this);
                IsBusy = false;
                IsWorkingView = false;

            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                IsBusy = false;
            }
        }
    }
}
