﻿using MyADA.Common.Services.Models;
using System;
using System.Threading.Tasks;
namespace MyADA.Common.ViewModels
{
    public interface ISettingsViewModel
    {
        MyADA.Common.Services.Models.CurrentUserProfile CurrentUserProfileInfos { get; set; }
        Task<bool> PrepareVM(CurrentUserProfile p_CurrentUserProfile);

    }
}
