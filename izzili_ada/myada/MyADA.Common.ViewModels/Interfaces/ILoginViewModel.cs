﻿using System;
namespace MyADA.Common.ViewModels
{
    public interface ILoginViewModel
    {
        string Email { get; set; }
        CoreSDK.IDelegateCommand LoginCommand { get; }
        CoreSDK.IDelegateCommand OpenGeneralConditionsCommand { get; }
    }
}
