﻿using CoreSDK;
using MyADA.Common.Services.Models;
using System;
using System.Threading.Tasks;
namespace MyADA.Common.ViewModels
{
    public interface IBurgerViewModel : IViewModel
    {
        bool IsBurgerPresented { get; set; }
        bool IsBusy { get; set; }
        string SelectedFeedUrl { get; set; }
        string SelectedItemFeedUrl { get; set; }
        Post SelectedItemFeed { get; set; }
        CoreSDK.IDelegateCommand ShowAudiNewsCommand { get; }
        CoreSDK.IDelegateCommand ShowHomeViewCommand { get; }
        CoreSDK.IDelegateCommand ShowLeadCommand { get; }
        CoreSDK.IDelegateCommand ShowMyNewsCommand { get; }
        CoreSDK.IDelegateCommand ShowMyRssCommand { get; }
        CoreSDK.IDelegateCommand ShowSalesToolCommand { get; }
        CoreSDK.IDelegateCommand ShowSalesToolsIncentivesCommand { get; }
        CoreSDK.IDelegateCommand ShowSalesToolsProductsCommand { get; }
        CoreSDK.IDelegateCommand ShowSalesToolsPromotionCommand { get; }
        CoreSDK.IDelegateCommand ShowSalesToolsServiceCommand { get; }
        CoreSDK.IDelegateCommand ShowSelfTrainingCommand { get; }
        CoreSDK.IDelegateCommand ShowViewReportCommand { get; }
        CoreSDK.IDelegateCommand ShowMyLeadsCommand { get; }
        CoreSDK.IDelegateCommand ShowSettingsCommand { get; }
        CoreSDK.IDelegateCommand ShowHomeViewFromHeaderCommand { get; }
        CoreSDK.IDelegateCommand AddOrRemoveRssFeedCommand { get; }
        CoreSDK.IDelegateCommand SaveRssCommand { get; }
        CoreSDK.IDelegateCommand ShowManageRssCommand { get; }
        CoreSDK.IDelegateCommand ShowFeedDetailCommand { get; }
        CoreSDK.IDelegateCommand ShowFeedItemDetailCommand { get; }
        CoreSDK.IDelegateCommand AddRssFeedCommand { get; }
        CoreSDK.IDelegateCommand ShowAboutViewCommand { get; }
    }
}
