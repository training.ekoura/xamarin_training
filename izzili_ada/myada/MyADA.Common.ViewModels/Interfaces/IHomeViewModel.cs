﻿using MyADA.Common.Services.Models;
using System;
using System.Threading.Tasks;
namespace MyADA.Common.ViewModels
{
    public interface IHomeViewModel
    {
        string AudiNewsNotif { get; set; }
        string CurrentMBPage { get; set; }
        bool IsVisibleAudiNewsNotif { get; set; }
        bool ShowShowroomPicker { get; set; }
        bool IsBusy { get; set; }
        bool IsHomeView { get; set; }
        bool IsVisibleLeadNotif { get; set; }
        bool IsVisibleSalesToolNotif { get; set; }
        bool IsVisibleSelfTrainingNotif { get; set; }
        bool OverHundredLeadNotif { get; set; }
        bool OverHundredAudiNewsNotif { get; set; }
        bool OverHundredSalesToolNotif { get; set; }
        bool OverHundredSelfTrainingNotif { get; set; }
        bool ShowReportForManager { get; set; }
        bool ShowReportForSimpleUser { get; set; }
        string LeadNotif { get; set; }
        string SalesToolNotif { get; set; }
        string SelfTrainingNotif { get; set; }
        string CurrentPageHeaderTitle { get; set; }
        string ReportSource { get; set; }
        MyADA.Common.Services.Models.CurrentUserProfile CurrentUserProfileInfos { get; set; }
        System.Collections.ObjectModel.ObservableCollection<RichMessageItem> MBAllRichMessage { get; set; }
        System.Collections.ObjectModel.ObservableCollection<RichMessageItem> MBAllSalesToolsRichMessage { get; set; }
        System.Collections.ObjectModel.ObservableCollection<RichMessageItem> MBAllSelfTrainingRichMessage { get; set; }
        System.Collections.ObjectModel.ObservableCollection<ShowroomModels> ShowRoomCollections { get; set; }
        System.Collections.ObjectModel.ObservableCollection<UserInfos> UsersCollections { get; set; }

        CoreSDK.IDelegateCommand ShowAudiNewsFromHomeCommand { get; }
        CoreSDK.IDelegateCommand ShowRichMessageCommand { get; }
        CoreSDK.IDelegateCommand ShowSalesToolFromHomeCommand { get; }
        CoreSDK.IDelegateCommand ShowSelfTrainingFromHomeCommand { get; }
        CoreSDK.IDelegateCommand ShowSettingsCommand { get; }
        CoreSDK.IDelegateCommand ShowViewReportFromHomeCommand { get; }
        CoreSDK.IDelegateCommand ShowMyLeadsCommand { get; }
        CoreSDK.IDelegateCommand ShowOverAllCommand { get; }
        Task<bool> PrepareVM();
        Task RefreshLeadData();
    }
}
