﻿using MyADA.Common.Services.Models;
using System;
using System.Threading.Tasks;
namespace MyADA.Common.ViewModels
{
    public interface ILeadViewModel
    {
        MyADA.Common.Services.Models.CurrentUserProfile CurrentUserProfileInfos { get; set; }
        bool ShowNoLeadMessage { get; set; }
        bool IsAllocatedVisible { get; set; }
        bool IsDefaultLeadCollectionFilter { get; set; }
        bool IsBusy { get; set; }
        bool FilterDoUpdate { get; set; }
        string FilterStyleRoot { get; set; }
        string FilterStyleDetails { get; set; }
        System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> LeadsCollection { get; set; }
        System.Collections.ObjectModel.ObservableCollection<LeadsItemViewModel> OnUseLeadsCollection { get; set; }
        CoreSDK.IDelegateCommand SelectRepresentativeCommand { get; }
        string PlaceHolderForLeadsPicker { get; set; }
        string NavigateLeadHeaderTitle { get; set; }
        MyADA.Common.Services.Models.UserInfos SelectedUser { get; set; }
        CoreSDK.IDelegateCommand ShowAllocatedLeadCommand { get; }
        CoreSDK.IDelegateCommand ShowLeadDetailsCommand { get; }
        CoreSDK.IDelegateCommand DoFilterCommand { get; }
        CoreSDK.IDelegateCommand ShowMyLeadDetailsCommand { get; }
        CoreSDK.IDelegateCommand SetLeadStatusCommand { get; }
        CoreSDK.IDelegateCommand LiveChangeLeadStatusOnDoneCommand { get; }
        CoreSDK.IDelegateCommand LiveChangeLeadStatusOnRecontactCommand { get; }
        CoreSDK.IDelegateCommand LiveChangeLeadStatusOnNotInterestCommand { get; }
        CoreSDK.IDelegateCommand ShowMyLeadsCommand { get; }
        CoreSDK.IDelegateCommand MyOwnLeadsPopinVisibilityCommand { get; }
        CoreSDK.IDelegateCommand LoadMoreCommand { get; }
        CoreSDK.IDelegateCommand CloseFilterViewAndLoadDataCommand { get; }
        CoreSDK.IDelegateCommand LoadMoreLeadOfTheDateCommand { get; }
        CoreSDK.IDelegateCommand LoadMoreLeadOnGoingCommand { get; }
        System.Collections.ObjectModel.ObservableCollection<MyADA.Common.Services.Models.UserInfos> UsersCollections { get; set; }
        Task<bool> PrepareVM(CurrentUserProfile p_CurrentUserProfile);
        Task ShowMyLeadsCommandFunction();
        bool IsAvailableShowroom { get; set; }
    }
}
