﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML LeadsItemViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  CoreSDK.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;

namespace MyADA.Common.ViewModels
{
	public abstract partial class LeadsItemViewModelBase : CoreSDK.ViewModel
	{
		#region === Propriétés ===

			#region === Propriété : IsSeleted ===

				public const string IsSeleted_PROPERTYNAME = "IsSeleted";

				private bool _IsSeleted;
				///<summary>
				/// Propriété : IsSeleted
				///</summary>
				public bool IsSeleted
				{
					get
					{
						return GetValue<bool>(() => _IsSeleted);
					}
					set
					{
						SetValue<bool>(() => _IsSeleted, (v) => _IsSeleted = v, value, IsSeleted_PROPERTYNAME,  DoIsSeletedBeforeSet, DoIsSeletedAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsSeletedBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsSeletedAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsSeletedDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : StatusColor ===

				public const string StatusColor_PROPERTYNAME = "StatusColor";

				private string _StatusColor;
				///<summary>
				/// Propriété : StatusColor
				///</summary>
				public string StatusColor
				{
					get
					{
						return GetValue<string>(() => _StatusColor);
					}
					set
					{
						SetValue<string>(() => _StatusColor, (v) => _StatusColor = v, value, StatusColor_PROPERTYNAME,  DoStatusColorBeforeSet, DoStatusColorAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoStatusColorBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoStatusColorAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyStatusColorDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowSelectionIcone ===

				public const string ShowSelectionIcone_PROPERTYNAME = "ShowSelectionIcone";

				private bool _ShowSelectionIcone;
				///<summary>
				/// Propriété : ShowSelectionIcone
				///</summary>
				public bool ShowSelectionIcone
				{
					get
					{
						return GetValue<bool>(() => _ShowSelectionIcone);
					}
					set
					{
						SetValue<bool>(() => _ShowSelectionIcone, (v) => _ShowSelectionIcone = v, value, ShowSelectionIcone_PROPERTYNAME,  DoShowSelectionIconeBeforeSet, DoShowSelectionIconeAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowSelectionIconeBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowSelectionIconeAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowSelectionIconeDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowAffectationName ===

				public const string ShowAffectationName_PROPERTYNAME = "ShowAffectationName";

				private bool _ShowAffectationName;
				///<summary>
				/// Propriété : ShowAffectationName
				///</summary>
				public bool ShowAffectationName
				{
					get
					{
						return GetValue<bool>(() => _ShowAffectationName);
					}
					set
					{
						SetValue<bool>(() => _ShowAffectationName, (v) => _ShowAffectationName = v, value, ShowAffectationName_PROPERTYNAME,  DoShowAffectationNameBeforeSet, DoShowAffectationNameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowAffectationNameBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowAffectationNameAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowAffectationNameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsBookedLead ===

				public const string IsBookedLead_PROPERTYNAME = "IsBookedLead";

				private bool _IsBookedLead;
				///<summary>
				/// Propriété : IsBookedLead
				///</summary>
				public bool IsBookedLead
				{
					get
					{
						return GetValue<bool>(() => _IsBookedLead);
					}
					set
					{
						SetValue<bool>(() => _IsBookedLead, (v) => _IsBookedLead = v, value, IsBookedLead_PROPERTYNAME,  DoIsBookedLeadBeforeSet, DoIsBookedLeadAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsBookedLeadBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsBookedLeadAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsBookedLeadDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FormattedDate ===

				public const string FormattedDate_PROPERTYNAME = "FormattedDate";

				private string _FormattedDate;
				///<summary>
				/// Propriété : FormattedDate
				///</summary>
				public string FormattedDate
				{
					get
					{
						return GetValue<string>(() => _FormattedDate);
					}
					set
					{
						SetValue<string>(() => _FormattedDate, (v) => _FormattedDate = v, value, FormattedDate_PROPERTYNAME,  DoFormattedDateBeforeSet, DoFormattedDateAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFormattedDateBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFormattedDateAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFormattedDateDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FormattedHeure ===

				public const string FormattedHeure_PROPERTYNAME = "FormattedHeure";

				private string _FormattedHeure;
				///<summary>
				/// Propriété : FormattedHeure
				///</summary>
				public string FormattedHeure
				{
					get
					{
						return GetValue<string>(() => _FormattedHeure);
					}
					set
					{
						SetValue<string>(() => _FormattedHeure, (v) => _FormattedHeure = v, value, FormattedHeure_PROPERTYNAME,  DoFormattedHeureBeforeSet, DoFormattedHeureAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFormattedHeureBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFormattedHeureAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFormattedHeureDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsLineVisble ===

				public const string IsLineVisble_PROPERTYNAME = "IsLineVisble";

				private bool _IsLineVisble;
				///<summary>
				/// Propriété : IsLineVisble
				///</summary>
				public bool IsLineVisble
				{
					get
					{
						return GetValue<bool>(() => _IsLineVisble);
					}
					set
					{
						SetValue<bool>(() => _IsLineVisble, (v) => _IsLineVisble = v, value, IsLineVisble_PROPERTYNAME,  DoIsLineVisbleBeforeSet, DoIsLineVisbleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsLineVisbleBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsLineVisbleAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsLineVisbleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LeadId ===

				public const string LeadId_PROPERTYNAME = "LeadId";

				private System.String _LeadId;
				///<summary>
				/// Propriété : LeadId
				///</summary>
				public System.String LeadId
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_LeadId_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_LeadId_GetValue, LeadsEncapsulation_LeadId_SetValue, value, LeadId_PROPERTYNAME,  DoLeadIdBeforeSet, DoLeadIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLeadIdBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLeadIdAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLeadIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowroomId ===

				public const string ShowroomId_PROPERTYNAME = "ShowroomId";

				private System.String _ShowroomId;
				///<summary>
				/// Propriété : ShowroomId
				///</summary>
				public System.String ShowroomId
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_ShowroomId_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_ShowroomId_GetValue, LeadsEncapsulation_ShowroomId_SetValue, value, ShowroomId_PROPERTYNAME,  DoShowroomIdBeforeSet, DoShowroomIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowroomIdBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowroomIdAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowroomIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UserId ===

				public const string UserId_PROPERTYNAME = "UserId";

				private System.String _UserId;
				///<summary>
				/// Propriété : UserId
				///</summary>
				public System.String UserId
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_UserId_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_UserId_GetValue, LeadsEncapsulation_UserId_SetValue, value, UserId_PROPERTYNAME,  DoUserIdBeforeSet, DoUserIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUserIdBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUserIdAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUserIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Tag ===

				public const string Tag_PROPERTYNAME = "Tag";

				private System.String _Tag;
				///<summary>
				/// Propriété : Tag
				///</summary>
				public System.String Tag
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_Tag_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_Tag_GetValue, LeadsEncapsulation_Tag_SetValue, value, Tag_PROPERTYNAME,  DoTagBeforeSet, DoTagAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTagBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTagAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTagDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Status ===

				public const string Status_PROPERTYNAME = "Status";

				private System.String _Status;
				///<summary>
				/// Propriété : Status
				///</summary>
				public System.String Status
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_Status_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_Status_GetValue, LeadsEncapsulation_Status_SetValue, value, Status_PROPERTYNAME,  DoStatusBeforeSet, DoStatusAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoStatusBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoStatusAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyStatusDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Title ===

				public const string Title_PROPERTYNAME = "Title";

				private System.String _Title;
				///<summary>
				/// Propriété : Title
				///</summary>
				public System.String Title
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_Title_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_Title_GetValue, LeadsEncapsulation_Title_SetValue, value, Title_PROPERTYNAME,  DoTitleBeforeSet, DoTitleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTitleBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTitleAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTitleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FirstName ===

				public const string FirstName_PROPERTYNAME = "FirstName";

				private System.String _FirstName;
				///<summary>
				/// Propriété : FirstName
				///</summary>
				public System.String FirstName
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_FirstName_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_FirstName_GetValue, LeadsEncapsulation_FirstName_SetValue, value, FirstName_PROPERTYNAME,  DoFirstNameBeforeSet, DoFirstNameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFirstNameBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFirstNameAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFirstNameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LastName ===

				public const string LastName_PROPERTYNAME = "LastName";

				private System.String _LastName;
				///<summary>
				/// Propriété : LastName
				///</summary>
				public System.String LastName
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_LastName_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_LastName_GetValue, LeadsEncapsulation_LastName_SetValue, value, LastName_PROPERTYNAME,  DoLastNameBeforeSet, DoLastNameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLastNameBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLastNameAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLastNameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Email ===

				public const string Email_PROPERTYNAME = "Email";

				private System.String _Email;
				///<summary>
				/// Propriété : Email
				///</summary>
				public System.String Email
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_Email_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_Email_GetValue, LeadsEncapsulation_Email_SetValue, value, Email_PROPERTYNAME,  DoEmailBeforeSet, DoEmailAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoEmailBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoEmailAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyEmailDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : RecontactOn ===

				public const string RecontactOn_PROPERTYNAME = "RecontactOn";

				private System.String _RecontactOn;
				///<summary>
				/// Propriété : RecontactOn
				///</summary>
				public System.String RecontactOn
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_RecontactOn_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_RecontactOn_GetValue, LeadsEncapsulation_RecontactOn_SetValue, value, RecontactOn_PROPERTYNAME,  DoRecontactOnBeforeSet, DoRecontactOnAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoRecontactOnBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoRecontactOnAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyRecontactOnDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CreatedDate ===

				public const string CreatedDate_PROPERTYNAME = "CreatedDate";

				private System.DateTime _CreatedDate;
				///<summary>
				/// Propriété : CreatedDate
				///</summary>
				public System.DateTime CreatedDate
				{
					get
					{
						return GetValue<System.DateTime>(LeadsEncapsulation_CreatedDate_GetValue);
					}
					set
					{
						SetValue<System.DateTime>(LeadsEncapsulation_CreatedDate_GetValue, LeadsEncapsulation_CreatedDate_SetValue, value, CreatedDate_PROPERTYNAME,  DoCreatedDateBeforeSet, DoCreatedDateAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCreatedDateBeforeSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCreatedDateAfterSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCreatedDateDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LastModified ===

				public const string LastModified_PROPERTYNAME = "LastModified";

				private System.DateTime _LastModified;
				///<summary>
				/// Propriété : LastModified
				///</summary>
				public System.DateTime LastModified
				{
					get
					{
						return GetValue<System.DateTime>(LeadsEncapsulation_LastModified_GetValue);
					}
					set
					{
						SetValue<System.DateTime>(LeadsEncapsulation_LastModified_GetValue, LeadsEncapsulation_LastModified_SetValue, value, LastModified_PROPERTYNAME,  DoLastModifiedBeforeSet, DoLastModifiedAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLastModifiedBeforeSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLastModifiedAfterSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLastModifiedDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Showroom ===

				public const string Showroom_PROPERTYNAME = "Showroom";

				private MyADA.Common.Services.Models.Showroom _Showroom;
				///<summary>
				/// Propriété : Showroom
				///</summary>
				public MyADA.Common.Services.Models.Showroom Showroom
				{
					get
					{
						return GetValue<MyADA.Common.Services.Models.Showroom>(LeadsEncapsulation_Showroom_GetValue);
					}
					set
					{
						SetValue<MyADA.Common.Services.Models.Showroom>(LeadsEncapsulation_Showroom_GetValue, LeadsEncapsulation_Showroom_SetValue, value, Showroom_PROPERTYNAME,  DoShowroomBeforeSet, DoShowroomAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowroomBeforeSet(string p_PropertyName, MyADA.Common.Services.Models.Showroom p_OldValue, MyADA.Common.Services.Models.Showroom p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowroomAfterSet(string p_PropertyName, MyADA.Common.Services.Models.Showroom p_OldValue, MyADA.Common.Services.Models.Showroom p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowroomDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : User ===

				public const string User_PROPERTYNAME = "User";

				private MyADA.Common.Services.Models.User _User;
				///<summary>
				/// Propriété : User
				///</summary>
				public MyADA.Common.Services.Models.User User
				{
					get
					{
						return GetValue<MyADA.Common.Services.Models.User>(LeadsEncapsulation_User_GetValue);
					}
					set
					{
						SetValue<MyADA.Common.Services.Models.User>(LeadsEncapsulation_User_GetValue, LeadsEncapsulation_User_SetValue, value, User_PROPERTYNAME,  DoUserBeforeSet, DoUserAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUserBeforeSet(string p_PropertyName, MyADA.Common.Services.Models.User p_OldValue, MyADA.Common.Services.Models.User p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUserAfterSet(string p_PropertyName, MyADA.Common.Services.Models.User p_OldValue, MyADA.Common.Services.Models.User p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUserDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux encapsulations ===


			#region === Encapsulation : LeadsEncapsulation ===

				// Champ pour l'encapsulation.
				// Les propriétés implémentées font directement référence à ce champ.
				private MyADA.Common.Services.Models.Lead _LeadsEncapsulation;

				///<summary>
				/// Encapsulation : LeadsEncapsulation.
				///</summary>
				public MyADA.Common.Services.Models.Lead GetLeadsEncapsulation()
				{
					return _LeadsEncapsulation;
				}

				///<summary>
				/// Encapsulation : LeadsEncapsulation.
				///</summary>
				public LeadsItemViewModel SetLeadsEncapsulation(MyADA.Common.Services.Models.Lead p_Value)
				{
					// En vue d'éventuelles notification (si initialisé), nous récupérons les anciennes valeurs (valeurs actuelles).
					var l_OldValueLeadId = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : LeadId;
					var l_OldValueShowroomId = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : ShowroomId;
					var l_OldValueUserId = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : UserId;
					var l_OldValueTag = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : Tag;
					var l_OldValueStatus = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : Status;
					var l_OldValueTitle = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : Title;
					var l_OldValueFirstName = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : FirstName;
					var l_OldValueLastName = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : LastName;
					var l_OldValueEmail = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : Email;
					var l_OldValueRecontactOn = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : RecontactOn;
					var l_OldValueCreatedDate = _LeadsEncapsulation == null || !IsInitialized ? default(System.DateTime) : CreatedDate;
					var l_OldValueLastModified = _LeadsEncapsulation == null || !IsInitialized ? default(System.DateTime) : LastModified;
					var l_OldValueShowroom = _LeadsEncapsulation == null || !IsInitialized ? default(MyADA.Common.Services.Models.Showroom) : Showroom;
					var l_OldValueUser = _LeadsEncapsulation == null || !IsInitialized ? default(MyADA.Common.Services.Models.User) : User;

					// Affectation du nouvel object d'encapsulation.
					_LeadsEncapsulation = p_Value;

					// Si pas initialisé, inutile d'aller plus loin.
					if(!IsInitialized) return this as LeadsItemViewModel;

					// Nous allons notifier, nous avons besoin des valeurs actuelles.
					var l_NewValueLeadId = _LeadsEncapsulation == null ? default(System.String) : LeadId;
					var l_NewValueShowroomId = _LeadsEncapsulation == null ? default(System.String) : ShowroomId;
					var l_NewValueUserId = _LeadsEncapsulation == null ? default(System.String) : UserId;
					var l_NewValueTag = _LeadsEncapsulation == null ? default(System.String) : Tag;
					var l_NewValueStatus = _LeadsEncapsulation == null ? default(System.String) : Status;
					var l_NewValueTitle = _LeadsEncapsulation == null ? default(System.String) : Title;
					var l_NewValueFirstName = _LeadsEncapsulation == null ? default(System.String) : FirstName;
					var l_NewValueLastName = _LeadsEncapsulation == null ? default(System.String) : LastName;
					var l_NewValueEmail = _LeadsEncapsulation == null ? default(System.String) : Email;
					var l_NewValueRecontactOn = _LeadsEncapsulation == null ? default(System.String) : RecontactOn;
					var l_NewValueCreatedDate = _LeadsEncapsulation == null ? default(System.DateTime) : CreatedDate;
					var l_NewValueLastModified = _LeadsEncapsulation == null ? default(System.DateTime) : LastModified;
					var l_NewValueShowroom = _LeadsEncapsulation == null ? default(MyADA.Common.Services.Models.Showroom) : Showroom;
					var l_NewValueUser = _LeadsEncapsulation == null ? default(MyADA.Common.Services.Models.User) : User;

					// Nous notifions.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LeadId_PROPERTYNAME, l_OldValueLeadId, l_NewValueLeadId));
					NotifyLeadIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ShowroomId_PROPERTYNAME, l_OldValueShowroomId, l_NewValueShowroomId));
					NotifyShowroomIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(UserId_PROPERTYNAME, l_OldValueUserId, l_NewValueUserId));
					NotifyUserIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Tag_PROPERTYNAME, l_OldValueTag, l_NewValueTag));
					NotifyTagDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Status_PROPERTYNAME, l_OldValueStatus, l_NewValueStatus));
					NotifyStatusDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Title_PROPERTYNAME, l_OldValueTitle, l_NewValueTitle));
					NotifyTitleDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(FirstName_PROPERTYNAME, l_OldValueFirstName, l_NewValueFirstName));
					NotifyFirstNameDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LastName_PROPERTYNAME, l_OldValueLastName, l_NewValueLastName));
					NotifyLastNameDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Email_PROPERTYNAME, l_OldValueEmail, l_NewValueEmail));
					NotifyEmailDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(RecontactOn_PROPERTYNAME, l_OldValueRecontactOn, l_NewValueRecontactOn));
					NotifyRecontactOnDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(CreatedDate_PROPERTYNAME, l_OldValueCreatedDate, l_NewValueCreatedDate));
					NotifyCreatedDateDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LastModified_PROPERTYNAME, l_OldValueLastModified, l_NewValueLastModified));
					NotifyLastModifiedDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Showroom_PROPERTYNAME, l_OldValueShowroom, l_NewValueShowroom));
					NotifyShowroomDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(User_PROPERTYNAME, l_OldValueUser, l_NewValueUser));
					NotifyUserDependencies();

					return this as LeadsItemViewModel;
				}

				#region === Propriété : LeadId ===

					///<summary>
					/// Méthode utilisée par la propriété LeadId pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_LeadId_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", LeadId_PROPERTYNAME));
						return _LeadsEncapsulation.LeadId;
					}

					///<summary>
					/// Méthode utilisée par la propriété LeadId pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_LeadId_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", LeadId_PROPERTYNAME));
						_LeadsEncapsulation.LeadId = p_Value;
					}


				#endregion

				#region === Propriété : ShowroomId ===

					///<summary>
					/// Méthode utilisée par la propriété ShowroomId pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_ShowroomId_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", ShowroomId_PROPERTYNAME));
						return _LeadsEncapsulation.ShowroomId;
					}

					///<summary>
					/// Méthode utilisée par la propriété ShowroomId pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_ShowroomId_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", ShowroomId_PROPERTYNAME));
						_LeadsEncapsulation.ShowroomId = p_Value;
					}


				#endregion

				#region === Propriété : UserId ===

					///<summary>
					/// Méthode utilisée par la propriété UserId pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_UserId_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", UserId_PROPERTYNAME));
						return _LeadsEncapsulation.UserId;
					}

					///<summary>
					/// Méthode utilisée par la propriété UserId pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_UserId_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", UserId_PROPERTYNAME));
						_LeadsEncapsulation.UserId = p_Value;
					}


				#endregion

				#region === Propriété : Tag ===

					///<summary>
					/// Méthode utilisée par la propriété Tag pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_Tag_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", Tag_PROPERTYNAME));
						return _LeadsEncapsulation.Tag;
					}

					///<summary>
					/// Méthode utilisée par la propriété Tag pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_Tag_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", Tag_PROPERTYNAME));
						_LeadsEncapsulation.Tag = p_Value;
					}


				#endregion

				#region === Propriété : Status ===

					///<summary>
					/// Méthode utilisée par la propriété Status pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_Status_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", Status_PROPERTYNAME));
						return _LeadsEncapsulation.Status;
					}

					///<summary>
					/// Méthode utilisée par la propriété Status pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_Status_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", Status_PROPERTYNAME));
						_LeadsEncapsulation.Status = p_Value;
					}


				#endregion

				#region === Propriété : Title ===

					///<summary>
					/// Méthode utilisée par la propriété Title pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_Title_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", Title_PROPERTYNAME));
						return _LeadsEncapsulation.Title;
					}

					///<summary>
					/// Méthode utilisée par la propriété Title pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_Title_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", Title_PROPERTYNAME));
						_LeadsEncapsulation.Title = p_Value;
					}


				#endregion

				#region === Propriété : FirstName ===

					///<summary>
					/// Méthode utilisée par la propriété FirstName pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_FirstName_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", FirstName_PROPERTYNAME));
						return _LeadsEncapsulation.FirstName;
					}

					///<summary>
					/// Méthode utilisée par la propriété FirstName pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_FirstName_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", FirstName_PROPERTYNAME));
						_LeadsEncapsulation.FirstName = p_Value;
					}


				#endregion

				#region === Propriété : LastName ===

					///<summary>
					/// Méthode utilisée par la propriété LastName pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_LastName_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", LastName_PROPERTYNAME));
						return _LeadsEncapsulation.LastName;
					}

					///<summary>
					/// Méthode utilisée par la propriété LastName pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_LastName_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", LastName_PROPERTYNAME));
						_LeadsEncapsulation.LastName = p_Value;
					}


				#endregion

				#region === Propriété : Email ===

					///<summary>
					/// Méthode utilisée par la propriété Email pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_Email_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", Email_PROPERTYNAME));
						return _LeadsEncapsulation.Email;
					}

					///<summary>
					/// Méthode utilisée par la propriété Email pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_Email_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", Email_PROPERTYNAME));
						_LeadsEncapsulation.Email = p_Value;
					}


				#endregion

				#region === Propriété : RecontactOn ===

					///<summary>
					/// Méthode utilisée par la propriété RecontactOn pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_RecontactOn_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", RecontactOn_PROPERTYNAME));
						return _LeadsEncapsulation.RecontactOn;
					}

					///<summary>
					/// Méthode utilisée par la propriété RecontactOn pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_RecontactOn_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", RecontactOn_PROPERTYNAME));
						_LeadsEncapsulation.RecontactOn = p_Value;
					}


				#endregion

				#region === Propriété : CreatedDate ===

					///<summary>
					/// Méthode utilisée par la propriété CreatedDate pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.DateTime LeadsEncapsulation_CreatedDate_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", CreatedDate_PROPERTYNAME));
						return _LeadsEncapsulation.CreatedDate;
					}

					///<summary>
					/// Méthode utilisée par la propriété CreatedDate pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_CreatedDate_SetValue(System.DateTime p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", CreatedDate_PROPERTYNAME));
						_LeadsEncapsulation.CreatedDate = p_Value;
					}


				#endregion

				#region === Propriété : LastModified ===

					///<summary>
					/// Méthode utilisée par la propriété LastModified pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.DateTime LeadsEncapsulation_LastModified_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", LastModified_PROPERTYNAME));
						return _LeadsEncapsulation.LastModified;
					}

					///<summary>
					/// Méthode utilisée par la propriété LastModified pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_LastModified_SetValue(System.DateTime p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", LastModified_PROPERTYNAME));
						_LeadsEncapsulation.LastModified = p_Value;
					}


				#endregion

				#region === Propriété : Showroom ===

					///<summary>
					/// Méthode utilisée par la propriété Showroom pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private MyADA.Common.Services.Models.Showroom LeadsEncapsulation_Showroom_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", Showroom_PROPERTYNAME));
						return _LeadsEncapsulation.Showroom;
					}

					///<summary>
					/// Méthode utilisée par la propriété Showroom pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_Showroom_SetValue(MyADA.Common.Services.Models.Showroom p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", Showroom_PROPERTYNAME));
						_LeadsEncapsulation.Showroom = p_Value;
					}


				#endregion

				#region === Propriété : User ===

					///<summary>
					/// Méthode utilisée par la propriété User pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private MyADA.Common.Services.Models.User LeadsEncapsulation_User_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", User_PROPERTYNAME));
						return _LeadsEncapsulation.User;
					}

					///<summary>
					/// Méthode utilisée par la propriété User pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_User_SetValue(MyADA.Common.Services.Models.User p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", User_PROPERTYNAME));
						_LeadsEncapsulation.User = p_Value;
					}


				#endregion


			#endregion

		#endregion
	}

	public partial class LeadsItemViewModel : LeadsItemViewModelBase
	{
	}
}
