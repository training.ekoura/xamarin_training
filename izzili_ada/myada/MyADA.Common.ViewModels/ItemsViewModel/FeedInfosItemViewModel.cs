﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML FeedInfosItemViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  CoreSDK.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;

namespace MyADA.Common.ViewModels
{
	public abstract partial class FeedInfosItemViewModelBase : CoreSDK.ViewModel
	{
		#region === Propriétés ===

			#region === Propriété : FeedId ===

				public const string FeedId_PROPERTYNAME = "FeedId";

				private System.String _FeedId;
				///<summary>
				/// Propriété : FeedId
				///</summary>
				public System.String FeedId
				{
					get
					{
						return GetValue<System.String>(FeedInfosEncapsulation_FeedId_GetValue);
					}
					set
					{
						SetValue<System.String>(FeedInfosEncapsulation_FeedId_GetValue, FeedInfosEncapsulation_FeedId_SetValue, value, FeedId_PROPERTYNAME,  DoFeedIdBeforeSet, DoFeedIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFeedIdBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFeedIdAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFeedIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Title ===

				public const string Title_PROPERTYNAME = "Title";

				private System.String _Title;
				///<summary>
				/// Propriété : Title
				///</summary>
				public System.String Title
				{
					get
					{
						return GetValue<System.String>(FeedInfosEncapsulation_Title_GetValue);
					}
					set
					{
						SetValue<System.String>(FeedInfosEncapsulation_Title_GetValue, FeedInfosEncapsulation_Title_SetValue, value, Title_PROPERTYNAME,  DoTitleBeforeSet, DoTitleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTitleBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTitleAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTitleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Url ===

				public const string Url_PROPERTYNAME = "Url";

				private System.String _Url;
				///<summary>
				/// Propriété : Url
				///</summary>
				public System.String Url
				{
					get
					{
						return GetValue<System.String>(FeedInfosEncapsulation_Url_GetValue);
					}
					set
					{
						SetValue<System.String>(FeedInfosEncapsulation_Url_GetValue, FeedInfosEncapsulation_Url_SetValue, value, Url_PROPERTYNAME,  DoUrlBeforeSet, DoUrlAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUrlBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUrlAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUrlDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CreatedDate ===

				public const string CreatedDate_PROPERTYNAME = "CreatedDate";

				private System.DateTime _CreatedDate;
				///<summary>
				/// Propriété : CreatedDate
				///</summary>
				public System.DateTime CreatedDate
				{
					get
					{
						return GetValue<System.DateTime>(FeedInfosEncapsulation_CreatedDate_GetValue);
					}
					set
					{
						SetValue<System.DateTime>(FeedInfosEncapsulation_CreatedDate_GetValue, FeedInfosEncapsulation_CreatedDate_SetValue, value, CreatedDate_PROPERTYNAME,  DoCreatedDateBeforeSet, DoCreatedDateAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCreatedDateBeforeSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCreatedDateAfterSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCreatedDateDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LastModified ===

				public const string LastModified_PROPERTYNAME = "LastModified";

				private System.DateTime _LastModified;
				///<summary>
				/// Propriété : LastModified
				///</summary>
				public System.DateTime LastModified
				{
					get
					{
						return GetValue<System.DateTime>(FeedInfosEncapsulation_LastModified_GetValue);
					}
					set
					{
						SetValue<System.DateTime>(FeedInfosEncapsulation_LastModified_GetValue, FeedInfosEncapsulation_LastModified_SetValue, value, LastModified_PROPERTYNAME,  DoLastModifiedBeforeSet, DoLastModifiedAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLastModifiedBeforeSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLastModifiedAfterSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLastModifiedDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux encapsulations ===


			#region === Encapsulation : FeedInfosEncapsulation ===

				// Champ pour l'encapsulation.
				// Les propriétés implémentées font directement référence à ce champ.
				private MyADA.Common.Services.Models.FeedInfos _FeedInfosEncapsulation;

				///<summary>
				/// Encapsulation : FeedInfosEncapsulation.
				///</summary>
				public MyADA.Common.Services.Models.FeedInfos GetFeedInfosEncapsulation()
				{
					return _FeedInfosEncapsulation;
				}

				///<summary>
				/// Encapsulation : FeedInfosEncapsulation.
				///</summary>
				public FeedInfosItemViewModel SetFeedInfosEncapsulation(MyADA.Common.Services.Models.FeedInfos p_Value)
				{
					// En vue d'éventuelles notification (si initialisé), nous récupérons les anciennes valeurs (valeurs actuelles).
					var l_OldValueFeedId = _FeedInfosEncapsulation == null || !IsInitialized ? default(System.String) : FeedId;
					var l_OldValueTitle = _FeedInfosEncapsulation == null || !IsInitialized ? default(System.String) : Title;
					var l_OldValueUrl = _FeedInfosEncapsulation == null || !IsInitialized ? default(System.String) : Url;
					var l_OldValueCreatedDate = _FeedInfosEncapsulation == null || !IsInitialized ? default(System.DateTime) : CreatedDate;
					var l_OldValueLastModified = _FeedInfosEncapsulation == null || !IsInitialized ? default(System.DateTime) : LastModified;

					// Affectation du nouvel object d'encapsulation.
					_FeedInfosEncapsulation = p_Value;

					// Si pas initialisé, inutile d'aller plus loin.
					if(!IsInitialized) return this as FeedInfosItemViewModel;

					// Nous allons notifier, nous avons besoin des valeurs actuelles.
					var l_NewValueFeedId = _FeedInfosEncapsulation == null ? default(System.String) : FeedId;
					var l_NewValueTitle = _FeedInfosEncapsulation == null ? default(System.String) : Title;
					var l_NewValueUrl = _FeedInfosEncapsulation == null ? default(System.String) : Url;
					var l_NewValueCreatedDate = _FeedInfosEncapsulation == null ? default(System.DateTime) : CreatedDate;
					var l_NewValueLastModified = _FeedInfosEncapsulation == null ? default(System.DateTime) : LastModified;

					// Nous notifions.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(FeedId_PROPERTYNAME, l_OldValueFeedId, l_NewValueFeedId));
					NotifyFeedIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Title_PROPERTYNAME, l_OldValueTitle, l_NewValueTitle));
					NotifyTitleDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Url_PROPERTYNAME, l_OldValueUrl, l_NewValueUrl));
					NotifyUrlDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(CreatedDate_PROPERTYNAME, l_OldValueCreatedDate, l_NewValueCreatedDate));
					NotifyCreatedDateDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LastModified_PROPERTYNAME, l_OldValueLastModified, l_NewValueLastModified));
					NotifyLastModifiedDependencies();

					return this as FeedInfosItemViewModel;
				}

				#region === Propriété : FeedId ===

					///<summary>
					/// Méthode utilisée par la propriété FeedId pour la lecture de la valeur corresponsande de FeedInfosEncapsulation.
					///</summary>
					private System.String FeedInfosEncapsulation_FeedId_GetValue()
					{
						if(_FeedInfosEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur FeedInfosEncapsulation.", FeedId_PROPERTYNAME));
						return _FeedInfosEncapsulation.FeedId;
					}

					///<summary>
					/// Méthode utilisée par la propriété FeedId pour l'affectation de la valeur corresponsande de FeedInfosEncapsulation.
					///</summary>
					private void FeedInfosEncapsulation_FeedId_SetValue(System.String p_Value)
					{
						if(_FeedInfosEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur FeedInfosEncapsulation.", FeedId_PROPERTYNAME));
						_FeedInfosEncapsulation.FeedId = p_Value;
					}


				#endregion

				#region === Propriété : Title ===

					///<summary>
					/// Méthode utilisée par la propriété Title pour la lecture de la valeur corresponsande de FeedInfosEncapsulation.
					///</summary>
					private System.String FeedInfosEncapsulation_Title_GetValue()
					{
						if(_FeedInfosEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur FeedInfosEncapsulation.", Title_PROPERTYNAME));
						return _FeedInfosEncapsulation.Title;
					}

					///<summary>
					/// Méthode utilisée par la propriété Title pour l'affectation de la valeur corresponsande de FeedInfosEncapsulation.
					///</summary>
					private void FeedInfosEncapsulation_Title_SetValue(System.String p_Value)
					{
						if(_FeedInfosEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur FeedInfosEncapsulation.", Title_PROPERTYNAME));
						_FeedInfosEncapsulation.Title = p_Value;
					}


				#endregion

				#region === Propriété : Url ===

					///<summary>
					/// Méthode utilisée par la propriété Url pour la lecture de la valeur corresponsande de FeedInfosEncapsulation.
					///</summary>
					private System.String FeedInfosEncapsulation_Url_GetValue()
					{
						if(_FeedInfosEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur FeedInfosEncapsulation.", Url_PROPERTYNAME));
						return _FeedInfosEncapsulation.Url;
					}

					///<summary>
					/// Méthode utilisée par la propriété Url pour l'affectation de la valeur corresponsande de FeedInfosEncapsulation.
					///</summary>
					private void FeedInfosEncapsulation_Url_SetValue(System.String p_Value)
					{
						if(_FeedInfosEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur FeedInfosEncapsulation.", Url_PROPERTYNAME));
						_FeedInfosEncapsulation.Url = p_Value;
					}


				#endregion

				#region === Propriété : CreatedDate ===

					///<summary>
					/// Méthode utilisée par la propriété CreatedDate pour la lecture de la valeur corresponsande de FeedInfosEncapsulation.
					///</summary>
					private System.DateTime FeedInfosEncapsulation_CreatedDate_GetValue()
					{
						if(_FeedInfosEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur FeedInfosEncapsulation.", CreatedDate_PROPERTYNAME));
						return _FeedInfosEncapsulation.CreatedDate;
					}

					///<summary>
					/// Méthode utilisée par la propriété CreatedDate pour l'affectation de la valeur corresponsande de FeedInfosEncapsulation.
					///</summary>
					private void FeedInfosEncapsulation_CreatedDate_SetValue(System.DateTime p_Value)
					{
						if(_FeedInfosEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur FeedInfosEncapsulation.", CreatedDate_PROPERTYNAME));
						_FeedInfosEncapsulation.CreatedDate = p_Value;
					}


				#endregion

				#region === Propriété : LastModified ===

					///<summary>
					/// Méthode utilisée par la propriété LastModified pour la lecture de la valeur corresponsande de FeedInfosEncapsulation.
					///</summary>
					private System.DateTime FeedInfosEncapsulation_LastModified_GetValue()
					{
						if(_FeedInfosEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur FeedInfosEncapsulation.", LastModified_PROPERTYNAME));
						return _FeedInfosEncapsulation.LastModified;
					}

					///<summary>
					/// Méthode utilisée par la propriété LastModified pour l'affectation de la valeur corresponsande de FeedInfosEncapsulation.
					///</summary>
					private void FeedInfosEncapsulation_LastModified_SetValue(System.DateTime p_Value)
					{
						if(_FeedInfosEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur FeedInfosEncapsulation.", LastModified_PROPERTYNAME));
						_FeedInfosEncapsulation.LastModified = p_Value;
					}


				#endregion


			#endregion

		#endregion
	}

	public partial class FeedInfosItemViewModel : FeedInfosItemViewModelBase
	{
	}
}
