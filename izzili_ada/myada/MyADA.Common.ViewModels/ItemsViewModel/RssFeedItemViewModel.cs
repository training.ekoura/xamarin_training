﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML RssFeedItemViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  CoreSDK.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;

namespace MyADA.Common.ViewModels
{
	public abstract partial class RssFeedItemViewModelBase : CoreSDK.ViewModel
	{
		#region === Propriétés ===

			#region === Propriété : IsSelected ===

				public const string IsSelected_PROPERTYNAME = "IsSelected";

				private bool _IsSelected;
				///<summary>
				/// Propriété : IsSelected
				///</summary>
				public bool IsSelected
				{
					get
					{
						return GetValue<bool>(() => _IsSelected);
					}
					set
					{
						SetValue<bool>(() => _IsSelected, (v) => _IsSelected = v, value, IsSelected_PROPERTYNAME,  DoIsSelectedBeforeSet, DoIsSelectedAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsSelectedBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsSelectedAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsSelectedDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LineColor ===

				public const string LineColor_PROPERTYNAME = "LineColor";

				private string _LineColor;
				///<summary>
				/// Propriété : LineColor
				///</summary>
				public string LineColor
				{
					get
					{
						return GetValue<string>(() => _LineColor);
					}
					set
					{
						SetValue<string>(() => _LineColor, (v) => _LineColor = v, value, LineColor_PROPERTYNAME,  DoLineColorBeforeSet, DoLineColorAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLineColorBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLineColorAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLineColorDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : NbPost ===

				public const string NbPost_PROPERTYNAME = "NbPost";

				private string _NbPost;
				///<summary>
				/// Propriété : NbPost
				///</summary>
				public string NbPost
				{
					get
					{
						return GetValue<string>(() => _NbPost);
					}
					set
					{
						SetValue<string>(() => _NbPost, (v) => _NbPost = v, value, NbPost_PROPERTYNAME,  DoNbPostBeforeSet, DoNbPostAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoNbPostBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoNbPostAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyNbPostDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FeedId ===

				public const string FeedId_PROPERTYNAME = "FeedId";

				private System.String _FeedId;
				///<summary>
				/// Propriété : FeedId
				///</summary>
				public System.String FeedId
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_FeedId_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_FeedId_GetValue, LeadsEncapsulation_FeedId_SetValue, value, FeedId_PROPERTYNAME,  DoFeedIdBeforeSet, DoFeedIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFeedIdBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFeedIdAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFeedIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Title ===

				public const string Title_PROPERTYNAME = "Title";

				private System.String _Title;
				///<summary>
				/// Propriété : Title
				///</summary>
				public System.String Title
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_Title_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_Title_GetValue, LeadsEncapsulation_Title_SetValue, value, Title_PROPERTYNAME,  DoTitleBeforeSet, DoTitleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTitleBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTitleAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTitleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Url ===

				public const string Url_PROPERTYNAME = "Url";

				private System.String _Url;
				///<summary>
				/// Propriété : Url
				///</summary>
				public System.String Url
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_Url_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_Url_GetValue, LeadsEncapsulation_Url_SetValue, value, Url_PROPERTYNAME,  DoUrlBeforeSet, DoUrlAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUrlBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUrlAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUrlDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CreatedDate ===

				public const string CreatedDate_PROPERTYNAME = "CreatedDate";

				private System.DateTime _CreatedDate;
				///<summary>
				/// Propriété : CreatedDate
				///</summary>
				public System.DateTime CreatedDate
				{
					get
					{
						return GetValue<System.DateTime>(LeadsEncapsulation_CreatedDate_GetValue);
					}
					set
					{
						SetValue<System.DateTime>(LeadsEncapsulation_CreatedDate_GetValue, LeadsEncapsulation_CreatedDate_SetValue, value, CreatedDate_PROPERTYNAME,  DoCreatedDateBeforeSet, DoCreatedDateAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCreatedDateBeforeSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCreatedDateAfterSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCreatedDateDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LastModified ===

				public const string LastModified_PROPERTYNAME = "LastModified";

				private System.DateTime _LastModified;
				///<summary>
				/// Propriété : LastModified
				///</summary>
				public System.DateTime LastModified
				{
					get
					{
						return GetValue<System.DateTime>(LeadsEncapsulation_LastModified_GetValue);
					}
					set
					{
						SetValue<System.DateTime>(LeadsEncapsulation_LastModified_GetValue, LeadsEncapsulation_LastModified_SetValue, value, LastModified_PROPERTYNAME,  DoLastModifiedBeforeSet, DoLastModifiedAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLastModifiedBeforeSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLastModifiedAfterSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLastModifiedDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux encapsulations ===


			#region === Encapsulation : LeadsEncapsulation ===

				// Champ pour l'encapsulation.
				// Les propriétés implémentées font directement référence à ce champ.
				private MyADA.Common.Services.Models.RssFeed _LeadsEncapsulation;

				///<summary>
				/// Encapsulation : LeadsEncapsulation.
				///</summary>
				public MyADA.Common.Services.Models.RssFeed GetLeadsEncapsulation()
				{
					return _LeadsEncapsulation;
				}

				///<summary>
				/// Encapsulation : LeadsEncapsulation.
				///</summary>
				public RssFeedItemViewModel SetLeadsEncapsulation(MyADA.Common.Services.Models.RssFeed p_Value)
				{
					// En vue d'éventuelles notification (si initialisé), nous récupérons les anciennes valeurs (valeurs actuelles).
					var l_OldValueFeedId = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : FeedId;
					var l_OldValueTitle = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : Title;
					var l_OldValueUrl = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : Url;
					var l_OldValueCreatedDate = _LeadsEncapsulation == null || !IsInitialized ? default(System.DateTime) : CreatedDate;
					var l_OldValueLastModified = _LeadsEncapsulation == null || !IsInitialized ? default(System.DateTime) : LastModified;

					// Affectation du nouvel object d'encapsulation.
					_LeadsEncapsulation = p_Value;

					// Si pas initialisé, inutile d'aller plus loin.
					if(!IsInitialized) return this as RssFeedItemViewModel;

					// Nous allons notifier, nous avons besoin des valeurs actuelles.
					var l_NewValueFeedId = _LeadsEncapsulation == null ? default(System.String) : FeedId;
					var l_NewValueTitle = _LeadsEncapsulation == null ? default(System.String) : Title;
					var l_NewValueUrl = _LeadsEncapsulation == null ? default(System.String) : Url;
					var l_NewValueCreatedDate = _LeadsEncapsulation == null ? default(System.DateTime) : CreatedDate;
					var l_NewValueLastModified = _LeadsEncapsulation == null ? default(System.DateTime) : LastModified;

					// Nous notifions.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(FeedId_PROPERTYNAME, l_OldValueFeedId, l_NewValueFeedId));
					NotifyFeedIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Title_PROPERTYNAME, l_OldValueTitle, l_NewValueTitle));
					NotifyTitleDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Url_PROPERTYNAME, l_OldValueUrl, l_NewValueUrl));
					NotifyUrlDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(CreatedDate_PROPERTYNAME, l_OldValueCreatedDate, l_NewValueCreatedDate));
					NotifyCreatedDateDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LastModified_PROPERTYNAME, l_OldValueLastModified, l_NewValueLastModified));
					NotifyLastModifiedDependencies();

					return this as RssFeedItemViewModel;
				}

				#region === Propriété : FeedId ===

					///<summary>
					/// Méthode utilisée par la propriété FeedId pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_FeedId_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", FeedId_PROPERTYNAME));
						return _LeadsEncapsulation.FeedId;
					}

					///<summary>
					/// Méthode utilisée par la propriété FeedId pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_FeedId_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", FeedId_PROPERTYNAME));
						_LeadsEncapsulation.FeedId = p_Value;
					}


				#endregion

				#region === Propriété : Title ===

					///<summary>
					/// Méthode utilisée par la propriété Title pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_Title_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", Title_PROPERTYNAME));
						return _LeadsEncapsulation.Title;
					}

					///<summary>
					/// Méthode utilisée par la propriété Title pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_Title_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", Title_PROPERTYNAME));
						_LeadsEncapsulation.Title = p_Value;
					}


				#endregion

				#region === Propriété : Url ===

					///<summary>
					/// Méthode utilisée par la propriété Url pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_Url_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", Url_PROPERTYNAME));
						return _LeadsEncapsulation.Url;
					}

					///<summary>
					/// Méthode utilisée par la propriété Url pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_Url_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", Url_PROPERTYNAME));
						_LeadsEncapsulation.Url = p_Value;
					}


				#endregion

				#region === Propriété : CreatedDate ===

					///<summary>
					/// Méthode utilisée par la propriété CreatedDate pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.DateTime LeadsEncapsulation_CreatedDate_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", CreatedDate_PROPERTYNAME));
						return _LeadsEncapsulation.CreatedDate;
					}

					///<summary>
					/// Méthode utilisée par la propriété CreatedDate pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_CreatedDate_SetValue(System.DateTime p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", CreatedDate_PROPERTYNAME));
						_LeadsEncapsulation.CreatedDate = p_Value;
					}


				#endregion

				#region === Propriété : LastModified ===

					///<summary>
					/// Méthode utilisée par la propriété LastModified pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.DateTime LeadsEncapsulation_LastModified_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", LastModified_PROPERTYNAME));
						return _LeadsEncapsulation.LastModified;
					}

					///<summary>
					/// Méthode utilisée par la propriété LastModified pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_LastModified_SetValue(System.DateTime p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", LastModified_PROPERTYNAME));
						_LeadsEncapsulation.LastModified = p_Value;
					}


				#endregion


			#endregion

		#endregion
	}

	public partial class RssFeedItemViewModel : RssFeedItemViewModelBase
	{
	}
}
