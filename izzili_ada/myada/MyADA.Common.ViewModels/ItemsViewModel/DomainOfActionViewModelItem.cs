﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML DomainOfActionViewModelItem
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  CoreSDK.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;

namespace MyADA.Common.ViewModels
{
	public abstract partial class DomainOfActionViewModelItemBase : CoreSDK.ViewModel
	{
		#region === Propriétés ===

			#region === Propriété : Id ===

				public const string Id_PROPERTYNAME = "Id";

				private System.Int32 _Id;
				///<summary>
				/// Propriété : Id
				///</summary>
				public System.Int32 Id
				{
					get
					{
						return GetValue<System.Int32>(DomainOfActionEncapsulation_Id_GetValue);
					}
					set
					{
						SetValue<System.Int32>(DomainOfActionEncapsulation_Id_GetValue, DomainOfActionEncapsulation_Id_SetValue, value, Id_PROPERTYNAME,  DoIdBeforeSet, DoIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIdBeforeSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIdAfterSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Libelle ===

				public const string Libelle_PROPERTYNAME = "Libelle";

				private System.String _Libelle;
				///<summary>
				/// Propriété : Libelle
				///</summary>
				public System.String Libelle
				{
					get
					{
						return GetValue<System.String>(DomainOfActionEncapsulation_Libelle_GetValue);
					}
					set
					{
						SetValue<System.String>(DomainOfActionEncapsulation_Libelle_GetValue, DomainOfActionEncapsulation_Libelle_SetValue, value, Libelle_PROPERTYNAME,  DoLibelleBeforeSet, DoLibelleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLibelleBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLibelleAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLibelleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IdForView ===

				public const string IdForView_PROPERTYNAME = "IdForView";

				private System.String _IdForView;
				///<summary>
				/// Propriété : IdForView
				///</summary>
				public System.String IdForView
				{
					get
					{
						return GetValue<System.String>(DomainOfActionEncapsulation_IdForView_GetValue);
					}
					set
					{
						SetValue<System.String>(DomainOfActionEncapsulation_IdForView_GetValue, DomainOfActionEncapsulation_IdForView_SetValue, value, IdForView_PROPERTYNAME,  DoIdForViewBeforeSet, DoIdForViewAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIdForViewBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIdForViewAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIdForViewDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsSelected ===

				public const string IsSelected_PROPERTYNAME = "IsSelected";

				private System.Boolean _IsSelected;
				///<summary>
				/// Propriété : IsSelected
				///</summary>
				public System.Boolean IsSelected
				{
					get
					{
						return GetValue<System.Boolean>(DomainOfActionEncapsulation_IsSelected_GetValue);
					}
					set
					{
						SetValue<System.Boolean>(DomainOfActionEncapsulation_IsSelected_GetValue, DomainOfActionEncapsulation_IsSelected_SetValue, value, IsSelected_PROPERTYNAME,  DoIsSelectedBeforeSet, DoIsSelectedAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsSelectedBeforeSet(string p_PropertyName, System.Boolean p_OldValue, System.Boolean p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsSelectedAfterSet(string p_PropertyName, System.Boolean p_OldValue, System.Boolean p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsSelectedDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux encapsulations ===


			#region === Encapsulation : DomainOfActionEncapsulation ===

				// Champ pour l'encapsulation.
				// Les propriétés implémentées font directement référence à ce champ.
				private MyADA.Common.Services.Models.DomainOfActionModel _DomainOfActionEncapsulation;

				///<summary>
				/// Encapsulation : DomainOfActionEncapsulation.
				///</summary>
				public MyADA.Common.Services.Models.DomainOfActionModel GetDomainOfActionEncapsulation()
				{
					return _DomainOfActionEncapsulation;
				}

				///<summary>
				/// Encapsulation : DomainOfActionEncapsulation.
				///</summary>
				public DomainOfActionViewModelItem SetDomainOfActionEncapsulation(MyADA.Common.Services.Models.DomainOfActionModel p_Value)
				{
					// En vue d'éventuelles notification (si initialisé), nous récupérons les anciennes valeurs (valeurs actuelles).
					var l_OldValueId = _DomainOfActionEncapsulation == null || !IsInitialized ? default(System.Int32) : Id;
					var l_OldValueLibelle = _DomainOfActionEncapsulation == null || !IsInitialized ? default(System.String) : Libelle;
					var l_OldValueIdForView = _DomainOfActionEncapsulation == null || !IsInitialized ? default(System.String) : IdForView;
					var l_OldValueIsSelected = _DomainOfActionEncapsulation == null || !IsInitialized ? default(System.Boolean) : IsSelected;

					// Affectation du nouvel object d'encapsulation.
					_DomainOfActionEncapsulation = p_Value;

					// Si pas initialisé, inutile d'aller plus loin.
					if(!IsInitialized) return this as DomainOfActionViewModelItem;

					// Nous allons notifier, nous avons besoin des valeurs actuelles.
					var l_NewValueId = _DomainOfActionEncapsulation == null ? default(System.Int32) : Id;
					var l_NewValueLibelle = _DomainOfActionEncapsulation == null ? default(System.String) : Libelle;
					var l_NewValueIdForView = _DomainOfActionEncapsulation == null ? default(System.String) : IdForView;
					var l_NewValueIsSelected = _DomainOfActionEncapsulation == null ? default(System.Boolean) : IsSelected;

					// Nous notifions.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Id_PROPERTYNAME, l_OldValueId, l_NewValueId));
					NotifyIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Libelle_PROPERTYNAME, l_OldValueLibelle, l_NewValueLibelle));
					NotifyLibelleDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(IdForView_PROPERTYNAME, l_OldValueIdForView, l_NewValueIdForView));
					NotifyIdForViewDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(IsSelected_PROPERTYNAME, l_OldValueIsSelected, l_NewValueIsSelected));
					NotifyIsSelectedDependencies();

					return this as DomainOfActionViewModelItem;
				}

				#region === Propriété : Id ===

					///<summary>
					/// Méthode utilisée par la propriété Id pour la lecture de la valeur corresponsande de DomainOfActionEncapsulation.
					///</summary>
					private System.Int32 DomainOfActionEncapsulation_Id_GetValue()
					{
						if(_DomainOfActionEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur DomainOfActionEncapsulation.", Id_PROPERTYNAME));
						return _DomainOfActionEncapsulation.Id;
					}

					///<summary>
					/// Méthode utilisée par la propriété Id pour l'affectation de la valeur corresponsande de DomainOfActionEncapsulation.
					///</summary>
					private void DomainOfActionEncapsulation_Id_SetValue(System.Int32 p_Value)
					{
						if(_DomainOfActionEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur DomainOfActionEncapsulation.", Id_PROPERTYNAME));
						_DomainOfActionEncapsulation.Id = p_Value;
					}


				#endregion

				#region === Propriété : Libelle ===

					///<summary>
					/// Méthode utilisée par la propriété Libelle pour la lecture de la valeur corresponsande de DomainOfActionEncapsulation.
					///</summary>
					private System.String DomainOfActionEncapsulation_Libelle_GetValue()
					{
						if(_DomainOfActionEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur DomainOfActionEncapsulation.", Libelle_PROPERTYNAME));
						return _DomainOfActionEncapsulation.Libelle;
					}

					///<summary>
					/// Méthode utilisée par la propriété Libelle pour l'affectation de la valeur corresponsande de DomainOfActionEncapsulation.
					///</summary>
					private void DomainOfActionEncapsulation_Libelle_SetValue(System.String p_Value)
					{
						if(_DomainOfActionEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur DomainOfActionEncapsulation.", Libelle_PROPERTYNAME));
						_DomainOfActionEncapsulation.Libelle = p_Value;
					}


				#endregion

				#region === Propriété : IdForView ===

					///<summary>
					/// Méthode utilisée par la propriété IdForView pour la lecture de la valeur corresponsande de DomainOfActionEncapsulation.
					///</summary>
					private System.String DomainOfActionEncapsulation_IdForView_GetValue()
					{
						if(_DomainOfActionEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur DomainOfActionEncapsulation.", IdForView_PROPERTYNAME));
						return _DomainOfActionEncapsulation.IdForView;
					}

					///<summary>
					/// Méthode utilisée par la propriété IdForView pour l'affectation de la valeur corresponsande de DomainOfActionEncapsulation.
					///</summary>
					private void DomainOfActionEncapsulation_IdForView_SetValue(System.String p_Value)
					{
						if(_DomainOfActionEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur DomainOfActionEncapsulation.", IdForView_PROPERTYNAME));
						_DomainOfActionEncapsulation.IdForView = p_Value;
					}


				#endregion

				#region === Propriété : IsSelected ===

					///<summary>
					/// Méthode utilisée par la propriété IsSelected pour la lecture de la valeur corresponsande de DomainOfActionEncapsulation.
					///</summary>
					private System.Boolean DomainOfActionEncapsulation_IsSelected_GetValue()
					{
						if(_DomainOfActionEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur DomainOfActionEncapsulation.", IsSelected_PROPERTYNAME));
						return _DomainOfActionEncapsulation.IsSelected;
					}

					///<summary>
					/// Méthode utilisée par la propriété IsSelected pour l'affectation de la valeur corresponsande de DomainOfActionEncapsulation.
					///</summary>
					private void DomainOfActionEncapsulation_IsSelected_SetValue(System.Boolean p_Value)
					{
						if(_DomainOfActionEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur DomainOfActionEncapsulation.", IsSelected_PROPERTYNAME));
						_DomainOfActionEncapsulation.IsSelected = p_Value;
					}


				#endregion


			#endregion

		#endregion
	}

	public partial class DomainOfActionViewModelItem : DomainOfActionViewModelItemBase
	{
	}
}
