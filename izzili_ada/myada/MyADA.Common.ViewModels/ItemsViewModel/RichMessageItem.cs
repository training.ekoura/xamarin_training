﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML RichMessageItem
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  CoreSDK.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;

namespace MyADA.Common.ViewModels
{
	public abstract partial class RichMessageItemBase : CoreSDK.ViewModel
	{
		#region === Propriétés ===

			#region === Propriété : id ===

				public const string id_PROPERTYNAME = "id";

				private MyADA.Common.DAOServices.Models.Id _id;
				///<summary>
				/// Propriété : id
				///</summary>
				public MyADA.Common.DAOServices.Models.Id id
				{
					get
					{
						return GetValue<MyADA.Common.DAOServices.Models.Id>(RichMessageEncapsulation_id_GetValue);
					}
					set
					{
						SetValue<MyADA.Common.DAOServices.Models.Id>(RichMessageEncapsulation_id_GetValue, RichMessageEncapsulation_id_SetValue, value, id_PROPERTYNAME,  DoidBeforeSet, DoidAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoidBeforeSet(string p_PropertyName, MyADA.Common.DAOServices.Models.Id p_OldValue, MyADA.Common.DAOServices.Models.Id p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoidAfterSet(string p_PropertyName, MyADA.Common.DAOServices.Models.Id p_OldValue, MyADA.Common.DAOServices.Models.Id p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyidDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : title ===

				public const string title_PROPERTYNAME = "title";

				private System.String _title;
				///<summary>
				/// Propriété : title
				///</summary>
				public System.String title
				{
					get
					{
						return GetValue<System.String>(RichMessageEncapsulation_title_GetValue);
					}
					set
					{
						SetValue<System.String>(RichMessageEncapsulation_title_GetValue, RichMessageEncapsulation_title_SetValue, value, title_PROPERTYNAME,  DotitleBeforeSet, DotitleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DotitleBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DotitleAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifytitleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : description ===

				public const string description_PROPERTYNAME = "description";

				private System.String _description;
				///<summary>
				/// Propriété : description
				///</summary>
				public System.String description
				{
					get
					{
						return GetValue<System.String>(RichMessageEncapsulation_description_GetValue);
					}
					set
					{
						SetValue<System.String>(RichMessageEncapsulation_description_GetValue, RichMessageEncapsulation_description_SetValue, value, description_PROPERTYNAME,  DodescriptionBeforeSet, DodescriptionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DodescriptionBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DodescriptionAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifydescriptionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : tags ===

				public const string tags_PROPERTYNAME = "tags";

				private System.Collections.Generic.IList<MyADA.Common.DAOServices.Models.Tag> _tags;
				///<summary>
				/// Propriété : tags
				///</summary>
				public System.Collections.Generic.IList<MyADA.Common.DAOServices.Models.Tag> tags
				{
					get
					{
						return GetValue<System.Collections.Generic.IList<MyADA.Common.DAOServices.Models.Tag>>(RichMessageEncapsulation_tags_GetValue);
					}
					set
					{
						SetValue<System.Collections.Generic.IList<MyADA.Common.DAOServices.Models.Tag>>(RichMessageEncapsulation_tags_GetValue, RichMessageEncapsulation_tags_SetValue, value, tags_PROPERTYNAME,  DotagsBeforeSet, DotagsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DotagsBeforeSet(string p_PropertyName, System.Collections.Generic.IList<MyADA.Common.DAOServices.Models.Tag> p_OldValue, System.Collections.Generic.IList<MyADA.Common.DAOServices.Models.Tag> p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DotagsAfterSet(string p_PropertyName, System.Collections.Generic.IList<MyADA.Common.DAOServices.Models.Tag> p_OldValue, System.Collections.Generic.IList<MyADA.Common.DAOServices.Models.Tag> p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifytagsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : content ===

				public const string content_PROPERTYNAME = "content";

				private System.String _content;
				///<summary>
				/// Propriété : content
				///</summary>
				public System.String content
				{
					get
					{
						return GetValue<System.String>(RichMessageEncapsulation_content_GetValue);
					}
					set
					{
						SetValue<System.String>(RichMessageEncapsulation_content_GetValue, RichMessageEncapsulation_content_SetValue, value, content_PROPERTYNAME,  DocontentBeforeSet, DocontentAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DocontentBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DocontentAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifycontentDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : image_name ===

				public const string image_name_PROPERTYNAME = "image_name";

				private System.String _image_name;
				///<summary>
				/// Propriété : image_name
				///</summary>
				public System.String image_name
				{
					get
					{
						return GetValue<System.String>(RichMessageEncapsulation_image_name_GetValue);
					}
					set
					{
						SetValue<System.String>(RichMessageEncapsulation_image_name_GetValue, RichMessageEncapsulation_image_name_SetValue, value, image_name_PROPERTYNAME,  Doimage_nameBeforeSet, Doimage_nameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void Doimage_nameBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void Doimage_nameAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void Notifyimage_nameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : image_url ===

				public const string image_url_PROPERTYNAME = "image_url";

				private System.String _image_url;
				///<summary>
				/// Propriété : image_url
				///</summary>
				public System.String image_url
				{
					get
					{
						return GetValue<System.String>(RichMessageEncapsulation_image_url_GetValue);
					}
					set
					{
						SetValue<System.String>(RichMessageEncapsulation_image_url_GetValue, RichMessageEncapsulation_image_url_SetValue, value, image_url_PROPERTYNAME,  Doimage_urlBeforeSet, Doimage_urlAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void Doimage_urlBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void Doimage_urlAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void Notifyimage_urlDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux encapsulations ===


			#region === Encapsulation : RichMessageEncapsulation ===

				// Champ pour l'encapsulation.
				// Les propriétés implémentées font directement référence à ce champ.
				private MyADA.Common.DAOServices.Models.AllRichMessageModel _RichMessageEncapsulation;

				///<summary>
				/// Encapsulation : RichMessageEncapsulation.
				///</summary>
				public MyADA.Common.DAOServices.Models.AllRichMessageModel GetRichMessageEncapsulation()
				{
					return _RichMessageEncapsulation;
				}

				///<summary>
				/// Encapsulation : RichMessageEncapsulation.
				///</summary>
				public RichMessageItem SetRichMessageEncapsulation(MyADA.Common.DAOServices.Models.AllRichMessageModel p_Value)
				{
					// En vue d'éventuelles notification (si initialisé), nous récupérons les anciennes valeurs (valeurs actuelles).
					var l_OldValueid = _RichMessageEncapsulation == null || !IsInitialized ? default(MyADA.Common.DAOServices.Models.Id) : id;
					var l_OldValuetitle = _RichMessageEncapsulation == null || !IsInitialized ? default(System.String) : title;
					var l_OldValuedescription = _RichMessageEncapsulation == null || !IsInitialized ? default(System.String) : description;
					var l_OldValuetags = _RichMessageEncapsulation == null || !IsInitialized ? default(System.Collections.Generic.IList<MyADA.Common.DAOServices.Models.Tag>) : tags;
					var l_OldValuecontent = _RichMessageEncapsulation == null || !IsInitialized ? default(System.String) : content;
					var l_OldValueimage_name = _RichMessageEncapsulation == null || !IsInitialized ? default(System.String) : image_name;
					var l_OldValueimage_url = _RichMessageEncapsulation == null || !IsInitialized ? default(System.String) : image_url;

					// Affectation du nouvel object d'encapsulation.
					_RichMessageEncapsulation = p_Value;

					// Si pas initialisé, inutile d'aller plus loin.
					if(!IsInitialized) return this as RichMessageItem;

					// Nous allons notifier, nous avons besoin des valeurs actuelles.
					var l_NewValueid = _RichMessageEncapsulation == null ? default(MyADA.Common.DAOServices.Models.Id) : id;
					var l_NewValuetitle = _RichMessageEncapsulation == null ? default(System.String) : title;
					var l_NewValuedescription = _RichMessageEncapsulation == null ? default(System.String) : description;
					var l_NewValuetags = _RichMessageEncapsulation == null ? default(System.Collections.Generic.IList<MyADA.Common.DAOServices.Models.Tag>) : tags;
					var l_NewValuecontent = _RichMessageEncapsulation == null ? default(System.String) : content;
					var l_NewValueimage_name = _RichMessageEncapsulation == null ? default(System.String) : image_name;
					var l_NewValueimage_url = _RichMessageEncapsulation == null ? default(System.String) : image_url;

					// Nous notifions.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(id_PROPERTYNAME, l_OldValueid, l_NewValueid));
					NotifyidDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(title_PROPERTYNAME, l_OldValuetitle, l_NewValuetitle));
					NotifytitleDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(description_PROPERTYNAME, l_OldValuedescription, l_NewValuedescription));
					NotifydescriptionDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(tags_PROPERTYNAME, l_OldValuetags, l_NewValuetags));
					NotifytagsDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(content_PROPERTYNAME, l_OldValuecontent, l_NewValuecontent));
					NotifycontentDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(image_name_PROPERTYNAME, l_OldValueimage_name, l_NewValueimage_name));
					Notifyimage_nameDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(image_url_PROPERTYNAME, l_OldValueimage_url, l_NewValueimage_url));
					Notifyimage_urlDependencies();

					return this as RichMessageItem;
				}

				#region === Propriété : id ===

					///<summary>
					/// Méthode utilisée par la propriété id pour la lecture de la valeur corresponsande de RichMessageEncapsulation.
					///</summary>
					private MyADA.Common.DAOServices.Models.Id RichMessageEncapsulation_id_GetValue()
					{
						if(_RichMessageEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur RichMessageEncapsulation.", id_PROPERTYNAME));
						return _RichMessageEncapsulation.id;
					}

					///<summary>
					/// Méthode utilisée par la propriété id pour l'affectation de la valeur corresponsande de RichMessageEncapsulation.
					///</summary>
					private void RichMessageEncapsulation_id_SetValue(MyADA.Common.DAOServices.Models.Id p_Value)
					{
						if(_RichMessageEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur RichMessageEncapsulation.", id_PROPERTYNAME));
						_RichMessageEncapsulation.id = p_Value;
					}


				#endregion

				#region === Propriété : title ===

					///<summary>
					/// Méthode utilisée par la propriété title pour la lecture de la valeur corresponsande de RichMessageEncapsulation.
					///</summary>
					private System.String RichMessageEncapsulation_title_GetValue()
					{
						if(_RichMessageEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur RichMessageEncapsulation.", title_PROPERTYNAME));
						return _RichMessageEncapsulation.title;
					}

					///<summary>
					/// Méthode utilisée par la propriété title pour l'affectation de la valeur corresponsande de RichMessageEncapsulation.
					///</summary>
					private void RichMessageEncapsulation_title_SetValue(System.String p_Value)
					{
						if(_RichMessageEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur RichMessageEncapsulation.", title_PROPERTYNAME));
						_RichMessageEncapsulation.title = p_Value;
					}


				#endregion

				#region === Propriété : description ===

					///<summary>
					/// Méthode utilisée par la propriété description pour la lecture de la valeur corresponsande de RichMessageEncapsulation.
					///</summary>
					private System.String RichMessageEncapsulation_description_GetValue()
					{
						if(_RichMessageEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur RichMessageEncapsulation.", description_PROPERTYNAME));
						return _RichMessageEncapsulation.description;
					}

					///<summary>
					/// Méthode utilisée par la propriété description pour l'affectation de la valeur corresponsande de RichMessageEncapsulation.
					///</summary>
					private void RichMessageEncapsulation_description_SetValue(System.String p_Value)
					{
						if(_RichMessageEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur RichMessageEncapsulation.", description_PROPERTYNAME));
						_RichMessageEncapsulation.description = p_Value;
					}


				#endregion

				#region === Propriété : tags ===

					///<summary>
					/// Méthode utilisée par la propriété tags pour la lecture de la valeur corresponsande de RichMessageEncapsulation.
					///</summary>
					private System.Collections.Generic.IList<MyADA.Common.DAOServices.Models.Tag> RichMessageEncapsulation_tags_GetValue()
					{
						if(_RichMessageEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur RichMessageEncapsulation.", tags_PROPERTYNAME));
						return _RichMessageEncapsulation.tags;
					}

					///<summary>
					/// Méthode utilisée par la propriété tags pour l'affectation de la valeur corresponsande de RichMessageEncapsulation.
					///</summary>
					private void RichMessageEncapsulation_tags_SetValue(System.Collections.Generic.IList<MyADA.Common.DAOServices.Models.Tag> p_Value)
					{
						if(_RichMessageEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur RichMessageEncapsulation.", tags_PROPERTYNAME));
						_RichMessageEncapsulation.tags = p_Value;
					}


				#endregion

				#region === Propriété : content ===

					///<summary>
					/// Méthode utilisée par la propriété content pour la lecture de la valeur corresponsande de RichMessageEncapsulation.
					///</summary>
					private System.String RichMessageEncapsulation_content_GetValue()
					{
						if(_RichMessageEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur RichMessageEncapsulation.", content_PROPERTYNAME));
						return _RichMessageEncapsulation.content;
					}

					///<summary>
					/// Méthode utilisée par la propriété content pour l'affectation de la valeur corresponsande de RichMessageEncapsulation.
					///</summary>
					private void RichMessageEncapsulation_content_SetValue(System.String p_Value)
					{
						if(_RichMessageEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur RichMessageEncapsulation.", content_PROPERTYNAME));
						_RichMessageEncapsulation.content = p_Value;
					}


				#endregion

				#region === Propriété : image_name ===

					///<summary>
					/// Méthode utilisée par la propriété image_name pour la lecture de la valeur corresponsande de RichMessageEncapsulation.
					///</summary>
					private System.String RichMessageEncapsulation_image_name_GetValue()
					{
						if(_RichMessageEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur RichMessageEncapsulation.", image_name_PROPERTYNAME));
						return _RichMessageEncapsulation.image_name;
					}

					///<summary>
					/// Méthode utilisée par la propriété image_name pour l'affectation de la valeur corresponsande de RichMessageEncapsulation.
					///</summary>
					private void RichMessageEncapsulation_image_name_SetValue(System.String p_Value)
					{
						if(_RichMessageEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur RichMessageEncapsulation.", image_name_PROPERTYNAME));
						_RichMessageEncapsulation.image_name = p_Value;
					}


				#endregion

				#region === Propriété : image_url ===

					///<summary>
					/// Méthode utilisée par la propriété image_url pour la lecture de la valeur corresponsande de RichMessageEncapsulation.
					///</summary>
					private System.String RichMessageEncapsulation_image_url_GetValue()
					{
						if(_RichMessageEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur RichMessageEncapsulation.", image_url_PROPERTYNAME));
						return _RichMessageEncapsulation.image_url;
					}

					///<summary>
					/// Méthode utilisée par la propriété image_url pour l'affectation de la valeur corresponsande de RichMessageEncapsulation.
					///</summary>
					private void RichMessageEncapsulation_image_url_SetValue(System.String p_Value)
					{
						if(_RichMessageEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur RichMessageEncapsulation.", image_url_PROPERTYNAME));
						_RichMessageEncapsulation.image_url = p_Value;
					}


				#endregion


			#endregion

		#endregion
	}

	public partial class RichMessageItem : RichMessageItemBase
	{
	}
}
