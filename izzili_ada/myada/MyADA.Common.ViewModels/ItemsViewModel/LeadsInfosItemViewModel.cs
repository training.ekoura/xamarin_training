﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML LeadsInfosItemViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  CoreSDK.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;

namespace MyADA.Common.ViewModels
{
	public abstract partial class LeadsInfosItemViewModelBase : CoreSDK.ViewModel
	{
		#region === Propriétés ===

			#region === Propriété : FullName ===

				public const string FullName_PROPERTYNAME = "FullName";

				private string _FullName;
				///<summary>
				/// Propriété : FullName
				///</summary>
				public string FullName
				{
					get
					{
						return GetValue<string>(() => _FullName);
					}
					set
					{
						SetValue<string>(() => _FullName, (v) => _FullName = v, value, FullName_PROPERTYNAME,  DoFullNameBeforeSet, DoFullNameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFullNameBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFullNameAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFullNameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FormattedDate ===

				public const string FormattedDate_PROPERTYNAME = "FormattedDate";

				private string _FormattedDate;
				///<summary>
				/// Propriété : FormattedDate
				///</summary>
				public string FormattedDate
				{
					get
					{
						return GetValue<string>(() => _FormattedDate);
					}
					set
					{
						SetValue<string>(() => _FormattedDate, (v) => _FormattedDate = v, value, FormattedDate_PROPERTYNAME,  DoFormattedDateBeforeSet, DoFormattedDateAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFormattedDateBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFormattedDateAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFormattedDateDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FormattedHeure ===

				public const string FormattedHeure_PROPERTYNAME = "FormattedHeure";

				private string _FormattedHeure;
				///<summary>
				/// Propriété : FormattedHeure
				///</summary>
				public string FormattedHeure
				{
					get
					{
						return GetValue<string>(() => _FormattedHeure);
					}
					set
					{
						SetValue<string>(() => _FormattedHeure, (v) => _FormattedHeure = v, value, FormattedHeure_PROPERTYNAME,  DoFormattedHeureBeforeSet, DoFormattedHeureAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFormattedHeureBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFormattedHeureAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFormattedHeureDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsDone ===

				public const string IsDone_PROPERTYNAME = "IsDone";

				private bool _IsDone;
				///<summary>
				/// Propriété : IsDone
				///</summary>
				public bool IsDone
				{
					get
					{
						return GetValue<bool>(() => _IsDone);
					}
					set
					{
						SetValue<bool>(() => _IsDone, (v) => _IsDone = v, value, IsDone_PROPERTYNAME,  DoIsDoneBeforeSet, DoIsDoneAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsDoneBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsDoneAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsDoneDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsNotInterest ===

				public const string IsNotInterest_PROPERTYNAME = "IsNotInterest";

				private bool _IsNotInterest;
				///<summary>
				/// Propriété : IsNotInterest
				///</summary>
				public bool IsNotInterest
				{
					get
					{
						return GetValue<bool>(() => _IsNotInterest);
					}
					set
					{
						SetValue<bool>(() => _IsNotInterest, (v) => _IsNotInterest = v, value, IsNotInterest_PROPERTYNAME,  DoIsNotInterestBeforeSet, DoIsNotInterestAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsNotInterestBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsNotInterestAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsNotInterestDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsRecontact ===

				public const string IsRecontact_PROPERTYNAME = "IsRecontact";

				private bool _IsRecontact;
				///<summary>
				/// Propriété : IsRecontact
				///</summary>
				public bool IsRecontact
				{
					get
					{
						return GetValue<bool>(() => _IsRecontact);
					}
					set
					{
						SetValue<bool>(() => _IsRecontact, (v) => _IsRecontact = v, value, IsRecontact_PROPERTYNAME,  DoIsRecontactBeforeSet, DoIsRecontactAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsRecontactBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsRecontactAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsRecontactDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsPhoneVisible ===

				public const string IsPhoneVisible_PROPERTYNAME = "IsPhoneVisible";

				private bool _IsPhoneVisible;
				///<summary>
				/// Propriété : IsPhoneVisible
				///</summary>
				public bool IsPhoneVisible
				{
					get
					{
						return GetValue<bool>(() => _IsPhoneVisible);
					}
					set
					{
						SetValue<bool>(() => _IsPhoneVisible, (v) => _IsPhoneVisible = v, value, IsPhoneVisible_PROPERTYNAME,  DoIsPhoneVisibleBeforeSet, DoIsPhoneVisibleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsPhoneVisibleBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsPhoneVisibleAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsPhoneVisibleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsEmailVisible ===

				public const string IsEmailVisible_PROPERTYNAME = "IsEmailVisible";

				private bool _IsEmailVisible;
				///<summary>
				/// Propriété : IsEmailVisible
				///</summary>
				public bool IsEmailVisible
				{
					get
					{
						return GetValue<bool>(() => _IsEmailVisible);
					}
					set
					{
						SetValue<bool>(() => _IsEmailVisible, (v) => _IsEmailVisible = v, value, IsEmailVisible_PROPERTYNAME,  DoIsEmailVisibleBeforeSet, DoIsEmailVisibleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsEmailVisibleBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsEmailVisibleAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsEmailVisibleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsFacebookVisible ===

				public const string IsFacebookVisible_PROPERTYNAME = "IsFacebookVisible";

				private bool _IsFacebookVisible;
				///<summary>
				/// Propriété : IsFacebookVisible
				///</summary>
				public bool IsFacebookVisible
				{
					get
					{
						return GetValue<bool>(() => _IsFacebookVisible);
					}
					set
					{
						SetValue<bool>(() => _IsFacebookVisible, (v) => _IsFacebookVisible = v, value, IsFacebookVisible_PROPERTYNAME,  DoIsFacebookVisibleBeforeSet, DoIsFacebookVisibleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsFacebookVisibleBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsFacebookVisibleAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsFacebookVisibleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsKakaoVisible ===

				public const string IsKakaoVisible_PROPERTYNAME = "IsKakaoVisible";

				private bool _IsKakaoVisible;
				///<summary>
				/// Propriété : IsKakaoVisible
				///</summary>
				public bool IsKakaoVisible
				{
					get
					{
						return GetValue<bool>(() => _IsKakaoVisible);
					}
					set
					{
						SetValue<bool>(() => _IsKakaoVisible, (v) => _IsKakaoVisible = v, value, IsKakaoVisible_PROPERTYNAME,  DoIsKakaoVisibleBeforeSet, DoIsKakaoVisibleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsKakaoVisibleBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsKakaoVisibleAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsKakaoVisibleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsModelVisible ===

				public const string IsModelVisible_PROPERTYNAME = "IsModelVisible";

				private bool _IsModelVisible;
				///<summary>
				/// Propriété : IsModelVisible
				///</summary>
				public bool IsModelVisible
				{
					get
					{
						return GetValue<bool>(() => _IsModelVisible);
					}
					set
					{
						SetValue<bool>(() => _IsModelVisible, (v) => _IsModelVisible = v, value, IsModelVisible_PROPERTYNAME,  DoIsModelVisibleBeforeSet, DoIsModelVisibleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsModelVisibleBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsModelVisibleAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsModelVisibleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsCommentVisible ===

				public const string IsCommentVisible_PROPERTYNAME = "IsCommentVisible";

				private bool _IsCommentVisible;
				///<summary>
				/// Propriété : IsCommentVisible
				///</summary>
				public bool IsCommentVisible
				{
					get
					{
						return GetValue<bool>(() => _IsCommentVisible);
					}
					set
					{
						SetValue<bool>(() => _IsCommentVisible, (v) => _IsCommentVisible = v, value, IsCommentVisible_PROPERTYNAME,  DoIsCommentVisibleBeforeSet, DoIsCommentVisibleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsCommentVisibleBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsCommentVisibleAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsCommentVisibleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LeadId ===

				public const string LeadId_PROPERTYNAME = "LeadId";

				private System.String _LeadId;
				///<summary>
				/// Propriété : LeadId
				///</summary>
				public System.String LeadId
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_LeadId_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_LeadId_GetValue, LeadsEncapsulation_LeadId_SetValue, value, LeadId_PROPERTYNAME,  DoLeadIdBeforeSet, DoLeadIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLeadIdBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLeadIdAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLeadIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ShowroomId ===

				public const string ShowroomId_PROPERTYNAME = "ShowroomId";

				private System.String _ShowroomId;
				///<summary>
				/// Propriété : ShowroomId
				///</summary>
				public System.String ShowroomId
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_ShowroomId_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_ShowroomId_GetValue, LeadsEncapsulation_ShowroomId_SetValue, value, ShowroomId_PROPERTYNAME,  DoShowroomIdBeforeSet, DoShowroomIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoShowroomIdBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoShowroomIdAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyShowroomIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UserId ===

				public const string UserId_PROPERTYNAME = "UserId";

				private System.String _UserId;
				///<summary>
				/// Propriété : UserId
				///</summary>
				public System.String UserId
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_UserId_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_UserId_GetValue, LeadsEncapsulation_UserId_SetValue, value, UserId_PROPERTYNAME,  DoUserIdBeforeSet, DoUserIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUserIdBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUserIdAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUserIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Title ===

				public const string Title_PROPERTYNAME = "Title";

				private System.String _Title;
				///<summary>
				/// Propriété : Title
				///</summary>
				public System.String Title
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_Title_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_Title_GetValue, LeadsEncapsulation_Title_SetValue, value, Title_PROPERTYNAME,  DoTitleBeforeSet, DoTitleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTitleBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTitleAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTitleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FirstName ===

				public const string FirstName_PROPERTYNAME = "FirstName";

				private System.String _FirstName;
				///<summary>
				/// Propriété : FirstName
				///</summary>
				public System.String FirstName
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_FirstName_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_FirstName_GetValue, LeadsEncapsulation_FirstName_SetValue, value, FirstName_PROPERTYNAME,  DoFirstNameBeforeSet, DoFirstNameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFirstNameBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFirstNameAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFirstNameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LastName ===

				public const string LastName_PROPERTYNAME = "LastName";

				private System.String _LastName;
				///<summary>
				/// Propriété : LastName
				///</summary>
				public System.String LastName
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_LastName_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_LastName_GetValue, LeadsEncapsulation_LastName_SetValue, value, LastName_PROPERTYNAME,  DoLastNameBeforeSet, DoLastNameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLastNameBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLastNameAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLastNameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Email ===

				public const string Email_PROPERTYNAME = "Email";

				private System.String _Email;
				///<summary>
				/// Propriété : Email
				///</summary>
				public System.String Email
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_Email_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_Email_GetValue, LeadsEncapsulation_Email_SetValue, value, Email_PROPERTYNAME,  DoEmailBeforeSet, DoEmailAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoEmailBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoEmailAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyEmailDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : PhoneNumber ===

				public const string PhoneNumber_PROPERTYNAME = "PhoneNumber";

				private System.String _PhoneNumber;
				///<summary>
				/// Propriété : PhoneNumber
				///</summary>
				public System.String PhoneNumber
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_PhoneNumber_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_PhoneNumber_GetValue, LeadsEncapsulation_PhoneNumber_SetValue, value, PhoneNumber_PROPERTYNAME,  DoPhoneNumberBeforeSet, DoPhoneNumberAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoPhoneNumberBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoPhoneNumberAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyPhoneNumberDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FacebookId ===

				public const string FacebookId_PROPERTYNAME = "FacebookId";

				private System.String _FacebookId;
				///<summary>
				/// Propriété : FacebookId
				///</summary>
				public System.String FacebookId
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_FacebookId_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_FacebookId_GetValue, LeadsEncapsulation_FacebookId_SetValue, value, FacebookId_PROPERTYNAME,  DoFacebookIdBeforeSet, DoFacebookIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFacebookIdBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFacebookIdAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFacebookIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : TwitterId ===

				public const string TwitterId_PROPERTYNAME = "TwitterId";

				private System.String _TwitterId;
				///<summary>
				/// Propriété : TwitterId
				///</summary>
				public System.String TwitterId
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_TwitterId_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_TwitterId_GetValue, LeadsEncapsulation_TwitterId_SetValue, value, TwitterId_PROPERTYNAME,  DoTwitterIdBeforeSet, DoTwitterIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTwitterIdBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTwitterIdAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTwitterIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : KakaoId ===

				public const string KakaoId_PROPERTYNAME = "KakaoId";

				private System.String _KakaoId;
				///<summary>
				/// Propriété : KakaoId
				///</summary>
				public System.String KakaoId
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_KakaoId_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_KakaoId_GetValue, LeadsEncapsulation_KakaoId_SetValue, value, KakaoId_PROPERTYNAME,  DoKakaoIdBeforeSet, DoKakaoIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoKakaoIdBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoKakaoIdAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyKakaoIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Tag ===

				public const string Tag_PROPERTYNAME = "Tag";

				private System.String _Tag;
				///<summary>
				/// Propriété : Tag
				///</summary>
				public System.String Tag
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_Tag_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_Tag_GetValue, LeadsEncapsulation_Tag_SetValue, value, Tag_PROPERTYNAME,  DoTagBeforeSet, DoTagAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTagBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTagAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTagDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Status ===

				public const string Status_PROPERTYNAME = "Status";

				private System.String _Status;
				///<summary>
				/// Propriété : Status
				///</summary>
				public System.String Status
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_Status_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_Status_GetValue, LeadsEncapsulation_Status_SetValue, value, Status_PROPERTYNAME,  DoStatusBeforeSet, DoStatusAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoStatusBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoStatusAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyStatusDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : RecontactOn ===

				public const string RecontactOn_PROPERTYNAME = "RecontactOn";

				private System.String _RecontactOn;
				///<summary>
				/// Propriété : RecontactOn
				///</summary>
				public System.String RecontactOn
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_RecontactOn_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_RecontactOn_GetValue, LeadsEncapsulation_RecontactOn_SetValue, value, RecontactOn_PROPERTYNAME,  DoRecontactOnBeforeSet, DoRecontactOnAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoRecontactOnBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoRecontactOnAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyRecontactOnDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Comment ===

				public const string Comment_PROPERTYNAME = "Comment";

				private System.String _Comment;
				///<summary>
				/// Propriété : Comment
				///</summary>
				public System.String Comment
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_Comment_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_Comment_GetValue, LeadsEncapsulation_Comment_SetValue, value, Comment_PROPERTYNAME,  DoCommentBeforeSet, DoCommentAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCommentBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCommentAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCommentDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Models ===

				public const string Models_PROPERTYNAME = "Models";

				private System.String _Models;
				///<summary>
				/// Propriété : Models
				///</summary>
				public System.String Models
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_Models_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_Models_GetValue, LeadsEncapsulation_Models_SetValue, value, Models_PROPERTYNAME,  DoModelsBeforeSet, DoModelsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoModelsBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoModelsAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyModelsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Category ===

				public const string Category_PROPERTYNAME = "Category";

				private System.String _Category;
				///<summary>
				/// Propriété : Category
				///</summary>
				public System.String Category
				{
					get
					{
						return GetValue<System.String>(LeadsEncapsulation_Category_GetValue);
					}
					set
					{
						SetValue<System.String>(LeadsEncapsulation_Category_GetValue, LeadsEncapsulation_Category_SetValue, value, Category_PROPERTYNAME,  DoCategoryBeforeSet, DoCategoryAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCategoryBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCategoryAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCategoryDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : History ===

				public const string History_PROPERTYNAME = "History";

				private System.Collections.Generic.IList<MyADA.Common.Services.Models.History> _History;
				///<summary>
				/// Propriété : History
				///</summary>
				public System.Collections.Generic.IList<MyADA.Common.Services.Models.History> History
				{
					get
					{
						return GetValue<System.Collections.Generic.IList<MyADA.Common.Services.Models.History>>(LeadsEncapsulation_History_GetValue);
					}
					set
					{
						SetValue<System.Collections.Generic.IList<MyADA.Common.Services.Models.History>>(LeadsEncapsulation_History_GetValue, LeadsEncapsulation_History_SetValue, value, History_PROPERTYNAME,  DoHistoryBeforeSet, DoHistoryAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoHistoryBeforeSet(string p_PropertyName, System.Collections.Generic.IList<MyADA.Common.Services.Models.History> p_OldValue, System.Collections.Generic.IList<MyADA.Common.Services.Models.History> p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoHistoryAfterSet(string p_PropertyName, System.Collections.Generic.IList<MyADA.Common.Services.Models.History> p_OldValue, System.Collections.Generic.IList<MyADA.Common.Services.Models.History> p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyHistoryDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CreatedDate ===

				public const string CreatedDate_PROPERTYNAME = "CreatedDate";

				private System.DateTime _CreatedDate;
				///<summary>
				/// Propriété : CreatedDate
				///</summary>
				public System.DateTime CreatedDate
				{
					get
					{
						return GetValue<System.DateTime>(LeadsEncapsulation_CreatedDate_GetValue);
					}
					set
					{
						SetValue<System.DateTime>(LeadsEncapsulation_CreatedDate_GetValue, LeadsEncapsulation_CreatedDate_SetValue, value, CreatedDate_PROPERTYNAME,  DoCreatedDateBeforeSet, DoCreatedDateAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCreatedDateBeforeSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCreatedDateAfterSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCreatedDateDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LastModified ===

				public const string LastModified_PROPERTYNAME = "LastModified";

				private System.DateTime _LastModified;
				///<summary>
				/// Propriété : LastModified
				///</summary>
				public System.DateTime LastModified
				{
					get
					{
						return GetValue<System.DateTime>(LeadsEncapsulation_LastModified_GetValue);
					}
					set
					{
						SetValue<System.DateTime>(LeadsEncapsulation_LastModified_GetValue, LeadsEncapsulation_LastModified_SetValue, value, LastModified_PROPERTYNAME,  DoLastModifiedBeforeSet, DoLastModifiedAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLastModifiedBeforeSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLastModifiedAfterSet(string p_PropertyName, System.DateTime p_OldValue, System.DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLastModifiedDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux encapsulations ===


			#region === Encapsulation : LeadsEncapsulation ===

				// Champ pour l'encapsulation.
				// Les propriétés implémentées font directement référence à ce champ.
				private MyADA.Common.Services.Models.LeadInfos _LeadsEncapsulation;

				///<summary>
				/// Encapsulation : LeadsEncapsulation.
				///</summary>
				public MyADA.Common.Services.Models.LeadInfos GetLeadsEncapsulation()
				{
					return _LeadsEncapsulation;
				}

				///<summary>
				/// Encapsulation : LeadsEncapsulation.
				///</summary>
				public LeadsInfosItemViewModel SetLeadsEncapsulation(MyADA.Common.Services.Models.LeadInfos p_Value)
				{
					// En vue d'éventuelles notification (si initialisé), nous récupérons les anciennes valeurs (valeurs actuelles).
					var l_OldValueLeadId = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : LeadId;
					var l_OldValueShowroomId = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : ShowroomId;
					var l_OldValueUserId = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : UserId;
					var l_OldValueTitle = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : Title;
					var l_OldValueFirstName = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : FirstName;
					var l_OldValueLastName = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : LastName;
					var l_OldValueEmail = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : Email;
					var l_OldValuePhoneNumber = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : PhoneNumber;
					var l_OldValueFacebookId = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : FacebookId;
					var l_OldValueTwitterId = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : TwitterId;
					var l_OldValueKakaoId = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : KakaoId;
					var l_OldValueTag = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : Tag;
					var l_OldValueStatus = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : Status;
					var l_OldValueRecontactOn = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : RecontactOn;
					var l_OldValueComment = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : Comment;
					var l_OldValueModels = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : Models;
					var l_OldValueCategory = _LeadsEncapsulation == null || !IsInitialized ? default(System.String) : Category;
					var l_OldValueHistory = _LeadsEncapsulation == null || !IsInitialized ? default(System.Collections.Generic.IList<MyADA.Common.Services.Models.History>) : History;
					var l_OldValueCreatedDate = _LeadsEncapsulation == null || !IsInitialized ? default(System.DateTime) : CreatedDate;
					var l_OldValueLastModified = _LeadsEncapsulation == null || !IsInitialized ? default(System.DateTime) : LastModified;

					// Affectation du nouvel object d'encapsulation.
					_LeadsEncapsulation = p_Value;

					// Si pas initialisé, inutile d'aller plus loin.
					if(!IsInitialized) return this as LeadsInfosItemViewModel;

					// Nous allons notifier, nous avons besoin des valeurs actuelles.
					var l_NewValueLeadId = _LeadsEncapsulation == null ? default(System.String) : LeadId;
					var l_NewValueShowroomId = _LeadsEncapsulation == null ? default(System.String) : ShowroomId;
					var l_NewValueUserId = _LeadsEncapsulation == null ? default(System.String) : UserId;
					var l_NewValueTitle = _LeadsEncapsulation == null ? default(System.String) : Title;
					var l_NewValueFirstName = _LeadsEncapsulation == null ? default(System.String) : FirstName;
					var l_NewValueLastName = _LeadsEncapsulation == null ? default(System.String) : LastName;
					var l_NewValueEmail = _LeadsEncapsulation == null ? default(System.String) : Email;
					var l_NewValuePhoneNumber = _LeadsEncapsulation == null ? default(System.String) : PhoneNumber;
					var l_NewValueFacebookId = _LeadsEncapsulation == null ? default(System.String) : FacebookId;
					var l_NewValueTwitterId = _LeadsEncapsulation == null ? default(System.String) : TwitterId;
					var l_NewValueKakaoId = _LeadsEncapsulation == null ? default(System.String) : KakaoId;
					var l_NewValueTag = _LeadsEncapsulation == null ? default(System.String) : Tag;
					var l_NewValueStatus = _LeadsEncapsulation == null ? default(System.String) : Status;
					var l_NewValueRecontactOn = _LeadsEncapsulation == null ? default(System.String) : RecontactOn;
					var l_NewValueComment = _LeadsEncapsulation == null ? default(System.String) : Comment;
					var l_NewValueModels = _LeadsEncapsulation == null ? default(System.String) : Models;
					var l_NewValueCategory = _LeadsEncapsulation == null ? default(System.String) : Category;
					var l_NewValueHistory = _LeadsEncapsulation == null ? default(System.Collections.Generic.IList<MyADA.Common.Services.Models.History>) : History;
					var l_NewValueCreatedDate = _LeadsEncapsulation == null ? default(System.DateTime) : CreatedDate;
					var l_NewValueLastModified = _LeadsEncapsulation == null ? default(System.DateTime) : LastModified;

					// Nous notifions.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LeadId_PROPERTYNAME, l_OldValueLeadId, l_NewValueLeadId));
					NotifyLeadIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ShowroomId_PROPERTYNAME, l_OldValueShowroomId, l_NewValueShowroomId));
					NotifyShowroomIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(UserId_PROPERTYNAME, l_OldValueUserId, l_NewValueUserId));
					NotifyUserIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Title_PROPERTYNAME, l_OldValueTitle, l_NewValueTitle));
					NotifyTitleDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(FirstName_PROPERTYNAME, l_OldValueFirstName, l_NewValueFirstName));
					NotifyFirstNameDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LastName_PROPERTYNAME, l_OldValueLastName, l_NewValueLastName));
					NotifyLastNameDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Email_PROPERTYNAME, l_OldValueEmail, l_NewValueEmail));
					NotifyEmailDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(PhoneNumber_PROPERTYNAME, l_OldValuePhoneNumber, l_NewValuePhoneNumber));
					NotifyPhoneNumberDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(FacebookId_PROPERTYNAME, l_OldValueFacebookId, l_NewValueFacebookId));
					NotifyFacebookIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(TwitterId_PROPERTYNAME, l_OldValueTwitterId, l_NewValueTwitterId));
					NotifyTwitterIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(KakaoId_PROPERTYNAME, l_OldValueKakaoId, l_NewValueKakaoId));
					NotifyKakaoIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Tag_PROPERTYNAME, l_OldValueTag, l_NewValueTag));
					NotifyTagDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Status_PROPERTYNAME, l_OldValueStatus, l_NewValueStatus));
					NotifyStatusDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(RecontactOn_PROPERTYNAME, l_OldValueRecontactOn, l_NewValueRecontactOn));
					NotifyRecontactOnDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Comment_PROPERTYNAME, l_OldValueComment, l_NewValueComment));
					NotifyCommentDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Models_PROPERTYNAME, l_OldValueModels, l_NewValueModels));
					NotifyModelsDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Category_PROPERTYNAME, l_OldValueCategory, l_NewValueCategory));
					NotifyCategoryDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(History_PROPERTYNAME, l_OldValueHistory, l_NewValueHistory));
					NotifyHistoryDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(CreatedDate_PROPERTYNAME, l_OldValueCreatedDate, l_NewValueCreatedDate));
					NotifyCreatedDateDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LastModified_PROPERTYNAME, l_OldValueLastModified, l_NewValueLastModified));
					NotifyLastModifiedDependencies();

					return this as LeadsInfosItemViewModel;
				}

				#region === Propriété : LeadId ===

					///<summary>
					/// Méthode utilisée par la propriété LeadId pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_LeadId_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", LeadId_PROPERTYNAME));
						return _LeadsEncapsulation.LeadId;
					}

					///<summary>
					/// Méthode utilisée par la propriété LeadId pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_LeadId_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", LeadId_PROPERTYNAME));
						_LeadsEncapsulation.LeadId = p_Value;
					}


				#endregion

				#region === Propriété : ShowroomId ===

					///<summary>
					/// Méthode utilisée par la propriété ShowroomId pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_ShowroomId_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", ShowroomId_PROPERTYNAME));
						return _LeadsEncapsulation.ShowroomId;
					}

					///<summary>
					/// Méthode utilisée par la propriété ShowroomId pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_ShowroomId_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", ShowroomId_PROPERTYNAME));
						_LeadsEncapsulation.ShowroomId = p_Value;
					}


				#endregion

				#region === Propriété : UserId ===

					///<summary>
					/// Méthode utilisée par la propriété UserId pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_UserId_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", UserId_PROPERTYNAME));
						return _LeadsEncapsulation.UserId;
					}

					///<summary>
					/// Méthode utilisée par la propriété UserId pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_UserId_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", UserId_PROPERTYNAME));
						_LeadsEncapsulation.UserId = p_Value;
					}


				#endregion

				#region === Propriété : Title ===

					///<summary>
					/// Méthode utilisée par la propriété Title pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_Title_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", Title_PROPERTYNAME));
						return _LeadsEncapsulation.Title;
					}

					///<summary>
					/// Méthode utilisée par la propriété Title pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_Title_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", Title_PROPERTYNAME));
						_LeadsEncapsulation.Title = p_Value;
					}


				#endregion

				#region === Propriété : FirstName ===

					///<summary>
					/// Méthode utilisée par la propriété FirstName pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_FirstName_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", FirstName_PROPERTYNAME));
						return _LeadsEncapsulation.FirstName;
					}

					///<summary>
					/// Méthode utilisée par la propriété FirstName pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_FirstName_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", FirstName_PROPERTYNAME));
						_LeadsEncapsulation.FirstName = p_Value;
					}


				#endregion

				#region === Propriété : LastName ===

					///<summary>
					/// Méthode utilisée par la propriété LastName pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_LastName_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", LastName_PROPERTYNAME));
						return _LeadsEncapsulation.LastName;
					}

					///<summary>
					/// Méthode utilisée par la propriété LastName pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_LastName_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", LastName_PROPERTYNAME));
						_LeadsEncapsulation.LastName = p_Value;
					}


				#endregion

				#region === Propriété : Email ===

					///<summary>
					/// Méthode utilisée par la propriété Email pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_Email_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", Email_PROPERTYNAME));
						return _LeadsEncapsulation.Email;
					}

					///<summary>
					/// Méthode utilisée par la propriété Email pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_Email_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", Email_PROPERTYNAME));
						_LeadsEncapsulation.Email = p_Value;
					}


				#endregion

				#region === Propriété : PhoneNumber ===

					///<summary>
					/// Méthode utilisée par la propriété PhoneNumber pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_PhoneNumber_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", PhoneNumber_PROPERTYNAME));
						return _LeadsEncapsulation.PhoneNumber;
					}

					///<summary>
					/// Méthode utilisée par la propriété PhoneNumber pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_PhoneNumber_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", PhoneNumber_PROPERTYNAME));
						_LeadsEncapsulation.PhoneNumber = p_Value;
					}


				#endregion

				#region === Propriété : FacebookId ===

					///<summary>
					/// Méthode utilisée par la propriété FacebookId pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_FacebookId_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", FacebookId_PROPERTYNAME));
						return _LeadsEncapsulation.FacebookId;
					}

					///<summary>
					/// Méthode utilisée par la propriété FacebookId pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_FacebookId_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", FacebookId_PROPERTYNAME));
						_LeadsEncapsulation.FacebookId = p_Value;
					}


				#endregion

				#region === Propriété : TwitterId ===

					///<summary>
					/// Méthode utilisée par la propriété TwitterId pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_TwitterId_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", TwitterId_PROPERTYNAME));
						return _LeadsEncapsulation.TwitterId;
					}

					///<summary>
					/// Méthode utilisée par la propriété TwitterId pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_TwitterId_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", TwitterId_PROPERTYNAME));
						_LeadsEncapsulation.TwitterId = p_Value;
					}


				#endregion

				#region === Propriété : KakaoId ===

					///<summary>
					/// Méthode utilisée par la propriété KakaoId pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_KakaoId_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", KakaoId_PROPERTYNAME));
						return _LeadsEncapsulation.KakaoId;
					}

					///<summary>
					/// Méthode utilisée par la propriété KakaoId pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_KakaoId_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", KakaoId_PROPERTYNAME));
						_LeadsEncapsulation.KakaoId = p_Value;
					}


				#endregion

				#region === Propriété : Tag ===

					///<summary>
					/// Méthode utilisée par la propriété Tag pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_Tag_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", Tag_PROPERTYNAME));
						return _LeadsEncapsulation.Tag;
					}

					///<summary>
					/// Méthode utilisée par la propriété Tag pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_Tag_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", Tag_PROPERTYNAME));
						_LeadsEncapsulation.Tag = p_Value;
					}


				#endregion

				#region === Propriété : Status ===

					///<summary>
					/// Méthode utilisée par la propriété Status pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_Status_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", Status_PROPERTYNAME));
						return _LeadsEncapsulation.Status;
					}

					///<summary>
					/// Méthode utilisée par la propriété Status pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_Status_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", Status_PROPERTYNAME));
						_LeadsEncapsulation.Status = p_Value;
					}


				#endregion

				#region === Propriété : RecontactOn ===

					///<summary>
					/// Méthode utilisée par la propriété RecontactOn pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_RecontactOn_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", RecontactOn_PROPERTYNAME));
						return _LeadsEncapsulation.RecontactOn;
					}

					///<summary>
					/// Méthode utilisée par la propriété RecontactOn pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_RecontactOn_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", RecontactOn_PROPERTYNAME));
						_LeadsEncapsulation.RecontactOn = p_Value;
					}


				#endregion

				#region === Propriété : Comment ===

					///<summary>
					/// Méthode utilisée par la propriété Comment pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_Comment_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", Comment_PROPERTYNAME));
						return _LeadsEncapsulation.Comment;
					}

					///<summary>
					/// Méthode utilisée par la propriété Comment pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_Comment_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", Comment_PROPERTYNAME));
						_LeadsEncapsulation.Comment = p_Value;
					}


				#endregion

				#region === Propriété : Models ===

					///<summary>
					/// Méthode utilisée par la propriété Models pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_Models_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", Models_PROPERTYNAME));
						return _LeadsEncapsulation.Models;
					}

					///<summary>
					/// Méthode utilisée par la propriété Models pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_Models_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", Models_PROPERTYNAME));
						_LeadsEncapsulation.Models = p_Value;
					}


				#endregion

				#region === Propriété : Category ===

					///<summary>
					/// Méthode utilisée par la propriété Category pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.String LeadsEncapsulation_Category_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", Category_PROPERTYNAME));
						return _LeadsEncapsulation.Category;
					}

					///<summary>
					/// Méthode utilisée par la propriété Category pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_Category_SetValue(System.String p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", Category_PROPERTYNAME));
						_LeadsEncapsulation.Category = p_Value;
					}


				#endregion

				#region === Propriété : History ===

					///<summary>
					/// Méthode utilisée par la propriété History pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.Collections.Generic.IList<MyADA.Common.Services.Models.History> LeadsEncapsulation_History_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", History_PROPERTYNAME));
						return _LeadsEncapsulation.History;
					}

					///<summary>
					/// Méthode utilisée par la propriété History pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_History_SetValue(System.Collections.Generic.IList<MyADA.Common.Services.Models.History> p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", History_PROPERTYNAME));
						_LeadsEncapsulation.History = p_Value;
					}


				#endregion

				#region === Propriété : CreatedDate ===

					///<summary>
					/// Méthode utilisée par la propriété CreatedDate pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.DateTime LeadsEncapsulation_CreatedDate_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", CreatedDate_PROPERTYNAME));
						return _LeadsEncapsulation.CreatedDate;
					}

					///<summary>
					/// Méthode utilisée par la propriété CreatedDate pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_CreatedDate_SetValue(System.DateTime p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", CreatedDate_PROPERTYNAME));
						_LeadsEncapsulation.CreatedDate = p_Value;
					}


				#endregion

				#region === Propriété : LastModified ===

					///<summary>
					/// Méthode utilisée par la propriété LastModified pour la lecture de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private System.DateTime LeadsEncapsulation_LastModified_GetValue()
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur LeadsEncapsulation.", LastModified_PROPERTYNAME));
						return _LeadsEncapsulation.LastModified;
					}

					///<summary>
					/// Méthode utilisée par la propriété LastModified pour l'affectation de la valeur corresponsande de LeadsEncapsulation.
					///</summary>
					private void LeadsEncapsulation_LastModified_SetValue(System.DateTime p_Value)
					{
						if(_LeadsEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur LeadsEncapsulation.", LastModified_PROPERTYNAME));
						_LeadsEncapsulation.LastModified = p_Value;
					}


				#endregion


			#endregion

		#endregion
	}

	public partial class LeadsInfosItemViewModel : LeadsInfosItemViewModelBase
	{
	}
}
