using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;
using MyADA.Common.DAOServices.Services;
using System.Threading.Tasks;
using MyADA.Common.Services.Models;
using System.Reflection;
using MyADA.Common.ViewModels.ItemsViewModel;
using MyADA.Common.Services.Requests;
using MyADA.Common.Services.Services;

namespace MyADA.Common.ViewModels
{
    public partial class SettingsViewModel : SettingsViewModelBase, MyADA.Common.ViewModels.ISettingsViewModel
    {
        #region ==================================== Properties ====================================
        private string Locale = Properties.Resources.CultureInfoCurrentCultureName;
        private IGlobalConfigService GlobalConfigService;

        private enum ChangeRequestStape { create, delete, update };
        private bool IsWorkingPicker = false;
        private bool IsWorkingRequest = false;

        #endregion

        #region ==================================== Initialisation ====================================
        protected override void OnInitialize()
        {
            base.OnInitialize();
            ResolveInitialisation();
        }

        private void fillInTexts()
        {

            PlaceHolderForSomeOneElsePicker = Properties.Resources.ChangeRequest_SomeoneElse_Text;
            PlaceHolderForTitlePicker = Properties.Resources.ChangeRequest_Title_Text;
            PlaceHolderForPositionPicker = Properties.Resources.ChangeRequest_Position_Text;
            PlaceHolderForOrganizationPicker = Properties.Resources.ChangeRequest_Organization_Text;
            PlaceHolderForDomainOfActionsPicker = Properties.Resources.ChangeRequest_DomainsOfAction_Text;

            DefaultPlaceHolderForSomeOneElsePicker = PlaceHolderForSomeOneElsePicker;
            DefaultPlaceHolderForTitlePicker = PlaceHolderForTitlePicker;
            DefaultPlaceHolderForPositionPicker = PlaceHolderForPositionPicker;
            DefaultPlaceHolderForOrganizationPicker = PlaceHolderForOrganizationPicker;
            DefaultPlaceHolderForDomainOfActionsPicker = PlaceHolderForDomainOfActionsPicker;


            // Picker Header Text
            HeaderTextHolderForSomeOneElsePicker = Properties.Resources.GeneralPickerHeaderText;
            HeaderTextHolderForTitlePicker = Properties.Resources.GeneralPickerHeaderText;
            HeaderTextHolderForPositionPicker = Properties.Resources.GeneralPickerHeaderText;
            HeaderTextHolderForOrganizationPicker = Properties.Resources.GeneralPickerHeaderText;
            HeaderTextHolderForDomainOfActionsPicker = Properties.Resources.GeneralPickerHeaderText;

            this.Name = string.Empty;
            this.SurName = string.Empty;
            this.Title = string.Empty;
            this.Email = string.Empty;

        }

        private void ResolveInitialisation()
        {
            GlobalConfigService = CoreSDK.Framework.Services.GlobalConfigService();
        }
        #endregion

        #region ==================================== Prepare ViewModel data ====================================
        /// <summary>
        /// Prepare ViewModel
        /// </summary>
        /// <returns></returns>
        public async Task<bool> PrepareVM(CurrentUserProfile p_CurrentUserProfile)
        {
            try
            {
                BeginWork();

                // Pickers PlaceHolder
                fillInTexts();

                // Get current user infos.
                this.CurrentUserProfileInfos = p_CurrentUserProfile;

                // Show Organization picker
                ShowRoomCollections.Clear();
                if (this.CurrentUserProfileInfos.Profile.Organization.employee)
                {
                    ShowOrganizationPicker = true;
                    var l_ShowroomService = CoreSDK.Framework.Services.Container().Resolve<IShowroomService>();
                    var l_ShowRoomList = await l_ShowroomService.GetShowroom();
                    ShowRoomCollections.AddRange(l_ShowRoomList);
                }
                else
                {
                    ShowOrganizationPicker = true;
                    ShowRoomCollections.Add(new ShowroomModels()
                    {
                        ShowroomId = CurrentUserProfileInfos.ShowroomId,
                        LocalName = CurrentUserProfileInfos.Showroom.LocalName,
                        Name = CurrentUserProfileInfos.Showroom.Name
                    });
                }

                // Show Email field
                ShowEmail = false;
                var l_Parameter = "?ShowroomId=" + p_CurrentUserProfile.ShowroomId;

                // ChangeRequest options default value
                IsChangeChecked = false;
                IsCreateChecked = false;
                IsDeleteChecked = false;
                EnableMe = true;
                EnableSomeoneElse = true;

                #region ====================================  Lists are static in WS User Service ====================================
                PositionCollections.Clear();
                DomainOfActionsCollections.Clear();
                UserTitleCollections.Clear();
                var l_UserService = CoreSDK.Framework.Services.Container().Resolve<IUserService>();
                PositionCollections.AddRange(l_UserService.GetPositionsList());



                DomainOfActionsCollections.AddRange(l_UserService.GetDomainOfActionsList().Select(x => new DomainOfActionViewModelItem().SetDomainOfActionEncapsulation(new DomainOfActionModel() { Id = x.Id, Libelle = x.Libelle, IdForView = x.IdForView })));
                UserTitleCollections.AddRange(l_UserService.GetTitlesList());

                #endregion

                #region ==================================== Users collection ====================================
                if (this.CurrentUserProfileInfos.Role.ToString() != Helper.ToolsBox.ADARole.user.ToString())
                {
                    var l_UsersList = await l_UserService.GetUsers(l_Parameter);
                    UsersCollections.AddRange(l_UsersList);
                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                EndWork();
            }
        }
        #endregion

        #region ==================================== Commands ====================================
        /// <summary>
        /// Load change request page.
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override async void OnShowChangeRequestViewCommand_Execute(object p_Parameter)
        {
            await CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(ViewName.ChangeRequestView, this);
        }

        /// <summary>
        /// Modify current user infos
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override void OnShowMyInfosCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                this.Name = CurrentUserProfileInfos.FirstName;
                this.SurName = CurrentUserProfileInfos.LastName;
                this.Title = CurrentUserProfileInfos.Title;
                this.UserId = CurrentUserProfileInfos.UserId;
            }
            finally
            {
                EndWork();
            }
        }

        /// <summary>
        /// Choose a user for changing or deleting.
        /// </summary>
        /// <param name="p_Parameter"></param>
        protected override async void OnSelectUserCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                var l_UserService = CoreSDK.Framework.Services.Container().Resolve<IUserService>();
                this.UserId = ((UserInfos)p_Parameter).UserId;
                PlaceHolderForSomeOneElsePicker = ((UserInfos)p_Parameter).FullName;
                var l_UserInfos = await l_UserService.GetUserInfos(this.UserId);
                ShowOrganizationPicker = l_UserInfos.Profile.Organization.employee ? true : false;
                this.Title = l_UserInfos.Title;
                PlaceHolderForTitlePicker = (this.Title != null) ? this.Title : "Title";
                this.Name = l_UserInfos.FirstName;
                this.SurName = l_UserInfos.LastName;
                #region ====================================== DomainOfActionsCollections traitement  for GAV=======================================

                //TypeInfo l_DomainsOfActionTypeInfo = l_UserInfos.Profile.DomainsOfAction.GetType().GetTypeInfo();
                //var l_DomainsOfAction = l_UserInfos.Profile.DomainsOfAction;
                //foreach (var item in DomainOfActionsCollections)
                //{
                //    foreach (PropertyInfo info in l_DomainsOfActionTypeInfo.DeclaredProperties)
                //    {
                //        if (item.Id.ToLower() == info.Name)
                //            if ((bool)info.GetValue(l_DomainsOfAction))
                //                item.IsSelected = true;
                //    }
                //}
                #endregion

                PositionCollections.Where(x => x.Id == l_UserInfos.FormattedProfile.Position.Id).First().IsSelected = true;
                //UserTitleCollections.Where(x => x.Id == l_UserInfos.FormattedProfile.Title.Id).First().IsSelected = true;
                //PlaceHolderForTitlePicker = (UserTitleCollections.Where(x => x.IsSelected == true).FirstOrDefault() != null) ? UserTitleCollections.Where(x => x.IsSelected == true).FirstOrDefault().Libelle : DefaultPlaceHolderForTitlePicker;

                if (!this.CurrentUserProfileInfos.Profile.Organization.employee)
                {
                    PlaceHolderForOrganizationPicker = ShowRoomCollections.FirstOrDefault().Name;
                }

                foreach (var item in l_UserInfos.FormattedProfile.DomainsOfAction)
                    DomainOfActionsCollections.Where(x => x.Id == item.Id).First().IsSelected = true;

                var DomainsSelections = string.Join(",", DomainOfActionsCollections.Where(x => x.IsSelected == true).ToList().Select(y => y.Libelle));
                PlaceHolderForDomainOfActionsPicker = (DomainsSelections.Length > 0) ? (DomainsSelections.Length > 20) ? DomainsSelections.Substring(0, 17) + "..." : DomainsSelections : DefaultPlaceHolderForDomainOfActionsPicker;
                var PositionSelections = PositionCollections.Where(x => x.IsSelected == true).FirstOrDefault();
                PlaceHolderForPositionPicker = (PositionSelections != null) ? PositionSelections.Libelle : DefaultPlaceHolderForPositionPicker;

                CoreSDK.Framework.Services.InteractionService().Close();
            }
            catch (Exception ex)
            {
                CoreSDK.Framework.Services.InteractionService().Alert("Error", ex.Message);
            }
            finally
            {
                EndWork();
            }
        }

        protected override void OnSelectUserPositionCommand_Execute(object p_Parameter)
        {
            if (IsWorkingPicker)
                return;
            try
            {
                IsWorkingPicker = true;
                BeginWork();

                var l_PositionSelection = (PositionModel)p_Parameter;

                foreach (var item in PositionCollections) { item.IsSelected = false; }

                if (l_PositionSelection != null)
                {
                    PositionCollections.Where(x => x.Id == l_PositionSelection.Id).FirstOrDefault().IsSelected = true;
                    PlaceHolderForPositionPicker = PositionCollections.Where(x => x.IsSelected == true).FirstOrDefault().Libelle;
                    IsWorkingPicker = false;
                    CoreSDK.Framework.Services.InteractionService().Close();
                }
            }
            finally
            {
                IsWorkingPicker = false;
                EndWork();
            }
        }

        protected override void OnSelectUserOrganizationCommand_Execute(object p_Parameter)
        {
            if (IsWorkingPicker)
                return;
            try
            {
                BeginWork();
                var l_Showroom = (ShowroomModels)p_Parameter;
                if (l_Showroom != null)
                {
                    IsWorkingPicker = true;
                    PlaceHolderForOrganizationPicker = ShowRoomCollections.Where(a => a.ShowroomId == l_Showroom.ShowroomId).FirstOrDefault().Name;
                    IsWorkingPicker = false;
                    CoreSDK.Framework.Services.InteractionService().Close();
                }
            }
            finally
            {
                IsWorkingPicker = false;
                EndWork();
            }
        }

        protected override async void OnSelectUserDomainOfActionsCommand_Execute(object p_Parameter)
        {
            if (IsWorkingPicker)
                return;
            try
            {
                //Faut voir comment bloquer le double click
                IsWorkingPicker = true;
                BeginWork();
                var l_NewPlaceHolder = string.Join(",", DomainOfActionsCollections.Where(x => x.IsSelected == true).ToList().Select(y => y.Libelle)); ;
                PlaceHolderForDomainOfActionsPicker = (l_NewPlaceHolder.Length > 20) ? l_NewPlaceHolder.Substring(0, 17) + "..." : l_NewPlaceHolder;
                CoreSDK.Framework.Services.InteractionService().Close();
                IsWorkingPicker = false;
            }
            finally
            {
                IsWorkingPicker = false;
                EndWork();
            }
        }

        protected override async void OnSendRequestionCommand_Execute(object p_Parameter)
        {
            if (IsWorkingRequest)
                return;
            try
            {
                IsWorkingRequest = true;
                if (!IsChanging && !IsCreation && !IsDeleting)
                    CoreSDK.Framework.Services.InteractionService().Alert("Error", Properties.Resources.ChangeRequestError);
                else
                {
                    BeginWork();
                    var l_NewUser = new ChangeRequest()
                    {
                        Title = UserTitleCollections.Where(x => x.IsSelected == true).First() != null ? UserTitleCollections.Where(x => x.IsSelected == true).First().Libelle : string.Empty,
                        FirstName = this.Name,
                        LastName = this.SurName,
                        // Domain of actions
                        ActionDomain = DomainOfActionsCollections.Where(x => x.IsSelected == true).Select(y => y.IdForView).ToArray(),
                        // Position      
                        Position = PositionCollections.Where(x => x.IsSelected == true).FirstOrDefault().IdForView,
                        ShowroomId = this.CurrentUserProfileInfos.ShowroomId,
                        Employee = false,
                        CreatedDate = 0,//Default value ==> confirmed with the developer of WS
                        LastModified = 0
                    };

                    if (GetState<ChangeRequestStape>() == ChangeRequestStape.update || GetState<ChangeRequestStape>() == ChangeRequestStape.delete)
                        l_NewUser.UserId = this.CurrentUserProfileInfos.UserId;
                    l_NewUser.Type = GetState<ChangeRequestStape>().ToString();
                    var l_ChangeRequestService = CoreSDK.Framework.Services.Container().Resolve<IChangeRequestService>();
                    var result = await l_ChangeRequestService.Create(l_NewUser);
                    if (result == true)
                    {
                        switch (GetState<ChangeRequestStape>())
                        {
                            case ChangeRequestStape.create:
                                CoreSDK.Framework.Services.InteractionService().Alert("Success", "The user was created successfully");
                                break;

                            case ChangeRequestStape.update:
                                CoreSDK.Framework.Services.InteractionService().Alert("Success", "The user has been updated successfully");
                                break;

                            case ChangeRequestStape.delete:
                                CoreSDK.Framework.Services.InteractionService().Alert("Success", "The user was deleted successfully");
                                break;
                            default:
                                break;
                        }
                    }
                    IsWorkingRequest = false;
                }
            }
            catch (Exception ex)
            {
                IsWorkingRequest = false;
                CoreSDK.Framework.Services.InteractionService().Alert("Error", ex.Message);
            }
            finally
            {
                IsWorkingRequest = false;
                EndWork();
            }

        }

        protected override void OnChangeCommand_Execute(object p_Parameter)
        {
            if (p_Parameter != null)
            {

                if (Convert.ToBoolean(p_Parameter))
                {
                    if (IsCreation)
                        IsCreation = false;

                    if (IsDeleting)
                        IsDeleting = false;
                }
                if (Convert.ToBoolean(p_Parameter) == true)
                {
                    SetState<ChangeRequestStape>(ChangeRequestStape.update);
                }
                //ShowEmail = false;
                // Pickers PlaceHolder
                fillInTexts();
            }
        }

        protected override void OnCreateCommand_Execute(object p_Parameter)
        {
            if (p_Parameter != null)
            {

                if (Convert.ToBoolean(p_Parameter))
                {
                    if (IsChanging)
                        IsChanging = false;

                    if (IsDeleting)
                        IsDeleting = false;
                }
                if (Convert.ToBoolean(p_Parameter) == true)
                {
                    DisableSomeOneElse = true;
                    DisableMe = true;
                    SetState<ChangeRequestStape>(ChangeRequestStape.create);
                }
                //ShowEmail = true;
                // Pickers PlaceHolder
                fillInTexts();
            }
        }

        protected override void OnDeleteCommand_Execute(object p_Parameter)
        {
            if (p_Parameter != null)
            {

                if (Convert.ToBoolean(p_Parameter))
                {
                    if (IsChanging)
                        IsChanging = false;

                    if (IsCreation)
                        IsCreation = false;
                }

                if (Convert.ToBoolean(p_Parameter) == true)
                {
                    DisableMe = true;
                    SetState<ChangeRequestStape>(ChangeRequestStape.delete);
                }
                //ShowEmail = false;
                // Pickers PlaceHolder
                fillInTexts();
            }
        }

        protected override void OnChangeRequestCommand_Execute(object p_Parameter)
        {
            IsChangeChecked = true;
            IsCreateChecked = false;
            IsDeleteChecked = false;
            EnableMe = true;
            EnableSomeoneElse = true;
            ShowEmail = false;
            fillInTexts();
        }

        protected override void OnCreateRequestCommand_Execute(object p_Parameter)
        {
            IsChangeChecked = false;
            IsCreateChecked = true;
            IsDeleteChecked = false;
            EnableMe = false;
            EnableSomeoneElse = false;
            ShowEmail = true;
            fillInTexts();
        }

        protected override void OnDeleteRequestCommand_Execute(object p_Parameter)
        {
            IsChangeChecked = false;
            IsCreateChecked = false;
            IsDeleteChecked = true;
            EnableMe = true;
            EnableSomeoneElse = true;
            ShowEmail = false;
            fillInTexts();
        }

        protected override void OnSelectTitleCommand_Execute(object p_Parameter)
        {
            if (IsWorkingPicker)
                return;
            try
            {
                BeginWork();
                var l_UserTitle = (UserTitle)p_Parameter;
                if (l_UserTitle != null)
                {
                    IsWorkingPicker = true;
                    this.Title = ((UserTitle)p_Parameter).Libelle;
                    UserTitleCollections.Where(x => x.Id == l_UserTitle.Id).FirstOrDefault().IsSelected = true;
                    PlaceHolderForTitlePicker = this.Title;
                    IsWorkingPicker = false;
                    CoreSDK.Framework.Services.InteractionService().Close();
                }
            }
            finally
            {
                IsWorkingPicker = false;
                EndWork();
            }
        }

        #endregion

        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }
    }
}
