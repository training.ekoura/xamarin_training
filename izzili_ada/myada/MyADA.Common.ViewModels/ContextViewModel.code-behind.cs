
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;
using System.Threading.Tasks;
using MyADA.Common.DAOServices.Requests;
using MyADA.Common.DAOServices.Services;

namespace MyADA.Common.ViewModels
{
    public partial class ContextViewModel : ContextViewModelBase, IContextViewModel
    {
        #region === Propriétés ===
        private string Locale = Properties.Resources.CultureInfoCurrentCultureName;
        #endregion

        protected override void OnInitialize()
        {
            base.OnInitialize();

        }

        public async Task AppStarter()
        {
           

        }

        public async Task RefreshDashboard()
        {
        }
    }
}
