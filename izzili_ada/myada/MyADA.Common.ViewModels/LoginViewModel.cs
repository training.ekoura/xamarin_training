﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML LoginViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  CoreSDK.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CoreSDK;

namespace MyADA.Common.ViewModels
{
	public abstract partial class LoginViewModelBase : CoreSDK.ViewModel
	{
		protected override void OnInitialize()
		{
			base.OnInitialize();

			// Initialisation de la commande LoginCommand.
			LoginCommand = new DelegateCommand(LoginCommand_CanExecute, LoginCommand_Execute);
			// Initialisation de la commande OpenGeneralConditionsCommand.
			OpenGeneralConditionsCommand = new DelegateCommand(OpenGeneralConditionsCommand_CanExecute, OpenGeneralConditionsCommand_Execute);
			// Initialisation de la commande LangugeSelectionCommand.
			LangugeSelectionCommand = new DelegateCommand(LangugeSelectionCommand_CanExecute, LangugeSelectionCommand_Execute);
		}

		#region === Propriétés ===

			#region === Propriété : Email ===

				public const string Email_PROPERTYNAME = "Email";

				private string _Email;
				///<summary>
				/// Propriété : Email
				///</summary>
				public string Email
				{
					get
					{
						return GetValue<string>(() => _Email);
					}
					set
					{
						SetValue<string>(() => _Email, (v) => _Email = v, value, Email_PROPERTYNAME,  DoEmailBeforeSet, DoEmailAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoEmailBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoEmailAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyEmailDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LoginCommand ===

				public const string LoginCommand_PROPERTYNAME = "LoginCommand";

				private IDelegateCommand _LoginCommand;
				///<summary>
				/// Propriété : LoginCommand
				///</summary>
				public IDelegateCommand LoginCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _LoginCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _LoginCommand, (v) => _LoginCommand = v, value, LoginCommand_PROPERTYNAME,  DoLoginCommandBeforeSet, DoLoginCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLoginCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLoginCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLoginCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : OpenGeneralConditionsCommand ===

				public const string OpenGeneralConditionsCommand_PROPERTYNAME = "OpenGeneralConditionsCommand";

				private IDelegateCommand _OpenGeneralConditionsCommand;
				///<summary>
				/// Propriété : OpenGeneralConditionsCommand
				///</summary>
				public IDelegateCommand OpenGeneralConditionsCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _OpenGeneralConditionsCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _OpenGeneralConditionsCommand, (v) => _OpenGeneralConditionsCommand = v, value, OpenGeneralConditionsCommand_PROPERTYNAME,  DoOpenGeneralConditionsCommandBeforeSet, DoOpenGeneralConditionsCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoOpenGeneralConditionsCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoOpenGeneralConditionsCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyOpenGeneralConditionsCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LangugeSelectionCommand ===

				public const string LangugeSelectionCommand_PROPERTYNAME = "LangugeSelectionCommand";

				private IDelegateCommand _LangugeSelectionCommand;
				///<summary>
				/// Propriété : LangugeSelectionCommand
				///</summary>
				public IDelegateCommand LangugeSelectionCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _LangugeSelectionCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _LangugeSelectionCommand, (v) => _LangugeSelectionCommand = v, value, LangugeSelectionCommand_PROPERTYNAME,  DoLangugeSelectionCommandBeforeSet, DoLangugeSelectionCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLangugeSelectionCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLangugeSelectionCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLangugeSelectionCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux commandes ===

			// Notez que les propriétés de commandes sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Commande : LoginCommand ===


				private bool LoginCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnLoginCommand_CanExecute(l_Parameter);
				}

				private void LoginCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnLoginCommand_Execute(l_Parameter);
				}

				protected virtual bool OnLoginCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnLoginCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : OpenGeneralConditionsCommand ===


				private bool OpenGeneralConditionsCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnOpenGeneralConditionsCommand_CanExecute(l_Parameter);
				}

				private void OpenGeneralConditionsCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnOpenGeneralConditionsCommand_Execute(l_Parameter);
				}

				protected virtual bool OnOpenGeneralConditionsCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnOpenGeneralConditionsCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : LangugeSelectionCommand ===


				private bool LangugeSelectionCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnLangugeSelectionCommand_CanExecute(l_Parameter);
				}

				private void LangugeSelectionCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnLangugeSelectionCommand_Execute(l_Parameter);
				}

				protected virtual bool OnLangugeSelectionCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnLangugeSelectionCommand_Execute(object p_Parameter);

			#endregion


		#endregion

	}

	public partial class LoginViewModel : LoginViewModelBase
	{
	}
}
