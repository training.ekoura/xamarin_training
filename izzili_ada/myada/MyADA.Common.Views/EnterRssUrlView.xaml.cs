﻿using CoreSDK;
using MyADA.Common.DAOServices.Services;
using MyADA.Common.ViewModels;
using MyADA.Common.Views.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyADA.Common.Views
{
    public partial class EnterRssUrlView : ContentPage, IEnterRssUrlView
    {
        public static string _labelAction = "_labelAction";
        public EnterRssUrlView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);
        }

        private async void backButton_Tapped(object sender, EventArgs e)
        {
            CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(MyADA.Common.ViewModels.ViewName.MyRSSView, BindingContext);
        }

        public void OnCellClicked(object sender, EventArgs e)
        {
            var b = (Button)sender;
            var t = b.CommandParameter;
            if(BindingContext != null) ((BurgerViewModel)BindingContext).AddOrRemoveRssFeedCommand.Execute(t);
        }

        public void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return; // has been set to null, do not 'process' tapped event*
            ((ListView)sender).SelectedItem = 0;
        }
    }
}
