﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;
using Xamarin.Forms;

using MyADA.Common.Views;
using MyADA.Common.Views.Interfaces;
using MyADA.Common.ViewModels;

namespace MyADA.Common.Views
{
    public partial class HomeView : ContentPage, IHomeView
    {
        public HomeView()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);
            BindingContext = CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>();
        }
    }
}
