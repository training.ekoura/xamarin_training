﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;
using MyADA.Common.Views.Interfaces;

using Xamarin.Forms;
using MyADA.Common.ViewModels;

namespace MyADA.Common.Views
{
    public partial class BookLeadsView : ContentPage, IBookLeadsView
    {
        public BookLeadsView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);
            //if(repeaterNewLeads.Children.Count == 0) lbTitleRepeater.Text = "No new leads available";
        }

        private void MyLeadsButton_Tapped(object sender, EventArgs e)
        {

        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (BindingContext != null)
            {
                ((LeadViewModel)BindingContext).RazFilter();
            }
            CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>().IsBusy = false;
            CoreSDK.Framework.Services.Container().Resolve<ILeadViewModel>().IsBusy = false;
            CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>().IsBusy = false;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>().IsBusy = false;
            CoreSDK.Framework.Services.Container().Resolve<ILeadViewModel>().IsBusy = false;
            CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>().IsBusy = false;
        }

    }
}
