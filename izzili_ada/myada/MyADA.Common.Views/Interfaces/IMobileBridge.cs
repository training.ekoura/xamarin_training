﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace MyADA.Common.Views
{
	public interface IMobileBridge
	{
		/*
			* Init Library with key/secret you obtained during registration with MobileBridge
			*/
		void MB_InitLibrary(string clientKey, string clientSecret);


		////////////////////////////// Data //////////////////////////////

		/*
	    /**
	    * Get all inbox data
	    */
		void MB_GetInboxData(MBXamarinFormsTrainingAppPage.InboxDataFinished _delegate);

		/*
	    /**
	    * Delete all inbox data
	    */
		void MB_DeleteInboxData(MBXamarinFormsTrainingAppPage.InboxDataFinished _delegate);

		/*
	    /**
	    * Delete all inbox data
	    */
		void MB_DeleteInboxItem(string inbox_id, MBXamarinFormsTrainingAppPage.InboxDataFinished _delegate);

		/**
		 * Requests locations data
		 *
		 * @param tagFilter : an array of tags for filtering locations list
		 */
		string MB_GetLocationsData(string tagFilter);


		/**
		 * Requests public promotions data
		 */
		void MB_GetPublicPromotionsData(MBXamarinFormsTrainingAppPage.MyDelegate _delegate);

		/**
		 * Requests saved promotions data
		 *
		 * @param type: active/used/all
		 */
		string MB_GetSavedPromotionsData(int type);


		void MB_GetRichMessageData(string assetInfo, MBXamarinFormsTrainingAppPage.RichMessageDelegate _delegate);
		void MB_GetAllRichMessages(MBXamarinFormsTrainingAppPage.RichMessageDelegate _delegate);

		////////////////////////////// Cache update //////////////////////////////

		void MB_UpdateCacheCustomerAtribute(MBXamarinFormsTrainingAppPage.CacheUpdateDelegate _delegate);


		////////////////////////////// Loyalty  //////////////////////////////

		/**
		 * Requests points
		 */
		string MB_GetPoints();

		/**
		 * Sets loyalty points
		 *
		 * @param points: amount of loyalty points to set
		 */
		string MB_SetPoints(string points);

		/**
		 * Requests loyalty points history
		 */
		string MB_GetPointsHistory();

		/**
		 * Triggers custom point event
		 *
		 * @param customEvent: event name
		 */
		bool MB_CustomPointEvent(string customEvent);

		/**
		 * Gets location notifications status (used for geo-targeting)
		 */

		////////////////////////////// Settings / Utilities //////////////////////////////

		bool MB_GetLocationReminderStatus();

		/**
		 * Enables or disables location notifications (used for geo-targeting)
		 *
		 * @param enabled Notifications triggered:
		 *                com.mobilebridge.MB_SettingEventReply - notification for passing messages and status changes.
		 */
		void MB_SetLocationReminderEnabled(bool enabled);


		/**
		 * Gets beacon notifications status (used for geo-targeting)
		 */
		bool MB_GetBeaconReminderStatus();

		/**
		 * Enables or disables location notifications (used for geo-targeting)
		 *
		 * @param enabled Notifications triggered:
		 *                com.mobilebridge.MB_SettingEventReply - notification for passing messages and status changes.
		 */
		void MB_SetBeaconReminderEnabled(bool enabled);

		/**
		 * Enables or disables notifications
		 *
		 * @param enable
		 * @param showAlert Notifications triggered:
		 *                  com.mobilebridge.MB_SettingEventReply - notification for passing messages and status changes.
		 */
		void MB_SetNotificationEnabled(bool enabled);

		/**
		 * Retrieves the app's notification status.
		 */
		bool MB_GetNotificationStatus();


		////////////////////////////// Test Purposes //////////////////////////////

		/**
		 * Adds users to a test group.
		 * This group can be used to send notifications to users in the group.
		 *
		 * @params firstName, lastName
		 */
		void MB_JoinTestGroup(String firstName, String lastName);


		/**
		 * Removes user from test group
		 *
		 * @params firstName, lastName
		 */
		void MB_LeaveTestGroup(string firstName, string lastName);

		/**
		 * Clears user history
		 * Deletes the user history on the MobileBridge server. This includes, for example, the promotions viewed or the polls that were answered.
		 * This methods enables you to clear the history programmatically, as part of the process of debugging your app.
		 * IMPORTANT: aims to be used ONLY for test purposes, don't use on "production"
		 */
		bool MB_ClearUserHistory();


		////////////////////////////// Analytics //////////////////////////////

		/**
		 * Logs the event of a user click, including the time-stamp of the event.
		 *
		 * @params key: the key of your choice, identifying the click event.
		 */
		void MB_ClickTraceLog(string key);

		/**
		 * Starts logging view trace, date and time that the user enters a screen.
		 *
		 * @params key: the key of your choice, identifying the page.
		 */
		void MB_ViewTraceLog_Start(string key);

		/**
		 * Stops logging view trace
		 *
		 * @params key: unique string identifier you used to start view trace
		 */
		void MB_ViewTraceLog_Stop(string key);


		////////////////////////////// Assets //////////////////////////////

		/**
		 * Return asset view:
		 * Promotion, rich message, survey, poll
		 *
		 * @params data: asset's data. Example {"id": "123"}
		 */
		Page MB_GetAssetView(string data);

		/*
	* Shows banner view
	* TODO: @param viewName, tag
	*/
		Page MB_ShowBannerView();



		////////////////////////////// Custom Attribute //////////////////////////////
		void MB_CustomerAttributeSetInt(int value, string key);
		void MB_CustomerAttributeSetTime(long value, string key);
		void MB_CustomerAttributeSetDate( long value, string key);
		void MB_CustomerAttributeSetText(string value, string key);
		void MB_CustomerAttributeSetMultiSelect(List<string> value, string key);
		void MB_CustomerAttributeSetSingleSelect(string value, string key);
		void MB_CustomerAttributeSetBool(Boolean value, string key);
		void MB_CustomerAttributeSetList(List<string> value, string key);

		void MB_GetCustomerAttribute(MBXamarinFormsTrainingAppPage.CustomerAttributeDelegate _delegate);
		void MB_SetCustomerAttribute(MBXamarinFormsTrainingAppPage.CustomerAttributeDelegate _delegate);
		void MB_ClearCustomerAttribute(MBXamarinFormsTrainingAppPage.CustomerAttributeDelegate _delegate);

		////////////////////////////// TOKEN //////////////////////////////
		/**
		* Send token to MobileBridge server. 
	    * push_type = 0 - APNS, push_type = 1 - Baidu
	    * (iOS will send APNS in amy case)                               
		*/
		void MB_SetToken(string token);


		/// //////////////// DEVICE CONTEXT  ///////////////
		string MB_GetDeviceContext();
	}
}

