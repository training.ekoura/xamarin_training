﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;
using MyADA.Common.Views.Interfaces;
using MyADA.Common.ViewModels;
using Xamarin.Forms;

namespace MyADA.Common.Views
{
    public partial class NavigateLeadsView : ContentPage, INavigateLeadsView
    {
        public NavigateLeadsView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            var l_HomeViewModel = CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>();
            l_HomeViewModel.IsBusy = false;
            var l_LeadViewModel = CoreSDK.Framework.Services.Container().Resolve<ILeadViewModel>();
            l_LeadViewModel.IsBusy = false;
            var l_BurgerViewModel = CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>();
            l_BurgerViewModel.IsBusy = false;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            var l_HomeViewModel = CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>();
            l_HomeViewModel.IsBusy = false;
            var l_LeadViewModel = CoreSDK.Framework.Services.Container().Resolve<ILeadViewModel>();
            l_LeadViewModel.IsBusy = false;
            var l_BurgerViewModel = CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>();
            l_BurgerViewModel.IsBusy = false;
        }
    }
}
