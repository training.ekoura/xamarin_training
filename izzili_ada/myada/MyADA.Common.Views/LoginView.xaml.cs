﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;
using Xamarin.Forms;

using MyADA.Common.Views;
using MyADA.Common.Views.Interfaces;
using MyADA.Common.ViewModels;

namespace MyADA.Common.Views
{
    public partial class LoginView : ContentPage, ILoginView
    {
        public LoginView()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            var data = CoreSDK.Framework.Services.Container().Resolve<ILoginViewModel>();
            BindingContext = data;
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);
        }

        public async void Clicked_English(object sender, EventArgs e)
        {
            var GlobalConfigService = CoreSDK.Framework.Services.GlobalConfigService();
            GlobalConfigService.Data.CultureInfo = "";
            Views.Properties.Resources.Culture = new System.Globalization.CultureInfo(GlobalConfigService.Data.CultureInfo);
            ViewModels.Properties.Resources.Culture = new System.Globalization.CultureInfo(GlobalConfigService.Data.CultureInfo);
            await CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Alert_ChangeLanguage_Title, Properties.Resources.Alert_ChangeLanguage_Message, 3, AlertType.Success);
        }

        public async void Clicked_Korean(object sender, EventArgs e)
        {
            var GlobalConfigService = CoreSDK.Framework.Services.GlobalConfigService();
            GlobalConfigService.Data.CultureInfo = ViewModels.Constants.KoreanCultureInfo;
            Views.Properties.Resources.Culture = new System.Globalization.CultureInfo(GlobalConfigService.Data.CultureInfo);
            ViewModels.Properties.Resources.Culture = new System.Globalization.CultureInfo(GlobalConfigService.Data.CultureInfo);
            await CoreSDK.Framework.Services.InteractionService().Alert(Properties.Resources.Alert_ChangeLanguage_Title, Properties.Resources.Alert_ChangeLanguage_Message, 3, AlertType.Success);
        }

        public async void Clicked_ConditionsLink(object sender, EventArgs e)
        {
            var _BurgeurVM = CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>();
            _BurgeurVM.ShowAboutViewCommand.Execute(null);
        }

        protected override bool OnBackButtonPressed()
        {
            if (Device.OS == TargetPlatform.Android || Device.OS == TargetPlatform.WinPhone)
            {
                return true;
            }
            else
                return base.OnBackButtonPressed();
        }
        
    }
}
