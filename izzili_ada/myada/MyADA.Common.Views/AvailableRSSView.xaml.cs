﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyADA.Common.Views.Interfaces;

using Xamarin.Forms;

using CoreSDK;

namespace MyADA.Common.Views
{
    public partial class AvailableRSSView : ContentPage, IAvailableRSSView
    {
        public AvailableRSSView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);

            FillRepeater();
        }

        private void backButton_Tapped(object sender, EventArgs e)
        {
            CoreSDK.Framework.Services.InteractionService().Close();
        }

        private void SaveButton_Tapped(object sender, EventArgs e)
        {

        }

        private void FillRepeater()
        {
            string str = "";

            List<string> list = new List<string> { str, str, str };

            repeaterAvailableRSS.ItemsSource = list;
        }
    }
}
