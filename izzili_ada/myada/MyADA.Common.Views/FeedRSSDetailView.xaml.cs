﻿using CoreSDK;
using MyADA.Common.Views.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyADA.Common.Views
{
    public partial class FeedRSSDetailView : ContentPage, IFeedRSSDetailView
    {
        public FeedRSSDetailView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);
        }
    }
}
