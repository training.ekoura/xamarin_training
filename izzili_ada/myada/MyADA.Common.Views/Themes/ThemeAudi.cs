﻿using MyADA.Common.Themes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyADA.Common.Themes
{
    public class ThemeAudi
    {
        public static void ThemeCallback(ResourceDictionary p_ResourceDictionary)
        {
            Theme.Apply(p_ResourceDictionary, new ThemeParameters()
            {
                ThemeExtension = "",
                ConnexionText = "My Sodexo",

                #region Gerant

                GerantMainColor = Color.FromHex("#C10418"),
                GerantSecondaryColor = Color.FromHex("#241C4B"),
                GerantPageBackgroundColor = Color.FromHex("#F2F2F2"),
                GerantSecondaryPageBackgroundColor = Color.White,
                PrimaryTextColor = Color.White,
                SecondaryTextColor = Color.Black,
                TertiaryTextColor = Color.Gray,
                RepeaterColor = Color.White,
                PopinBackgroundColor = Color.White,
                RoundedButtonColor = Color.FromHex("#C30D21"),
                FixedButtoncolor = Color.FromHex("#F2F2F2"),

                #endregion

                #region ADA

                BackButtonBackgroundColor = Color.FromHex("#252A2D"),        // Noir clair
                BurgerButtonBorderColor = Color.Transparent,
                BurgerButtonBackgroundColor = Color.Transparent,
                BurgerTitleSeparatorsColor = Color.FromHex("#6D7275"),        // Gris moyen
                BurgerTitleBackgroundColor = Color.FromHex("#252A2D"),        // Noir clair
                BurgerItemSeparatorsColor = Color.FromHex("#42474B"),        // Gris foncé
                BurgerItemBackgroundColor = Color.FromHex("#2E3337"),        // Noir/Gris
                BurgerTextColor = Color.White,
                HeaderBackgroundColor = Color.FromHex("#EAEEF1"),        // Gris très clair
                HeaderTextColor = Color.Black,
                BoxTitleBackgroundColor = Color.Black,
                BoxTitleSecondaryBackgroundColor = Color.FromHex("#AC132F"),        // Rouge pâle
                BoxTitleTextColor = Color.White,
                BoxItemBackgroundColor = Color.FromRgba(255, 255, 255, 0.5), // Blanc avec transparence
                MBBoxItemBackgroundColor = Color.FromRgba(127, 127, 127, 0.9),
                BoxItemBorderColor = Color.FromHex("#E5E5E5"),        // Gris clair
                BoxItemTextColor = Color.Black,
                BoxItemSecondaryTextColor = Color.Red,
                SettingsBackgroundColor = Color.FromHex("#252A2D"),        // Noir clair
                LoginTextColor = Color.White,
                SettingsTextColor = Color.White,
                SendTextColor = Color.White,
                FixedButtonBackgroundColor = Color.FromHex("#25292D"),
                FixedButtonTextColor = Color.White,
                FieldHeaderTextColor = Color.Black,
                PickerTextColor = Color.Black,
                AdaPickerBackgroundColor = Color.FromHex("#F7F7F7"),
                AdaPickerMeBackgroundColor = Color.FromHex("#E4E2E2"),
                AdaPickerSomeoneElseBackgroundColor = Color.FromHex("#DEDCDC"),
                DarkPopinBackgroundColor = Color.FromRgba(0, 0, 0, 0.8),       // Noir avec transparence
                DarkPopinTextColor = Color.White,
                BoxRSSBackgroundColor = Color.FromHex("#EEEEEE"),
                AffectedLeadBackgroundColor = Color.FromHex("#E9EFF1"),        // Gris très clair
                HomeDisabledCommandTextColor = Color.FromHex("#A7ACAF"),
                BurgerDisabledCommandTextColor = Color.FromHex("#6D7579"),
                EntryPlaceholderColor = Color.Gray

                #endregion
            });
        }
    }
}
