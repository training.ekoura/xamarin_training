﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using CoreSDK.Controls;
using CoreSDK;
using MyADA.Common.Views.ContentViews;

namespace MyADA.Common.Themes
{
    /// <summary>
    /// Nous utilisons une classe de paramètres pour centraliser et normaliser les thèmes.
    /// Sans cette logique, toute différence de délcaration dans un thème pourrait avoir
    /// des effets de bord lors du passage d'un thème à l'autre.
    /// </summary>
    public class ThemeParameters
    {
        private const string ND = "[NOT DEFINED]";
        public const string ImageDirectory = "MySodexo.Common.Views.";
        public const string BackgroundImageLocation = "MySodexo.Common.Views.Images.BackGroundImage";
        public const string Logo = "Logo";
        public const string BurgerFooter = "BurgerFooter";
        public const string Back = "back";
        public const string Menu = "Menu";
        public const string News = "News";
        public const string CreerBars = "CreerBars";

        public ThemeParameters()
        {
            HeaderLabelBackgroundColor = Color.Black;
            ThemeMaincolor = Color.White;
        }

        #region MY RESTAU

        public string ThemeExtension { get; set; }
        public Color TypeRecetteColor { get; set; }
        public Color TypeRecetteLabelStackBackgroundColor { get; set; }
        public Color TypeRecetteListStackBackgroundColor { get; set; }
        public Color ThemeMaincolor { get; set; }
        public Color CartButtonBackColor { get; set; }
        public Color UnderLineZrColor { get; set; }
        public string ConnexionText { get; set; }
        public string MenuListSideImage { get; set; }
        public Color HeaderLabelBackgroundColor { get; set; }
        public Color HomePageFormBackgroundColor { get; set; }
        public Color CarrouselDividerColor { get; set; }
        public Color HeaderBarLabelTextColor { get; set; }
        public Color NewsTitleTextColor { get; set; }
        public Color OtherPageFormBackgroundColor { get; set; }
        public Color HomePageLogoBackgroundColor { get; set; }
        public Color SecbuttonBackgroundColor { get; set; }
        public Color MainButtonBackgroundColor { get; set; }
        public Color MenuScrollViewBackgroundColor { get; set; }
        public double MenuScrollViewBackgroundColorOpacity { get; set; }
        public Color TetiaryButtonTextColor { get; set; }
        public Color TextColor { get; set; }
        public Color EntryTextColor { get; set; }
        public Color ListViewTextColor { get; set; }
        public Color CreationPageUnderBarLabel { get; set; }
        public Color CreationPageUnderBarLabelBottom { get; set; }
        public string BackGroundImage { get; set; }
        public double StackOpacity { get; set; }
        public Color ListBackGroundColor { get; set; }
        public double FooterImageMultiplier { get; set; }
        public double HeaderLabelOpacity { get; set; }
        public Color BadgeStack1Color { get; set; }
        public Color BadgeStack2Color { get; set; }
        public Color NewsUnderImageBarColor { get; set; }
        public Color BadgeAmountColor { get; set; }
        public Color BadgeTextColor { get; set; }
        public Color BadgeSelectedAmountColor { get; set; }
        public Color BadgeSelectedBackgroundColor { get; set; }
        public Color BadgeDividerColor { get; set; }
        public Color BadgeListSeperatorColor { get; set; }
        public Color ParameterDividerLabelColor { get; set; }
        public Color ServiceViewHeaderColor { get; set; }
        public double ServiceViewStackOpacity { get; set; }
        public Color ServiceViewHeaderTextColor { get; set; }
        public Color ServiceViewPrimaryColor { get; set; }
        public Color ServiceViewSecondaryColor { get; set; }
        public Color MenuViewMainHeaderColor { get; set; }
        public double MenuViewMainHeaderOpacity { get; set; }
        public Color MenuViewRecetteNomColor { get; set; }
        public Color l_ComposanteLayoutColor { get; set; }
        public Color AllergeneTextColor { get; set; }
        public Color BadgeBorderColor { get; set; }

        public Color HamburgerBackGroundColor { get; set; }
        public Color HamburgerFavorisAndAjouterBackGroundColor { get; set; }
        public Color HamburgerFavorisAndAjouterSeperatorColor { get; set; }
        public Color HamburgerListViewLabelAndLabelColor { get; set; }
        public Color CreationListViewSeperatorColor { get; set; }
        public Color BurgerStatusBarColor { get; set; }

        public Color NewMenuViewMainHeaderColor { get; set; }
        public double NewMenuViewMainHeaderOpacity { get; set; }
        public Color NewMenuViewSwipeOptionBackgroundColor { get; set; }
        public double NewMenuViewSwipeOptionOpacity { get; set; }
        public Color UnderBarLineColor { get; set; }

        public Color NewMenuViewDateListViewTextColor { get; set; }
        public Color NewMenuViewDateListViewStatusTextColor { get; set; }
        public Color NewMenuViewDateListViewSeperatorBackgroundColor { get; set; }
        public Color NewMenuViewVoirLaCarteBackgroundColor { get; set; }
        public Color NewMenuViewButtomNotificationBackgroundColor { get; set; }
        public Color NewMenuViewButtomNotificationTextColor { get; set; }
        public Color OrderNotificationTextColor { get; set; }
        public Color OrderNotificationDateTextColor { get; set; }
        public Color OrderProcessButtonTextColor { get; set; }
        public Color OrderProcessItemHeaderTextColor { get; set; }
        public Color OrderProcessPopUpHeaderTitleTextColor { get; set; }
        public Color OrderProcessItemPlusTextColor { get; set; }
        public Color OrderProcessLabelTextColor { get; set; }
        public Color OrderNotificationBackgroundColor { get; set; }
        public Color RecetteDetailUnderBarColor { get; set; }
        public Color RecetteDetailTitleStackBackGroundColor { get; set; }
        public Color RecetteDetailHeaderTitleColor { get; set; }
        public Color RecetteDetailSectionStackBackGroundColor { get; set; }
        public Color allergenConcerneColor { get; set; }
        public Color allergenNonConcerneColor { get; set; }


        #region Gerant

        public Color GerantMainColor { get; set; }
        public Color GerantSecondaryColor { get; set; }
        public Color GerantPageBackgroundColor { get; set; }
        public Color GerantSecondaryPageBackgroundColor { get; set; }
        public Color PrimaryTextColor { get; set; }
        public Color SecondaryTextColor { get; set; }
        public Color TertiaryTextColor { get; set; }
        public Color RepeaterColor { get; set; }
        public Color PopinBackgroundColor { get; set; }
        public Color FixedButtoncolor { get; set; }
        public Color RoundedButtonColor { get; set; }

        #endregion

        #endregion

        #region ADA

        public Color BackButtonBackgroundColor { get; set; }
        public Color BurgerButtonBackgroundColor { get; set; }
        public Color BurgerButtonBorderColor { get; set; }
        public Color BurgerItemBackgroundColor { get; set; }
        public Color BurgerItemSeparatorsColor { get; set; }
        public Color BurgerTextColor { get; set; }
        public Color BurgerTitleBackgroundColor { get; set; }
        public Color BurgerTitleSeparatorsColor { get; set; }
        public Color BoxItemBackgroundColor { get; set; }
        public Color MBBoxItemBackgroundColor { get; set; }
        public Color BoxItemBorderColor { get; set; }
        public Color BoxItemSecondaryTextColor { get; set; }
        public Color BoxItemTextColor { get; set; }
        public Color BoxTitleBackgroundColor { get; set; }
        public Color BoxTitleSecondaryBackgroundColor { get; set; }
        public Color BoxTitleTextColor { get; set; }
        public Color DoubleArrowedGridColor { get; set; }
        public Color FieldHeaderTextColor { get; set; }
        public Color FixedButtonBackgroundColor { get; set; }
        public Color FixedButtonTextColor { get; set; }
        public Color HeaderBackgroundColor { get; set; }
        public Color HeaderTextColor { get; set; }
        public Color LoginTextColor { get; set; }
        public Color PickerTextColor { get; set; }
        public Color SendTextColor { get; set; }
        public Color SettingsBackgroundColor { get; set; }
        public Color SettingsTextColor { get; set; }
        public Color AdaPickerBackgroundColor { get; set; }
        public Color AdaPickerMeBackgroundColor { get; set; }
        public Color AdaPickerSomeoneElseBackgroundColor { get; set; }
        public Color DarkPopinBackgroundColor { get; set; }
        public Color DarkPopinTextColor { get; set; }
        public Color BoxRSSBackgroundColor { get; set; }
        public Color AffectedLeadBackgroundColor { get; set; }
        public Color HomeDisabledCommandTextColor { get; set; }
        public Color BurgerDisabledCommandTextColor { get; set; }
        public Color EntryPlaceholderColor { get; set; }

        #endregion
    }


    /// <summary>Cette classe et cette méthode existe pour les même raisons qu'exprimées ci-dessus.</summary>
    public static class Theme
    {
        #region ==ResourceKeys==

        #region ANCIEN MAIS GENERE DES ERREURS SI ENLEVES

        public const string BackGroundImage = "BackGroundImage";
        public const string MENUS_DATEPANEL_COLOR = "MenusDatePanelColor";
        public const string MENUS_DATEPANEL_BACKCOLOR = "MenusDatePanelBackColor";
        public const string MENUS_PREVIOUS_IMAGESOURCE = "MenusPreviousImageSource";
        public const string CHECKBOX_OFF_IMAGESOURCE = "Checkboxoffimagesource";
        public const string CHECKBOX_ON_IMAGESOURCE = "Checkboxonimagesource";
        public const string MENUS_NEXT_IMAGESOURCE = "MenusNextImageSource";
        public const string MENUS_MENUPANEL_COLOR = "MenusMenuPanelColor";
        public const string MENUS_MENUPANEL_BACKCOLOR = "MenusMenuDatePanelBackColor";
        public const string MENUS_RECETTENOM_COLOR = "MENUS_RECETTENOM_COLOR";
        public const string MENUS_LabelComposante_COLOR = "MENUS_LabelComposante_COLOR";
        public const string GridUp = "GridUp";
        public const string notifImage = "notifImage";
        public const string GerantPageBackgroundColor = "GerantPageBackgroundColor";
        public const string GerantSecondaryPageBackgroundColor = "GerantSecondaryPageBackgroundColor";
        public const string GerantMainColor = "GerantMainColor";
        public const string GerantSecondaryColor = "GerantSecondaryColor";
        public const string RepeaterColor = "RepeaterColor";
        public const string PrimaryTextColor = "PrimaryTextColor";
        public const string SecondaryTextColor = "SecondaryTextColor";
        public const string TertiaryTextColor = "TertiaryTextColor";
        public const string PopinBackgroundColor = "PopinBackgroundColor";
        public const string OrderListViewItemHeight = "OrderListViewItemHeight";

        #endregion

        #region ADA

        public const string PickerBackImageBoxHeight = "PickerBackImageBoxHeight";
        public const string PickerBackImageHeight = "PickerBackImageHeight";

        public const string BUTTON_FIXED_HEIGHT = "BUTTON_FIXED_HEIGHT";
        public const string BORDER_BOX_HEIGHT = "BORDER_BOX_HEIGHT";
        public const string BUTTON_BOTTOM_WIDTH = "BUTTON_BOTTOM_WIDTH";
        public const string LEADITEM_GRID_WIDTH = "LEADITEM_GRID_WIDTH";
        public const string TITLE_HEIGHT = "TITLE_HEIGHT";
        public const string BACKBUTTON_PADDING = "BACKBUTTON_PADDING";
        public const string BACKBUTTON_WIDTH = "BACKBUTTON_WIDTH";
        public const string BACKBUTTON_BORDER_SIZE = "BACKBUTTON_BORDER_SIZE";
        public const string HEADER_HEIGHT = "HEADER_HEIGHT";
        public const string IMG_BURGER_PADDING = "IMG_BURGER_PADDING";
        public const string LOGIN_TITLE_PADDING = "LOGIN_TITLE_PADDING";
        public const string VIEW_PADDING = "VIEW_PADDING";
        public const string HEADERWEBVIEW_BACKBUTTON_WIDTH = "HEADERWEBVIEW_BACKBUTTON_WIDTH";
        public const string HEADERWEBVIEW_ADATITLE_WIDTH = "HEADERWEBVIEW_ADATITLE_WIDTH";
        public const string ENTRY_PADDING = "ENTRY_PADDING";
        public const string CHANGEREQUEST_OPTIONS_HEIGHT = "CHANGEREQUEST_OPTIONS_HEIGHT";
        public const string BUTTONLIST_FONTSIZE = "BUTTONLIST_FONTSIZE";
        public const string PICKER_HEIGHT = "PICKER_HEIGHT";

        public const string OnePixelValue = "OnePixelValue";
        public const string BackButtonBackgroundColor = "BackButtonBackgroundColor";

        public const string BurgerButtonBorderColor = "BurgerButtonBorderColor";
        public const string BurgerButtonBackgroundColor = "BurgerButtonBackgroundColor";
        public const string BurgerTitleSeparatorsColor = "BurgerTitleSeparatorsColor";
        public const string BurgerTitleBackgroundColor = "BurgerTitleBackgroundColor";
        public const string BurgerItemSeparatorsColor = "BurgerItemSeparatorsColor";
        public const string BurgerItemBackgroundColor = "BurgerItemBackgroundColor";
        public const string BurgerTextColor = "BurgerTextColor";
        public const string HeaderBackgroundColor = "HeaderBackgroundColor";
        public const string HeaderTextColor = "HeaderTextColor";
        public const string BoxTitleBackgroundColor = "BoxTitleBackgroundColor";
        public const string BoxTitleSecondaryBackgroundColor = "BoxTitleSecondaryBackgroundColor";
        public const string BoxTitleTextColor = "BoxTitleTextColor";
        public const string BoxItemBackgroundColor = "BoxItemBackgroundColor";
        public const string AdaPickerBackgroundColor = "AdaPickerBackgroundColor";
        public const string AdaPickerMeBackgroundColor = "AdaPickerMeBackgroundColor";
        public const string AdaPickerSomeoneElseBackgroundColor = "AdaPickerSomeoneElseBackgroundColor";
        public const string MBBoxItemBackgroundColor = "MBBoxItemBackgroundColor";
        public const string DarkPopinBackgroundColor = "DarkPopinBackgroundColor";
        public const string BoxRSSBackgroundColor = "BoxRSSBackgroundColor";
        public const string AffectedLeadBackgroundColor = "AffectedLeadBackgroundColor";
        public const string HomeDisabledCommandTextColor = "HomeDisabledCommandTextColor";
        public const string BurgerDisabledCommandTextColor = "BurgerDisabledCommandTextColor";

        public const string BoxItemBorderColor = "BoxItemBorderColor";
        public const string BoxItemSecondaryTextColor = "BoxItemSecondaryTextColor";
        public const string BoxItemTextColor = "BoxItemTextColor";
        public const string SettingsBackgroundColor = "SettingsBackgroundColor";
        public const string LoginTextColor = "LoginTextColor";
        public const string SettingsTextColor = "SettingsTextColor";
        public const string SendTextColor = "SendTextColor";
        public const string DoubleArrowedGridColor = "DoubleArrowedGridColor";
        public const string FixedButtonBackgroundColor = "FixedButtonBackgroundColor";
        public const string FixedButtonTextColor = "FixedButtonTextColor";
        public const string FieldHeaderTextColor = "FieldHeaderTextColor";
        public const string PickerTextColor = "PickerTextColor";
        public const string DarkPopinTextColor = "DarkPopinTextColor";
        public const string EntryPlaceholderColor = "EntryPlaceholderColor";

        public const string LoginTitleLabelStyle = "LoginTitleLabelStyle";
        public const string BurgerLargeLabelStyle = "BurgerLargeLabelStyle";
        public const string BurgerMediumLabelStyle = "BurgerMediumLabelStyle";
        public const string HeaderLabelStyle = "HeaderLabelStyle";
        public const string BoxLittleTitleStyle = "BoxLittleTitleStyle";
        public const string BoxTitleStyle = "BoxTitleStyle";
        public const string BoxTitleItemStyle = "BoxTitleItemStyle";
        public const string BoxLabelStyle = "BoxLabelStyle";
        public const string BoxMediumLabelStyle = "BoxMediumLabelStyle";
        public const string BoxSmallLabelStyle = "BoxSmallLabelStyle";
        public const string BoxItalicLabelStyle = "BoxItalicLabelStyle";
        public const string BoxItalicBoldedLabelStyle = "BoxItalicBoldedLabelStyle";
        public const string BoxBoldedLabelStyle = "BoxBoldedLabelStyle";
        public const string BoxBoldedSmallLabelStyle = "BoxBoldedSmallLabelStyle";
        public const string BoxBoldedMediumLabelStyle = "BoxBoldedMediumLabelStyle";
        public const string BoxBoldedLargeLabelStyle = "BoxBoldedLargeLabelStyle";
        public const string BoxRadioButtonLabelStyle = "BoxRadioButtonLabelStyle";
        public const string BoldedSmallLabelStyle = "BoldedSmallLabelStyle";
        public const string BoxItemSecondaryLabelStyle = "BoxItemSecondaryLabelStyle";
        public const string SmallItemListLabelStyle = "SmallItemListLabelStyle";
        public const string LoginLabelStyle = "LoginLabelStyle";
        public const string SettingsLabelStyle = "SettingsLabelStyle";
        public const string SaveLabelStyle = "SaveLabelStyle";
        public const string SendLabelStyle = "SendLabelStyle";
        public const string SendRequestLabelStyle = "SendRequestLabelStyle";
        public const string NotifLabelStyle = "NotifLabelStyle";
        public const string FilterBoldedMediumLabelStyle = "FilterBoldedMediumLabelStyle";
        public const string DarkPopinLabelStyle = "DarkPopinLabelStyle";
        public const string FixedButtonLabelStyle = "FixedButtonLabelStyle";
        public const string SmallFixedButtonLabelStyle = "SmallFixedButtonLabelStyle";
        public const string AllocateLeadsLabelStyle = "AllocateLeadsLabelStyle";
        public const string PickerItemLabelStyle = "PickerItemLabelStyle";
        public const string ActionsWorkflowLabelStyle = "ActionsWorkflowLabelStyle";
        public const string DoubledRowTitleLabelStyle = "DoubledRowTitleLabelStyle";
        public const string ProfileBoldLabelStyle = "ProfileBoldLabelStyle";

        public const string BurgerButtonStyle = "BurgerButtonStyle";
        public const string FixedButtonStyle = "FixedButtonStyle";

        public const string BoxFrameStyle = "BoxFrameStyle";
        public const string FieldHeaderStyle = "FieldHeaderStyle";
        public const string SimpleEntryStyle = "SimplEntryStyle";
        public const string MyReportAdaPickerStyle = "MyReportAdaPickerStyle";
        public const string MediumTextAdaPickerStyle = "MediumTextAdaPickerStyle";
        public const string MediumBoldedTextAdaPickerStyle = "MediumBoldedTextAdaPickerStyle";
        public const string SelectControlStyle = "SelectControlStyle";

        public const string HeaderGridStyle = "HeaderGridStyle";
        public const string BoxItemGridStyle = "BoxItemGridStyle";
        public const string DoubleArrowedGridStyle = "DoubleArrowedGridStyle";

        public const string HomeBackgroundImage = "HomeBackgroundImage";         // Image de fond HomeView
        public const string BackImage = "BackImage";                   // Flèche retour
        public const string BurgerImage = "BurgerImage";                 // Image du bouton pour faire apparaitre le burger (version noire)
        public const string BurgerWhiteImage = "BurgerWhiteImage";            // Image du bouton burger dans le menu Burger (version blanche)
        public const string BrandImage = "BrandImage";                  // Logo de la marque
        public const string LargeBrandImage = "LargeBrandImage";             // Logo de la marque, plus gros pour la page Login
        public const string ArrowSingleBlackImage = "ArrowSingleBlackImage";       // Flèche noire simple
        public const string ArrowSingleRedImage = "ArrowSingleRedImage";         // Flèche rouge simple
        public const string ArrowDoubleRedImage = "ArrowDoubleRedImage";         // Flèche rouge double
        public const string ArrowSingleWhiteImage = "ArrowSingleWhiteImage";       // Flèche blanche simple
        public const string ArrowDoubleWhiteImage = "ArrowDoubleWhiteImage";       // Flèche blanche double
        public const string HistogramOutlineImage = "HistogramOutlineImage";       // Image graphique en barres
        public const string RadioButtonUncheckedImage = "RadioButtonUncheckedImage";   // Radiobutton décoché
        public const string RadioButtonCheckedImage = "RadioButtonCheckedImage";     // Radiobutton coché (rouge)
        public const string RedCircleImage = "RedCircleImage";              // Rond rouge
        public const string CheckedNoImage = "CheckedNoImage";              // Checkbox décochée
        public const string CheckedYesImage = "CheckedYesImage";             // Checkbox cochée
        public const string TimerDarkGrayImage = "TimerDarkGrayImage";          // Horloge gris foncé
        public const string TimerLightGrayImage = "TimerLightGrayImage";         // Horloge gris clair
        public const string TimerRedImage = "TimerRedImage";               // Horloge rouge
        public const string TimerOrangeImage = "TimerOrangeImage";            // Horloge orange
        public const string FilterBlackImage = "FilterBlackImage";            // Image filtre noir du Header
        public const string FilterRedImage = "FilterRedImage";              // Image filtre red du Header
        public const string FilterWhiteImage = "FilterWhiteImage";            // Image filtre blanc du Filtre
        public const string DefaultCarImage = "DefaultCarImage";             // Image par défault pour InboxDetailView
        public const string DisquetteImage = "DisquetteImage";              // Disquette rouge
        public const string InformationBlackImage = "InformationBlackImage";       // black info image
        public const string InformationRedImage = "InformationRedImage";         // red info image
        public const string InformationCircleImage = "InformationCircleImage";      // "i" info dans rond noir
        public const string RadioButtonWhiteImage = "RadioButtonWhiteImage";       // Radiobutton aux contours blancs
        public const string RadioButtonWhiteFilledImage = "RadioButtonWhiteFilledImage"; // Radiobutton aux contours blancs rempli par un rond rouge
        public const string PuceOnImage = "PuceOnImage";                 // Puce vide
        public const string PuceOffImage = "PuceOffImage";                // Puce rouge checkée
        public const string FlagSouthKoreaImage = "FlagSouthKoreaImage";         // Drapeau de changement de langue
        public const string FlagUnitedStatesImage = "FlagUnitedStatesImage";       // Drapeau de changement de langue
        public const string CrossButtonImage = "CrossButtonImage";            // Bouton de fermeture
        public const string WhitePlusImage = "WhitePlusImage";              // Un "+" blanc
        public const string WhiteCrossImage = "WhiteCrossImage";             // Croix blanche de fermeture de popin
        public const string ArrowBackWhiteImage = "ArrowBackWhiteImage";         // Pour back dans une vue contenant une webview (HeaderWebView)

        public const string BackgroundImagePage = "BackgroundImagePage";
        public const string BurgerImageStyle = "BurgerImageStyle";
        public const string BurgerWhiteImageStyle = "BurgerWhiteImageStyle";
        public const string BrandImageStyle = "BrandImageStyle";
        public const string LargeBrandImageStyle = "LargeBrandImageStyle";
        public const string UncheckButtonImageStyle = "UncheckButtonImageStyle";
        public const string CheckButtonImageStyle = "CheckButtonImageStyle";
        public const string ArrowSingleBlackImageStyle = "ArrowSingleBlackImageStyle";
        public const string ArrowSingleRedImageStyle = "ArrowSingleRedImageStyle";
        public const string ArrowDoubleWhiteImageStyle = "ArrowDoubleWhiteImageStyle";
        public const string HistogramOutlineImageStyle = "HistogramOutlineImageStyle";
        public const string NewHistogramOutlineImageStyle = "NewHistogramOutlineImageStyle";
        public const string RadioButtonUncheckedImageStyle = "RadioButtonUncheckedImageStyle";
        public const string RadioButtonCheckedImageStyle = "RadioButtonCheckedImageStyle";
        public const string RedCircleImageStyle = "RedCircleImageStyle";
        public const string CheckedNoImageStyle = "CheckedNoImageStyle";
        public const string CheckedYesImageStyle = "CheckedYesImageStyle";
        public const string TimerDarkGrayImageStyle = "TimerDarkGrayImageStyle";
        public const string TimerWhiteImageStyle = "TimerWhiteImageStyle";
        public const string TimerLightGrayImageStyle = "TimerLightGrayImageStyle";
        public const string TimerRedImageStyle = "TimerRedImageStyle";
        public const string TimerOrangeImageStyle = "TimerOrangeImageStyle";
        public const string ArrowWhiteBackImageStyle = "ArrowWhiteBackImageStyle";
        public const string HeaderFilterImageStyle = "HeaderFilterImageStyle";
        public const string FilterWhiteImageStyle = "FilterWhiteImageStyle";
        public const string FilterRedImageStyle = "FilterRedImageStyle";
        public const string DefaultCarImageStyle = "DefaultCarImageStyle";
        public const string DisquetteImageStyle = "DisquetteImageStyle";
        public const string InformationBlackImageStyle = "InformationBlackImageStyle";
        public const string InformationBlackSmallImageStyle = "InformationBlackSmallImageStyle";
        public const string InformationRedImageStyle = "InformationRedImageStyle";
        public const string RadioButtonWhiteImageStyle = "RadioButtonWhiteImageStyle";
        public const string RadioButtonWhiteFilledImageStyle = "RadioButtonWhiteFilledImageStyle";
        public const string InformationCircleImageStyle = "InformationCircleImageStyle";
        public const string PuceOnImageStyle = "PuceOnImageStyle";
        public const string PuceOffImageStyle = "PuceOffImageStyle";
        public const string FlagSouthKoreaImageStyle = "FlagSouthKoreaImageStyle";
        public const string FlagUnitedStatesImageStyle = "FlagUnitedStatesImageStyle";
        public const string CrossButtonImageStyle = "CrossButtonImageStyle";
        public const string WhitePlusImageStyle = "WhitePlusImageStyle";
        public const string WhiteCrossImageStyle = "WhiteCrossImageStyle";
        public const string ArrowBackWhiteImageStyle = "ArrowBackWhiteImageStyle";

        public const string EmptyKey = "EmptyKey";

        #endregion

        #endregion

        public static void Apply(ResourceDictionary p_ResourceDictionary, ThemeParameters p_Parameters)
        {
            // Il est possible d'adapter aussi le thème en  fonction de la plateforme.
            // Il est possible de faire toutes sortes de calcules, comme par exemple retrouver
            // la résolution du Device et calculer la taille de police la plus appropriée.
            // Toutes sortes de scénarios sont possibles : c'est la raison des thèmes en C#.
            // Ici nous adaptons simplement la taille de police par défaut.

            p_ResourceDictionary.Add(GridUp, 4);

            //Custon defined locations and values to be used in differentiating themes and OS below
            #region ==Local File Locations per theme===

            #region MY RESTAU
            string checkBoxOffLocation = "";
            string checkBoxOnLocation = "";
            string backImageLocation = "";
            string MenuPreviousDate = "";
            string MenuNextDate = "";

            string notifImageLocation = "";

            #endregion

            #region ADA

            string HomeBackgroundImageLocation = "MyADA.Common.Views.Images.background-audi.jpg";

            string BurgerImageLocation = "";
            string BurgerWhiteImageLocation = "";
            string BrandImageLocation = "";
            string LargeBrandImageLocation = "";
            string ArrowSingleBlackImageLocation = "";
            string ArrowSingleRedImageLocation = "";
            string ArrowDoubleRedImageLocation = "";
            string ArrowSingleWhiteImageLocation = "";
            string ArrowDoubleWhiteImageLocation = "";
            string HistogramOutlineImageLocation = "";
            string RadioButtonUncheckedImageLocation = "";
            string NewRadioButtonUncheckedImageLocation = "";
            string RadioButtonCheckedImageLocation = "";
            string NewRadioButtonCheckedImageLocation = "";
            string RedCircleImageLocation = "";
            string CheckedNoImageLocation = "";
            string CheckedYesImageLocation = "";
            string TimerDarkGrayImageLocation = "";
            string TimerLightGrayImageLocation = "";
            string TimerRedImageLocation = "";
            string TimerOrangeImageLocation = "";
            string FilterBlackImageLocation = "";
            string FilterWhiteImageLocation = "";
            string FilterRedImageLocation = "";
            string DefaultCarImageLocation = "";
            string DisquetteImageLocation = "";
            string InformationBlackImageLocation = "";
            string InformationRedImageLocation = "";
            string RadioButtonWhiteImageLocation = "";
            string RadioButtonWhiteFilledImageLoaction = "";
            string InformationCircleImageLocation = "";
            string PuceOnImageLocation = "";
            string PuceOffImageLocation = "";
            string FlagSouthKoreaImageLocation = "";
            string FlagUnitedStatesImageLocation = "";
            string CrossButtonImageLocation = "";
            string WhitePlusImageLocation = "";
            string WhiteCrossImageLocation = "";
            string ArrowBackWhiteImageLocation = "";

            #endregion

            #endregion

            p_ResourceDictionary.Add(BackGroundImage, "");

            //Used to filter based on different theme config
            #region === OS Resources(Switch Statements) ===

            #region MY RESTAU

            switch (Device.OS)
            {
                case TargetPlatform.Android:
                    checkBoxOffLocation = String.Format("CaseNonCoche{0}.png", p_Parameters.ThemeExtension);
                    checkBoxOnLocation = String.Format("CaseCoche{0}.png", p_Parameters.ThemeExtension);
                    p_ResourceDictionary["BackGroundImage"] = String.Format("BackGroundImage.jpg");
                    MenuPreviousDate = "MenuPrevious.png";
                    MenuNextDate = "MenuNext.png";
                    notifImageLocation = String.Format("Notif{0}.png", p_Parameters.ThemeExtension);
                    break;

                case TargetPlatform.WinPhone:
                    checkBoxOffLocation = String.Format("Assets/CaseNonCoche{0}.png", p_Parameters.ThemeExtension);
                    checkBoxOnLocation = String.Format("Assets/CaseCoche{0}.png", p_Parameters.ThemeExtension);
                    p_ResourceDictionary["BackGroundImage"] = String.Format("Assets/BackGroundImage.jpg");
                    MenuPreviousDate = "Assets/MenuPrevious.png";
                    MenuNextDate = "Assets/MenuNext.png";
                    notifImageLocation = String.Format("Assets/Notif{0}.png", p_Parameters.ThemeExtension);
                    break;

                case TargetPlatform.iOS:
                    checkBoxOffLocation = String.Format("CaseNonCoche{0}.png", p_Parameters.ThemeExtension);
                    checkBoxOnLocation = String.Format("CaseCoche{0}.png", p_Parameters.ThemeExtension);
                    p_ResourceDictionary["BackGroundImage"] = String.Format("BackGroundImage.jpg");
                    MenuPreviousDate = "MenuPrevious.png";
                    MenuNextDate = "MenuNext.png";
                    notifImageLocation = String.Format("Notif{0}.png", p_Parameters.ThemeExtension);
                    break;

                default:
                    break;

            }

            #endregion

            #region ADA

            switch (Device.OS)
            {
                case TargetPlatform.Android:
                    BurgerImageLocation = "MenuBurger.png";
                    BurgerWhiteImageLocation = "Menu.png";
                    BrandImageLocation = "LogoAudi.png";
                    LargeBrandImageLocation = (Device.Idiom == TargetIdiom.Phone) ? "LogoAudiMedium.png" : "LogoAudiLarge.png";
                    ArrowSingleBlackImageLocation = "ArrowSingleBlack.png";
                    ArrowSingleRedImageLocation = "ArrowSingleRed.png";
                    ArrowDoubleRedImageLocation = "ArrowDoubleRed.png";
                    ArrowSingleWhiteImageLocation = "ArrowSingleWhite.png";
                    ArrowDoubleWhiteImageLocation = "ArrowDoubleWhite.png";
                    HistogramOutlineImageLocation = "HistogramOutlineBlack.png";
                    RadioButtonUncheckedImageLocation = "CircleOutlineBlack.png";
                    NewRadioButtonUncheckedImageLocation = "puceOff.png";
                    RadioButtonCheckedImageLocation = "CircleOutlineBlackFilled.png";
                    NewRadioButtonCheckedImageLocation = "puceOn.png";
                    RedCircleImageLocation = "CircleRed.png";
                    CheckedNoImageLocation = "CheckedNo.png";
                    CheckedYesImageLocation = "CheckedYes.png";
                    TimerDarkGrayImageLocation = "TimerDarkGrey.png";
                    TimerLightGrayImageLocation = "TimerLightGrey.png";
                    TimerRedImageLocation = "TimerRed.png";
                    TimerOrangeImageLocation = "TimerOrange.png";
                    backImageLocation = "back.png";
                    FilterBlackImageLocation = "FilterDarkGrey.png";
                    FilterRedImageLocation = "FilterRed.png";
                    FilterWhiteImageLocation = "FilterWhite.png";
                    DefaultCarImageLocation = "imgDefaultInboxDetail.png";
                    DisquetteImageLocation = "Disquette.png";
                    InformationBlackImageLocation = "iInformationCircleBlack.png";
                    InformationRedImageLocation = "iInformationBlack.png";
                    RadioButtonWhiteImageLocation = "RadioButtonWhite.png";
                    RadioButtonWhiteFilledImageLoaction = "RadioButtonWhiteFilled.png";
                    InformationCircleImageLocation = "iInformationCircleBlack.png";
                    PuceOnImageLocation = "puceOn.png";
                    PuceOffImageLocation = "puceOff.png";
                    FlagSouthKoreaImageLocation = "FlagSouthKorea.png";
                    FlagUnitedStatesImageLocation = "FlagUnitedStates.png";
                    CrossButtonImageLocation = "CrossButton.png";
                    WhitePlusImageLocation = "WhitePlus.png";
                    WhiteCrossImageLocation = "WhiteCross.png";
                    ArrowBackWhiteImageLocation = "ArrowBackWhite.png";
                    break;

                case TargetPlatform.WinPhone:
                    BurgerImageLocation = "Assets/menu-burger.png";
                    BurgerWhiteImageLocation = "Assets/Menu.png";
                    BrandImageLocation = "Assets/logo-audi.png";
                    LargeBrandImageLocation = (Device.Idiom == TargetIdiom.Phone) ? "Assets/LogoAudiMedium.png" : "Assets/LogoAudiLarge.png";
                    ArrowSingleBlackImageLocation = "Assets/ArrowSingleBlack.png";
                    ArrowSingleRedImageLocation = "Assets/arrow-single-red.png";
                    ArrowDoubleRedImageLocation = "Assets/arrow-double-red.png";
                    ArrowSingleWhiteImageLocation = "Assets/arrow-single-white.png";
                    ArrowDoubleWhiteImageLocation = "Assets/ArrowDoubleWhite.png";
                    HistogramOutlineImageLocation = "Assets/histogram-outline-black.png";
                    RadioButtonUncheckedImageLocation = "Assets/circle-outline-black.png";
                    NewRadioButtonUncheckedImageLocation = "Assets/puceOff.png";
                    RadioButtonCheckedImageLocation = "Assets/CircleOutlineBlackFilled.png";
                    NewRadioButtonCheckedImageLocation = "Assets/puceOn.png";
                    RedCircleImageLocation = "Assets/circle-red.png";
                    CheckedNoImageLocation = "Assets/CheckedNo.png";
                    CheckedYesImageLocation = "Assets/CheckedYes.png";
                    TimerDarkGrayImageLocation = "Assets/TimerDarkGrey.png";
                    TimerLightGrayImageLocation = "Assets/TimerLightGrey.png";
                    TimerRedImageLocation = "Assets/TimerRed.png";
                    TimerOrangeImageLocation = "Assets/TimerOrange.png";
                    backImageLocation = "Assets/back.png";
                    FilterBlackImageLocation = "Assets/FilterDarkGrey.png";
                    FilterRedImageLocation = "Assets/FilterRed.png";
                    FilterWhiteImageLocation = "Assets/FilterWhite.png";
                    DefaultCarImageLocation = "Assets/imgDefaultInboxDetail.png";
                    DisquetteImageLocation = "Assets/Disquette.png";
                    InformationBlackImageLocation = "Assets/iInformationCircleBlack.png";
                    InformationRedImageLocation = "Assets/iInformationRed.png";
                    RadioButtonWhiteImageLocation = "Assets/RadioButtonWhite.png";
                    RadioButtonWhiteFilledImageLoaction = "Assets/RadioButtonWhiteFilled.png";
                    InformationCircleImageLocation = "Assets/iInformationCircleBlack.png";
                    PuceOnImageLocation = "Assets/puceOn.png";
                    PuceOffImageLocation = "Assets/puceOff.png";
                    FlagSouthKoreaImageLocation = "Assets/FlagSouthKorea.png";
                    FlagUnitedStatesImageLocation = "Assets/FlagUnitedStates.png";
                    CrossButtonImageLocation = "Assets/CrossButton.png";
                    WhitePlusImageLocation = "Assets/WhitePlus.png";
                    WhiteCrossImageLocation = "Assets/WhiteCross.png";
                    ArrowBackWhiteImageLocation = "Assets/ArrowBackWhite.png";
                    break;

                case TargetPlatform.iOS:
                    BurgerImageLocation = "menu-burger.png";
                    BurgerWhiteImageLocation = "Menu.png";
                    BrandImageLocation = "logo-audi.png";
                    LargeBrandImageLocation = (Device.Idiom == TargetIdiom.Phone) ? "LogoAudiMedium.png" : "LogoAudiLarge.png";
                    ArrowSingleBlackImageLocation = "ArrowSingleBlack.png";
                    ArrowSingleRedImageLocation = "arrow-single-red.png";
                    ArrowDoubleRedImageLocation = "arrow-double-red.png";
                    ArrowSingleWhiteImageLocation = "arrow-single-white.png";
                    ArrowDoubleWhiteImageLocation = "ArrowDoubleWhite.png";
                    HistogramOutlineImageLocation = "histogram-outline-black.png";
                    RadioButtonUncheckedImageLocation = "circle-outline-black.png";
                    NewRadioButtonUncheckedImageLocation = "puceOff.png";
                    RadioButtonCheckedImageLocation = "CircleOutlineBlackFilled.png";
                    NewRadioButtonCheckedImageLocation = "puceOn.png";
                    RedCircleImageLocation = "circle-red.png";
                    CheckedNoImageLocation = "CheckedNo.png";
                    CheckedYesImageLocation = "CheckedYes.png";
                    TimerDarkGrayImageLocation = "TimerDarkGrey.png";
                    TimerLightGrayImageLocation = "TimerLightGrey.png";
                    TimerRedImageLocation = "TimerRed.png";
                    TimerOrangeImageLocation = "TimerOrange.png";
                    backImageLocation = "back.png";
                    FilterBlackImageLocation = "FilterDarkGrey.png";
                    FilterRedImageLocation = "FilterRed.png";
                    FilterWhiteImageLocation = "FilterWhite.png";
                    DefaultCarImageLocation = "imgDefaultInboxDetail.png";
                    DisquetteImageLocation = "Disquette.png";
                    InformationBlackImageLocation = "iInformationCircleBlack.png";
                    InformationRedImageLocation = "iInformationBlack.png";
                    RadioButtonWhiteImageLocation = "RadioButtonWhite.png";
                    RadioButtonWhiteFilledImageLoaction = "RadioButtonWhiteFilled.png";
                    InformationCircleImageLocation = "iInformationCircleBlack.png";
                    PuceOnImageLocation = "puceOn.png";
                    PuceOffImageLocation = "puceOff.png";
                    FlagSouthKoreaImageLocation = "FlagSouthKorea.png";
                    FlagUnitedStatesImageLocation = "FlagUnitedStates.png";
                    CrossButtonImageLocation = "CrossButton.png";
                    WhitePlusImageLocation = "WhitePlus.png";
                    WhiteCrossImageLocation = "WhiteCross.png";
                    ArrowBackWhiteImageLocation = "ArrowBackWhite.png";
                    break;

                default:
                    break;

            }

            #endregion

            #endregion


            // Création des ressources à base de Key.
            // Ces ressources peuvent être utilisée en tout point de l'UI avec la syntaxe suivante:
            // Property="{DynamicResource NomDeLaKey}"
            #region === Ressources Key ===

            #region MY RESTAU


            p_ResourceDictionary.Add(notifImage, FileImageSource.FromFile(notifImageLocation));

            p_ResourceDictionary.Add(GerantMainColor, p_Parameters.GerantMainColor);
            p_ResourceDictionary.Add(GerantSecondaryColor, p_Parameters.GerantSecondaryColor);
            p_ResourceDictionary.Add(GerantPageBackgroundColor, p_Parameters.GerantPageBackgroundColor);
            p_ResourceDictionary.Add(GerantSecondaryPageBackgroundColor, p_Parameters.GerantSecondaryPageBackgroundColor);

            p_ResourceDictionary.Add(PrimaryTextColor, p_Parameters.PrimaryTextColor);
            p_ResourceDictionary.Add(SecondaryTextColor, p_Parameters.SecondaryTextColor);
            p_ResourceDictionary.Add(TertiaryTextColor, p_Parameters.TertiaryTextColor);

            p_ResourceDictionary.Add(RepeaterColor, p_Parameters.RepeaterColor);
            p_ResourceDictionary.Add(PopinBackgroundColor, p_Parameters.PopinBackgroundColor);


            //**for menuList**//
            //for Date
            p_ResourceDictionary.Add(MENUS_DATEPANEL_BACKCOLOR, p_Parameters.NewMenuViewSwipeOptionBackgroundColor);
            p_ResourceDictionary.Add(MENUS_DATEPANEL_COLOR, Color.White);
            p_ResourceDictionary.Add(MENUS_PREVIOUS_IMAGESOURCE, ImageSource.FromFile(MenuPreviousDate));
            p_ResourceDictionary.Add(CHECKBOX_OFF_IMAGESOURCE, ImageSource.FromFile(checkBoxOffLocation));
            p_ResourceDictionary.Add(CHECKBOX_ON_IMAGESOURCE, ImageSource.FromFile(checkBoxOnLocation));
            p_ResourceDictionary.Add(MENUS_NEXT_IMAGESOURCE, ImageSource.FromFile(MenuNextDate));

            //for MenuNom (When there are more than one meal)
            p_ResourceDictionary.Add(MENUS_MENUPANEL_BACKCOLOR, p_Parameters.l_ComposanteLayoutColor);
            p_ResourceDictionary.Add(MENUS_MENUPANEL_COLOR, p_Parameters.ThemeMaincolor);
            p_ResourceDictionary.Add(MENUS_RECETTENOM_COLOR, p_Parameters.MenuViewRecetteNomColor);
            p_ResourceDictionary.Add(MENUS_LabelComposante_COLOR, p_Parameters.TextColor);

            //For Buttons
            p_ResourceDictionary.Add(OrderListViewItemHeight, (double)Device.OnPlatform(55, 60, 70));

            #endregion

            #region ADA

            // Clés statiques
            p_ResourceDictionary.Add(OnePixelValue, (double)Device.OnPlatform(1, 1, 1.5));
            p_ResourceDictionary.Add(PickerBackImageBoxHeight, (double)Device.OnPlatform(35, 35, 40));
            p_ResourceDictionary.Add(PickerBackImageHeight, (double)Device.OnPlatform(25, 20, 20));

            p_ResourceDictionary.Add(BUTTON_BOTTOM_WIDTH, (Device.Idiom == TargetIdiom.Phone) ? new GridLength(3, GridUnitType.Star) : new GridLength(1.5, GridUnitType.Star));
            p_ResourceDictionary.Add(LEADITEM_GRID_WIDTH, (Device.Idiom == TargetIdiom.Phone) ? new GridLength(3, GridUnitType.Star) : new GridLength(2, GridUnitType.Star));
            p_ResourceDictionary.Add(BACKBUTTON_WIDTH, (Device.Idiom == TargetIdiom.Phone) ? new GridLength(3, GridUnitType.Star) : new GridLength(2, GridUnitType.Star));
            p_ResourceDictionary.Add(HEADER_HEIGHT, (Device.Idiom == TargetIdiom.Phone) ? new GridLength(50, GridUnitType.Absolute) : new GridLength(75, GridUnitType.Absolute));
            p_ResourceDictionary.Add(HEADERWEBVIEW_BACKBUTTON_WIDTH, (Device.Idiom == TargetIdiom.Phone) ? new GridLength(1.2, GridUnitType.Star) : new GridLength(1, GridUnitType.Star));
            p_ResourceDictionary.Add(HEADERWEBVIEW_ADATITLE_WIDTH, (Device.Idiom == TargetIdiom.Phone) ? new GridLength(1, GridUnitType.Star) : new GridLength(3, GridUnitType.Star));
            p_ResourceDictionary.Add(CHANGEREQUEST_OPTIONS_HEIGHT, (Device.Idiom == TargetIdiom.Phone) ? new GridLength(90, GridUnitType.Absolute) : new GridLength(100, GridUnitType.Absolute));
            p_ResourceDictionary.Add(IMG_BURGER_PADDING, (Device.Idiom == TargetIdiom.Phone) ? new Thickness(15, 0, 0, 0) : new Thickness(20, 0, 0, 0));
            p_ResourceDictionary.Add(LOGIN_TITLE_PADDING, (Device.Idiom == TargetIdiom.Phone) ? new Thickness(0, 10, 0, 0) : new Thickness(0, 50, 0, 0));
            p_ResourceDictionary.Add(VIEW_PADDING, (Device.Idiom == TargetIdiom.Phone) ? new Thickness(20) : new Thickness(40));
            p_ResourceDictionary.Add(BUTTON_FIXED_HEIGHT, (Device.Idiom == TargetIdiom.Phone) ? (double)Device.OnPlatform(55, 48, 66) : (double)65);
            p_ResourceDictionary.Add(TITLE_HEIGHT, (Device.Idiom == TargetIdiom.Phone) ? (double)50 : (double)70);
            p_ResourceDictionary.Add(BUTTONLIST_FONTSIZE, (Device.Idiom == TargetIdiom.Phone) ? (double)13 : (double)16);
            p_ResourceDictionary.Add(PICKER_HEIGHT, (Device.Idiom == TargetIdiom.Phone) ? (double)35 : (double)37);

            p_ResourceDictionary.Add(ENTRY_PADDING, (Device.OS == TargetPlatform.iOS) ? new Thickness(28, 5, 5, 5) : new Thickness(28, 0, 0, 0));
            p_ResourceDictionary.Add(BACKBUTTON_BORDER_SIZE, (Device.OS == TargetPlatform.Android) ? new GridLength(0.7, GridUnitType.Absolute) : new GridLength(1, GridUnitType.Absolute));

            p_ResourceDictionary.Add(BORDER_BOX_HEIGHT, (Device.Idiom == TargetIdiom.Tablet && Device.OS == TargetPlatform.iOS) ? new GridLength(1, GridUnitType.Absolute) : new GridLength(0.7, GridUnitType.Absolute));


            // Couleurs -----------------------------------------------------------------------------------------------
            p_ResourceDictionary.Add(BackButtonBackgroundColor, p_Parameters.BackButtonBackgroundColor);
            p_ResourceDictionary.Add(BurgerButtonBorderColor, p_Parameters.BurgerButtonBorderColor);
            p_ResourceDictionary.Add(BurgerButtonBackgroundColor, p_Parameters.BurgerButtonBackgroundColor);
            p_ResourceDictionary.Add(BurgerTitleSeparatorsColor, p_Parameters.BurgerTitleSeparatorsColor);
            p_ResourceDictionary.Add(BurgerTitleBackgroundColor, p_Parameters.BurgerTitleBackgroundColor);
            p_ResourceDictionary.Add(BurgerItemSeparatorsColor, p_Parameters.BurgerItemSeparatorsColor);
            p_ResourceDictionary.Add(BurgerItemBackgroundColor, p_Parameters.BurgerItemBackgroundColor);
            p_ResourceDictionary.Add(BurgerTextColor, p_Parameters.BurgerTextColor);
            p_ResourceDictionary.Add(HeaderBackgroundColor, p_Parameters.HeaderBackgroundColor);
            p_ResourceDictionary.Add(HeaderTextColor, p_Parameters.HeaderTextColor);
            p_ResourceDictionary.Add(BoxTitleBackgroundColor, p_Parameters.BoxTitleBackgroundColor);
            p_ResourceDictionary.Add(BoxTitleSecondaryBackgroundColor, p_Parameters.BoxTitleSecondaryBackgroundColor);
            p_ResourceDictionary.Add(BoxTitleTextColor, p_Parameters.BoxTitleTextColor);
            p_ResourceDictionary.Add(BoxItemBackgroundColor, p_Parameters.BoxItemBackgroundColor);
            p_ResourceDictionary.Add(MBBoxItemBackgroundColor, p_Parameters.MBBoxItemBackgroundColor);
            p_ResourceDictionary.Add(BoxItemBorderColor, p_Parameters.BoxItemBorderColor);
            p_ResourceDictionary.Add(BoxItemSecondaryTextColor, p_Parameters.BoxItemSecondaryTextColor);
            p_ResourceDictionary.Add(BoxItemTextColor, p_Parameters.BoxItemTextColor);
            p_ResourceDictionary.Add(SettingsBackgroundColor, p_Parameters.SettingsBackgroundColor);
            p_ResourceDictionary.Add(LoginTextColor, p_Parameters.LoginTextColor);
            p_ResourceDictionary.Add(SettingsTextColor, p_Parameters.SettingsTextColor);
            p_ResourceDictionary.Add(SendTextColor, p_Parameters.SendTextColor);
            p_ResourceDictionary.Add(DoubleArrowedGridColor, p_Parameters.DoubleArrowedGridColor);
            p_ResourceDictionary.Add(FixedButtonBackgroundColor, p_Parameters.FixedButtonBackgroundColor);
            p_ResourceDictionary.Add(FixedButtonTextColor, p_Parameters.FixedButtonTextColor);
            p_ResourceDictionary.Add(FieldHeaderTextColor, p_Parameters.FieldHeaderTextColor);
            p_ResourceDictionary.Add(PickerTextColor, p_Parameters.PickerTextColor);
            p_ResourceDictionary.Add(AdaPickerBackgroundColor, p_Parameters.AdaPickerBackgroundColor);
            p_ResourceDictionary.Add(AdaPickerMeBackgroundColor, p_Parameters.AdaPickerMeBackgroundColor);
            p_ResourceDictionary.Add(AdaPickerSomeoneElseBackgroundColor, p_Parameters.AdaPickerSomeoneElseBackgroundColor);
            p_ResourceDictionary.Add(DarkPopinBackgroundColor, p_Parameters.DarkPopinBackgroundColor);
            p_ResourceDictionary.Add(DarkPopinTextColor, p_Parameters.DarkPopinTextColor);
            p_ResourceDictionary.Add(BoxRSSBackgroundColor, p_Parameters.BoxRSSBackgroundColor);
            p_ResourceDictionary.Add(AffectedLeadBackgroundColor, p_Parameters.AffectedLeadBackgroundColor);
            p_ResourceDictionary.Add(HomeDisabledCommandTextColor, p_Parameters.HomeDisabledCommandTextColor);
            p_ResourceDictionary.Add(BurgerDisabledCommandTextColor, p_Parameters.BurgerDisabledCommandTextColor);
            p_ResourceDictionary.Add(EntryPlaceholderColor, p_Parameters.EntryPlaceholderColor);


            // Images ------------------------------------------------------------------------------------------------
            p_ResourceDictionary.Add(BurgerImage, FileImageSource.FromFile(BurgerImageLocation));
            p_ResourceDictionary.Add(BurgerWhiteImage, FileImageSource.FromFile(BurgerWhiteImageLocation));
            p_ResourceDictionary.Add(BrandImage, FileImageSource.FromFile(BrandImageLocation));
            p_ResourceDictionary.Add(LargeBrandImage, FileImageSource.FromFile(LargeBrandImageLocation));
            p_ResourceDictionary.Add(BackImage, FileImageSource.FromFile(backImageLocation));

            p_ResourceDictionary.Add(RadioButtonUncheckedImage, FileImageSource.FromFile(RadioButtonUncheckedImageLocation));
            p_ResourceDictionary.Add(RadioButtonCheckedImage, FileImageSource.FromFile(RadioButtonCheckedImageLocation));
            p_ResourceDictionary.Add(CheckedNoImage, FileImageSource.FromFile(CheckedNoImageLocation));
            p_ResourceDictionary.Add(CheckedYesImage, FileImageSource.FromFile(CheckedYesImageLocation));

            p_ResourceDictionary.Add(RadioButtonWhiteImage, FileImageSource.FromFile(RadioButtonWhiteImageLocation));
            p_ResourceDictionary.Add(RadioButtonWhiteFilledImage, FileImageSource.FromFile(RadioButtonWhiteFilledImageLoaction));
            p_ResourceDictionary.Add(PuceOnImage, FileImageSource.FromFile(PuceOnImageLocation));
            p_ResourceDictionary.Add(PuceOffImage, FileImageSource.FromFile(PuceOffImageLocation));

            p_ResourceDictionary.Add(CrossButtonImage, FileImageSource.FromFile(CrossButtonImageLocation));
            p_ResourceDictionary.Add(WhiteCrossImageLocation, FileImageSource.FromFile(WhiteCrossImageLocation));
            p_ResourceDictionary.Add(WhitePlusImage, FileImageSource.FromFile(WhitePlusImageLocation));

            p_ResourceDictionary.Add(FilterBlackImage, FileImageSource.FromFile(FilterBlackImageLocation));
            p_ResourceDictionary.Add(FilterWhiteImage, FileImageSource.FromFile(FilterWhiteImageLocation));
            p_ResourceDictionary.Add(FilterRedImage, FileImageSource.FromFile(FilterRedImageLocation));


            p_ResourceDictionary.Add(ArrowSingleBlackImage, FileImageSource.FromFile(ArrowSingleBlackImageLocation));
            p_ResourceDictionary.Add(ArrowSingleRedImage, FileImageSource.FromFile(ArrowSingleRedImageLocation));
            p_ResourceDictionary.Add(ArrowDoubleRedImage, FileImageSource.FromFile(ArrowDoubleRedImageLocation));
            p_ResourceDictionary.Add(ArrowSingleWhiteImage, FileImageSource.FromFile(ArrowSingleWhiteImageLocation));
            p_ResourceDictionary.Add(ArrowDoubleWhiteImage, FileImageSource.FromFile(ArrowDoubleWhiteImageLocation));
            p_ResourceDictionary.Add(ArrowBackWhiteImage, FileImageSource.FromFile(ArrowBackWhiteImageLocation));

            p_ResourceDictionary.Add(TimerDarkGrayImage, FileImageSource.FromFile(TimerDarkGrayImageLocation));
            p_ResourceDictionary.Add(TimerLightGrayImage, FileImageSource.FromFile(TimerLightGrayImageLocation));
            p_ResourceDictionary.Add(TimerRedImage, FileImageSource.FromFile(TimerRedImageLocation));
            p_ResourceDictionary.Add(TimerOrangeImage, FileImageSource.FromFile(TimerOrangeImageLocation));

            p_ResourceDictionary.Add(InformationBlackImage, FileImageSource.FromFile(InformationBlackImageLocation));
            p_ResourceDictionary.Add(InformationRedImage, FileImageSource.FromFile(InformationRedImageLocation));
            p_ResourceDictionary.Add(InformationCircleImage, FileImageSource.FromFile(InformationCircleImageLocation));

            p_ResourceDictionary.Add(RedCircleImage, FileImageSource.FromFile(RedCircleImageLocation));
            p_ResourceDictionary.Add(HistogramOutlineImage, FileImageSource.FromFile(HistogramOutlineImageLocation));
            p_ResourceDictionary.Add(DisquetteImage, FileImageSource.FromFile(DisquetteImageLocation));

            p_ResourceDictionary.Add(FlagSouthKoreaImage, FileImageSource.FromFile(FlagSouthKoreaImageLocation));
            p_ResourceDictionary.Add(FlagUnitedStatesImage, FileImageSource.FromFile(FlagUnitedStatesImageLocation));

            p_ResourceDictionary.Add(DefaultCarImage, FileImageSource.FromFile(DefaultCarImageLocation));


            #endregion

            #endregion

            // Création des styles par défaut.
            #region === Styles ===

            #region Labels

            p_ResourceDictionary.Add(LoginTitleLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.FontSizeProperty, Value = (Device.Idiom == TargetIdiom.Phone) ? 20 : 28 },
                }
            });

            p_ResourceDictionary.Add(BurgerLargeLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.BurgerTextColor                  },
                    new Setter { Property = Label.FontAttributesProperty,    Value = FontAttributes.Bold                           },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? 18 : 24 },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.StartAndExpand                  },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand                 }
                }
            });

            p_ResourceDictionary.Add(BurgerMediumLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.BurgerTextColor                  },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? 14 : 18 },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.StartAndExpand                  },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand                 }
                }
            });

            p_ResourceDictionary.Add(HeaderLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.HeaderTextColor                  },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? 24 : 28 },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.StartAndExpand                  },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand                 }
                }
            });

            p_ResourceDictionary.Add(BoxLittleTitleStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.BoxTitleTextColor                },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? 16 : 22 },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand                 },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand                 }
                }
            });

            p_ResourceDictionary.Add(BoxTitleStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.BoxTitleTextColor                },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? 18 : 24 },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand                 },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand                 }
                }
            });

            p_ResourceDictionary.Add(DoubledRowTitleLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.BoxTitleTextColor                },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? 18 : 22 },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand                 },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand                 }
                }
            });

            p_ResourceDictionary.Add(BoxTitleItemStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.BoxItemTextColor                 },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? 18 : 24 },
                    new Setter { Property = Label.FontAttributesProperty,    Value = FontAttributes.Bold                           },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.StartAndExpand                  },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.StartAndExpand                  }
                }
            });

            p_ResourceDictionary.Add(BoxLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.BoxItemTextColor                 },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? 14 : 18 },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.Start                           },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand                 }
                }
            });

            p_ResourceDictionary.Add(BoxMediumLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty, Value = p_Parameters.BoxItemTextColor                                                                   },
                    new Setter { Property = Label.FontSizeProperty,  Value = (Device.Idiom == TargetIdiom.Phone) ? 16 : Device.GetNamedSize(NamedSize.Medium, typeof(Label)) },
                }
            });

            p_ResourceDictionary.Add(BoxSmallLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty, Value = p_Parameters.BoxItemTextColor                                                                  },
                    new Setter { Property = Label.FontSizeProperty,  Value = (Device.Idiom == TargetIdiom.Phone) ? 14 : Device.GetNamedSize(NamedSize.Small, typeof(Label)) },
                }
            });

            p_ResourceDictionary.Add(BoxItalicLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty, Value = p_Parameters.BoxItemTextColor                },
                    new Setter { Property = Label.FontSizeProperty,  Value = (Device.Idiom == TargetIdiom.Phone) ? 8 : 12 }
                }
            });

            p_ResourceDictionary.Add(BoxItalicBoldedLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,      Value = p_Parameters.BoxItemTextColor                 },
                    new Setter { Property = Label.FontAttributesProperty, Value = FontAttributes.Bold | FontAttributes.Italic   },
                    new Setter { Property = Label.FontSizeProperty,       Value = (Device.Idiom == TargetIdiom.Phone) ? 14 : 18 }
                }
            });

            p_ResourceDictionary.Add(BoxBoldedLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,      Value = p_Parameters.BoxItemTextColor },
                    new Setter { Property = Label.FontAttributesProperty, Value = FontAttributes.Bold           }
                }
            });

            p_ResourceDictionary.Add(BoxBoldedSmallLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,      Value = p_Parameters.BoxItemTextColor                       },
                    new Setter { Property = Label.FontAttributesProperty, Value = FontAttributes.Bold                                 },
                    new Setter { Property = Label.FontSizeProperty,       Value = Device.GetNamedSize(NamedSize.Small, typeof(Label)) }
                }
            });

            p_ResourceDictionary.Add(BoxBoldedMediumLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,      Value = p_Parameters.BoxItemTextColor                        },
                    new Setter { Property = Label.FontAttributesProperty, Value = FontAttributes.Bold                                  },
                    new Setter { Property = Label.FontSizeProperty,       Value = Device.GetNamedSize(NamedSize.Small, typeof(Label)) }
                }
            });

            p_ResourceDictionary.Add(FilterBoldedMediumLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,      Value = p_Parameters.BurgerTextColor                         },
                    new Setter { Property = Label.FontAttributesProperty, Value = FontAttributes.Bold                                  },
                    new Setter { Property = Label.FontSizeProperty,       Value = Device.GetNamedSize(NamedSize.Medium, typeof(Label)) }
                }
            });

            p_ResourceDictionary.Add(BoxBoldedLargeLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,      Value = p_Parameters.BoxItemTextColor                       },
                    new Setter { Property = Label.FontAttributesProperty, Value = FontAttributes.Bold                                 },
                    new Setter { Property = Label.FontSizeProperty,       Value = Device.GetNamedSize(NamedSize.Large, typeof(Label)) }
                }
            });

            p_ResourceDictionary.Add(ProfileBoldLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,      Value = p_Parameters.BoxItemTextColor                       },
                    new Setter { Property = Label.FontSizeProperty,       Value = Device.GetNamedSize(NamedSize.Medium, typeof(Label)) }
                }
            });

            p_ResourceDictionary.Add(BoxRadioButtonLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,      Value = p_Parameters.BoxItemTextColor },
                    new Setter { Property = Label.FontAttributesProperty, Value = FontAttributes.Bold           }
                }
            });

            p_ResourceDictionary.Add(BoldedSmallLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,      Value = p_Parameters.BoxItemTextColor                       },
                    new Setter { Property = Label.FontAttributesProperty, Value = FontAttributes.Bold                                 },
                    new Setter { Property = Label.FontSizeProperty,       Value = Device.GetNamedSize(NamedSize.Small, typeof(Label)) }
                }
            });

            p_ResourceDictionary.Add(BoxItemSecondaryLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty, Value = p_Parameters.BoxItemSecondaryTextColor        },
                    new Setter { Property = Label.FontSizeProperty,  Value = (Device.Idiom == TargetIdiom.Phone) ? 16 : 20 }
                }
            });

            p_ResourceDictionary.Add(SmallItemListLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty, Value = p_Parameters.BoxItemTextColor                       },
                    new Setter { Property = Label.FontSizeProperty,  Value = Device.GetNamedSize(NamedSize.Small, typeof(Label)) }
                }
            });

            p_ResourceDictionary.Add(LoginLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.LoginTextColor                   },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? 18 : 24 },
                    new Setter { Property = Label.FontAttributesProperty,    Value = FontAttributes.Bold                           },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.StartAndExpand                  },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.Center                          }
                }
            });

            p_ResourceDictionary.Add(SettingsLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.SettingsTextColor                },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? 17 : 24 },
                    new Setter { Property = Label.FontAttributesProperty,    Value = FontAttributes.Bold                           },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.StartAndExpand                  },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.Center                          }
                }
            });

            p_ResourceDictionary.Add(SaveLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.SettingsTextColor                },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? 20 : 26 },
                    new Setter { Property = Label.FontAttributesProperty,    Value = FontAttributes.Bold                           },
                }
            });

            p_ResourceDictionary.Add(SendLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.SendTextColor                    },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? 20 : 26 },
                    new Setter { Property = Label.FontAttributesProperty,    Value = FontAttributes.Bold                           },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.StartAndExpand                  },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.Center                          }
                }
            });

            p_ResourceDictionary.Add(SendRequestLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.SettingsTextColor                },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? 18 : 24 },
                    new Setter { Property = Label.FontAttributesProperty,    Value = FontAttributes.Bold                           },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.StartAndExpand                  },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.Center                          }
                }
            });

            p_ResourceDictionary.Add(NotifLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = Color.White                                         },
                    new Setter { Property = Label.FontSizeProperty,          Value = Device.GetNamedSize(NamedSize.Small, typeof(Label)) },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.Center                                },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.Center                                }
                }
            });

            p_ResourceDictionary.Add(DarkPopinLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty, Value = p_Parameters.DarkPopinTextColor                      },
                    new Setter { Property = Label.FontSizeProperty,  Value = (Device.Idiom == TargetIdiom.Phone) ? Device.GetNamedSize(NamedSize.Small, typeof(Label)) : Device.GetNamedSize(NamedSize.Medium, typeof(Label)) }
                }
            });

            p_ResourceDictionary.Add(FixedButtonLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.FixedButtonTextColor                                                               },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? Device.GetNamedSize(NamedSize.Medium, typeof(Label)) : 24 },
                    new Setter { Property = Label.FontAttributesProperty,    Value = FontAttributes.Bold                                                                             },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand                                                                   },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand                                                                   }
                }
            });

            p_ResourceDictionary.Add(SmallFixedButtonLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty,         Value = p_Parameters.FixedButtonTextColor                                                              },
                    new Setter { Property = Label.FontSizeProperty,          Value = (Device.Idiom == TargetIdiom.Phone) ? Device.GetNamedSize(NamedSize.Small, typeof(Label)) : 20 },
                    new Setter { Property = Label.FontAttributesProperty,    Value = FontAttributes.Bold                                                                            },
                    new Setter { Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand                                                                  },
                    new Setter { Property = Label.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand                                                                  }
                }
            });

            p_ResourceDictionary.Add(AllocateLeadsLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty, Value = p_Parameters.BoxItemTextColor },
                    new Setter { Property = Label.FontSizeProperty, Value = (Device.Idiom == TargetIdiom.Phone) ? Device.GetNamedSize(NamedSize.Small, typeof(Label)) : Device.GetNamedSize(NamedSize.Medium, typeof(Label)) }
                }
            });

            p_ResourceDictionary.Add(PickerItemLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty, Value = p_Parameters.BoxItemTextColor },
                    new Setter { Property = Label.FontSizeProperty, Value = (Device.Idiom == TargetIdiom.Phone) ? Device.GetNamedSize(NamedSize.Small, typeof(Label)) : Device.GetNamedSize(NamedSize.Medium, typeof(Label)) }
                }
            });

            p_ResourceDictionary.Add(ActionsWorkflowLabelStyle, new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter { Property = Label.TextColorProperty, Value = p_Parameters.BoxItemTextColor },
                    new Setter { Property = Label.FontSizeProperty,  Value = (Device.Idiom == TargetIdiom.Phone) ? Device.GetNamedSize(NamedSize.Small, typeof(Label)) : Device.GetNamedSize(NamedSize.Medium, typeof(Label)) },
                }
            });

            #endregion

            #region Buttons

            p_ResourceDictionary.Add(BurgerButtonStyle, new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter { Property = Button.BorderColorProperty,     Value = p_Parameters.BurgerButtonBorderColor     },
                    new Setter { Property = Button.BackgroundColorProperty, Value = p_Parameters.BurgerButtonBackgroundColor },
                    new Setter { Property = Button.ImageProperty,           Value = BurgerImageLocation                      },
                    //new Setter { Property = Button.HeightRequestProperty,   Value = 30                                       },
                    new Setter { Property = Button.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand            }
                }
            });

            p_ResourceDictionary.Add(FixedButtonStyle, new Style(typeof(StackLayout))
            {
                Setters =
                {
                    new Setter { Property = StackLayout.BackgroundColorProperty,   Value = p_Parameters.FixedButtonBackgroundColor },
                    new Setter { Property = StackLayout.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand             },
                    new Setter { Property = StackLayout.VerticalOptionsProperty,   Value = LayoutOptions.FillAndExpand             }
                }
            });

            #endregion

            #region Images

            p_ResourceDictionary.Add(EmptyKey, new Style(typeof(Image))
            {
                Setters = 
                {
                    new Setter { Property = Image.SourceProperty, Value = FileImageSource.FromFile("") } 
                }
            });

            p_ResourceDictionary.Add(BackgroundImagePage, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = ImageSource.FromResource(HomeBackgroundImageLocation) },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.Fill                                           }
                }
            });

            p_ResourceDictionary.Add(BurgerImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = BurgerImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFill   }
                }
            });

            p_ResourceDictionary.Add(BurgerWhiteImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = BurgerWhiteImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFill        }
                }
            });

            p_ResourceDictionary.Add(BrandImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,          Value = BrandImageLocation            },
                    new Setter { Property = Image.AspectProperty,          Value = Aspect.AspectFit              },
                    new Setter { Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand }
                }
            });

            p_ResourceDictionary.Add(LargeBrandImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = LargeBrandImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit        }
                }
            });

            // Selection button
            p_ResourceDictionary.Add(UncheckButtonImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Button.ImageProperty,           Value = NewRadioButtonUncheckedImageLocation},
                    new Setter { Property = Button.HeightRequestProperty,   Value = 40}, 
                    new Setter { Property = Button.WidthRequestProperty,    Value = 40}
                }
            });

            p_ResourceDictionary.Add(CheckButtonImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Button.ImageProperty,           Value = NewRadioButtonCheckedImageLocation},
                    new Setter { Property = Button.HeightRequestProperty,   Value = 40},
                    new Setter { Property = Button.WidthRequestProperty,    Value = 40}
                }
            });

            p_ResourceDictionary.Add(ArrowSingleBlackImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,          Value = ArrowSingleBlackImageLocation },
                    new Setter { Property = Image.AspectProperty,          Value = Aspect.AspectFit              },
                    new Setter { Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand }
                }
            });

            p_ResourceDictionary.Add(ArrowSingleRedImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,          Value = ArrowSingleRedImageLocation   },
                    new Setter { Property = Image.AspectProperty,          Value = Aspect.AspectFit              },
                    new Setter { Property = Image.WidthRequestProperty,    Value = 10                            },
                    new Setter { Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand }
                }
            });

            p_ResourceDictionary.Add(ArrowDoubleWhiteImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = ArrowDoubleWhiteImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit              }
                }
            });

            p_ResourceDictionary.Add(HistogramOutlineImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = HistogramOutlineImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit              },
                }
            });

            p_ResourceDictionary.Add(NewHistogramOutlineImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,        Value = HistogramOutlineImageLocation },
                    new Setter { Property = Image.AspectProperty,        Value = Aspect.AspectFit              },
                    new Setter { Property = Image.HeightRequestProperty, Value = 35                            },
                    new Setter { Property = Image.WidthRequestProperty,  Value = 35                            },
                }
            });

            p_ResourceDictionary.Add(RadioButtonUncheckedImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,            Value = RadioButtonUncheckedImageLocation },
                    new Setter { Property = Image.AspectProperty,            Value = Aspect.AspectFit                  },
                    new Setter { Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand     },
                    new Setter { Property = Image.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand     },
                    new Setter { Property = Image.HeightRequestProperty,     Value = 20                                }
                }
            });

            p_ResourceDictionary.Add(RadioButtonCheckedImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,            Value = RadioButtonCheckedImageLocation },
                    new Setter { Property = Image.AspectProperty,            Value = Aspect.AspectFit                },
                    new Setter { Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand   },
                    new Setter { Property = Image.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand   },
                    new Setter { Property = Image.HeightRequestProperty,     Value = 20                              }
                }
            });

            p_ResourceDictionary.Add(RedCircleImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,            Value = RedCircleImageLocation        },
                    new Setter { Property = Image.AspectProperty,            Value = Aspect.AspectFill             },
                    new Setter { Property = Image.WidthRequestProperty,      Value = 20                            },
                    new Setter { Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand },
                    new Setter { Property = Image.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand }
                }
            });

            p_ResourceDictionary.Add(CheckedNoImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = CheckedNoImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit       },
                }
            });

            p_ResourceDictionary.Add(CheckedYesImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = CheckedYesImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit        },
                }
            });

            p_ResourceDictionary.Add(TimerDarkGrayImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,        Value = TimerDarkGrayImageLocation },
                    new Setter { Property = Image.AspectProperty,        Value = Aspect.AspectFit           },
                    new Setter { Property = Image.HeightRequestProperty, Value = 20                         },
                }
            });

            p_ResourceDictionary.Add(TimerWhiteImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                }
            });


            p_ResourceDictionary.Add(TimerLightGrayImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,        Value = TimerLightGrayImageLocation },
                    new Setter { Property = Image.AspectProperty,        Value = Aspect.AspectFit            },
                    new Setter { Property = Image.HeightRequestProperty, Value = 20                          },
                }
            });

            p_ResourceDictionary.Add(TimerRedImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,        Value = TimerRedImageLocation },
                    new Setter { Property = Image.AspectProperty,        Value = Aspect.AspectFit      },
                    new Setter { Property = Image.HeightRequestProperty, Value = 20                    },

                }
            });

            p_ResourceDictionary.Add(TimerOrangeImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,        Value = TimerOrangeImageLocation },
                    new Setter { Property = Image.AspectProperty,        Value = Aspect.AspectFit         },
                    new Setter { Property = Image.HeightRequestProperty, Value = 20                       },

                }
            });

            p_ResourceDictionary.Add(ArrowWhiteBackImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,            Value = backImageLocation             },
                    new Setter { Property = Image.AspectProperty,            Value = Aspect.AspectFit              },
                    new Setter { Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand },
                    new Setter { Property = Image.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand },
                    new Setter { Property = Image.HeightRequestProperty,     Value = 10                            }
                }
            });

            p_ResourceDictionary.Add(HeaderFilterImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,            Value = FilterBlackImageLocation      },
                    new Setter { Property = Image.AspectProperty,            Value = Aspect.AspectFit              },
                    new Setter { Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand },
                    new Setter { Property = Image.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand },
                    new Setter { Property = Image.HeightRequestProperty,     Value = 22                            }
                }
            });

            p_ResourceDictionary.Add(FilterWhiteImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,            Value = FilterWhiteImageLocation      },
                    new Setter { Property = Image.AspectProperty,            Value = Aspect.AspectFit              },
                    new Setter { Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.StartAndExpand  },
                    new Setter { Property = Image.VerticalOptionsProperty,   Value = LayoutOptions.CenterAndExpand },
                    new Setter { Property = Image.HeightRequestProperty,     Value = 22                            }
                }
            });

            p_ResourceDictionary.Add(DefaultCarImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,            Value = DefaultCarImageLocation     },
                    new Setter { Property = Image.AspectProperty,            Value = Aspect.AspectFill           },
                    new Setter { Property = Image.HeightRequestProperty,     Value = 170                         },
                    new Setter { Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand },
                    new Setter { Property = Image.VerticalOptionsProperty,   Value = LayoutOptions.FillAndExpand }
                }
            });

            p_ResourceDictionary.Add(DisquetteImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,        Value = DisquetteImageLocation },
                    new Setter { Property = Image.AspectProperty,        Value = Aspect.AspectFit       },
                    new Setter { Property = Image.HeightRequestProperty, Value = 20                     },
                }
            });

            p_ResourceDictionary.Add(InformationBlackImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,        Value = InformationBlackImageLocation },
                    new Setter { Property = Image.AspectProperty,        Value = Aspect.AspectFit              },
                    new Setter { Property = Image.HeightRequestProperty, Value = 20                            },
                }
            });

            p_ResourceDictionary.Add(InformationBlackSmallImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,        Value = InformationBlackImageLocation },
                    new Setter { Property = Image.AspectProperty,        Value = Aspect.AspectFit              },
                    new Setter { Property = Image.HeightRequestProperty, Value = 20                            },
                }
            });

            p_ResourceDictionary.Add(InformationRedImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,        Value = InformationRedImageLocation },
                    new Setter { Property = Image.AspectProperty,        Value = Aspect.AspectFit            },
                    new Setter { Property = Image.HeightRequestProperty, Value = 20                          },
                }
            });

            p_ResourceDictionary.Add(RadioButtonWhiteImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = RadioButtonWhiteImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit              },
                }
            });

            p_ResourceDictionary.Add(RadioButtonWhiteFilledImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = RadioButtonWhiteFilledImageLoaction },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit                    },
                }
            });

            p_ResourceDictionary.Add(InformationCircleImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = InformationCircleImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit               },
                }
            });

            p_ResourceDictionary.Add(PuceOnImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = PuceOnImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit    },
                    new Setter { Property = Image.HeightRequestProperty, Value = 30           },
                }
            });

            p_ResourceDictionary.Add(PuceOffImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = PuceOffImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit     },
                    new Setter { Property = Image.HeightRequestProperty, Value = 30            },
                }
            });

            p_ResourceDictionary.Add(FlagSouthKoreaImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = FlagSouthKoreaImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit            }
                }
            });

            p_ResourceDictionary.Add(FlagUnitedStatesImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = FlagUnitedStatesImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit              }
                }
            });

            p_ResourceDictionary.Add(CrossButtonImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = CrossButtonImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit         }
                }
            });

            p_ResourceDictionary.Add(WhiteCrossImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = WhiteCrossImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit        }
                }
            });

            p_ResourceDictionary.Add(WhitePlusImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty, Value = WhitePlusImageLocation },
                    new Setter { Property = Image.AspectProperty, Value = Aspect.AspectFit       }
                }
            });

            p_ResourceDictionary.Add(ArrowBackWhiteImageStyle, new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter { Property = Image.SourceProperty,        Value = ArrowBackWhiteImageLocation },
                    new Setter { Property = Image.HeightRequestProperty, Value = 19                          }
                }
            });

            #endregion

            #region Fields

            p_ResourceDictionary.Add(FieldHeaderStyle, new Style(typeof(Field))
            {
                Setters =
                {
                    new Setter{Property = Field.LabelColorProperty, Value = p_Parameters.FieldHeaderTextColor}
                }
            });

            #endregion

            #region Entries

            p_ResourceDictionary.Add(SimpleEntryStyle, new Style(typeof(Entry))
            {
                Setters =
                {
                    new Setter { Property = Entry.TextColorProperty,        Value = p_Parameters.BoxItemTextColor                 },
                    new Setter { Property = Entry.PlaceholderColorProperty, Value = p_Parameters.BoxItemTextColor                 },
                    new Setter { Property = Entry.FontSizeProperty,         Value = (Device.Idiom == TargetIdiom.Phone) ? 18 : 24 }
                }
            });

            #endregion

            #region AdaPickers

            p_ResourceDictionary.Add(MyReportAdaPickerStyle, new Style(typeof(AdaPicker))
            {
                Setters =
                {
                    new Setter { Property = AdaPicker.PlaceholderFontSizeProperty,                 Value = Device.GetNamedSize(NamedSize.Small, typeof(AdaPicker)) },
                    new Setter { Property = AdaPicker.PlaceholderHorizontalTextAllignmentProperty, Value = TextAlignment.Start                                     },
                    new Setter { Property = AdaPicker.PlaceholderVerticalTextAlignmentProperty,    Value = TextAlignment.Center                                    },
                }
            });

            p_ResourceDictionary.Add(MediumTextAdaPickerStyle, new Style(typeof(AdaPicker))
            {
                Setters =
                {
                    new Setter { Property = AdaPicker.PlaceholderFontSizeProperty,              Value = Device.GetNamedSize(NamedSize.Small, typeof(AdaPicker)) },
                    new Setter { Property = AdaPicker.PlaceholderVerticalTextAlignmentProperty, Value = TextAlignment.Center                                    },
                    new Setter { Property = AdaPicker.ForwardImageStackColorProperty,           Value = Color.FromHex("#C2BDBB")                                },
                    new Setter { Property = AdaPicker.PaddingProperty,                          Value = new Thickness(25,0,0,0)                                 },
                    new Setter { Property = AdaPicker.ItemSpacingProperty,                      Value = 10                                                      }
                }
            });

            p_ResourceDictionary.Add(MediumBoldedTextAdaPickerStyle, new Style(typeof(AdaPicker))
            {
                Setters =
                {
                    new Setter { Property = AdaPicker.PlaceholderFontSizeProperty,              Value = Device.GetNamedSize(NamedSize.Small, typeof(AdaPicker)) },
                    new Setter { Property = AdaPicker.PlaceholderVerticalTextAlignmentProperty, Value = TextAlignment.Center                                    },
                    new Setter { Property = AdaPicker.PlaceholderFontAttributeProperty,         Value = FontAttributes.Bold                                     },
                    new Setter { Property = AdaPicker.ForwardImageStackColorProperty,           Value = Color.FromHex("#C2BDBB")                                },
                    new Setter { Property = AdaPicker.PaddingProperty,                          Value = new Thickness(25,0,0,0)                                 },
                    new Setter { Property = AdaPicker.ItemSpacingProperty,                      Value = 10                                                      }
                }
            });

            #endregion

            #region Grids

            p_ResourceDictionary.Add(HeaderGridStyle, new Style(typeof(Grid))
            {
                Setters = { new Setter { Property = Grid.BackgroundColorProperty, Value = p_Parameters.HeaderBackgroundColor } }
            });

            p_ResourceDictionary.Add(BoxItemGridStyle, new Style(typeof(Grid))
            {
                Setters =
                {
                    new Setter { Property = Grid.BackgroundColorProperty,   Value = p_Parameters.BoxItemBackgroundColor },
                    new Setter { Property = Grid.OpacityProperty,           Value = 0.8                                 }
                }
            });

            p_ResourceDictionary.Add(DoubleArrowedGridStyle, new Style(typeof(Grid))
            {
                Setters =
                {
                    new Setter { Property = Grid.BackgroundColorProperty, Value = p_Parameters.SettingsBackgroundColor                                              },
                    new Setter { Property = Grid.PaddingProperty,         Value = (Device.Idiom == TargetIdiom.Phone) ? new Thickness(15, 0) : new Thickness(20, 0) }
                }
            });

            #endregion

            p_ResourceDictionary.Add("GrayBorderStyle", new Style(typeof(StackLayout))
            {
                Setters = 
                {
                    new Setter { Property = StackLayout.BackgroundColorProperty,   Value = Color.FromHex("#C6C6C6")    },
                    new Setter { Property = StackLayout.VerticalOptionsProperty,   Value = LayoutOptions.FillAndExpand },
                    new Setter { Property = StackLayout.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand } 
                }
            });

            #endregion

            #region === Ajouté par Michaël pour les couleursdes text et pickers ===

            // Vu avec Guillaume : On met en fond une couleur qui rappel le thème.

            //p_ResourceDictionary.Add(new Style(typeof(Entry))
            //{
            //    Setters = 
            //    {
            //        new Setter{Property = Entry.BackgroundColorProperty ,  Value = Color.FromHex("FFFFCA9E")},
            //    }
            //});

            p_ResourceDictionary.Add(new Style(typeof(Xamarin.Forms.Picker))
            {
                Setters = 
                    {
                        new Setter{Property = Xamarin.Forms.Picker.BackgroundColorProperty , Value = Color.FromHex("FFDDDDDD")},
                    }
            });


            #endregion

        }
    }
}
