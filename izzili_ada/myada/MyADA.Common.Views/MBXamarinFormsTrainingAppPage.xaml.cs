using Xamarin.Forms;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using MyADA.Common;
using MyADA.Common.DAOServices.Models;
using MyADA.Common.Views.Interfaces;
using CoreSDK;
using MyADA.Common.ViewModels;
using CoreSDK.Controls;
using System.Threading.Tasks;

namespace MyADA.Common.Views
{
    public partial class MBXamarinFormsTrainingAppPage : ContentPage, IMBXamarinFormsTrainingAppPage
	{
		//MyDelegate mbDelegate;
		InboxDataFinished mbInboxDelegate;
		RichMessageDelegate rmDelegate;
		CustomerAttributeDelegate mbCustomerAttributeDelegate;
		CacheUpdateDelegate mbCacheUpdateDelegate;

		public MBXamarinFormsTrainingAppPage()
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);
            //IMobileBridge iMobileBridge = DependencyService.Get<IMobileBridge>();
            //rmDelegate = new RichMessageDelegate();
            ////iMobileBridge.MB_GetInboxData(mbInboxDelegate);
            //iMobileBridge.MB_GetAllRichMessages(rmDelegate);
            var l_BindingContexte = CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>();
            BindingContext = l_BindingContexte;


            //BuildView(l_BindingContexte.CurrentMBPage);
		}

		public class Person
		{
			public string Name { get; set; }
			public DateTime Birthday { get; set; }
		}


        public async void BuildView(string p_TypePage) 
        {

            var l_AudiNewsFlashRepeater = new RepeaterView()
            {
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.FromHex("#C5C9CB"),
                ItemSpacing = 0,
                #region ==Template==
                ItemTemplate = new DataTemplate(() =>
                {

                    var l_MainGrid = new Grid()
                    {
                        HorizontalOptions = LayoutOptions.End,
                        VerticalOptions = LayoutOptions.Fill,
                        BackgroundColor = Color.Transparent,
                        Padding = new Thickness(0, 0, 0, 0),
                        RowSpacing = 0,
                        ColumnDefinitions = { 
                            new ColumnDefinition { Width = new GridLength(10, GridUnitType.Star) } ,
                            new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) } 
                        },
                        RowDefinitions = { 
                            new RowDefinition { Height = GridLength.Auto } ,
                            new RowDefinition { Height = GridLength.Auto } ,
                            new RowDefinition { Height = 35 } 
                        }
                    };

                    #region ==================================== Divider Up ====================================
                    var l_DividerUp = new StackLayout()
                    {
                        BackgroundColor = Color.FromHex("#B8BCBE"),
                        VerticalOptions = LayoutOptions.Fill,
                        HorizontalOptions = LayoutOptions.FillAndExpand
                    };
                    l_DividerUp.SetDynamicResource(StackLayout.HeightRequestProperty, Themes.Theme.OnePixelValue);
                    Grid.SetColumn(l_DividerUp, 0);
                    Grid.SetColumnSpan(l_DividerUp,2);
                    Grid.SetRow(l_DividerUp, 0);
                    l_MainGrid.Children.Add(l_DividerUp);
                    #endregion

                    #region ==================================== Divider Down ====================================
                    var l_DividerDown = new StackLayout()
                    {
                        BackgroundColor = Color.FromHex("#9A9D9E"),
                        VerticalOptions = LayoutOptions.Fill,
                        HorizontalOptions = LayoutOptions.FillAndExpand
                    };
                    l_DividerDown.SetDynamicResource(StackLayout.HeightRequestProperty, Themes.Theme.OnePixelValue);
                    Grid.SetColumn(l_DividerDown, 0);
                    Grid.SetColumnSpan(l_DividerDown, 2);
                    Grid.SetRow(l_DividerDown, 1);
                    l_MainGrid.Children.Add(l_DividerDown);
                    #endregion

                    #region ==================================== Label ====================================
                    var l_NewsTitle = new Label()
                    {
                        TextColor = Color.Black,
                        //FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start
                    };

                    #region ==================================== Open details page ====================================
                        var l_Gesture = new TapGestureRecognizer();
                        var gestureBinding = new Binding();
                        gestureBinding.Source = this;
                        gestureBinding.Path = "BindingContext.ShowRichMessageCommand";
                        l_Gesture.SetBinding(TapGestureRecognizer.CommandProperty, gestureBinding);
                        l_Gesture.SetBinding(TapGestureRecognizer.CommandParameterProperty, ".");
                    #endregion


                    var l_NewsTitleStack = new StackLayout()
                    {
                        Padding = new Thickness(10,0,0,0),
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand
                    };
                    l_NewsTitleStack.Children.Add(l_NewsTitle);
                    l_NewsTitleStack.GestureRecognizers.Add(l_Gesture);

                    Grid.SetColumn(l_NewsTitleStack, 0);
                    Grid.SetRow(l_NewsTitleStack, 2);
                    l_MainGrid.Children.Add(l_NewsTitleStack);

                    l_NewsTitle.SetBinding(Label.TextProperty, "title");
                    #endregion

                    #region ==================================== Save image ====================================
                    var l_SaveImage = new Image()
                    {
                        HorizontalOptions = LayoutOptions.EndAndExpand,
                        VerticalOptions = LayoutOptions.CenterAndExpand                            
                    };
                    l_SaveImage.SetDynamicResource(Image.StyleProperty, Themes.Theme.DisquetteImageStyle);

                    var l_SaveImageStack = new StackLayout()
                    {
                        Padding = new Thickness(0, 0, 0, 10),
                        VerticalOptions = LayoutOptions.CenterAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand
                    };
                    l_SaveImageStack.Children.Add(l_SaveImage);
                    Grid.SetColumn(l_SaveImageStack, 1);
                    Grid.SetRow(l_SaveImageStack, 2);
                    l_MainGrid.Children.Add(l_SaveImageStack);

                    #endregion

                    return l_MainGrid;
                })
                #endregion
            };

            switch (p_TypePage)
            {
                case ViewModels.ViewName.AudiNewsView:
                    l_AudiNewsFlashRepeater.SetBinding(RepeaterView.ItemsSourceProperty, "MBAllRichMessage");
                    break;

                case ViewModels.ViewName.SalesTools:
                    l_AudiNewsFlashRepeater.SetBinding(RepeaterView.ItemsSourceProperty, "MBAllSalesToolsRichMessage");
                    break;

                case ViewModels.ViewName.SelfTraining:
                    l_AudiNewsFlashRepeater.SetBinding(RepeaterView.ItemsSourceProperty, "MBAllSelfTrainingRichMessage");
                    break;
                
                default:
                    l_AudiNewsFlashRepeater.SetBinding(RepeaterView.ItemsSourceProperty, "MBAllRichMessage");
                    break;
            }


            

            // Ajout du repeater dans le mean stack.
            l_MainStackLayout.Children.Add(l_AudiNewsFlashRepeater);

            CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>().IsBusy = false;
            CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>().IsBusy = false;
            CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>().IsBurgerPresented = false;
        }

        #region ==================================== Mobile bridge (Save Example) ====================================
            void OnButtonClicked(object sender, EventArgs args)
		{
			//mbDelegate = new MyDelegate();

			IMobileBridge iMobileBridge = DependencyService.Get<IMobileBridge>();

			//iMobileBridge.MB_SetToken("token");


			rmDelegate = new RichMessageDelegate();
            mbInboxDelegate = new InboxDataFinished();
            iMobileBridge.MB_GetInboxData(mbInboxDelegate);
            iMobileBridge.MB_GetAllRichMessages(rmDelegate);
			//iMobileBridge.MB_GetRichMessageData("{\"id\":2320,\"type\":\"ARTICLE\"}", rmDelegate);
			//iMobileBridge.MB_GetAllRichMessages(rmDelegate);

			//			mbInboxDelegate = new InboxDataFinished();
			//			iMobileBridge.MB_GetInboxData(mbInboxDelegate); 
			//			iMobileBridge.MB_DeleteInboxData(mbInboxDelegate);
			//			iMobileBridge.MB_DeleteInboxItem("9e6F7i2YNyn9My", mbInboxDelegate);


			/*

			mbCustomerAttributeDelegate = new CustomerAttributeDelegate();

			// global CA settings
			//////////////////////////////

						var array = new List<string>();
						var item1 = "1";
						array.Add(item1);
						var item2 = "2";
						array.Add(item2);

						iMobileBridge.MB_CustomerAttributeSetMultiSelect(array, "ms");
						iMobileBridge.MB_CustomerAttributeSetMultiSelect(array, "list_my");
						iMobileBridge.MB_CustomerAttributeSetTime(7200, "last_login");
						iMobileBridge.MB_CustomerAttributeSetDate(1469459750, "date_of_birth");
						iMobileBridge.MB_CustomerAttributeSetSingleSelect("Red", "hair_color");

			iMobileBridge.MB_CustomerAttributeSetText("lior", "last_name");

			iMobileBridge.MB_CustomerAttributeSetBool(true, "bool");
			iMobileBridge.MB_CustomerAttributeSetInt(21, "int_value");
			
			// Set API
			iMobileBridge.MB_SetCustomerAttribute(mbCustomerAttributeDelegate);

			// Get API
			iMobileBridge.MB_GetCustomerAttribute(mbCustomerAttributeDelegate);

			// Clear API
			iMobileBridge.MB_ClearCustomerAttribute(mbCustomerAttributeDelegate);
*/


			mbCacheUpdateDelegate = new CacheUpdateDelegate();
			iMobileBridge.MB_UpdateCacheCustomerAtribute(mbCacheUpdateDelegate);



			var str = iMobileBridge.MB_GetDeviceContext();
		}
        #endregion

		public class MyDelegate
		{
			public void MB_GetPublicPromotionDataFinished(string data)
			{
				List<PublicPromotion> myArray = JsonConvert.DeserializeObject<List<PublicPromotion>>(data);
			}
		}

		// Inbox
		/// ///////////////////
		public class InboxDataFinished
		{
			public void MB_GetInboxDataFinished(string data)
			{
				InboxModel retData = JsonConvert.DeserializeObject<InboxModel>(data);
				int i = 0;
				i++;

			}

			public void MB_InboxDataDeleted(string data)
			{
				//InboxModel retData = JsonConvert.DeserializeObject<InboxModel>(data);
				int i = 0;
				i++;
			}

			public void MB_InboxItemDeleted(string data)
			{
				//InboxModel retData = JsonConvert.DeserializeObject<InboxModel>(data);
				int i = 0;
				i++;
			}
		}


		public class CustomerAttributeDelegate
		{
			public void MB_GetCustomerAttributeDataFinished(string data)
			{
				int i = 0;
				i++;
			}

			public void MB_CustomerAttributeDeleted(string data)
			{
				int i = 0;
				i++;

			}

			public void MB_SetCustomerAttributeDataFinished(string data)
			{
				int i = 0;
				i++;
			}
		}

		public class RichMessageDelegate
		{
            public List<AllRichMessageModel> _GetAllAudiNewsData = new List<AllRichMessageModel>();
            public List<AllRichMessageModel> _GetAllSelfTrainingData = new List<AllRichMessageModel>();
            public List<AllRichMessageModel> _GetAllSalesToolsData = new List<AllRichMessageModel>();
			public void MB_GetRichMessageData(string data)
			{
				RichMessageModel retData = JsonConvert.DeserializeObject<RichMessageModel>(data);
				int i = 0;
				i++;
			}

			public void MB_GetAllRichMessages(string data)
			{
                //DeserializeObject
                List<AllRichMessageModel> l_GetAllData = JsonConvert.DeserializeObject<List<AllRichMessageModel>>(data);
                _GetAllAudiNewsData.Clear();
                _GetAllSelfTrainingData.Clear();
                _GetAllSalesToolsData.Clear();

                var l_HomeViewModel = CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>();
                // List of Audi News
                _GetAllAudiNewsData.AddRange(l_GetAllData.FindAll(x => x.tags[0].slug.Equals(ViewModels.Constants.RichMessageTagNews)));

                // List of Self Training
                _GetAllSelfTrainingData.AddRange(l_GetAllData.FindAll(x => x.tags[0].slug.Equals(ViewModels.Constants.RichMessageTagTraining)));

                // List of Sales Tools
                _GetAllSalesToolsData.AddRange(l_GetAllData.FindAll(x => x.tags[0].slug.Equals(ViewModels.Constants.RichMessageTagSalesTool)));

                // Audi News
                l_HomeViewModel.MBAllRichMessage.Clear();
                foreach (var item in _GetAllAudiNewsData)
                {
                    l_HomeViewModel.MBAllRichMessage.Add(new RichMessageItem().SetRichMessageEncapsulation(item));
                }

                // Self training
                l_HomeViewModel.MBAllSelfTrainingRichMessage.Clear();
                foreach (var item in _GetAllSelfTrainingData)
                {
                    l_HomeViewModel.MBAllSelfTrainingRichMessage.Add(new RichMessageItem().SetRichMessageEncapsulation(item));
                }

                // Sales tools
                l_HomeViewModel.MBAllSalesToolsRichMessage.Clear();
                foreach (var item in _GetAllSalesToolsData)
                {
                    l_HomeViewModel.MBAllSalesToolsRichMessage.Add(new RichMessageItem().SetRichMessageEncapsulation(item));
                }

                l_HomeViewModel.IsVisibleAudiNewsNotif = (l_HomeViewModel.MBAllRichMessage.Count > 0) ? true : false;
                l_HomeViewModel.OverHundredAudiNewsNotif = (l_HomeViewModel.MBAllRichMessage.Count > 99) ? true : false;
                l_HomeViewModel.AudiNewsNotif = (l_HomeViewModel.OverHundredAudiNewsNotif) ? "99" : l_HomeViewModel.MBAllRichMessage.Count.ToString();

                l_HomeViewModel.IsVisibleSalesToolNotif = (l_HomeViewModel.MBAllSalesToolsRichMessage.Count > 0) ? true : false;
                l_HomeViewModel.OverHundredSalesToolNotif = (l_HomeViewModel.MBAllSalesToolsRichMessage.Count > 99) ? true : false;
                l_HomeViewModel.SalesToolNotif = (l_HomeViewModel.OverHundredSalesToolNotif) ? "99" : l_HomeViewModel.MBAllSalesToolsRichMessage.Count.ToString();

                l_HomeViewModel.IsVisibleSelfTrainingNotif = (l_HomeViewModel.MBAllSelfTrainingRichMessage.Count > 0) ? true : false;
                l_HomeViewModel.OverHundredSelfTrainingNotif = (l_HomeViewModel.MBAllSelfTrainingRichMessage.Count > 99) ? true : false;
                l_HomeViewModel.SelfTrainingNotif = (l_HomeViewModel.OverHundredSelfTrainingNotif) ? "99" : l_HomeViewModel.MBAllSelfTrainingRichMessage.Count.ToString();

				int i = 0;
				i++;
			}

		}

		// delegate from UpdateCustomerAttribute caching
		public class CacheUpdateDelegate
		{
			public void MB_UpdateCacheFinished()
			{
				IMobileBridge iMobileBridge = DependencyService.Get<IMobileBridge>();
				CustomerAttributeDelegate mbCustomerAttributeDelegate = new CustomerAttributeDelegate();
				iMobileBridge.MB_GetCustomerAttribute(mbCustomerAttributeDelegate);
			}

		}

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>().IsBusy = false;
            CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>().IsBusy = false;
            CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>().IsBurgerPresented = false;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>().IsBusy = false;
            CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>().IsBusy = false;
            CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>().IsBurgerPresented = false;

        }

	}
}

