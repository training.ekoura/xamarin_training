﻿using CoreSDK;
using MyADA.Common.ViewModels;
using MyADA.Common.Views.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyADA.Common.Views
{
    public partial class RSSFeedView : ContentPage, IRSSFeedView
    {
        IEnumerable<Post> posts;

        public RSSFeedView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);
        }

        private void backButton_Tapped(object sender, EventArgs e)
        {
            CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection("IMyRSSView", BindingContext);
        }

        //private async void FeedRSS_Tapped(object sender, EventArgs e)
        //{
        //    StackLayout stack = (StackLayout)sender;
        //    Label lb = new Label();

        //    foreach(VisualElement ve in stack.Children)
        //    {
        //        if (ve is Label) lb = (Label)ve;
        //    }

        //    foreach(Post post in posts)
        //    {
        //        if(post.Title == lb.Text)
        //        {
        //            //CoreSDK.Framework.Services.InteractionService().Open("IFeedRSSDetailView");
        //            await Navigation.PushAsync(new FeedRSSDetailView(post));
        //        }
        //    }
        //}

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ((BurgerViewModel)BindingContext).IsBusy = false;
            //CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>().IsBusy = false;
        }
    }
}
