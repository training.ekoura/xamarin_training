﻿using MyADA.Common.Views.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using CoreSDK;

namespace MyADA.Common.Views
{
    public partial class ManageRSSView : ContentPage, IManageRSSView
    {
        public ManageRSSView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);

            repeaterManageRSS.ItemsSource = new List<string> { "" };
        }

        private void btAdd_Tapped(object sender, EventArgs e)
        {
            CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection("IEnterRssUrlView");
        }

        private void backButton_Tapped(object sender, EventArgs e)
        {
            CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection("IEnterRssUrlView");
        }
    }
}
