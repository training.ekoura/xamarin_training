﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;
using Xamarin.Forms;

using MyADA.Common.Views;
using MyADA.Common.Views.Interfaces;
using MyADA.Common.Views.Properties;
using MyADA.Common.ViewModels;

namespace MyADA.Common.Views
{
    public partial class BurgerView : ContentPage, IBurgerView
    {
        public BurgerView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            BindingContext = CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>();



            this.Watch<MyADA.Common.ViewModels.BurgerViewModel>((BurgerVM) =>
            {
                if (BurgerVM == null) return;
                {
                    var mdp = this.Parent as MasterDetailView;
                    if (mdp != null) mdp.IsPresented = BurgerVM.IsBurgerPresented;
                    else return;
                }

                //mdp.SetBinding<MySodexo.Common.ViewModels.BurgerViewModel>(MasterDetailView.IsVisibleProperty, vm => BurgerVM.IsBurgerPresented);
            }, "IsBurgerPresented");
        }
    }
}
