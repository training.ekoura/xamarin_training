﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyADA.Common.Views.Interfaces;

using Xamarin.Forms;

namespace MyADA.Common.Views
{
    public partial class InboxDetailView : ContentPage, IInboxDetailView
    {
        public InboxDetailView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);
        }
    }
}
