﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyADA.Common.Views.Interfaces;

using Xamarin.Forms;
using CoreSDK.Controls;
using CoreSDK;
using MyADA.Common.Services.Models;
using MyADA.Common.ViewModels;

namespace MyADA.Common.Views
{
    public partial class ChangeRequestView : ContentPage, IChangeRequestView
    {
        public ChangeRequestView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);
        }

        private void OnChangeClicked(object sender, EventArgs e)
        {
            if (BindingContext != null) ((SettingsViewModel)BindingContext).ChangeRequestCommand.Execute(((Grid)sender));
        }

        private void OnCreateClicked(object sender, EventArgs e)
        {
            if (BindingContext != null) ((SettingsViewModel)BindingContext).CreateRequestCommand.Execute(((Grid)sender));
        }

        private void OnDeleteClicked(object sender, EventArgs e)
        {
            if (BindingContext != null) ((SettingsViewModel)BindingContext).DeleteRequestCommand.Execute(((Grid)sender));
        }

        private void backButton_Tapped(object sender, EventArgs e)
        {
            CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection(MyADA.Common.ViewModels.ViewName.SettingsProfileView, BindingContext);
        }
    }
}