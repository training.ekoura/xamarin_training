﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;

using Xamarin.Forms;

namespace MyADA.Common.Views.ContentViews
{
    public partial class PickerBackButton : ContentView
    {
        public PickerBackButton()
        {
            InitializeComponent();
            var l_BackStackGesture = new TapGestureRecognizer();
            l_BackStackGesture.Tapped += l_BackStackGesture_Tapped;
            BackStack.GestureRecognizers.Add(l_BackStackGesture);
        }

        void l_BackStackGesture_Tapped(object sender, EventArgs e)
        {
            CoreSDK.Framework.Services.InteractionService().Close();
        }
    }
}
