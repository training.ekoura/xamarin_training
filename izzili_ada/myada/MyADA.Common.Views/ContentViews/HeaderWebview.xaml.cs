﻿using CoreSDK;
using MyADA.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyADA.Common.Views.ContentViews
{
    public partial class HeaderWebview : ContentView
    {
        public bool Drawer;

        public HeaderWebview()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>();
        }

        private void BackButton_Tapped(object sender, EventArgs e)
        {
            CoreSDK.Framework.Services.InteractionService().Close();
        }

        public void OnToggleRequest()
        {
            Drawer = !Drawer;
        }
    }
}
