﻿using CoreSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//using CoreSDK.Controls;

using MyADA.Common.Views.Interfaces;

using Xamarin.Forms;
using MyADA.Common.ViewModels;

namespace MyADA.Common.Views.ContentViews
{
    public partial class FilterView : ContentView, IFilterView
    {
        public FilterView()
        {
            InitializeComponent();
        }

        private void CloseFilter(object sender, EventArgs e)
        {
            this.IsVisible = false;
            var l_VM = CoreSDK.Framework.Services.Container().Resolve<ILeadViewModel>();
            l_VM.CloseFilterViewAndLoadDataCommand.Execute(null);
        }

        private void SelectControl_Tapped(object sender, EventArgs e)
        {

        }

        public void TappedSecondItemRepeater(object sender, EventArgs e)
        {
            // Image Uncheck
            if (((Image)sender).StyleId == null || ((Image)sender).StyleId == "1")
            {
                
                ((Image)sender).SetDynamicResource(Image.StyleProperty, Themes.Theme.RadioButtonWhiteImageStyle);
                ((Image)sender).StyleId = "0";
            }
            // Image checked
            else
            {
                ((Image)sender).SetDynamicResource(Image.StyleProperty, Themes.Theme.RadioButtonWhiteFilledImageStyle);
                ((Image)sender).StyleId = "1";
            }
        }

        public void TappedItemRepeater(object sender, EventArgs e)
        {
            // Image Uncheck
            if (((Image)sender).StyleId == null || ((Image)sender).StyleId == "0")
            {
                ((Image)sender).SetDynamicResource(Image.StyleProperty, Themes.Theme.RadioButtonWhiteFilledImageStyle);
                ((Image)sender).StyleId = "1";
            }
            // Image checked
            else
            {
                ((Image)sender).SetDynamicResource(Image.StyleProperty, Themes.Theme.RadioButtonWhiteImageStyle);
                ((Image)sender).StyleId = "0";
            }
        }
    }
}
