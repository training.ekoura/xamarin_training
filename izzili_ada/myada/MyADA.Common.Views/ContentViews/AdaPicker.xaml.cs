﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using CoreSDK;
using CoreSDK.Controls;
using System.Windows.Input;

namespace MyADA.Common.Views.ContentViews
{
    public partial class AdaPicker : ContentView
    {
        public ListView _ListView;
        public AdaPicker()
        {
            InitializeComponent();

            var tg = new TapGestureRecognizer();
            tg.Tapped += (s, e) => { OpenTemplateView(); };
            mainGrid.GestureRecognizers.Add(tg);
            forwardImageStack.GestureRecognizers.Add(tg);
        }

        #region === PickerType ===

        public static readonly BindableProperty PickerTypeProperty = BindableProperty.Create<AdaPicker, string>(p => p.PickerType, default(string), propertyChanged: (d, o, n) => (d as AdaPicker).PickerType_Changed(o, n));

        private void PickerType_Changed(string oldvalue, string newvalue)
        {
            //mainText.Text = newvalue;
        }

        public string PickerType
        {
            get { return (string)GetValue(PickerTypeProperty); }
            set { SetValue(PickerTypeProperty, value); }
        }

        #endregion

        #region === Placeholder ===

        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create<AdaPicker, string>(p => p.Placeholder, default(string), propertyChanged: (d, o, n) => (d as AdaPicker).Placeholder_Changed(o, n));

        private void Placeholder_Changed(string oldvalue, string newvalue)
        {
            mainText.Text = newvalue;
        }

        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        #endregion

        #region === PlaceholderBackGroundColor ===

        public static readonly BindableProperty PlaceholderBackGroundColorProperty = BindableProperty.Create<AdaPicker, Color>(p => p.PlaceholderBackGroundColor, default(Color), propertyChanged: (d, o, n) => (d as AdaPicker).PlaceholderBackGroundColor_Changed(o, n));

        private void PlaceholderBackGroundColor_Changed(Color oldvalue, Color newvalue)
        {
            mainGrid.BackgroundColor = newvalue;
        }

        [TypeConverter(typeof(ColorTypeConverter))]
        public Color PlaceholderBackGroundColor
        {
            get { return (Color)GetValue(PlaceholderBackGroundColorProperty); }
            set { SetValue(PlaceholderBackGroundColorProperty, value); }
        }

        #endregion

        #region === PlaceholderFontSize ===

        public static readonly BindableProperty PlaceholderFontSizeProperty = BindableProperty.Create<AdaPicker, double>(p => p.PlaceholderFontSize, default(double), propertyChanged: (d, o, n) => (d as AdaPicker).PlaceholderFontSize_Changed(o, n));

        /// <summary>
        /// Pour mettre à jour les bonnes couleurs des puces.
        /// </summary>
        /// <param name="oldvalue"></param>
        /// <param name="newvalue"></param>
        private void PlaceholderFontSize_Changed(double oldvalue, double newvalue)
        {
            mainText.FontSize = newvalue;
        }

        public double PlaceholderFontSize
        {
            get { return (double)GetValue(PlaceholderFontSizeProperty); }
            set { SetValue(PlaceholderFontSizeProperty, value); }
        }

        #endregion

        #region === PlaceholderFontAttribute ===

        public static readonly BindableProperty PlaceholderFontAttributeProperty = BindableProperty.Create<AdaPicker, FontAttributes>(p => p.PlaceholderFontAttribute, FontAttributes.None, propertyChanged: (d, o, n) => (d as AdaPicker).PlaceholderFontAttribute_Changed(o, n));

        /// <summary>
        /// Pour mettre à jour les bonnes couleurs des puces.
        /// </summary>
        /// <param name="oldvalue"></param>
        /// <param name="newvalue"></param>
        private void PlaceholderFontAttribute_Changed(FontAttributes oldvalue, FontAttributes newvalue)
        {
            mainText.FontAttributes = newvalue;
        }

        public FontAttributes PlaceholderFontAttribute
        {
            get { return (FontAttributes)GetValue(PlaceholderFontAttributeProperty); }
            set { SetValue(PlaceholderFontAttributeProperty, value); }
        }

        #endregion

        #region === PlaceholderHorizontalTextAllignment ===

        public static readonly BindableProperty PlaceholderHorizontalTextAllignmentProperty = BindableProperty.Create<AdaPicker, TextAlignment>(p => p.PlaceholderHorizontalTextAllignment, TextAlignment.Start, propertyChanged: (d, o, n) => (d as AdaPicker).PlaceholderHorizontalTextAllignment_Changed(o, n));

        /// <summary>Alignement horizontal du texte du bouton.</summary>
        /// <param name="oldvalue">Ancienne valeur.</param>
        /// <param name="newvalue">Nouvelle valeur.</param>
        private void PlaceholderHorizontalTextAllignment_Changed(TextAlignment oldvalue, TextAlignment newvalue)
        {
            mainText.HorizontalTextAlignment = newvalue;
        }

        public TextAlignment PlaceholderHorizontalTextAllignment
        {
            get { return (TextAlignment)GetValue(PlaceholderHorizontalTextAllignmentProperty); }
            set { SetValue(PlaceholderHorizontalTextAllignmentProperty, value); }
        }

        #endregion

        #region === PlaceholderVerticalTextAlignment ===

        public static readonly BindableProperty PlaceholderVerticalTextAlignmentProperty = BindableProperty.Create<AdaPicker, TextAlignment>(p => p.PlaceholderVerticalTextAlignment, TextAlignment.Center, propertyChanged: (d, o, n) => (d as AdaPicker).PlaceholderVerticalTextAlignment_Changed(o, n));

        /// <summary>Alignement horizontal du texte du bouton.</summary>
        /// <param name="oldvalue">Ancienne valeur.</param>
        /// <param name="newvalue">Nouvelle valeur.</param>
        private void PlaceholderVerticalTextAlignment_Changed(TextAlignment oldvalue, TextAlignment newvalue)
        {
            mainText.VerticalTextAlignment = newvalue;
        }

        public TextAlignment PlaceholderVerticalTextAlignment
        {
            get { return (TextAlignment)GetValue(PlaceholderVerticalTextAlignmentProperty); }
            set { SetValue(PlaceholderVerticalTextAlignmentProperty, value); }
        }

        #endregion

        #region === PlaceholderPadding ===

        public static readonly BindableProperty PlaceholderPaddingProperty = BindableProperty.Create<AdaPicker, Thickness>(p => p.PlaceholderPadding, default(Thickness), propertyChanged: (d, o, n) => (d as AdaPicker).PlaceholderPadding_Changed(o, n));

        private void PlaceholderPadding_Changed(Thickness oldvalue, Thickness newvalue)
        {
            mainTextStack.Padding = newvalue;
        }

        [TypeConverter(typeof(ThicknessTypeConverter))]
        public Thickness PlaceholderPadding
        {
            get { return (Thickness)GetValue(PlaceholderPaddingProperty); }
            set { SetValue(PlaceholderPaddingProperty, value); }
        }

        #endregion

        #region === ForwardImageStackColor ===

        public static readonly BindableProperty ForwardImageStackColorProperty = BindableProperty.Create<AdaPicker, Color>(p => p.ForwardImageStackColor, default(Color), propertyChanged: (d, o, n) => (d as AdaPicker).ForwardImageStackColor_Changed(o, n));

        private void ForwardImageStackColor_Changed(Color oldvalue, Color newvalue)
        {
            forwardImageStack.BackgroundColor = newvalue;
        }

        [TypeConverter(typeof(ColorTypeConverter))]
        public Color ForwardImageStackColor
        {
            get { return (Color)GetValue(ForwardImageStackColorProperty); }
            set { SetValue(ForwardImageStackColorProperty, value); }
        }

        #endregion

        #region === HeaderText ===

        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create<AdaPicker, string>(p => p.HeaderText, default(string), propertyChanged: (d, o, n) => (d as AdaPicker).HeaderText_Changed(o, n));

        private void HeaderText_Changed(string oldvalue, string newvalue)
        {
        }

        public string HeaderText
        {
            get { return (string)GetValue(HeaderTextProperty); }
            set { SetValue(HeaderTextProperty, value); }
        }

        #endregion

        #region === ItemTemplate ===

        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create<AdaPicker, DataTemplate>(p => p.ItemTemplate, default(DataTemplate), propertyChanged: (d, o, n) => (d as AdaPicker).ItemTemplate_Changed(o, n));

        private void ItemTemplate_Changed(DataTemplate oldvalue, DataTemplate newvalue)
        {
        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        #endregion

        #region === ItemSource ===

        public static readonly BindableProperty ItemSourceProperty = BindableProperty.Create<AdaPicker, object>(p => p.ItemSource, default(object), propertyChanged: (d, o, n) => (d as AdaPicker).ItemSource_Changed(o, n));

        private void ItemSource_Changed(object oldvalue, object newvalue)
        {

        }

        public object ItemSource
        {
            get { return (object)GetValue(ItemSourceProperty); }
            set { SetValue(ItemSourceProperty, value); }
        }
        #endregion

        #region === ItemSpacing ===

        public static readonly BindableProperty ItemSpacingProperty = BindableProperty.Create<AdaPicker, double>(p => p.ItemSpacing, default(double), propertyChanged: (d, o, n) => (d as AdaPicker).ItemSpacing_Changed(o, n));

        /// <summary>
        /// </summary>
        /// <param name="oldvalue"></param>
        /// <param name="newvalue"></param>
        private void ItemSpacing_Changed(double oldvalue, double newvalue)
        {
        }

        public double ItemSpacing
        {
            get { return (double)GetValue(ItemSpacingProperty); }
            set { SetValue(ItemSpacingProperty, value); }
        }

        #endregion

        #region === TapCommand ===

        public static readonly BindableProperty TapCommandProperty = BindableProperty.Create<AdaPicker, ICommand>(p => p.TapCommand, default(ICommand), propertyChanged: (d, o, n) => (d as AdaPicker).TapCommand_Changed(o, n));

        private void TapCommand_Changed(ICommand oldvalue, ICommand newvalue)
        {
        }

        public ICommand TapCommand
        {
            get { return (ICommand)GetValue(TapCommandProperty); }
            set { SetValue(TapCommandProperty, value); }
        }

        #endregion

        #region === TapCommandParameter ===

        public static readonly BindableProperty TapCommandParameterProperty = BindableProperty.Create<AdaPicker, object>(p => p.TapCommandParameter, default(object), propertyChanged: (d, o, n) => (d as AdaPicker).TapCommandParameter_Changed(o, n));

        private void TapCommandParameter_Changed(object oldvalue, object newvalue)
        {

        }

        public object TapCommandParameter
        {
            get { return (object)GetValue(TapCommandParameterProperty); }
            set { SetValue(TapCommandParameterProperty, value); }
        }

        #endregion

        #region === InnerButtonText ===

        public static readonly BindableProperty InnerButtonTextProperty = BindableProperty.Create<AdaPicker, string>(p => p.InnerButtonText, default(string), propertyChanged: (d, o, n) => (d as AdaPicker).InnerButtonText_Changed(o, n));

        private void InnerButtonText_Changed(string oldvalue, string newvalue)
        {
        }

        public string InnerButtonText
        {
            get { return (string)GetValue(InnerButtonTextProperty); }
            set { SetValue(InnerButtonTextProperty, value); }
        }

        #endregion

        void OpenTemplateView()
        {
            //<Grid.Padding>
            //    <OnPlatform x:TypeArguments="Thickness" iOS="0, 20, 0, 0" Android="0" WinPhone="0" />
            //</Grid.Padding>

            #region Other Header

            OtherHeader header = new OtherHeader();

            #endregion Other Header

            #region BlackHeader

            var blackHeader = new Grid()
            {
                VerticalOptions=LayoutOptions.Fill,
                HorizontalOptions=LayoutOptions.FillAndExpand,
                ColumnSpacing=0,
                ColumnDefinitions = 
                {
                    new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition() { Width = new GridLength(6, GridUnitType.Star) },
                    new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
                }
            };
            blackHeader.SetDynamicResource(Grid.BackgroundColorProperty, Themes.Theme.BoxTitleBackgroundColor);
            blackHeader.SetDynamicResource(Grid.HeightRequestProperty, Themes.Theme.TITLE_HEIGHT);

            #region backImage

            var backImage = new ContentViews.PickerBackButton();
            //backImage.SetDynamicResource(ContentViews.PickerBackButton.HeightRequestProperty, Themes.Theme.PickerBackImageBoxHeight);
            var backGesture = new TapGestureRecognizer();
            backGesture.Tapped += (s, e) =>
            {
                var interactionService = CoreSDK.Framework.Services.InteractionService() as NavigationPageInteractionService;
                interactionService.Close();
            };

            Grid.SetColumn(backImage, 1);
            Grid.SetRow(backImage, 1);

            Grid gridBackImage = new Grid()
            {
                RowDefinitions = 
                {
                    new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) },
                    new RowDefinition() { Height = new GridLength(2, GridUnitType.Star) },
                    new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) },
                },
                ColumnDefinitions = 
                {
                    new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition() { Width = (Device.Idiom == TargetIdiom.Phone) ? new GridLength(3, GridUnitType.Star) : new GridLength(2, GridUnitType.Star) },
                    new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
                },
                RowSpacing = 0,
                ColumnSpacing = 0,
                Children = { backImage }
            };

            Grid.SetColumn(gridBackImage, 0);
            gridBackImage.GestureRecognizers.Add(backGesture);
            blackHeader.Children.Add(gridBackImage);

            #endregion

            #region HeaderText

            var blackHeaderText = new NormalFontLabel()
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,
                Text = HeaderText,
            };
            blackHeaderText.SetDynamicResource(Label.StyleProperty, Themes.Theme.BoxTitleStyle);

            var stackTitle = new StackLayout()
            {
                Children = { blackHeaderText },
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            Grid.SetColumn(stackTitle, 1);
            blackHeader.Children.Add(stackTitle);

            #endregion

            #endregion

            #region RepeaterList
            
            var itemList = new CoreSDK.Controls.RepeaterView()
            {
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.FillAndExpand,
            };
            itemList.ItemSpacing  = ItemSpacing;
            itemList.ItemsSource  = ItemSource;
            itemList.ItemTemplate = ItemTemplate;

            // TODO-AFA : GAV 07/08/2016=> I add this part because this picker doesn't work with repeaterView.
            _ListView = new ListView(ListViewCachingStrategy.RetainElement);
            {
                _ListView.HasUnevenRows = true;
                _ListView.SeparatorVisibility = SeparatorVisibility.None;
                _ListView.VerticalOptions = LayoutOptions.Fill;
                _ListView.HorizontalOptions = LayoutOptions.FillAndExpand;
                // On bind les propriétés.
                _ListView.SetBinding(ListView.ItemsSourceProperty, new Binding(AdaPicker.ItemSourceProperty.PropertyName, BindingMode.TwoWay, null, null, null, this));
                // Contruction du template de ListView si nécessaire.
                if (ItemTemplate != null)
                {
                    _ListView.ItemTemplate = new DataTemplate(() =>
                    {
                        var l_ViewCell = new ViewCell();
                        l_ViewCell.View = ItemTemplate.CreateContent() as View;
                        return l_ViewCell;
                    });
                }
            }
            if (Device.OS == TargetPlatform.iOS) _ListView.BackgroundColor = Color.Transparent;

            // Semble ne pas marcher
            _ListView.ItemTapped += (object sender, ItemTappedEventArgs e) =>
            {
                if (e.Item == null) return;
                ((ListView)sender).SelectedItem = null;
            };

            var listScrollView = new ScrollView()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
            };

            switch (Device.OS)
            {
                case TargetPlatform.Android:
                    listScrollView.Content = _ListView;
                    break;

                case TargetPlatform.WinPhone:
                    listScrollView.Content = itemList;
                    break;
                case TargetPlatform.iOS:
                    listScrollView.Content = _ListView;
                    break;
            }            

            #endregion

            #region MainGrid

            Grid mainGrid;

            if(PickerType != "UnifiedBorders")
            {
                mainGrid = new Grid()
                {
                    RowDefinitions =
                    {
                        new RowDefinition() { Height = GridLength.Auto                      },
                        new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) },
                        new RowDefinition() { Height = GridLength.Auto                      }
                    },
                    Padding = (Device.Idiom == TargetIdiom.Phone) ? new Thickness(20) : new Thickness(40),
                    RowSpacing = 0,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Children = { blackHeader, listScrollView }
                };
                //mainGrid.SetDynamicResource(Grid.BackgroundColorProperty, Themes.Theme.BoxItemBackgroundColor);

                Grid.SetRow(blackHeader, 0);
                Grid.SetRow(listScrollView, 1);
            }
            else
            {
                //---------------------------------------------------------------------
                // Borders

                StackLayout stackLeft = new StackLayout();
                StackLayout stackRight = new StackLayout();
                StackLayout stackBottom = new StackLayout();

                stackLeft.SetDynamicResource(StackLayout.StyleProperty, "GrayBorderStyle");
                stackRight.SetDynamicResource(StackLayout.StyleProperty, "GrayBorderStyle");
                stackBottom.SetDynamicResource(StackLayout.StyleProperty, "GrayBorderStyle");

                mainGrid = new Grid()
                {
                    ColumnDefinitions = 
                    {
                        new ColumnDefinition { Width = 0.7                                  },
                        new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                        new ColumnDefinition { Width = 0.7                                  },
                    },
                    RowDefinitions =
                    {
                        new RowDefinition { Height = (Device.Idiom == TargetIdiom.Tablet && Device.OS == TargetPlatform.iOS) ? new GridLength(1, GridUnitType.Absolute) : new GridLength(0.7, GridUnitType.Absolute) },
                        new RowDefinition { Height = new GridLength(1, GridUnitType.Star)                                                                                                                            },
                        new RowDefinition { Height = (Device.Idiom == TargetIdiom.Tablet && Device.OS == TargetPlatform.iOS) ? new GridLength(1, GridUnitType.Absolute) : new GridLength(0.7, GridUnitType.Absolute) },
                    },
                    Children = { stackLeft, stackRight, stackBottom },
                    ColumnSpacing = 0,
                    RowSpacing = 0,
                    Padding = (Device.Idiom == TargetIdiom.Phone) ? new Thickness(20) : new Thickness(40)
                };

                Grid.SetColumn(stackLeft, 0);
                Grid.SetRow(stackLeft, 0);
                Grid.SetRowSpan(stackLeft, 3);

                Grid.SetColumn(stackRight, 2);
                Grid.SetRow(stackRight, 0);
                Grid.SetRowSpan(stackRight, 3);

                Grid.SetColumn(stackBottom, 1);
                Grid.SetRow(stackBottom, 2);

                //----------------------------------------------------------------
                // Content

                Grid contentGrid = new Grid()
                {
                    RowDefinitions =
                    {
                        new RowDefinition() { Height = GridLength.Auto                      },
                        new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) },
                        new RowDefinition() { Height = GridLength.Auto                      }
                    },
                    Padding = 0,
                    RowSpacing = 0,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Children = { blackHeader, listScrollView }
                };
                contentGrid.SetDynamicResource(Grid.BackgroundColorProperty, Themes.Theme.BoxItemBackgroundColor);

                Grid.SetRow(blackHeader, 0);
                Grid.SetRow(listScrollView, 1);

                //----------------------------------------------------------------
                // Add contentGrid to mainGrid

                Grid.SetColumn(contentGrid, 1);
                Grid.SetRow(contentGrid, 1);
                mainGrid.Children.Add(contentGrid);
            }


            #endregion

            #region TemplateGrid

            Grid templateGrid = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition() { Height = new GridLength(50, GridUnitType.Absolute) },
                    new RowDefinition() { Height = new GridLength(1, GridUnitType.Star)      },
                },
                Children = { header, mainGrid },
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions   = LayoutOptions.FillAndExpand,
                Padding = (Device.OS == TargetPlatform.iOS) ? new Thickness(0, 20, 0, 0) : new Thickness(0)
            };

            Grid.SetRow(header, 0);
            Grid.SetRow(mainGrid, 1);

            #endregion TemplateGrid

            #region TemplatePage

            var TemplatePage = new ContentPage();
            TemplatePage.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);
            TemplatePage.Content = templateGrid;

            //null all items to conserve memory
            TemplatePage.Disappearing += (s, e) =>
            {
                blackHeader = null;
                backImage = null;
                blackHeaderText = null;
                _ListView = null;
                itemList = null;
                listScrollView = null;
                mainGrid = null;
                templateGrid = null;
                TemplatePage = null;
            };

            #endregion

            #region Bouton Valider

            if (PickerType == "Advanced")
            {
                #region Design and layout

                var designGrid = new Grid()
                {
                    ColumnDefinitions = 
                    {
                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star)   },
                        new ColumnDefinition() { Width = new GridLength(2.5, GridUnitType.Star) },
                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star)   },
                    },
                    ColumnSpacing = 0,
                    Padding = new Thickness(10),
                    VerticalOptions = LayoutOptions.Fill,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                };
                designGrid.SetDynamicResource(Grid.HeightRequestProperty, Themes.Theme.BORDER_BOX_HEIGHT);

                #region Black Background

                var colouredGrid = new Grid()
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Padding = new Thickness(10,10,10,5),
                    RowDefinitions = 
                    {
                        new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) },
                        new RowDefinition() { Height = GridLength.Auto },
                    },
                };
                colouredGrid.SetDynamicResource(Grid.StyleProperty, Themes.Theme.DoubleArrowedGridStyle);

                #endregion

                #region Label & Image

                var btnLabel = new Label() { HorizontalOptions = LayoutOptions.StartAndExpand, VerticalOptions = LayoutOptions.CenterAndExpand };
                btnLabel.Text = InnerButtonText;
                btnLabel.SetDynamicResource(Label.StyleProperty, Themes.Theme.SendRequestLabelStyle);

                var doubleImage = new Image() { HorizontalOptions = LayoutOptions.EndAndExpand, VerticalOptions = LayoutOptions.CenterAndExpand };
                doubleImage.SetDynamicResource(Image.StyleProperty, Themes.Theme.ArrowDoubleWhiteImageStyle);

                Grid.SetColumn(btnLabel, 0);
                Grid.SetColumn(doubleImage, 1);
                colouredGrid.Children.Add(btnLabel);
                colouredGrid.Children.Add(doubleImage);

                #endregion

                #endregion

                #region Gesture

                var btnGesture = new TapGestureRecognizer();
                btnGesture.Tapped += (s, e) => { if (TapCommand != null) TapCommand.Execute(TapCommandParameter); };
                colouredGrid.GestureRecognizers.Add(btnGesture);

                #endregion

                Grid.SetColumn(colouredGrid, 1);
                Grid.SetRow(designGrid, 2);
                designGrid.Children.Add(colouredGrid);
                mainGrid.Children.Add(designGrid);
            }

            #endregion
            
            var l_InteractionService = CoreSDK.Framework.Services.InteractionService() as NavigationPageInteractionService;

            l_InteractionService.Open(TemplatePage, true);
			NavigationPage.SetHasNavigationBar(TemplatePage, false);
			NavigationPage.SetHasBackButton(TemplatePage, false);
        }
    }
}
