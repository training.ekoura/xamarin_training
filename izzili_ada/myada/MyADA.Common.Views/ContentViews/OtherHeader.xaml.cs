﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CoreSDK;

using Xamarin.Forms;
using MyADA.Common.ViewModels;

namespace MyADA.Common.Views.ContentViews
{
    public partial class OtherHeader : ContentView
    {
        public bool Drawer;

        public OtherHeader()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            //BindingContext = CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>();
        }

        public void OnToggleRequest()
        {
            Drawer = !Drawer;
        }
    }
}
