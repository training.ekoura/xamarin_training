﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MyADA.Common.Views.ContentViews
{
    public partial class AdaFilter : ContentView
    {
        public AdaFilter()
        {
            InitializeComponent();
        }

        private void imgFilter_Tapped(object sender, EventArgs e)
        {
            ContentPage page = this.Parent.Parent as ContentPage;
            FilterView filter = page.Content.FindByName<FilterView>("filter");
            filter.IsVisible = true;
        }
    }
}
