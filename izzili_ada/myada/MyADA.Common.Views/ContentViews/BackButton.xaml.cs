﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

using CoreSDK;

namespace MyADA.Common.Views.ContentViews
{
    public partial class BackButton : ContentView
    {
        public BackButton()
        {
            InitializeComponent();
            var l_BackStackGesture = new TapGestureRecognizer();
            l_BackStackGesture.Tapped += l_BackStackGesture_Tapped;
            BackStack.GestureRecognizers.Add(l_BackStackGesture);
        }

        void l_BackStackGesture_Tapped(object sender, EventArgs e)
        {
            // GAV : Dangereux de faire ça sur ce projet.
            //CoreSDK.Framework.Services.InteractionService().Close();
        }
    }
}
