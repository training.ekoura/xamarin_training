﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CoreSDK;

using Xamarin.Forms;
using MyADA.Common.ViewModels;

namespace MyADA.Common.Views.ContentViews
{
    public partial class Header : ContentView
    {
        public bool Drawer;

        public Header()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>();
        }

        private void btBurger_Clicked(object sender, EventArgs e)
        {
            var mdv = this.Parent.Parent.Parent as MasterDetailView;
            OnToggleRequest();
            mdv.IsPresented = true;
        }

        public void OnToggleRequest()
        {
            Drawer = !Drawer;
        }
    }
}
