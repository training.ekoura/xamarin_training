﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyADA.Common.Views
{
    public class FilterImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if((bool)value)
                return FileImageSource.FromResource("MyADA.Common.Views.Images.filterHeaderEnable.png");
            else
                return FileImageSource.FromResource("MyADA.Common.Views.Images.filterHeader.png");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
                return FileImageSource.FromResource("MyADA.Common.Views.Images.filterHeaderEnable.png");
            else
                return FileImageSource.FromResource("MyADA.Common.Views.Images.filterHeader.png");
        }
    }
}
