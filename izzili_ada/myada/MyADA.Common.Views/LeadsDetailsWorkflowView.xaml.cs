﻿using CoreSDK;
using CoreSDK.Controls;
using MyADA.Common.Views.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyADA.Common.Views
{
    public partial class LeadsDetailsWorkflowView : ContentPage, ILeadsDetailsWorkflowView
    {
        public ControlBoolean _OldDoneValue = ControlBoolean.False;
        public ControlBoolean _OldRecontactValue = ControlBoolean.False;
        public ControlBoolean _OldNotInterestValue = ControlBoolean.False;

        public LeadsDetailsWorkflowView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);

            BindingContextChanged += LeadDetailsWorkflow_BindingContextChanged;
        }

        private void backButton_Tapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }

        private void LeadDetailsWorkflow_BindingContextChanged(object sender, EventArgs e)
        {

            switchControlDone.PropertyChanged += (s, ev) =>
            {
                if (ev.PropertyName == "IsSelected")
                {
                    if (BindingContext != null)
                    {
                        if (((SelectControl)s).IsSelected != _OldRecontactValue)
                        {
                            ((MyADA.Common.ViewModels.LeadViewModel)BindingContext).LiveChangeLeadStatusOnDoneCommand.Execute(((SelectControl)s).IsSelected.ToString());
                            _OldRecontactValue = ((SelectControl)s).IsSelected;
                        }
                    }
                }
            };

            switchControlRecontactOn.PropertyChanged += (s, ev) =>
            {
                if (ev.PropertyName == "IsSelected")
                {
                    if (BindingContext != null)
                    {
                        if (((SelectControl)s).IsSelected != _OldDoneValue)
                        {
                            ((MyADA.Common.ViewModels.LeadViewModel)BindingContext).LiveChangeLeadStatusOnRecontactCommand.Execute(((SelectControl)s).IsSelected.ToString());
                            _OldDoneValue = ((SelectControl)s).IsSelected;
                        }
                    }
                }
            };

            switchControlNoInterest.PropertyChanged += (s, ev) =>
            {
                if (ev.PropertyName == "IsSelected")
                {
                    if (BindingContext != null)
                    {
                        if (((SelectControl)s).IsSelected != _OldNotInterestValue)
                        {
                            ((MyADA.Common.ViewModels.LeadViewModel)BindingContext).LiveChangeLeadStatusOnNotInterestCommand.Execute(((SelectControl)s).IsSelected.ToString());
                            _OldNotInterestValue = ((SelectControl)s).IsSelected;
                        }

                    }
                }
            };
        }
    }
}
