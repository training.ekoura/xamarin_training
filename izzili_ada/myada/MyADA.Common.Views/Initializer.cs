﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;
using System.Reflection;

using MyADA.Common.Views.Interfaces;
using MyADA.Common.Views.ContentViews;

namespace MyADA.Common.Views
{
    public class Initializer 
    {
        public static void RegisterAll()
        {
            // Référencement de l'assembly
            CoreSDK.Framework.Services.AssemblyService().RegisterAssembly(typeof(Initializer).GetTypeInfo().Assembly);

            // Definition des thèmes
            var l_ThemeService = CoreSDK.Framework.Services.ThemeService();
            //l_ThemeService.Register("ThemeA", Themes.ThemeA.ThemeCallback);
            //l_ThemeService.Apply("ThemeA");

            // Référencement des Views
            CoreSDK.Framework.Services.Container().Register<ILoginView, LoginView>();
            CoreSDK.Framework.Services.Container().Register<IMasterDetailView, MasterDetailView>();
            CoreSDK.Framework.Services.Container().Register<IBurgerView, BurgerView>();
            CoreSDK.Framework.Services.Container().Register<IHomeView, HomeView>();
            CoreSDK.Framework.Services.Container().Register<ISettingsProfileView, SettingsProfileView>();
            CoreSDK.Framework.Services.Container().Register<ISettingsChangeView, SettingsChangeView>();
            CoreSDK.Framework.Services.Container().Register<IChangeRequestView, ChangeRequestView>();
            CoreSDK.Framework.Services.Container().Register<IMyLeadsView, MyLeadsView>();
            CoreSDK.Framework.Services.Container().Register<IMyReportView, MyReportView>();
            CoreSDK.Framework.Services.Container().Register<IMyRSSView, MyRSSView>();
            CoreSDK.Framework.Services.Container().Register<IMBXamarinFormsTrainingAppPage, MBXamarinFormsTrainingAppPage>();
            CoreSDK.Framework.Services.Container().Register<IInboxDetailView, InboxDetailView>();
            CoreSDK.Framework.Services.Container().Register<ICommercialLeadsView, CommercialLeadsView>();
            CoreSDK.Framework.Services.Container().Register<IBookLeadsView, BookLeadsView>();
            CoreSDK.Framework.Services.Container().Register<IMyOwnLeadsView, MyOwnLeadsView>();
            CoreSDK.Framework.Services.Container().Register<IFilterView, FilterView>();
            CoreSDK.Framework.Services.Container().Register<IAvailableRSSView, AvailableRSSView>();
            CoreSDK.Framework.Services.Container().Register<INavigateLeadsView, NavigateLeadsView>();
            CoreSDK.Framework.Services.Container().Register<IReadDocumentView, ReadDocumentView>();
            CoreSDK.Framework.Services.Container().Register<ILeadsDetailsView, LeadsDetailsView>();
            CoreSDK.Framework.Services.Container().Register<IMBNewsDetails, MBNewsDetails>();
            CoreSDK.Framework.Services.Container().Register<IManageRSSView, ManageRSSView>();
            CoreSDK.Framework.Services.Container().Register<IEnterRssUrlView, EnterRssUrlView>();
            CoreSDK.Framework.Services.Container().Register<IRSSFeedView, RSSFeedView>();
            CoreSDK.Framework.Services.Container().Register<ILeadsDetailsWorkflowView, LeadsDetailsWorkflowView>();
            CoreSDK.Framework.Services.Container().Register<IFeedRSSDetailView, FeedRSSDetailView>();
            CoreSDK.Framework.Services.Container().Register<IAboutView, AboutView>();
        }
    }
}
