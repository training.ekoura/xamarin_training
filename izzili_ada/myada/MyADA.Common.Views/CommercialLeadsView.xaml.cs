﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyADA.Common.Views.Interfaces;

using Xamarin.Forms;
using MyADA.Common.ViewModels;

namespace MyADA.Common.Views
{
    public partial class CommercialLeadsView : ContentPage, ICommercialLeadsView
    {
        public CommercialLeadsView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);

            FillRepeater();
        }

        private void MyLeadsButton_Tapped(object sender, EventArgs e)
        {

        }

        private void FillRepeater()
        {
            string str = "";

            List<string> list = new List<string> { str, str, str };

            //repeaterNewLeads.ItemsSource = list;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if(BindingContext != null){
                ((LeadViewModel)BindingContext).RazFilter();
            }
        }
    }
}
