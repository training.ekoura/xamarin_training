﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;
using MyADA.Common.Views.Interfaces;
using MyADA.Common.ViewModels;
using Xamarin.Forms;
using CoreSDK.Controls;
using CoreSDK.Behaviors;

namespace MyADA.Common.Views
{
    public partial class MyLeadsView : ContentPage, IMyLeadsView
    {
        public static string _labelAction = "_labelAction";
        public bool _IsLoading = false;
        public ListView repeaterLeads;

        public MyLeadsView()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);

            CreateRepeaterLeads();

            //repeaterLeads.ItemAppearing += repeaterLeads_ItemAppearing;
        }


        private async Task LoadItems()
        {
           /* _IsLoading = true;

            Device.StartTimer(TimeSpan.FromSeconds(2), () =>
            {
                Task.Factory.StartNew(async () =>
                {
                    await ((LeadViewModel)BindingContext).LoadMoreLeadData();
                    _IsLoading = false;
                });

                return false;
            });*/
        }

        private void CreateRepeaterLeads()
        {
            repeaterLeads = (Device.OS == TargetPlatform.Android) ? new ListView(ListViewCachingStrategy.RecycleElement) : new ListView()
            {
                BackgroundColor     = Color.Transparent,
                SeparatorVisibility = SeparatorVisibility.None,
                HasUnevenRows       = true,
                HorizontalOptions   = LayoutOptions.FillAndExpand,
                VerticalOptions     = LayoutOptions.Fill
            };

            DataTemplate repeaterTemplate = new DataTemplate(() =>
            {
                #region TEMPLATE GRID

                Grid templateGrid = new Grid()
                {
                    HeightRequest     = 50,
                    ColumnSpacing     = 0,
                    RowSpacing        = 0,
                    Padding           = 0,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions   = LayoutOptions.FillAndExpand,
                    ColumnDefinitions =
                    {
                        new ColumnDefinition { Width = new GridLength(0.5, GridUnitType.Star) },
                        new ColumnDefinition { Width = new GridLength(4.3, GridUnitType.Star) },
                        new ColumnDefinition { Width = new GridLength(0.7, GridUnitType.Star) },
                        new ColumnDefinition { Width = new GridLength(1.5, GridUnitType.Star) },
                    },
                    RowDefinitions =
                    {
                        new RowDefinition { Height = new GridLength(0.5, GridUnitType.Absolute) },
                        new RowDefinition { Height = new GridLength(1,   GridUnitType.Star)     },
                        new RowDefinition { Height = new GridLength(0.5, GridUnitType.Absolute) },
                    }
                };

                #endregion TEMPLATE GRID

                #region IMAGE RED ARROW

                Image imgRedArrow = new Image()
                {
                    HorizontalOptions = LayoutOptions.End,
                    VerticalOptions   = LayoutOptions.CenterAndExpand
                };
                imgRedArrow.SetDynamicResource(Image.StyleProperty, "ArrowSingleRedImageStyle");
                Grid.SetColumn(imgRedArrow, 0);
                Grid.SetRow(imgRedArrow, 1);
                templateGrid.Children.Add(imgRedArrow);

                #endregion IMAGE RED ARROW

                #region LIST BUTTON

                ListButton listButton = new ListButton()
                {
                    BackgroundColor   = Color.Transparent,
                    StyleId           = "_labelAction",
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    VerticalOptions   = LayoutOptions.FillAndExpand
                };
                listButton.SetBinding(ListButton.TextProperty, "Tag");
                listButton.SetBinding(ListButton.CommandParameterProperty, ".");
                listButton.SetDynamicResource(ListButton.TextColorProperty, "BoxItemTextColor");
                listButton.SetDynamicResource(ListButton.FontSizeProperty, "BUTTONLIST_FONTSIZE");
                listButton.Clicked += OnCellClicked;
                Grid.SetColumn(listButton, 1);
                Grid.SetRow(listButton, 1);
                templateGrid.Children.Add(listButton);

                #endregion LIST BUTTON

                #region STACKLAYOUT CHECKBOXES & CHILDREN

                StackLayout stackCheckBoxes = new StackLayout()
                {
                    BackgroundColor   = Color.Transparent,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions   = LayoutOptions.FillAndExpand
                };
                stackCheckBoxes.SetBinding(StackLayout.IsVisibleProperty, "ShowSelectionIcone");
                Grid.SetColumn(stackCheckBoxes, 2);
                Grid.SetRow(stackCheckBoxes, 1);
                templateGrid.Children.Add(stackCheckBoxes);


                Button buttonChecked = new Button()
                {
                    BackgroundColor   = Color.Transparent,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions   = LayoutOptions.CenterAndExpand
                };
                buttonChecked.SetDynamicResource(Button.StyleProperty, "CheckButtonImageStyle");
                buttonChecked.SetBinding(Button.IsVisibleProperty, "IsSeleted");
                buttonChecked.SetBinding(ListButton.CommandParameterProperty, ".");
                buttonChecked.Clicked += OnCellClicked;
                stackCheckBoxes.Children.Add(buttonChecked);


                Button buttonUnchecked = new Button()
                {
                    BackgroundColor   = Color.Transparent,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions   = LayoutOptions.CenterAndExpand
                };
                buttonUnchecked.SetDynamicResource(Button.StyleProperty, "UncheckButtonImageStyle");
                buttonUnchecked.SetBinding(Button.IsVisibleProperty, "IsSeleted", BindingMode.Default, new ReverseBooleanConverter());
                buttonUnchecked.SetBinding(ListButton.CommandParameterProperty, ".");
                buttonUnchecked.Clicked += OnCellClicked;
                stackCheckBoxes.Children.Add(buttonUnchecked);

                #endregion STACKLAYOUT CHECKBOXES & CHILDREN

                #region IMAGE STATUS

                Image imgStatus = new Image();
                imgStatus.VerticalOptions = LayoutOptions.CenterAndExpand;
                imgStatus.SetBinding(Image.StyleProperty, "StatusColor", BindingMode.Default, new HorlogeStatusConverter());
                imgStatus.SetBinding(Image.IsVisibleProperty, "ShowSelectionIcone", BindingMode.Default, new ReverseBooleanConverter());
                Grid.SetColumn(imgStatus, 2);
                Grid.SetRow(imgStatus, 1);
                templateGrid.Children.Add(imgStatus);

                #endregion IMAGE STATUS

                #region STACKLAYOUT LEADS & CHILDREN

                StackLayout stackLeads = new StackLayout()
                {
                    Padding           = new Thickness(0, 10, 10, 10),
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions   = LayoutOptions.FillAndExpand
                };
                stackLeads.SetBinding(StackLayout.IsVisibleProperty, "ShowAffectationName");
                Grid.SetColumn(stackLeads, 3);
                Grid.SetRow(stackLeads, 1);
                templateGrid.Children.Add(stackLeads);
                

                StackLayout stackLine = new StackLayout()
                {
                    BackgroundColor   = Color.FromHex("#A6A7A9"),
                    Padding           = 1,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions   = LayoutOptions.FillAndExpand
                };
                stackLeads.Children.Add(stackLine);


                NormalFontLabel lbUserName = new NormalFontLabel()
                {
                    FontSize                = Device.GetNamedSize(NamedSize.Micro, typeof(NormalFontLabel)),
                    HorizontalOptions       = LayoutOptions.FillAndExpand,
                    VerticalOptions         = LayoutOptions.FillAndExpand,
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalTextAlignment   = TextAlignment.Center
                };
                lbUserName.SetBinding(NormalFontLabel.TextProperty, "User.ShortName");
                lbUserName.SetDynamicResource(NormalFontLabel.TextColorProperty, "BoxItemTextColor");
                lbUserName.SetDynamicResource(NormalFontLabel.BackgroundColorProperty, "AffectedLeadBackgroundColor");
                stackLine.Children.Add(lbUserName);

                #endregion STACKLAYOUT LEADS & CHILDREN

                #region SEPARATOR BOTTOM

                StackLayout stackSeparatorBottom = new StackLayout();
                stackSeparatorBottom.SetDynamicResource(StackLayout.StyleProperty, "GrayBorderStyle");
                Grid.SetColumn(stackSeparatorBottom, 0);
                Grid.SetColumnSpan(stackSeparatorBottom, 4);
                Grid.SetRow(stackSeparatorBottom, 2);
                templateGrid.Children.Add(stackSeparatorBottom);

                #endregion SEPARATOR BOTTOM

                return new ViewCell { View = templateGrid };
            });

            #region BEHAVIOR

            ListViewPagningBehavior behavior = new ListViewPagningBehavior();
            behavior.SetBinding(ListViewPagningBehavior.CommandProperty, "LoadMoreCommand");
            behavior.Converter = new ItemVisibilityEventArgstemConverter();
            repeaterLeads.Behaviors.Add(behavior);

            #endregion BEHAVIOR

            repeaterLeads.SetBinding(ListView.ItemsSourceProperty, "LeadsCollection");
            repeaterLeads.SetBinding(ListView.SelectedItemProperty, "SelectedLead");
            repeaterLeads.ItemTemplate = repeaterTemplate;
            repeaterLeads.ItemSelected += OnItemSelected;
            stackRepeaterLeads.Children.Add(repeaterLeads);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (BindingContext != null)
            {
                ((LeadViewModel)BindingContext).RazFilter(false);
                ((LeadViewModel)BindingContext).IsDefaultLeadCollectionFilter = true;
            }
        }

        void repeaterLeads_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            if (_IsLoading)
                return;
            var l_ListItemSource = ((LeadViewModel)BindingContext).LeadsCollection;
            if (e.Item == l_ListItemSource[l_ListItemSource.Count - 1])
            {
                LoadItems();
            }
        }

        private void ImgInfo_Tapped(object sender, EventArgs e)
        {
            popinTimer.IsVisible = true;
        }

        private void ImgClose_Tapped(object sender, EventArgs e)
        {
            popinTimer.IsVisible = false;
        }

        public void TappedAllocateCheckBox(object sender, EventArgs e)
        {
            Grid grid = (Grid)sender;

            foreach(VisualElement ve in grid.Children)
            {
                if(ve is Image)
                {
                    // Image Uncheck
                    //if (ve.StyleId == null || ve.StyleId == "0")
                    //{
                    //    //ve.SetDynamicResource(Image.StyleProperty, Themes.Theme.CheckedYesImageStyle);
                    //    ve.StyleId = "1";
                    //    ((MyADA.Common.ViewModels.LeadViewModel)BindingContext).ShowAllocatedLeadCommand.Execute((ve).StyleId);
                    //}
                    //// Image checked
                    //else
                    //{
                    //    //ve.SetDynamicResource(Image.StyleProperty, Themes.Theme.CheckedNoImageStyle);
                    //    ve.StyleId = "0";
                    //    ((MyADA.Common.ViewModels.LeadViewModel)BindingContext).ShowAllocatedLeadCommand.Execute((ve).StyleId);
                    //}
                    ((MyADA.Common.ViewModels.LeadViewModel)BindingContext).ShowAllocatedLeadCommand.Execute((ve).StyleId);
                }
            }

            //// Image Uncheck
            //if (((Image)sender).StyleId == null || ((Image)sender).StyleId == "0")
            //{
            //    ((Image)sender).SetDynamicResource(Image.StyleProperty, Themes.Theme.CheckedYesImageStyle);
            //    ((Image)sender).StyleId = "1";
            //    ((MyADA.Common.ViewModels.LeadViewModel)BindingContext).ShowAllocatedLeadCommand.Execute(((Image)sender).StyleId);
            //}
            //// Image checked
            //else
            //{
            //    ((Image)sender).SetDynamicResource(Image.StyleProperty, Themes.Theme.CheckedNoImageStyle);
            //    ((Image)sender).StyleId = "0";
            //    ((MyADA.Common.ViewModels.LeadViewModel)BindingContext).ShowAllocatedLeadCommand.Execute(((Image)sender).StyleId);
            //}
        }

        public void OnCellClicked(object sender, EventArgs e)
        {
            var b = (Button)sender;
            var t = b.CommandParameter;
            if (b.StyleId == _labelAction)
                ((LeadViewModel)BindingContext).ShowLeadDetailsCommand.Execute(t);
            else
                ((LeadViewModel)BindingContext).AddOrRemoveAffectationCommand.Execute(t);
        }

        public void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return; // has been set to null, do not 'process' tapped event
            ((LeadViewModel)BindingContext).ShowLeadDetailsCommand.Execute(e.SelectedItem);
            ((ListView)sender).SelectedItem = 0; // de-select the row
        }
    }
}
