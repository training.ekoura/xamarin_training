﻿using MyADA.Common.Views.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyADA.Common.Views
{
    public partial class SettingsChangeView : ContentPage, ISettingsChangeView
    {
        public bool Drawer;
        public SettingsChangeView()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);
        }

        private void btBurger_Clicked(object sender, EventArgs e)
        {
            var mdv = this.Parent as MasterDetailView;
            OnToggleRequest();
            mdv.IsPresented = true;
        }

        public void OnToggleRequest()
        {
            Drawer = !Drawer;
        }
    }
}
