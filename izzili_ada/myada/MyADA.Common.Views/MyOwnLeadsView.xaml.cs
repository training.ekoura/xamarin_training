﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;
using MyADA.Common.Views.Interfaces;

using Xamarin.Forms;
using MyADA.Common.ViewModels;
using CoreSDK.Controls;
using CoreSDK.Behaviors;

namespace MyADA.Common.Views
{
    public partial class MyOwnLeadsView : ContentPage, IMyOwnLeadsView
    {
        ListView repeaterLeadsOfTheDay;
        ListView repeaterLeadsOnGoing;
        public bool CheckIfLoad = false;

        public MyOwnLeadsView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);

            CreateRepeaterLeadsOfTheDay();
            CreateRepeaterLeadsOnGoing();
        }

        private void CreateRepeaterLeadsOfTheDay()
        {
            repeaterLeadsOfTheDay = (Device.OS == TargetPlatform.Android) ? new ListView(ListViewCachingStrategy.RecycleElement) : new ListView()
            {
                BackgroundColor = Color.Transparent,
                SeparatorVisibility = SeparatorVisibility.None,
                HasUnevenRows = true,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Fill
            };

            DataTemplate repeaterTemplate = new DataTemplate(() =>
            {
                #region TEMPLATE GRID

                Grid templateGrid = new Grid()
                {
                    ColumnSpacing = 0,
                    RowSpacing = 0,
                    ColumnDefinitions =
			        {
				        new ColumnDefinition { Width = new GridLength(0.7, GridUnitType.Absolute) },
				        new ColumnDefinition { Width = new GridLength(1,   GridUnitType.Star)     },
				        new ColumnDefinition { Width = new GridLength(0.7, GridUnitType.Absolute) },
			        },
                    RowDefinitions =
			        {
				        new RowDefinition { Height = new GridLength(0.5, GridUnitType.Absolute) }, // StaticResource ?
				        new RowDefinition { Height = new GridLength(1,   GridUnitType.Star)     },
				        new RowDefinition { Height = new GridLength(0.5, GridUnitType.Absolute) }, // StaticResource ?
			        }
                };
                //templateGrid.SetBinding(Grid.IsVisibleProperty, "IsLineVisble");

                #endregion TEMPLATE GRID

                #region STACKLAYOUT LINES

                StackLayout stackLineLeft = new StackLayout()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };
                stackLineLeft.SetDynamicResource(StackLayout.BackgroundColorProperty, "BoxItemBorderColor");
                Grid.SetColumn(stackLineLeft, 0);
                Grid.SetRow(stackLineLeft, 0);
                Grid.SetRowSpan(stackLineLeft, 3);
                templateGrid.Children.Add(stackLineLeft);

                StackLayout stackLineRight = new StackLayout()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };
                stackLineRight.SetDynamicResource(StackLayout.BackgroundColorProperty, "BoxItemBorderColor");
                Grid.SetColumn(stackLineRight, 2);
                Grid.SetRow(stackLineRight, 0);
                Grid.SetRowSpan(stackLineRight, 3);
                templateGrid.Children.Add(stackLineRight);

                StackLayout stackLineBottom = new StackLayout()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };
                stackLineBottom.SetDynamicResource(StackLayout.BackgroundColorProperty, "BoxItemBorderColor");
                Grid.SetColumn(stackLineBottom, 0);
                Grid.SetColumnSpan(stackLineBottom, 3);
                Grid.SetRow(stackLineBottom, 2);
                templateGrid.Children.Add(stackLineBottom);

                #endregion STACKLAYOUT LINES

                #region ITEM GRID

                Grid itemGrid = new Grid()
                {
                    Padding = new Thickness(20, 10),
                    ColumnSpacing = 10,
                    ColumnDefinitions =
			        {
				        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
				        new ColumnDefinition() { Width = (Device.Idiom == TargetIdiom.Phone) ? new GridLength(3, GridUnitType.Star) : new GridLength(2, GridUnitType.Star) },
				        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
			        }
                };
                itemGrid.SetDynamicResource(Grid.BackgroundColorProperty, "BoxItemBackgroundColor");
                Grid.SetColumn(itemGrid, 1);
                Grid.SetRow(itemGrid, 1);
                templateGrid.Children.Add(itemGrid);

                #endregion ITEM GRID

                #region RED ARROW IMAGE

                Image imgRedArrow = new Image();
                imgRedArrow.SetDynamicResource(Image.StyleProperty, "ArrowSingleRedImageStyle");
                Grid.SetColumn(imgRedArrow, 0);
                itemGrid.Children.Add(imgRedArrow);

                #endregion RED ARROW IMAGE

                #region TAG LABEL

                NormalFontLabel tagLabel = new NormalFontLabel()
                {
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand
                };
                tagLabel.SetBinding(NormalFontLabel.TextProperty, "Tag");
                tagLabel.SetDynamicResource(NormalFontLabel.StyleProperty, "BoxSmallLabelStyle");
                Grid.SetColumn(tagLabel, 1);
                itemGrid.Children.Add(tagLabel);

                #endregion TAG LABEL

                #region STATUS TIMER IMAGE

                Image imgStatusTimer = new Image()
                {
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand
                };
                imgStatusTimer.SetBinding(Image.StyleProperty, "StatusColor", BindingMode.Default, new HorlogeStatusConverter());
                Grid.SetColumn(imgStatusTimer, 2);
                itemGrid.Children.Add(imgStatusTimer);

                #endregion STATUS TIMER IMAGE

                return new ViewCell { View = templateGrid };
            });

            #region BEHAVIOR

            ListViewPagningBehavior behavior = new ListViewPagningBehavior();
            behavior.SetBinding(ListViewPagningBehavior.CommandProperty, "LoadMoreLeadOfTheDateCommand");
            behavior.Converter = new ItemVisibilityEventArgstemConverter();
            repeaterLeadsOfTheDay.Behaviors.Add(behavior);

            #endregion BEHAVIOR

            repeaterLeadsOfTheDay.SetBinding(ListView.ItemsSourceProperty, "OnUseDayLeadsCollection");
            repeaterLeadsOfTheDay.SetBinding(ListView.SelectedItemProperty, "SelectedLead");
            repeaterLeadsOfTheDay.ItemTemplate = repeaterTemplate;
            repeaterLeadsOfTheDay.ItemSelected += OnItemSelected;
            Grid.SetRow(repeaterLeadsOfTheDay, 3);
            mainGrid.Children.Add(repeaterLeadsOfTheDay);
        }

        private void CreateRepeaterLeadsOnGoing()
        {
            repeaterLeadsOnGoing = (Device.OS == TargetPlatform.Android) ? new ListView(ListViewCachingStrategy.RecycleElement) : new ListView()
            {
                BackgroundColor = Color.Transparent,
                SeparatorVisibility = SeparatorVisibility.None,
                HasUnevenRows = true,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Fill
            };

            DataTemplate repeaterTemplate = new DataTemplate(() =>
            {
                #region TEMPLATE GRID

                Grid templateGrid = new Grid()
                {
                    ColumnSpacing = 0,
                    RowSpacing = 0,
                    ColumnDefinitions =
			        {
				        new ColumnDefinition { Width = new GridLength(0.7, GridUnitType.Absolute) },
				        new ColumnDefinition { Width = new GridLength(1  , GridUnitType.Star)     },
				        new ColumnDefinition { Width = new GridLength(0.7, GridUnitType.Absolute) },
			        },
                    RowDefinitions =
			        {
				        new RowDefinition { Height = new GridLength(0.5, GridUnitType.Absolute) }, // StaticResource ?
				        new RowDefinition { Height = new GridLength(1,   GridUnitType.Star)     },
				        new RowDefinition { Height = new GridLength(0.5, GridUnitType.Absolute) }, // StaticResource ?
			        }
                };
                templateGrid.SetBinding(Grid.IsVisibleProperty, "IsLineVisble");

                #endregion TEMPLATE GRID

                #region STACKLAYOUT LINES

                StackLayout stackLineLeft = new StackLayout()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };
                stackLineLeft.SetDynamicResource(StackLayout.BackgroundColorProperty, "BoxItemBorderColor");
                Grid.SetColumn(stackLineLeft, 0);
                Grid.SetRow(stackLineLeft, 0);
                Grid.SetRowSpan(stackLineLeft, 3);
                templateGrid.Children.Add(stackLineLeft);

                StackLayout stackLineRight = new StackLayout()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };
                stackLineRight.SetDynamicResource(StackLayout.BackgroundColorProperty, "BoxItemBorderColor");
                Grid.SetColumn(stackLineRight, 2);
                Grid.SetRow(stackLineRight, 0);
                Grid.SetRowSpan(stackLineRight, 3);
                templateGrid.Children.Add(stackLineRight);

                StackLayout stackLineBottom = new StackLayout()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };
                stackLineLeft.SetDynamicResource(StackLayout.BackgroundColorProperty, "BoxItemBorderColor");
                Grid.SetColumn(stackLineBottom, 0);
                Grid.SetColumnSpan(stackLineBottom, 3);
                Grid.SetRow(stackLineBottom, 2);
                templateGrid.Children.Add(stackLineBottom);

                #endregion STACKLAYOUT LINES

                #region ITEM GRID

                Grid itemGrid = new Grid()
                {
                    Padding = new Thickness(20, 10),
                    RowSpacing = 0,
                    ColumnSpacing = 10,
                    ColumnDefinitions =
			        {
				        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
				        new ColumnDefinition() { Width = (Device.Idiom == TargetIdiom.Phone) ? new GridLength(3, GridUnitType.Star) : new GridLength(2, GridUnitType.Star) },
				        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
			        }
                };
                itemGrid.SetDynamicResource(Grid.BackgroundColorProperty, "BoxItemBackgroundColor");
                Grid.SetColumn(itemGrid, 1);
                Grid.SetRow(itemGrid, 1);
                templateGrid.Children.Add(itemGrid);

                #endregion ITEM GRID

                #region RED ARROW IMAGE

                Image imgRedArrow = new Image();
                imgRedArrow.VerticalOptions = LayoutOptions.CenterAndExpand;
                imgRedArrow.SetDynamicResource(Image.StyleProperty, "ArrowSingleRedImageStyle");
                Grid.SetColumn(imgRedArrow, 0);
                itemGrid.Children.Add(imgRedArrow);

                #endregion RED ARROW IMAGE

                #region SECOND ITEM GRID

                Grid secondItemGrid = new Grid()
                {
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    ColumnSpacing = 0,
                    ColumnDefinitions =
			        {
				        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
				        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
			        }
                };
                Grid.SetColumn(secondItemGrid, 1);
                itemGrid.Children.Add(secondItemGrid);

                #endregion SECOND ITEM GRID

                #region TAG LABEL

                NormalFontLabel tagLabel = new NormalFontLabel()
                {
                    LineBreakMode = LineBreakMode.TailTruncation,
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    VerticalOptions = LayoutOptions.EndAndExpand
                };
                tagLabel.SetBinding(NormalFontLabel.TextProperty, "Tag");
                tagLabel.SetDynamicResource(NormalFontLabel.StyleProperty, "BoxSmallLabelStyle");
                Grid.SetColumn(tagLabel, 0);
                secondItemGrid.Children.Add(tagLabel);

                #endregion TAG LABEL

                #region STACKLAYOUT DATE LABEL

                StackLayout stackItalicLabel = new StackLayout();
                stackItalicLabel.Padding = new Thickness(0, 0, 0, 2);
                Grid.SetColumn(stackItalicLabel, 1);
                secondItemGrid.Children.Add(stackItalicLabel);

                #endregion STACKLAYOUT DATE LABEL

                #region DATE LABEL

                ItalicFontLabel dateLabel = new ItalicFontLabel()
                {
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    VerticalOptions = LayoutOptions.EndAndExpand
                };
                dateLabel.SetBinding(ItalicFontLabel.TextProperty, "FormattedDate");
                dateLabel.SetDynamicResource(ItalicFontLabel.StyleProperty, "BoxItalicLabelStyle");
                stackItalicLabel.Children.Add(dateLabel);

                #endregion DATE LABEL

                #region STATUS TIMER IMAGE

                Image imgStatusTimer = new Image()
                {
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand
                };
                imgStatusTimer.SetBinding(Image.StyleProperty, "StatusColor", BindingMode.Default, new HorlogeStatusConverter());
                Grid.SetColumn(imgStatusTimer, 2);
                itemGrid.Children.Add(imgStatusTimer);

                #endregion STATUS TIMER IMAGE

                return new ViewCell { View = templateGrid };
            });

            #region BEHAVIOR

            ListViewPagningBehavior behavior = new ListViewPagningBehavior();
            behavior.SetBinding(ListViewPagningBehavior.CommandProperty, "LoadMoreLeadOnGoingCommand");
            behavior.Converter = new ItemVisibilityEventArgstemConverter();
            repeaterLeadsOnGoing.Behaviors.Add(behavior);

            #endregion BEHAVIOR

            repeaterLeadsOnGoing.SetBinding(ListView.ItemsSourceProperty, "OnUseOthersDayLeadsCollection");
            repeaterLeadsOnGoing.SetBinding(ListView.SelectedItemProperty, "SelectedLead");
            repeaterLeadsOnGoing.ItemTemplate = repeaterTemplate;
            repeaterLeadsOnGoing.ItemSelected += OnItemSelected;
            Grid.SetRow(repeaterLeadsOnGoing, 5);
            mainGrid.Children.Add(repeaterLeadsOnGoing);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (BindingContext != null)
            {
                ((LeadViewModel)BindingContext).IsDefaultLeadCollectionFilter = false;
                ((LeadViewModel)BindingContext).RazFilter();
            }
            var l_HomeViewModel = CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>();
            l_HomeViewModel.IsBusy = false;
            var l_LeadViewModel = CoreSDK.Framework.Services.Container().Resolve<ILeadViewModel>();
            l_LeadViewModel.IsBusy = false;
            var l_BurgerViewModel = CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>();
            l_BurgerViewModel.IsBusy = false;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            var l_HomeViewModel = CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>();
            l_HomeViewModel.IsBusy = false;
            var l_LeadViewModel = CoreSDK.Framework.Services.Container().Resolve<ILeadViewModel>();
            l_LeadViewModel.IsBusy = false;
            var l_BurgerViewModel = CoreSDK.Framework.Services.Container().Resolve<IBurgerViewModel>();
            l_BurgerViewModel.IsBusy = false;
        }

        public async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {

                if (CheckIfLoad) return;

                CheckIfLoad = true;
                LoadingScreen.IsVisible = true;

                await Task.Delay(300);

                if (e.SelectedItem == null) return; // has been set to null, do not 'process' tapped event
                ((LeadViewModel)BindingContext).ShowMyLeadDetailsCommand.Execute(e.SelectedItem);

                var l_NewPage = new LeadsDetailsWorkflowView();
                l_NewPage.BindingContext = this.BindingContext;

                Navigation.PushAsync(l_NewPage, true);

                ((ListView)sender).SelectedItem = 0; // de-select the row
                LoadingScreen.IsVisible = false;
                CheckIfLoad = false;
            }
            catch (Exception ex)
            {

            }
        }

        private void ImgInfo_Tapped(object sender, EventArgs e)
        {
            popinTimer.IsVisible = true;
        }

        private void ImgClose_Tapped(object sender, EventArgs e)
        {
            popinTimer.IsVisible = false;
        }
    }
}
