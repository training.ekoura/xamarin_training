﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSDK;
using MyADA.Common.Views.Interfaces;

using Xamarin.Forms;
using MyADA.Common.ViewModels;

namespace MyADA.Common.Views
{
    public partial class ReadDocumentView : ContentPage, IReadDocumentView
    {
        public ReadDocumentView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);

            var l_BindingContexte = CoreSDK.Framework.Services.Container().Resolve<IHomeViewModel>();
            webView.Source = new HtmlWebViewSource()
            {
                Html = l_BindingContexte.ReportSource
            };

            //webView.Navigating += WebView_Navigating;
            //webView.Navigated += WebView_Navigated;
        }

        void WebView_Navigating(object sender, WebNavigatingEventArgs e)
        {
            //gridActivityIndicator.IsVisible = true;
        }

        void WebView_Navigated(object sender, WebNavigatedEventArgs e)
        {
            //gridActivityIndicator.IsVisible = false;
        }
    }
}
