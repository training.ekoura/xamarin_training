﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyADA.Common.Views.Interfaces;
using CoreSDK;

using Xamarin.Forms;

namespace MyADA.Common.Views
{
    public partial class MyRSSView : ContentPage, IMyRSSView
    {
        public MyRSSView()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);
        }

        private void backButton_Tapped(object sender, EventArgs e)
        {
            CoreSDK.Framework.Services.InteractionService().OpenMasterDetailSection("IMBXamarinFormsTrainingAppPage");
        }
    }
}
