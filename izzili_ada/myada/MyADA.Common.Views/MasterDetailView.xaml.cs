﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

using MyADA.Common.Views;
using MyADA.Common.Views.Interfaces;

namespace MyADA.Common.Views
{
    public partial class MasterDetailView : MasterDetailPage, IMasterDetailView
    {
        public MasterDetailView()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            //IsPresented = false;
        }

        protected override bool OnBackButtonPressed()
        {
            if (Device.OS == TargetPlatform.Android || Device.OS == TargetPlatform.WinPhone)
            {
                return true;
            }
            else
                return base.OnBackButtonPressed();
        }
    }
}
