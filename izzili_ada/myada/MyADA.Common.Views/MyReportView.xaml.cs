﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyADA.Common.Views;
using MyADA.Common.Views.Interfaces;
using CoreSDK;

using Xamarin.Forms;
using MyADA.Common.ViewModels;

namespace MyADA.Common.Views
{
    public partial class MyReportView : ContentPage, IMyReportView
    {
        public MyReportView()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            this.SetDynamicResource(ContentPage.BackgroundImageProperty, Themes.Theme.BackGroundImage);
        }

    }
}
