﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK
{
    public class ApplicationStateService : CoreSDK.IApplicationStateService
    {
        // Pour des questions de performences, un dictionnaire est utilisé plutôt qu'une simple liste.
        private Dictionary<string, bool> _States = new Dictionary<string, bool>();

        public event EventHandler<ApplicationStateChangeEventArgs> StateChanged;

        #region === Block d'édition et notification différée ===

        // Nous ne voulons en aucun cas émettre les notifications au sein du lock. 
        // Nous les différons en fin de traitement de block d'édition.

        private List<ApplicationStateChangeEventArgs> _DelayedNotifications;

        private void AddDelayedNotificationStateChanged(string p_State, bool p_Value)
        {
            if (StateChanged != null)
            {
                // Au besoin on crée une liste.
                if(_DelayedNotifications == null) _DelayedNotifications = new List<ApplicationStateChangeEventArgs>();
                // On ne stock ue si il y a au moins un abonnement.
                _DelayedNotifications.Add(new ApplicationStateChangeEventArgs(p_State, p_Value));
            }
        }

        // En cas de block dans un block.
        private int _EditCount = 0;

        private void BeginEdit()
        {
            System.Threading.Monitor.Enter(_States);
            _EditCount++;
            if (_EditCount == 1)
            {
                // Premier block.
                // On ne fait rien de particulier.
            }
        }

        private void EndEdit()
        {
            _EditCount--;

            if (_EditCount == 0)
            {
                // C'était le dernier block.
                var l_Notifications = _DelayedNotifications;
                _DelayedNotifications = null;
                System.Threading.Monitor.Exit(_States);

                if (l_Notifications != null)
                {
                    // On notifie.
                    foreach (var l_Notification in l_Notifications)
                    {
                        StateChanged(this, l_Notification);
                    }
                }
            }
        }

        private UsingAction EditBlock()
        {
            return new UsingAction(BeginEdit, EndEdit);
        }

        #endregion

        private void SetState(string p_State, bool p_Value)
        {
            var l_OldValue = GetState(p_State);
            if (l_OldValue != p_Value)
            {
                if (p_Value)
                {
                    _States[p_State] = true;
                    AddDelayedNotificationStateChanged(p_State, true);
                }
                else
                {
                    // Pusique p_Value est false, c'est que l_OldValue est true et donc que p_State est dans le dictionnaire.
                    _States.Remove(p_State);
                    AddDelayedNotificationStateChanged(p_State, false);
                }
            }
        }

        private bool GetState(string p_State)
        {
            return _States.ContainsKey(p_State);
        }

        public void Clear(string p_GroupName = null)
        {
            using (EditBlock())
            {
                foreach (var l_State in _States.Keys.Where(k => p_GroupName == null || k.StartsWith(p_GroupName)).ToList())
                {
                    SetState(l_State, false);
                }
            }
        }

        public void Set(params string[] p_States)
        {
            using (EditBlock())
            {
                var l_PreviousStates = _States.Keys.ToList();
                foreach (var l_State in p_States)
                {
                    if (l_PreviousStates.Contains(l_State))
                    {
                        l_PreviousStates.Remove(l_State);
                    }
                    else
                    {
                        SetState(l_State, true);
                    }
                }

                foreach (var l_State in l_PreviousStates)
                {
                    SetState(l_State, false);
                }
            }
        }

        public void Add(params string[] p_States)
        {
            using (EditBlock())
            {
                foreach (var l_State in p_States)
                {
                    SetState(l_State, true);
                }
            }
        }

        public void Remove(params string[] p_States)
        {
            using (EditBlock())
            {
                foreach (var l_State in p_States)
                {
                    SetState(l_State, false);
                }
            }
        }

        public void Reverse(params string[] p_States)
        {
            using (EditBlock())
            {
                foreach (var l_State in p_States)
                {
                    SetState(l_State, !GetState(l_State));
                }
            }
        }

        public bool HasAll(params string[] p_States)
        {
            using (EditBlock())
            {
                foreach (var l_State in p_States)
                {
                    if (!_States.ContainsKey(l_State))
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public bool HasOne(params string[] p_States)
        {
            using (EditBlock())
            {
                foreach (var l_State in p_States)
                {
                    if (_States.ContainsKey(l_State))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public bool HasNone(params string[] p_States)
        {
            using (EditBlock())
            {
                foreach (var l_State in p_States)
                {
                    if (_States.ContainsKey(l_State))
                    {
                        return false;
                    }
                }
                return true;
            }
        }
    }
}
