﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK
{
    class FakeNetworkService : INetworkService
    {
        public bool IsConnected
        {
            get { return true; }
        }

        public event EventHandler<EventArgs> StateChanged;
    }
}
