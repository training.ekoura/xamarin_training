﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK
{
    internal class ObservableObjectValidationContext : IValidationContext
    {
        internal ObservableObjectValidationContext(Action p_ClearDelegate, Action<string, string[]> p_AddErrorDelegate, bool p_CompleteValidation = true)
        {
            _ClearDelegate = p_ClearDelegate;
            _AddErrorDelegate = p_AddErrorDelegate;
            _CompleteValidation = p_CompleteValidation;
        }

        private Action _ClearDelegate;

        private Action<string, string[]> _AddErrorDelegate;

        private bool _CompleteValidation = true;
        public bool CompleteValidation
        {
            get { return _CompleteValidation; }
        }

        public void ClearErrors()
        {
            _ClearDelegate();
        }

        public void AddError(string p_Message, params string[] p_Properties)
        {
            _AddErrorDelegate(p_Message, p_Properties);
        }

        public bool Involve<T>(T p_Item, Action<IValidationContext, T> p_Action = null)
        {
            if(p_Item is ObservableObject)
            {
                return (p_Item as ObservableObject).Validate(_CompleteValidation, (c)=> p_Action(c, p_Item));
            }            
            return false;
        }

        public bool InvolveItems<T>(IEnumerable<T> p_Items, Action<IValidationContext, T> p_Action = null)
        {
            if (p_Items != null)
            {
                var l_IsValid = true;
                foreach (var l_Item in p_Items)
                {
                    l_IsValid = Involve<T>(l_Item, p_Action) && l_IsValid; // Attention, l'ordre est important
                }
                return l_IsValid;
            }
            return false;
        }
    }
}
