﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK
{
    public interface ICryptography
    {
        /// <summary>
        /// Hash en sha256 la chaine.
        /// Retour la chaine résultante en hexa
        /// </summary>
        /// <param name="p_chaine"></param>
         string SHA256StringHash(string p_chaine);
    }
}
