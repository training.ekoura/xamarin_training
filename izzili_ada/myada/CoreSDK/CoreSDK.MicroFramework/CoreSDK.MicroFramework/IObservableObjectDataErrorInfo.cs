﻿using System;
namespace CoreSDK
{
    public interface IObservableObjectDataErrorInfo
    {
        string Message { get; }
        string PropertyName { get; }
        string ToString();
    }
}
