﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK
{
    public static class Extensions
    {
        private static Lazy<CoreSDK.IContainer> _Container = new Lazy<CoreSDK.IContainer>(()=>new CoreSDK.Container(), true);

        /// <summary>
        /// Retourne le conteneur d'IOC.
        /// </summary>
        public static CoreSDK.IContainer Container(this CoreSDK.IService p_Services)
        {
            return _Container.Value;
        }

        /// <summary>
        /// Le service de gestion des thèmes.
        /// </summary>
        public static CoreSDK.IApplicationStateService ApplicationStateService(this CoreSDK.IService p_Services)
        {
            return p_Services.Container().Resolve<CoreSDK.IApplicationStateService>();
        }

        /// <summary>
        /// Le service de gestion des assemblies.
        /// </summary>
        public static CoreSDK.IAssemblyService AssemblyService(this CoreSDK.IService p_Services)
        {
            return p_Services.Container().Resolve<CoreSDK.IAssemblyService>();
        }

        /// <summary>
        /// Retourne le service d'intéractions.
        /// </summary>
        public static CoreSDK.IInteractionService InteractionService(this CoreSDK.IService p_Services)
        {
            return p_Services.Container().Resolve<CoreSDK.IInteractionService>();
        }

        /// <summary>
        /// Retourne le service Http.
        /// </summary>
        public static CoreSDK.IHttpService HttpService(this CoreSDK.IService p_Services)
        {
            return p_Services.Container().Resolve<CoreSDK.IHttpService>();
        }

        /// <summary>
        /// Retourne le service Http.
        /// </summary>
        public static CoreSDK.IRemoteCallService RemoteCallService(this CoreSDK.IService p_Services)
        {
            return p_Services.Container().Resolve<CoreSDK.IRemoteCallService>();
        }

        /// <summary>
        /// Retourne le service de fichiers.
        /// </summary>
        public static CoreSDK.IFileService FileService(this CoreSDK.IService p_Services, bool p_ThrowExceptionIfNotFound = true)
        {
            return p_Services.Container().Resolve<CoreSDK.IFileService>(p_ThrowExceptionIfNotFound: p_ThrowExceptionIfNotFound);
        }

        /// <summary>
        /// Retourne le service de sirialisation.
        /// </summary>
        public static CoreSDK.ISerializerService SerializerService(this CoreSDK.IService p_Services)
        {
            return p_Services.Container().Resolve<CoreSDK.ISerializerService>();
        }

        /// <summary>
        /// Le service de gestion des thèmes.
        /// </summary>
        public static CoreSDK.Themes.IThemeService ThemeService(this CoreSDK.IService p_Services)
        {
            return p_Services.Container().Resolve<CoreSDK.Themes.IThemeService>();
        }

        /// <summary>
        /// Service de cryptographie
        /// </summary>
        /// <param name="p_Services"></param>
        /// <returns></returns>
        public static CoreSDK.ICryptography CryptoGraphyService(this CoreSDK.IService p_Services)
        {
            return p_Services.Container().Resolve<CoreSDK.ICryptography>();
        }




        /// <summary>
        /// Ajoute les éléments à la collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p_Collection"></param>
        /// <param name="p_Items"></param>
        public static void AddRange<T>(this System.Collections.ObjectModel.ObservableCollection<T> p_Collection, IEnumerable<T> p_Items)
        {
            if(p_Items!=null)
            {
                foreach(var l_Item in p_Items)
                {
                    p_Collection.Add(l_Item);
                }
            }
        }


        public static void Each<T>(this IEnumerable<T> p_Enum, Action<T> p_Action)
        {
            p_Enum.Each((i, item) => p_Action(item));
        }

        public static void Each<T>(this IEnumerable<T> p_Enum, Action<int, T> p_Action)
        {
            if (p_Action == null) return;
            var l_Index = 0;
            foreach (var l_Item in p_Enum)
            {
                p_Action(l_Index, l_Item);
                l_Index++;
            }
        }

        public static Memento Memento(this object p_Object)
        {
            var l_Result = new Memento();
            l_Result.Save(p_Object);
            return l_Result;
        }


        public static Exception Deepest(this Exception p_Exception)
        {
            if (p_Exception == null) return null;
            while (true)
            {
                if (p_Exception.InnerException == null) return p_Exception;
                p_Exception = p_Exception.InnerException;
            }
        }
    }
}
