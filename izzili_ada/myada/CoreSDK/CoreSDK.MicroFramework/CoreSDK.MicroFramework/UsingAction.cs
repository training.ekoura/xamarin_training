﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoreSDK
{
    public class UsingAction : IDisposable
    {
        public UsingAction(Action p_Begin, Action p_End)
        {
            p_Begin();
            _End = p_End;
        }

        private Action _End;

        void IDisposable.Dispose()
        {
            if (_End == null) return;
            _End();
            _End = null;
        }
    }
}
