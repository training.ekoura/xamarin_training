﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK
{
    public abstract class InstanceManagerBase : IInstanceManager
    {
        private Type _InterfaceType;
        public Type InterfaceType
        {
            get { return _InterfaceType; }
            set { _InterfaceType = value; }
        }

        private Type _ImplementationType;
        public Type ImplementationType
        {
            get { return _ImplementationType; }
            set { _ImplementationType = value; }
        }

        public void Initialize()
        {
            OnInitialize();
        }

        public void Teminate()
        {
            OnTeminate();
        }

        public object GetInstance(object p_Options)
        {
            return OnGetInstance(p_Options);
        }

        public virtual void OnInitialize()
        {

        }

        public virtual void OnTeminate()
        {

        }

        public abstract object OnGetInstance(object p_Options);
    }

    public class InstanceManager : InstanceManagerBase
    {
        public override object OnGetInstance(object p_Options)
        {
            return Activator.CreateInstance(ImplementationType);
        }
    }

    public class SingletonInstanceManager : InstanceManagerBase
    {
        private int _MainThreadID;

        private object _GlobalInstance;

        [ThreadStatic]
        private object _ThreadInstance;

        private bool _OnePerThread = false;
        public bool OnePerThread
        {
            get { return _OnePerThread; }
            set { _OnePerThread = value; }
        }

        public SingletonInstanceManager()
        {
            //_MainThreadID = System.Threading.Thread.CurrentThread.ManagedThreadId;
        }

        public SingletonInstanceManager(object p_Instance)
        {
            _GlobalInstance = p_Instance;
        }

        public override object OnGetInstance(object p_Options)
        {
            if (!OnePerThread)
            {
                if (_GlobalInstance == null)
                {
                    _GlobalInstance = Activator.CreateInstance(ImplementationType);
                }
                return _GlobalInstance;
            }
            else
            {
                if (_ThreadInstance == null)
                {
                    _ThreadInstance = Activator.CreateInstance(ImplementationType);
                }
                return _ThreadInstance;
            }
        }
    }
}
