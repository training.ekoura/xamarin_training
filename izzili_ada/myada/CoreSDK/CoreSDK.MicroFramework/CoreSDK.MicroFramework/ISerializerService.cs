﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK
{
    public interface ISerializerService
    {
        StringBuilder Serialize<T>(T p_Object);

        T Deserialize<T>(StringBuilder p_Data);
    }
}
