﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;
namespace CoreSDK
{
    public enum RemoteCallServiceMode { Sodexo, Demo }
    
    /// <summary>
    /// Le service des appels distant est très fortement orienté sur la logique de service Sodexo.
    /// Toute cette logique a du être entièrement développée en raison de la non normalisation des services.
    /// 
    /// Les résulats des appels des services peuvent être mis en cache via <see cref="LocalStorageCacheIsEnabled"/>  (true par défaut) et <see cref="p_CacheValideDuration"/> des méthodes d'appel.
    /// Attention cependant, ce mécanisme s'appuie sur <see cref="IFileService"/> qui n'existe pas par défaut en test.
    /// </summary>
    public interface IRemoteCallService
    {
        /// <summary>
        /// Paramètres qui seront ajoutés à toutes les requêtes.
        /// </summary>
        object GlobalParameters { get; set; }

        /// <summary>
        /// Si un nom est défini, alors un paramètre avec ce non et un Guid est ajouté à chaque requête (pour éviter la mise en cache dans certains scénarios).
        /// </summary>
        string HttpNoCacheParameterName { get; set; }

        bool LocalStorageCacheIsEnabled { get; set; }

        /// <summary>
        /// Adresse du serveur.
        /// </summary>
        string ServerUri { get; set; }

        RemoteCallServiceMode Mode { get; set; }

        /// <summary>
        /// Appel d'un service qui ne renvoi rien.
        /// </summary>
        /// <param name="p_ServiceName">Nom du service.</param>
        /// <param name="p_MethodName">Nom de la méthode.</param>
        /// <param name="p_Parameters">Paramètres.</param>
        /// <param name="p_CacheValideDuration">
        /// Durée de validité du cache. 
        /// Si ce paramètre est transmis, alors le resultat de l'appel est mis en cache (si le résultat est Success = true).
        /// Au delà de la durée de validité du cache, l'appel est à nouveau effectué et le cache redéfini.
        /// </param>
        /// <returns></returns>
        System.Threading.Tasks.Task<RemoteCallResult> Call(string p_ServiceName, string p_MethodName = null, object p_Parameters = null, TimeSpan p_CacheValideDuration = default(TimeSpan));

        /// <summary>
        /// Appel d'un service qui retourne des données.
        /// </summary>
        /// <typeparam name="TResult">Type des données retournées.</typeparam>
        /// <param name="p_ServiceName">Nom du service.</param>
        /// <param name="p_MethodName">Nom de la méthode.</param>
        /// <param name="p_Parameters">Paramètres.</param>
        /// <param name="p_CacheValideDuration">
        /// Durée de validité du cache. 
        /// Si ce paramètre est transmis, alors le resultat de l'appel est mis en cache (si le résultat est Success = true).
        /// Au delà de la durée de validité du cache, l'appel est à nouveau effectué et le cache redéfini.
        /// </param>
        /// <returns></returns>
        System.Threading.Tasks.Task<RemoteCallResult<TResult>> Call<TResult>(string p_ServiceName, string p_MethodName = null, object p_Parameters = null, TimeSpan p_CacheValideDuration = default(TimeSpan));

        /// <summary>
        /// Suppression d'une entrée dans le cache
        /// Attention Supprime toute les données correspondante au webservice
        /// </summary>
        /// <param name="p_Url">Key à supprimer</param>
        void DeleteCacheReference(string p_Uri);

    
    }

    public class RemoteCallResult
    {
        /// <summary>
        /// True si l'appel à résussi. 
        /// C'est une convention Sodexo.
        /// </summary>
        [DataMember(Name = "sucess")]
        public bool Success { get; set; }

        private string _ExceptionMessage;
        /// <summary>
        /// Message d'erreur (si il y a lieu).
        /// C'est une convention Sodexo.
        /// </summary>
        [DataMember(Name = "exceptionMessage")]
        public string ExceptionMessage {
            get
            {
                return _ExceptionMessage;
            }
            set
            {
                _ExceptionMessage = value;
                ExceptionComesFromBusinessLayer = !String.IsNullOrWhiteSpace(value);
                ExceptionComesFromCommunicationLayer = false;
                ExceptionComesFromNetwork = false;
            }
        }


        private string _ExceptionType;
        [DataMember(Name = "exceptionType")]
        public string ExceptionType
        {
            get
            {
                return _ExceptionType;
            }
            set
            {
                _ExceptionType = value;
            }
        }


        /// <summary>
        /// True si l'exception est de type communication.
        /// </summary>
        public bool ExceptionComesFromCommunicationLayer { get; private set; }

        /// <summary>
        /// True si l'exception est de type métier.
        /// </summary>
        public bool ExceptionComesFromBusinessLayer { get; private set; }


        /// <summary>
        /// True si l'exception vient du réseau (ou du webservice indispo)
        /// </summary>
        public bool ExceptionComesFromNetwork { get; private set; }

        public void SetNetworkException()
        {
            _ExceptionMessage = Properties.Resources.ERR_NETWORK;
            _ExceptionType = Properties.Resources.ERR_NETWORK;
            ExceptionComesFromBusinessLayer = false;
            ExceptionComesFromCommunicationLayer = false;
            ExceptionComesFromNetwork = true;
        }

        public void SetCommunicationException(Exception p_Exception)
        {
            _ExceptionMessage = p_Exception.Message;
            _ExceptionType = p_Exception.Message;
            ExceptionComesFromBusinessLayer = false;
            ExceptionComesFromCommunicationLayer = true;
            ExceptionComesFromNetwork = false;
        }
    }

    public class RemoteCallResult<T> : RemoteCallResult
    {
        /// <summary>
        /// Objet de données retourné par le service.
        /// C'est une convention Sodexo.
        /// </summary>
        [DataMember(Name = "data")]
        public T Data { get; set; }
    }
}
