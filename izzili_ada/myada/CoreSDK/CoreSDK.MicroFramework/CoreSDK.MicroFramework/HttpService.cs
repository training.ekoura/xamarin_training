﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Net;

namespace CoreSDK
{
    public class HttpService : CoreSDK.IHttpService 
    {
        private const string GET = "GET";

        private const string POST = "POST";

        public Task<Stream> Get(string l_Uri)
        {
            return Run(l_Uri);
        }

        private Task<Stream> Run(string l_Uri)
        {
            var tcs = new TaskCompletionSource<Stream>();

            try
            {               
                if (!string.IsNullOrEmpty(l_Uri))
                {
                    // Il faut appeler le serveur.
                    var l_WebRequest = System.Net.HttpWebRequest.CreateHttp(l_Uri);
                    l_WebRequest.Headers["Cache-Control"] = "max-age=1800";
                 
                    AsyncCallback l_EndResponseStream = (p_AsyncResult) =>
                    {
                        try
                        {
                            System.Diagnostics.Debug.WriteLine(String.Format("EndResponseStream - debut"));
                            using (var l_WebResponse = (HttpWebResponse)l_WebRequest.EndGetResponse(p_AsyncResult))
                            {
                                 byte[] TargetImageByte;
                                    System.Diagnostics.Debug.WriteLine(String.Format("1"));
                                    using (var l_Stream = l_WebResponse.GetResponseStream())
                                    {
                                        System.Diagnostics.Debug.WriteLine(String.Format("2"));
                                        //l_Stream.CopyTo(l_Result);
                                        TargetImageByte = ReadFully(l_Stream);
                                    }
                                    System.Diagnostics.Debug.WriteLine(String.Format("3"));
                                    tcs.SetResult(new MemoryStream(TargetImageByte));
                                    System.Diagnostics.Debug.WriteLine(String.Format("EndResponseStream - Fin"));
                            }
                        }
                        
                        catch (Exception l_Exception)
                        {
                            tcs.SetException(l_Exception);
                        }

                    };

                    AsyncCallback l_EndRequestStream = (p_AsyncResult) =>
                    {
                        try
                        {
                            System.Diagnostics.Debug.WriteLine(String.Format("EndRequestStream"));
                            var l_Stream = l_WebRequest.EndGetRequestStream(p_AsyncResult);
                            l_WebRequest.BeginGetResponse(l_EndResponseStream, null);
                            l_Stream.Dispose();
                        }
                        catch (Exception l_Exception)
                        {
                            tcs.SetException(l_Exception);
                        }
                    };

                    if (l_WebRequest.Method == GET)
                    {
                        l_WebRequest.BeginGetResponse(l_EndResponseStream, null);
                    }
                    else if (l_WebRequest.Method == POST)
                    {
                        l_WebRequest.BeginGetRequestStream(l_EndRequestStream, null);
                    }
                }
            }
            catch (Exception l_Exception)
            {
                Task.Run(() =>
                {
                    tcs.SetException(l_Exception);
                });
            }
     
        
            
            return tcs.Task;
        }
        public static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
    }
         
}

