﻿using System;
namespace CoreSDK
{
    public interface IDelegateCommand
    {
        bool CanExecute(object parameter);
        event EventHandler CanExecuteChanged;
        string Caption { get; set; }
        void Execute(object parameter);
        string Icon { get; set; }
        void InvalidateCanExecute();
        bool IsEnabled { get; set; }
        string ShortCut { get; set; }
    }
}
