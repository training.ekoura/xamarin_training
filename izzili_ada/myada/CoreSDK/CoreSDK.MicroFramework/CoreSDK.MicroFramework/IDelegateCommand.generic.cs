﻿using System;
namespace CoreSDK
{
    public interface IDelegateCommand<TArgument> : IDelegateCommand
    {
        TArgument Argument { get; set; }
    }
}
