﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK
{
    public interface IPhoneService
    {
        Task SendSms(string number, string body);
        void DialNumber(string number);
    }
}
