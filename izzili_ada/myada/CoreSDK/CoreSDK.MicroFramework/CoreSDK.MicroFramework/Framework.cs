﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace CoreSDK
{
    // TODO MLB : Implémenter la gestion GPS
    // TODO MLB : Implémenter un scheduler.
    // TODO MLB : S'assurer du fonctionnement de INetworkService sur les différentes plateformes.
    // TODO : Dans InteractionService, s'assurer que pop event ne se déclanche que quand un close est fait.

    public static class Framework
    {
        public static void Initialize()
        {
            // Service de sérialisation
            CoreSDK.Framework.Services.Container().Register<CoreSDK.ISerializerService, CoreSDK.SerializerService>(new CoreSDK.SingletonInstanceManager());

            // Service des états applicatifs.
            CoreSDK.Framework.Services.Container().Register<CoreSDK.IApplicationStateService, CoreSDK.ApplicationStateService>(new CoreSDK.SingletonInstanceManager());

            // Service de gestion des assemblies.
            CoreSDK.Framework.Services.Container().Register<CoreSDK.IAssemblyService, CoreSDK.AssemblyService>(new CoreSDK.SingletonInstanceManager());

            // Enregistrement du NetworkService (Fake)
            //Sodexo.Framework.Services.Container().Register<Sodexo.INetworkService, Sodexo.NetworkService>(new Sodexo.SingletonInstanceManager(), RegisterBehavior.OverrideIfIsRegistred);

            // Enregistrement du service HTTP
            CoreSDK.Framework.Services.Container().Register<CoreSDK.IHttpService, CoreSDK.HttpService>(new CoreSDK.SingletonInstanceManager());

            // Enregistrement du service Remote
            CoreSDK.Framework.Services.Container().Register<CoreSDK.IRemoteCallService, CoreSDK.RemoteCallService>(new CoreSDK.SingletonInstanceManager());

            // Enregistrement de l'assembly
            CoreSDK.Framework.Services.AssemblyService().RegisterAssembly(typeof(Framework).GetTypeInfo().Assembly);
        }

        public static IService Services
        {
            get { return null; }
        }
    }
}
