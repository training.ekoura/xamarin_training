﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoreSDK
{
    public static class ObservableObjectValidationContextExtension
    {
        public static bool AddError_Required<TValue>(this IValidationContext p_Context, TValue p_Value, string p_PropertyName)
        {
            if (typeof(TValue)==typeof(string))
            {
                if(string.IsNullOrWhiteSpace(p_Value as string))
                {
                    p_Context.AddError("Cette valeur est requise.", p_PropertyName);
                    return false;
                }
            }
            else if (Equals(p_Value, default(TValue)))
            {
                p_Context.AddError("Cette valeur est requise.", p_PropertyName);
                return false;
            }

            return true;
        }
    }
}
