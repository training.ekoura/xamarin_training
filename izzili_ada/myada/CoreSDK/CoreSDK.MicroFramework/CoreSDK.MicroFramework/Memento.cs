﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace CoreSDK
{
    /// <summary>
    /// Pattern memento.
    /// Ne gère que les types value.
    /// </summary>
    public class Memento
    {
        private class Info
        {
            public Type Type { get; set; }
            public object Value { get; set; }
        }

        private Dictionary<string, Info> _Data = new Dictionary<string, Info>();

        public void Save(object p_Object, params string[] p_SpecialIncludedNames)
        {
            if (p_Object == null) return;
            var l_ObjectType = p_Object.GetType();
            while (l_ObjectType != typeof(object) && l_ObjectType != typeof(ObservableObject))
            {
                foreach (var l_Property in l_ObjectType.GetTypeInfo().DeclaredProperties)
                {
                    if (l_Property.CanRead && l_Property.CanWrite
                       && (
                            l_Property.PropertyType.GetTypeInfo().IsValueType
                            || l_Property.PropertyType == typeof(string)
                            || (p_SpecialIncludedNames != null && p_SpecialIncludedNames.Contains(l_Property.Name))
                       )
                    )
                    {
                        _Data[l_Property.Name] = new Info() { Type = l_ObjectType, Value = l_Property.GetValue(p_Object, null) };
                    }
                }
                l_ObjectType = l_ObjectType.GetTypeInfo().BaseType;
            }
        }

        public void Load(object p_Object)
        {
            if (p_Object == null) return;
            foreach (var l_Entry in _Data)
            {
                var l_Property = l_Entry.Value.Type.GetTypeInfo().GetDeclaredProperty(l_Entry.Key);
                l_Property.SetValue(p_Object, _Data[l_Property.Name].Value);
            }
        }
    }
}
