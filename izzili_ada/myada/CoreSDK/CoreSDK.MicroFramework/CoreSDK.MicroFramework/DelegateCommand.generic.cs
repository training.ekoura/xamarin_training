﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CoreSDK
{
    public class DelegateCommand<TArgument> : DelegateCommand, IDelegateCommand<TArgument>
    {
        public const string Parameter_PROPERTYNAME = "Parameter";

        private TArgument _Argument;
        ///<summary>
        /// Property : Parameter
        ///</summary>
        public TArgument Argument
        {
            get { return GetValue<TArgument>(() => _Argument); }
            set
            {
                SetValue<TArgument>(() => _Argument, (v) => _Argument = v, value, Parameter_PROPERTYNAME,
                (pn, ov, nv) =>
                {
                    if (ov != null && ov is INotifyPropertyChanged)
                    {
                        (ov as INotifyPropertyChanged).PropertyChanged -= DelegateCommand_PropertyChanged;
                    }
                }, 
                (pn, ov, nv) =>
                {
                    if (nv != null && nv is INotifyPropertyChanged)
                    {
                        (nv as INotifyPropertyChanged).PropertyChanged += DelegateCommand_PropertyChanged;
                    }
                });
            }
        }

        void DelegateCommand_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            NotifyCanExecuteChanged();
        }

        public DelegateCommand(Func<object, Boolean> p_CanExecuteDelegate, Action<object> p_ExecuteDelegate)
            : base(p_CanExecuteDelegate, p_ExecuteDelegate)
        {
            if (typeof(TArgument) == typeof(string))
            {
                _Argument = (TArgument)(object)(string.Empty);
            }
            else
            {
                _Argument = (TArgument)Activator.CreateInstance(typeof(TArgument));
            }
        }
    }
}
