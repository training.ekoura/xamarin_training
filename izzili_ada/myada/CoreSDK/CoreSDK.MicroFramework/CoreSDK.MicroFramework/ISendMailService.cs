﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK
{
    public interface ISendMailService
    {
        void SendEmailAsync(string email, string subject, string body = null);
    }
}
