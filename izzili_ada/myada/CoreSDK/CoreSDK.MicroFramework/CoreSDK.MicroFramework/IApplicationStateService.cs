﻿using System;
namespace CoreSDK
{
    /// <summary>
    /// Le service des états applicatifs gère des états globaux à l'application (Ce service est thread  safe).
    /// 
    /// - Un état applicatif est un simple nom auquel est associé une valeur true / false.
    /// - Un état applicatif peut être true / actif ou false / inactif.
    /// 
    /// L'intérêt est de pouvoir gérer toutes sortes d'états différents à l'échelle de l'application,
    /// tous les services autour, mais aussi l'UI peuvent ainsi s'adapter à des changements d'états.
    /// 
    /// Par exemple:
    /// 
    /// - Connecté / Pas connecté.
    /// - Authentifié / Pas authentifié.
    /// - En cours de téléchargement / pas en cours de téléchargement
    /// - ...
    /// 
    /// Il est particulièrement utile de gérer des états de profil (droits) : 
    /// 
    /// - Droit de consultation de certains informations.
    /// - Droit d'ajout, d'édition.
    /// - Droit de supression.
    /// 
    /// C'est à vous d'imaginer vos états applicatifs.
    /// Des règles de nommages bien définies sont plus que nécessaires, 
    /// surtout lorsque le nombre d'états grandi.
    /// 
    /// Dans un contexte Xaml, pour adapter l'UI automatiquement en fonction des chnagements d'état, 
    /// vous pouvez utiliser les behaviors suivants (d'autres béhavior peuvent être ajoutés
    /// selon les besoins) :
    /// 
    /// <seealso cref="CoreSDK.Behaviors.EnableApplicationStateBehavior"/>
    /// <seealso cref="CoreSDK.Behaviors.VisibleApplicationStateBehavior"/>
    /// </summary>
    public interface IApplicationStateService
    {
        /// <summary>
        /// Efface tous les états applicatifs.
        /// </summary>
        /// <param name="p_GroupName">Si un nom de groupe est précisé, le clear ne porte que sur les état dont le nom commence par le nom de groupe.</param>
        void Clear(string p_GroupName = null);

        /// <summary>
        /// Défini les état applicatifs actifs.
        /// Attention, il y a une différence importante avec Add().
        /// Au terme de Set(), seuls les états applicatifs transmis seront actifs.
        /// Sur le plan logique Set() est équivalent à Clear() suivi de Add(),
        /// cependant certains mécanismes sont optimisés pour éviter les notifications
        /// inutiles.
        /// </summary>
        /// <param name="p_States">Les états applicatifs.</param>
        void Set(params string[] p_States);

        /// <summary>
        /// Ajoute / active des états applicatifs.
        /// </summary>
        /// <param name="p_States">Les états applicatifs à ajouter / activer.</param>
        void Add(params string[] p_States);

        /// <summary>
        /// Renvoi true si tous les états applicatifs transmis sont actifs.
        /// </summary>
        /// <param name="p_States">Les états applicatifs à tester.</param>
        /// <returns>true si tous les états applicatifs sont actifs.</returns>
        bool HasAll(params string[] p_States);

        /// <summary>
        /// Renvoi true si aucun des états applicatifs transmis n'est actif.
        /// </summary>
        /// <param name="p_States">Les états applicatifs à tester.</param>
        /// <returns>true si aucun des états applicatifs n'est actif.</returns>
        bool HasNone(params string[] p_States);

        /// <summary>
        /// Renvoi true si au moins un états applicatifs transmis est actif.
        /// </summary>
        /// <param name="p_States">Les états applicatifs à tester.</param>
        /// <returns>true si au moins un des états applicatifs sont actif.</returns>
        bool HasOne(params string[] p_States);

        /// <summary>
        /// Supprime / désactive les états applicatifs transmis.
        /// </summary>
        /// <param name="p_States">Les états applicatifs à supprimer / désactiver.</param>
        void Remove(params string[] p_States);

        /// <summary>
        /// Inverse l'état des états applicatifs transmis.
        /// </summary>
        /// <param name="p_States">Les états applicatifs à inverser.</param>
        void Reverse(params string[] p_States);

        /// <summary>
        /// Notifie quand il y a un changement sur un état applicatif.
        /// </summary>
        event EventHandler<ApplicationStateChangeEventArgs> StateChanged;
    }

    /// <summary>
    /// EventArgs utiliser pour notifier un changement d'état d'un état applicatif.
    /// </summary>
    public class ApplicationStateChangeEventArgs : EventArgs
    {
        public ApplicationStateChangeEventArgs(string p_State, bool p_Value)
        {
            _State = p_State;
            _Value = p_Value;
        }

        private string _State;
        /// <summary>
        /// Etat concerné par la notification.
        /// </summary>
        public string State
        {
            get { return _State; }
        }

        private bool _Value;
        /// <summary>
        /// Nouvelle valeur de l'état.
        /// </summary>
        public bool Value
        {
            get { return _Value; }
        }
    }
}
