﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK
{
    public class DelegateCommand : Command, IDelegateCommand
    {
        public DelegateCommand(Action<object> p_ExecuteDelegate)
        {
            _ExecuteDelegate = p_ExecuteDelegate;
        }

        public DelegateCommand(Func<object, bool> p_CanExecuteDelegate, Action<object> p_ExecuteDelegate)
        {
            _CanExecuteDelegate = p_CanExecuteDelegate;
            _ExecuteDelegate = p_ExecuteDelegate;
        }

        private Func<object, bool> _CanExecuteDelegate;

        private Action<object> _ExecuteDelegate;

        protected override bool OnCanExecute(object parameter)
        {            
            if (_CanExecuteDelegate != null) return _CanExecuteDelegate(parameter);
            return true;
        }

        protected override void OnExecute(object parameter)
        {
            _ExecuteDelegate(parameter);
        }
    }
}
