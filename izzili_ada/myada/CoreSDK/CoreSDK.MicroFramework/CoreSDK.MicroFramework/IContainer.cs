﻿using System;
namespace CoreSDK
{
    public enum RegisterBehavior { 
        ThrowExceptionIfIsRegistred, 
        DoNothingIfIsRegistred,
        OverrideIfIsRegistred 
    }

    public interface IContainer
    {
        /// <summary>
        /// Enregistre une interface dans le conteneur.
        /// </summary>
        /// <typeparam name="TInterface">Interface à enregistrer.</typeparam>
        /// <typeparam name="TImplementation">Implémentation de l'interface.</typeparam>
        /// <param name="p_Manager">Manager d'instance pour les implémentations.</param>
        /// <param name="p_Behavior">Comportement à adopter si l'interface a déjà été enregistrée.</param>
        void Register<TInterface, TImplementation>(IInstanceManager p_Manager = null, RegisterBehavior p_Behavior = RegisterBehavior.ThrowExceptionIfIsRegistred)
            where TInterface : class
            where TImplementation : class;

        /// <summary>
        /// Résoud une interface dans le conteneur.
        /// L'interface ne peut être résolue que si un <see cref="Register"/> a été fait avant.
        /// </summary>
        /// <typeparam name="TInterface">Type de l'interface à résoudre.</typeparam>
        /// <param name="p_Options">Objet qui sera transmis en paramètre au manager lors de la demande d'instance.</param>
        /// <param name="p_ThrowExceptionIfNotFound">Si true, alors une exception est déclanchée si la résolution est impossible.</param>
        TInterface Resolve<TInterface>(object p_Options = null, bool p_ThrowExceptionIfNotFound = true) where TInterface : class;

        /// <summary>
        /// Résoud une interface dans le conteneur.
        /// L'interface ne peut être résolue que si un <see cref="Register"/> a été fait avant.
        /// </summary>
        /// <param name="p_Type">Type de l'interface à résoudre.</param>
        /// <param name="p_Options">Objet qui sera transmis en paramètre au manager lors de la demande d'instance.</param>
        /// <param name="p_ThrowExceptionIfNotFound">Si true, alors une exception est déclanchée si la résolution est impossible.</param>
        object Resolve(Type p_Type, object p_Options = null, bool p_ThrowExceptionIfNotFound = true);

        /// <summary>
        /// Résoud une interface dans le conteneur.
        /// L'interface ne peut être résolue que si un <see cref="Register"/> a été fait avant.
        /// </summary>
        /// <param name="p_TypeName">Nom du type de l'interface à résoudre.</param>
        /// <param name="p_Options">Objet qui sera transmis en paramètre au manager lors de la demande d'instance.</param>
        /// <param name="p_ThrowExceptionIfNotFound">Si true, alors une exception est déclanchée si la résolution est impossible.</param>
        object Resolve(string p_TypeName, object p_Options = null, bool p_ThrowExceptionIfNotFound = true);
    }
}
