﻿using System;
namespace CoreSDK
{
    public interface IObservableObject
    {
        event EventHandler<System.ComponentModel.DataErrorsChangedEventArgs> ErrorsChanged;
        System.Collections.IEnumerable GetErrors(string propertyName);
        bool HasErrors { get; }
        bool IsInitialized { get; }
        bool IsWorking { get; }
        object Master { get; }
        event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        object this[string p_Expression] { get; }
        bool Validate(bool p_CompleteValidation = true);
        void ValidateRulesAdd(Action<IValidationContext> p_ValidationCallback);
        void ValidateRulesClear();
        void ValidateRulesDefault();
        void ValidateRulesRemove(Action<IValidationContext> p_ValidationCallback);
        void ValidateRulesReplace(Action<IValidationContext> p_ValidationCallback);
        event EventHandler<EventArgs> WorkStarted;
        event EventHandler<EventArgs> WorkStoped;
    }
}
