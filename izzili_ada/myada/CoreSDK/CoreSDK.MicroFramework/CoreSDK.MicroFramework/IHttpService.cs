﻿using System;
using System.IO;
namespace CoreSDK
{
    public interface IHttpService
    {
        System.Threading.Tasks.Task<Stream> Get(string l_Uri);
    }
}
