﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace CoreSDK
{
    public abstract class Command : ObservableObject, ICommand
    {
        #region === Propriétés ===

        private bool _IsEnabled = true;
        ///<Summary>
        /// Propriété : IsEnabled.
        ///</Summary>
        public bool IsEnabled
        {
            get { return GetValue<bool>(() => _IsEnabled); }
            set
            {
                SetValue<bool>(() => _IsEnabled, (v) => _IsEnabled = v, value, IsEnabled_PROPERTYNAME, null, (pn, o, n) => NotifyCanExecuteChanged());
            }
        }

        public const string IsEnabled_PROPERTYNAME = "IsEnabled";

        private string _Caption;
        ///<Summary>
        /// Propriété : Caption.
        ///</Summary>
        public string Caption
        {
            get { return GetValue<string>(() => _Caption); }
            set { SetValue<string>(() => _Caption, (v) => _Caption = v, value, Caption_PROPERTYNAME); }
        }

        public const string Caption_PROPERTYNAME = "Caption";

        private string _Icon;
        ///<Summary>
        /// Propriété : Icon.
        ///</Summary>
        public string Icon
        {
            get { return GetValue<string>(() => _Icon); }
            set { SetValue<string>(() => _Icon, (v) => _Icon = v, value, Icon_PROPERTYNAME); }
        }

        public const string Icon_PROPERTYNAME = "Icon";

        private string _ShortCut;
        ///<Summary>
        /// Propriété : ShortCut.
        ///</Summary>
        public string ShortCut
        {
            get { return GetValue<string>(() => _ShortCut); }
            set { SetValue<string>(() => _ShortCut, (v) => _ShortCut = v, value, ShortCut_PROPERTYNAME); }
        }

        public const string ShortCut_PROPERTYNAME = "ShortCut";

        #endregion

        #region === CanExecute / Execute ===

        private bool? _CanExecute;

        public bool CanExecute(object parameter)
        {
            if (_CanExecute.HasValue) return _CanExecute.Value;
            _CanExecute = IsEnabled && NotifyAcceptExecute(parameter) && OnCanExecute(parameter);
            return _CanExecute.Value;

        }

        protected abstract bool OnCanExecute(object p_Options);

        public void Execute(object parameter)
        {
            if (!CanExecute(parameter)) return;
            NotifyBeforeExecute(parameter);
            OnExecute(parameter);
            NotifyAfterExecute(parameter);
        }

        protected abstract void OnExecute(object p_Options);

        public event EventHandler<AcceptExecuteCommandEventArgs> AcceptExecute;

        private bool NotifyAcceptExecute(object parameter)
        {
            if (AcceptExecute != null)
            {
                var l_Args = new AcceptExecuteCommandEventArgs(this, parameter);
                AcceptExecute(this, l_Args);
                if (!l_Args.AgreeExecute)
                {
                    _CanExecute = false;
                    return false;
                }
            }
            return true;
        }

        public event EventHandler<BeforeExecuteCommandEventArgs> BeforeExecute;

        private void NotifyBeforeExecute(object parameter)
        {
            if (BeforeExecute != null)
            {
                BeforeExecute(this, new BeforeExecuteCommandEventArgs(this, parameter));
            }
        }

        public event EventHandler<AfterExecuteCommandEventArgs> AfterExecute;

        private void NotifyAfterExecute(object parameter)
        {
            if (AfterExecute != null)
            {
                AfterExecute(this, new AfterExecuteCommandEventArgs(this, parameter));
            }
        }

        #endregion

        #region === Notification ===

        public event EventHandler CanExecuteChanged;

        protected void NotifyCanExecuteChanged()
        {
            _CanExecute = null;
            if (CanExecuteChanged != null)
            {
                CanExecuteChanged(this, new EventArgs());
            }
        }

        public void InvalidateCanExecute()
        {
            NotifyCanExecuteChanged();
        }

        #endregion
    }

    public class AcceptExecuteCommandEventArgs : EventArgs
    {
        public AcceptExecuteCommandEventArgs(Command p_Command, object p_CommandParameter)
        {
            _Command = p_Command;
            _CommandParameter = p_CommandParameter;
        }

        private Command _Command;
        public Command Command
        {
            get { return _Command; }
            internal set { _Command = value; }
        }

        private object _CommandParameter;
        public object CommandParameter
        {
            get { return _CommandParameter; }
            internal set { _CommandParameter = value; }
        }

        private bool _AgreeExecute = true;
        public bool AgreeExecute
        {
            get { return _AgreeExecute; }
            set { _AgreeExecute = value; }
        }
    }

    public class BeforeExecuteCommandEventArgs : EventArgs
    {
        public BeforeExecuteCommandEventArgs(Command p_Command, object p_CommandParameter)
        {
            _Command = p_Command;
            _CommandParameter = p_CommandParameter;
        }

        private Command _Command;
        public Command Command
        {
            get { return _Command; }
            internal set { _Command = value; }
        }

        private object _CommandParameter;
        public object CommandParameter
        {
            get { return _CommandParameter; }
            internal set { _CommandParameter = value; }
        }
    }

    public class AfterExecuteCommandEventArgs : EventArgs
    {
        public AfterExecuteCommandEventArgs(Command p_Command, object p_CommandParameter)
        {
            _Command = p_Command;
            _CommandParameter = p_CommandParameter;
        }

        private Command _Command;
        public Command Command
        {
            get { return _Command; }
            internal set { _Command = value; }

        }
        private object _CommandParameter;
        public object CommandParameter
        {
            get { return _CommandParameter; }
            internal set { _CommandParameter = value; }
        }
    }
}
