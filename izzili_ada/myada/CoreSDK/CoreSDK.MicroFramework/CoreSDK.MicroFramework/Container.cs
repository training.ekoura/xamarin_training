﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK
{
    public class Container : IContainer
    {
        private object _Locker = new object();

        private Dictionary<Type, IInstanceManager> _Registrations = new Dictionary<Type, IInstanceManager>();

        public void Register<TInterface, TImplementation>(IInstanceManager p_Manager = null, RegisterBehavior p_Behavior = RegisterBehavior.ThrowExceptionIfIsRegistred)
            where TInterface : class
            where TImplementation : class
        {
            lock (_Locker)
            {
                var l_InterfaceType = typeof(TInterface);
                var l_ImplementationType = typeof(TImplementation);

                if (_Registrations.ContainsKey(l_InterfaceType))
                {
                    var l_Manager = _Registrations[l_InterfaceType];
                    if (p_Behavior == RegisterBehavior.ThrowExceptionIfIsRegistred) throw new Exception(string.Format(Properties.Resources.ERR_Container_Register_Exist, typeof(TInterface).Name,l_Manager.ImplementationType.Name));
                    if (p_Behavior == RegisterBehavior.DoNothingIfIsRegistred) return;
                    l_Manager.Teminate();
                    _Registrations.Remove(l_InterfaceType);
                }

                p_Manager = p_Manager ?? new InstanceManager();

                p_Manager.InterfaceType = l_InterfaceType;
                p_Manager.ImplementationType = l_ImplementationType;
                p_Manager.Initialize();

                _Registrations[typeof(TInterface)] = p_Manager;
            }
        }

        public object Resolve(Type p_Type, object p_Options = null, bool p_ThrowExceptionIfNotFound = true)
        {
            lock (_Locker)
            {
                // Première chose à faire, trouver le manager.
                // Si aucun manager n'existe, c'est que le register n'a pas été fait.
                var l_Manager = default(IInstanceManager);
                lock (_Locker)
                {
                    if (_Registrations.ContainsKey(p_Type))
                    {
                        l_Manager = _Registrations[p_Type];
                    }
                }
                if (l_Manager == null && p_ThrowExceptionIfNotFound) throw new Exception(string.Format(Properties.Resources.ERR_Container_Resolve_NotRegistred, p_Type.Name));

                // Demander une instance au manager
                var l_Result = default(object);
                if (l_Manager != null)
                {
                    try
                    {
                        l_Result = l_Manager.GetInstance(p_Options);
                    }
                    catch(Exception l_Exception)
                    {
                        // C'est un cas sensible.
                        // Il est préférable de transmettre un message explicit.
                        if (l_Result == null && p_ThrowExceptionIfNotFound) throw new Exception(string.Format(Properties.Resources.ERR_Container_Resolve_GetInstanceException, p_Type.Name), l_Exception);
                    }
                }
                if (l_Result == null && p_ThrowExceptionIfNotFound) throw new Exception(string.Format(Properties.Resources.ERR_Container_Resolve_IsNull, p_Type.Name));

                // Retourner le résultat.
                return l_Result;
            }
        }

        public object Resolve(string p_TypeName, object p_Options = null, bool p_ThrowExceptionIfNull = true)
        {
            var l_Key = (from k in _Registrations.Keys where k.Name == p_TypeName select k).FirstOrDefault();
            if(l_Key == null)
            {
                if (p_ThrowExceptionIfNull) throw new Exception(string.Format(Properties.Resources.ERR_Container_Resolve_NotRegistred, p_TypeName));
                return null;
            }
            else
            {
                return Resolve(l_Key, p_Options, p_ThrowExceptionIfNull);
            }
        }

        public TInterface Resolve<TInterface>(object p_Options = null, bool p_ThrowExceptionIfNull = true)
            where TInterface : class
        {
            return Resolve(typeof(TInterface), p_Options, p_ThrowExceptionIfNull) as TInterface;
        }
    }
}
