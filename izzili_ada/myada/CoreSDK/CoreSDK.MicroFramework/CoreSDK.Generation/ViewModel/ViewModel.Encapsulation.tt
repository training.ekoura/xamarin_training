﻿<#@ template language="C#" debug="True" #><#+
	public partial class elementEncapsulation {

		private List<elementProperty> _Properties = new List<elementProperty>();

		private List<CodeProperty> _CodeProperties = new List<CodeProperty>();

		private List<elementEncapsulationAdapt> _Adapts = new List<elementEncapsulationAdapt>();

		public void Analyse_Phase1()
        {
			var l_CodeProperties = GetCodeProperties(SourceType);
			foreach(var l_CodeProperty in l_CodeProperties)
            {
				var l_Remove = (from r in Remove where r.Name==l_CodeProperty.Name select r).FirstOrDefault();
				if(l_Remove == null)
                {
					var l_Adapt = (from a in Adapt where a.Name==l_CodeProperty.Name select a).FirstOrDefault();
					var l_Property = new elementProperty()
					{
						Name = (string.IsNullOrWhiteSpace(Prefix) ? "" : Prefix) + (l_Adapt != null && !string.IsNullOrWhiteSpace(l_Adapt.NewName) ? l_Adapt.NewName : l_CodeProperty.Name),
						Type = (l_Adapt != null && !string.IsNullOrWhiteSpace(l_Adapt.NewType) ? l_Adapt.NewType : l_CodeProperty.Type.AsFullName)
					};
					l_Property.SetCodeForGetValue(()=> WriteCode("{0}_{1}_GetValue", Name, l_Property.Name));
					l_Property.SetCodeForSetValue(()=> WriteCode("{0}_{1}_SetValue", Name, l_Property.Name));
					CurrentViewModel.Contract.Property.Add(l_Property);  

					_Properties.Add(l_Property);
					_CodeProperties.Add(l_CodeProperty);
					_Adapts.Add(l_Adapt);
                }
            }			
        }

		public void Analyse_Phase2()
        {
		
        }

		public void Analyse_Phase3()
		{
        }

		public void Write()
        {		
			using(RegionBlock("Encapsulation : {0}", Name))
            {
				WriteComment("Champ pour l'encapsulation.");
				WriteComment("Les propriétés implémentées font directement référence à ce champ.");
				WriteCodeLine("private {0} _{1};", SourceType, Name);

				WriteCodeLine();
			
				WriteCommentSummary("Encapsulation : {0}.", Name);
				WriteCodeLine("public {0} Get{1}()", SourceType, Name);
				using(CodeBlock())
				{
					WriteCodeLine("return _{0};", Name);
				}

				WriteCodeLine();

				WriteCommentSummary("Encapsulation : {0}.", Name);
				WriteCodeLine("public {2} Set{1}({0} p_Value)", SourceType, Name, CurrentViewModel.ViewModelName);
				using(CodeBlock())
				{
					WriteComment("En vue d'éventuelles notification (si initialisé), nous récupérons les anciennes valeurs (valeurs actuelles).");
					foreach(var l_Property in _Properties)
					{
						WriteCodeLine("var l_OldValue{1} = _{0} == null || !IsInitialized ? default({2}) : {1};",Name,l_Property.Name, l_Property.Type);
					}
				
					WriteCodeLine();
				
					WriteComment("Affectation du nouvel object d'encapsulation.");
					WriteCodeLine("_{0} = p_Value;", Name);
				
					WriteCodeLine();

					WriteComment("Si pas initialisé, inutile d'aller plus loin.");
					WriteCodeLine("if(!IsInitialized) return this as {0};", CurrentViewModel.ViewModelName);

					WriteCodeLine();

					WriteComment("Nous allons notifier, nous avons besoin des valeurs actuelles.");
					foreach(var l_Property in _Properties)
					{
						WriteCodeLine("var l_NewValue{1} = _{0} == null ? default({2}) : {1};",Name,l_Property.Name, l_Property.Type);
					}

					WriteCodeLine();

					WriteComment("Nous notifions.");
					foreach(var l_Property in _Properties)
					{
						WriteCodeLine("NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs({0}_PROPERTYNAME, l_OldValue{0}, l_NewValue{0}));", l_Property.Name);
						WriteCodeLine("Notify{0}Dependencies();", l_Property.Name);
					}

					WriteCodeLine();

					WriteCodeLine("return this as {0};", CurrentViewModel.ViewModelName);
				}
            

				WriteCodeLine();

				foreach(var l_Property in _Properties)
				{
					using(RegionBlock("Propriété : {0}", l_Property.Name))
					{
						var l_CodeProperty = _CodeProperties[_Properties.IndexOf(l_Property)];
						var l_Adapt = _Adapts[_Properties.IndexOf(l_Property)];

						WriteCommentSummary("Méthode utilisée par la propriété {1} pour la lecture de la valeur corresponsande de {0}.",Name, l_Property.Name);
						WriteCodeLine("private {2} {0}_{1}_GetValue()", Name, l_Property.Name, l_Property.Type);
						using(CodeBlock())
						{
							if(l_Adapt!=null)
                            {
								WriteCodeLine("return On{0}_{1}_GetValue();", Name, l_Property.Name);
                            }
							else
                            {
								WriteCodeLine("if(_{0} == null) throw new Exception(string.Format(\"Le model d'encapsulation n'est pas défini. Get impossible sur {0}.\", {1}_PROPERTYNAME));", Name, l_CodeProperty.Name);
								WriteCodeLine("return _{0}.{1};", Name, l_CodeProperty.Name, l_Property.Type);
                            }
						}

						WriteCodeLine();

						WriteCommentSummary("Méthode utilisée par la propriété {1} pour l'affectation de la valeur corresponsande de {0}.",Name, l_Property.Name);
						WriteCodeLine("private void {0}_{1}_SetValue({2} p_Value)", Name, l_Property.Name, l_Property.Type);
						using(CodeBlock())
						{
							if(l_Adapt!=null)
                            {
								WriteCodeLine("On{0}_{1}_SetValue(p_Value);", Name, l_Property.Name);
                            }
							else
                            {
								WriteCodeLine("if(_{0} == null) throw new Exception(string.Format(\"Le model d'encapsulation n'est pas défini. Set impossible sur {0}.\", {1}_PROPERTYNAME));", Name, l_CodeProperty.Name);
								WriteCodeLine("_{0}.{1} = p_Value;", Name, l_CodeProperty.Name);
                            }
						}

						WriteCodeLine();

						if(l_Adapt!=null)
						{
							WriteCommentSummary("Méthode d'adaptation de la valeur de la propriété {1} pour la lecture de la valeur corresponsande de {0}.",Name, l_Property.Name);
							WriteCodeLine("protected abstract {2} On{0}_{1}_GetValue();", Name, l_Property.Name, l_Property.Type);

							WriteCodeLine();

							WriteCommentSummary("Méthode d'adaptation de la valeur de la propriété {1} pour l'affectation de la valeur corresponsande de {0}.",Name, l_Property.Name);
							WriteCodeLine("protected abstract void On{0}_{1}_SetValue({2} p_Value);", Name, l_Property.Name, l_Property.Type);

							WriteCodeLine();
						}
					}

					WriteCodeLine();
                }
            }
        }
    }	
#>