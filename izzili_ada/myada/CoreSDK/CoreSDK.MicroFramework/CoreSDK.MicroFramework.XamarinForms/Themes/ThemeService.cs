﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK.Themes
{
    public class ThemeServiceXamarin : CoreSDK.Themes.IThemeServiceXamarin
    {
        private Dictionary<string, Action<ResourceDictionary>> _Themes = new Dictionary<string, Action<ResourceDictionary>>();

        private string _Current;
        public string Current
        {
            get { return _Current; }
        }

        public string[] Themes
        {
            get 
            {
                lock (_Themes)
                {
                    return _Themes.Keys.ToArray();
                }
            }
        }

        public void Register(string p_ThemeName, Action<ResourceDictionary> p_Callback)
        {
            lock (_Themes)
            {
                if (string.IsNullOrWhiteSpace(p_ThemeName)) throw new ThemeServiceException(string.Format(Properties.Resources.ERR_ThemManager_ThemeNameBadFormat));
                if (p_Callback == null) throw new ThemeServiceException(string.Format(Properties.Resources.ERR_ThemManager_CallbackCantBeNull));
                _Themes[p_ThemeName] = p_Callback;
            }
        }

        public void Apply(string p_ThemeName)
        {
            //if (!_Themes.ContainsKey(p_ThemeName)) throw new ThemeServiceException(string.Format(Properties.Resources.ERR_ThemManager_ThemeDoesNotExist, p_ThemeName));
            try
            {
                var l_Callback = (!_Themes.ContainsKey(p_ThemeName)) ? _Themes["my-sodexo"] : _Themes[p_ThemeName];
                var l_ResourceDictionary = new ResourceDictionary();

                l_ResourceDictionary.Add("ImageNameConverter", new ImageNameConverter());
                l_ResourceDictionary.Add("ReverseBooleanConverter", new ReverseBooleanConverter());
                l_ResourceDictionary.Add("ExpressionConverter", new ExpressionConverter());
                l_ResourceDictionary.Add("EnumConverter", new EnumConverter());
                l_ResourceDictionary.Add("HorlogeStatusConverter", new HorlogeStatusConverter());
                l_ResourceDictionary.Add("StringToControlBoleanConverter", new StringToControlBoleanConverter());
                l_ResourceDictionary.Add("IconeSecondaryVisibiltyConverter", new IconeSecondaryVisibiltyConverter());
             
                l_Callback(l_ResourceDictionary);
                Application.Current.Resources = l_ResourceDictionary;
                _Current = p_ThemeName;
            }
            catch(Exception l_Exception)
            {
                throw new ThemeServiceException(string.Format(Properties.Resources.ERR_ThemManager_Apply, p_ThemeName, l_Exception.Message));
            }
        }
    }
}
