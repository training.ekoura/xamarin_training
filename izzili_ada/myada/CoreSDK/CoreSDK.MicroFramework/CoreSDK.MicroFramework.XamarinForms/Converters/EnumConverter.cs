﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Reflection;

namespace CoreSDK
{
    public class EnumDataSource
    {
        private EnumItem[] _Items;
        public EnumItem[] Items
        {
            get { return _Items; }
        }

        private Type _Type;
        public Type Type
        {
            get { return _Type; }
            set 
            {
                if (value == null) return;
                _Items = null;
                _Type = value;
                
                var l_Assembly = _Type.GetTypeInfo().Assembly;

                var l_ResxName = (from n in l_Assembly.GetManifestResourceNames() where n.EndsWith(".Properties.Resources.resources") select n.Replace(".resources", "")).FirstOrDefault();
                if (l_ResxName == null) return;

                var l_Namespace = l_ResxName.Replace(".Properties.Resources.resources", "");

                global::System.Resources.ResourceManager l_RessourceManager = new global::System.Resources.ResourceManager(l_ResxName, l_Assembly);
                if (l_RessourceManager == null) return;

                var l_BaseResourceName = "ENUM_" + _Type.Name + "_";


                var l_Names = Enum.GetNames(_Type).ToArray();
                _Items = (from n in Enum.GetNames(_Type)
                          let v = Enum.Parse(_Type, n)
                          select new EnumItem()
                          {
                              Name = n,
                              Value = v,
                              Icon = GetResourceString(l_RessourceManager, l_BaseResourceName + n + "_ICON", ""),
                              Label = GetResourceString(l_RessourceManager, l_BaseResourceName + n + "_LABEL", "["+n+"]"),
                              Description = GetResourceString(l_RessourceManager, l_BaseResourceName + n + "_DESCRIPTION"),
                          }).ToArray();
            }
        }

        private string GetResourceString(global::System.Resources.ResourceManager p_ResourceManager, string p_Name, string p_DefaultValue = "")
        {
            if (p_ResourceManager == null) return p_DefaultValue;
            try
            {
                return p_ResourceManager.GetString(p_Name);
            }
            catch
            {
                return p_DefaultValue;
            }
        }
    }

    public class EnumItem
    {
        /// <summary>
        /// Nom de la valeur de l'énumération.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Valeur de l'énumération.
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Icone de la valeur (si existe en ressource).
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Libellé de la valeur (si existe en ressource) ou nom de la valeur entre crochets.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Description de la valeur (si existeen ressource).
        /// </summary>
        public string Description { get; set; }
    }

    public class EnumConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(parameter is EnumDataSource)) throw new Exception(Properties.Resources.ERR_EnumConverter_BadParameter);
            var l_Parameter = parameter as EnumDataSource;
            if (l_Parameter.Items == null) throw new Exception(Properties.Resources.ERR_EnumConverter_EnumDataSourceNotInitialized);
            if (value == null)
            {
                value = Activator.CreateInstance(l_Parameter.Type);
            }
            var l_Item = (from i in l_Parameter.Items where Equals(i.Value, value) select i).FirstOrDefault();
            if (l_Item == null) throw new Exception(string.Format(Properties.Resources.ERR_EnumConverter_ItemNotFound, value));
            return l_Item;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(parameter is EnumDataSource)) throw new Exception(Properties.Resources.ERR_EnumConverter_BadParameter);
            var l_Parameter = parameter as EnumDataSource;
            if (l_Parameter.Items == null) throw new Exception(Properties.Resources.ERR_EnumConverter_EnumDataSourceNotInitialized);
            if (value == null)
            {
                value = l_Parameter.Items.First();
            }
            var l_Item = (from i in l_Parameter.Items where Equals(i, value) select i).FirstOrDefault();
            if (l_Item == null) throw new Exception(string.Format(Properties.Resources.ERR_EnumConverter_ItemNotFound, value));
            return l_Item.Value;
        }
    }
}
