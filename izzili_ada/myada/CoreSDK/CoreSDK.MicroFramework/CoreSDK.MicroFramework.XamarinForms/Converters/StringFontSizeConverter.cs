﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK
{
    public class StringFontSizeConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                value = Application.Current.Resources[value.ToString()];
                return value;
            }
            else
                //breaks the application if value is 0
                value = 12.5;
                return value;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                value = Application.Current.Resources[value.ToString()];
                return value;
            }
            else
                value = 12.5;
                return value;
        }
    }
}
