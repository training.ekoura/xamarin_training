﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK
{
    public class IconeSecondaryVisibiltyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if(parameter.ToString()=="On")
                {
                    return (bool)value ? Application.Current.Resources["PuceOnImageStyle"] : Application.Current.Resources["EmptyKey"];
                }
                else
                    return !(bool)value ? Application.Current.Resources["PuceOffImageStyle"] : Application.Current.Resources["EmptyKey"];
            }
            else
                return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
