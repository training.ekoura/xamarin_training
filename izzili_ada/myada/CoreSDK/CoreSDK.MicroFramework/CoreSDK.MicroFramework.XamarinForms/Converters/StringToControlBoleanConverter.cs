﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK
{
    //corrected in mysodexo.trunk
    public class StringToControlBoleanConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Equals(value, true) ? ControlBoolean.True : ControlBoolean.False;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((ControlBoolean)value == ControlBoolean.True) ? true : false;
        }
    }

    //used only in ada.
    public class StringToControlBoleanTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(Type sourceType)
        {
            return true;
        }

        public override object ConvertFrom(System.Globalization.CultureInfo culture, object value)
        {
            return (value == null) ? ControlBoolean.False : Equals(value, true) || ((value.ToString() == "True") || (value.ToString() == "true")) ? ControlBoolean.True : ControlBoolean.False;
        }
    }
}


