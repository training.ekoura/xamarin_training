﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK
{
    public class ExpressionConverter : IValueConverter
    {
        private const string CONTAINS = "contains:";

        object IValueConverter.Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (parameter == null) throw new Exception(Properties.Resources.ERR_ExpressionConverter_ParameterIsMissing);
            var l_Parameter = parameter.ToString();
            if (string.IsNullOrWhiteSpace(l_Parameter)) throw new Exception(Properties.Resources.ERR_ExpressionConverter_ParameterIsMissing);

            if (l_Parameter == "!")
            {
                // Cas particulier, inverse un booléen
                return Equals(true, value);
            }
            else if (l_Parameter == "null")
            {
                return Equals(null, value);
            }
            else if (l_Parameter == "!null")
            {
                return !Equals(null, value);
            }
            else if(l_Parameter.StartsWith("="))
            {
                // Le test se fait sous forme chaîne
                var l_Value = l_Parameter.Remove(0, 1);
                return Equals(l_Value, (value == null ? "" : value.ToString()));
            }
            else if (l_Parameter.StartsWith("!="))
            {
                // Le test se fait sous forme chaîne
                var l_Value = l_Parameter.Remove(0, 1);
                return !Equals(l_Value, (value == null ? "" : value.ToString()));
            }
            else if (l_Parameter.StartsWith(CONTAINS))
            {
                var l_Value = l_Parameter.Remove(0, CONTAINS.Length);
                return value == null ? false : value.ToString().Contains(l_Value);
            }
            else if (l_Parameter.StartsWith("!"+CONTAINS))
            {
                var l_Value = l_Parameter.Remove(0, CONTAINS.Length + 1);
                return !(value == null ? false : value.ToString().Contains(l_Value));
            }
            else
            {
                throw new Exception(string.Format(Properties.Resources.ERR_ExpressionConverter_BadExpression, l_Parameter));
            }
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Equals(value, false) ? true : false;
        }
    }
}
