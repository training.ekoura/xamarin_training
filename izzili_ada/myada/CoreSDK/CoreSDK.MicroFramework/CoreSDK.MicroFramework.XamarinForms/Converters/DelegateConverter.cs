﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK
{
    public class DelegateConverter<TSourceValue> : Xamarin.Forms.IValueConverter
    {
        private Func<TSourceValue, Type, object, System.Globalization.CultureInfo, object> _ConvertCallback;

        private Func<object, Type, object, System.Globalization.CultureInfo, TSourceValue> _ConvertBackCallback;

        public DelegateConverter(Func<TSourceValue, Type, object, System.Globalization.CultureInfo, object> p_ConvertCallback)
        {
            _ConvertCallback = p_ConvertCallback;
        }

        public DelegateConverter(Func<TSourceValue, Type, object, System.Globalization.CultureInfo, object> p_ConvertCallback, Func<object, Type, object, System.Globalization.CultureInfo, TSourceValue> p_ConvertBackCallback)
        {
            _ConvertCallback = p_ConvertCallback;
            _ConvertBackCallback = p_ConvertBackCallback;
        }

        object Xamarin.Forms.IValueConverter.Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (_ConvertCallback != null)
                {
                    return _ConvertCallback((TSourceValue)value, targetType, parameter, culture);
                }
                return value;
            }
            catch
            {
                return value;
            }
        }

        object Xamarin.Forms.IValueConverter.ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (_ConvertBackCallback!=null)
                {
                    return _ConvertBackCallback(value, targetType, parameter, culture);
                }
                return value;
            }
            catch
            {
                return value;
            }
        }
    }
}
