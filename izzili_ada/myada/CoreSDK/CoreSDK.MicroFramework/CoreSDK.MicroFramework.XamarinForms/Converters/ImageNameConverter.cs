﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK
{
    public class ImageNameConverter : IValueConverter
    {
        private const string NOT_AVAILABLE_IMAGE_RESOURCE_NAME = "Sodexo.image-not-available.jpg";

        private Stream GetDefaultStream(string p_DefaultResourceName)
        {
            var l_logHeader = String.Format("===> test {0}", p_DefaultResourceName);
            System.Diagnostics.Debug.WriteLine(l_logHeader);
            var l_Result = CoreSDK.Framework.Services.Container().Resolve<IAssemblyService>().GetResourceStream(p_DefaultResourceName);
            var l_logHeader2 = String.Format("===> test2 {0}", p_DefaultResourceName);
            System.Diagnostics.Debug.WriteLine(l_logHeader2);
            if (l_Result == null) return CoreSDK.Framework.Services.Container().Resolve<IAssemblyService>().GetResourceStream(NOT_AVAILABLE_IMAGE_RESOURCE_NAME);
            return l_Result;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var l_FullName = value != null ? value.ToString() : (parameter != null ? parameter.ToString() : string.Empty);
            var l_DefaultResourceName = parameter != null ? parameter.ToString() : NOT_AVAILABLE_IMAGE_RESOURCE_NAME;
            var l_logHeader = String.Format("===> Image- value={0}", l_FullName);

            System.Diagnostics.Debug.WriteLine(l_logHeader);

            try
            {
                var l_ValueParts = l_FullName.Split('|');
                var l_Type = default(string);
                var l_Name = l_FullName;

                if (l_ValueParts.Length == 1)
                {
                    l_Type = "res";
                    l_Name = l_ValueParts[0];
                }
                else if (l_ValueParts.Length == 2)
                {
                    l_Type = l_ValueParts[0].ToLower();
                    l_Name = l_ValueParts[1];
                }

                if (l_Type == "file")
                {
                    System.Diagnostics.Debug.WriteLine(l_logHeader + "- Mode = FILE, Ref={0}", l_Name);
                    if (targetType == typeof(FileImageSource))
                    {
                        // Pour le chemin vers les ressources WM.
                        switch (Device.OS)
                        {
                            case TargetPlatform.WinPhone:
                                l_Name = "Assets/" + l_Name;
                                break;
                            default:
                                break;
                        }
                        // Nous sommes obligé de gérer ce cas à cause de Xamarin Button.
                        return FileImageSource.FromFile(l_Name);
                    }
                    else
                    {
                        // Pour le chemin vers les ressources WM.
                        switch (Device.OS)
                        {
                            case TargetPlatform.WinPhone:
                                l_Name = "Assets/" + l_Name;
                                break;
                            default:
                                break;
                        }

                        return ImageSource.FromFile(l_Name);
                    }
                }
                else if (l_Type == "uri")
                {
                    System.Diagnostics.Debug.WriteLine(l_logHeader + "- Mode = URI");

                    if (targetType == typeof(FileImageSource))
                    {
                        // Nous sommes obligé de gérer ce cas à cause de Xamarin Button.
                        System.Diagnostics.Debug.WriteLine(String.Format(l_logHeader + "- Exception : " + Properties.Resources.ERR_ImageNameConverter_FileImageSource));
                        throw new ArgumentException(Properties.Resources.ERR_ImageNameConverter_FileImageSource);
                    }
                    else
                    {
                        try
                        {
                            ImageCache _imageCache = new ImageCache();
                            if (_imageCache.IsCached(l_Name))
                                return ImageSource.FromFile(_imageCache.GetImagePath(l_Name));

                            else
                            {                                
                               return ImageSource.FromStream(() =>
                                    {
                                        try
                                        {
                                            var l_HttpService = CoreSDK.Framework.Services.HttpService();
                                            var l_Stream = l_HttpService.Get(l_Name).Result;
                                            if (l_Stream == null)
                                                return GetDefaultStream(l_DefaultResourceName);
                                            l_Stream.Position = 0;
                                            l_Stream.Flush();
                                            byte[] TargetImageByte = ReadFully(l_Stream);
                                            l_Stream.Dispose();

                                            var r = _imageCache.AddImage(new MemoryStream(TargetImageByte), l_Name);

                                            return new MemoryStream(TargetImageByte);
                                        }
                                        catch (Exception)
                                        {

                                            return GetDefaultStream(l_DefaultResourceName);
                                        }                                     
                                    });                                
                             
                            }
                        }
                        catch (Exception l_Exception)
                        {
                            return ImageSource.FromStream(() => { return GetDefaultStream(l_DefaultResourceName); });
                        }
                    }
                }
                else if (l_Type == "urinocache")
                {
                    System.Diagnostics.Debug.WriteLine(l_logHeader + "- Mode = URINoCache");
                    if (targetType == typeof(FileImageSource))
                    {
                        // Nous sommes obligé de gérer ce cas à cause de Xamarin Button.
                        System.Diagnostics.Debug.WriteLine(String.Format(l_logHeader + "- Exception : " + Properties.Resources.ERR_ImageNameConverter_FileImageSource));
                        throw new ArgumentException(Properties.Resources.ERR_ImageNameConverter_FileImageSource);
                    }
                    else
                    {
                        return ImageSource.FromStream(() =>
                                   {
                                       try
                                       {
                                           var l_HttpService = CoreSDK.Framework.Services.HttpService();
                                           var l_Stream = l_HttpService.Get(l_Name).Result;
                                           if (l_Stream == null)
                                               return GetDefaultStream(l_DefaultResourceName);
                                           l_Stream.Position = 0;
                                           return l_Stream;
                                       }
                                       catch (Exception l_Exception)
                                       {
                                           return GetDefaultStream(l_DefaultResourceName);
                                       }
                                   });
                    }
                }
                else if (l_Type == "res")
                {
                    System.Diagnostics.Debug.WriteLine(l_logHeader + "- Mode = RESOURCE\n- Ref={0}", l_Name);

                    if (targetType == typeof(FileImageSource))
                    {
                        // Nous sommes obligé de gérer ce cas à cause de Xamarin Button.
                        System.Diagnostics.Debug.WriteLine(String.Format(l_logHeader + "- Exception : " + Properties.Resources.ERR_ImageNameConverter_FileImageSource));
                        throw new ArgumentException(Properties.Resources.ERR_ImageNameConverter_FileImageSource);
                    }
                    else
                    {
                        return ImageSource.FromStream(() =>
                        {
                            try
                            {
                                var l_Stream = CoreSDK.Framework.Services.Container().Resolve<IAssemblyService>().GetResourceStream(l_Name);
                                if (l_Stream == null)
                                {
                                    l_Stream = GetDefaultStream(l_DefaultResourceName);
                                }
                                l_Stream.Position = 0;
                                return l_Stream;
                            }
                            catch
                            {
                                return GetDefaultStream(l_DefaultResourceName);
                            }
                        });
                    }
                }
                else
                {
                    throw new ArgumentException(string.Format(Properties.Resources.ERR_ImageNameConverter_BadImageNameFormat, l_FullName));
                }
            }
            catch (ArgumentException l_ArgumentException)
            {
                // On remonte ces erreurs qui sont de type bogues.
                throw;
            }
            catch (Exception l_Exception)
            {
                return ImageSource.FromStream(() =>
                {
                    return GetDefaultStream(l_DefaultResourceName);
                });
            }
        }

        public static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
