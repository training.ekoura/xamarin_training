﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK
{
    public class HorlogeStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                switch (value.ToString())
                {
                    case "red":
                        value = Application.Current.Resources["TimerRedImageStyle"];
                        break;
                    case "orange":
                        value = Application.Current.Resources["TimerOrangeImageStyle"];
                        break;
                    case "gray":
                        value = Application.Current.Resources["TimerDarkGrayImageStyle"];
                        break;
                    default:
                        value = Application.Current.Resources["TimerWhiteImageStyle"];
                        break;
                }
                
                return value;
            }
            else
                //breaks the application if value is 0
                value = 12.5;
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }
    }
}
