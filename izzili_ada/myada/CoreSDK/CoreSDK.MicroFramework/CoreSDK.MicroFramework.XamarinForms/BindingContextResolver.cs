﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK
{
    public class BindingContextResolver
    {
        public static readonly BindableProperty TypeProperty = BindableProperty.CreateAttached<BindingContextResolver, Type>(
               bindable => BindingContextResolver.GetType(bindable),
               null, /* default value */
               BindingMode.OneWay,
               null,
               (b, o, n) => BindingContextResolver.OnTypeChanged(b, o, n),
               null,
               null);

        public static void SetType(BindableObject p_Target, Type p_Type)
        {

        }

        public static Type GetType(BindableObject p_Target)
        {
            return null;
        }

        public static void OnTypeChanged(BindableObject p_Target, Type o, Type n)
        {
            var l_Instance = CoreSDK.Framework.Services.Container().Resolve(n);
            p_Target.BindingContext = l_Instance;
        }
    }
}
