﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CoreSDK
{
    public class RssFeedReader
    {
        /// <summary>Récupère les items RSS d'une URL fournie et les renvoie sous forme d'objet Post.</summary>
        /// <param name="url">L'URL du flux RSS fourni.</param>
        /// <returns>Object Post.</returns>
        public IEnumerable<Post> ReadItem(string url)
        {
            var rssFeed = XDocument.Load(url);

            var posts = from item in rssFeed.Descendants("item") select new Post
            {
                Title         = item.Element("title").Value,
                Description = item.Element("description").Value,
                PublishedDate = item.Element("pubDate").Value,
                Category      = item.Element("category").Value,
                Link          = item.Element("link").Value
            };

            return posts;
        }
    }

    public class Post
    {
        public string Title         { get; set; }
        public string Description   { get; set; }
        public string PublishedDate { get; set; }
        public string Category      { get; set; }
        public string Link          { get; set; }
    }
}
