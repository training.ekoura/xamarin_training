﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace CoreSDK
{
    public static class FrameworkXamarinForms
    {
        public static void Initialize()
        {
            // Service de gestion des thèmes.
            CoreSDK.Framework.Services.Container().Register<CoreSDK.Themes.IThemeService, CoreSDK.Themes.ThemeServiceXamarin>(new CoreSDK.SingletonInstanceManager());

            // Service de navigation.
            // Xamarin met en place un système de navigation qui peut être utilisé si il suffit pour répondre aux besoin.
            // Ceci n'est qu'une implémentation en alternative donnée en exemple.
            CoreSDK.Framework.Services.Container().Register<CoreSDK.IInteractionService, NavigationPageInteractionService>(new CoreSDK.SingletonInstanceManager());

            // Enregistrement de l'assembly
            CoreSDK.Framework.Services.AssemblyService().RegisterAssembly(typeof(FrameworkXamarinForms).GetTypeInfo().Assembly);
        }
    }
}
