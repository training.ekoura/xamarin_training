﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using CoreSDK.Controls;

using Xamarin.Forms;

namespace CoreSDK.ContentViews
{
    public class StatusFilterContentView : ContentView
    {
        public StatusFilterContentView()
        {
            #region important
            VerticalOptions = LayoutOptions.FillAndExpand;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            Padding = new Thickness(20, 0);
            #endregion

            #region Header
            var header = new Label()
            {
                Text = Properties.Resources.StatutFilterContent_Header_Text,
            };
            header.SetDynamicResource(Label.StyleProperty, "TitlePopinStyle");
            #endregion

            var buttonList = new RepeaterView()
            {
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                 
                ItemSpacing = Device.OnPlatform(23, 23, 5),
                #region ==Template==
                ItemTemplate = new DataTemplate(() =>
                {
                    var commandButton = new CustomButton()
                    {
                        ButtonType = CustomButton.ButtonTypes.RoundedPopInChecked,
                        ImageButton = CustomButton.ImageForButton.ImageOnRight,
                        CenteredItems = false,
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        HorizontalLabelOption = LayoutOptions.Start,
                        VerticalLabelOption = LayoutOptions.Center,
                        VerticalImageOption = LayoutOptions.Center,
                        //IsButtonChecked=false,
                    };
                    commandButton.SetBinding(CustomButton.ButtonLabelProperty, "LibelleGerant");
                    commandButton.SetBinding(CustomButton.IsButtonCheckedProperty, "IsChecked", BindingMode.TwoWay);
                    commandButton.SetBinding(CustomButton.ButtonColorProperty,"StatutCodeCouleur",BindingMode.Default, new StringToColorConverter(), null);                   
                    return commandButton;
                })
                #endregion
               
            };
            buttonList.SetBinding(RepeaterView.ItemsSourceProperty, "ListStatuts");

            var modifierButtonStack = new StackLayout()
            {
                Padding = new Thickness(40, 10),
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Children = { buttonList }
            };

            var modifierButtonScrollView = new ScrollView()
            {
                HeightRequest= Device.OnPlatform(400,400,400),
                HorizontalOptions=LayoutOptions.FillAndExpand,
                VerticalOptions=LayoutOptions.FillAndExpand,
                Content=modifierButtonStack
            };
            

            #region Button validate
            var validateButton = new RoundedButton()
            {
                Text = Properties.Resources.StatutFilterContent_ButtonValidation_Text,
                TextColor = Color.Red,
                WidthRequest = 15,
                FontSize = 19
            };

            validateButton.SetBinding(RoundedButton.CommandProperty, "FilterStatutCommand");
            validateButton.SetDynamicResource(RoundedButton.StyleProperty, "RoundedButtonStyle");
            validateButton.WidthRequest = Device.OnPlatform(190, 190, 310);
            #endregion

            var ContentStack = new StackLayout()
            {
                Padding = new Thickness(10, Device.OnPlatform(10, 10, 20), 10, 20),
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Spacing = 15,
                Children = { header, modifierButtonScrollView, validateButton }
            };

            Content = ContentStack;
            buttonList.Update();
        }
    }
}
