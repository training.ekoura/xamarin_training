﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using CoreSDK.Controls;

using Xamarin.Forms;

namespace CoreSDK.ContentViews
{
    public class ModifierStockContentView : ContentView
    {
        public ModifierStockContentView()
        {
            VerticalOptions = LayoutOptions.FillAndExpand;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            Padding = new Thickness(20, 0);

            #region Header
            var header = new Label()
            {
            };
            header.SetDynamicResource(Label.StyleProperty, "TitlePopinStyle");
            header.SetBinding(Label.TextProperty, "HeadTitleStockLabel");
            #endregion

            #region meal
            var mealHeader = new Label()
            {
            };
            mealHeader.SetDynamicResource(Label.StyleProperty, "PopinContentTextStyle");
            mealHeader.SetBinding(Label.TextProperty, "StockArticleSelected.LibelleCommercial");

            var mealContent = new Label()
            {
            };
            mealContent.SetDynamicResource(Label.StyleProperty, "PopinContentTextStyle");
            mealContent.SetBinding(Label.TextProperty, "StockArticleSelected.LibelleTechnique");

            var mealStack = new StackLayout()
            {
                VerticalOptions=LayoutOptions.Fill,
                HorizontalOptions=LayoutOptions.FillAndExpand,
                Spacing=5,
                Children = { mealHeader, mealContent }
            };

            #endregion

            #region chifre
			var chifre = new CustomEntry()
            {
                //Text=""
                TextColor = Color.Black,
                Placeholder = "_ _",
                PlaceholderColor = Color.Black,
                Keyboard = Keyboard.Numeric,
				Font= Font.SystemFontOfSize(70),
				FontSize= 70,
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.Center,
                StyleId = "StockEntry"
            };
            chifre.SetBinding(Entry.TextProperty, "TypedNumber");
            chifre.TextChanged += (sender, args) =>
            {
                if (chifre.Text.Contains("."))
                {
                    chifre.Text = chifre.Text.Remove(chifre.Text.Length - 1);  
                }

                // Remove 3rd character as user types
                if (chifre.Text.Length > 2)       
                {
                    chifre.Text = chifre.Text.Remove(chifre.Text.Length - 1);  
                }
            };
            chifre.Unfocused += (sender, args) =>
            {
                string _text = chifre.Text;
                chifre.Text = (_text.Length == 1) ? string.Format("0{0}", _text) : _text;
            };

            var chifreStack = new StackLayout()
            {
                Padding= new Thickness(0,20,0,40),
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Children={chifre}
            };
            #endregion

            #region Button (ajouter/reduire)
            var modifierButton = new RoundedButton()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            modifierButton.SetDynamicResource(RoundedButton.StyleProperty, "RoundedButtonStyle");
            modifierButton.SetBinding(RoundedButton.TextProperty, "ButtonAddRemoveStockText");
            modifierButton.SetBinding(RoundedButton.CommandProperty, "UpdateStockCommand");

            var modifierButtonStack = new StackLayout()
            {
                Padding = new Thickness(60, 0),
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Children = { modifierButton}
            };

            #endregion

            var ContentStack = new StackLayout()
            {
                Padding = new Thickness(10, Device.OnPlatform(10, 10, 20), 10, 20),
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Spacing=15,
                Children = { header, mealStack, chifre, modifierButtonStack }
            };

            Content = ContentStack;
        }
    }
}
