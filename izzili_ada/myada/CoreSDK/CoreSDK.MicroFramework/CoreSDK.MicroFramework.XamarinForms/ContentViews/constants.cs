﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK.ContentViews
{
    public static class constants
    {
        public const string ModifierStockContentView = "ModifierStockContentView";
        public const string PlatstatusContentView = "PlatstatusContentView";
        public const string CalndarContentView = "CalndarContentView";
        public const string YesNoContentView = "YesNoContentView";
        public const string StatusFilterContentView = "StatusFilterContentView";
    }
}
