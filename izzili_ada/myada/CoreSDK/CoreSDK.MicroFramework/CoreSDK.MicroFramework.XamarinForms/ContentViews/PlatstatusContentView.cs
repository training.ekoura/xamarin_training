﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using CoreSDK.Controls;

using Xamarin.Forms;

namespace CoreSDK.ContentViews
{
    public class PlatstatusContentView : ContentView
    {
        public PlatstatusContentView()
        {
            VerticalOptions = LayoutOptions.FillAndExpand;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            Padding = new Thickness(20, 0);

            #region Header
            var header = new Label()
            {
                Text="Changez le statut de plat",
                //FontSize=32,
                //TextColor=Color.Black,
                //VerticalOptions=LayoutOptions.Fill,
                //HorizontalOptions=LayoutOptions.FillAndExpand,
                //VerticalTextAlignment=TextAlignment.Center,
                //HorizontalTextAlignment=TextAlignment.Center
            };
            header.SetDynamicResource(Label.StyleProperty, "TitlePopinStyle");
            #endregion

            #region Button (ajouter/reduire)
            var buttonList = new RepeaterView()
            {
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                ItemSpacing = Device.OnPlatform(10,8,5),
                 #region ==Template==
                ItemTemplate = new DataTemplate(() =>
                {
                    
                    var commandButton = new CustomButton()
                    {
                        ButtonType=CustomButton.ButtonTypes.RoundedPopIn,
                        ImageButton = CustomButton.ImageForButton.ImageOnRight,
                        CenteredItems = false,
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        HorizontalLabelOption = LayoutOptions.Start,
                        VerticalLabelOption = LayoutOptions.Center,
                        VerticalImageOption = LayoutOptions.Center,
                      //  ImageButtonSource = "Sodexo.Images.Gerant.arrow.png",

                    };
                    //commandButton.SetBinding(CustomButton.ButtonColorProperty, "");
                    commandButton.SetBinding(CustomButton.ButtonLabelProperty, "LibelleGerant");
                    commandButton.SetBinding(CustomButton.TapCommandParameterProperty, ".");
                    commandButton.SetBinding(CustomButton.TapCommandProperty, new Binding("UpdateStatutCommand") { Source=this.BindingContext });
                    commandButton.SetBinding(CustomButton.ButtonColorProperty,"StatutCodeCouleur",BindingMode.Default, new StringToColorConverter(), null);

                    return commandButton;
                })
                #endregion
            };
            buttonList.SetBinding(RepeaterView.ItemsSourceProperty, "ListStatuts");

            var modifierButtonStack = new StackLayout()
            {
                Padding = new Thickness(40, 0),
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Children = { buttonList }
            };

            #endregion

            var ContentStack = new StackLayout()
            {
                Padding = new Thickness(10, Device.OnPlatform(10, 10, 20), 10, 20),
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Spacing=15,
                Children = { header, modifierButtonStack }
            };

            Content = ContentStack;
        }
    }
}
