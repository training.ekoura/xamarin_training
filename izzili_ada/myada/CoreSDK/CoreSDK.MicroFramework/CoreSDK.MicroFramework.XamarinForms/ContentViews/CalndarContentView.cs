﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using CoreSDK.Controls;

using Xamarin.Forms;

namespace CoreSDK.ContentViews
{
    public class CalndarContentView : ContentView
    {
        public CalndarContentView()
        {
            VerticalOptions = LayoutOptions.FillAndExpand;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            Padding = new Thickness(20, 0);

            var calendar = new Calendar() { };
            calendar.SetBinding(Calendar.ReceivedDateProperty, "SelectedDate");

            var voirCommandesButton = new RoundedButton()
            {
                Text = Properties.Resources.Calendar_ContentView_Button_Text,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.Center
            };

            voirCommandesButton.SetDynamicResource(RoundedButton.StyleProperty, "RoundedButtonStyle");
            voirCommandesButton.SetBinding(RoundedButton.CommandProperty, "ChangeDateCommand");
            voirCommandesButton.SetBinding(RoundedButton.CommandParameterProperty, new Binding("SelectedDate") { Source = calendar });

            var ContentStack = new StackLayout()
            {
                Padding = new Thickness(10, Device.OnPlatform(10,10,20),10,20),
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Spacing=0,
                Children = { calendar, voirCommandesButton }
            };

            Content = ContentStack;
        }
    }
}
