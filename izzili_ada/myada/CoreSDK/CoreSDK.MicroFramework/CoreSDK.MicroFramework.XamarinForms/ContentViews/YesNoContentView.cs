﻿using CoreSDK.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Xamarin.Forms;

namespace CoreSDK.ContentViews
{
    public class YesNoContentView : ContentView
    {
        public YesNoContentView()
        {
            VerticalOptions = LayoutOptions.FillAndExpand;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            Padding = new Thickness(20, 0);

            #region Header

            Label header = new Label() { Text = Properties.Resources.YesNo_ContentView_Header_Text };
            header.SetDynamicResource(Label.StyleProperty, "TitlePopinStyle");

            #endregion

            #region Controls

            Label label = new Label()
            {
                TextColor = Color.Black,
                HorizontalTextAlignment = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };
            label.SetBinding(Label.TextProperty, "TextLabelPopinYesNo");

            #region Gestion de la taille du texte en fonction des devices et plateformes

            switch (Device.Idiom)
            {
                case TargetIdiom.Desktop:
                    label.FontSize = 16;
                    break;
                case TargetIdiom.Phone:
                    switch (Device.OS)
	                {
                        case TargetPlatform.Android:
                            label.FontSize = 16;
                            break;
                        case TargetPlatform.Other:
                            label.FontSize = 16;
                            break;
                        case TargetPlatform.WinPhone:
                            label.FontSize = 18;
                            break;
                        case TargetPlatform.Windows:
                            label.FontSize = 16;
                            break;
                        case TargetPlatform.iOS:
                            label.FontSize = 14;
                            break;
                        default:
                            break;
	                }
                    break;
                case TargetIdiom.Tablet:
                    label.FontSize = 16;
                    break;
                case TargetIdiom.Unsupported:
                    label.FontSize = 16;
                    break;
                default:
                    break;
            }

            #endregion

            Grid.SetRow(label, 0);
            Grid.SetColumnSpan(label, 2);

            RoundedButton rbtYes = new RoundedButton()
            {
                Text = Properties.Resources.Deconnexion_ContentView_ButtonYes_Text,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            rbtYes.SetDynamicResource(RoundedButton.StyleProperty, "RoundedButtonStyle");
            rbtYes.SetBinding(RoundedButton.CommandProperty, "ClickYesCommand");

            Grid.SetRow(rbtYes, 1);
            Grid.SetColumn(rbtYes, 0);

            RoundedButton rbtNo = new RoundedButton()
            {
                Text = Properties.Resources.Deconnexion_ContentView_ButtonNo_Text,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            rbtNo.SetDynamicResource(RoundedButton.StyleProperty, "RoundedButtonStyle");
            rbtNo.SetBinding(RoundedButton.CommandProperty, "ClickNoCommand");

            Grid.SetRow(rbtNo, 1);
            Grid.SetColumn(rbtNo, 1);

            #endregion

            #region Layouts

            Grid ContentGrid = new Grid()
            {
                ColumnDefinitions = new ColumnDefinitionCollection
                {
                    new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) }
                },
                RowDefinitions = new RowDefinitionCollection
                {
                    new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) },
                    new RowDefinition() { Height = new GridLength(2, GridUnitType.Star) }
                },
                RowSpacing = 10
            };

            var ContentStack = new StackLayout()
            {
                Padding = new Thickness(10, 10, 10, 30),
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Spacing = 15
            };

            // Les boutons ont un rendu naturellement plus petits sur Windows, on adapter sur les autres plateformes
            if(Device.OS == TargetPlatform.WinPhone)
            {
                ContentGrid.Padding = new Thickness(0, 0, 0, 10);

                ContentGrid.Children.Add(label);
                ContentGrid.Children.Add(rbtYes);
                ContentGrid.Children.Add(rbtNo);

                ContentStack.Children.Add(header);
                ContentStack.Children.Add(ContentGrid);
            }
            else
            {
                ContentGrid.Padding = new Thickness(20, 0, 20, 10);
                ContentGrid.RowDefinitions[0].Height = new GridLength(1, GridUnitType.Auto);
                ContentGrid.ColumnSpacing = 20;

                ContentGrid.Children.Add(rbtYes);
                ContentGrid.Children.Add(rbtNo);

                ContentStack.Children.Add(header);
                ContentStack.Children.Add(label);

                // Sinon le ContentGrid est placé avant le Label
                if (ContentStack.Children.Contains(label)) ContentStack.Children.Add(ContentGrid);
            }

            #endregion

            Content = ContentStack;
        }
    }
}
