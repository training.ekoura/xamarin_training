﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Xamarin.Forms;
using CoreSDK.ContentViews;

namespace CoreSDK.ContentViews
{
    public class PopInView : ContentView
    {
        public PopInView(bool IsBurger, string PopInViewName)
        {
            var ContentStack = new StackLayout()
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Spacing = 0,
                Children =
                {
                    new PopUpheader(),
                }
            };

            switch (PopInViewName)
            {
                case constants.ModifierStockContentView:
                    ContentStack.Children.Add(new ModifierStockContentView());
                    break;
                case constants.PlatstatusContentView:
                    ContentStack.Children.Add(new PlatstatusContentView());
                    break;
                case constants.CalndarContentView:
                    ContentStack.Children.Add(new CalndarContentView());
                    break;
                case constants.YesNoContentView:
                    ContentStack.Children.Add(new YesNoContentView());
                    break;
                case constants.StatusFilterContentView:
                    ContentStack.Children.Add(new StatusFilterContentView());
                    break;
                default:
                    break;
            }

            var ContentStackFormater = new StackLayout()
            {
                Children = { ContentStack }
            };

            if (!(IsBurger && (Device.Idiom == TargetIdiom.Tablet)))
            {
                ContentStackFormater.SetDynamicResource(StackLayout.StyleProperty, "DarkPaddedPopInStyle");
            }

            Content = ContentStackFormater;

        }
    }
}
