﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace CoreSDK.ContentViews
{
    public class PopUpheader : ContentView
    {
        public PopUpheader()
        {
            //HeightRequest=60;
            //VerticalOptions = LayoutOptions.FillAndExpand;
            //HorizontalOptions = LayoutOptions.FillAndExpand;
            
            var contentGrid = new Grid()
            {
                //VerticalOptions = LayoutOptions.FillAndExpand,
                //HorizontalOptions = LayoutOptions.FillAndExpand,
                //RowSpacing=0,
                //ColumnSpacing=0,
                //Padding=0,
                RowDefinitions =  { 
                            new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) },
                            new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) },
                        },
                ColumnDefinitions =  { 
                            new ColumnDefinition() { Width = 20 },
                            new ColumnDefinition() { Width = 40 },
                            new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
                            new ColumnDefinition() { Width = 40 },
                            new ColumnDefinition() { Width = 20 },
                        },
            };
            contentGrid.SetDynamicResource(Grid.StyleProperty, "DefaultGridStyle");

            #region transparentStack
            var transparentStack = new StackLayout()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor=Color.Transparent,
            };
            #endregion

            Grid.SetRow(transparentStack, 0);
            Grid.SetColumn(transparentStack, 0);
            Grid.SetColumnSpan(transparentStack, 5);
            contentGrid.Children.Add(transparentStack);

            #region WhiteStack
            var whiteStack = new StackLayout()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.White,
            };
            #endregion

            Grid.SetRow(whiteStack, 1);
            Grid.SetColumn(whiteStack, 1);
            Grid.SetColumnSpan(whiteStack, 3);
            contentGrid.Children.Add(whiteStack);

            #region cancelImage
            var cancelImage = new Image()
            {
                WidthRequest=Device.OnPlatform(40,40,50),
                HeightRequest = Device.OnPlatform(40, 40, 50),
                Source = ImageSource.FromResource("Sodexo.Images.Gerant.CloseSco.png")
            };
            cancelImage.SetDynamicResource(Image.StyleProperty, "popIncancelImageStyle");
            #endregion

            Grid.SetRow(cancelImage, 0);
            Grid.SetRowSpan(cancelImage, 2);
            Grid.SetColumn(cancelImage, 3);
            Grid.SetColumnSpan(cancelImage, 2);
            contentGrid.Children.Add(cancelImage);


            #region cancelImageStack
            var cancelImageStack = new StackLayout()
            {
                VerticalOptions=LayoutOptions.FillAndExpand,
                HorizontalOptions=LayoutOptions.FillAndExpand,
                BackgroundColor=Color.Transparent
            };
            var cancelImageGesture = new TapGestureRecognizer();
            cancelImageGesture.SetBinding(TapGestureRecognizer.CommandProperty, "ClosePopInCommand");
            cancelImageStack.GestureRecognizers.Add(cancelImageGesture);
            #endregion

            Grid.SetRow(cancelImageStack, 0);
            Grid.SetRowSpan(cancelImageStack, 2);
            Grid.SetColumn(cancelImageStack, 3);
            Grid.SetColumnSpan(cancelImageStack, 2);
            contentGrid.Children.Add(cancelImageStack);


            this.SetDynamicResource(ContentView.StyleProperty, "PopUpHeaderStyle");
            Content = contentGrid;
        }
    }
}
