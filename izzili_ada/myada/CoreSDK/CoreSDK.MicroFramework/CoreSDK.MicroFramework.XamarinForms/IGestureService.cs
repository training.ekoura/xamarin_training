﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK
{
    public interface IGestureService
    {
        void Hook(View p_Object);
    }
}
