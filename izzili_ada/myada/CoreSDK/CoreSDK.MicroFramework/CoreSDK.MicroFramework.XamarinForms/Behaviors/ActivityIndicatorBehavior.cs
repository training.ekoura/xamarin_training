﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK.Behaviors
{
    public class ActivityIndicatorBehavior : Xamarin.Forms.Behavior<Grid>
    {
        #region === Label ===

        public static readonly BindableProperty LabelProperty = BindableProperty.Create<ActivityIndicatorBehavior, string>(p => p.Label, default(string), propertyChanged: (d, o, n) => (d as ActivityIndicatorBehavior).Label_Changed(o, n));

        private void Label_Changed(string oldvalue, string newvalue)
        {
            UpdateLabel();
        }

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        #endregion

        #region === LabelColor ===

        public static readonly BindableProperty LabelColorProperty = BindableProperty.Create<ActivityIndicatorBehavior, Color>(p => p.LabelColor, Color.White, propertyChanged: (d, o, n) => (d as ActivityIndicatorBehavior).LabelColor_Changed(o, n));

        private void LabelColor_Changed(Color oldvalue, Color newvalue)
        {
            UpdateLabel();
        }

        public Color LabelColor
        {
            get { return (Color)GetValue(LabelColorProperty); }
            set { SetValue(LabelColorProperty, value); }
        }

        #endregion

        #region === BackgroundColor ===

        public static readonly BindableProperty BackgroundColorProperty = BindableProperty.Create<ActivityIndicatorBehavior, Color>(p => p.BackgroundColor, Color.FromRgba(0,0,0,0.5), propertyChanged: (d, o, n) => (d as ActivityIndicatorBehavior).BackgroundColor_Changed(o, n));

        private void BackgroundColor_Changed(Color oldvalue, Color newvalue)
        {
            if (_SubGrid == null) return;
            _SubGrid.BackgroundColor = BackgroundColor;
        }

        public Color BackgroundColor
        {
            get { return (Color)GetValue(BackgroundColorProperty); }
            set { SetValue(BackgroundColorProperty, value); }
        }

        #endregion

        private Grid _AssociatedObject;
        protected Grid AssociatedObject
        {
            get { return _AssociatedObject; }
        }

        protected override void OnAttachedTo(Grid bindable)
        {
            base.OnAttachedTo(bindable);
            _AssociatedObject = bindable;
            bindable.BindingContextChanged += bindable_BindingContextChanged;
            AttachToObservableObject();
            Update();
        }

        protected override void OnDetachingFrom(Grid bindable)
        {
            DettachFromObservableObject();
            bindable.BindingContextChanged -= bindable_BindingContextChanged;
            _AssociatedObject = null;
            base.OnDetachingFrom(bindable);
        }

        void bindable_BindingContextChanged(object sender, EventArgs e)
        {
            DettachFromObservableObject();
            AttachToObservableObject();
            Update();
        }

        private ObservableObject _ObservableObject;

        private void AttachToObservableObject()
        {
            if (_AssociatedObject == null) return;
            _ObservableObject = _AssociatedObject.BindingContext as ObservableObject;
            if(_ObservableObject != null)
            {
                _ObservableObject.WorkStarted += _ObservableObject_WorkStarted;
                _ObservableObject.WorkStoped += _ObservableObject_WorkStoped;
            }
        }

        private void DettachFromObservableObject()
        {
            if (_AssociatedObject == null) return;
            if (_ObservableObject != null)
            {
                _ObservableObject.WorkStarted -= _ObservableObject_WorkStarted;
                _ObservableObject.WorkStoped -= _ObservableObject_WorkStoped;
            }
            _ObservableObject = null;
        }

        void _ObservableObject_WorkStarted(object sender, EventArgs e)
        {
            Update();
        }

        void _ObservableObject_WorkStoped(object sender, EventArgs e)
        {
            Update();
        }

        private Grid _SubGrid;

        private Label _Label;

        private void Update()
        {
            lock (this)
            {
                try
                {
                    if (_AssociatedObject == null || _ObservableObject == null) return;

                    Device.BeginInvokeOnMainThread(() =>
                    {

                        if (_ObservableObject.IsWorking)
                        {
                            if (_SubGrid == null)
                            {
                                _SubGrid = new Grid();
                                Grid.SetRowSpan(_SubGrid, Math.Max(1, _AssociatedObject.RowDefinitions.Count));
                                Grid.SetColumnSpan(_SubGrid, Math.Max(1, _AssociatedObject.ColumnDefinitions.Count));
                                _SubGrid.HorizontalOptions = LayoutOptions.Fill;
                                _SubGrid.VerticalOptions = LayoutOptions.Fill;
                                _SubGrid.BackgroundColor = BackgroundColor;

                                var l_StackLayout = new StackLayout();
                                l_StackLayout.HorizontalOptions = LayoutOptions.Center;
                                l_StackLayout.VerticalOptions = LayoutOptions.Center;

                                var l_ActivityIndicator = new ActivityIndicator() { Scale = Device.OnPlatform(1.5, 1, 1) };
                                l_ActivityIndicator.IsRunning = true;
                                l_ActivityIndicator.Color = Color.White;
                                l_ActivityIndicator.HorizontalOptions = LayoutOptions.Center;
                                l_ActivityIndicator.VerticalOptions = LayoutOptions.Center;
                                l_ActivityIndicator.WidthRequest = 128;
                                l_StackLayout.Children.Add(l_ActivityIndicator);

                                _Label = new Label();
                                _Label.HorizontalOptions = LayoutOptions.Center;
                                UpdateLabel();
                                l_StackLayout.Children.Add(_Label);

                                _SubGrid.Children.Add(l_StackLayout);

                                _AssociatedObject.Children.Add(_SubGrid);
                            }
                        }
                        else
                        {
                            if (_SubGrid != null)
                            {
                                _AssociatedObject.Children.Remove(_SubGrid);
                                _SubGrid = null;
                            }
                        }
                    });
                }
                catch (Exception)
                {


                }
            }
         
          
        }

        private void UpdateLabel()
        {
            if (_Label == null) return;
            _Label.Text = Label;
            _Label.TextColor = LabelColor;
        }
    }
}
