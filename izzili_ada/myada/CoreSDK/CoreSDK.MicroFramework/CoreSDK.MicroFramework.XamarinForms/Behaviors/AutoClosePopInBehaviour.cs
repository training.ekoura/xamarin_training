﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK.Behaviors
{
    public class AutoClosePopInBehaviour : Xamarin.Forms.Behavior<MasterDetailPage>
    {

        protected override void OnAttachedTo(MasterDetailPage view)
        {
            base.OnAttachedTo(view);

            view.IsPresentedChanged += view_IsPresentedChanged;
        }

        void view_IsPresentedChanged(object sender, EventArgs e)
        {
            var content = sender as MasterDetailPage;

            if(!(content).IsPresented)
            {
                var viewsChildren = ((Grid)((ContentPage)(content.Master)).Content).Children;
                ContentView viewToRemove= null;

                foreach (var viewElement in viewsChildren .Where(x => x.StyleId == "PopInContainer"))
                {
                    viewToRemove= (ContentView)viewElement;
                }

                if(viewToRemove!=null)
                    viewsChildren.Remove(viewToRemove);
            }
        }

        protected override void OnDetachingFrom(MasterDetailPage view)
        {
            base.OnDetachingFrom(view);
        }
    }
}
