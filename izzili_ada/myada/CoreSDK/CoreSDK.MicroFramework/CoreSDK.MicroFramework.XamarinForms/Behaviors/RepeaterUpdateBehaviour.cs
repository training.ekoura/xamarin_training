﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK.Behaviors
{
    public class RepeaterUpdateBehaviour : Xamarin.Forms.Behavior<View>
    {

        protected async override void OnAttachedTo(View view)
        {
            base.OnAttachedTo(view);

            for (int i = 0; i < 4; i++ )
            {
                await Task.Delay(300);
                view.SetDynamicResource(View.BackgroundColorProperty, "GerantMainColor");
                await Task.Delay(300);
                view.BackgroundColor = Color.Transparent;
            }
        }

        protected override void OnDetachingFrom(View view)
        {
            base.OnDetachingFrom(view);
        }
    }
}
