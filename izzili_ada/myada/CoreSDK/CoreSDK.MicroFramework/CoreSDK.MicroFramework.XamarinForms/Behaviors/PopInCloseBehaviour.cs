﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK.Behaviors
{
    public class PopInCloseBehaviour : Xamarin.Forms.Behavior<ContentView>
    {

        protected override void OnAttachedTo(ContentView view)
        {
            base.OnAttachedTo(view);

            view.PropertyChanged += view_PropertyChanged;
        }

        void view_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
			//test if the popIn.IsVisible has been changed to false
            if (e.PropertyName=="IsVisible")
            {
                if(!(sender as ContentView).IsVisible)
                {
					//gets the currentPage where the popIn has been placed on top of
                    var parentView = ((sender as ContentView).Parent as Grid);


					//keeps track of contents on the currentPage and finally removes all content added with PopIn View
					//removes all children after the PoPIn index and also the PoPIn to:
					int generalCount = 1;
					int popInPosition = 1;
					foreach (var child in parentView.Children) 
					{
						if(child.StyleId!="PopInContainer")
						{
							generalCount++;
						}
						else
							popInPosition=generalCount;

					}

					for(int i=parentView.Children.Count; i>= popInPosition; i--)
					{
						parentView.Children.RemoveAt(i-1);
					}
                }
            }
        }

        protected override void OnDetachingFrom(ContentView view)
        {
            base.OnDetachingFrom(view);
        }
    }
}
