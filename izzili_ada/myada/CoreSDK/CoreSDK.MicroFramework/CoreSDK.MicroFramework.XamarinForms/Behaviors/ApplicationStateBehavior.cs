﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK.Behaviors
{
    public enum ApplicationStateBehaviorRuleType 
    { 
        /// <summary>
        /// Toutes les états doivent être actifs.
        /// </summary>
        HasAll,
        /// <summary>
        /// Au moins un des états doit être actif.
        /// </summary>
        HasOne, 
        /// <summary>
        /// Aucun des états ne doit être actif.
        /// </summary>
        HasNone 
    }

    /// <summary>
    /// Behavior de base pour créer des comportement UI qui s'adaptent automatiquement en fonction 
    /// des changements d'états applicatifs.
    /// 
    /// Il suffit de surcharger DoUpdate() pour implémenter le comportement souhaité.
    /// </summary>
    public abstract class ApplicationStateBehavior<T> : Xamarin.Forms.Behavior<T> where T : Xamarin.Forms.BindableObject
    {
        private T _AssociatedObject;
        protected T AssociatedObject
        {
            get { return _AssociatedObject; }
        }

        private ApplicationStateBehaviorRuleType _Rule = ApplicationStateBehaviorRuleType.HasAll;
        /// <summary>
        /// Règle d'analyse des états applicatifs.
        /// Par défaut, tous les états doivent être actifs.
        /// </summary>
        public ApplicationStateBehaviorRuleType Rule
        {
            get { return _Rule; }
            set 
            {
                if (_Rule != value)
                {
                    _Rule = value;
                    Update();
                }
            }
        }

        private bool _Reverse;
        /// <summary>
        /// Si true, inverse la logique.
        /// </summary>
        public bool Reverse
        {
            get { return _Reverse; }
            set 
            {
                if (_Reverse != value)
                {
                    _Reverse = value;
                    Update();
                }
            }
        }

        private string _States;
        /// <summary>
        /// Un ou plusieurs états applicatifs, séparés par des virgules.
        /// </summary>
        public string States
        {
            get { return _States; }
            set 
            {
                if (_States != value)
                {
                    _StatesAsArray = null;
                    _States = value;
                    Update();
                }
            }
        }

        private string[] _StatesAsArray;

        protected override void OnAttachedTo(T bindable)
        {
            base.OnAttachedTo(bindable);
            _AssociatedObject = bindable;
            CoreSDK.Framework.Services.ApplicationStateService().StateChanged += ApplicationStateBehavior_StateChanged;
            Update();
        }

        protected override void OnDetachingFrom(T bindable)
        {
            CoreSDK.Framework.Services.ApplicationStateService().StateChanged -= ApplicationStateBehavior_StateChanged;
            _AssociatedObject = default(T);
            base.OnDetachingFrom(bindable);
        }

        private void ApplicationStateBehavior_StateChanged(object sender, CoreSDK.ApplicationStateChangeEventArgs e)
        {
            Update();
        }

        private void Update()
        {
            if (_AssociatedObject == null) return;
            if (_StatesAsArray == null) _StatesAsArray = (_States ?? string.Empty).Split(',').Select(r=>r.Trim()).ToArray();
            var l_RuleIsValide = false;

            switch(_Rule)
            {
                case ApplicationStateBehaviorRuleType.HasAll:
                    l_RuleIsValide = CoreSDK.Framework.Services.ApplicationStateService().HasAll(_StatesAsArray);
                    break;
                case ApplicationStateBehaviorRuleType.HasNone:
                    l_RuleIsValide = CoreSDK.Framework.Services.ApplicationStateService().HasNone(_StatesAsArray);
                    break;
                case ApplicationStateBehaviorRuleType.HasOne:
                    l_RuleIsValide = CoreSDK.Framework.Services.ApplicationStateService().HasOne(_StatesAsArray);
                    break;
            }

            DoUpdate(_Reverse ? !l_RuleIsValide : l_RuleIsValide);
        }

        /// <summary>
        /// Surcharger cette méthode.
        /// </summary>
        /// <param name="p_RuleIsValide">true si la règle est valide.</param>
        protected abstract void DoUpdate(bool p_RuleIsValide);
    }
}
