﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK.Behaviors
{
    public class BurgerListViewItemClickBehaviour : Xamarin.Forms.Behavior<ListView>
    {

        protected override void OnAttachedTo(ListView view)
        {
            base.OnAttachedTo(view);

            view.ItemSelected += async(object sender, SelectedItemChangedEventArgs e)=>
            {
                var sBurgerView = (((((sender as ListView).Parent as StackLayout).Parent as Grid).Parent as Controls.AlternateBurger).Parent as Controls.TouchContentView);
                await sBurgerView.TranslateTo(480, 0, 0, Easing.Linear);
                if ((sBurgerView.Parent as Grid) != null)
                    (sBurgerView.Parent as Grid).Children.Remove(sBurgerView);
            };
        }

        protected override void OnDetachingFrom(ListView view)
        {
            base.OnDetachingFrom(view);
        }
    }
}
