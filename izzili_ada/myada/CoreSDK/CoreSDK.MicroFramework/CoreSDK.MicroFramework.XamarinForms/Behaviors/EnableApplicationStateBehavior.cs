﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK.Behaviors
{
    /// <summary>
    /// Active ou désactive une View en fonction d'une règle d'états applicatifs.
    /// </summary>
    public class EnableApplicationStateBehavior : ApplicationStateBehavior<Xamarin.Forms.View>
    {
        protected override void DoUpdate(bool p_RuleIsValide)
        {
            AssociatedObject.IsEnabled = p_RuleIsValide;
        }
    }
}
