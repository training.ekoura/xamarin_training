﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK.Behaviors
{
    public class GridButtonBehaviour : Xamarin.Forms.Behavior<Grid>
    {

        protected override void OnAttachedTo(Grid grid)
        {
            base.OnAttachedTo(grid);

            var gridLayoutTapGesture = new TapGestureRecognizer();
            gridLayoutTapGesture.Tapped += async (object sender, EventArgs e) =>
            {
                //animate a click effect
                await grid.ScaleTo(0.85, 35, Easing.SinIn);
                await grid.ScaleTo(1, 35, Easing.SinInOut);
            };

            grid.GestureRecognizers.Add(gridLayoutTapGesture);
        }

        protected override void OnDetachingFrom(Grid grid)
        {
            base.OnDetachingFrom(grid);
        }
    }
}
