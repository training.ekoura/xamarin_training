﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK
{
    public static class ThemeNameForCarousel
    {
        public const string MySodexo = "my-sodexo";
        public const string MySodexo360 = "sxo-360";
        public const string InspirationEntreprise = "inspirations-entreprise";
        public const string InspirationScolaire = "inspirations-scolaire";
        public const string SxoScolaire = "sxo-scolaire";
        public const string Score = "score";
        public const string CityLight = "citylights";
    }
}
