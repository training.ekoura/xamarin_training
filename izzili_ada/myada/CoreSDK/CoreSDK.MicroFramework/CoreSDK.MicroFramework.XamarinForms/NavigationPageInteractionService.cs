﻿using CoreSDK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Reflection;
using CoreSDK.Controls;

namespace CoreSDK
{
    public class NavigationPageInteractionService : CoreSDK.IInteractionService
    {
        #region ==================================== Properties ====================================
        // Usefull images for the alert box
        public const string IconInformation = "CoreSDK.Images.Info";
        public const string IconError = "CoreSDK.Images.Error";
        public const string IconSuccess = "CoreSDK.Images.Success";
        public const string IconSave = "CoreSDK.Images.Disquette";
        public const string IconExtensions = ".png";
        public const string Const_MasterDetailView = "MasterDetailView";

        private object _Locker = new object();

        private string _ThemeNameValue;


        private Page _RootPage;

        public NavigationPage _RootNavigationPage;

        private Stack<PageInfo> _PageInfoStack = new Stack<PageInfo>();
        private List<String> OpenedPagesList = new List<String>();

        private bool ChangeStatusBarColor = false;


        private double _DeviceWidth;
        public double DeviceWidth { get { return _DeviceWidth; } set { _DeviceWidth = value; } }

        private double _DeviceHeight;
        public double DeviceHeight { get { return _DeviceHeight; } set { _DeviceHeight = value; } }

        private string _DeviceIMEI;
        public string DeviceIMEI { 
            get { return _DeviceIMEI; } 
            set { 
                _DeviceIMEI = value; 
            }
        }

        private string _DeviceToken;
        public string DeviceToken
        {
            get { return _DeviceToken; }
            set
            {
                _DeviceToken = value;
            }
        }

        private string _DevicePlatform;
        public string DevicePlatform
        {
            get { return _DevicePlatform; }
            set
            {
                _DevicePlatform = value;
            }
        }

        private bool alreadyLoaded = false;
        private bool alreadyLoadedBurger = false;
        private bool alreadyLoadedPopIn = false;

        public string ThemeNameValue
        {
            set { _ThemeNameValue = value; }
            get { return _ThemeNameValue; }
        }
        #endregion

        #region ==================================== Set Main Page ====================================

        void _NavigationPage_Pushed(object sender, NavigationEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("NAVIGATION PUSHED RECEIVED");
            lock (_Locker)
            {
                if (_PageInfoStack.Count == 0 || _PageInfoStack.Peek().Page != e.Page)
                {
                    var l_PageInfo = new PageInfo(this, _PageInfoStack.Count == 0 ? null : _PageInfoStack.Peek().Page, e.Page, null, null);
                    _PageInfoStack.Push(l_PageInfo);
                    System.Diagnostics.Debug.WriteLine("NAVIGATION PUSHED AUTO ADD");
                }
            }
        }

        void _NavigationPage_Popped(object sender, NavigationEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("NAVIGATION POP RECEIVED");
            lock (_Locker)
            {
                if (_PageInfoStack.Count > 0 && _PageInfoStack.Peek().Page == e.Page)
                {
                    var l_PageInfo = _PageInfoStack.Pop();
                    l_PageInfo.SetIsCanceled();
                    System.Diagnostics.Debug.WriteLine("NAVIGATION PUSHED AUTO REMOVE");
                }
            }
        }

        void _Set(Page p_Page, object p_Context)
        {
            System.Diagnostics.Debug.WriteLine("NAVIGATION SET: {0}", p_Page);
            //foreach (var l_PageInfo in _PageInfoStack) // bug sur disconnect : quand on passe une commande et on fait disconnect
            //{
            //    l_PageInfo.SetIsCanceled();
            //}
            _PageInfoStack.Clear();
            OpenedPagesList.Clear();
            if (_RootNavigationPage != null)
            {
                _RootPage.BindingContext = null;
                _RootNavigationPage.Pushed -= _NavigationPage_Pushed;
                _RootNavigationPage.Popped -= _NavigationPage_Popped;
            }

            _RootPage = p_Page;

#if DEBUG
            DevelopperToolInsert(_RootPage);
#endif

            if (p_Page is NavigationPage)
            {
                _RootNavigationPage = p_Page as NavigationPage;
            }
            else
            {
                _RootNavigationPage = new NavigationPage(p_Page);
            }

            if (_RootNavigationPage != null)
            {
                if (p_Context != null)
                {
                    _RootPage.BindingContext = p_Context;
                }
                _RootNavigationPage.Pushed += _NavigationPage_Pushed;
                _RootNavigationPage.Popped += _NavigationPage_Popped;
            }

            // Only for IOS
            if (Device.OS == TargetPlatform.iOS)
            {
                if (ChangeStatusBarColor)
                {
                    _RootNavigationPage.BarTextColor = Color.Black;
                    _RootNavigationPage.BarBackgroundColor = Color.White;
                    ChangeStatusBarColor = false;
                }
                else
                {
                    _RootNavigationPage.BarTextColor = Color.White;
                    _RootNavigationPage.BarBackgroundColor = Color.Black;
                }
                //ChangeStatusBarColor("");
            }

            Application.Current.MainPage = _RootNavigationPage;

        }

        public void Set<TInteraction>(object p_Context = null, bool p_ChangeStatusBarColor = false)
            where TInteraction : class
        {
            var l_Page = CoreSDK.Framework.Services.Container().Resolve<TInteraction>() as Page;
            if (l_Page == null) throw new Exception(string.Format(Properties.Resources.ERR_INTERACTION_NOTFOUND, typeof(TInteraction).FullName));

            // On change la couleur du statusBar IOS
            if (p_ChangeStatusBarColor)
            {
                ChangeStatusBarColor = true;
            }

            _Set(l_Page, p_Context);
        }

        public void Set(string p_InteractionName, object p_Context = null)
        {
            var l_Page = CoreSDK.Framework.Services.Container().Resolve(p_InteractionName) as Page;
            if (l_Page == null) throw new Exception(string.Format(Properties.Resources.ERR_INTERACTION_NOTFOUND, p_InteractionName));
            _Set(l_Page, p_Context);
        }

        public void Set(string p_InteractionName, object p_Context = null, bool p_ChangeStatusBarColor = false, bool p_AllPlatForm = true)
        {
            var l_Page = CoreSDK.Framework.Services.Container().Resolve(p_InteractionName) as Page;
            if (l_Page == null) throw new Exception(string.Format(Properties.Resources.ERR_INTERACTION_NOTFOUND, p_InteractionName));

            if (p_ChangeStatusBarColor)
            {
                ChangeStatusBarColor = true;
            }

            if (p_AllPlatForm)
                _Set(l_Page, p_Context);

            // Exclusivement que pour pouvoir agir sur la couleur du status bar dans le cas IOS
            else
            {
                if (Device.OS == TargetPlatform.iOS)
                    _Set(l_Page, p_Context);
            }

        }

        public void Set(object p_Page, object p_Context = null)
        {
            if (!(p_Page is Page)) throw new Exception("Must be a page.");
            _Set(p_Page as Page, p_Context);
        }
        #endregion

        #region ==================================== Functions for Open pages ====================================
        void _PushPage(Page p_Page, object p_Context, Action<PageInfo> p_ReturnAction)
        {
            //            var l_PageInfo = new PageInfo(this, _PageInfoStack.Count == 0 ? null : _PageInfoStack.Peek().Page, p_Page, p_Context, p_ReturnAction);
            //            _PageInfoStack.Push(l_PageInfo);

            //            if (p_Context != null)
            //            {
            //                p_Page.BindingContext = p_Context;
            //            }

            //#if DEBUG
            //            DevelopperToolInsert(p_Page);
            //#endif
            //            l_PageInfo.Start();
            //            _RootNavigationPage.Navigation.PushAsync(p_Page);


            #region newOpen
            int ViewCount = 0;
            foreach (var view in _RootNavigationPage.Navigation.NavigationStack.Where(x => x.GetType() == p_Page.GetType()))
            {
                ViewCount += 1;
            }

            //if View does not exist in stack Already, we do the old traitement
            if (ViewCount == 0)
            {
                var l_PageInfo = new PageInfo(this, _PageInfoStack.Count == 0 ? null : _PageInfoStack.Peek().Page, p_Page, p_Context, p_ReturnAction);
                _PageInfoStack.Push(l_PageInfo);

                if (p_Context != null)
                {
                    p_Page.BindingContext = p_Context;
                }

#if DEBUG
                DevelopperToolInsert(p_Page);
#endif
                l_PageInfo.Start();
                _RootNavigationPage.Navigation.PushAsync(p_Page,false);
                OpenedPagesList.Add(p_Page.GetType().Name);
            }
            #endregion
        }

        public Task<IInteractionResult<TResult>> Open<TInteraction, TResult>(object p_Context = null)
        {
            System.Diagnostics.Debug.WriteLine("NAVIGATION OPEN: {0}", typeof(TInteraction));
            lock (_Locker)
            {
                var l_Page = CoreSDK.Framework.Services.Container().Resolve(typeof(TInteraction)) as Page;
                if (l_Page == null) throw new Exception(string.Format(Properties.Resources.ERR_INTERACTION_NOTFOUND, typeof(TInteraction).FullName));

                var tcs = new TaskCompletionSource<IInteractionResult<TResult>>();

                _PushPage(l_Page, p_Context, pi =>
                {
                    tcs.SetResult(new InteractionResult<TResult>((TResult)pi.Result, pi.IsCanceled));
                });

                return tcs.Task;
            }
        }

        public Task<IInteractionResult<TResult>> Open<TResult>(string p_InteractionName, object p_Context = null)
        {
            System.Diagnostics.Debug.WriteLine("NAVIGATION OPEN: {0}", p_InteractionName);
            lock (_Locker)
            {
                var l_Page = CoreSDK.Framework.Services.Container().Resolve(p_InteractionName) as Page;
                if (l_Page == null) throw new Exception(string.Format(Properties.Resources.ERR_INTERACTION_NOTFOUND, p_InteractionName));

                var tcs = new TaskCompletionSource<IInteractionResult<TResult>>();

                _PushPage(l_Page, p_Context, pi =>
                {
                    tcs.SetResult(new InteractionResult<TResult>((TResult)pi.Result, pi.IsCanceled));
                });

                return tcs.Task;
            }
        }

        public Task<IInteractionResult> Open<TInteraction>(object p_Context = null)
        {
            System.Diagnostics.Debug.WriteLine("NAVIGATION OPEN: {0}", typeof(TInteraction));
            lock (_Locker)
            {
                var l_Page = CoreSDK.Framework.Services.Container().Resolve(typeof(TInteraction)) as Page;
                if (l_Page == null) throw new Exception(string.Format(Properties.Resources.ERR_INTERACTION_NOTFOUND, typeof(TInteraction).FullName));

                var tcs = new TaskCompletionSource<IInteractionResult>();

                _PushPage(l_Page, p_Context, pi =>
                {
                    tcs.SetResult(new InteractionResult(pi.IsCanceled));
                });

                return tcs.Task;
            }
        }

        public Task<IInteractionResult> Open(string p_InteractionName, object p_Context = null)
        {
            System.Diagnostics.Debug.WriteLine("NAVIGATION OPEN: {0}", p_InteractionName);
            lock (_Locker)
            {
                var l_Page = CoreSDK.Framework.Services.Container().Resolve(p_InteractionName) as Page;
                if (l_Page == null) throw new Exception(string.Format(Properties.Resources.ERR_INTERACTION_NOTFOUND, p_InteractionName));

                var tcs = new TaskCompletionSource<IInteractionResult>();

                _PushPage(l_Page, p_Context, pi =>
                {
                    tcs.SetResult(new InteractionResult(pi.IsCanceled));
                });

                return tcs.Task;
            }
        }

        public Task<IInteractionResult> Open(Page p_Page, object p_Context = null)
        {
            System.Diagnostics.Debug.WriteLine("NAVIGATION OPEN: {0}", p_Page);
            lock (_Locker)
            {
                var tcs = new TaskCompletionSource<IInteractionResult>();

                _PushPage(p_Page, p_Context, pi =>
                {
                    tcs.SetResult(new InteractionResult(pi.IsCanceled));
                });

                return tcs.Task;
            }
        }

        #endregion

        #region ==================================== New function for navigation in Parent MasterDetailsPage ====================================
        public Task<IInteractionResult> OpenMasterDetailSection(string p_InteractionName, object p_Context = null)
        {
            System.Diagnostics.Debug.WriteLine("NAVIGATION OPEN Section Page: {0}", p_InteractionName);
            lock (_Locker)
            {
                var l_Page = CoreSDK.Framework.Services.Container().Resolve(p_InteractionName) as Page;
                var tcs = new TaskCompletionSource<IInteractionResult>();

                _PushPageMaster(l_Page, p_Context, pi =>
                {
                    tcs.SetResult(new InteractionResult(pi.IsCanceled));
                });

                return tcs.Task;
            }
        }

        void _PushPageMaster(Page p_Page, object p_Context, Action<PageInfo> p_ReturnAction)
        {

            if (p_Context != null)
                p_Page.BindingContext = p_Context;

            if (Device.Idiom != TargetIdiom.Tablet)
            {
                if (_RootNavigationPage.Navigation.NavigationStack.LastOrDefault().GetType().GetTypeInfo().BaseType == typeof(MasterDetailPage))
                {
                    if (((MasterDetailPage)_RootNavigationPage.Navigation.NavigationStack.LastOrDefault()).Detail.GetType().Name != p_Page.GetType().Name)
                    {
                        ((MasterDetailPage)_RootNavigationPage.Navigation.NavigationStack.LastOrDefault()).Detail = p_Page;
                        ((MasterDetailPage)_RootNavigationPage.Navigation.NavigationStack.LastOrDefault()).IsPresented = false;
                    }
                }
            }
            else
            {
                if (_RootNavigationPage.Navigation.NavigationStack.LastOrDefault().GetType().Name == Const_MasterDetailView)
                {
                    if (((MasterDetailPage)_RootNavigationPage.Navigation.NavigationStack.LastOrDefault()).Detail.GetType().Name != p_Page.GetType().Name)
                    {
                        ((MasterDetailPage)_RootNavigationPage.Navigation.NavigationStack.LastOrDefault()).Detail = p_Page;
                    }
                }
            }
        }

        #endregion

        #region ==================================== Functions for Close pages ====================================
        public void Close(int numberOfCloseToPerform = 0)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("NAVIGATION CUSTOM-CLOSE: redirectIndex: {0}", numberOfCloseToPerform);
                if (numberOfCloseToPerform > 0)
                {
                    for (int i = _RootNavigationPage.Navigation.NavigationStack.Count; i >= numberOfCloseToPerform; i--)
                    {
                        //_PageInfoStack.ToList<PageInfo>()[i - 1].SetResult(null);
                        _RootNavigationPage.Navigation.PopAsync(false);
                    }
                }
            }
            catch (Exception ex)
            {
                OldAlert("Problem Close", ex.Message);
            }
        }

        public void CloseAll()
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("NAVIGATION CUSTOM-CLOSE ALL :");

                for (int i = 1; i < _RootNavigationPage.Navigation.NavigationStack.Count; i++)
                {
                    _RootNavigationPage.Navigation.PopAsync(false);
                    OpenedPagesList.Remove(_RootNavigationPage.Navigation.NavigationStack.LastOrDefault().GetType().Name);
                }

                //_RootNavigationPage.Navigation.PopToRootAsync(false);
            }
            catch (Exception ex)
            {
                OldAlert("Problem Close", ex.Message);
            }
        }

        public async Task Close(bool p_Animation, int p_RedirectPageIndex = 0, string p_InteractionName = null, object p_Context = null)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("NAVIGATION CUSTOM-CLOSE: redirectIndex: {0} - ViewName: {1}", p_RedirectPageIndex, p_InteractionName);
                if (p_RedirectPageIndex != 0)
                {
                    var l_Page = CoreSDK.Framework.Services.Container().Resolve(p_InteractionName) as Page;
                    l_Page.BindingContext = p_Context;
                    await _RootNavigationPage.Navigation.PushAsync(l_Page);
                    OpenedPagesList.Add(l_Page.GetType().Name);
                    //remove all pages except the newly added l_Page and stops at home..
                    //should be modified to remove p_RedirectPageIndex number of pages starting from before the l_Page
                    //int count = 0;
                    //for (int j = 0; j == p_RedirectPageIndex; j++ )
                    //{

                    //}
                    for (int i = _RootNavigationPage.Navigation.NavigationStack.Count - 1; i > 1; i--)
                    {
                        _PageInfoStack.ToList<PageInfo>()[i - 1].SetResult(null);
                        OpenedPagesList.Remove(_RootNavigationPage.Navigation.NavigationStack[i - 1].GetType().Name);
                        _RootNavigationPage.Navigation.RemovePage(_RootNavigationPage.Navigation.NavigationStack[i - 1]);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                OldAlert("Problem Close", ex.Message);
            }
        }

        public void Close<TResult>(TResult p_Result, bool p_Animation = false)
        {
            System.Diagnostics.Debug.WriteLine("NAVIGATION CLOSE: Result = {0}", p_Result);
            lock (_Locker)
            {
                var l_PageInfo = _PageInfoStack.Pop();
                l_PageInfo.SetResult(p_Result);
                _RootNavigationPage.Navigation.PopAsync(p_Animation);
                //ici, i remove the lastPage from our List of Opened Pages to disable
                //closing a page that doesnt exist.
                OpenedPagesList.Remove(OpenedPagesList.LastOrDefault());
            }
        }

        public void Close()
        {
            System.Diagnostics.Debug.WriteLine("NAVIGATION CLOSE");
            lock (_Locker)
            {
                Close<object>(null);
            }
        }

        public void Close(string viewName = "", bool IsInterface = false)
        {
            //Checks the if the View exists in the List of opened Pages 
            //before attempting to remove it.
            if (IsInterface)
            {
                var l_Page = CoreSDK.Framework.Services.Container().Resolve(viewName) as Page;
                if (l_Page != null)
                    if (OpenedPagesList.Contains(l_Page.GetType().Name))
                    {
                        lock (_Locker)
                        {
                            Close<object>(null);
                        }
                    }

            }
            else
            {
                if (OpenedPagesList.Contains(viewName))
                {
                    lock (_Locker)
                    {
                        Close<object>(null);
                    }
                }

            }
        }

        public void Cancel()
        {
            System.Diagnostics.Debug.WriteLine("NAVIGATION CANCEL");
            lock (_Locker)
            {
                var l_PageInfo = _PageInfoStack.Pop();
                l_PageInfo.SetIsCanceled();
                _RootNavigationPage.Navigation.PopAsync();
            }
        }

        public Page Current
        {
            get
            {
                lock (_Locker)
                {
                    return _PageInfoStack.Count == 0 ? null : _PageInfoStack.Peek().Page;
                }
            }
        }
       
        #endregion

        #region ==================================== Show Alert messages ====================================
        public Task OldAlert(string p_Title, string p_Text)
        {
            lock (_Locker)
            {
                return _RootNavigationPage.DisplayAlert(p_Title, p_Text, Properties.Resources.INTERACTIONS_LABEL_OK);
            }
        }

        /// <summary>
        /// Initialy we used _RootNavigationPage.DisplayAlert(p_Title, p_Text, Properties.Resources.INTERACTIONS_LABEL_OK);
        /// Alert message. We changed this to a custom renderer view.
        /// </summary>
        /// <param name="p_Title"></param>
        /// <param name="p_Text"></param>
        /// <param name="_typeAlert"></param>
        /// <param name="_BackColor"></param>
        /// <returns></returns>
        public async Task Alert(string p_Title, string p_Text, double _AlerteTempo, AlertType _typeAlert, string _StackBackColor, string _LabelColor)
        {
            try
            {

                if (!alreadyLoaded)
                {


                    // To avoid concurrente access to this method
                    alreadyLoaded = true;

                    // This block code is for getting the grid of the current page 
                    var typePage = _RootNavigationPage.CurrentPage.GetType().GetTypeInfo().BaseType;
                    var defautGrid = new Grid();

                    // Traitement of pages with firt element equal to Page or ContentPage
                    if (typePage == typeof(Xamarin.Forms.Page) || typePage == typeof(Xamarin.Forms.ContentPage))
                    {

                        // Pages with first contentPage equal to ScrollView
                        if (((Xamarin.Forms.ContentPage)_RootNavigationPage.CurrentPage).Content.GetType() == typeof(Xamarin.Forms.ScrollView))
                            defautGrid = (Xamarin.Forms.Grid)((Xamarin.Forms.ScrollView)((Xamarin.Forms.ContentPage)_RootNavigationPage.CurrentPage).Content).Content;
                        else
                            // Pages with first contentPage equal to Grid
                            defautGrid = (Xamarin.Forms.Grid)((Xamarin.Forms.ContentPage)_RootNavigationPage.CurrentPage).Content;
                    }
                    // Traitement of pages with firt element equal to MaterDetailPage
                    else if (typePage == typeof(Xamarin.Forms.MasterDetailPage))
                        defautGrid = (Xamarin.Forms.Grid)((Xamarin.Forms.ContentPage)((Xamarin.Forms.MasterDetailPage)_RootNavigationPage.CurrentPage).Detail).Content;


                    // We create a new grid which would take the whole page. This should be the parent of our notification message.
                    #region ================================= This grid take the whole page =======================================
                    var _element = new Grid();
                    Grid.SetRowSpan(_element, Math.Max(1, defautGrid.RowDefinitions.Count));
                    Grid.SetColumnSpan(_element, Math.Max(1, defautGrid.ColumnDefinitions.Count));
                    _element.BackgroundColor = new Color(0, 0, 0, 0.2);
                    _element.HorizontalOptions = LayoutOptions.FillAndExpand;
                    _element.VerticalOptions = LayoutOptions.FillAndExpand;
                    #endregion

                    // Notification icône.
                    #region ======================================== Type message icone ================================================

                    var imagePath = "";
                    switch (_typeAlert)
                    {
                        case AlertType.Information:
                            imagePath = String.Format("{0}{1}{2}", IconInformation, ThemeNameValue, IconExtensions);
                            break;

                        case AlertType.Error:
                            imagePath = String.Format("{0}{1}{2}", IconError, ThemeNameValue, IconExtensions);
                            break;

                        case AlertType.Success:
                            imagePath = String.Format("{0}{1}{2}", IconSuccess, ThemeNameValue, IconExtensions);
                            break;

                        case AlertType.Save:
                            imagePath = String.Format("{0}{1}{2}", IconSave, ThemeNameValue, IconExtensions);
                            break;

                        default:
                            imagePath = String.Format("{0}{1}{2}", IconInformation, ThemeNameValue, IconExtensions);
                            break;
                    }

                    var embeddedImage = new Image { Aspect = Aspect.AspectFit, VerticalOptions = LayoutOptions.Center, HorizontalOptions = LayoutOptions.Center, WidthRequest = 35, HeightRequest = 35 };
                    embeddedImage.Source = ImageSource.FromResource(imagePath);

                    var imageStackLayout = new StackLayout
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        Padding = new Thickness(0, 10, 0, 10)
                    };

                    imageStackLayout.Children.Add(embeddedImage);

                    #endregion


                    // Notification message.
                    #region ====================================== Popup message ================================================
                    NormalFontLabel lblMessage = new NormalFontLabel();
                    lblMessage.Text = string.Format("{0}", p_Text);
                    lblMessage.TextColor = Color.FromHex(_LabelColor);
                    lblMessage.FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)); // As possible put this value by the method
                    lblMessage.HorizontalOptions = LayoutOptions.FillAndExpand;
                    lblMessage.HorizontalTextAlignment = TextAlignment.Center;
                    lblMessage.VerticalOptions = LayoutOptions.CenterAndExpand;
                    #endregion


                    // Receive the notification message.
                    #region ====================================== This grid contains should receive the notification message ====================================
                    var alertMessageGrid = new Grid()
                    {
                        RowSpacing = 10,
                        Padding = new Thickness(10, 0, 10, 15),
                        VerticalOptions = LayoutOptions.CenterAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        RowDefinitions = new RowDefinitionCollection { 
                                new RowDefinition {Height = GridLength.Auto},
                                new RowDefinition {Height = GridLength.Auto}
                            },
                        ColumnDefinitions = new ColumnDefinitionCollection { 
                                new ColumnDefinition { Width = new GridLength(1,GridUnitType.Star)}
                            },
                        BackgroundColor = Color.FromHex(_StackBackColor)
                    };

                    // First column
                    Grid.SetColumn(imageStackLayout, 0);
                    Grid.SetRow(imageStackLayout, 0);
                    alertMessageGrid.Children.Add(imageStackLayout);

                    // Second column
                    Grid.SetColumn(lblMessage, 0);
                    Grid.SetRow(lblMessage, 1);
                    alertMessageGrid.Children.Add(lblMessage);

                    var alertMessageStack = new StackLayout
                    {
                        VerticalOptions = LayoutOptions.CenterAndExpand,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        WidthRequest = 250,
                        BackgroundColor = Color.Transparent,
                        Children = { alertMessageGrid },
                    };

                    // Add the message in the grid.
                    _element.Children.Add(alertMessageStack);
                    defautGrid.Children.Add(_element);
                    #endregion


                    // Usefull for this animation.
                    #region =============================== Message Animation ========================================
                    //_element.TranslateTo(0, 50, 400, Easing.Linear);
                    #endregion


                    // Message Tempo
                    var l_TempoVal = (int)(_AlerteTempo * 1000);
                    await Task.Delay(l_TempoVal);

                    #region ===================================== Remove message  =============================================

                    /*var oldOpacity = 1.00;
                        for (int i = 0; i < 10; i++)
                        {
                            oldOpacity -= 0.1;

                            foreach (var child in _element.Children)
                            {
                                child.Opacity = oldOpacity;
                            }
                            await Task.Delay(50);
                        }
                        */
                    defautGrid.Children.Remove(_element);
                    #endregion

                    //alreadyLoaded = false;
                }
            }
            catch (Exception ex)
            {
                OldAlert(p_Title, p_Text);
            }
            finally
            {
                alreadyLoaded = false;
            }


        }

        public Task<bool> Confirm(string p_Title, string p_Text)
        {
            lock (_Locker)
            {
                return _RootNavigationPage.DisplayAlert(p_Title, p_Text, Properties.Resources.INTERACTIONS_LABEL_OK, Properties.Resources.INTERACTIONS_LABEL_CANCEL);
            }
        }

        #endregion

        #region ==================================== Second Burger ====================================
        public void ViewSecondBurger(IViewModel IVM)
        {
            if(IVM!=null)
            {
                try
                {
                    if (!alreadyLoadedBurger)
                    {
                        alreadyLoadedBurger = true;

                        // This block code is for getting the grid of the current page 
                        var typePage = _RootNavigationPage.CurrentPage.GetType().GetTypeInfo().BaseType;

                        var defautGrid = new Grid();

                        #region ==Test Type of Page and assign defaultGrid ==
                        if (typePage == typeof(Xamarin.Forms.Page) || typePage == typeof(Xamarin.Forms.ContentPage))
                        {

                            // Pages with first contentPage equal to ScrollView
                            if (((Xamarin.Forms.ContentPage)_RootNavigationPage.CurrentPage).Content.GetType() == typeof(Xamarin.Forms.ScrollView))
                                defautGrid = (Xamarin.Forms.Grid)((Xamarin.Forms.ScrollView)((Xamarin.Forms.ContentPage)_RootNavigationPage.CurrentPage).Content).Content;
                            else
                                // Pages with first contentPage equal to Grid
                                defautGrid = (Xamarin.Forms.Grid)((Xamarin.Forms.ContentPage)_RootNavigationPage.CurrentPage).Content;
                        }
                        // Traitement of pages with firt element equal to MaterDetailPage
                        else if (typePage == typeof(Xamarin.Forms.MasterDetailPage))
                            defautGrid = (Xamarin.Forms.Grid)((Xamarin.Forms.ContentPage)((Xamarin.Forms.MasterDetailPage)_RootNavigationPage.CurrentPage).Detail).Content;
                        #endregion

                        // We create a new grid which would take the whole defaultGrid. This should be the parent of our notification message.
                        #region alternateBurgerView
                        var alternateBurgerView = new Controls.AlternateBurger()
                        {
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            //IsVisible=false,
                            #region ==Template==
                            ItemTemplate = new DataTemplate(() =>
                            {
                                #region ==Create Overlay with Grid Setup===
                                var Overlay = new Grid()
                                {
                                    ColumnSpacing = 0,
                                    RowSpacing = 15,
                                    Padding = 0,
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                    VerticalOptions = LayoutOptions.FillAndExpand,
                                    RowDefinitions = 
                                        { 
                                            new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) },
                                            new RowDefinition() { Height = new GridLength(6.4, GridUnitType.Star) },
                                            new RowDefinition() { Height = GridLength.Auto },
                                            new RowDefinition() { Height = new GridLength(1.1, GridUnitType.Star) },
                                        },
                                    ColumnDefinitions = 
                                        { 
                                            new ColumnDefinition() { Width = new GridLength(3.5, GridUnitType.Star) },
                                            new ColumnDefinition() { Width = new GridLength(12.5, GridUnitType.Star) },
                                            new ColumnDefinition() { Width = new GridLength(4.5, GridUnitType.Star) },
                                        }
                                };
                                #endregion

                                #region transparentSidePadding
                                var transparentSidePadding = new StackLayout()
                                {
                                    BackgroundColor = Color.Gray,
                                    Opacity = 0.1,
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                    VerticalOptions = LayoutOptions.FillAndExpand,
                                };
                                var transparentSidePaddingTapGesture = new TapGestureRecognizer();
                                transparentSidePaddingTapGesture.Tapped += async (object sender, EventArgs e) =>
                                {
                                    //put event or callback to unshow this burger.
                                    var contentView = ((((sender as StackLayout).Parent as Grid).Parent as Controls.AlternateBurger).Parent as Controls.TouchContentView);
                                    await contentView.TranslateTo(480, 0, 100, Easing.Linear);
                                    defautGrid.Children.Remove(contentView);
                                };
                                transparentSidePadding.GestureRecognizers.Add(transparentSidePaddingTapGesture);
                                #endregion

                                Grid.SetRow(transparentSidePadding, 0);
                                Grid.SetRowSpan(transparentSidePadding, 4);
                                Grid.SetColumn(transparentSidePadding, 0);
                                Overlay.Children.Add(transparentSidePadding);

                                #region bg
                                var backgroundStack = new StackLayout()
                                {
                                    //BackgroundColor = Color.FromHex("#333333"),
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                    VerticalOptions = LayoutOptions.FillAndExpand,
                                };
                                backgroundStack.SetDynamicResource(StackLayout.StyleProperty, "HamburgerBodyStyle");
                                #endregion

                                Grid.SetRow(backgroundStack, 0);
                                Grid.SetRowSpan(backgroundStack, 4);
                                Grid.SetColumn(backgroundStack, 1);
                                Grid.SetColumnSpan(backgroundStack, 2);
                                Overlay.Children.Add(backgroundStack);

                                #region Header
                                var headerLabel = new Label()
                                {
                                    Text = Properties.Resources.SecondBurger_Header_Label,
                                    TextColor=Color.White
                                };
                                headerLabel.SetDynamicResource(Label.FontSizeProperty, "Style46");
                                headerLabel.SetDynamicResource(Label.StyleProperty, "HamburgerHeaderLabelStyle");

                                var HeaderStack = new StackLayout()
                                {
                                    Children = { headerLabel }
                                };
                                HeaderStack.SetDynamicResource(StackLayout.StyleProperty, "HamburgerHeaderStyle");
                                #endregion

                                Grid.SetRow(HeaderStack, 0);
                                Grid.SetColumn(HeaderStack, 1);
                                Grid.SetColumnSpan(HeaderStack, 2);
                                Overlay.Children.Add(HeaderStack);

                                #region Body
                                var optionItemList = new Controls.RepeaterView()
                                {
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                    VerticalOptions = LayoutOptions.FillAndExpand,
                                    //ItemSpacing=5,
                                    #region Template
                                    ItemTemplate = new DataTemplate(() =>
                                    {
                                        alreadyLoaded = true;
                                        var totalGrid = new Grid()
                                        {
                                            ColumnSpacing = 0,
                                            RowSpacing = 0,
                                            Padding = 0,
                                            HorizontalOptions = LayoutOptions.FillAndExpand,
                                            VerticalOptions = LayoutOptions.Fill,
                                            BackgroundColor = Color.Transparent,
                                            RowDefinitions = 
                                            { 
                                                new RowDefinition() { Height = Device.OnPlatform(50, 45, 55) },
                                                new RowDefinition() { Height = 1 }
                                            }
                                        };

                                        #region optionLabel
                                        var optionLabel = new Label()
                                        {
                                            HorizontalOptions = LayoutOptions.FillAndExpand,
                                            VerticalOptions = LayoutOptions.FillAndExpand,
                                            VerticalTextAlignment = TextAlignment.Center,
                                            HorizontalTextAlignment = TextAlignment.Start,
                                            //TextColor = Color.White,
                                            LineBreakMode = Xamarin.Forms.LineBreakMode.WordWrap,
                                        };
                                        optionLabel.SetBinding(Label.TextProperty, "Name");
                                        optionLabel.SetDynamicResource(Label.StyleProperty, "HamburgerOptionsLabelStyle");
                                        optionLabel.SetDynamicResource(Label.FontSizeProperty, "Style36");

                                        var optionLabelConatainerStack = new StackLayout()
                                        {
                                            HorizontalOptions = LayoutOptions.FillAndExpand,
                                            VerticalOptions = LayoutOptions.FillAndExpand,
                                            Children = { optionLabel },
                                        };
                                        #endregion

                                        Grid.SetRow(optionLabelConatainerStack, 0);
                                        totalGrid.Children.Add(optionLabelConatainerStack);

                                        #region optionLabelUnderbar
                                        var optionLabelUnderbar = new StackLayout()
                                        {
                                            VerticalOptions = LayoutOptions.FillAndExpand,
                                            HorizontalOptions = LayoutOptions.FillAndExpand,
                                            BackgroundColor = Color.FromHex("#545454")
                                        };
                                        #endregion

                                        Grid.SetRow(optionLabelUnderbar, 1);
                                        totalGrid.Children.Add(optionLabelUnderbar);

                                        //var CustomBurgerListViewCell = new ViewCell()
                                        //{
                                        //    View = totalGrid
                                        //};
                                        var totalGridTapGesture = new TapGestureRecognizer();
                                        totalGridTapGesture.SetBinding(TapGestureRecognizer.CommandParameterProperty, ".");
                                        totalGridTapGesture.SetBinding(TapGestureRecognizer.CommandProperty, new Binding(path: "CurrentItemSelectedCommand") { Source = IVM});
                                        totalGrid.GestureRecognizers.Add(totalGridTapGesture);


                                        //var totalGridTrigger = new DataTrigger(typeof(Grid));
                                        //totalGridTrigger.Binding = new Binding("SelectedStateColor");
                                        //totalGridTrigger.Value = "";
                                        //Setter setter = new Setter ();
                                        //setter.Property = Grid.BackgroundColorProperty;
                                        //setter.Value = Color.Transparent;
                                        //totalGridTrigger.Setters.Add(setter);

                                        //totalGrid.Triggers.Add(totalGridTrigger);

                                        //var totalGridContentView = new ContentView()
                                        //{
                                        //    HorizontalOptions = LayoutOptions.FillAndExpand,
                                        //    VerticalOptions = LayoutOptions.Fill,
                                        //    Content=totalGrid
                                        //};
                                        //totalGridContentView.SetBinding(ContentView.BackgroundColorProperty, "SelectedStateColor", BindingMode.Default, new ColorConvertor(), null);

                                        alreadyLoaded = false;

                                        return totalGrid;                                        
                                    })
                                    #endregion
                                };
                                //optionItemList.Behaviors.Add(new Behaviors.BurgerListViewItemClickBehaviour());
                                optionItemList.SetBinding(Controls.RepeaterView.ItemsSourceProperty, "ListItems");
                                //optionItemList.SetBinding(ListView.SelectedItemProperty, "CurrentItem", BindingMode.TwoWay);

                                //var invokeCommandAction = new CommandAction() { Command = new Binding(path: "CurrentItemSelectedCommand") };
                                //var eventTrigger = new EventTrigger() {Event = "ItemTapped" };
                                //eventTrigger.Actions.Add(invokeCommandAction);
                                //optionItemList.Triggers.Add(eventTrigger);

                                var BodyStack = new StackLayout()
                                {
                                    VerticalOptions=LayoutOptions.FillAndExpand,
                                    HorizontalOptions=LayoutOptions.FillAndExpand,
                                    Children = { optionItemList }
                                };
                                BodyStack.SetDynamicResource(StackLayout.StyleProperty, "HamburgerBodyStyle");
                                #endregion

                                Grid.SetRow(BodyStack, 1);
                                Grid.SetColumn(BodyStack, 1);
                                Grid.SetColumnSpan(BodyStack, 2);
                                Overlay.Children.Add(BodyStack);

                                #region Badge Details
                                var detailLabel = new Label()
                                {
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                    VerticalOptions = LayoutOptions.Center,
                                    VerticalTextAlignment = TextAlignment.End,
                                    HorizontalTextAlignment = TextAlignment.Start,
                                    Text = Properties.Resources.SecondBurger_Solde_Details_Label
                                };
                                detailLabel.SetDynamicResource(Label.FontSizeProperty, "Style34");
                                detailLabel.SetDynamicResource(Label.StyleProperty, "HamburgerSoldeBadgeLabelStyle");

                                var soldeAmountLabel = new Label()
                                {
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                    VerticalOptions = LayoutOptions.Center,
                                    VerticalTextAlignment = TextAlignment.Center,
                                    HorizontalTextAlignment = TextAlignment.Start,
                                    //TextColor = Color.FromHex("#FFFFFF"),
                                   // Text = "34,45€"
                                };
                                //soldeAmountLabel.SetDynamicResource(Label.FontSizeProperty, "Style86");
                                soldeAmountLabel.SetBinding(Label.TextProperty, "BadgeVM.BadgeValue");
                                soldeAmountLabel.SetDynamicResource(Label.StyleProperty, "HamburgerOptionsLabelStyle");
                                soldeAmountLabel.SetBinding(Label.FontSizeProperty, "BadgeVM.SoldeSizeForSecondBurger", BindingMode.Default, new StringFontSizeConverter());

                                var soldContaionerStack = new StackLayout()
                                {
                                    Spacing = 10,
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                    VerticalOptions = LayoutOptions.FillAndExpand,
                                    Children = { detailLabel, soldeAmountLabel }
                                };

                                var detailsStack = new StackLayout()
                                {
                                    Padding = new Thickness(Device.OnPlatform(30, 25, 30), 0, 0, 0),
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                    VerticalOptions = LayoutOptions.Fill,
                                    Children = { soldContaionerStack }
                                };
                                detailsStack.SetBinding(StackLayout.IsVisibleProperty, "BadgeVM.IsBadgeAffiche");
                                #endregion

                                Grid.SetRow(detailsStack, 2);
                                Grid.SetColumn(detailsStack, 1);
                                Grid.SetColumnSpan(detailsStack, 2);
                                Overlay.Children.Add(detailsStack);

                                #region recharge button
                                var myButton = new Controls.CustomButton()
                                {
                                    //ViewSender=Controls.CustomButton.ViewSenderTypes.SecondBurger,
                                    ButtonType = Controls.CustomButton.ButtonTypes.Burger,
                                    VerticalOptions=LayoutOptions.FillAndExpand,
                                    HorizontalOptions=LayoutOptions.FillAndExpand,
                                };
                                myButton.SetBinding(Controls.CustomButton.ButtonLabelProperty, "BadgeBtnText");
                                myButton.SetBinding(Controls.CustomButton.TapCommandProperty, "RechargeBadgeCommand");

                                var rechargeContainerStack = new StackLayout()
                                {
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                    VerticalOptions = LayoutOptions.FillAndExpand,
                                    Children = { myButton }
                                };

                                var rechargeButtonStack = new StackLayout()
                                {
                                    Padding = new Thickness(Device.OnPlatform(30,25,30), 0, 0, 15),
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                    VerticalOptions = LayoutOptions.FillAndExpand,
                                    Children = { rechargeContainerStack }
                                };
                                rechargeButtonStack.SetBinding(StackLayout.IsVisibleProperty, "BadgeVM.IsBadgeAffiche");
                                #endregion

                                Grid.SetRow(rechargeButtonStack, 3);
                                Grid.SetColumn(rechargeButtonStack, 1);
                                Overlay.Children.Add(rechargeButtonStack);

                                Overlay.Behaviors.Add(new Behaviors.ActivityIndicatorBehavior() { Label = Properties.Resources.Label_ISWorking});
                                return Overlay;
                            })
                            #endregion
                        };
                        #endregion
                        //assign binding context from received vM
                        alternateBurgerView.BindingContext = IVM;

                        var touchContentViewContainer = new Controls.TouchContentView()
                        {
                            StyleId = "SecondBurgertouchContentViewContainer",
                            VerticalOptions=LayoutOptions.FillAndExpand,
                            HorizontalOptions=LayoutOptions.FillAndExpand,
                            Content = alternateBurgerView
                        };
                        touchContentViewContainer.Behaviors.Add(new Behaviors.BurgerSwipeBehaviour());
                        

                        Grid.SetRowSpan(touchContentViewContainer, Math.Max(1, defautGrid.RowDefinitions.Count));
                        Grid.SetColumnSpan(touchContentViewContainer, Math.Max(1, defautGrid.ColumnDefinitions.Count));
                        //defautGrid.Children.Add(touchContentViewContainer);

                        try
                        {
                            foreach (var view in defautGrid.Children)
                            {
                                if (view.GetType() == typeof(Controls.TouchContentView))
                                {
                                    defautGrid.Children.Remove(view);
                                }
                            }
                        }
                        catch (Exception exMsg)
                        {
                            //
                        }
                        finally
                        {
                            defautGrid.Children.Add(touchContentViewContainer);
                        }
                    }
                }
                catch (Exception ex)
                {
                    OldAlert(" ", ex.Message);
                }
                finally
                {
                    alreadyLoadedBurger = false;
                }
            }
        }

        public  void RemoveSecondBurger(object currentPage)
        {
            try
            {
                var myCurrentPage = currentPage as ContentPage;
                var thisGrid = myCurrentPage.Content as Grid;
                var gridChildren = thisGrid.Children;

                foreach (var item in gridChildren)
                {
                    if (item.GetType() == typeof(CoreSDK.Controls.TouchContentView))
                    {
                        thisGrid.Children.Remove(item);
                    }
                }
            }
            catch (Exception ex)
            {
                //
            }
        }
        #endregion

        #region ==================================== View Popin ====================================
        
        public async Task ViewPopIn(IViewModel IVM, string PopInViewName, bool IsBurger)
        {
            if (IVM != null)
            {
                await Task.Factory.StartNew( () =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        try
                        {
                            if (!alreadyLoadedPopIn)
                            {
                                alreadyLoadedPopIn = true;

                                // This block code is for getting the grid of the current page 
                                var typePage = _RootNavigationPage.CurrentPage.GetType().GetTypeInfo().BaseType;

                                var defautGrid = new Grid();

                                #region ==Test Type of Page and assign defaultGrid ==
                                if (typePage == typeof(Xamarin.Forms.Page) || typePage == typeof(Xamarin.Forms.ContentPage))
                                {

                                    // Pages with first contentPage equal to ScrollView
                                    if (((Xamarin.Forms.ContentPage)_RootNavigationPage.CurrentPage).Content.GetType() == typeof(Xamarin.Forms.ScrollView))
                                        defautGrid = (Xamarin.Forms.Grid)((Xamarin.Forms.ScrollView)((Xamarin.Forms.ContentPage)_RootNavigationPage.CurrentPage).Content).Content;
                                    else
                                        // Pages with first contentPage equal to Grid
                                        defautGrid = (Xamarin.Forms.Grid)((Xamarin.Forms.ContentPage)_RootNavigationPage.CurrentPage).Content;
                                }
                                // Traitement of pages with firt element equal to MaterDetailPage
                                else if (typePage == typeof(Xamarin.Forms.MasterDetailPage))
                                {
                                    if (IsBurger)
                                    {
                                        defautGrid = (Xamarin.Forms.Grid)((Xamarin.Forms.ContentPage)((Xamarin.Forms.MasterDetailPage)_RootNavigationPage.CurrentPage).Master).Content;
                                    }
                                    else
                                    {
                                        defautGrid = (Xamarin.Forms.Grid)((Xamarin.Forms.ContentPage)((Xamarin.Forms.MasterDetailPage)_RootNavigationPage.CurrentPage).Detail).Content;
                                    }
                                }
                                #endregion

                                var indicatorView = new StackLayout()
                                {
                                    VerticalOptions = LayoutOptions.FillAndExpand,
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                    StyleId = "IndicatorView",
                                    BackgroundColor = new Color(0, 0, 0, 0.7),
                                };
                                Grid.SetRowSpan(indicatorView, Math.Max(1, defautGrid.RowDefinitions.Count));
                                Grid.SetColumnSpan(indicatorView, Math.Max(1, defautGrid.ColumnDefinitions.Count));
                                defautGrid.Children.Add(indicatorView);

                                //added this Grid to allow popIn View react to BeginWork && EndWork.
                                var behavedGrid = new Grid()
                                {
                                    VerticalOptions = LayoutOptions.FillAndExpand,
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                    Padding=0,
                                    RowDefinitions =
                                    {
                                         new RowDefinition() { Height = new GridLength(1, GridUnitType.Star)  },
                                    }
                                };
                                behavedGrid.Behaviors.Add(new Behaviors.ActivityIndicatorBehavior());

                                var ActualContentView = new ContentViews.PopInView(IsBurger, PopInViewName);
                                Grid.SetRow(ActualContentView, 0);
                                behavedGrid.Children.Add(ActualContentView);


                                var touchContentViewContainer = new ContentView()
                                {
                                    VerticalOptions = LayoutOptions.FillAndExpand,
                                    HorizontalOptions = LayoutOptions.FillAndExpand,
                                    StyleId = "PopInContainer",
                                    //IsBurger is paased to treat case of burger on Tablet.
                                    Content = behavedGrid
                                };
                                touchContentViewContainer.SetBinding(ContentView.IsVisibleProperty, "IsPopInVisibile");
                                touchContentViewContainer.Behaviors.Add(new Behaviors.PopInCloseBehaviour());
                                touchContentViewContainer.BindingContext = IVM;

                                Grid.SetRowSpan(touchContentViewContainer, Math.Max(1, defautGrid.RowDefinitions.Count));
                                Grid.SetColumnSpan(touchContentViewContainer, Math.Max(1, defautGrid.ColumnDefinitions.Count));

                                try
                                {
                                    int count = 0;
                                    foreach (var view in defautGrid.Children.Where(x => (x.GetType() == typeof(ContentView)) && (x.StyleId != "HeaderContentView")))
                                    {
                                        count++;
                                        //defautGrid.Children.Remove(view);
                                    }

                                    if (count == 0)
                                    {
                                        defautGrid.Children.Add(touchContentViewContainer);
                                    }
                                    defautGrid.Children.Remove(indicatorView);
                                }
                                catch (Exception exMsg)
                                {
                                    //
                                }
                                //finally
                                //{
                                //    defautGrid.Children.Add(touchContentViewContainer);
                                //    defautGrid.Children.Remove(indicatorView);
                                //}
                            }
                        }
                        catch (Exception ex)
                        {
                            OldAlert(" ", ex.Message);
                        }
                        finally
                        {
                            alreadyLoadedPopIn = false;
                        }
                    });
                });
            }
        }
       
        #endregion

        #region ==================================== Development tools ====================================

#if DEBUG

        private bool _UseDevelopperTool = true;
        public bool UseDevelopperTool
        {
            get { return _UseDevelopperTool; }
            set { _UseDevelopperTool = value; }
        }

        private void DevelopperToolInsert(Page p_Page)
        {
            if (!_UseDevelopperTool) return;
            try
            {
                foreach (var l_Method in (from m in p_Page.GetType().GetTypeInfo().DeclaredMethods where m.GetCustomAttribute(typeof(DevelopperToolAttribute), true) != null select m))
                {
                    p_Page.ToolbarItems.Add(new ToolbarItem("V:" + l_Method.Name, "", () =>
                    {
                        try
                        {
                            l_Method.Invoke(p_Page, null);
                        }
                        catch
                        {

                        }
                    }, ToolbarItemOrder.Secondary, -10000));
                }

                if (p_Page.BindingContext != null)
                {
                    foreach (var l_Method in (from m in p_Page.BindingContext.GetType().GetTypeInfo().DeclaredMethods where m.GetCustomAttribute(typeof(DevelopperToolAttribute), true) != null select m))
                    {
                        p_Page.ToolbarItems.Add(new ToolbarItem("VM:" + l_Method.Name, "", () =>
                        {
                            try
                            {
                                l_Method.Invoke(p_Page.BindingContext, null);
                            }
                            catch
                            {

                            }
                        }, ToolbarItemOrder.Secondary, -10000));
                    }
                }
            }
            catch
            {

            }
        }
#endif

        #endregion

        #region ==================================== TInteraction ====================================
        
        public TInteraction Create<TInteraction>()
            where TInteraction : class, IInteractionBase
        {
            var l_Interaction = CoreSDK.Framework.Services.Container().Resolve<TInteraction>();
            if (l_Interaction == null) throw new Exception("Impossible de trouver l'intéraction.");
            return l_Interaction;
        }
        
        #endregion

        #region ==================================== Page infos bloc ====================================
        
        private class PageInfo
        {
            public PageInfo(NavigationPageInteractionService p_PageNavigationService, Page p_ParentPage, Page p_Page, object p_Context, Action<PageInfo> p_CallbackAction)
            {
                PageNavigationService = p_PageNavigationService;
                ParentPage = p_ParentPage;
                Page = p_Page;
                Context = p_Context;
                IsCanceled = true;
                CallbackAction = p_CallbackAction;
            }

            public NavigationPageInteractionService PageNavigationService { get; private set; }

            public Page ParentPage { get; private set; }

            public Page Page { get; private set; }

            public object Context { get; private set; }

            public object Result { get; set; }

            public bool IsCanceled { get; set; }

            private Action<PageInfo> CallbackAction { get; set; }

            public void SetResult(object p_Result)
            {
                Result = p_Result;
                IsCanceled = false;
                Finish();
            }

            public void SetIsCanceled()
            {
                IsCanceled = true;
                Finish();
            }

            public void Start()
            {

            }

            public void Finish()
            {
                if (CallbackAction == null) return;
                CallbackAction(this);
            }
        }

        #endregion

    }
}
