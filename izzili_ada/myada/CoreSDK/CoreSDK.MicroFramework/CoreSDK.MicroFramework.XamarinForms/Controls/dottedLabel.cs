﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK.Controls
{

    #region ============================= DottedLabel =============================
        public class dottedLabel: Label
        {
            public dottedLabel ()
            {
                FontAttributes = Xamarin.Forms.FontAttributes.Bold;
                Text =  "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D " +
                    "\u002D \u002D \u002D \u002D ";
               //"\u2014 \u002D \u002D \u002D \u002D \u002D \u002D \u002D  " +
               //"\u2014 \u002D \u002D \u002D \u002D \u002D \u002D \u002D  " +
               //"\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D  " +
               //"\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D  " +
               //"\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D  " +
               //"\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D  " +
               //"\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D  " +
               //"\u002D \u002D \u002D \u002D \u002D \u002D \u002D \u002D";
                HorizontalOptions = LayoutOptions.FillAndExpand;
                VerticalOptions = LayoutOptions.Fill;
                VerticalTextAlignment = TextAlignment.Center;
                HorizontalTextAlignment = TextAlignment.Center;
                FontAttributes = Xamarin.Forms.FontAttributes.Bold;
                LineBreakMode = LineBreakMode.NoWrap;
                FontSize = Device.OnPlatform(6, 6, 8);
                SetDynamicResource(Label.StyleProperty, "ThemedTextStyle");
            }

        }

    #endregion

    #region ============================= Dotted Image =============================
        public class DottedImage : Image
        {
            public string dottedImagePath = "Sodexo.Images.dottedLineImage.png";
            public DottedImage()
            {
                VerticalOptions = LayoutOptions.Center;
                HorizontalOptions = LayoutOptions.Center;
                Aspect = Aspect.Fill;
                Source = ImageSource.FromResource(dottedImagePath);
            }
        }

    #endregion

    }
