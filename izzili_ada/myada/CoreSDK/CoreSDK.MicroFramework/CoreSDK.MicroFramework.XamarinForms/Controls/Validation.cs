﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK.Controls
{
    [Obsolete]
    public class Validation : Xamarin.Forms.ContentView
    {
        public Validation()
        {
            this.Padding = 1;
        }

        private object _OldBindingContext;

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            if (_OldBindingContext is INotifyDataErrorInfo)
            {
                (_OldBindingContext as INotifyDataErrorInfo).ErrorsChanged -= ValidationControl_ErrorsChanged;
            }
            _OldBindingContext = BindingContext;
            if(BindingContext is INotifyDataErrorInfo)
            {
                (BindingContext as INotifyDataErrorInfo).ErrorsChanged += ValidationControl_ErrorsChanged;                
            }
        }

        void ValidationControl_ErrorsChanged(object sender, DataErrorsChangedEventArgs e)
        {
            try
            {
                var ndei = (BindingContext as INotifyDataErrorInfo);
                if (ndei != null)
                {
                    var errors = ndei.GetErrors(PropertyName).Cast<object>().ToList();
                    if (errors.Count > 0)
                    {
                        this.BackgroundColor = Color.Red;
                    }
                    else
                    {
                        this.BackgroundColor = Color.Transparent;
                    }
                }
            }
            catch
            {

            }
        }

        public static readonly BindableProperty PropertyNameProperty = BindableProperty.Create<Validation, string>(p => p.PropertyName, null, propertyChanged: OnPropertyNameChanged);

        private static void OnPropertyNameChanged(BindableObject bindable, string oldvalue, string newvalue)
        {

        }

        public string PropertyName
        {
            get { return (string)GetValue(PropertyNameProperty); }
            set { SetValue(PropertyNameProperty, value); }
        }


    }
}
