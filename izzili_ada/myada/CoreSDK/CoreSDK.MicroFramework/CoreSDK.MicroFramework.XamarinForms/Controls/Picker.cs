﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace CoreSDK.Controls
{
    public class Picker : ContentView
    {
        public Picker()
        {
            _BuildTemplate();
            Items.CollectionChanged += Items_CollectionChanged;
        }

        #region === Items ===

        public static readonly BindableProperty ItemsProperty = BindableProperty.Create<Picker, ObservableCollection<object>>(p => p.Items, default(ObservableCollection<object>), propertyChanged: (d, o, n) => (d as Picker).Items_Changed(o, n));

        private void Items_Changed(ObservableCollection<object> oldvalue, ObservableCollection<object> newvalue)
        {

        }

        public ObservableCollection<object> Items
        {
            get 
            {
                if (GetValue(ItemsProperty) == null) SetValue(ItemsProperty, new ObservableCollection<object>()); 
                return (ObservableCollection<object>)GetValue(ItemsProperty); 
            }
        }

        private int _UseItems = 0;
        void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (_UseItems != 0) return;
            this.SetBinding(Picker.ItemsSourceProperty, new Binding(Picker.ItemsProperty.PropertyName, BindingMode.OneWay, null, null, null, this));
            _UseItems = 1;
        }

        #endregion

        #region === ItemsSource ===

        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create<Picker, object>(p => p.ItemsSource, default(object), propertyChanged: (d, o, n) => (d as Picker).ItemsSource_Changed(o, n));

        private void ItemsSource_Changed(object oldvalue, object newvalue)
        {
            if (_UseItems == 1)
            {
                _UseItems = 2;
                Items.Clear();
                _UseItems = 0;
            }
        }

        public object ItemsSource
        {
            get { return (object)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        #endregion

        #region === SelectedIndex ===

        public static readonly BindableProperty SelectedIndexProperty = BindableProperty.Create<Picker, int>(p => p.SelectedIndex, -1, propertyChanged: (d, o, n) => (d as Picker).SelectedIndex_Changed(o, n), defaultBindingMode: BindingMode.TwoWay);

        private void SelectedIndex_Changed(int oldvalue, int newvalue)
        {
            try
            {
                if (ItemsSource is IEnumerable && newvalue >= 0)
                {
                    var l_List = (ItemsSource as IEnumerable).Cast<object>().ToList();
                    if (newvalue < l_List.Count)
                    {
                        SelectedItem = l_List[newvalue];
                        return;
                    }
                }
                SelectedIndex = -1;
                SelectedItem = null;
            }
            finally
            {
                _NotifySelectedIndexChanged();
            }
        }

        public int SelectedIndex
        {
            get { return (int)GetValue(SelectedIndexProperty); }
            set { SetValue(SelectedIndexProperty, value); }
        }

        #endregion
        
        #region === SelectedItem ===

        public static readonly BindableProperty SelectedItemProperty = BindableProperty.Create<Picker, object>(p => p.SelectedItem, default(object), propertyChanged: (d, o, n) => (d as Picker).SelectedItem_Changed(o, n), defaultBindingMode: BindingMode.TwoWay);

        private void SelectedItem_Changed(object oldvalue, object newvalue)
        {
            if (ItemsSource is IEnumerable && newvalue != null)
            {
                SelectedIndex = (ItemsSource as IEnumerable).Cast<object>().ToList().IndexOf(newvalue);
                return;
            }
            SelectedIndex = -1;
            SelectedItem = null;
        }

        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        #endregion

        #region === ItemTemplate ===

        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create<Picker, DataTemplate>(p => p.ItemTemplate, default(DataTemplate), propertyChanged: (d, o, n) => (d as Picker).ItemTemplate_Changed(o, n));

        private void ItemTemplate_Changed(DataTemplate oldvalue, DataTemplate newvalue)
        {

        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        #endregion

        #region === Title ===

        public static readonly BindableProperty TitleProperty = BindableProperty.Create<Picker, string>(p => p.Title, default(string), propertyChanged: (d, o, n) => (d as Picker).Title_Changed(o, n));

        private void Title_Changed(string oldvalue, string newvalue)
        {

        }

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        #endregion
        
        #region === Placeholder ===

        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create<Picker, string>(p => p.Placeholder, default(string), propertyChanged: (d, o, n) => (d as Picker).Placeholder_Changed(o, n));

        private void Placeholder_Changed(string oldvalue, string newvalue)
        {

        }

        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        #endregion

        #region === DisplayMemberName ===

        public static readonly BindableProperty DisplayMemberNameProperty = BindableProperty.Create<Picker, string>(p => p.DisplayMemberName, default(string), propertyChanged: (d, o, n) => (d as Picker).DisplayMemberName_Changed(o, n));

        private void DisplayMemberName_Changed(string oldvalue, string newvalue)
        {
            _UpdateTextBinding();
        }

        public string DisplayMemberName
        {
            get { return (string)GetValue(DisplayMemberNameProperty); }
            set { SetValue(DisplayMemberNameProperty, value); }
        }

        #endregion

        #region === TextColor ===

        public static readonly BindableProperty TextColorProperty = BindableProperty.Create<Picker, Color>(p => p.TextColor, Color.Default, propertyChanged: (d, o, n) => (d as Picker).TextColor_Changed(o, n));

        private void TextColor_Changed(Color oldvalue, Color newvalue)
        {

        }

        [TypeConverter(typeof(ColorTypeConverter))]
        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        #endregion

        #region === TextBackgroundColor ===

        public static readonly BindableProperty TextBackgroundColorProperty = BindableProperty.Create<Picker, Color>(p => p.TextBackgroundColor, default(Color), propertyChanged: (d, o, n) => (d as Picker).TextBackgroundColor_Changed(o, n));

        private void TextBackgroundColor_Changed(Color oldvalue, Color newvalue)
        {

        }

        [TypeConverter(typeof(ColorTypeConverter))]
        public Color TextBackgroundColor
        {
            get { return (Color)GetValue(TextBackgroundColorProperty); }
            set { SetValue(TextBackgroundColorProperty, value); }
        }

        #endregion

        private new View Content { get { return base.Content; } set { base.Content = value; } }

        private Grid _RootGrid;
        private Entry _Entry;
        private Grid _Overlay;
        private Image _DropDownImage;
        private ContentPage _Page;
        private ListView _ListView;

        /// <summary>
        /// Contruit le picker (sur la base d'un modèle de contenu).
        /// </summary>
        private void _BuildTemplate()
        {
            // Root Grid
            _RootGrid = new Grid()
            {
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.Fill,
            };
            _RootGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            _RootGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            {
                //Entry
                _Entry = new Entry()
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    TextColor = Color.Black
                };
                _SetTemplateBinding(_Entry, Entry.PlaceholderProperty, Picker.PlaceholderProperty.PropertyName);
                _SetTemplateBinding(_Entry, Entry.IsEnabledProperty, Picker.IsEnabledProperty.PropertyName);
                _SetTemplateBinding(_Entry, Entry.TextColorProperty, Picker.TextColorProperty.PropertyName);
                _SetTemplateBinding(_Entry, Entry.BackgroundColorProperty, Picker.TextBackgroundColorProperty.PropertyName);
                Grid.SetColumn(_Entry, 0);
                Grid.SetRow(_Entry, 0);
                _Entry.Focused += (s, e) =>
                {
                    // Au focus, on ouvre le picker (au cas ou, mais normalement ne doit pas se produire).
                    _Open();
                };
                _RootGrid.Children.Add(_Entry);

                // Image du DropDown
                _DropDownImage = new Image()
                {
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    Source = ImageSource.FromResource("CoreSDK.Images.dropdown.png"),
                    TranslationX = -20
                };
                Grid.SetColumn(_DropDownImage, 0);
                Grid.SetRow(_DropDownImage, 0);
                _RootGrid.Children.Add(_DropDownImage);

                // Un overlay est utiliser pour normalement empêcher le focus et l'apparition du keyboard.
                _Overlay = new Grid()
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    BackgroundColor = Color.Transparent
                };
                Grid.SetColumn(_Overlay, 0);
                Grid.SetRow(_Overlay, 0);
                _Overlay.GestureRecognizers.Add(new TapGestureRecognizer((v) =>
                {
                    // Lorsque l'on tap l'overlay, on ouvre le picker.
                    _Open();
                }));
                _RootGrid.Children.Add(_Overlay);
            }

            Content = _RootGrid;

            _UpdateTextBinding();
        }
        
        /// <summary>
        /// Cette méthode est utilisée pour binder une propriété d'un objet du modèle à une propriété du picker.
        /// </summary>
        /// <param name="p_Object"></param>
        /// <param name="p_Property"></param>
        /// <param name="p_Path"></param>
        private void _SetTemplateBinding(BindableObject p_Object, BindableProperty p_Property, string p_Path)
        {
            p_Object.SetBinding(p_Property, new Binding(p_Path, BindingMode.OneWay, null, null, null, this));
        }

        /// <summary>
        /// Cette méthode met à jour le binding de la propriété text de l'Entry. Ce binding peut changer en fonction de DisplayName.
        /// </summary>
        private void _UpdateTextBinding()
        {
            if (_Entry == null) return;
            var l_Path = string.IsNullOrWhiteSpace(DisplayMemberName) ? SelectedItemProperty.PropertyName : SelectedItemProperty.PropertyName + "." + DisplayMemberName;
            _Entry.SetBinding(Entry.TextProperty, new Binding(l_Path, BindingMode.OneWay, null, null, null, this));
        }

        /// <summary>
        /// Ouvre le picker.
        /// </summary>
        public void _Open()
        {
            if (!(ItemsSource is IEnumerable) || _Page != null) return;
            if (!IsEnabled) return;

            var l_InteractionService = CoreSDK.Framework.Services.InteractionService() as NavigationPageInteractionService;

            // A chaque foi on reconstruit la page.
            _Page = new ContentPage()
            {
                Title = Title
            };
            {
                // La page contient une ListView.
                _ListView = new ListView();
                {
                    // On bind les propriétés.
                    _ListView.SetBinding(ListView.ItemsSourceProperty, new Binding(Picker.ItemsSourceProperty.PropertyName, BindingMode.OneWay, null, null, null, this));
                    _ListView.SetBinding(ListView.SelectedItemProperty, new Binding(Picker.SelectedItemProperty.PropertyName, BindingMode.TwoWay, null, null, null, this));

                    // Contruction du template de ListView si nécessaire.
                    if (ItemTemplate != null)
                    {
                        _ListView.ItemTemplate = new DataTemplate(() =>
                        {
                            var l_ViewCell = new ViewCell();
                            l_ViewCell.View = ItemTemplate.CreateContent() as View;
                            return l_ViewCell;
                        });
                    }

                    _ListView.ItemSelected += _ListView_ItemSelected;
                    _Page.Disappearing += _Page_Disappearing;
                }

                var _Header = new Label
                {
                    Text = Properties.Resources.PICKER_Header,
                    FontSize= Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                    HeightRequest = 35,
                    VerticalTextAlignment= TextAlignment.Center,
                    VerticalOptions= LayoutOptions.CenterAndExpand,
                    HorizontalOptions= LayoutOptions.CenterAndExpand,
                };

                var _StackLayout = new StackLayout()
                {
                    Children =
                    {
                        _Header,
                        _ListView
                    },
                    Padding = 10,
                    Spacing = 10
                };
                _Page.Content = _StackLayout;
            }

            l_InteractionService.Open(_Page, true);
        }

        void _ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (_Page == null) return;
            // Dès qu'un élément est sélectionné, on ferme le picker.
            var l_InteractionService = CoreSDK.Framework.Services.InteractionService() as NavigationPageInteractionService;
            l_InteractionService.Close();
        }

        void _Page_Disappearing(object sender, EventArgs e)
        {
            if (_Page == null) return;
            _Page.Disappearing -= _Page_Disappearing;
            _ListView.ItemSelected -= _ListView_ItemSelected;
            _ListView = null;
            _Page = null;
        }

        public event EventHandler<EventArgs> SelectedIndexChanged;

        private void _NotifySelectedIndexChanged()
        {
            if (SelectedIndexChanged != null)
            {
                SelectedIndexChanged(this, new EventArgs());
            }
        }
    }
}

