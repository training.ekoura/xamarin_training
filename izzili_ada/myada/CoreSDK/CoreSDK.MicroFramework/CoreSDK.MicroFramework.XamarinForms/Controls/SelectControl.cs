﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK.Controls
{
    public class SelectControl : StackLayout
    {
        public SelectControl()
        {
            BackgroundColor = Color.Transparent;
            VerticalOptions = LayoutOptions.FillAndExpand;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            var transparentStackGesture = new TapGestureRecognizer();
            transparentStackGesture.Tapped += (s, e) => 
            {
                if (IsSelected == ControlBoolean.True)
                    IsSelected = ControlBoolean.False;
                else
                    IsSelected = ControlBoolean.True;

                //UpdateChildrenLayout();
            };
            GestureRecognizers.Add(transparentStackGesture);
        }

        #region === IsSelected ===

        public static readonly BindableProperty IsSelectedProperty = BindableProperty.Create<SelectControl, ControlBoolean>(p => p.IsSelected, ControlBoolean.False, propertyChanged: (d, o, n) => (d as SelectControl).IsSelected_Changed(o, n));

        private void IsSelected_Changed(ControlBoolean oldvalue, ControlBoolean newvalue)
        {
        }

        [TypeConverter(typeof(StringToControlBoleanTypeConverter))]
        public ControlBoolean IsSelected
        {
            get { return (ControlBoolean)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        #endregion
    }
}
