﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CoreSDK.Controls
{
    public class RepeaterView : StackLayout
    {
        int NombreItem;
        public enum RepeaterViewProperty
        {
            True,
            False
        }

        public RepeaterView()
        {
            this.NombreItem = 0;
            //this.Spacing = ItemSpacing;
        }

        #region === ItemsSource ===

        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create<RepeaterView, object>(p => p.ItemsSource, default(object), propertyChanged: (d, o, n) => (d as RepeaterView).ItemsSource_Changed(o, n));

        private void ItemsSource_Changed(object oldvalue, object newvalue)
        {
            if (oldvalue is INotifyCollectionChanged)
            {
                (oldvalue as INotifyCollectionChanged).CollectionChanged -= RepeaterView_CollectionChanged;
            }

            if (newvalue is INotifyCollectionChanged)
            {
                (newvalue as INotifyCollectionChanged).CollectionChanged += RepeaterView_CollectionChanged;
            }
            Device.BeginInvokeOnMainThread(() =>
           Update());
        }

        public object ItemsSource
        {
            get { return (object)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        void RepeaterView_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
               Update(e));
            }
            catch (Exception)
            {
                //
            }
            finally
            {
                //FirstItem.Behaviors.Add(new Behaviors.RepeaterUpdateBehaviour());
            }
        }
        #endregion

        #region === ItemTemplate ===

        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create<RepeaterView, DataTemplate>(p => p.ItemTemplate, default(DataTemplate), propertyChanged: (d, o, n) => (d as RepeaterView).ItemTemplate_Changed(o, n));

        private void ItemTemplate_Changed(DataTemplate oldvalue, DataTemplate newvalue)
        {
            //if (oldvalue != newvalue)
            //    Update();
        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        #endregion

        #region === ItemSpacing ===

        public static readonly BindableProperty ItemSpacingProperty = BindableProperty.Create<RepeaterView, double>(p => p.ItemSpacing, default(double), propertyChanged: (d, o, n) => (d as RepeaterView).ItemSpacing_Changed(o, n));

        /// <summary>
        /// </summary>
        /// <param name="oldvalue"></param>
        /// <param name="newvalue"></param>
        private void ItemSpacing_Changed(double oldvalue, double newvalue)
        {
            Update();
        }

        public double ItemSpacing
        {
            get { return (double)GetValue(ItemSpacingProperty); }
            set { SetValue(ItemSpacingProperty, value); }
        }

        #endregion

        #region === HasDottedLines ===

        public static readonly BindableProperty HasDottedLinesProperty = BindableProperty.Create<RepeaterView, RepeaterViewProperty>(p => p.HasDottedLines, RepeaterViewProperty.False, propertyChanged: (d, o, n) => (d as RepeaterView).HasDottedLines_Changed(o, n));

        /// <summary>
        /// </summary>
        /// <param name="oldvalue"></param>
        /// <param name="newvalue"></param>
        private void HasDottedLines_Changed(RepeaterViewProperty oldvalue, RepeaterViewProperty newvalue)
        {
            Update();
        }

        public RepeaterViewProperty HasDottedLines
        {
            get { return (RepeaterViewProperty)GetValue(HasDottedLinesProperty); }
            set { SetValue(HasDottedLinesProperty, value); }
        }

        #endregion

        #region === HasTopSeperator ===

        public static readonly BindableProperty HasTopSeperatorProperty = BindableProperty.Create<RepeaterView, RepeaterViewProperty>(p => p.HasTopSeperator, RepeaterViewProperty.False, propertyChanged: (d, o, n) => (d as RepeaterView).HasTopSeperator_Changed(o, n));

        /// <summary>
        /// </summary>
        /// <param name="oldvalue"></param>
        /// <param name="newvalue"></param>
        private void HasTopSeperator_Changed(RepeaterViewProperty oldvalue, RepeaterViewProperty newvalue)
        {
            Update();
        }

        public RepeaterViewProperty HasTopSeperator
        {
            get { return (RepeaterViewProperty)GetValue(HasTopSeperatorProperty); }
            set { SetValue(HasTopSeperatorProperty, value); }
        }

        #endregion

        #region === DoRefresh ===

        public static readonly BindableProperty DoRefreshProperty = BindableProperty.Create<RepeaterView, RepeaterViewProperty>(p => p.DoRefresh, RepeaterViewProperty.False, propertyChanged: (d, o, n) => (d as RepeaterView).DoRefresh_Changed(o, n));

        private void DoRefresh_Changed(RepeaterViewProperty oldvalue, RepeaterViewProperty newvalue)
        {
            Update();
        }


        public RepeaterViewProperty DoRefresh
        {
            get { return (RepeaterViewProperty)GetValue(DoRefreshProperty); }
            set { SetValue(DoRefreshProperty, value); }
        }

        #endregion


        public void Update(NotifyCollectionChangedEventArgs e)
        {

            if (ItemTemplate == null || !(ItemsSource is IEnumerable)) return;


            var invalidate = false;

            if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                this.Children.Clear();
                invalidate = true;
            }

            else
            {

                this.Spacing = ItemSpacing;
                var l_NCount = (ItemsSource as IEnumerable).Cast<object>().Count();

                var l_Content = ItemTemplate.CreateContent() as View;
                if (e.OldItems != null)
                {
                    this.Children.RemoveAt(e.OldStartingIndex);
                    invalidate = true;
                }

                if (e.NewItems != null)
                {
                    var count = 0;
                    for (var i = 0; i < e.NewItems.Count; ++i)
                    {
                        
                        var item = e.NewItems[i];
                        //var view = this.CreateChildViewFor(item);

                        Debug.WriteLine("RepeaterViewUpdate ");
                        if (count == 0 && HasTopSeperator == RepeaterViewProperty.True)
                        {
                            this.Spacing = ItemSpacing / 2;
                            this.Children.Add(new dottedLabel() { HeightRequest = ItemSpacing, FontSize = (ItemSpacing / 2) + 1 });
                        }
                        this.Children.Add(l_Content);
                        l_Content.BindingContext = item;

                        if ((HasDottedLines == RepeaterViewProperty.True) && (count < l_NCount - 1))
                        {
                            this.Spacing = ItemSpacing / 2;
                            this.Children.Add(new dottedLabel() { HeightRequest = ItemSpacing, FontSize = (ItemSpacing / 2) + 1 });
                        }
                    }
                    invalidate = true;
                }
            }
            if (invalidate)
            {
                this.UpdateChildrenLayout();
                this.InvalidateLayout();
            }


        }

        public void Update()
        {

            if (ItemTemplate == null || !(ItemsSource is IEnumerable)) return;

            this.Spacing = ItemSpacing;


            var l_NCount = (ItemsSource as IEnumerable).Cast<object>().Count();

                this.Children.Clear();

                NombreItem = l_NCount;

                var count = 0;
                foreach (var l_Item in (ItemsSource as IEnumerable))
                {
                    var l_Content = ItemTemplate.CreateContent() as View;
                    if (l_Content != null)
                    {
                        try
                        {
                            Debug.WriteLine("RepeaterViewUpdate ");
                            if (count == 0 && HasTopSeperator == RepeaterViewProperty.True)
                            {
                                this.Spacing = ItemSpacing / 2;
                                this.Children.Add(new dottedLabel() { HeightRequest = ItemSpacing, FontSize = (ItemSpacing / 2) + 1 });
                            }

                            this.Children.Add(l_Content);
                            l_Content.BindingContext = l_Item;


                        }
                        catch (Exception l_Exception)
                        {
                            Debug.WriteLine("RepeaterView : " + l_Exception.Message);
                        }

                        if ((HasDottedLines == RepeaterViewProperty.True) && (count < l_NCount - 1))
                        {
                            this.Spacing = ItemSpacing / 2;
                            this.Children.Add(new dottedLabel() { HeightRequest = ItemSpacing, FontSize = (ItemSpacing / 2) + 1 });
                        }

                        //flashing update Behavior
                        //if (count == 0 && ShowsUpdate==RepeaterViewProperty.True)
                        //{
                        //    l_Content.Behaviors.Add(new Behaviors.RepeaterUpdateBehaviour());
                        //}
                    }
                    count++;
                }

                UpdateChildrenLayout();
                InvalidateLayout();
            
        }


    }

    public class CustomRepeaterView : StackLayout
    {
        public CustomRepeaterView()
        {
            this.Spacing = 0;
            //this.VerticalOptions = LayoutOptions.FillAndExpand;
            //this.HorizontalOptions = LayoutOptions.FillAndExpand;
            this.BackgroundColor = Color.Red;
        }

        // Correspond to max number of item on the same line if Collection count equal to this value.
        private int MaxValue = 3;
        // Correspond to number of elements on the same line if collection count greater than than MaxValue
        private int NbItemPerLine = 2;
        private int NbGridRows;
        // Style on Grid
        private Xamarin.Forms.GridLength GridColumnAuto = GridLength.Auto;

        public CustomRepeaterView(int _MaxValue, int _NbItemPerLine, Xamarin.Forms.GridLength _GridColumnAuto)
        {
            this.Spacing = 0;
            this.NbItemPerLine = _NbItemPerLine;
            this.MaxValue = _MaxValue;
            this.GridColumnAuto = _GridColumnAuto;
        }

        #region === ItemsSource ===

        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create<CustomRepeaterView, object>(p => p.ItemsSource, default(object), propertyChanged: (d, o, n) => (d as CustomRepeaterView).ItemsSource_Changed(o, n));

        private void ItemsSource_Changed(object oldvalue, object newvalue)
        {
            if (oldvalue is INotifyCollectionChanged)
            {
                (oldvalue as INotifyCollectionChanged).CollectionChanged -= CustomRepeaterView_CollectionChanged;
            }

            if (newvalue is INotifyCollectionChanged)
            {
                (newvalue as INotifyCollectionChanged).CollectionChanged += CustomRepeaterView_CollectionChanged;
            }
            Update();
        }

        public object ItemsSource
        {
            get { return (object)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        void CustomRepeaterView_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Update();
        }

        #endregion

        #region === ItemTemplate ===

        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create<CustomRepeaterView, DataTemplate>(p => p.ItemTemplate, default(DataTemplate), propertyChanged: (d, o, n) => (d as CustomRepeaterView).ItemTemplate_Changed(o, n));

        private void ItemTemplate_Changed(DataTemplate oldvalue, DataTemplate newvalue)
        {
            Update();
        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        #endregion

        private void Update()
        {
            this.Children.Clear();
            if (ItemTemplate == null || !(ItemsSource is IEnumerable)) return;

            var l_Grid = new Grid()
            {
                //HorizontalOptions = LayoutOptions.Fill,
                RowSpacing = 2
            };

            ////////////////////////////////////////////////////////
            // Determine number of Rows and columns
            ////////////////////////////////////////////////////////
            if ((ItemsSource as IEnumerable).Cast<object>().Count() == MaxValue)
            {
                NbItemPerLine = MaxValue;
                NbGridRows = 1;
            }
            else
            {
                //We set column to auto if there is a difference between MaxValue && NbItemPerLine
                if (MaxValue != NbItemPerLine)
                    GridColumnAuto = GridLength.Auto;

                var collectionCount = Math.Round((decimal)(ItemsSource as IEnumerable).Cast<object>().Count() / NbItemPerLine, 0, MidpointRounding.AwayFromZero);
                NbGridRows = (int)collectionCount;
            }

            ////////////////////////////////////////////////////////
            // Create Grid Rows
            ////////////////////////////////////////////////////////
            for (int i = 0; i < NbGridRows; i++)
            {
                l_Grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }


            ////////////////////////////////////////////////////////
            // Create Grid column
            ////////////////////////////////////////////////////////
            for (int j = 0; j < NbItemPerLine; j++)
            {
                l_Grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridColumnAuto });
            }

            ////////////////////////////////////////////////////////
            // Content creation
            ////////////////////////////////////////////////////////
            var countLoop = 0;
            var currentColumn = 0;
            var currentRow = 0;
            foreach (var l_Item in (ItemsSource as IEnumerable))
            {
                var l_Content = ItemTemplate.CreateContent() as View;
                if (l_Content != null)
                {
                    try
                    {
                        //this.Children.Add(l_Content);

                        if (countLoop % NbItemPerLine != 0)
                        {
                            currentColumn++;
                            l_Content.HorizontalOptions = LayoutOptions.Start;
                        }
                        else
                        {
                            l_Content.HorizontalOptions = LayoutOptions.End;
                            currentColumn = 0;
                            if (countLoop > 0)
                                currentRow++;
                        }

                        l_Grid.Children.Add(l_Content, currentColumn, currentRow);
                        l_Content.BindingContext = l_Item;

                        countLoop++;
                    }
                    catch (Exception l_Exception)
                    {
                        Debug.WriteLine("CustomRepeaterView : " + l_Exception.Message);
                    }
                }
            }

            // Adding new content to the parent stacklayout.
            this.Children.Add(l_Grid);


            UpdateChildrenLayout();
            InvalidateLayout();
        }

    }

}
