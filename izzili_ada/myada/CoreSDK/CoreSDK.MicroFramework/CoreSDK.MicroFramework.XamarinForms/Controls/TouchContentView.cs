﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSDK.Controls
{
    public class TouchContentView : Xamarin.Forms.ContentView
    {
        public TouchContentView()
        {
            this.HorizontalOptions = Xamarin.Forms.LayoutOptions.Fill;
            this.VerticalOptions = Xamarin.Forms.LayoutOptions.Fill;
        }
    }
}
