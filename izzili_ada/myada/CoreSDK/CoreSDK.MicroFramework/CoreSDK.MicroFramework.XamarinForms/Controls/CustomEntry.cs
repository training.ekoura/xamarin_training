﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace CoreSDK.Controls
{
    public class CustomEntry : Entry
    {
        // Need to overwrite default handler because we cant Invoke otherwise
        public new event EventHandler Completed;

        public CustomEntry Next { get; set; }

        public static readonly BindableProperty ReturnTypeProperty =
            BindableProperty.Create<CustomEntry, ReturnType>(s => s.ReturnType, ReturnType.Done);

        public ReturnType ReturnType
        {
            get { return (ReturnType)GetValue(ReturnTypeProperty); }
            set { SetValue(ReturnTypeProperty, value); }
        }

        public void InvokeCompleted()
        {
            if (this.Completed != null)
                this.Completed.Invoke(this, null);
        }
        public CustomEntry()
        {
            Completed += Goto;
        }

        private void Goto(object sender, EventArgs e)
        {
            if (sender != null && ((CustomEntry)sender).Next != null)
                ((CustomEntry)sender).Next.Focus();
        }

		#region === Font ===

		public static readonly BindableProperty FontProperty = BindableProperty.Create<CustomEntry, Font>(p => p.Font, default(Font), propertyChanged: (d, o, n) => (d as CustomEntry).FontProperty_Changed(o, n));

		private void FontProperty_Changed(Font oldvalue, Font newvalue)
		{
		}

		[TypeConverter(typeof(FontSizeConverter))]
		public Font Font
		{
			get { return (Font)GetValue(FontProperty); }
			set { SetValue(FontProperty, value); }
		}

		#endregion
    }

    public enum ReturnType
    {
        Go,
        Next,
        Done,
        Send,
        Search
    }
}

