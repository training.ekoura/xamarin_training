﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace CoreSDK.Controls
{
    public class CustomPlusPicker : ContentView
    {
        public int selectedValue = 0;
        public int selectedValue2 = 0;
        public string themeExtension;
        public string AlerteMessage;
        private bool CanShowAlertMessage=false;
        public CustomPlusPicker(string p_AlerteMessage= "")
        {
            AlerteMessage = p_AlerteMessage;
            //_ApplyTheme();
            _BuildTemplate();
            Items.CollectionChanged += Items_CollectionChanged;
        }

        #region === Items ===

        public static readonly BindableProperty ItemsProperty = BindableProperty.Create<CustomPlusPicker, ObservableCollection<object>>(p => p.Items, default(ObservableCollection<object>), propertyChanged: (d, o, n) => (d as CustomPlusPicker).Items_Changed(o, n));

        private void Items_Changed(ObservableCollection<object> oldvalue, ObservableCollection<object> newvalue)
        {

        }

        public ObservableCollection<object> Items
        {
            get
            {
                if (GetValue(ItemsProperty) == null) SetValue(ItemsProperty, new ObservableCollection<object>());
                return (ObservableCollection<object>)GetValue(ItemsProperty);
            }
        }

        private int _UseItems = 0;
        void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (_UseItems != 0) return;
            this.SetBinding(CustomPlusPicker.ItemsSourceProperty, new Binding(CustomPlusPicker.ItemsProperty.PropertyName, BindingMode.OneWay, null, null, null, this));
            _UseItems = 1;
        }

        #endregion

        #region === ItemsSource ===

        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create<CustomPlusPicker, object>(p => p.ItemsSource, default(object), propertyChanged: (d, o, n) => (d as CustomPlusPicker).ItemsSource_Changed(o, n));

        private void ItemsSource_Changed(object oldvalue, object newvalue)
        {
            if (_UseItems == 1)
            {
                _UseItems = 2;
                Items.Clear();
                _UseItems = 0;
            }
        }

        public object ItemsSource
        {
            get { return (object)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        #endregion

        #region === SelectedIndex ===

        public static readonly BindableProperty SelectedIndexProperty = BindableProperty.Create<CustomPlusPicker, int>(p => p.SelectedIndex, -1, propertyChanged: (d, o, n) => (d as CustomPlusPicker).SelectedIndex_Changed(o, n), defaultBindingMode: BindingMode.TwoWay);

        private void SelectedIndex_Changed(int oldvalue, int newvalue)
        {
            try
            {
                if (ItemsSource is IEnumerable && newvalue >= 0)
                {
                    if ((oldvalue == null && AlerteMessage.Length > 1) || (oldvalue != newvalue && AlerteMessage.Length > 1))
                        CanShowAlertMessage = true;
                    else
                        CanShowAlertMessage = false;

                    var l_List = (ItemsSource as IEnumerable).Cast<object>().ToList();
                    if (newvalue < l_List.Count)
                    {
                        SelectedItem = l_List[newvalue];
                        //selectedValue = (SelectedIndex + 1);
                        //selectedValue2 = newvalue+1;
                        return;
                    }
                }
                SelectedIndex = -1;
                SelectedItem = null;
            }
            finally
            {
                _NotifySelectedIndexChanged();
            }
        }

        public int SelectedIndex
        {
            get { return (int)GetValue(SelectedIndexProperty); }
            set { SetValue(SelectedIndexProperty, value); }
        }

        #endregion

        #region === SelectedItem ===

        public static readonly BindableProperty SelectedItemProperty = BindableProperty.Create<CustomPlusPicker, object>(p => p.SelectedItem, default(object), propertyChanged: (d, o, n) => (d as CustomPlusPicker).SelectedItem_Changed(o, n), defaultBindingMode: BindingMode.TwoWay);

        private void SelectedItem_Changed(object oldvalue, object newvalue)
        {
            

            if (ItemsSource is IEnumerable && newvalue != null)
            {
                SelectedIndex = (ItemsSource as IEnumerable).Cast<object>().ToList().IndexOf(newvalue);
                return;
            }
            SelectedIndex = -1;
            SelectedItem = null;
        }

        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        #endregion

        #region === ItemTemplate ===

        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create<CustomPlusPicker, DataTemplate>(p => p.ItemTemplate, default(DataTemplate), propertyChanged: (d, o, n) => (d as CustomPlusPicker).ItemTemplate_Changed(o, n));

        private void ItemTemplate_Changed(DataTemplate oldvalue, DataTemplate newvalue)
        {

        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        #endregion

        #region === Title ===

        public static readonly BindableProperty TitleProperty = BindableProperty.Create<CustomPlusPicker, string>(p => p.Title, default(string), propertyChanged: (d, o, n) => (d as CustomPlusPicker).Title_Changed(o, n));

        public void Title_Changed(string oldvalue, string newvalue)
        {

        }

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        #endregion

        #region === Placeholder ===

        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create<CustomPlusPicker, string>(p => p.Placeholder, default(string), propertyChanged: (d, o, n) => (d as CustomPlusPicker).Placeholder_Changed(o, n));

        private void Placeholder_Changed(string oldvalue, string newvalue)
        {

        }

        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        #endregion

        #region === DisplayMemberName ===

        public static readonly BindableProperty DisplayMemberNameProperty = BindableProperty.Create<CustomPlusPicker, string>(p => p.DisplayMemberName, default(string), propertyChanged: (d, o, n) => (d as CustomPlusPicker).DisplayMemberName_Changed(o, n));

        private void DisplayMemberName_Changed(string oldvalue, string newvalue)
        {
            //_UpdateTextBinding();
        }

        public string DisplayMemberName
        {
            get { return (string)GetValue(DisplayMemberNameProperty); }
            set { SetValue(DisplayMemberNameProperty, value); }
        }

        #endregion

        #region === TextColor ===

        public static readonly BindableProperty TextColorProperty = BindableProperty.Create<CustomPlusPicker, Color>(p => p.TextColor, Color.Default, propertyChanged: (d, o, n) => (d as CustomPlusPicker).TextColor_Changed(o, n));

        private void TextColor_Changed(Color oldvalue, Color newvalue)
        {

        }

        [TypeConverter(typeof(ColorTypeConverter))]
        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        #endregion

        #region === TextBackgroundColor ===

        public static readonly BindableProperty TextBackgroundColorProperty = BindableProperty.Create<CustomPlusPicker, Color>(p => p.TextBackgroundColor, default(Color), propertyChanged: (d, o, n) => (d as CustomPlusPicker).TextBackgroundColor_Changed(o, n));

        private void TextBackgroundColor_Changed(Color oldvalue, Color newvalue)
        {

        }

        [TypeConverter(typeof(ColorTypeConverter))]
        public Color TextBackgroundColor
        {
            get { return (Color)GetValue(TextBackgroundColorProperty); }
            set { SetValue(TextBackgroundColorProperty, value); }
        }

        #endregion

        #region === CommandParameter(case RecetteId) ===

        public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create<CustomPlusPicker, string>(p => p.CommandParameter, default(string), propertyChanged: (d, o, n) => (d as CustomPlusPicker).CommandParameter_Changed(o, n));

        private void CommandParameter_Changed(string oldvalue, string newvalue)
        {

        }

        public string CommandParameter
        {
            get { return (string)GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        #endregion

        #region === CommandParameter2(case RecetteQuantiteMaximumCommandable) ===

        public static readonly BindableProperty CommandParameter2Property = BindableProperty.Create<CustomPlusPicker, int>(p => p.CommandParameter2, default(int), propertyChanged: (d, o, n) => (d as CustomPlusPicker).CommandParameter2_Changed(o, n));

        private void CommandParameter2_Changed(int oldvalue, int newvalue)
        {
            if (ItemsSource is IEnumerable && newvalue > 0)
            {
                try
                {

                }
                catch (Exception)
                {
                    //
                }
                finally
                {
                    _NotifyCommandProperty2Changed();
                }
            }
        }

        public int CommandParameter2
        {
            get { return (int)GetValue(CommandParameter2Property); }
            set { SetValue(CommandParameter2Property, value); }
        }

        #endregion

        #region === ThemeParameter ===

        public static readonly BindableProperty ThemeParameterProperty = BindableProperty.Create<CustomPlusPicker, object>(p => p.ThemeParameter, default(object), propertyChanged: (d, o, n) => (d as CustomPlusPicker).ThemeParameter_Changed(o, n));


        private void ThemeParameter_Changed(object oldvalue, object newvalue)
        {
            //_ApplyTheme();
            _BuildTemplate();
        }

        public object ThemeParameter
        {
            get { return (object)GetValue(ThemeParameterProperty); }
            set { SetValue(ThemeParameterProperty, value); }
        }

        #endregion

        private new View Content { get { return base.Content; } set { base.Content = value; } }

        private Grid _RootGrid;
        private Image _ImageBg;
        private Label _Label;
        private StackLayout _Overlay;
        private Image _PlusImage;
        private ContentPage _Page;
        private ListView _ListView;

        /// <summary>
        /// Contruit le CustomPlusPicker (sur la base d'un modèle de contenu).
        /// </summary>
        private void _BuildTemplate()
        {
            // Root Grid
            _RootGrid = new Grid()
            {
                ColumnSpacing=0,
                RowSpacing=0,
                BackgroundColor=Color.Transparent,
                //substract from inidvidual reapeterViewHeights
                //Padding=Device.OnPlatform(new Thickness(10,10,10,10),new Thickness(10,10,10,10),new Thickness(10,10,10,10)),
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                RowDefinitions = 
                                { 
                                    new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) },
                                },
                ColumnDefinitions = 
                                { 
                                    new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
                                }
            };

            #region ==Overlay to handle taps==
            _Overlay = new StackLayout()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.Transparent
                //Opacity=0
            };

            
             var OverlayGesture = new TapGestureRecognizer();


             OverlayGesture.Tapped += (object sender, EventArgs e) =>
                {
                    System.Diagnostics.Debug.WriteLine("Enter Gesture: ");
                    _Open(); 
                
                };

             _Overlay.GestureRecognizers.Add(OverlayGesture);

            
            /*
            _Overlay.GestureRecognizers.Add(new TapGestureRecognizer((v) =>
            {
                // Lorsque l'on tap l'overlay, on ouvre le CustomPlusPicker.
                _Open();
            }));*/
            #endregion

            Grid.SetColumn(_Overlay, 0);
            Grid.SetRow(_Overlay, 0);
            _RootGrid.Children.Add(_Overlay);
            

            Content = _RootGrid;
        }

        /// <summary>
        /// Cette méthode est utilisée pour binder une propriété d'un objet du modèle à une propriété du CustomPlusPicker.
        /// </summary>
        /// <param name="p_Object"></param>
        /// <param name="p_Property"></param>
        /// <param name="p_Path"></param>
        private void _SetTemplateBinding(BindableObject p_Object, BindableProperty p_Property, string p_Path)
        {
            p_Object.SetBinding(p_Property, new Binding(p_Path, BindingMode.OneWay, null, null, null, this));
        }

        /// <summary>
        /// Ouvre le CustomPlusPicker.
        /// </summary>
            public void _Open()
        {
            if (!(ItemsSource is IEnumerable) || _Page != null) return;
            if (!IsEnabled) return;

            var l_InteractionService = CoreSDK.Framework.Services.InteractionService() as NavigationPageInteractionService;

            // A chaque foi on reconstruit la page.
            _Page = new ContentPage()
            {
                Title = Title
            };
            {
                // La page contient une ListView.
                _ListView = new ListView();
                {
                    // On bind les propriétés.
                    _ListView.SetBinding(ListView.ItemsSourceProperty, new Binding(CustomPlusPicker.ItemsSourceProperty.PropertyName, BindingMode.OneWay, null, null, null, this));
                    _ListView.SetBinding(ListView.SelectedItemProperty, new Binding(CustomPlusPicker.SelectedItemProperty.PropertyName, BindingMode.TwoWay, null, null, null, this));

                    // Contruction du template de ListView si nécessaire.
                    if (ItemTemplate != null)
                    {
                        _ListView.ItemTemplate = new DataTemplate(() =>
                        {
                            var l_ViewCell = new ViewCell();
                            l_ViewCell.View = ItemTemplate.CreateContent() as View;
                            return l_ViewCell;
                        });
                    }

                    _ListView.ItemSelected += _ListView_ItemSelected;
                    _Page.Disappearing += _Page_Disappearing;
                }

                var _Header = new Label
                {
                    Text = Properties.Resources.PICKER_Header,
                    FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                    HeightRequest = 35,
                    VerticalTextAlignment = TextAlignment.Center,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                };

                var _StackLayout = new StackLayout()
                {
                    Children =
                    {
                        _Header,
                        _ListView
                    },
                    Padding = 10,
                    Spacing = 10
                };

                var _GridLayout = new Grid();
                _GridLayout.Children.Add(_StackLayout);


                _Page.Content = _GridLayout;
            }

            l_InteractionService.Open(_Page, true);
        }

        async void _ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (_Page == null) return;
            if (AlerteMessage.Length > 1 && CanShowAlertMessage) { 
                await CoreSDK.Framework.Services.InteractionService().Alert("", AlerteMessage, 2, AlertType.Success);
                CanShowAlertMessage = false;
            }
            var l_InteractionService = CoreSDK.Framework.Services.InteractionService() as NavigationPageInteractionService;
            l_InteractionService.Close();
        }

        void _Page_Disappearing(object sender, EventArgs e)
        {
            if (_Page == null) return;

            _Page.Disappearing -= _Page_Disappearing;
            _ListView.ItemSelected -= _ListView_ItemSelected;
            _ListView = null;
            _Page = null;

        }

        public event EventHandler<EventArgs> SelectedIndexChanged;

        private void _NotifySelectedIndexChanged()
        {
            if (SelectedIndexChanged != null)
            {
                SelectedIndexChanged(this, new EventArgs());
            }
        }

        public event EventHandler<EventArgs> CommandProperty2Changed;

        private void _NotifyCommandProperty2Changed()
        {
            if (CommandProperty2Changed != null)
            {
                CommandProperty2Changed(this, new EventArgs());
            }
        }

        private void _ApplyTheme()
        {
            switch ((string)ThemeParameter)
            {
                case ThemeNameForCarousel.MySodexo:
                    themeExtension = "";
                    break;
                case ThemeNameForCarousel.InspirationEntreprise:
                    themeExtension = "Insp";
                    break;
                case ThemeNameForCarousel.InspirationScolaire:
                    themeExtension = "InspEdu";
                    break;
                case ThemeNameForCarousel.MySodexo360:
                    themeExtension = "360";
                    break;
            }
        }
    }
}

