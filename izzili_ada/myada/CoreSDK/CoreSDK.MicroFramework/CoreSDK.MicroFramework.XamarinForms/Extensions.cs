﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK
{
    public static class Extensions
    {

        public static void Invalidate(this View p_View)
        {
            // Astuce pour corriger un bogue Xamarin
            p_View.WidthRequest = p_View.Width + 1;
            p_View.WidthRequest = -1;
        }
    }
}
