﻿// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.42000
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace CoreSDK.Properties {
    using System;
    using System.Reflection;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources_fr {
        
        private static System.Resources.ResourceManager resourceMan;
        
        private static System.Globalization.CultureInfo resourceCulture;
        
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources_fr() {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static System.Resources.ResourceManager ResourceManager {
            get {
                if (object.Equals(null, resourceMan)) {
                    System.Resources.ResourceManager temp = new System.Resources.ResourceManager("CoreSDK.Properties.Resources.fr", typeof(Resources_fr).GetTypeInfo().Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        internal static string ERR_BindableObjectExtensions_GetViewModel_NotFound {
            get {
                return ResourceManager.GetString("ERR_BindableObjectExtensions_GetViewModel_NotFound", resourceCulture);
            }
        }
        
        internal static string ERR_EnumConverter_BadParameter {
            get {
                return ResourceManager.GetString("ERR_EnumConverter_BadParameter", resourceCulture);
            }
        }
        
        internal static string ERR_EnumConverter_EnumDataSourceNotInitialized {
            get {
                return ResourceManager.GetString("ERR_EnumConverter_EnumDataSourceNotInitialized", resourceCulture);
            }
        }
        
        internal static string ERR_EnumConverter_ItemNotFound {
            get {
                return ResourceManager.GetString("ERR_EnumConverter_ItemNotFound", resourceCulture);
            }
        }
        
        internal static string ERR_ExpressionConverter_BadExpression {
            get {
                return ResourceManager.GetString("ERR_ExpressionConverter_BadExpression", resourceCulture);
            }
        }
        
        internal static string ERR_ExpressionConverter_ParameterIsMissing {
            get {
                return ResourceManager.GetString("ERR_ExpressionConverter_ParameterIsMissing", resourceCulture);
            }
        }
        
        internal static string ERR_ImageNameConverter_BadImageNameFormat {
            get {
                return ResourceManager.GetString("ERR_ImageNameConverter_BadImageNameFormat", resourceCulture);
            }
        }
        
        internal static string ERR_ImageNameConverter_FileImageSource {
            get {
                return ResourceManager.GetString("ERR_ImageNameConverter_FileImageSource", resourceCulture);
            }
        }
        
        internal static string ERR_INTERACTION_NOTFOUND {
            get {
                return ResourceManager.GetString("ERR_INTERACTION_NOTFOUND", resourceCulture);
            }
        }
        
        internal static string ERR_NAVIGATION_DIRECT_POP {
            get {
                return ResourceManager.GetString("ERR_NAVIGATION_DIRECT_POP", resourceCulture);
            }
        }
        
        internal static string ERR_NAVIGATION_DIRECT_PUSH {
            get {
                return ResourceManager.GetString("ERR_NAVIGATION_DIRECT_PUSH", resourceCulture);
            }
        }
        
        internal static string ERR_ThemManager_Apply {
            get {
                return ResourceManager.GetString("ERR_ThemManager_Apply", resourceCulture);
            }
        }
        
        internal static string ERR_ThemManager_CallbackCantBeNull {
            get {
                return ResourceManager.GetString("ERR_ThemManager_CallbackCantBeNull", resourceCulture);
            }
        }
        
        internal static string ERR_ThemManager_ThemeDoesNotExist {
            get {
                return ResourceManager.GetString("ERR_ThemManager_ThemeDoesNotExist", resourceCulture);
            }
        }
        
        internal static string ERR_ThemManager_ThemeNameBadFormat {
            get {
                return ResourceManager.GetString("ERR_ThemManager_ThemeNameBadFormat", resourceCulture);
            }
        }
        
        internal static string INTERACTIONS_LABEL_CANCEL {
            get {
                return ResourceManager.GetString("INTERACTIONS_LABEL_CANCEL", resourceCulture);
            }
        }
        
        internal static string INTERACTIONS_LABEL_OK {
            get {
                return ResourceManager.GetString("INTERACTIONS_LABEL_OK", resourceCulture);
            }
        }
        
        internal static string PICKER_Header {
            get {
                return ResourceManager.GetString("PICKER_Header", resourceCulture);
            }
        }
        
        internal static string SecondBurger_Header_Label {
            get {
                return ResourceManager.GetString("SecondBurger_Header_Label", resourceCulture);
            }
        }
        
        internal static string SecondBurger_Recharge_Button_Text {
            get {
                return ResourceManager.GetString("SecondBurger_Recharge_Button_Text", resourceCulture);
            }
        }
        
        internal static string SecondBurger_Solde_Details_Label {
            get {
                return ResourceManager.GetString("SecondBurger_Solde_Details_Label", resourceCulture);
            }
        }
        
        internal static string BUTTON_Validate {
            get {
                return ResourceManager.GetString("BUTTON_Validate", resourceCulture);
            }
        }
        
        internal static string Label_ISWorking {
            get {
                return ResourceManager.GetString("Label_ISWorking", resourceCulture);
            }
        }
        
        internal static string Calendar_ContentView_Button_Text {
            get {
                return ResourceManager.GetString("Calendar_ContentView_Button_Text", resourceCulture);
            }
        }
        
        internal static string Deconnexion_ContentView_ButtonNo_Text {
            get {
                return ResourceManager.GetString("Deconnexion_ContentView_ButtonNo_Text", resourceCulture);
            }
        }
        
        internal static string Deconnexion_ContentView_ButtonYes_Text {
            get {
                return ResourceManager.GetString("Deconnexion_ContentView_ButtonYes_Text", resourceCulture);
            }
        }
        
        internal static string YesNo_ContentView_Header_Text {
            get {
                return ResourceManager.GetString("YesNo_ContentView_Header_Text", resourceCulture);
            }
        }
        
        internal static string Deconnexion_ContentView_Label_Text {
            get {
                return ResourceManager.GetString("Deconnexion_ContentView_Label_Text", resourceCulture);
            }
        }
        
        internal static string AnnulerCommande_ContentView_Label_Text {
            get {
                return ResourceManager.GetString("AnnulerCommande_ContentView_Label_Text", resourceCulture);
            }
        }
        
        internal static string StatutFilterContent_Header_Text {
            get {
                return ResourceManager.GetString("StatutFilterContent_Header_Text", resourceCulture);
            }
        }
        
        internal static string StatutFilterContent_ButtonValidation_Text {
            get {
                return ResourceManager.GetString("StatutFilterContent_ButtonValidation_Text", resourceCulture);
            }
        }
        
        internal static string Friday {
            get {
                return ResourceManager.GetString("Friday", resourceCulture);
            }
        }
        
        internal static string Monday {
            get {
                return ResourceManager.GetString("Monday", resourceCulture);
            }
        }
        
        internal static string Saturday {
            get {
                return ResourceManager.GetString("Saturday", resourceCulture);
            }
        }
        
        internal static string Sunday {
            get {
                return ResourceManager.GetString("Sunday", resourceCulture);
            }
        }
        
        internal static string Thursday {
            get {
                return ResourceManager.GetString("Thursday", resourceCulture);
            }
        }
        
        internal static string Tuesday {
            get {
                return ResourceManager.GetString("Tuesday", resourceCulture);
            }
        }
        
        internal static string Wednesday {
            get {
                return ResourceManager.GetString("Wednesday", resourceCulture);
            }
        }
    }
}
