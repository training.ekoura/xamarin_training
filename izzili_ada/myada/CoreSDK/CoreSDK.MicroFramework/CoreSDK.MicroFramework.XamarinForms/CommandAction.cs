﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CoreSDK
{

    public class CommandAction : TriggerAction<BindableObject>
    {
        private class CommandHelper : BindableObject
        {
            #region === Command ===

            public static readonly BindableProperty CommandProperty = BindableProperty.Create<CommandHelper, ICommand>(p => p.Command, default(ICommand), propertyChanged: (d, o, n) => (d as CommandHelper).Command_Changed(o, n));

            private void Command_Changed(ICommand oldvalue, ICommand newvalue)
            {

            }

            public ICommand Command
            {
                get { return (ICommand)GetValue(CommandProperty); }
                set { SetValue(CommandProperty, value); }
            }

            #endregion

            #region === CommandParameter ===

            public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create<CommandHelper, object>(p => p.CommandParameter, default(object), propertyChanged: (d, o, n) => (d as CommandHelper).CommandParameter_Changed(o, n));

            private void CommandParameter_Changed(object oldvalue, object newvalue)
            {

            }

            public object CommandParameter
            {
                get { return (object)GetValue(CommandParameterProperty); }
                set { SetValue(CommandParameterProperty, value); }
            }

            #endregion
        }

        public CommandAction()
        {

        }

        private BindingBase _Command;
        public BindingBase Command
        {
            get { return _Command; }
            set { _Command = value; }
        }

        private object _CommandParameter;
        public object CommandParameter
        {
            get { return _CommandParameter; }
            set { _CommandParameter = value; }
        }


        protected override void Invoke(BindableObject sender)
        {
            if (Command == null) return;

            var _Helper = new CommandHelper();
            try
            {
                _Helper.SetBinding(CommandHelper.BindingContextProperty, new Binding("BindingContext", BindingMode.OneWay, null, null, null, sender));
                _Helper.SetBinding(CommandHelper.CommandProperty, Command);
                if (CommandParameter is BindingBase)
                {
                    _Helper.SetBinding(CommandHelper.CommandParameterProperty, CommandParameter as BindingBase);
                }
                else
                {
                    _Helper.CommandParameter = CommandParameter;
                }


                if (_Helper.Command != null && _Helper.Command.CanExecute(_Helper.CommandParameter))
                {
                    _Helper.Command.Execute(_Helper.CommandParameter);
                }
            }
            finally
            {
                if (CommandParameter is BindingBase)
                {
                    _Helper.RemoveBinding(CommandHelper.CommandParameterProperty);
                }                  
                _Helper.RemoveBinding(CommandHelper.CommandProperty);
                _Helper.RemoveBinding(CommandHelper.BindingContextProperty);
            }
        }
    }
}
