﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoreSDK
{
    public static class BindableObjectExtensions
    {
        /// <summary>
        /// Cette méthode retourne le ViewModel en BindingContext.
        /// </summary>
        public static TViewModel Get<TViewModel>(this BindableObject p_BindableObject, bool p_ExceptionIfNull = true)
            where TViewModel : class, IViewModel
        {
            var l_ViewModel = p_BindableObject.BindingContext as TViewModel;
            if (l_ViewModel == null && p_ExceptionIfNull) throw new Exception(string.Format(Properties.Resources.ERR_BindableObjectExtensions_GetViewModel_NotFound, typeof(TViewModel).Name));
            return l_ViewModel;
        }

        /// <summary>
        /// Cette méthode permet de surveiller (et donc de réagir) des propriétés du ViewModel sous jascent au BindableObject.
        /// </summary>
        /// <typeparam name="TViewModel">Type du ViewModel attendu.</typeparam>
        /// <param name="p_BindableObject">BindableObject dont le BindingContext doit être le ViewModel attendu.</param>
        /// <param name="p_CallBack">Methode à appeler en cas de modification.</param>
        /// <param name="p_Properties">Liste des propriétés à surveiller. Les états peuvent aussi être surveillés.</param>
        public static void Watch<TViewModel>(this BindableObject p_BindableObject, Action<TViewModel> p_CallBack, params string[] p_Properties)
            where TViewModel : class, IViewModel
        {
            new WatchClass<TViewModel>(p_BindableObject, p_Properties, p_CallBack, false);
        }

        /// <summary>
        /// Cette méthode permet de surveiller (et donc de réagir) des propriétés du ViewModel sous jascent au BindableObject.
        /// </summary>
        /// <typeparam name="TViewModel">Type du ViewModel attendu.</typeparam>
        /// <param name="p_BindableObject">BindableObject dont le BindingContext doit être le ViewModel attendu.</param>
        /// <param name="p_CallBack">Methode à appeler en cas de modification.</param>
        /// <param name="p_ForceUpdateEventIfViewModelIsNull">Si true, alors la méthode de mise à jour est appelée même si le ViewModel est null.</param>
        /// <param name="p_Properties">Liste des propriétés à surveiller. Les états peuvent aussi être surveillés.</param>
        public static void Watch<TViewModel>(this BindableObject p_BindableObject, Action<TViewModel> p_CallBack, bool p_ForceUpdateEventIfViewModelIsNull, params string[] p_Properties)
            where TViewModel : class, IViewModel
        {
            new WatchClass<TViewModel>(p_BindableObject, p_Properties, p_CallBack, p_ForceUpdateEventIfViewModelIsNull);
        }

        private class WatchClass<TContext>
            where TContext : class
        {
            public WatchClass(BindableObject p_BindableObject, string[] p_Properties, Action<TContext> p_CallBack, bool p_ForceUpdateEventIfViewModelIsNull)
            {
                _BindableObject = p_BindableObject;
                _Properties = (from p in p_Properties select p.StartsWith("[") ? "Item" + p : p).ToList();
                _Callback = p_CallBack;
                _BindableObject.BindingContextChanged += (s, e) => UpdateContext();
                _ForceUpdateEventIfViewModelIsNull = p_ForceUpdateEventIfViewModelIsNull;
                UpdateContext();
            }

            private BindableObject _BindableObject;

            private List<string> _Properties;

            Action<TContext> _Callback;

            private INotifyPropertyChanged _ViewModel;

            private bool _ForceUpdateEventIfViewModelIsNull = false;

            void Update()
            {
                var l_ViewModel = _ViewModel as TContext;
                if (l_ViewModel != null || _ForceUpdateEventIfViewModelIsNull)
                {
                    _Callback(l_ViewModel);
                }
            }

            void UpdateContext()
            {
                if (_ViewModel != null)
                {
                    _ViewModel.PropertyChanged -= _Context_PropertyChanged;
                }
                
                _ViewModel = _BindableObject.BindingContext as TContext as INotifyPropertyChanged;

                if (_ViewModel != null)
                {
                    _ViewModel.PropertyChanged += _Context_PropertyChanged;
                }

                Update();
            }

            void _Context_PropertyChanged(object sender, PropertyChangedEventArgs e)
            {
                if ((e.PropertyName.EndsWith("[]") && _Properties.Contains(e.PropertyName.Remove(e.PropertyName.Length-2,2)))
                    || _Properties.Contains(e.PropertyName))
                {
                    Update();
                }
            }
        }
    }
}
