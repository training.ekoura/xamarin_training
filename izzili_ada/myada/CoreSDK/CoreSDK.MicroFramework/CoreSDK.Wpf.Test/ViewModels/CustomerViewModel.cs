﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML CustomerViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  Sodexo.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;

namespace Sodexo.Wpf.Test.ViewModels
{
	public abstract partial class CustomerViewModelBase : Sodexo.ViewModel
	{
		protected override void OnInitialize()
		{
			base.OnInitialize();

			// Initialisation de la propriété Id avec sa valeur par défaut.
			Id = Guid.NewGuid();
		}

		protected override void AfterInitialize()
		{
			base.AfterInitialize();

			// Notification des dépendances de la propriété Firstname.
			NotifyFirstnameDependencies();
			// Notification des dépendances de la propriété Lastname.
			NotifyLastnameDependencies();
		}

		#region === Propriétés ===

			#region === Propriété : Id ===

				public const string Id_PROPERTYNAME = "Id";

				private Guid _Id;
				///<summary>
				/// Propriété : Id
				/// Cette propriété sert d'identifiant.
				/// Il faut noter qu'elle est initialisée avec une valeur par défaut.
				///</summary>
				public Guid Id
				{
					get
					{
						return GetValue<Guid>(() => _Id);
					}
					set
					{
						SetValue<Guid>(() => _Id, (v) => _Id = v, value, Id_PROPERTYNAME,  DoIdBeforeSet, DoIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIdBeforeSet(string p_PropertyName, Guid p_OldValue, Guid p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIdAfterSet(string p_PropertyName, Guid p_OldValue, Guid p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Gender ===

				public const string Gender_PROPERTYNAME = "Gender";

				private GenderEnum _Gender;
				///<summary>
				/// Propriété : Gender
				///</summary>
				public GenderEnum Gender
				{
					get
					{
						return GetValue<GenderEnum>(() => _Gender);
					}
					set
					{
						SetValue<GenderEnum>(() => _Gender, (v) => _Gender = v, value, Gender_PROPERTYNAME,  DoGenderBeforeSet, DoGenderAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoGenderBeforeSet(string p_PropertyName, GenderEnum p_OldValue, GenderEnum p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoGenderAfterSet(string p_PropertyName, GenderEnum p_OldValue, GenderEnum p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyGenderDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Firstname ===

				public const string Firstname_PROPERTYNAME = "Firstname";

				private string _Firstname;
				///<summary>
				/// Propriété : Firstname
				///</summary>
				public string Firstname
				{
					get
					{
						return GetValue<string>(() => _Firstname);
					}
					set
					{
						SetValue<string>(() => _Firstname, (v) => _Firstname = v, value, Firstname_PROPERTYNAME,  DoFirstnameBeforeSet, DoFirstnameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFirstnameBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFirstnameAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
					// Puisque cette propriété a des dépendances, il faut les notifier.
					NotifyFirstnameDependencies();
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFirstnameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
					DoFullnameUpdate();
				}

			#endregion

			#region === Propriété : Lastname ===

				public const string Lastname_PROPERTYNAME = "Lastname";

				private string _Lastname;
				///<summary>
				/// Propriété : Lastname
				///</summary>
				public string Lastname
				{
					get
					{
						return GetValue<string>(() => _Lastname);
					}
					set
					{
						SetValue<string>(() => _Lastname, (v) => _Lastname = v, value, Lastname_PROPERTYNAME,  DoLastnameBeforeSet, DoLastnameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLastnameBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLastnameAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
					// Puisque cette propriété a des dépendances, il faut les notifier.
					NotifyLastnameDependencies();
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLastnameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
					DoFullnameUpdate();
				}

			#endregion

			#region === Propriété : Email ===

				public const string Email_PROPERTYNAME = "Email";

				private string _Email;
				///<summary>
				/// Propriété : Email
				///</summary>
				public string Email
				{
					get
					{
						return GetValue<string>(() => _Email);
					}
					set
					{
						SetValue<string>(() => _Email, (v) => _Email = v, value, Email_PROPERTYNAME,  DoEmailBeforeSet, DoEmailAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoEmailBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoEmailAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyEmailDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Fullname ===

				public const string Fullname_PROPERTYNAME = "Fullname";

				private string _Fullname;
				///<summary>
				/// Propriété : Fullname
				///</summary>
				public string Fullname
				{
					get
					{
						return GetValue<string>(() => _Fullname);
					}
					private set
					{
						SetValue<string>(() => _Fullname, (v) => _Fullname = v, value, Fullname_PROPERTYNAME,  DoFullnameBeforeSet, DoFullnameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFullnameBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFullnameAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFullnameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux propriétés calculées ===

			// Notez que les propriétés calculées sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Propriété calculée : Fullname  ===

				///<summary>
				/// Méthode de calcule de la propriété : Fullname.
				/// Cette propriété est la concaténation de Firstname et Lastname.
				///</summary>
				protected abstract string OnFullname_Calculate();


			#endregion

		#endregion

		#region === Spécificitées liées aux mises à jour (Update) ===

			#region === Mise à jour : FullnameUpdate ===

				private bool _IsInFullnameUpdate;
				///<summary>
				/// Cette méthode est appelée pour provoquer la mise à jour : FullnameUpdate.
				///</summary>
				private void DoFullnameUpdate()
				{
					lock(Locker)
					{
						if(_IsInFullnameUpdate) return;
						try
						{
							_IsInFullnameUpdate = true;
							OnFullnameUpdate_Execute();
						}
						finally
						{
							_IsInFullnameUpdate = false;
						}
					}
				}

				///<summary>
				/// Méthode de mise à jour : FullnameUpdate.
				///</summary>
				private void OnFullnameUpdate_Execute()
				{
					Fullname = OnFullname_Calculate();
				}

			#endregion


		#endregion

	}

	public partial class CustomerViewModel : CustomerViewModelBase
	{
	}
}
