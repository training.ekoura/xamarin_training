
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;

namespace Sodexo.Wpf.Test.ViewModels
{
	public partial class InvoiceViewModel : InvoiceViewModelBase
	{
        protected override void OnTotalUpdate_Execute()
        {
            System.Diagnostics.Debug.WriteLine("===> Update");

            var l_TotalExcludingTaxes = 0m;
            var l_TotalTaxes = 0m;

            Taxes.Clear();

            foreach (var l_Line in Lines)
            {
                l_Line.Amount = l_Line.Quantity * l_Line.UnitPrice;
                l_TotalExcludingTaxes += l_Line.Amount;
                l_TotalTaxes += l_Line.Amount * (decimal)l_Line.VatRate / 100m;

                var l_Item = (from i in Taxes where i.Key == l_Line.VatRate select i).FirstOrDefault();
                if (l_Item == null)
                {
                    l_Item = new TaxeKeyValue() { Key = l_Line.VatRate, Value = 0m };
                    Taxes.Add(l_Item);
                }
                l_Item.Value = l_Item.Value + l_Line.Amount * (decimal)l_Line.VatRate / 100m;
            }

            TotalExcludingTaxes = l_TotalExcludingTaxes;
            TotalTaxes = l_TotalTaxes;
            TotalIncludingTaxes = l_TotalExcludingTaxes + TotalTaxes;
        }

        public override void OnAddLineCommand_Execute(object p_Parameter)
        {
            Lines.Add(new InvoiceLineViewModel(this));
        }

        protected override string OnTest_Calculate()
        {
            return "ok";
        }

        public override bool OnDeleteLineCommand_CanExecute(InvoiceLineViewModel p_Parameter)
        {
            return true;
        }

        public override void OnDeleteLineCommand_Execute(InvoiceLineViewModel p_Parameter)
        {
            Lines.Remove(p_Parameter);
        }

        protected override void OnValidate(IValidationContext p_Context)
        {
            base.OnValidate(p_Context);

            p_Context.AddError_Required(Number, Number_PROPERTYNAME);
            p_Context.AddError_Required(Description, Description_PROPERTYNAME);
            p_Context.AddError_Required(Date, Date_PROPERTYNAME);

            if (Lines.Count == 0) p_Context.AddError("Ne peut pas �tre vide", Lines_PROPERTYNAME);
            p_Context.InvolveItems(Lines, (l_Context, l_Line) =>
            {
                if (string.IsNullOrWhiteSpace(l_Line.Description)) l_Context.AddError("Requis", InvoiceLineViewModel.Description_PROPERTYNAME);
                if (l_Line.Quantity <= 0) l_Context.AddError("La quantit� doit �tre positive", InvoiceLineViewModelBase.Quantity_PROPERTYNAME);
            });
        }

        public override void OnOkCommand_Execute(object p_Parameter)
        {
            if (Validate())
            {
                System.Windows.MessageBox.Show("ok");
            }
        }
    }
}
