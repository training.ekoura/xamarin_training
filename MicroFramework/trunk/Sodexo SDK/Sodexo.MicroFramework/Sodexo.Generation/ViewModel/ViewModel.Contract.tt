﻿<#@ template language="C#" debug="True" #><#+
	//--------------------------------------------------------------------------------
	// Extension du ViewModel généré à partir du Xsd.
	//--------------------------------------------------------------------------------	
	public partial class ViewModelContract {

		private List<Action> _CodeForConstructor = new List<Action>();

		public void AddCodeToConstructor(Action p_Code)
        {
			_CodeForConstructor.Add(p_Code);
        }

		private List<Action> _CodeAfterInit = new List<Action>();

		public void AddCodeAfterInit(Action p_Code)
        {
			_CodeAfterInit.Add(p_Code);
        }

		public void WriteConstructor()
        {
			if(_CodeForConstructor.Count>0)
            {
				WriteCodeLine("protected override void OnInitialize()");	
				using(CodeBlock())
				{
					WriteCodeLine("base.OnInitialize();");
					WriteCodeLine();
					foreach(var l_Code in _CodeForConstructor)
					{
						l_Code();
					}
				}

				WriteCodeLine();
            }

			if(_CodeAfterInit.Count>0)
            {
				WriteCodeLine("protected override void AfterInitialize()");	
				using(CodeBlock())
				{
					WriteCodeLine("base.AfterInitialize();");
					WriteCodeLine();
					foreach(var l_Code in _CodeAfterInit)
					{
						l_Code();
					}
				}

				WriteCodeLine();
            }
        }

		public elementProperty FindProperty(string p_PropertyName, bool p_ExceptionIfNotExist = true)
        {
			var l_Result = (from p in Property where p.Name == p_PropertyName select p).FirstOrDefault();
			if(l_Result == null && p_ExceptionIfNotExist) throw new Exception(string.Format("La propriété '{0}' n'a pas été trouvée dans le contrat du ViewModel.", p_PropertyName));
			return l_Result;
        }

		public void Analyse_Phase1()
        {
			foreach(var l_Property in this.Property)
			{
				l_Property.Analyse_Phase1();
			}

			foreach(var l_Collection in this.Collection)
			{
				l_Collection.Analyse_Phase1();
			}

			foreach(var l_Calculation in this.Calculation)
			{
				l_Calculation.Analyse_Phase1();
			}

			foreach(var l_Update in this.Update)
			{
				l_Update.Analyse_Phase1();
			}

			foreach(var l_Command in this.Command)
			{
				l_Command.Analyse_Phase1();
			}

			foreach(var l_Copy in this.Copy)
			{
				l_Copy.Analyse_Phase1();
			}

			foreach(var l_Encapsulation in this.Encapsulation)
			{
				l_Encapsulation.Analyse_Phase1();
			}
        }

		public void Analyse_Phase2()
        {
			foreach(var l_Property in this.Property)
			{
				l_Property.Analyse_Phase2();
			}

			foreach(var l_Collection in this.Collection)
			{
				l_Collection.Analyse_Phase2();
			}

			foreach(var l_Calculation in this.Calculation)
			{
				l_Calculation.Analyse_Phase2();
			}

			foreach(var l_Update in this.Update)
			{
				l_Update.Analyse_Phase2();
			}

			foreach(var l_Command in this.Command)
			{
				l_Command.Analyse_Phase2();
			}

			foreach(var l_Copy in this.Copy)
			{
				l_Copy.Analyse_Phase2();
			}

			foreach(var l_Encapsulation in this.Encapsulation)
			{
				l_Encapsulation.Analyse_Phase2();
			}
        }

		public void Analyse_Phase3()
        {
			foreach(var l_Property in this.Property)
			{
				l_Property.Analyse_Phase3();
			}

			foreach(var l_Collection in this.Collection)
			{
				l_Collection.Analyse_Phase3();
			}

			foreach(var l_Calculation in this.Calculation)
			{
				l_Calculation.Analyse_Phase3();
			}

			foreach(var l_Update in this.Update)
			{
				l_Update.Analyse_Phase3();
			}

			foreach(var l_Command in this.Command)
			{
				l_Command.Analyse_Phase3();
			}

			foreach(var l_Copy in this.Copy)
			{
				l_Copy.Analyse_Phase3();
			}

			foreach(var l_Encapsulation in this.Encapsulation)
			{
				l_Encapsulation.Analyse_Phase3();
			}
        }

		public void Analyse()
        {
			Analyse_Phase1();
			Analyse_Phase2();
			Analyse_Phase3();
        }

		public void Write()
        {
			WriteConstructor();

			if(this.Property.Count>0)
            {
				using(RegionBlock("Propriétés"))
				{
					// Brassage de toutes les propriétés.
					foreach(var l_Property in this.Property)
					{
						l_Property.Write();
					}
				}

				WriteCodeLine();
            }

			if(this.Collection.Count>0)
            {
				using(RegionBlock("Spécificitées liées aux collections"))
				{
					WriteComment("Notez que les propriétés de collection sont implémentées dans la région 'Propriétés'.");
					WriteComment("Vous ne trouverez ci-après que le code spécifique.");

					WriteCodeLine();

					// Brassage de toutes les propriétés.
					foreach(var l_Collection in this.Collection)
					{
						l_Collection.Write();
					}
				}

				WriteCodeLine();
            }

			if(this.Calculation.Count>0)
            {
				using(RegionBlock("Spécificitées liées aux propriétés calculées"))
				{
					WriteComment("Notez que les propriétés calculées sont implémentées dans la région 'Propriétés'.");
					WriteComment("Vous ne trouverez ci-après que le code spécifique.");

					WriteCodeLine();

					// Brassage de toutes les propriétés.
					foreach(var l_Calculation in this.Calculation)
					{
						l_Calculation.Write();
					}
				}

				WriteCodeLine();
            }

			if(this.Update.Count>0)
            {
				using(RegionBlock("Spécificitées liées aux mises à jour (Update)"))
				{
					// Brassage de toutes les propriétés.
					foreach(var l_Update in this.Update)
					{
						l_Update.Write();
					}
				}

				WriteCodeLine();
            }

			if(this.Command.Count>0)
            {
				using(RegionBlock("Spécificitées liées aux commandes"))
				{
					WriteComment("Notez que les propriétés de commandes sont implémentées dans la région 'Propriétés'.");
					WriteComment("Vous ne trouverez ci-après que le code spécifique.");

					WriteCodeLine();

					// Brassage de toutes les propriétés.
					foreach(var l_Command in this.Command)
					{
						l_Command.Write();
					}
				}

				WriteCodeLine();
            }

			if(this.Copy.Count>0)
            {
				using(RegionBlock("Spécificitées liées aux copies"))
				{
					WriteCodeLine();

					foreach(var l_Copy in this.Copy)
					{
						l_Copy.Write();
					}
				}

				WriteCodeLine();
            }

			if(this.Encapsulation.Count>0)
            {
				using(RegionBlock("Spécificitées liées aux encapsulations"))
				{
					WriteCodeLine();

					foreach(var l_Encapsulation in this.Encapsulation)
					{
						l_Encapsulation.Write();
					}
				}
            }
        }
    }	
#>