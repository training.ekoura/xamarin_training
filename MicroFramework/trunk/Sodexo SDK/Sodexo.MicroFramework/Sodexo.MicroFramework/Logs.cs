﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sodexo
{
    public class Logs
    {
        private static object _Locker = new object();

        private static List<string> _Logs = new List<string>();

        public static void Secure(string p_Info, Action p_Action)
        {
            lock(_Locker)
            {
#if DEBUG
                try
                {
                    p_Action();
                }
                catch(Exception l_Exception)
                {
                    _Logs.Add(string.Format("{0} : {1}", p_Info, l_Exception.Message));
                    throw;
                }
#else
                p_Action();
#endif
            }
        }

        public static string[] Errors
        {
            get { return _Logs.ToArray(); }
        }
    }
}
