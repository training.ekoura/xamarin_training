﻿using System;
namespace Sodexo
{
    public interface IDelegateCommand<TArgument> : IDelegateCommand
    {
        TArgument Argument { get; set; }
    }
}
