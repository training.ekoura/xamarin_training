﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Sodexo
{

    public class ImageCache
    {

        // Cache of recently used images
        static List<string> ImagesCached;
        static bool isInitialize = false;
        private static IFileService _FileService = Sodexo.Framework.Services.Container().Resolve<IFileService>();

        public ImageCache()
        {          
            if (!isInitialize)
            {
                System.Diagnostics.Debug.WriteLine("deb initialize");
                ImagesCached = _FileService.GetFilesInPath().ToList();
                isInitialize = true;
                System.Diagnostics.Debug.WriteLine("fin initialize");
                System.Diagnostics.Debug.WriteLine(ImagesCached.ToString());
            }
        }

        public bool AddImage(MemoryStream image, string uri)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("ADD" + uri);
                Uri _myUri = new Uri(uri);
                _FileService.SaveImage(image, _myUri.Segments.Last());
                ImagesCached.Add(_myUri.Segments.Last());
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public string GetImagePath(string uri)
        {
            Uri _myUri = new Uri(uri);
            return _FileService.GetFileWPath(_myUri.Segments.Last());
        }

        public MemoryStream GetImage(string uri)
        {
            System.Diagnostics.Debug.WriteLine("GET" + uri);
            Uri _myUri = new Uri(uri);
            return _FileService.LoadImage(_myUri.Segments.Last());
        }

        public bool IsCached(string uri)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Cache" + uri);
                Uri _myUri = new Uri(uri);
                if (ImagesCached.Where(x => x.Split('\\').Last() == _myUri.Segments.Last()).FirstOrDefault() != null)
                {
                    System.Diagnostics.Debug.WriteLine("Cache true");
                    return true;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Cache false");
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
