﻿using System;
namespace Sodexo
{
    public interface IAssemblyService
    {
        System.Reflection.Assembly[] Assemblies { get; }
        System.IO.Stream GetResourceStream(string p_Name);
        void RegisterAssembly(System.Reflection.Assembly p_Assembly);
    }
}
