﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace Sodexo
//{
//    public class JobService
//    {
//        private class JobInfo
//        {
//            public Job Job { get; set; }
//            public Action Action { get; set; }
//            public DateTime NextDateTime { get; set; }
//        }

//        private object _Locker = new object();

//        private List<JobInfo> _Jobs = new List<JobInfo>();

//        private Task _Task;

//        private CancellationTokenSource _TaskCancellationTokenSource;

//        public JobService()
//        {
//            _TaskCancellationTokenSource = new CancellationTokenSource();
//            _Task = new Task(Run, _TaskCancellationTokenSource.Token);
//            _Task.Start();
//        }

//        public Job Run(Action<Job> p_Action, TimeSpan p_Delay = default(TimeSpan), string p_Caption = null, string p_Message = null)
//        {
//            var l_Job = new Job()
//            {
//                IsRunning = false,
//                Caption = p_Caption,
//                Message = p_Message,
//                Progess = 0
//            };

//            var l_JobInfo = (new JobInfo() { Job = l_Job, Action = () => p_Action(l_Job) };

//            lock(_Locker)
//            {
//                _Jobs.Add(l_JobInfo);
//            }
//            Update(p_Delay);

//            var l_Scheduler = TaskScheduler.FromCurrentSynchronizationContext();

//            Task
//                .Delay(p_Delay)
//                .ContinueWith((t) => p_Action(l_Job), l_Scheduler)
//                .ContinueWith((t) =>
//                {
//                    lock(_Locker)
//                    {
//                        _Jobs.Remove(l_JobInfo);
//                    }
//                })
//                .Start();
                
//            return l_Job;
//        }

//        public RepeatedJob RunRepeated(Action<RepeatedJob> p_Action, TimeSpan p_Delay = default(TimeSpan), TimeSpan p_Interval = default(TimeSpan), string p_Caption = null, string p_Message = null)
//        {
//            var l_Job = new RepeatedJob()
//            {
//                IsRunning = false,
//                Caption = p_Caption,
//                Message = p_Message,
//                Progess = 0,
//                Interval = p_Interval
//            };

//            var l_JobInfo = (new JobInfo() { Job = l_Job, Action = () => p_Action(l_Job) };

//            lock(_Locker)
//            {
//                _Jobs.Add(l_JobInfo);
//            }
//            Update(p_Delay);

//            var l_Scheduler = TaskScheduler.FromCurrentSynchronizationContext();

//            Task
//                .Delay(p_Delay)
//                .ContinueWith((t) => p_Action(l_Job), l_Scheduler)
//                .ContinueWith((t)=>
//                {

//                })
//                .ContinueWith((t) =>
//                {
//                    lock(_Locker)
//                    {
//                        _Jobs.Remove(l_JobInfo);
//                    }
//                })
//                .Start();
                
//            return l_Job;
//        }

//        private void Update(TimeSpan p_Delay = default(TimeSpan))
//        {
//            lock (_Locker)
//            {
//            }
//        }

//        private void Run()
//        {
//            while(!_TaskCancellationTokenSource.IsCancellationRequested)
//            {
//                var l_Now = DateTime.Now;
//                var l_Next = default(JobInfo);
//                lock(_Locker)
//                {
//                    l_Next = (from j in _Jobs orderby j.NextDateTime where j.NextDateTime < l_Now select j).FirstOrDefault();
//                }
//                _Task.
//            }
//        }
//    }

//    public class Job : ObservableObject
//    {
//        private bool _IsRunning;
//        public bool IsRunning { get { return GetValue<bool>(() => _IsRunning); } set { SetValue<bool>(() => _IsRunning, (v) => _IsRunning = v, value, "IsRunning"); } }

//        private string _Caption;
//        public string Caption { get { return GetValue<string>(() => _Caption); } set { SetValue<string>(() => _Caption, (v) => _Caption = v, value, "Caption"); } }

//        private string _Message;
//        public string Message { get { return GetValue<string>(() => _Message); } set { SetValue<string>(() => _Message, (v) => _Message = v, value, "Message"); } }

//        private double _Progess;
//        public double Progess { get { return GetValue<double>(() => _Progess); } set { SetValue<double>(() => _Progess, (v) => _Progess = v, value, "Progess"); } }

//        public void Remove();
//    }

//    public class RepeatedJob : Job
//    {
//        private TimeSpan _Interval;
//        public TimeSpan Interval { get { return GetValue<TimeSpan>(() => _Interval); } set { SetValue<TimeSpan>(() => _Interval, (v) => _Interval = v, value, "Interval"); } }

//        public void Pause()
//        {

//        }

//        public void Resume()
//        {

//        }
//    }
//}
