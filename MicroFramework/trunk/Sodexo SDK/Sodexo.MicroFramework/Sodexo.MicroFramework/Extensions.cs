﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sodexo
{
    public static class Extensions
    {
        private static Lazy<Sodexo.IContainer> _Container = new Lazy<Sodexo.IContainer>(()=>new Sodexo.Container(), true);

        /// <summary>
        /// Retourne le conteneur d'IOC.
        /// </summary>
        public static Sodexo.IContainer Container(this Sodexo.IService p_Services)
        {
            return _Container.Value;
        }

        /// <summary>
        /// Le service de gestion des thèmes.
        /// </summary>
        public static Sodexo.IApplicationStateService ApplicationStateService(this Sodexo.IService p_Services)
        {
            return p_Services.Container().Resolve<Sodexo.IApplicationStateService>();
        }

        /// <summary>
        /// Le service de gestion des assemblies.
        /// </summary>
        public static Sodexo.IAssemblyService AssemblyService(this Sodexo.IService p_Services)
        {
            return p_Services.Container().Resolve<Sodexo.IAssemblyService>();
        }

        /// <summary>
        /// Retourne le service d'intéractions.
        /// </summary>
        public static Sodexo.IInteractionService InteractionService(this Sodexo.IService p_Services)
        {
            return p_Services.Container().Resolve<Sodexo.IInteractionService>();
        }

        /// <summary>
        /// Retourne le service Http.
        /// </summary>
        public static Sodexo.IHttpService HttpService(this Sodexo.IService p_Services)
        {
            return p_Services.Container().Resolve<Sodexo.IHttpService>();
        }

        /// <summary>
        /// Retourne le service Http.
        /// </summary>
        public static Sodexo.IRemoteCallService RemoteCallService(this Sodexo.IService p_Services)
        {
            return p_Services.Container().Resolve<Sodexo.IRemoteCallService>();
        }

        /// <summary>
        /// Retourne le service de fichiers.
        /// </summary>
        public static Sodexo.IFileService FileService(this Sodexo.IService p_Services, bool p_ThrowExceptionIfNotFound = true)
        {
            return p_Services.Container().Resolve<Sodexo.IFileService>(p_ThrowExceptionIfNotFound: p_ThrowExceptionIfNotFound);
        }

        /// <summary>
        /// Retourne le service de sirialisation.
        /// </summary>
        public static Sodexo.ISerializerService SerializerService(this Sodexo.IService p_Services)
        {
            return p_Services.Container().Resolve<Sodexo.ISerializerService>();
        }

        /// <summary>
        /// Le service de gestion des thèmes.
        /// </summary>
        public static Sodexo.Themes.IThemeService ThemeService(this Sodexo.IService p_Services)
        {
            return p_Services.Container().Resolve<Sodexo.Themes.IThemeService>();
        }

        /// <summary>
        /// Service de cryptographie
        /// </summary>
        /// <param name="p_Services"></param>
        /// <returns></returns>
        public static Sodexo.ICryptography CryptoGraphyService(this Sodexo.IService p_Services)
        {
            return p_Services.Container().Resolve<Sodexo.ICryptography>();
        }




        /// <summary>
        /// Ajoute les éléments à la collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p_Collection"></param>
        /// <param name="p_Items"></param>
        public static void AddRange<T>(this System.Collections.ObjectModel.ObservableCollection<T> p_Collection, IEnumerable<T> p_Items)
        {
            if(p_Items!=null)
            {
                foreach(var l_Item in p_Items)
                {
                    p_Collection.Add(l_Item);
                }
            }
        }


        public static void Each<T>(this IEnumerable<T> p_Enum, Action<T> p_Action)
        {
            p_Enum.Each((i, item) => p_Action(item));
        }

        public static void Each<T>(this IEnumerable<T> p_Enum, Action<int, T> p_Action)
        {
            if (p_Action == null) return;
            var l_Index = 0;
            foreach (var l_Item in p_Enum)
            {
                p_Action(l_Index, l_Item);
                l_Index++;
            }
        }

        public static Memento Memento(this object p_Object)
        {
            var l_Result = new Memento();
            l_Result.Save(p_Object);
            return l_Result;
        }


        public static Exception Deepest(this Exception p_Exception)
        {
            if (p_Exception == null) return null;
            while (true)
            {
                if (p_Exception.InnerException == null) return p_Exception;
                p_Exception = p_Exception.InnerException;
            }
        }
    }
}
