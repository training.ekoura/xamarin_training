﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sodexo
{
    public class ObservableObjectDataErrorInfo : IObservableObjectDataErrorInfo
    {
        public ObservableObjectDataErrorInfo(string p_PropertyName, string p_Message)
        {
            _PropertyName = p_PropertyName;
            _Message = p_Message;
        }

        private string _PropertyName;
        public string PropertyName
        {
            get { return _PropertyName; }
        }

        private string _Message;
        public string Message
        {
            get { return _Message; }
        }

        public override string ToString()
        {
            return _Message;
        }
    }
}
