﻿using System;
using System.IO;
namespace Sodexo
{
    public interface IHttpService
    {
        System.Threading.Tasks.Task<Stream> Get(string l_Uri);
    }
}
