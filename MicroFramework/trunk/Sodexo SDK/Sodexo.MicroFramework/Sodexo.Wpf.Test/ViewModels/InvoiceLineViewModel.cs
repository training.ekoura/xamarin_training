﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML InvoiceLineViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  Sodexo.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;

namespace Sodexo.Wpf.Test.ViewModels
{
	public abstract partial class InvoiceLineViewModelBase : Sodexo.ViewModel
	{
		protected override void OnInitialize()
		{
			base.OnInitialize();

			// Initialisation de la propriété VatRate avec sa valeur par défaut.
			VatRate = 20m;
		}

		#region === Propriétés ===

			#region === Propriété : LineIndex ===

				public const string LineIndex_PROPERTYNAME = "LineIndex";

				private int _LineIndex;
				///<summary>
				/// Propriété : LineIndex
				///</summary>
				public int LineIndex
				{
					get
					{
						return GetValue<int>(() => _LineIndex);
					}
					set
					{
						SetValue<int>(() => _LineIndex, (v) => _LineIndex = v, value, LineIndex_PROPERTYNAME,  DoLineIndexBeforeSet, DoLineIndexAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLineIndexBeforeSet(string p_PropertyName, int p_OldValue, int p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLineIndexAfterSet(string p_PropertyName, int p_OldValue, int p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLineIndexDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Description ===

				public const string Description_PROPERTYNAME = "Description";

				private string _Description;
				///<summary>
				/// Propriété : Description
				///</summary>
				public string Description
				{
					get
					{
						return GetValue<string>(() => _Description);
					}
					set
					{
						SetValue<string>(() => _Description, (v) => _Description = v, value, Description_PROPERTYNAME,  DoDescriptionBeforeSet, DoDescriptionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDescriptionBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDescriptionAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDescriptionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Quantity ===

				public const string Quantity_PROPERTYNAME = "Quantity";

				private decimal _Quantity;
				///<summary>
				/// Propriété : Quantity
				///</summary>
				public decimal Quantity
				{
					get
					{
						return GetValue<decimal>(() => _Quantity);
					}
					set
					{
						SetValue<decimal>(() => _Quantity, (v) => _Quantity = v, value, Quantity_PROPERTYNAME,  DoQuantityBeforeSet, DoQuantityAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoQuantityBeforeSet(string p_PropertyName, decimal p_OldValue, decimal p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoQuantityAfterSet(string p_PropertyName, decimal p_OldValue, decimal p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyQuantityDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UnitPrice ===

				public const string UnitPrice_PROPERTYNAME = "UnitPrice";

				private decimal _UnitPrice;
				///<summary>
				/// Propriété : UnitPrice
				///</summary>
				public decimal UnitPrice
				{
					get
					{
						return GetValue<decimal>(() => _UnitPrice);
					}
					set
					{
						SetValue<decimal>(() => _UnitPrice, (v) => _UnitPrice = v, value, UnitPrice_PROPERTYNAME,  DoUnitPriceBeforeSet, DoUnitPriceAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUnitPriceBeforeSet(string p_PropertyName, decimal p_OldValue, decimal p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUnitPriceAfterSet(string p_PropertyName, decimal p_OldValue, decimal p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUnitPriceDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : VatRate ===

				public const string VatRate_PROPERTYNAME = "VatRate";

				private decimal _VatRate;
				///<summary>
				/// Propriété : VatRate
				///</summary>
				public decimal VatRate
				{
					get
					{
						return GetValue<decimal>(() => _VatRate);
					}
					set
					{
						SetValue<decimal>(() => _VatRate, (v) => _VatRate = v, value, VatRate_PROPERTYNAME,  DoVatRateBeforeSet, DoVatRateAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoVatRateBeforeSet(string p_PropertyName, decimal p_OldValue, decimal p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoVatRateAfterSet(string p_PropertyName, decimal p_OldValue, decimal p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyVatRateDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Amount ===

				public const string Amount_PROPERTYNAME = "Amount";

				private decimal _Amount;
				///<summary>
				/// Propriété : Amount
				///</summary>
				public decimal Amount
				{
					get
					{
						return GetValue<decimal>(() => _Amount);
					}
					set
					{
						SetValue<decimal>(() => _Amount, (v) => _Amount = v, value, Amount_PROPERTYNAME,  DoAmountBeforeSet, DoAmountAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoAmountBeforeSet(string p_PropertyName, decimal p_OldValue, decimal p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoAmountAfterSet(string p_PropertyName, decimal p_OldValue, decimal p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyAmountDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

	}

	public partial class InvoiceLineViewModel : InvoiceLineViewModelBase
	{
		///<summary>
		/// Le créateur nécessite un maître.
		///</summary>
		public InvoiceLineViewModel(InvoiceViewModel p_Master)
		{
			Master = p_Master;
		}

		public new InvoiceViewModel Master
		{
			get
			{
				return (InvoiceViewModel)base.Master;
			}
			set
			{
				base.Master = value;
			}
		}

	}
}
