﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace Sodexo.Controls
{
    public class AlternateBurger : ContentView
    {
        public AlternateBurger()
        {
        }

        #region === ItemTemplate ===

        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create<AlternateBurger, DataTemplate>(p => p.ItemTemplate, default(DataTemplate), propertyChanged: (d, o, n) => (d as AlternateBurger).ItemTemplate_Changed(o, n));

        private void ItemTemplate_Changed(DataTemplate oldvalue, DataTemplate newvalue)
        {
            if (newvalue == null)
                return;

            ItemTemplate = newvalue;
            Content= ItemTemplate.CreateContent() as View;
        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        #endregion

        private new View Content { get { return base.Content; } set { base.Content = value; } }

        /// <summary>
        /// Cette méthode est utilisée pour binder une propriété d'un objet du modèle à une propriété du AlternateBurger.
        /// </summary>
        /// <param name="p_Object"></param>
        /// <param name="p_Property"></param>
        /// <param name="p_Path"></param>
        private void _SetTemplateBinding(BindableObject p_Object, BindableProperty p_Property, string p_Path)
        {
            p_Object.SetBinding(p_Property, new Binding(p_Path, BindingMode.OneWay, null, null, null, this));
        }

        private void _ApplyTheme()
        {
            //switch ((string)ThemeParameter)
            //{
            //    case ThemeNameForCarousel.MySodexo:
            //        themeExtension = "";
            //        break;
            //    case ThemeNameForCarousel.InspirationEntreprise:
            //        themeExtension = "Insp";
            //        break;
            //    case ThemeNameForCarousel.InspirationScolaire:
            //        themeExtension = "InspEdu";
            //        break;
            //    case ThemeNameForCarousel.MySodexo360:
            //        themeExtension = "360";
            //        break;
            //}
        }
    }
}

