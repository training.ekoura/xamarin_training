﻿using System;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace Sodexo.Controls
{
    public class CustomEditor : Editor 
    {
        public CustomEditor()
        {

        }
        public static readonly BindableProperty FontFamilyProperty = BindableProperty.Create<CustomEditor, string>(p => p.FontFamily, Font.Default.FontFamily);
        public static readonly BindableProperty FontSizeProperty = BindableProperty.Create<CustomEditor, double>(p => p.FontSize, Font.Default.FontSize);
        public static readonly BindableProperty TextColorProperty = BindableProperty.Create<CustomEditor, Color>(p => p.TextColor, Color.Default);
        public static readonly BindableProperty PlaceHolderTextColorProperty = BindableProperty.Create<CustomEditor, Color>(p => p.TextColor, Color.Default);
        public static readonly BindableProperty PlaceHolderProperty = BindableProperty.Create<CustomEditor, string>(p => p.PlaceHolder, null);

        public string FontFamily
        {
            get { return (string)GetValue(FontFamilyProperty); }
            set { SetValue(FontFamilyProperty, value); }
        }
        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }
        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }
        public string PlaceHolder
        {
            get { return (string)GetValue(PlaceHolderProperty); }
            set { SetValue(PlaceHolderProperty, value); }
        }

        public Color PlaceHolderTextColor
        {
            get { return (Color)GetValue(PlaceHolderTextColorProperty); }
            set { SetValue(PlaceHolderTextColorProperty, value); }
        }
    }
}