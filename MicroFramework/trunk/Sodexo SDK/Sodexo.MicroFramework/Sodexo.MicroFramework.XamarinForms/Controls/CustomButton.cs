﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sodexo.Controls
{
    public class CustomButton : ContentView
    {
        private StackLayout _RootStackLayout;
        private Grid gridForImage;
        private Image buttonImage;
        private Label buttonLabel;
        private ExtendedLabel buttonExtendedLabel;
        private RoundedButton roundedButton;


        public enum ButtonTypes
        {
            Burger,
            Commande,
            Fixed,
            FixedWithImage,
            Rounded
        }

        public enum ViewSenderTypes
        {
            SecondBurger,
            Default
        }

        public enum ImageForButton
        {
            ImageOnLeft,
            ImageOnRight,
            None
        }


        public CustomButton()
        {

        }


        #region === ViewSender ===

        public static readonly BindableProperty ViewSenderProperty = BindableProperty.Create<CustomButton, ViewSenderTypes>(p => p.ViewSender, ViewSenderTypes.Default, propertyChanged: (d, o, n) => (d as CustomButton).ViewSender_Changed(o, n));

        private void ViewSender_Changed(ViewSenderTypes oldvalue, ViewSenderTypes newvalue)
        {

        }

        public ViewSenderTypes ViewSender
        {
            get { return (ViewSenderTypes)GetValue(ViewSenderProperty); }
            set { SetValue(ViewSenderProperty, value); }
        }

        #endregion

        #region === ItemTemplate ===

        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create<CustomButton, DataTemplate>(p => p.ItemTemplate, null, propertyChanged: (d, o, n) => (d as CustomButton).ItemTemplate_Changed(o, n));

        // J'ai commenté le build ne voyant pas son utilité pour le moment.
        private void ItemTemplate_Changed(DataTemplate oldvalue, DataTemplate newvalue)
        {
            //Build();
        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        #endregion

        #region === TapCommand ===

        public static readonly BindableProperty TapCommandProperty = BindableProperty.Create<CustomButton, ICommand>(p => p.TapCommand, default(ICommand), propertyChanged: (d, o, n) => (d as CustomButton).TapCommand_Changed(o, n));

        private void TapCommand_Changed(ICommand oldvalue, ICommand newvalue)
        {
        }

        public ICommand TapCommand
        {
            get { return (ICommand)GetValue(TapCommandProperty); }
            set { SetValue(TapCommandProperty, value); }
        }

        #endregion

        #region === TapCommandParameter ===

        public static readonly BindableProperty TapCommandParameterProperty = BindableProperty.Create<CustomButton, object>(p => p.TapCommandParameter, default(object), propertyChanged: (d, o, n) => (d as CustomButton).TapCommandParameter_Changed(o, n));

        private void TapCommandParameter_Changed(object oldvalue, object newvalue)
        {

        }

        public object TapCommandParameter
        {
            get { return (object)GetValue(TapCommandParameterProperty); }
            set { SetValue(TapCommandParameterProperty, value); }
        }

        #endregion

        #region === ButtonType ===

        public static readonly BindableProperty ButtonTypeProperty = BindableProperty.Create<CustomButton, ButtonTypes>(p => p.ButtonType, ButtonTypes.Fixed, propertyChanged: (d, o, n) => (d as CustomButton).ButtonType_Changed(o, n));

        /// <summary>
        /// Pour mettre à jour les bonnes couleurs des puces.
        /// </summary>
        /// <param name="oldvalue"></param>
        /// <param name="newvalue"></param>
        private void ButtonType_Changed(ButtonTypes oldvalue, ButtonTypes newvalue)
        {
            
        }

        public ButtonTypes ButtonType
        {
            get { return (ButtonTypes)GetValue(ButtonTypeProperty); }
            set { SetValue(ButtonTypeProperty, value); }
        }

        #endregion

        #region === ButtonLabel ===

        public static readonly BindableProperty ButtonLabelProperty = BindableProperty.Create<CustomButton, string>(p => p.ButtonLabel, default(string), propertyChanged: (d, o, n) => (d as CustomButton).ButtonLabel_Changed(o, n));

        /// <summary>
        /// Pour mettre à jour les bonnes couleurs des puces.
        /// </summary>
        /// <param name="oldvalue"></param>
        /// <param name="newvalue"></param>
        private void ButtonLabel_Changed(string oldvalue, string newvalue)
        {
            Rebuild();
        }

        public string ButtonLabel
        {
            get { return (string)GetValue(ButtonLabelProperty); }
            set { SetValue(ButtonLabelProperty, value); }
        }

        #endregion

        #region === ButtonColor ===

        public static readonly BindableProperty ButtonColorProperty = BindableProperty.Create<CustomButton, Color>(p => p.ButtonColor, default(Color), propertyChanged: (d, o, n) => (d as CustomButton).ButtonColor_Changed(o, n));

        /// <summary>
        /// Pour mettre à jour les bonnes couleurs des puces.
        /// </summary>
        /// <param name="oldvalue"></param>
        /// <param name="newvalue"></param>
        private void ButtonColor_Changed(Color oldvalue, Color newvalue)
        {
            
        }

        [TypeConverter(typeof(ColorTypeConverter))]
        public Color ButtonColor
        {
            get { return (Color)GetValue(ButtonColorProperty); }
            set { SetValue(ButtonColorProperty, value); }
        }

        #endregion

        #region === ImageButton ===

        public static readonly BindableProperty ImageButtonProperty = BindableProperty.Create<CustomButton, ImageForButton>(p => p.ImageButton, ImageForButton.None, propertyChanged: (d, o, n) => (d as CustomButton).ImageButton_Changed(o, n));

        /// <summary>Indique si une image doit être inclue dans le bouton.</summary>
        /// <param name="value">La valeur de l'énumération ImageButton.</param>
        private void ImageButton_Changed(ImageForButton oldvalue, ImageForButton newvalue)
        {
            
        }

        public ImageForButton ImageButton
        {
            get { return (ImageForButton)GetValue(ImageButtonProperty); }
            set { SetValue(ImageButtonProperty, value); }
        }

        #endregion

        #region === ImageButtonSource ===

        public static readonly BindableProperty ImageButtonSourceProperty = BindableProperty.Create<CustomButton, string>(p => p.ImageButtonSource, default(string), propertyChanged: (d, o, n) => (d as CustomButton).ImageButtonSource_Changed(o, n));

        /// <summary>Indique le chemin de l'image qui doit être inclue dans le bouton.</summary>
        /// <param name="value">Le chemin de l'image.</param>
        private void ImageButtonSource_Changed(string oldvalue, string newvalue)
        {
            
        }

        public string ImageButtonSource
        {
            get { return (string)GetValue(ImageButtonSourceProperty); }
            set { SetValue(ImageButtonSourceProperty, value); }
        }

        #endregion

        #region === HorizontalImageOption ===

        public static readonly BindableProperty HorizontalImageOptionProperty = BindableProperty.Create<CustomButton, LayoutOptions>(p => p.HorizontalImageOption, LayoutOptions.FillAndExpand, propertyChanged: (d, o, n) => (d as CustomButton).HorizontalImageOption_Changed(o, n));

        /// <summary>Alignement horizontal de l'image dans le bouton.</summary>
        /// <param name="oldvalue">Ancienne valeur.</param>
        /// <param name="newvalue">Nouvelle valeur.</param>
        private void HorizontalImageOption_Changed(LayoutOptions oldvalue, LayoutOptions newvalue)
        {
            
        }

        [TypeConverter(typeof(LayoutOptionsConverter))]
        public LayoutOptions HorizontalImageOption
        {
            get { return (LayoutOptions)GetValue(HorizontalImageOptionProperty); }
            set { SetValue(HorizontalImageOptionProperty, value); }
        }

        #endregion

        #region === HorizontalLabelOption ===

        public static readonly BindableProperty HorizontalLabelOptionProperty = BindableProperty.Create<CustomButton, LayoutOptions>(p => p.HorizontalLabelOption, LayoutOptions.CenterAndExpand, propertyChanged: (d, o, n) => (d as CustomButton).HorizontalLabelOption_Changed(o, n));

        /// <summary>Alignement horizontal du texte du bouton.</summary>
        /// <param name="oldvalue">Ancienne valeur.</param>
        /// <param name="newvalue">Nouvelle valeur.</param>
        private void HorizontalLabelOption_Changed(LayoutOptions oldvalue, LayoutOptions newvalue)
        {
            
        }

        [TypeConverter(typeof(LayoutOptionsConverter))]
        public LayoutOptions HorizontalLabelOption
        {
            get { return (LayoutOptions)GetValue(HorizontalLabelOptionProperty); }
            set { SetValue(HorizontalLabelOptionProperty, value); }
        }

        #endregion

        #region === VerticalImageOption ===

        public static readonly BindableProperty VerticalImageOptionProperty = BindableProperty.Create<CustomButton, LayoutOptions>(p => p.VerticalImageOption, LayoutOptions.FillAndExpand, propertyChanged: (d, o, n) => (d as CustomButton).VerticalImageOption_Changed(o, n));

        /// <summary>Alignement vertical de l'image dans le bouton.</summary>
        /// <param name="oldvalue">Ancienne valeur.</param>
        /// <param name="newvalue">Nouvelle valeur.</param>
        private void VerticalImageOption_Changed(LayoutOptions oldvalue, LayoutOptions newvalue)
        {
            
        }

        [TypeConverter(typeof(LayoutOptionsConverter))]
        public LayoutOptions VerticalImageOption
        {
            get { return (LayoutOptions)GetValue(VerticalImageOptionProperty); }
            set { SetValue(VerticalImageOptionProperty, value); }
        }

        #endregion

        #region === VerticalLabelOption ===

        public static readonly BindableProperty VerticalLabelOptionProperty = BindableProperty.Create<CustomButton, LayoutOptions>(p => p.VerticalLabelOption, LayoutOptions.CenterAndExpand, propertyChanged: (d, o, n) => (d as CustomButton).VerticalLabelOption_Changed(o, n));

        /// <summary>Alignement vertical du texte du bouton.</summary>
        /// <param name="oldvalue">Ancienne valeur.</param>
        /// <param name="newvalue">Nouvelle valeur.</param>
        private void VerticalLabelOption_Changed(LayoutOptions oldvalue, LayoutOptions newvalue)
        {
            
        }

        [TypeConverter(typeof(LayoutOptionsConverter))]
        public LayoutOptions VerticalLabelOption
        {
            get { return (LayoutOptions)GetValue(VerticalLabelOptionProperty); }
            set { SetValue(VerticalLabelOptionProperty, value); }
        }

        #endregion

        #region === CenteredItems ===

        public static readonly BindableProperty CenteredItemsProperty = BindableProperty.Create<CustomButton, bool>(p => p.CenteredItems, default(bool), propertyChanged: (d, o, n) => (d as CustomButton).CenteredItems_Changed(o, n));

        /// <summary>Indique si l'image et le texte sont centrés au milieu du bouton.</summary>
        /// <param name="oldvalue">Ancienne valeur.</param>
        /// <param name="newvalue">Nouvelle valeur.</param>
        private void CenteredItems_Changed(bool oldvalue, bool newvalue)
        {
            
        }

        public bool CenteredItems
        {
            get { return (bool)GetValue(CenteredItemsProperty); }
            set { SetValue(CenteredItemsProperty, value); }
        }

        #endregion

        #region === IsLabelUnderlined ===

        public static readonly BindableProperty IsLabelUnderlinedProperty = BindableProperty.Create<CustomButton, bool>(p => p.IsLabelUnderlined, default(bool), propertyChanged: (d, o, n) => (d as CustomButton).IsLabelUnderlined_Changed(o, n));

        /// <summary>Utilise ou non un texte souligné.</summary>
        /// <param name="oldvalue">Ancienne valeur.</param>
        /// <param name="newvalue">Nouvelle valeur.</param>
        private void IsLabelUnderlined_Changed(bool oldvalue, bool newvalue)
        {
            
        }

        public bool IsLabelUnderlined
        {
            get { return (bool)GetValue(IsLabelUnderlinedProperty); }
            set { SetValue(IsLabelUnderlinedProperty, value); }
        }

        #endregion


        private void Rebuild()
        {
            var dataTemplate = new DataTemplate(() =>
            {
                _RootStackLayout = new StackLayout() { VerticalOptions = LayoutOptions.FillAndExpand, Padding = new Thickness(10,0) };
                _RootStackLayout.Children.Clear();
                //_RootStackLayout.Behaviors.Add(new Behaviors.ViewButtonBehaviour());

                #region ButtonType

                switch (ButtonType)
                {
                    case ButtonTypes.Burger:
                        _RootStackLayout.SetDynamicResource(StackLayout.HeightRequestProperty, "BUTTON_BURGER_HEIGHT");
                        _RootStackLayout.SetDynamicResource(StackLayout.StyleProperty, "AltHamburgerButtonStyle");
                        break;
                    case ButtonTypes.Commande:
                        _RootStackLayout.VerticalOptions = LayoutOptions.FillAndExpand;
                        _RootStackLayout.SetDynamicResource(StackLayout.StyleProperty, "FixedButtonColorStyle");
                        break;
                    case ButtonTypes.Fixed:
                        _RootStackLayout.SetDynamicResource(StackLayout.HeightRequestProperty, "BUTTON_FIXED_HEIGHT");
                        _RootStackLayout.SetDynamicResource(StackLayout.StyleProperty, "FixedButtonColorStyle");
                        break;
                    default:
                        _RootStackLayout.SetDynamicResource(StackLayout.HeightRequestProperty, "BUTTON_FIXED_HEIGHT");
                        _RootStackLayout.SetDynamicResource(StackLayout.StyleProperty, "FixedButtonColorStyle");
                        break;
                }

                #endregion

                #region ButtonLabel / ButtonExtendedLabel

                if(IsLabelUnderlined)
                {
                    buttonExtendedLabel = new ExtendedLabel() { Text = ButtonLabel, HorizontalOptions = HorizontalLabelOption, VerticalOptions = VerticalLabelOption };
                    buttonExtendedLabel.SetDynamicResource(ExtendedLabel.StyleProperty, "BUTTON_EXTENDEDLABEL_STYLE");
                }
                else
                {
                    buttonLabel = new Label() { Text = ButtonLabel, HorizontalOptions = HorizontalLabelOption, VerticalOptions = VerticalLabelOption };
                    buttonLabel.SetDynamicResource(Label.StyleProperty, "BUTTON_LABEL_STYLE");
                }

                #endregion

                if(ButtonType == ButtonTypes.FixedWithImage || ButtonType == ButtonTypes.Rounded)
                {
                    #region Image

                    buttonImage = new Image() { HorizontalOptions = HorizontalImageOption, VerticalOptions = VerticalImageOption };
                    buttonImage.Source = ImageButton != ImageForButton.None ? FileImageSource.FromResource(ImageButtonSource) : "";

                    #endregion

                    // On prépare une grid pour l'image et le texte du bouton
                    gridForImage = new Grid();
                    gridForImage.VerticalOptions = LayoutOptions.FillAndExpand;

                    #region RoundedButton / FixedWithImage

                    if (ButtonType == ButtonTypes.Rounded)
                    {
                        #region RootGrid

                        var rootGrid = new Grid()
                        {
                            Padding=0,
                            ColumnSpacing=0,
                            BackgroundColor = ButtonColor,
                            VerticalOptions = LayoutOptions.Fill,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            ColumnDefinitions =  
                            { 
                                new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
                                new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) }
                            },
                        };

                        #endregion

                        #region RoundedButton

                        var roundedButton = new RoundedButton()
                        {
                            TextColor = Color.White,
                            BackgroundColor = Color.FromHex("#C10418"),
                            BorderColor = Color.Transparent,
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            HorizontalOptions = LayoutOptions.FillAndExpand
                        };
                        roundedButton.SetDynamicResource(RoundedButton.StyleProperty, "RoundedButtonStyle");

                        Grid.SetColumn(roundedButton, 0);
                        Grid.SetColumnSpan(roundedButton, 2);
                        rootGrid.Children.Add(roundedButton);

                        #endregion

                        #region LabelText

                        var label = new Label()
                        {
                            TextColor=Color.White,
                            VerticalOptions=LayoutOptions.FillAndExpand,
                            HorizontalOptions=LayoutOptions.FillAndExpand,
                            VerticalTextAlignment=TextAlignment.Center,
                            HorizontalTextAlignment=TextAlignment.Start
                        };
                        label.Text = ButtonLabel;

                        var labelStack = new StackLayout()
                        {
                            BackgroundColor = Color.Transparent,
                            Padding=new Thickness(40,0,0,0),
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Children = { label }
                        };

                        Grid.SetColumn(labelStack, 0);
                        Grid.SetColumnSpan(labelStack, 2);
                        rootGrid.Children.Add(labelStack);

                        #endregion

                        #region ArrowImage

                        var arrowImage = new Image()
                        {
                            BackgroundColor=Color.Transparent,
                            VerticalOptions=LayoutOptions.CenterAndExpand,
                            HorizontalOptions = LayoutOptions.CenterAndExpand,
                            Aspect=Aspect.AspectFit,
                            Source=ImageSource.FromResource("Sodexo.Images.Gerant.arrow.png")
                        };

                        Grid.SetColumn(arrowImage, 1);
                        rootGrid.Children.Add(arrowImage);

                        #endregion

                        #region TransparentCover

                        var transparentGrid = new Grid()
                        {
                            BackgroundColor = Color.Transparent,
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                        };

                        Grid.SetColumn(transparentGrid, 0);
                        Grid.SetColumnSpan(transparentGrid, 2);
                        rootGrid.Children.Add(transparentGrid);

                        #endregion

                        #region GridTapGesture

                        var GridTapGesture = new TapGestureRecognizer();
                        GridTapGesture.Tapped += async (object sender, EventArgs e) =>
                        {
                            if (TapCommand != null)
                            {
                                TapCommand.Execute(TapCommandParameter);
                            }
                        };
                        transparentGrid.GestureRecognizers.Add(GridTapGesture);

                        #endregion

                        return rootGrid;
                    }
                    else
                    {
                        #region ImageButton / CenteredItems

                        switch (ImageButton)
                        {
                            #region On utilise une image, placée à gauche du texte

                            case ImageForButton.ImageOnLeft:
                                if (CenteredItems)
                                {
                                    // Le texte et l'image sont centrés dans le bouton
                                    gridForImage.HorizontalOptions = LayoutOptions.Center;
                                    gridForImage.ColumnDefinitions = new ColumnDefinitionCollection
                                    {
                                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
                                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) }
                                    };
                                }
                                else
                                {
                                    // Le texte et l'image ne sont pas centrés dans le bouton
                                    gridForImage.HorizontalOptions = LayoutOptions.FillAndExpand;
                                    gridForImage.ColumnDefinitions = new ColumnDefinitionCollection
                                    {
                                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
                                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) }
                                    };
                                }

                                // Ajout de l'image dans la grid
                                Grid.SetColumn(buttonImage, 0);
                                gridForImage.Children.Add(buttonImage);

                                // Ajout du label, souligné ou non dans la grid
                                if (IsLabelUnderlined)
                                {
                                    Grid.SetColumn(buttonExtendedLabel, 1);
                                    gridForImage.Children.Add(buttonExtendedLabel);
                                }
                                else
                                {
                                    Grid.SetColumn(buttonLabel, 1);
                                    gridForImage.Children.Add(buttonLabel);
                                }

                                // Grid ajouté au RootStackLayout
                                _RootStackLayout.Children.Add(gridForImage);
                                break;

                            #endregion

                            #region On utilise une image, placée à droite du texte

                            case ImageForButton.ImageOnRight:
                                if (CenteredItems)
                                {
                                    // Le texte et l'image sont centrés dans le bouton
                                    gridForImage.HorizontalOptions = LayoutOptions.Center;
                                    gridForImage.ColumnDefinitions = new ColumnDefinitionCollection
                                {
                                    new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
                                    new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) }
                                };
                                }
                                else
                                {
                                    // Le texte et l'image ne sont pas centrés dans le bouton
                                    gridForImage.HorizontalOptions = LayoutOptions.FillAndExpand;
                                    gridForImage.ColumnDefinitions = new ColumnDefinitionCollection
                                {
                                    new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
                                    new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) }
                                };
                                }

                                // Ajout du label, souligné ou non dans la grid
                                if (IsLabelUnderlined)
                                {
                                    Grid.SetColumn(buttonExtendedLabel, 0);
                                    gridForImage.Children.Add(buttonExtendedLabel);
                                }
                                else
                                {
                                    Grid.SetColumn(buttonLabel, 0);
                                    gridForImage.Children.Add(buttonLabel);
                                }

                                // Ajout de l'image dans la grid
                                Grid.SetColumn(buttonImage, 1);
                                gridForImage.Children.Add(buttonImage);

                                // Grid ajouté au RootStackLayout
                                _RootStackLayout.Children.Add(gridForImage);

                                break;
                            #endregion

                            #region Aucune image utilisée

                            case ImageForButton.None:
                                gridForImage = null;
                                _RootStackLayout.Children.Add(buttonLabel);
                                break;

                            default:
                                gridForImage = null;
                                _RootStackLayout.Children.Add(buttonLabel);
                                break;

                            #endregion
                        }

                        #endregion
                    }

                    #endregion
                }
                else
                {
                    _RootStackLayout.Children.Add(buttonLabel);
                }

                #region TapGesture

                var StackTapGesture = new TapGestureRecognizer();
                StackTapGesture.Tapped += async (object sender, EventArgs e) =>
                {
                    if (TapCommand != null)
                    {
                        //if( ViewSender==ViewSenderTypes.SecondBurger)
                        //{
                        //    try
                        //    {
                        //        var a = ((this.Parent as StackLayout) as StackLayout);
                        //        var b = (((a.Parent as StackLayout).Parent as Grid).Parent as AlternateBurger);
                        //        var contentView = b.Parent as TouchContentView;
                        //        var defautGrid = contentView.Parent as Grid;
                        //        await contentView.TranslateTo(480, 0, 100, Easing.Linear);
                        //        defautGrid.Children.Remove(contentView);
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        Sodexo.Framework.Services.InteractionService().Alert("", "ViewSender heirarchy: " + ex.Message);
                        //    }
                        //}
                        TapCommand.Execute(TapCommandParameter);
                    }
                };

                _RootStackLayout.GestureRecognizers.Add(StackTapGesture);

                #endregion

                return _RootStackLayout;
            });

            this.ItemTemplate = dataTemplate;
            Content = ItemTemplate.CreateContent() as View;
        }
    }
}
