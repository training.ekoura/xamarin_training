﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sodexo.Behaviors
{
    /// <summary>
    /// Affiche ou non une View en fonction d'une règle d'états applicatifs.
    /// </summary>
    public class VisibleApplicationStateBehavior : ApplicationStateBehavior<Xamarin.Forms.View>
    {
        protected override void DoUpdate(bool p_RuleIsValide)
        {
            AssociatedObject.IsVisible = p_RuleIsValide;
        }
    }
}
