﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sodexo.Behaviors
{
    public class CheckBoxBehaviour : Xamarin.Forms.Behavior<Image>
    {

        protected override void OnAttachedTo(Image image)
        {
            base.OnAttachedTo(image);
            

            var viewLayoutTapGesture = new TapGestureRecognizer();
            viewLayoutTapGesture.Tapped += async (object sender, EventArgs e) =>
            {
                var source = (image.Source as FileImageSource).File;
                if(source.Contains("Non"))
                    image.SetDynamicResource(Image.SourceProperty, "Checkboxonimagesource");
                else
                    image.SetDynamicResource(Image.SourceProperty, "Checkboxoffimagesource");
            };

            image.GestureRecognizers.Add(viewLayoutTapGesture);
        }

        protected override void OnDetachingFrom(Image image)
        {
            base.OnDetachingFrom(image);
        }
    }
}
