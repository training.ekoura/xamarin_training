﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sodexo.Themes
{
    public interface IThemeServiceXamarin : Sodexo.Themes.IThemeService
    {
        /// <summary>
        /// Enregistre un thème.
        /// </summary>
        /// <param name="p_ThemeName">Nom du thème.</param>
        /// <param name="p_Callback">
        /// Callback qui sera appelé pour définir le thème. 
        /// Le but de ce callback est de définir des ressources.
        /// Chaque thème doit définir les même ressources (Key).
        /// </param>
        void Register(string p_ThemeName, Action<Xamarin.Forms.ResourceDictionary> p_Callback);
    }
}
