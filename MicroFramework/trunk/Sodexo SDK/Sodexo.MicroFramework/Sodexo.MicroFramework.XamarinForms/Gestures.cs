﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sodexo
{
    /// <summary>
    /// Cette classe est utilisée pour contourner l'incapacité de Xamarin de gérer correctement la collection
    /// des gestures. Elle deviendra inutile lorsque ce problème aura disparu.
    /// 
    /// Elle pert un Gesture.Hook="True" sur les Views qui sont concernées par d'autres gestures que TapGestureRecognizer.
    /// </summary>
    public class Gestures
    {
        public static readonly BindableProperty HookProperty = BindableProperty.CreateAttached<Gestures, bool>(
               bindable => Gestures.GetHook(bindable),
               false, /* default value */
               BindingMode.OneWay,
               null,
               (b, o, n) => Gestures.OnHookChanged(b, o, n),
               null,
               null);

        public static bool GetHook(BindableObject bo)
        {
            return (bool)bo.GetValue(Gestures.HookProperty);
        }

        public static void SetHook(BindableObject bo, bool value)
        {
            bo.SetValue(Gestures.HookProperty, value);
        }

        public static void OnHookChanged(BindableObject bo, bool oldValue, bool newValue)
        {
            var l_View = bo as View;
            // On ne gère pas le Hook=false qui n'a pas de sens.
            if (l_View != null && newValue == true)
            {
                // L'astuce est toute simple, un IGestureService est implémenté par chaque plateforme
                // pour gérer les gestures. Ces implémentations utilisent un GestureManager pour implémenter
                // le comportement.
                Sodexo.Framework.Services.Container().Resolve<IGestureService>().Hook(l_View);
            }
        }
    }

    public class BaseGestureRecognizer : Element, IGestureRecognizer
    {
        public BaseGestureRecognizer()
        {

        }

        public BaseGestureRecognizer(Action p_ManipulationStartCallback, Action<GestureManipulationInfo> p_ManipulationDeltaCallback, Action<GestureManipulationInfo> p_ManipulationStopCallback)
        {
            if (p_ManipulationStartCallback != null) ManipulationStarted += (s, e) => p_ManipulationStartCallback();
            if (p_ManipulationDeltaCallback != null) ManipulationDelta += (s, e) => p_ManipulationDeltaCallback(e);
            if (p_ManipulationStopCallback != null) ManipulationStoped += (s, e) => p_ManipulationStopCallback(e);
        }

        internal void NotifyManipulationStart()
        {
            OnManipulationStart();
            if (ManipulationStarted != null) ManipulationStarted(this, new EventArgs());
        }

        internal void NotifyManipulationDelta(GestureManipulationInfo p_GestureManipulationInfo)
        {
            OnManipulationDelta(p_GestureManipulationInfo);
            if (ManipulationDelta != null) ManipulationDelta(this, p_GestureManipulationInfo);
        }

        internal void NotifyManipulationStop(GestureManipulationInfo p_GestureManipulationInfo)
        {
            OnManipulationStop(p_GestureManipulationInfo);
            if (ManipulationStoped != null) ManipulationStoped(this, p_GestureManipulationInfo);
        }

        protected virtual void OnManipulationStart()
        {

        }

        protected virtual void OnManipulationDelta(GestureManipulationInfo p_GestureManipulationInfo)
        {

        }

        protected virtual void OnManipulationStop(GestureManipulationInfo p_GestureManipulationInfo)
        {

        }

        public event EventHandler<EventArgs> ManipulationStarted;

        public event EventHandler<GestureManipulationInfo> ManipulationDelta;

        public event EventHandler<GestureManipulationInfo> ManipulationStoped;
    }

    public class GestureManipulationInfo : EventArgs
    {
        public GestureManipulationInfo(double p_DeltaX, double p_DeltaY, double p_TotalX, double p_TotalY)
        {
            _DeltaX = p_DeltaX;
            _DeltaY = p_DeltaY;
            _TotalX = p_TotalX;
            _TotalY = p_TotalY;
        }

        private double _DeltaX;
        public double DeltaX
        {
            get { return _DeltaX; }
        }

        private double _DeltaY;
        public double DeltaY
        {
            get { return _DeltaY; }
        }

        private double _TotalX;
        public double TotalX
        {
            get { return _TotalX; }
        }

        private double _TotalY;
        public double TotalY
        {
            get
            {
                return _TotalY;
            }
        }

        public override string ToString()
        {
            return string.Format("Delta({0};{1}), Total({2};{3})", _DeltaX, _DeltaY, _TotalX, _TotalY);
        }
    }

    public abstract class GestureManager
    {
        private View _View;
        public View View
        {
            get { return _View; }
        }

        public GestureManager(View p_View)
        {
            _View = p_View;
            // Evènement utilisé pour réagir au loaded.
            _View.SizeChanged += _View_SizeChanged;
        }

        void _View_SizeChanged(object sender, EventArgs e)
        {
            _View.SizeChanged -= _View_SizeChanged;
            OnLoaded();
        }

        private IEnumerable<BaseGestureRecognizer> GetGestureRecognizers()
        {
            return _View.GestureRecognizers.Where(gr => gr is BaseGestureRecognizer).Cast<BaseGestureRecognizer>();
        }

        protected abstract void OnLoaded();

        private GestureManipulationInfo _LastGestureManipulationInfo;

        protected void NotifyManipulationStart()
        {
            _LastGestureManipulationInfo = new GestureManipulationInfo(0, 0, 0, 0);
            foreach (var gr in GetGestureRecognizers())
            {
                gr.NotifyManipulationStart();
            }
        }

        protected void NotifyManipulationDelta(GestureManipulationInfo p_GestureManipulationInfo)
        {
            _LastGestureManipulationInfo = p_GestureManipulationInfo;
            foreach (var gr in GetGestureRecognizers())
            {
                gr.NotifyManipulationDelta(p_GestureManipulationInfo);
            }
        }

        protected void NotifyManipulationStop()
        {
            foreach (var gr in GetGestureRecognizers())
            {
                gr.NotifyManipulationStop(_LastGestureManipulationInfo);
            }
            _LastGestureManipulationInfo = null;
        }
    }

    public class SwipeGestureRecognizer : BaseGestureRecognizer
    {
        #region === Sensibility ===

        public static readonly BindableProperty SensibilityProperty = BindableProperty.Create<SwipeGestureRecognizer, int>(p => p.Sensibility, 50, propertyChanged: (d, o, n) => (d as SwipeGestureRecognizer).Sensibility_Changed(o, n));

        private void Sensibility_Changed(int oldvalue, int newvalue)
        {

        }

        public int Sensibility
        {
            get { return (int)GetValue(SensibilityProperty); }
            set { SetValue(SensibilityProperty, value); }
        }

        #endregion

        #region === LeftCommand ===

        public static readonly BindableProperty LeftCommandProperty = BindableProperty.Create<SwipeGestureRecognizer, ICommand>(p => p.LeftCommand, default(ICommand), propertyChanged: (d, o, n) => (d as SwipeGestureRecognizer).LeftCommand_Changed(o, n));

        private void LeftCommand_Changed(ICommand oldvalue, ICommand newvalue)
        {

        }

        public ICommand LeftCommand
        {
            get { return (ICommand)GetValue(LeftCommandProperty); }
            set { SetValue(LeftCommandProperty, value); }
        }

        #endregion

        #region === LeftCommandParameter ===

        public static readonly BindableProperty LeftCommandParameterProperty = BindableProperty.Create<SwipeGestureRecognizer, object>(p => p.LeftCommandParameter, default(object), propertyChanged: (d, o, n) => (d as SwipeGestureRecognizer).LeftCommandParameter_Changed(o, n));

        private void LeftCommandParameter_Changed(object oldvalue, object newvalue)
        {

        }

        public object LeftCommandParameter
        {
            get { return (object)GetValue(LeftCommandParameterProperty); }
            set { SetValue(LeftCommandParameterProperty, value); }
        }

        #endregion

        #region === RightCommand ===

        public static readonly BindableProperty RightCommandProperty = BindableProperty.Create<SwipeGestureRecognizer, ICommand>(p => p.RightCommand, default(ICommand), propertyChanged: (d, o, n) => (d as SwipeGestureRecognizer).RightCommand_Changed(o, n));

        private void RightCommand_Changed(ICommand oldvalue, ICommand newvalue)
        {

        }

        public ICommand RightCommand
        {
            get { return (ICommand)GetValue(RightCommandProperty); }
            set { SetValue(RightCommandProperty, value); }
        }

        #endregion

        #region === RightCommandParameter ===

        public static readonly BindableProperty RightCommandParameterProperty = BindableProperty.Create<SwipeGestureRecognizer, object>(p => p.RightCommandParameter, default(object), propertyChanged: (d, o, n) => (d as SwipeGestureRecognizer).RightCommandParameter_Changed(o, n));

        private void RightCommandParameter_Changed(object oldvalue, object newvalue)
        {

        }

        public object RightCommandParameter
        {
            get { return (object)GetValue(RightCommandParameterProperty); }
            set { SetValue(RightCommandParameterProperty, value); }
        }

        #endregion

        #region === TopCommand ===

        public static readonly BindableProperty TopCommandProperty = BindableProperty.Create<SwipeGestureRecognizer, ICommand>(p => p.TopCommand, default(ICommand), propertyChanged: (d, o, n) => (d as SwipeGestureRecognizer).TopCommand_Changed(o, n));

        private void TopCommand_Changed(ICommand oldvalue, ICommand newvalue)
        {

        }

        public ICommand TopCommand
        {
            get { return (ICommand)GetValue(TopCommandProperty); }
            set { SetValue(TopCommandProperty, value); }
        }

        #endregion

        #region === TopCommandParameter ===

        public static readonly BindableProperty TopCommandParameterProperty = BindableProperty.Create<SwipeGestureRecognizer, object>(p => p.TopCommandParameter, default(object), propertyChanged: (d, o, n) => (d as SwipeGestureRecognizer).TopCommandParameter_Changed(o, n));

        private void TopCommandParameter_Changed(object oldvalue, object newvalue)
        {

        }

        public object TopCommandParameter
        {
            get { return (object)GetValue(TopCommandParameterProperty); }
            set { SetValue(TopCommandParameterProperty, value); }
        }

        #endregion

        #region === BottomCommand ===

        public static readonly BindableProperty BottomCommandProperty = BindableProperty.Create<SwipeGestureRecognizer, ICommand>(p => p.BottomCommand, default(ICommand), propertyChanged: (d, o, n) => (d as SwipeGestureRecognizer).BottomCommand_Changed(o, n));

        private void BottomCommand_Changed(ICommand oldvalue, ICommand newvalue)
        {

        }

        public ICommand BottomCommand
        {
            get { return (ICommand)GetValue(BottomCommandProperty); }
            set { SetValue(BottomCommandProperty, value); }
        }

        #endregion

        #region === BottomCommandParameter ===

        public static readonly BindableProperty BottomCommandParameterProperty = BindableProperty.Create<SwipeGestureRecognizer, object>(p => p.BottomCommandParameter, default(object), propertyChanged: (d, o, n) => (d as SwipeGestureRecognizer).BottomCommandParameter_Changed(o, n));

        private void BottomCommandParameter_Changed(object oldvalue, object newvalue)
        {

        }

        public object BottomCommandParameter
        {
            get { return (object)GetValue(BottomCommandParameterProperty); }
            set { SetValue(BottomCommandParameterProperty, value); }
        }

        #endregion

        public event EventHandler<EventArgs> Left;

        public event EventHandler<EventArgs> Right;

        public event EventHandler<EventArgs> Top;

        public event EventHandler<EventArgs> Bottom;

        public SwipeGestureRecognizer()
        {

        }

        protected override void OnManipulationStart()
        {
            base.OnManipulationStart();
        }

        protected override void OnManipulationDelta(GestureManipulationInfo p_GestureManipulationInfo)
        {
            base.OnManipulationDelta(p_GestureManipulationInfo);
        }

        protected override void OnManipulationStop(GestureManipulationInfo p_GestureManipulationInfo)
        {
            base.OnManipulationStop(p_GestureManipulationInfo);
            if(p_GestureManipulationInfo.TotalX<0 && Math.Abs(p_GestureManipulationInfo.TotalX)>Sensibility)
            {
                System.Diagnostics.Debug.WriteLine("LEFT");
                if (LeftCommand != null) LeftCommand.Execute(LeftCommandParameter);
                if (Left != null) Left(this, new EventArgs());
            }
            else if (p_GestureManipulationInfo.TotalX > 0 && Math.Abs(p_GestureManipulationInfo.TotalX) > Sensibility)
            {
                System.Diagnostics.Debug.WriteLine("RIGHT");
                if (RightCommand != null) RightCommand.Execute(RightCommandParameter);
                if (Right != null) Right(this, new EventArgs());
            }
            else if (p_GestureManipulationInfo.TotalY < 0 && Math.Abs(p_GestureManipulationInfo.TotalY) > Sensibility)
            {
                System.Diagnostics.Debug.WriteLine("TOP");
                if (TopCommand != null) TopCommand.Execute(TopCommandParameter);
                if (Top != null) Top(this, new EventArgs());
            }
            else if (p_GestureManipulationInfo.TotalY > 0 && Math.Abs(p_GestureManipulationInfo.TotalY) > Sensibility)
            {
                System.Diagnostics.Debug.WriteLine("BOTTOM");
                if (BottomCommand != null) BottomCommand.Execute(BottomCommandParameter);
                if (Bottom != null) Bottom(this, new EventArgs());
            }
        }
    }
}

