﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Sodexo
{
    public static class FrameworkXamarinForms
    {
        public static void Initialize()
        {
            // Service de gestion des thèmes.
            Sodexo.Framework.Services.Container().Register<Sodexo.Themes.IThemeService, Sodexo.Themes.ThemeServiceXamarin>(new Sodexo.SingletonInstanceManager());

            // Service de navigation.
            // Xamarin met en place un système de navigation qui peut être utilisé si il suffit pour répondre aux besoin.
            // Ceci n'est qu'une implémentation en alternative donnée en exemple.
            Sodexo.Framework.Services.Container().Register<Sodexo.IInteractionService, NavigationPageInteractionService>(new Sodexo.SingletonInstanceManager());

            // Enregistrement de l'assembly
            Sodexo.Framework.Services.AssemblyService().RegisterAssembly(typeof(FrameworkXamarinForms).GetTypeInfo().Assembly);
        }
    }
}
