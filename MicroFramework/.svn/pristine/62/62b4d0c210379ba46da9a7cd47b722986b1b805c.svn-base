﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sodexo.Controls
{
    public class CustomButton : ContentView
    {
        private StackLayout _RootStackLayout;
        private Label buttonLabel;

        public enum ButtonTypes
        {
            Burger,
            Commande,
            Rounded,
            Fixed
        }
        public enum ViewSenderTypes
        {
            SecondBurger,
            Default
        }

        public CustomButton()
        {
           
        }



        #region === TapCommandParameter ===

        public static readonly BindableProperty TapCommandParameterProperty = BindableProperty.Create<CustomButton, object>(p => p.TapCommandParameter, default(object), propertyChanged: (d, o, n) => (d as CustomButton).TapCommandParameter_Changed(o, n));

        private void TapCommandParameter_Changed(object oldvalue, object newvalue)
        {

        }

        public object TapCommandParameter
        {
            get { return (object)GetValue(TapCommandParameterProperty); }
            set { SetValue(TapCommandParameterProperty, value); }
        }

        #endregion

        #region === ViewSender ===

        public static readonly BindableProperty ViewSenderProperty = BindableProperty.Create<CustomButton, ViewSenderTypes>(p => p.ViewSender, ViewSenderTypes.Default, propertyChanged: (d, o, n) => (d as CustomButton).ViewSender_Changed(o, n));

        private void ViewSender_Changed(ViewSenderTypes oldvalue, ViewSenderTypes newvalue)
        {

        }

        public ViewSenderTypes ViewSender
        {
            get { return (ViewSenderTypes)GetValue(ViewSenderProperty); }
            set { SetValue(ViewSenderProperty, value); }
        }

        #endregion

        #region === ItemTemplate ===

            public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create<CustomButton, DataTemplate>(p => p.ItemTemplate, null, propertyChanged: (d, o, n) => (d as CustomButton).ItemTemplate_Changed(o, n));
            
            // J'ai commenté le build ne voyant pas son utilité pour le moment.
            private void ItemTemplate_Changed(DataTemplate oldvalue, DataTemplate newvalue)
            {
                //Build();
            }

            public DataTemplate ItemTemplate
            {
                get { return (DataTemplate)GetValue(ItemTemplateProperty); }
                set { SetValue(ItemTemplateProperty, value); }
            }

        #endregion

        #region === TapCommand ===

            public static readonly BindableProperty TapCommandProperty = BindableProperty.Create<CustomButton, ICommand>(p => p.TapCommand, default(ICommand), propertyChanged: (d, o, n) => (d as CustomButton).TapCommand_Changed(o, n));

            private void TapCommand_Changed(ICommand oldvalue, ICommand newvalue)
            {
            }

            public ICommand TapCommand
            {
                get { return (ICommand)GetValue(TapCommandProperty); }
                set { SetValue(TapCommandProperty, value); }
            }

        #endregion

        #region === ButtonLabel ===

            public static readonly BindableProperty ButtonLabelProperty = BindableProperty.Create<CustomButton, string>(p => p.ButtonLabel, default(string), propertyChanged: (d, o, n) => (d as CustomButton).ButtonLabel_Changed(o, n));

            /// <summary>
            /// Pour mettre à jour les bonnes couleurs des puces.
            /// </summary>
            /// <param name="oldvalue"></param>
            /// <param name="newvalue"></param>
            private void ButtonLabel_Changed(string oldvalue, string newvalue)
            {
                Rebuild();
            }

            public string ButtonLabel
            {
                get { return (string)GetValue(ButtonLabelProperty); }
                set { SetValue(ButtonLabelProperty, value); }
            }

        #endregion

        #region === ButtonType ===

            public static readonly BindableProperty ButtonTypeProperty = BindableProperty.Create<CustomButton, ButtonTypes>(p => p.ButtonType, ButtonTypes.Fixed, propertyChanged: (d, o, n) => (d as CustomButton).ButtonType_Changed(o, n));

            /// <summary>
            /// Pour mettre à jour les bonnes couleurs des puces.
            /// </summary>
            /// <param name="oldvalue"></param>
            /// <param name="newvalue"></param>
            private void ButtonType_Changed(ButtonTypes oldvalue, ButtonTypes newvalue)
            {
                Rebuild();
            }

            public ButtonTypes ButtonType
            {
                get { return (ButtonTypes)GetValue(ButtonTypeProperty); }
                set { SetValue(ButtonTypeProperty, value); }
            }

            #endregion

        #region === ButtonColor ===

            public static readonly BindableProperty ButtonColorProperty = BindableProperty.Create<CustomButton, Color>(p => p.ButtonColor, default(Color), propertyChanged: (d, o, n) => (d as CustomButton).ButtonColor_Changed(o, n));

            /// <summary>
            /// Pour mettre à jour les bonnes couleurs des puces.
            /// </summary>
            /// <param name="oldvalue"></param>
            /// <param name="newvalue"></param>
            private void ButtonColor_Changed(Color oldvalue, Color newvalue)
            {
            }

            [TypeConverter(typeof(ColorTypeConverter))]
            public Color ButtonColor
            {
                get { return (Color)GetValue(ButtonLabelProperty); }
                set { SetValue(ButtonLabelProperty, value); }
            }

            #endregion

        private void Rebuild()
            {
                var dataTemplate = new DataTemplate(() =>
                {
                    _RootStackLayout = new StackLayout()
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand
                    };
                    _RootStackLayout.Children.Clear();
                    
                    switch (ButtonType)
                    {
                        case ButtonTypes.Burger:
                            _RootStackLayout.SetDynamicResource(StackLayout.HeightRequestProperty, "BUTTON_BURGER_HEIGHT");
                            _RootStackLayout.SetDynamicResource(StackLayout.StyleProperty, "AltHamburgerButtonStyle");
                            break;
                        case ButtonTypes.Commande:
                            _RootStackLayout.VerticalOptions = LayoutOptions.FillAndExpand;
                            _RootStackLayout.SetDynamicResource(StackLayout.StyleProperty, "FixedButtonColorStyle");
                            break;
                        case ButtonTypes.Fixed:
                            _RootStackLayout.SetDynamicResource(StackLayout.HeightRequestProperty, "BUTTON_FIXED_HEIGHT");
                            _RootStackLayout.SetDynamicResource(StackLayout.StyleProperty, "FixedButtonColorStyle");
                            break;
                        default:
                            _RootStackLayout.SetDynamicResource(StackLayout.HeightRequestProperty, "BUTTON_FIXED_HEIGHT");
                            _RootStackLayout.SetDynamicResource(StackLayout.StyleProperty, "FixedButtonColorStyle");
                            break;
                    }
                    
                    //_RootStackLayout.Behaviors.Add(new Behaviors.ViewButtonBehaviour());

                    //code d'avant
                    if(ButtonType!=ButtonTypes.Rounded)
                    {
                        buttonLabel = new Label() { Text = ButtonLabel };
                        buttonLabel.SetDynamicResource(Label.StyleProperty, "BUTTON_LABEL_STYLE");


                        var StackTapGesture = new TapGestureRecognizer();
                        StackTapGesture.Tapped += async (object sender, EventArgs e) =>
                        {
                            if (TapCommand != null)
                            {
                                //if( ViewSender==ViewSenderTypes.SecondBurger)
                                //{
                                //    try
                                //    {
                                //        var a = ((this.Parent as StackLayout) as StackLayout);
                                //        var b = (((a.Parent as StackLayout).Parent as Grid).Parent as AlternateBurger);
                                //        var contentView = b.Parent as TouchContentView;
                                //        var defautGrid = contentView.Parent as Grid;
                                //        await contentView.TranslateTo(480, 0, 100, Easing.Linear);
                                //        defautGrid.Children.Remove(contentView);
                                //    }
                                //    catch (Exception ex)
                                //    {
                                //        Sodexo.Framework.Services.InteractionService().Alert("", "ViewSender heirarchy: " + ex.Message);
                                //    }
                                //}
                                TapCommand.Execute(TapCommandParameter);
                            }
                        };
                        _RootStackLayout.GestureRecognizers.Add(StackTapGesture);

                        _RootStackLayout.Children.Add(buttonLabel);

                        return _RootStackLayout;
                    }
                    //new code for rounded button type 
                    else
                    {
                        var rootGrid = new Grid()
                        {
                            Padding=0,
                            ColumnSpacing=0,
                            BackgroundColor = ButtonColor,
                            VerticalOptions = LayoutOptions.Fill,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            ColumnDefinitions =  
                            { 
                                new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
                                new ColumnDefinition() { Width = 100 }
                            },
                        };

                        #region rounded button
                        var roundedButton = new RoundedButton()
                        {
                            TextColor = Color.White,
                            BackgroundColor = Color.FromHex("#C10418"),
                            BorderColor = Color.Transparent,
                            HeightRequest = 80,
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            HorizontalOptions = LayoutOptions.FillAndExpand
                        };
                        roundedButton.SetDynamicResource(RoundedButton.StyleProperty, "RoundedButtonStyle");
                        #endregion

                        Grid.SetColumn(roundedButton, 0);
                        Grid.SetColumnSpan(roundedButton, 2);
                        rootGrid.Children.Add(roundedButton);

                        #region LabelText
                        var label = new Label()
                        {
                            TextColor=Color.White,
                            VerticalOptions=LayoutOptions.FillAndExpand,
                            HorizontalOptions=LayoutOptions.FillAndExpand,
                            VerticalTextAlignment=TextAlignment.Center,
                            HorizontalTextAlignment=TextAlignment.Start
                        };
                        label.Text = ButtonLabel;

                        var labelStack = new StackLayout()
                        {
                            BackgroundColor = Color.Transparent,
                            Padding=new Thickness(40,0,0,0),
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            Children = { label }
                        };

                        #endregion

                        Grid.SetColumn(labelStack, 0);
                        Grid.SetColumnSpan(labelStack, 2);
                        rootGrid.Children.Add(labelStack);

                        #region arrowImage
                        var arrowImage = new Image()
                        {
                            BackgroundColor=Color.Transparent,
                            VerticalOptions=LayoutOptions.CenterAndExpand,
                            HorizontalOptions = LayoutOptions.CenterAndExpand,
                            Aspect=Aspect.AspectFit,
                            Source=ImageSource.FromResource("Sodexo.Images.Gerant.arrow.png")
                        };
                        #endregion

                        Grid.SetColumn(arrowImage, 1);
                        rootGrid.Children.Add(arrowImage);

                        #region transparentCover
                        var transparentGrid = new Grid()
                        {
                            BackgroundColor = Color.Transparent,
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                        };
                        #endregion

                        Grid.SetColumn(transparentGrid, 0);
                        Grid.SetColumnSpan(transparentGrid, 2);
                        rootGrid.Children.Add(transparentGrid);

                        var GridTapGesture = new TapGestureRecognizer();
                        GridTapGesture.Tapped += async (object sender, EventArgs e) =>
                        {
                            if (TapCommand != null)
                            {
                                TapCommand.Execute(TapCommandParameter);
                            }
                        };
                        transparentGrid.GestureRecognizers.Add(GridTapGesture);

                        return rootGrid;
                    }
                });

                this.ItemTemplate = dataTemplate;
                Content = ItemTemplate.CreateContent() as View;
            }

    }
}
