﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sodexo.Controls
{
    public class AdvancedPicker : Grid
    {
        // Properties
        protected StackLayout PagingStackLayout;
        protected ScrollView ScrollView;
        protected readonly ICommand SelectedCommand;
        protected readonly StackLayout ItemsStackLayout;
        protected Boolean Wait = false;
        private Boolean _isScrollAutomaticInitialized;
        protected int l_PaddingValue = 12;
        protected double l_ItemWidth;

        /// <summary>
        /// Constructor
        /// </summary>
        public AdvancedPicker()
        {

            BackgroundColor = Color.Green;

            // Our ScrollView
            ScrollView = new ScrollView
            {
                Orientation = ScrollOrientation.Horizontal,
                BackgroundColor = Color.White
            };

            // Scroll event
            ScrollView.Scrolled += ScrollView_Scrolled;
            
            l_ItemWidth = (Sodexo.Framework.Services.InteractionService().DeviceWidth - l_PaddingValue*2)/3;

            // List des items
            ItemsStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Padding = new Thickness(0,0,0,0),
                Spacing = 0,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };

            ScrollView.Content = new StackLayout(){
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Children = { 
                    new StackLayout() { WidthRequest = l_ItemWidth, VerticalOptions = LayoutOptions.FillAndExpand, BackgroundColor = Color.Transparent, Children = { new Label { Text = "", FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) } } }, 
                    ItemsStackLayout,
                     new StackLayout() { WidthRequest = l_ItemWidth, VerticalOptions = LayoutOptions.FillAndExpand, BackgroundColor = Color.Transparent, Children = { new Label { Text = "", FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) } } }        
                }
            };
            Children.Add(ScrollView);

            PagingStackLayout = new StackLayout()
            {
                HorizontalOptions = LayoutOptions.Center,
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.EndAndExpand,
                Padding = Device.OnPlatform<Thickness>(
                new Thickness(0, 0, 0, 0),
                new Thickness(0, 0, 0, 0),
                new Thickness(0, 0, 0, 0)),
                Opacity = 0.5,
                Children = { 
                    new BoxView() { BackgroundColor = Color.Green, WidthRequest = 20, HeightRequest = 20 }
                }
            };
            Children.Add(PagingStackLayout);

            SelectedCommand = new Command<object>(item =>
            {
                var selectable = item as ISelectable;
                if (selectable == null) return;

                SetSelected(selectable);
                SelectedItem = selectable.IsSelected ? selectable : null;
            });

            PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName == "Orientation")
                {
                    ItemsStackLayout.Orientation = ScrollView.Orientation == ScrollOrientation.Horizontal ? StackOrientation.Horizontal : StackOrientation.Vertical;
                }

            };
        }


        public int ItemsCount
        {
            get { return this.ItemsStackLayout.Children.Count; }
        }

        protected virtual void SetSelected(ISelectable selectable)
        {
            selectable.IsSelected = true;
        }

        public View ActualElement
        {
            get
            {
                return ItemsStackLayout.Children[ActualElementIndex];
            }
        }
        public int ActualElementIndex { get; set; }

        public bool ScrollToStartOnSelected { get; set; }

        public event EventHandler SelectedItemChanged;

        public static readonly BindableProperty ItemsSourceProperty =
            BindableProperty.Create<AdvancedPicker, IEnumerable>(p => p.ItemsSource, default(IEnumerable<object>), BindingMode.TwoWay, null, ItemsSourceChanged);

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly BindableProperty SelectedItemProperty =
            BindableProperty.Create<AdvancedPicker, object>(p => p.SelectedItem, default(object), BindingMode.TwoWay, null, OnSelectedItemChanged);

        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly BindableProperty ItemTemplateProperty =
            BindableProperty.Create<AdvancedPicker, DataTemplate>(p => p.ItemTemplate, default(DataTemplate));

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        private static void ItemsSourceChanged(BindableObject bindable, IEnumerable oldValue, IEnumerable newValue)
        {
            var itemsLayout = (AdvancedPicker)bindable;
            itemsLayout.SetItems();
            itemsLayout.SetPagination();
            //itemsLayout.ScrollAutomaticAsync();

        }

        protected virtual void SetItems()
        {
            ItemsStackLayout.Children.Clear();

            if (ItemsSource == null)
                return;

            foreach (var item in ItemsSource)
                ItemsStackLayout.Children.Add(GetItemView(item));

            SelectedItem = ItemsSource.OfType<ISelectable>().FirstOrDefault(x => x.IsSelected);
        }

        protected virtual View GetItemView(object item)
        {
            var content = ItemTemplate.CreateContent();
            var view = content as View;
            view.WidthRequest = l_ItemWidth;
            view.BackgroundColor = Color.Blue;
            


            if (view == null) return null;

            view.BindingContext = item;

            var gesture = new TapGestureRecognizer
            {
                Command = SelectedCommand,
                CommandParameter = item
            };

            AddGesture(view, gesture);

            return view;
        }

        protected void AddGesture(View view, TapGestureRecognizer gesture)
        {
            view.GestureRecognizers.Add(gesture);

            var layout = view as Layout<View>;

            if (layout == null)
                return;

            foreach (var child in layout.Children)
                AddGesture(child, gesture);
        }

        private static void OnSelectedItemChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var itemsView = (AdvancedPicker)bindable;
            if (newValue == oldValue)
                return;

            var selectable = newValue as ISelectable;
            itemsView.SetSelectedItem(selectable ?? oldValue as ISelectable);
        }

        protected virtual void SetSelectedItem(ISelectable selectedItem)
        {
            var items = ItemsSource;

            foreach (var item in items.OfType<ISelectable>())
                item.IsSelected = selectedItem != null && item == selectedItem && selectedItem.IsSelected;

            var handler = SelectedItemChanged;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        protected virtual async void ScrollAutomaticAsync()
        {
            while (!_isScrollAutomaticInitialized)
            {
                _isScrollAutomaticInitialized = true;
                if (!Wait)
                {
                    SetActivePage();
                    await Task.Delay(5000);
                    this.ActualElementIndex++;
                    await ScrollToActualAsync();
                    _isScrollAutomaticInitialized = false;
                }

            }
        }

        private async Task ScrollToActualAsync()
        {
            if (this.ActualElementIndex == this.ItemsCount)
                this.ActualElementIndex = 0;

            if (this.ActualElementIndex < 0)
                this.ActualElementIndex = 0;

            try
            {
                await this.ScrollView.ScrollToAsync(this.ActualElement.X, 0, false);
            }
            catch
            {
                //invalid scroll: sometimes happen
            }
        }

        protected virtual void SetPagination()
        {
            this.ActualElementIndex = 0;
            /*PagingStackLayout.Children.Clear();
            for (int i = 0; i < this.ItemsCount; i++)
            {
                var view = new BoxView() { BackgroundColor = Color.Black, WidthRequest = 10, HeightRequest = 10 };
                PagingStackLayout.Children.Add(view);
            }*/
        }

        protected virtual void SetActivePage()
        {
            try
            {
                for (int i = 0; i < this.ItemsCount; i++)
                {
                    //(PagingStackLayout.Children[i] as BoxView).BackgroundColor = Color.Black;
                    (ItemsStackLayout.Children[i] as Label).TextColor = Color.Gray;
                }
                //(PagingStackLayout.Children[this.ActualElementIndex] as BoxView).BackgroundColor = Color.Red;
                (ItemsStackLayout.Children[this.ActualElementIndex] as Label).TextColor = Color.Black;
            }
            catch { }
        }

        void ScrollView_Scrolled(object sender, ScrolledEventArgs e)
        {
            if (e.ScrollX % ItemsStackLayout.Children.First().Width != 0 && !Wait)
                Wait = true;

            for (int i = 1; i < this.ItemsCount; i++)
            {
                var previousItemX = ItemsStackLayout.Children[i - 1].X;
                var actualItemX = ItemsStackLayout.Children[i].X;

                if (e.ScrollX >= actualItemX-l_ItemWidth/3 && e.ScrollX <= actualItemX)
                {
                    this.ActualElementIndex = e.ScrollX == actualItemX ? i : ((actualItemX - e.ScrollX - l_ItemWidth) > 0 ? i - 1 : i);
                    SetActivePage();
                }
                else if (e.ScrollX == 0)
                {
                    this.ActualElementIndex = 0;
                    SetActivePage();
                }
            }
        }
    }

    public interface ISelectable
    {
        bool IsSelected { get; set; }

        ICommand SelectCommand { get; set; }
    }
}
