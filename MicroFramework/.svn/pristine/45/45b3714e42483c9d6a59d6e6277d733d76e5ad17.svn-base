﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Sodexo
{
    public class ApplicationMayCloseEventArgs
    {
        private bool _MayClose = true;
        public bool MayClose
        {
            get { return _MayClose; }
            set { _MayClose = value; }
        }
    }

    public interface IInteractionService
    {
        void Set<TInteraction>(object p_Context = null, bool p_ChangeStatusBarColor = false) where TInteraction : class;
		void Set(string p_InteractionName, object p_Context = null, bool p_ChangeStatusBarColor = false, bool p_AllPlatForm = true);
        void Set(object p_Page, object p_Context = null);

        Task<IInteractionResult<TResult>> Open<TInteraction, TResult>(object p_Context = null);
        Task<IInteractionResult> Open<TInteraction>(object p_Context = null);

        Task<IInteractionResult<TResult>> Open<TResult>(string p_InteractionName, object p_Context = null);
        Task<IInteractionResult> Open(string p_InteractionName, object p_Context = null);

        void Cancel();
        void Close();
        void Close(int numberOfCloseToPerform);
        void Close(string viewName, bool isInterface = false);
        void Close<TResult>(TResult p_Result, bool p_Animation = true);
        Task Close(bool p_Animation=false, int p_RedirectPageIndex = 0, string p_InteractionName = null, object p_Context = null);

        string ThemeNameValue { set; get; }
        double DeviceWidth { set; get; }
        double DeviceHeight { set; get; }

        void ViewSecondBurger(IViewModel IVM);
        Task RemoveSecondBurger(object CurrentPage);
        Task Alert(string p_Title, string p_Text, int _AlerteTempo = 3, AlertType typeAlert = AlertType.Error, string _BackColor = Constants.DefaultStackBackGroundColor, string _LabelColor = Constants.DefaultLabelColor);
        Task<bool> Confirm(string p_Title, string p_Text);

#if DEBUG 
        /// <summary>
        /// True si vous voulez utiliser les outils développeur en mode DEBUG.
        /// </summary>
        bool UseDevelopperTool { get; set; }
#endif
    }

    public interface IInteractionBase
    {
        void Cancel();
    }

    public interface IInteraction : IInteractionBase
    {
        Task<IInteractionResult> Run();
        void Close();
    }

    public interface IParameterizedInteraction<TParameters> : IInteractionBase
    {
        Task<IInteractionResult> Run(TParameters p_Parameters);
        void Close();
    }

    public interface IInteraction<TResult> : IInteractionBase
    {
        Task<IInteractionResult<TResult>> Run();
        void Close(TResult p_Result);
    }

    public interface IParameterizedInteraction<TParameters, TResult> : IInteractionBase
    {
        Task<IInteractionResult<TResult>> Run(TParameters p_Parameters);
        void Close(TResult p_Result);
    }

    public interface IInteractionResult
    {
        bool IsCanceled { get; }
    }

    public interface IInteractionResult<TResult>
    {
        bool IsCanceled { get; }

        TResult Result { get; }
    }
}

