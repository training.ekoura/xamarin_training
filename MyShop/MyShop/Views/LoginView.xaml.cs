﻿using System;
using System.Collections.Generic;
using MyShop.ViewModels;
using Xamarin.Forms;

namespace MyShop.Views
{
    public partial class LoginView : ContentPage
    {
        public LoginView()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            //appTitle.RotateYTo((-8 * Math.PI / 180), 0, null);
            appTitle.Rotation = -5;
            appTitle.TranslationY = -10;
        }

        void Entry_Focused(System.Object sender, Xamarin.Forms.FocusEventArgs e)
        {
        }
    }
}
