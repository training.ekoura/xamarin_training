﻿using System;
using System.Collections.Generic;
using MyShop.Models;
using Xamarin.Forms;

namespace MyShop.Views
{
    public partial class MenuView : ContentPage
    {
        MainView RootPage { get => Application.Current.MainPage as MainView; }
        List<HomeMenuItem> menuItems;
        public MenuView()
        {
            InitializeComponent();

            menuItems = new List<HomeMenuItem>
                {
                new HomeMenuItem {Id = MenuItemType.Logout, Title="Browse" },
                new HomeMenuItem {Id = MenuItemType.Orders, Title="About" }
            };

            ListViewMenu.ItemsSource = menuItems;

            ListViewMenu.SelectedItem = menuItems[0];
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                    return;

                var id = (int)((HomeMenuItem)e.SelectedItem).Id;
                await RootPage.NavigateFromMenu(id);
            };
        }
    }
}
