﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MyShop.Services;
using MyShop.Views;
using MyShop.Services.Navigation;
using MyShop.Views.Interface;
using System.Threading.Tasks;
using MyShop.ViewModels.Base;
using Xamarin.Essentials;

namespace MyShop
{
    public partial class App : Application
    {
        public static double ScreenHeight { get; set; }
        public static double ScreenWidth { get; set; }

        public App()
        {
            InitializeComponent();
            InitApp();

            //DependencyService.Register<MockDataStore>();
            //
            //MainPage = new NavigationPage(new LoginView());
        }

        protected override void OnStart()
        {
            InitNavigation();
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        private Task InitNavigation()
        {
            var navigationService = ViewModelLocator.Resolve<INavigationService>();
            return navigationService.InitializeAsync();
        }

        private void InitApp()
        {
            // Get Metrics
            var mainDisplayInfo = DeviceDisplay.MainDisplayInfo;

            ScreenHeight = mainDisplayInfo.Height;
            ScreenWidth = mainDisplayInfo.Width;
        }


    }
}
