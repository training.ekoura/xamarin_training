﻿using System;
using System.Text;
using MyShop.Helpers.Interface;

namespace MyShop.Helpers
{
    internal  class SerializerService : ISerializerService
    {
        public T Deserialize<T>(StringBuilder p_Data)
        {
            var l_Serializer = Newtonsoft.Json.JsonSerializer.Create();

            using (var l_StreamReader = new System.IO.StringReader(p_Data.ToString()))
            {
                using (var l_JsonReader = new Newtonsoft.Json.JsonTextReader(l_StreamReader))
                {
                    var l_Result = l_Serializer.Deserialize<T>(l_JsonReader);
                    return (T)l_Result;
                }
            }

        }

        public StringBuilder Serialize<T>(T p_Object)
        {
            var l_Result = new StringBuilder();
            if (p_Object != null)
            {
                var l_Serializer = Newtonsoft.Json.JsonSerializer.Create();
                using (var l_StreamReader = new System.IO.StringWriter(l_Result))
                {
                    using (var l_JsonWriter = new Newtonsoft.Json.JsonTextWriter(l_StreamReader))
                    {
                        l_Serializer.Serialize(l_JsonWriter, p_Object);
                    }
                }
            }
            return l_Result;
        }
    }
}
