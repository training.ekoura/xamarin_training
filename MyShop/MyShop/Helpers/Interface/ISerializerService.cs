﻿using System;
using System.Text;

namespace MyShop.Helpers.Interface
{
    public interface ISerializerService
    {
        StringBuilder Serialize<T>(T p_Object);
        T Deserialize<T>(StringBuilder p_Data);
    }
}
