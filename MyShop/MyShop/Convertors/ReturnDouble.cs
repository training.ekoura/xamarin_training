﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace MyShop.Convertors
{
    public class ReturnDouble : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double returnValue;
            double.TryParse(value.ToString(), out returnValue);
            return returnValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
