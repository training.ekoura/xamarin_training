﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using Xamarin.Forms;

namespace MyShop.CustumControl
{
    public class RepeaterView : StackLayout
    {
        private bool hasDots;

        public RepeaterView()
        {
            //this.Spacing = ItemSpacing;
            hasDots = false;
        }

        #region === ItemsSource ===

        //public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create<RepeaterView, object>(p => p.ItemsSource, default(object), propertyChanged: (d, o, n) => (d as RepeaterView).ItemsSource_Changed(o, n));
        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.CreateAttached("ItemsSourceProperty", typeof(object), typeof(RepeaterView), default(object), propertyChanged: (d, o, n) => (d as RepeaterView).ItemsSource_Changed(o, n));
        private void ItemsSource_Changed(object oldvalue, object newvalue)
        {
            if (oldvalue is INotifyCollectionChanged)
            {
                (oldvalue as INotifyCollectionChanged).CollectionChanged -= RepeaterView_CollectionChanged;
            }

            if (newvalue is INotifyCollectionChanged)
            {
                (newvalue as INotifyCollectionChanged).CollectionChanged += RepeaterView_CollectionChanged;
            }
            Update();
        }

        public object ItemsSource
        {
            get => (object)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        void RepeaterView_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Update();
        }

        #endregion

        #region === ItemTemplate ===

        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create("ItemTemplate", typeof(DataTemplate), typeof(RepeaterView), default(DataTemplate), propertyChanged: (d, o, n) => (d as RepeaterView).ItemTemplate_Changed(o, n));

        private void ItemTemplate_Changed(object oldvalue, object newvalue)
        {
            Update();
        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        #endregion

        #region === ItemSpacing ===

        public static readonly BindableProperty ItemSpacingProperty = BindableProperty.Create("ItemSpacing",typeof(double), typeof(RepeaterView), default(double), propertyChanged: (d, o, n) => (d as RepeaterView).ItemSpacing_Changed(o, n));

        /// <summary>
        /// </summary>
        /// <param name="oldvalue"></param>
        /// <param name="newvalue"></param>
        private void ItemSpacing_Changed(object oldvalue, object newvalue)
        {

        }

        public double ItemSpacing
        {
            get { return (double)GetValue(ItemSpacingProperty); }
            set { SetValue(ItemSpacingProperty, value); }
        }

        #endregion

        #region === HasDottedLines ===

        public static readonly BindableProperty HasDottedLinesProperty = BindableProperty.Create("HasDottedLines", typeof(string), typeof(RepeaterView) ,default(string), propertyChanged: (d, o, n) => (d as RepeaterView).HasDottedLines_Changed(o, n));

        /// <summary>
        /// </summary>
        /// <param name="oldvalue"></param>
        /// <param name="newvalue"></param>
        private void HasDottedLines_Changed(object oldvalue, object newvalue)
        {
            if (!string.IsNullOrEmpty(newvalue as string))
                hasDots = true;
        }

        public string HasDottedLines
        {
            get => (string)GetValue(HasDottedLinesProperty);
            set => SetValue(HasDottedLinesProperty, value);
        }

        #endregion

        private void Update()
        {
            this.Children.Clear();
            if (ItemTemplate == null || !(ItemsSource is IEnumerable)) return;

            this.Spacing = ItemSpacing;

            foreach (var l_Item in (ItemsSource as IEnumerable))
            {
                var l_Content = ItemTemplate.CreateContent() as View;
                if (l_Content != null)
                {
                    try
                    {
                        this.Children.Add(l_Content);
                        l_Content.BindingContext = l_Item;
                    }
                    catch (Exception l_Exception)
                    {
                        Debug.WriteLine("RepeaterView : " + l_Exception.Message);
                    }

                    if (hasDots)
                    {
                        var newLabel = new Label()
                        {
                            HeightRequest = ItemSpacing,
                            FontSize = (ItemSpacing / 2),
                            FontAttributes = Xamarin.Forms.FontAttributes.Bold,
                            Text = "\u2014       \u2014         \u2014        \u2014        \u2014        \u2014        \u2014" +
                                    "\u2014       \u2014         \u2014        \u2014        \u2014        \u2014        \u2014" +
                                    "\u2014       \u2014         \u2014        \u2014        \u2014        \u2014        \u2014" +
                                    "\u2014       \u2014         \u2014        \u2014        \u2014        \u2014        \u2014" +
                                    "\u2014       \u2014         \u2014        \u2014        \u2014        \u2014        \u2014" +
                                    "\u2014       \u2014         \u2014        \u2014        \u2014        \u2014        \u2014" +
                                    "\u2014       \u2014         \u2014        \u2014        \u2014        \u2014        \u2014" +
                                    "\u2014       \u2014         \u2014        \u2014        \u2014        \u2014        \u2014" +
                                    "\u2014       \u2014         \u2014        \u2014        \u2014        \u2014        \u2014" +
                                    "\u2014       \u2014         \u2014        \u2014        \u2014        \u2014        \u2014" +
                                    "\u2014       \u2014         \u2014        \u2014        \u2014        \u2014        \u2014",
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Start,
                            LineBreakMode = LineBreakMode.MiddleTruncation,
                        };

                        newLabel.SetDynamicResource(Label.StyleProperty, "ThemedTextStyle");

                        this.Children.Add(newLabel);
                    }
                }
            }
            UpdateChildrenLayout();
            InvalidateLayout();
        }

        //public ObservableCollection<T> ItemsSource
        //{
        //    get { return (ObservableCollection<T>)GetValue(ItemsSourceProperty); }
        //    set { SetValue(ItemsSourceProperty, value); }
        //}

        //public static readonly BindableProperty ItemsSourceProperty =
        //BindableProperty.Create<RepeaterView<T>, ObservableCollection<T>>(p => p.ItemsSource, new ObservableCollection<T>(), BindingMode.OneWay, null, ItemsSource_Changed);

        //private static void ItemsSource_Changed(BindableObject bindable, ObservableCollection<T> oldValue, ObservableCollection<T> newValue)
        //{
        //    var control = bindable as RepeaterView<T>;
        //    control.ItemsSource.CollectionChanged += control.ItemsSource_CollectionChanged;
        //    control.Children.Clear();

        //    foreach (var item in newValue)
        //    {
        //        var cell = control.ItemTemplate.CreateContent();
        //        control.Children.Add(((ViewCell)cell).View);
        //    }

        //    control.UpdateChildrenLayout();
        //    control.InvalidateLayout();
        //}

        //public delegate void RepeaterViewItemAddedEventHandler(object sender, RepeaterViewItemAddedEventArgs args);
        //public event RepeaterViewItemAddedEventHandler ItemCreated;

        //protected virtual void NotifyItemAdded(View view, object model)
        //{
        //    if (ItemCreated != null)
        //    {
        //        ItemCreated(this, new RepeaterViewItemAddedEventArgs(view, model));
        //    }
        //}

        //void ItemsSource_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        //{
        //    if (e.OldItems != null)
        //    {
        //        this.Children.RemoveAt(e.OldStartingIndex);
        //        this.UpdateChildrenLayout();
        //        this.InvalidateLayout();
        //    }

        //    if (e.NewItems != null)
        //    {
        //        foreach (T item in e.NewItems)
        //        {
        //            var cell = this.ItemTemplate.CreateContent();

        //            View view;
        //            if (cell is ViewCell)
        //                view = ((ViewCell)cell).View;
        //            else
        //                view = (View)cell;

        //            view.BindingContext = item;
        //            this.Children.Insert(ItemsSource.IndexOf(item), view);
        //            NotifyItemAdded(view, item);
        //        }

        //        this.UpdateChildrenLayout();
        //        this.InvalidateLayout();
        //    }
        //}

        //public static readonly BindableProperty ItemTemplateProperty =
        //BindableProperty.Create<RepeaterView<T>, DataTemplate>(p => p.ItemTemplate, default(DataTemplate));

        //public DataTemplate ItemTemplate
        //{
        //    get { return (DataTemplate)GetValue(ItemTemplateProperty); }
        //    set { SetValue(ItemTemplateProperty, value); }
        //}
    }

    public class CustomRepeaterView : StackLayout
    {
        public CustomRepeaterView()
        {
            this.Spacing = 0;
            //this.VerticalOptions = LayoutOptions.FillAndExpand;
            //this.HorizontalOptions = LayoutOptions.FillAndExpand;
            //this.BackgroundColor = Color.Red;
        }

        // Correspond to max number of item on the same line if Collection count equal to this value.
        private int MaxValue = 2;
        // Correspond to number of elements on the same line if collection count greater than than MaxValue
        private int NbItemPerLine = 2;
        private int NbGridRows;
        // Style on Grid
        private Xamarin.Forms.GridLength GridColumnAuto = GridLength.Auto;

        public CustomRepeaterView(int _MaxValue, int _NbItemPerLine, Xamarin.Forms.GridLength _GridColumnAuto)
        {
            this.Spacing = 0;
            this.NbItemPerLine = _NbItemPerLine;
            this.MaxValue = _MaxValue;
            this.GridColumnAuto = _GridColumnAuto;
        }

        #region === ItemsSource ===

        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create("ItemsSource", typeof(object), typeof(CustomRepeaterView), default(object), propertyChanged: (d, o, n) => (d as CustomRepeaterView).ItemsSource_Changed(o, n));

        private void ItemsSource_Changed(object oldvalue, object newvalue)
        {
            if (oldvalue is INotifyCollectionChanged)
            {
                (oldvalue as INotifyCollectionChanged).CollectionChanged -= CustomRepeaterView_CollectionChanged;
            }

            if (newvalue is INotifyCollectionChanged)
            {
                (newvalue as INotifyCollectionChanged).CollectionChanged += CustomRepeaterView_CollectionChanged;
            }
            Update();
        }

        public object ItemsSource
        {
            get => (object)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        void CustomRepeaterView_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Update();
        }

        #endregion

        #region === ItemTemplate ===

        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create("ItemTemplate", typeof(DataTemplate), typeof(CustomRepeaterView), default(DataTemplate), propertyChanged: (d, o, n) => (d as CustomRepeaterView).ItemTemplate_Changed(o, n));

        private void ItemTemplate_Changed(object oldvalue, object newvalue)
        {
            Update();
        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        #endregion

        private void Update()
        {
            this.Children.Clear();
            if (ItemTemplate == null || !(ItemsSource is IEnumerable)) return;

            var l_Grid = new Grid()
            {
                //HorizontalOptions = LayoutOptions.Fill,
                RowSpacing = 2
            };

            ////////////////////////////////////////////////////////
            // Determine number of Rows and columns
            ////////////////////////////////////////////////////////
            if ((ItemsSource as IEnumerable).Cast<object>().Count() == MaxValue)
            {
                NbItemPerLine = MaxValue;
                NbGridRows = 1;
            }
            else
            {
                //We set column to auto if there is a difference between MaxValue && NbItemPerLine
                if (MaxValue != NbItemPerLine)
                    GridColumnAuto = GridLength.Auto;

                var collectionCount = Math.Round((decimal)(ItemsSource as IEnumerable).Cast<object>().Count() / NbItemPerLine, 0, MidpointRounding.AwayFromZero);
                NbGridRows = (int)collectionCount;
            }

            ////////////////////////////////////////////////////////
            // Create Grid Rows
            ////////////////////////////////////////////////////////
            for (int i = 0; i < NbGridRows; i++)
            {
                l_Grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }


            ////////////////////////////////////////////////////////
            // Create Grid column
            ////////////////////////////////////////////////////////
            for (int j = 0; j < NbItemPerLine; j++)
            {
                l_Grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridColumnAuto });
            }

            ////////////////////////////////////////////////////////
            // Content creation
            ////////////////////////////////////////////////////////
            var countLoop = 0;
            var currentColumn = 0;
            var currentRow = 0;
            foreach (var l_Item in (ItemsSource as IEnumerable))
            {
                var l_Content = ItemTemplate.CreateContent() as View;
                if (l_Content != null)
                {
                    try
                    {
                        //this.Children.Add(l_Content);

                        if (countLoop % NbItemPerLine != 0)
                        {
                            currentColumn++;
                            l_Content.HorizontalOptions = LayoutOptions.Start;
                        }
                        else
                        {
                            l_Content.HorizontalOptions = LayoutOptions.End;
                            currentColumn = 0;
                            if (countLoop > 0)
                                currentRow++;
                        }

                        l_Grid.Children.Add(l_Content, currentColumn, currentRow);
                        l_Content.BindingContext = l_Item;

                        countLoop++;
                    }
                    catch (Exception l_Exception)
                    {
                        Debug.WriteLine("CustomRepeaterView : " + l_Exception.Message);
                    }
                }
            }

            // Adding new content to the parent stacklayout.
            this.Children.Add(l_Grid);


            UpdateChildrenLayout();
            InvalidateLayout();
        }

    }

}
