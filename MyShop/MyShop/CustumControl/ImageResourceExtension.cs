﻿using System;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyShop.CustumControl
{
    [ContentProperty(nameof(Source))]
    public class ImageResourceExtension : IMarkupExtension
    {
        public string Source { get; set; }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Source == null)
            {
                return null;
            }

            var resourceSplit = Source.Split('|');
            var typeResource = resourceSplit[0];
            var resourceName = resourceSplit[1];

            if (typeResource.Equals(ResourceImageType.res.ToString()))
                return ImageSource.FromResource(resourceName, typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            else
                return null;
        }
    }

    public enum ResourceImageType {
        res,
        file,
        uri
    }
}