﻿using System;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using Xamarin.Forms;

namespace MyShop.CustumControl
{
    public class GradientView : ContentView
    {
        public Color StartColor { get; set; } = Color.Transparent;
        public Color EndColor { get; set; } = Color.Transparent;
        public double StartColorOpacity { get; set; } = 1;
        public double EndColorOpacity { get; set; } = 1;
        public bool Horizontal { get; set; } = false;

        public GradientView()
        {
            SKCanvasView canvasView = new SKCanvasView();
            canvasView.PaintSurface += OnCanvasViewPaintSurface;
            Content = canvasView;
        }

        void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            SKImageInfo info = args.Info;
            SKSurface surface = args.Surface;
            SKCanvas canvas = surface.Canvas;

            canvas.Clear();

            StartColorOpacity = Math.Floor(StartColorOpacity * 255);
            EndColorOpacity = Math.Floor(EndColorOpacity * 255);
            

            var colors = new SKColor[] { StartColor.ToSKColor().WithAlpha((byte)(0xFF * (1 - StartColorOpacity))), EndColor.ToSKColor().WithAlpha((byte)(0xFF * (1 - EndColorOpacity))) };
            SKPoint startPoint = new SKPoint(0, 0);
            SKPoint endPoint = Horizontal ? new SKPoint(info.Width, 0) : new SKPoint(0, info.Height);

            var shader = SKShader.CreateLinearGradient(startPoint, endPoint, colors, null, SKShaderTileMode.Clamp);

            SKPaint paint = new SKPaint
            {
                Style = SKPaintStyle.Fill,
                Shader = shader
            };

            canvas.DrawRect(new SKRect(0, 0, info.Width, info.Height), paint);
        }
    }
}

