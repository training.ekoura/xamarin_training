﻿using Xamarin.Forms;

namespace MyShop.Effects
{
	public class EntryLineColorEffect : RoutingEffect
	{
		public EntryLineColorEffect() : base("MyShop.EntryLineColorEffect")
		{
		}
	}
}
