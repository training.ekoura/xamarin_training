﻿using System;
using System.Text.RegularExpressions;

namespace MyShop.Validations
{
    public class IsValidEmail<T> : IValidationRule<T>
    {
        public IsValidEmail()
        {
        }

        public string ValidationMessage { get ; set ; }

        public bool Check(T value)
        {
            if (value == null)
            {
                return false;
            }
            return Regex.IsMatch((value as string), @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }
    }
}
