﻿namespace MyShop.Validations
{
    public interface IValidity
    {
        bool IsValid { get; set; }
    }
}
