﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MyShop.Services.Interface;
using MyShop.ViewModels;
using Newtonsoft.Json;

namespace MyShop.Services
{

    /// <summary>
    /// /https://firebase.google.com/docs/reference/rest/auth
    /// </summary>
    public class LoginService : ILoginService
    {
        public async Task<UserResponse> Authenticate(UserRequest uRequest, string urlSegment)         {
            //var requestUri = "https://docs.xamarin.com/demo/stock.json";
            var requestUri = string.Format(GlobalSettings.AuthenticateEndPoint, urlSegment);             using (var client = new HttpClient())             {                 var result = await client.PostAsync(requestUri,
                    new StringContent(
                        JsonConvert.SerializeObject(uRequest),
                        Encoding.UTF8, GlobalSettings.DefaultServicesFormat)
                            );

                if (result.IsSuccessStatusCode)
                {
                    var test = result.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<UserResponse>(await result.Content.ReadAsStringAsync());
                }
                else
                {
                    Console.WriteLine(result.Content.ReadAsStringAsync().Result);
                    return null;
                }
            }         }


    }
}
