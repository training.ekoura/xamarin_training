﻿using System;
using System.IO;
using System.Text;

namespace MyShop.Services.Interface
{
    public interface IFileService
    {
        /// <summary>
        /// Supprimer un fichier.
        /// </summary>
        /// <param name="p_FileName"></param>
        void Delete(string p_FileName);

        /// <summary>
        /// Teste si un fichier existe.
        /// </summary>
        /// <param name="p_FileName"></param>
        /// <returns></returns>
        bool Exist(string p_FileName);

        /// <summary>
        /// Donne la date de création ou de modification du fichier.
        /// </summary>
        /// <param name="p_FileName"></param>
        /// <returns></returns>
        DateTime GetLastWriteTime(string p_FileName);

        /// <summary>
        /// Charge un fichier sous la forme d'un objet.
        /// Le fichier est dans le format de sérialisation commun à l'application,
        /// c'est à dire normalement Json.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p_FileName"></param>
        /// <param name="p_DefaultValue"></param>
        /// <returns></returns>
        T LoadObject<T>(string p_FileName, T p_DefaultValue = default(T));

        /// <summary>
        /// Charge un fichier texte.
        /// </summary>
        /// <param name="p_FileName"></param>
        /// <returns></returns>
        System.Text.StringBuilder LoadText(string p_FileName);

        /// <summary>
        /// Enregistre un objet dans un fichier.
        /// Le format de sérialisation est celui commun à l'application,
        /// c'est à dire normalement Json.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p_FileName"></param>
        /// <param name="p_Object"></param>
        void SaveObject<T>(string p_FileName, T p_Object);

        /// <summary>
        /// Enregistre un fichier texte.
        /// </summary>
        /// <param name="p_FileName"></param>
        /// <param name="p_Text"></param>
        void SaveText(string p_FileName, System.Text.StringBuilder p_Text);

        /// <summary>
        /// Ajoute du texte à un fichier.
        /// </summary>
        /// <param name="p_FileName"></param>
        /// <param name="p_Text"></param>
        void AppendText(string p_FileName, StringBuilder p_Text);


        void SaveImage(MemoryStream p_Stream, string p_FileName);

        MemoryStream LoadImage(string p_FileName);

        string[] GetFilesInPath();

        string GetFileWPath(string p_FileName);

    }
}