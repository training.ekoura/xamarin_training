﻿using System;
using System.Threading.Tasks;

namespace MyShop.Services.Interface
{
    public interface IDialogService
    {
        Task ShowAlertAsync(string message, string title, string buttonLabel);
    }
}