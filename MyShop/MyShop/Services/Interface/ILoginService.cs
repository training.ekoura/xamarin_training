﻿using System;
using System.Threading.Tasks;

namespace MyShop.Services.Interface
{
    public interface ILoginService
    {
        Task<UserResponse> Authenticate(UserRequest uRequest, string urlSegment);
    }
}
