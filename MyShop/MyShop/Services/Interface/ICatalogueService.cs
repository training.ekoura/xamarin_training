﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyShop.Models;

namespace MyShop.Services.Interface
{
    public interface ICatalogueService
    {
        Task<Dictionary<string, Product>> GetProducts();
    }
}
