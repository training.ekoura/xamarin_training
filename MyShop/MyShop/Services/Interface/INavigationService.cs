﻿using System;
using System.Threading.Tasks;
using MyShop.ViewModels;
using Xamarin.Forms;

namespace MyShop.Services.Navigation
{
    public interface INavigationService
    {
        BaseViewModel PreviousPageViewModel { get; }
        Task InitializeAsync();                                                 // Effectue une navigation vers l’une des deux pages lorsque l’application est lancée.
        Task NavigateToAsync<TViewModel>() where TViewModel : BaseViewModel;    // Effectue une navigation hiérarchique vers une page spécifiée.
        Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : BaseViewModel; // Effectue une navigation hiérarchique vers une page spécifiée, en passant un paramètre.
        Task RemoveLastFromBackStackAsync();                                    // Supprime la page précédente de la pile de navigation.
        Task RemoveBackStackAsync();                                            // Supprime toutes les pages précédentes de la pile de navigation.
        MasterDetailPage _rootMasterDetailPage { get; set; }
        Task MasterDetailPageNavigation();
        Task MasterDetailPageItemNavigation<TViewModel>() where TViewModel : BaseViewModel;
        Task MasterDetailPageItemNavigation<TViewModel>(object parameter) where TViewModel : BaseViewModel;
    }
}
