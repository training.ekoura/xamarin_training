﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using MyShop.Models;
using MyShop.Services.Interface;
using MyShop.ViewModels;
using Newtonsoft.Json;

namespace MyShop.Services
{
    public class CatalogueService : ICatalogueService
    {
        public CatalogueService()
        {
        }

        public async Task<Dictionary<string, Product>> GetProducts()
        {
            using (var client = new HttpClient())             {
                var result = await client.GetAsync(GlobalSettings.ProductsEndPoint);

                if (result.IsSuccessStatusCode)
                {
                    var test = result.Content.ReadAsStringAsync().Result;
                    Console.WriteLine(test);
                    return JsonConvert.DeserializeObject<Dictionary<string, Product>>(result.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    Console.WriteLine(result.Content.ReadAsStringAsync().Result);
                    return null;
                }
            }
        }
    }
}
