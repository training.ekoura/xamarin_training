﻿using System;
namespace MyShop.Services
{
    public class UserRequest
    {
        public string email { get; set; }
        public string password { get; set; }
        public bool returnSecureToken { get; set; }
    }
}
