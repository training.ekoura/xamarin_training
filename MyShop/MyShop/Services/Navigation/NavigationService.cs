﻿using System;
using System.Globalization;
using System.Reflection;
using System.Threading.Tasks;
using MyShop.ViewModels;
using MyShop.Views;
using MyShop.Views.Navigation;
using Xamarin.Forms;

namespace MyShop.Services.Navigation
{
    public class NavigationService : INavigationService
    {
        private readonly IGlobalSettingsService _settingsService;

        public NavigationService(IGlobalSettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        public BaseViewModel PreviousPageViewModel {
            get
            {
                var mainPage = Application.Current.MainPage as CustomNavigationView;
                var viewModel = mainPage.Navigation.NavigationStack[mainPage.Navigation.NavigationStack.Count - 2].BindingContext;
                return viewModel as BaseViewModel;
            }
        }

        public MasterDetailPage _rootMasterDetailPage { get; set; }

        public Task InitializeAsync()
        {
            if (string.IsNullOrEmpty(_settingsService.AuthIdToken))
                return NavigateToAsync<LoginViewModel>();
            else
                return NavigateToAsync<MainViewModel>();
        }

        public async Task MasterDetailPageNavigation() {

            _rootMasterDetailPage = new MasterDetailPage();
            _rootMasterDetailPage.Master = new MenuView();
            _rootMasterDetailPage.Detail = new CustomNavigationView(new ItemsPage());
            _rootMasterDetailPage.MasterBehavior = MasterBehavior.Popover;
            Application.Current.MainPage = _rootMasterDetailPage;
            await (_rootMasterDetailPage.BindingContext as BaseViewModel).InitializeAsync(null);
        }

        public Task MasterDetailPageItemNavigation<TViewModel>() where TViewModel : BaseViewModel
        {
            return InternalNavigateToAsync(typeof(TViewModel), null);
        }

        public Task MasterDetailPageItemNavigation<TViewModel>(object parameter) where TViewModel : BaseViewModel
        {
            return InternalNavigateToAsync(typeof(TViewModel), parameter);
        }

        private async Task InternalMasterDetailPageItemNavigation(Type viewModelType, object parameter)
        {
            Page page = CreatePage(viewModelType, parameter);
            _rootMasterDetailPage.Detail = new CustomNavigationView(page);
            await (page.BindingContext as BaseViewModel).InitializeAsync(null);
        }

        public Task NavigateToAsync<TViewModel>() where TViewModel : BaseViewModel
        {
            return InternalNavigateToAsync(typeof(TViewModel), null);
        }

        public Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : BaseViewModel
        {
            return InternalNavigateToAsync(typeof(TViewModel), parameter);
        }

        private async Task InternalNavigateToAsync(Type viewModelType, object parameter)
        {
            Page page = CreatePage(viewModelType, parameter);

            if (page is MainView)
            {
                Application.Current.MainPage = new CustomNavigationView(page);
            }
            else
            {
                var navigationPage = Application.Current.MainPage as CustomNavigationView;
                if (navigationPage != null)
                {
                    await navigationPage.PushAsync(page);
                }
                else
                {
                    Application.Current.MainPage = new CustomNavigationView(page);
                }
            }

            await (page.BindingContext as BaseViewModel).InitializeAsync(parameter);
        }


        private Type GetPageTypeForViewModel(Type viewModelType)
        {
            var viewName = viewModelType.FullName.Replace("Model", string.Empty);
            var viewModelAssemblyName = viewModelType.GetTypeInfo().Assembly.FullName;
            var viewAssemblyName = string.Format(CultureInfo.InvariantCulture, "{0}, {1}", viewName, viewModelAssemblyName);
            Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>> viewAssemblyName >>>>>>>>>>>>>>>>>>" + viewAssemblyName);
            var viewType = Type.GetType(viewAssemblyName);
            return viewType;
        }

        private Page CreatePage(Type viewModelType, object parameter)
        {
            Type pageType = GetPageTypeForViewModel(viewModelType);
            if (pageType == null)
            {
                throw new Exception($"Cannot locate page type for {viewModelType}");
            }

            Page page = Activator.CreateInstance(pageType) as Page;
            return page;
        }

        public Task RemoveBackStackAsync()
        {
            var mainPage = Application.Current.MainPage as CustomNavigationView;

            if (mainPage != null)
            {
                for (int i = 0; i < mainPage.Navigation.NavigationStack.Count - 1; i++)
                {
                    var page = mainPage.Navigation.NavigationStack[i];
                    mainPage.Navigation.RemovePage(page);
                }
            }

            return Task.FromResult(true);
        }

        public Task RemoveLastFromBackStackAsync()
        {
            var mainPage = Application.Current.MainPage as CustomNavigationView;

            if (mainPage != null)
            {
                mainPage.Navigation.RemovePage(
                    mainPage.Navigation.NavigationStack[mainPage.Navigation.NavigationStack.Count - 2]);
            }

            return Task.FromResult(true);
        }
    }
}
