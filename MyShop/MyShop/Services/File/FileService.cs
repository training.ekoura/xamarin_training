﻿using System;
using System.IO;
using System.Text;
using MyShop.Services.Interface;

namespace MyShop.Services.File
{
    public class FileService : IFileService
    {
        static string sPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public bool Exist(string p_FileName)
        {
            p_FileName = Path.Combine(sPath, p_FileName);
            return System.IO.File.Exists(p_FileName);
        }

        public string GetFileWPath(string p_FileName)
        {
            return System.IO.Path.Combine(sPath, p_FileName);
        }

        public string[] GetFilesInPath()
        {
            return Directory.GetFiles(sPath);
        }


        public void Delete(string p_FileName)
        {
            p_FileName = Path.Combine(sPath, p_FileName);
            System.IO.File.Delete(p_FileName);
        }

        public void SaveImage(MemoryStream p_Stream, string p_FileName)
        {
            using (FileStream file = new FileStream(System.IO.Path.Combine(sPath, p_FileName), FileMode.Create, System.IO.FileAccess.Write))
            {
                byte[] bytes = new byte[p_Stream.Length];
                p_Stream.Read(bytes, 0, (int)p_Stream.Length);
                file.Write(bytes, 0, bytes.Length);
                p_Stream.Close();
            }
        }


        public MemoryStream LoadImage(string p_FileName)
        {
            MemoryStream ms = new MemoryStream();
            using (FileStream file = new FileStream(System.IO.Path.Combine(sPath, p_FileName), FileMode.Open, FileAccess.Read))
            {
                byte[] bytes = new byte[file.Length];
                file.Read(bytes, 0, (int)file.Length);
                ms.Write(bytes, 0, (int)file.Length);
            }
            return ms;
        }



        public StringBuilder LoadText(string p_FileName)
        {
            p_FileName = Path.Combine(sPath, p_FileName);
            if (System.IO.File.Exists(p_FileName))
            {
                using (var l_Stream = System.IO.File.OpenText(p_FileName))
                {
                    return new StringBuilder(l_Stream.ReadToEnd());
                }
            }
            return new StringBuilder();
        }

        public void SaveText(string p_FileName, StringBuilder p_Text)
        {
            p_FileName = Path.Combine(sPath, p_FileName);
            using (var l_stream = System.IO.File.OpenWrite(p_FileName))
            using (var l_StreamWriter = new System.IO.StreamWriter(l_stream))
            {
                l_StreamWriter.Write(p_Text.ToString());
            }
        }

        public T LoadObject<T>(string p_FileName, T p_DefaultValue = default(T))
        {
            if (Exist(p_FileName))
            {
                return Deserialize<T>(LoadText(p_FileName));
            }
            else
            {
                return p_DefaultValue;
            }
        }

        public void SaveObject<T>(string p_FileName, T p_Object)
        {
            SaveText(p_FileName, Serialize<T>(p_Object));
        }

        public DateTime GetLastWriteTime(string p_FileName)
        {
            p_FileName = Path.Combine(sPath, p_FileName);
            return System.IO.File.GetLastWriteTime(p_FileName);
        }


        public void AppendText(string p_FileName, StringBuilder p_Text)
        {
            p_FileName = Path.Combine(sPath, p_FileName);
            using (var l_StreamWriter = System.IO.File.AppendText(p_FileName))
            {
                l_StreamWriter.Write(p_Text.ToString());
            }
        }


        public StringBuilder Serialize<T>(T p_Object)
        {
            var l_Result = new StringBuilder();
            if (p_Object != null)
            {
                var l_Serializer = Newtonsoft.Json.JsonSerializer.Create();
                using (var l_StreamReader = new System.IO.StringWriter(l_Result))
                {
                    using (var l_JsonWriter = new Newtonsoft.Json.JsonTextWriter(l_StreamReader))
                    {
                        l_Serializer.Serialize(l_JsonWriter, p_Object);
                    }
                }
            }
            return l_Result;
        }

        public T Deserialize<T>(StringBuilder p_Data)
        {
            var l_Serializer = Newtonsoft.Json.JsonSerializer.Create();

            using (var l_StreamReader = new System.IO.StringReader(p_Data.ToString()))
            {
                using (var l_JsonReader = new Newtonsoft.Json.JsonTextReader(l_StreamReader))
                {
                    var l_Result = l_Serializer.Deserialize<T>(l_JsonReader);
                    return (T)l_Result;
                }
            }
        }

    }
}
