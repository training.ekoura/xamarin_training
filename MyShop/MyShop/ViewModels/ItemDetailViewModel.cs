﻿using System;

using MyShop.Models;
using MyShop.ViewModels.Interface;

namespace MyShop.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel, IItemDetailViewModel
    {
        public Product Item { get; set; }
        public ItemDetailViewModel(Product item = null)
        {
            Title = item?.Title;
            Item = item;
        }
    }
}
