﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using MyShop.Models;
using MyShop.Services.Interface;
using MyShop.Services.Navigation;
using MyShop.ViewModels.Interface;
using Xamarin.Forms;

namespace MyShop.ViewModels
{
    public class MainViewModel : BaseViewModel, IMainViewModel
    {
        private ICatalogueService _catalogueService;
        private IGlobalSettingsService _globalSettings;
        private INavigationService _navigationService;
        private IDialogService _dialogService;

        public double _BoxWidth;
        public double BoxWidth { get => _BoxWidth; set => SetProperty(ref _BoxWidth, value); }
        public double _BoxHeight;
        public double BoxHeight{ get => _BoxHeight; set => SetProperty(ref _BoxHeight, value); }


        private Product _CurrentProduct;
        public Product CurrentProduct
        {
            get => _CurrentProduct;
            set
            {
                /*if(value != _CurrentProduct && _CurrentProduct != null)
                    ShowDetailProduct.Execute(null);
                    */
                SetProperty(ref _CurrentProduct, value);
            }
        }

        // Command
        public Command ShowDetailProduct { get; set; }
        //=> new Command(() => ;

        public ObservableCollection<Product> Products { get; set; }

        public override async Task InitializeAsync(object navigationData)
        {
            await PrepareVM();
        }

        public MainViewModel(
                                    ICatalogueService catalogueService,
                                    INavigationService navigationService,
                                    IGlobalSettingsService globalSettings,
                                    IDialogService dialogService
                            )
        {
            _catalogueService = catalogueService;
            _navigationService = navigationService;
            _globalSettings = globalSettings;
            _dialogService = dialogService;


            BoxHeight = 100;// Math.Floor(App.ScreenHeight/15);
            BoxWidth = Math.Floor(App.ScreenWidth/6.8);

            Products = new ObservableCollection<Product>();

            CurrentProduct = new Product();
            ShowDetailProduct = new Command(async () => await ExecuteShowDetailProductCommand());
        }

        private async Task ExecuteShowDetailProductCommand()
        {
            await _navigationService.NavigateToAsync<ItemDetailViewModel>(CurrentProduct);
        }

        public async Task PrepareVM()
        {
            try
            {
                Products.Clear();
                var products = await _catalogueService.GetProducts();

                foreach (var key in products.Keys.ToList())
                {

                    Products.Add(
                        new Product()
                        {
                            Description = products[key].Description,
                            Id = key,
                            ImageUrl = products[key].ImageUrl,
                            IsFavorite = products[key].IsFavorite,
                            Price = products[key].Price,
                            Title = products[key].Title
                        }
                        );
                }

                /*for (int i = 0; i < products.Count; i++)
                {
                    Console.WriteLine(products.Keys);
                }*/

                /*foreach (var item in products)
                {
                    Products.Add(item);
                }*/

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
            }
        }

    }
}
