﻿using System;
using MyShop.Helpers.Interface;
using MyShop.Services;
using MyShop.Services.Interface;
using Xamarin.Forms;

namespace MyShop.ViewModels
{
    public class GlobalSettings : IGlobalSettingsService
    {
        public const string DefaultServicesFormat = "application/json";
        private const string ConfigFileName = "CONFIG_B8A2283CD2944B93B7867110F101AC71.config";

        public UserResponse CurrentUser { get; set; }
        //EndPoints
        public const string AuthenticateEndPoint = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/{0}?key=AIzaSyA2_tMawHxi4skhuoShPl-fAPfHZ6isp2M";
        public const string ProductsEndPoint = "https://treaningflutter.firebaseio.com/products.json";

        private IFileReadWrite _fileService;
        //public static GlobalSettings Instance { get; } = new GlobalSettings();

        public string IdentityEndpointBase { get ; set ; }
        public string Latitude { get ; set ; }
        public string Longitude { get ; set ; }
        public bool AllowGpsLocation { get ; set ; }

        public string AuthIdToken {
            get => CurrentUser.idToken;
            set => CurrentUser.idToken = value;
        }
        public string UserEmail
        {
            get => CurrentUser.idToken;
            set => CurrentUser.idToken = value;
        }
        public string UserRefreshToken
        {
            get => CurrentUser.refreshToken;
            set => CurrentUser.refreshToken = value;
        }
        public string UserLocalId
        {
            get => CurrentUser.localId;
            set => CurrentUser.localId = value;
        }
        public DateTime UserExpiresIn
        {
            get => DateTime.ParseExact(CurrentUser.expiresIn, "yyyy-MM-dd HH:mm:ss,fff",
                                       System.Globalization.CultureInfo.InvariantCulture);
        }
        
        public GlobalSettings(IFileService fileService)
        {
            _fileService = DependencyService.Get<IFileReadWrite>();
            LoadConfig();
        }

        public void LoadConfig()
        {
            if (_fileService.Exist(ConfigFileName))
            {
                CurrentUser = _fileService.LoadObject<UserResponse>(ConfigFileName);
            }
            else
            {
                CurrentUser = new UserResponse();
            }
        }

        public void SaveConfig()
        {
            _fileService.SaveObject<UserResponse>(ConfigFileName, CurrentUser);
        }

    }
}
