﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using MyShop.Models;
using MyShop.Views;
using MyShop.ViewModels.Interface;
using MyShop.Services.Interface;
using MyShop.Services.Navigation;
using System.Linq;

namespace MyShop.ViewModels
{
    public class ItemsViewModel : BaseViewModel, IItemsViewModel
    {
        private ICatalogueService _catalogueService;
        private IGlobalSettingsService _globalSettings;
        private INavigationService _navigationService;
        private IDialogService _dialogService;

        public ObservableCollection<Item> Items { get; set; }
        public Command LoadItemsCommand { get; set; }

        public ObservableCollection<Product> Products { get; set; }

        public override async Task InitializeAsync(object navigationData)
        {
            await PrepareVM();
        }

        public ItemsViewModel(
                                    ICatalogueService catalogueService,
                                    INavigationService navigationService,
                                    IGlobalSettingsService globalSettings,
                                    IDialogService dialogService
                            )
        {
            _catalogueService = catalogueService;
            _navigationService = navigationService;
            _globalSettings = globalSettings;
            _dialogService = dialogService;

            Products = new ObservableCollection<Product>();

            Title = "Browse";
            Items = new ObservableCollection<Item>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            MessagingCenter.Subscribe<NewItemPage, Item>(this, "AddItem", async (obj, item) =>
            {
                var newItem = item as Item;
                Items.Add(newItem);
                await DataStore.AddItemAsync(newItem);
            });
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await DataStore.GetItemsAsync(true);
                foreach (var item in items)
                {
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task PrepareVM()
        {
            try
            {
                Products.Clear();
                var products = await _catalogueService.GetProducts();

                foreach (var key in products.Keys.ToList())
                {

                    Products.Add(
                        new Product()
                        {
                            Description = products[key].Description,
                            Id = key,
                            ImageUrl = products[key].ImageUrl,
                            IsFavorite = products[key].IsFavorite,
                            Price = products[key].Price,
                            Title = products[key].Title
                        }
                        );
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
            }
        }
    }
}