﻿using System;
using MyShop.Services;
using MyShop.Services.Interface;

namespace MyShop.ViewModels
{
    public interface IGlobalSettingsService
    {
        string IdentityEndpointBase { get; set; }
        string Latitude { get; set; }
        string Longitude { get; set; }
        bool AllowGpsLocation { get; set; }
        UserResponse CurrentUser { get; set; }

        string AuthIdToken { get; set; }
        string UserEmail { get; set; }
        string UserRefreshToken { get; set; }
        DateTime UserExpiresIn { get; }
        string UserLocalId { get; set; }

        void LoadConfig();
        void SaveConfig();
    }
}
