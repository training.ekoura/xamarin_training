﻿using System;
using System.Globalization;
using System.Reflection;
using Autofac;
using MyShop.Helpers;
using MyShop.Helpers.Interface;
using MyShop.Services;
using MyShop.Services.Dialog;
using MyShop.Services.File;
using MyShop.Services.Interface;
using MyShop.Services.Navigation;
using MyShop.ViewModels.Interface;
using Xamarin.Forms;

namespace MyShop.ViewModels.Base
{
    public static class ViewModelLocator
    {
        private static IContainer _container;

        public static readonly BindableProperty AutoWireViewModelProperty =
            BindableProperty.CreateAttached("AutoWireViewModel", typeof(bool), typeof(ViewModelLocator), default(bool), propertyChanged: OnAutoWireViewModelChanged);

        public static bool GetAutoWireViewModel(BindableObject bindable)
        {
            return (bool)bindable.GetValue(ViewModelLocator.AutoWireViewModelProperty);
        }

        public static void SetAutoWireViewModel(BindableObject bindable, bool value)
        {
            bindable.SetValue(ViewModelLocator.AutoWireViewModelProperty, value);
        }

        public static bool UseMockService { get; set; }

        static ViewModelLocator()
        {
            var builder = new ContainerBuilder();

            // View models .
            //builder.RegisterInstance(new LoginViewModel());
            builder.RegisterType<LoginViewModel>();
            builder.RegisterType<MainViewModel>();
            builder.RegisterType<ItemsViewModel>();
            builder.RegisterType<ItemDetailViewModel>();
            

            // Services .
            builder.RegisterType<FileService>().As<IFileService>();
            builder.RegisterType<NavigationService>().As<INavigationService>();
            builder.RegisterType<DialogService>().As<IDialogService>();
            builder.RegisterType<LoginService>().As<ILoginService>();
            builder.RegisterType<CatalogueService>().As<ICatalogueService>();
            builder.RegisterType<SerializerService>().As<ISerializerService>();

            // Global Config
            builder.RegisterType<GlobalSettings>().As<IGlobalSettingsService>().SingleInstance();
            


            // Setting config
            _container = builder.Build() ;
        }

        public static void UpdateDependencies(bool useMockServices)
        {
            // Change injected dependencies
            /*if (useMockServices)
            {
                _container.Register<ICatalogService, CatalogMockService>();
                _container.Register<IBasketService, BasketMockService>();
                _container.Register<IOrderService, OrderMockService>();
                _container.Register<IUserService, UserMockService>();
                _container.Register<ICampaignService, CampaignMockService>();

                UseMockService = true;
            }
            else
            {
                _container.Register<ICatalogService, CatalogService>();
                _container.Register<IBasketService, BasketService>();
                _container.Register<IOrderService, OrderService>();
                _container.Register<IUserService, UserService>();
                _container.Register<ICampaignService, CampaignService>();

                UseMockService = false;
            }*/
        }

        public static T Resolve<T>() where T : class
        {
            return _container.Resolve<T>();
        }

        private static void OnAutoWireViewModelChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as Element;
            if (view == null)
            {
                return;
            }

            var viewType = view.GetType();
            var viewName = viewType.FullName.Replace(".Views.", ".ViewModels.");
            var viewAssemblyName = viewType.GetTypeInfo().Assembly.FullName;
            var viewModelName = string.Format(CultureInfo.InvariantCulture, "{0}Model, {1}", viewName, viewAssemblyName);

            var viewModelType = Type.GetType(viewModelName);
            if (viewModelType == null)
            {
                return;
            }
            var viewModel = _container.Resolve(viewModelType);
            view.BindingContext = viewModel;
        }
    }
}