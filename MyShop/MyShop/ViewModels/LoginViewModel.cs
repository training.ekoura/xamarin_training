﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using MyShop.Models;
using MyShop.Services;
using MyShop.Services.Interface;
using MyShop.Services.Navigation;
using MyShop.Validations;
using MyShop.ViewModels.Base;
using MyShop.ViewModels.Interface;
using Xamarin.Forms;

namespace MyShop.ViewModels
{
    public class LoginViewModel : BaseViewModel, ILoginViewModel
    {
        // Properties
        private ILoginService _loginService;
        private IGlobalSettingsService _globalSettings;
        private INavigationService _navigationService;
        private IDialogService _dialogService;

        
        private ValidatableObject<string> _email;
        private ValidatableObject<string> _password;
        public ValidatableObject<string> Email
        {
            get { return _email;}
            set { SetProperty(ref _email, value); }
        }

        public ValidatableObject<string> Password
        {
            get {return _password;}
            set { SetProperty(ref _password, value); }
        }

        private bool _IsShowLogin = true;
        public bool IsShowLogin
        {
            get { return _IsShowLogin; }
            set { SetProperty(ref _IsShowLogin, value); }
        }

        private string _LoginOrSignUpLabelText = "SIGNUP INSTEAD";
        public string LoginOrSignUpLabelText
        {
            get { return _LoginOrSignUpLabelText; }
            set { SetProperty(ref _LoginOrSignUpLabelText, value); }
        }

        private string _ErrorLoginMessage = "";
        public string ErrorLoginMessage
        {
            get => _ErrorLoginMessage;
            set => SetProperty(ref _ErrorLoginMessage, value);
        }

        // Command
        public Command SignUp { get; set; }
        public Command LogIn { get; set; }
        public Command IsLoginOrSignUp { get; set; }
        public Command ValidatePasswordCommand => new Command(() => ValidatePassword());
        public Command ValidateEmailCommand => new Command(() => ValidateEmail());

        

        public LoginViewModel(
                                    ILoginService loginService,
                                    INavigationService navigationService,
                                    IGlobalSettingsService globalSettings,
                                    IDialogService dialogService)
        {
            _loginService = loginService;
            _navigationService = navigationService;
            _globalSettings = globalSettings;
            _dialogService = dialogService;

            
            SignUp = new Command(async () => await ExecuteSignUpCommand());
            LogIn = new Command(async () => await ExecuteLogInCommand());
            IsLoginOrSignUp = new Command(() => ExecuteIsLoginOrSignUptCommand());
            _email = new ValidatableObject<string>();
            _password = new ValidatableObject<string>();

            AddValidations();
        }

        private void ExecuteIsLoginOrSignUptCommand()
        {
            ErrorLoginMessage = "";
            IsShowLogin = !IsShowLogin;
            if(IsShowLogin)
                LoginOrSignUpLabelText = "SIGNUP INSTEAD";
            else
                LoginOrSignUpLabelText = "LOGIN INSTEAD";
        }

        private async Task ExecuteLogInCommand()
        {
            if (IsBusy || !Validate())
                return;

            IsBusy = true;

            try
            {
                var uRequest = new UserRequest()
                {
                    email = _email.Value,
                    password = Password.Value,
                    returnSecureToken = true
                };

                var urlSegment = "verifyPassword";

                var infosUser = await _loginService.Authenticate(uRequest, urlSegment);

                if (infosUser != null)
                {
                    ErrorLoginMessage = "";
                    _globalSettings.CurrentUser.idToken = infosUser.idToken;
                    _globalSettings.CurrentUser.email = infosUser.email;
                    _globalSettings.CurrentUser.expiresIn = DateTime.Now.AddSeconds(double.Parse(infosUser.expiresIn)).ToString();
                    _globalSettings.CurrentUser.refreshToken = infosUser.refreshToken;
                    _globalSettings.CurrentUser.localId = infosUser.localId;

                    _globalSettings.SaveConfig();

                    await _navigationService.NavigateToAsync<MainViewModel>();
                }
                else {
                    ErrorLoginMessage = "Login or Password invalid";
                    await _dialogService.ShowAlertAsync("Login or Password invalid", "Error", "OK");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task ExecuteSignUpCommand()
        {
            if (IsBusy || !Validate())
                return;

            IsBusy = true;


            try
            {
                var uRequest = new UserRequest()
                {
                    email = _email.Value,
                    password = Password.Value,
                    returnSecureToken = true
                };

                var urlSegment = "signupNewUser";

                var infosUser = await _loginService.Authenticate(uRequest, urlSegment);

                if (infosUser != null)
                {
                    ErrorLoginMessage = "";
                    _globalSettings.CurrentUser.idToken = infosUser.idToken;
                    _globalSettings.CurrentUser.email = infosUser.email;
                    _globalSettings.CurrentUser.expiresIn = DateTime.Now.AddSeconds(double.Parse(infosUser.expiresIn)).ToString();
                    _globalSettings.CurrentUser.refreshToken = infosUser.refreshToken;
                    _globalSettings.CurrentUser.localId = infosUser.localId;

                    _globalSettings.SaveConfig();

                    await _navigationService.NavigateToAsync<MainViewModel>();
                }
                else {
                    ErrorLoginMessage = "Login or Password invalid";
                    await _dialogService.ShowAlertAsync("Login or Password invalid", "Error", "OK");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }


        private void AddValidations()
        {
            _email.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "A Email is required." });
            _email.Validations.Add(new IsValidEmail<string> { ValidationMessage = "Enter a valid email address. (ex: john@ekoura.com)" });


            _password.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "A password is required." });
            _password.Validations.Add(new IsValidPassword<string>() );


        }

        private bool ValidatePassword()
        {
            return _password.Validate();
        }

        private bool ValidateEmail()
        {
            return _email.Validate();
        }

        private bool Validate()
        {
            bool isValidUser = ValidateEmail();
            bool isValidPassword = ValidatePassword();

            return isValidUser && isValidPassword;
        }
    }
}
