﻿using System;
namespace MyShop.Models
{
    public class Product
    {
        public String   Id { get; set; }
        public String   Title { get; set; }
        public String   Description { get; set; }
        public double   Price { get; set; }
        public String   ImageUrl { get; set; }
        public bool     IsFavorite { get; set; }
    }
}
