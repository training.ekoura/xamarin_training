﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyShop.Models
{
    public enum MenuItemType
    {
        Shop,
        Orders,
        Manage_Products,
        Logout
    }

    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
