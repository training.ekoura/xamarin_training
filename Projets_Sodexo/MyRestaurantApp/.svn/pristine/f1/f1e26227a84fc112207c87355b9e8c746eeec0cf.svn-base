﻿<?xml version="1.0" encoding="utf-8" ?>
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:ctrl="clr-namespace:Sodexo.Controls;assembly=Sodexo.MicroFramework.XamarinForms"
             xmlns:mfx="clr-namespace:Sodexo;assembly=Sodexo.MicroFramework.XamarinForms"
             xmlns:local="clr-namespace:MyRestauApp.Common.Views;assembly=MyRestauApp.Common.Views"
             xmlns:sodexobh="clr-namespace:Sodexo.Behaviors;assembly=Sodexo.MicroFramework.XamarinForms"
             xmlns:contentViews="clr-namespace:MyRestauApp.Common.Views;assembly=MyRestauApp.Common.Views"
             Style="{DynamicResource ContentPageStyle}"
             x:Class="MyRestauApp.Common.Views.OrderDetailView">

  <ContentPage.Resources>
    <ResourceDictionary>
      <mfx:ImageNameConverter x:Key="ImageNameConverter"/>
      <mfx:StringToColorConverter x:Key="StringToColorConverter"/>
    </ResourceDictionary>
  </ContentPage.Resources>
  
  <Grid RowSpacing="0" Style="{DynamicResource DefaultGridStyle}">
    <Grid.Behaviors>
      <sodexobh:ActivityIndicatorBehavior/>
    </Grid.Behaviors>
    
    <Grid.RowDefinitions>
      <RowDefinition Height="2*"/>
      <RowDefinition Height="8*"/>
      <RowDefinition Height="*"/>
    </Grid.RowDefinitions>

    <!--region Header-->
    <contentViews:Header Grid.Row="0" Style="{DynamicResource DefaultContentViewStyle}" HeaderType="IsCommande" HeaderContentType="IsText" />
    <!--endregion-->
    
    <!--region Content-->
    <Grid x:Name="subGrid" Grid.Row="1" Grid.RowSpacing="0" BackgroundColor="{DynamicResource GerantSecondaryPageBackgroundColor}">
      <Grid.RowDefinitions>
        <RowDefinition Height="Auto"/>
        <RowDefinition Height="*"/>
      </Grid.RowDefinitions>

      <!--region Entête-->
      <Grid Grid.Row="0" Grid.ColumnSpacing="0" Padding="20,22" BackgroundColor="{DynamicResource GerantSecondaryPageBackgroundColor}">
        <Grid.ColumnDefinitions>
          <ColumnDefinition Width="*"/>
          <ColumnDefinition Width="Auto"/>
          <ColumnDefinition Width="*"/>
          <ColumnDefinition Width="Auto"/>
          <ColumnDefinition Width="*"/>
        </Grid.ColumnDefinitions>
        
        <!--region Colonne Plats/Produits-->
        <Grid Grid.Column="0" Grid.RowSpacing="0" HorizontalOptions="FillAndExpand">
          <Grid.RowDefinitions>
            <RowDefinition Height="*"/>
            <RowDefinition Height="*"/>
          </Grid.RowDefinitions>

          <Label Grid.Row="0" Text="{Binding Order.FormattedLabelPlat}" TextColor="{DynamicResource SecondaryTextColor}" FontSize="Small" FontAttributes="Bold"
                 HorizontalOptions="CenterAndExpand"  VerticalOptions="End" VerticalTextAlignment="End"/>

          <Label Grid.Row="1" Text="{Binding Order.FormattedLabelProduit}" TextColor="{DynamicResource SecondaryTextColor}" FontSize="Small" FontAttributes="Bold"
                 HorizontalOptions="CenterAndExpand" VerticalOptions="Start" VerticalTextAlignment="Start"/>
        </Grid>
        <!--endregion-->
        
        <!--region Separateur-->
        <Image x:Name="firstSeparator"
               Grid.Column="1"
               Source="{local:ImageResource MyRestauApp.Common.Views.Images.VerticalDottedSeparator.png}"
               VerticalOptions="CenterAndExpand"
               HorizontalOptions="Center"/>
        <!--endregion-->

        <!--region Colonne Prix Total-->
        <StackLayout Grid.Column="2" HorizontalOptions="FillAndExpand" VerticalOptions="FillAndExpand">
          <Label Text="{Binding Order.PrixTotalTTCFormat}" TextColor="{DynamicResource SecondaryTextColor}" FontSize="Large"
                 HorizontalOptions="FillAndExpand" VerticalOptions="FillAndExpand" VerticalTextAlignment="Center" HorizontalTextAlignment="Center"  />
        </StackLayout>
        <!--endregion-->

        <!--region Separateur-->
        <Image x:Name="secondSeparator"
               Grid.Column="3"
               Source="{local:ImageResource MyRestauApp.Common.Views.Images.VerticalDottedSeparator.png}"
               VerticalOptions="CenterAndExpand"
               HorizontalOptions="Center"/>
        <!--endregion-->
        
        <!--region Colonne Bouton Prêt-->
        <StackLayout Grid.Column="4" HorizontalOptions="FillAndExpand" VerticalOptions="FillAndExpand">
          <ctrl:CustomButton Grid.Column="2" ButtonType="Rounded" CenteredItems="False" HorizontalOptions="FillAndExpand" VerticalOptions="FillAndExpand"
                                   ImageButton="ImageOnRight" ImageButtonSource="Sodexo.Images.Gerant.arrow.png" VerticalImageOption="Center"
                                  TapCommand="{Binding ViewStatutPopInCommand}" TapCommandParameter="{Binding .}"  HorizontalLabelOption="Start" VerticalLabelOption="Center" ButtonLabel="{Binding Order.Statut.LibelleGerant}" ButtonColor="{Binding Order.Statut.StatutCodeCouleur, Converter={StaticResource StringToColorConverter}}"/>
          <StackLayout.Padding>
            <OnPlatform x:TypeArguments="Thickness" iOS="40,0,0,0" Android="30,0,0,0" WinPhone="35,0,0,0"  />
          </StackLayout.Padding>
          <!--<Image Source="{local:ImageResource MyRestauApp.Common.Views.Images.ImgBtPret.png}" HorizontalOptions="FillAndExpand" VerticalOptions="FillAndExpand">
          </Image>
          <StackLayout.GestureRecognizers>
            <TapGestureRecognizer Command="{Binding ViewStatutPopInCommand}" CommandParameter="{Binding .}"/>
          </StackLayout.GestureRecognizers>-->
        </StackLayout>
        <!--endregion-->
        
      </Grid>
      <!--endregion-->
      
      <!--region Scrollable Content-->
      <ScrollView x:Name="repeaterScrollContainer" Grid.Row="1" Orientation="Vertical">
          
        <!--region RepeaterComposantes-->
        <ctrl:RepeaterView ItemsSource="{Binding Order.ArticlesByComposanteRecettes}" VerticalOptions="Fill">
          <ctrl:RepeaterView.ItemTemplate>
            <DataTemplate>

              <Grid RowSpacing="0" VerticalOptions="Fill" HorizontalOptions="FillAndExpand">
                <Grid.RowDefinitions>
                  <RowDefinition Height="60"/>
                  <RowDefinition Height="Auto"/>
                </Grid.RowDefinitions>
                  
                <!--region Header Composantes-->
                <StackLayout Grid.Row="0" HorizontalOptions="FillAndExpand" VerticalOptions="FillAndExpand" BackgroundColor="{DynamicResource GerantPageBackgroundColor}" Padding="20,0,0,0">
                  <Label Text="{Binding Nom}" TextColor="{DynamicResource TertiaryTextColor}" FontSize="Small" FontAttributes="Bold" HorizontalOptions="StartAndExpand" VerticalOptions="CenterAndExpand"/>
                </StackLayout>
                <!--endregion-->
                  
                <!--region RepeaterRecettes-->
                <ctrl:RepeaterView Grid.Row="1"  ItemsSource="{Binding Articles}" HasDottedLines="True" ItemSpacing="10" BackgroundColor="{DynamicResource RepeaterColor}" Padding="0,10">
                  <ctrl:RepeaterView.ItemTemplate>
                    <DataTemplate>
                        
                      <Grid RowSpacing="0" ColumnSpacing="0" HorizontalOptions="FillAndExpand" VerticalOptions="Fill" Padding="15,10">
                        <Grid.ColumnDefinitions>
                          <ColumnDefinition Width="4*"/>
                          <ColumnDefinition Width="*"/>
                        </Grid.ColumnDefinitions>
                        <Grid.RowDefinitions>
                          <RowDefinition Height="Auto"/>
                          <RowDefinition Height="Auto"/>
                        </Grid.RowDefinitions>
                          
                        <Label Grid.Row="0" Grid.Column="0" Text="{Binding LibelleCommercial}" TextColor="{DynamicResource SecondaryTextColor}" FontSize="Medium" HorizontalOptions="FillAndExpand" VerticalOptions="Fill" HorizontalTextAlignment="Start"  />
                        <Label Grid.Row="1" Grid.Column="0" Text="{Binding LibelleTechnique}"  TextColor="{DynamicResource TertiaryTextColor}" FontSize="Small" HorizontalOptions="FillAndExpand" VerticalOptions="Fill" HorizontalTextAlignment="Start"  />
                        <Label Grid.Row="0" Grid.Column="1" Text="{Binding Quantite}" TextColor="{DynamicResource SecondaryTextColor}" FontAttributes="Bold" FontSize="Medium" HorizontalOptions="FillAndExpand" VerticalOptions="Fill" HorizontalTextAlignment="End" VerticalTextAlignment="Start" />
                      </Grid>
                        
                    </DataTemplate>
                  </ctrl:RepeaterView.ItemTemplate>
                </ctrl:RepeaterView>
                <!--endregion-->
                  
              </Grid>                
            </DataTemplate>
          </ctrl:RepeaterView.ItemTemplate>
        </ctrl:RepeaterView>
        <!--endregion-->
          
      </ScrollView>
      <!--endregion-->
      
    </Grid>
    <!--endregion-->

    <!--region Annuler la commande-->

    <!-- Dans le code XAML ou C#, ButtonLabel et TapCommand contiennent le Rebuild(). Il faut donc que l'une de ces 2 propriétés soit la dernière déclarée.-->
    <ctrl:CustomButton x:Name="annulerButton" IsVisible="{Binding IsVisibleButtonCancel}" Grid.Row="2" CenteredItems="True" ButtonType="FixedWithImage"
                       ImageButton="ImageOnLeft" ImageButtonSource="Sodexo.Images.Gerant.Basket.png" VerticalImageOption="Fill" HorizontalImageOption="Fill" 
                       VerticalLabelOption="CenterAndExpand" HorizontalLabelOption="StartAndExpand" IsLabelUnderlined="True" ButtonLabel="Annuler la commande" TapCommand="{Binding CancelOrderCommand}"/>
    <!--endregion-->
    
  </Grid>
</ContentPage>