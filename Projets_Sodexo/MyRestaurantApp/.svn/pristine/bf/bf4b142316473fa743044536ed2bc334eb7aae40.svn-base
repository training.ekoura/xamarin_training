
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using MyRestauApp.Common.DAOServices;
using MyRestauApp.Common.DAOServices.Models;
using MyRestauApp.Common.DAOServices.Requests;
using MyRestauApp.Common.ViewModels.ItemsViewModel;
using Sodexo;
using MyRestauApp.Common.DAOServices.Services;
using System.Globalization;

namespace MyRestauApp.Common.ViewModels
{
	public partial class OrderDetailViewModel : OrderDetailViewModelBase, IOrderDetailViewModel
	{
        #region Proprietes
        private ICommandeService CommandService;
        private IGlobalConfigService GlobalConfigService;
        private CultureInfo Culture;
        #endregion

        #region Initialisation
        protected override void OnInitialize()
        {
            base.OnInitialize();
            ResolveInitiliazation();
        }

        public void ResolveInitiliazation()
        {
            CommandService = Sodexo.Framework.Services.Container().Resolve<ICommandeService>();
            GlobalConfigService = Sodexo.Framework.Services.GlobalConfigService();
            Culture = new CultureInfo(Properties.Resources.CultureInfoCurrentCultureName.Replace("_", "-"));
        }
        #endregion End Initialisation

        // Appel� � partir de la vue OrderListView via DashboardViewModel
        #region Prepare 
        //TODO-Rimeh g�rer le cas offline
        public async Task<bool> PrepareOrderDetailVM(int commandId)
        {
            try
            {
                BeginWork();
                CommandService = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.DAOServices.Services.ICommandeService>();
                GlobalConfigService = Sodexo.Framework.Services.GlobalConfigService();
                OrderId = commandId;
                var commandServiceResult = await CommandService.GetCommandeGerantAsync(GlobalConfigService.Data.Token.TokenVal, commandId);
                if (commandServiceResult.Success)
                {
                    if (commandServiceResult.Data != null)
                    {
                        CommandDetail = new CommandDetailViewModelItem().SetCommandDetailEncapsulation(commandServiceResult.Data);
                        HeaderText = CommandDetail.ZoneRestaurationNom;
                        CommandDetail.FormattedDateForView = DateTime.ParseExact(commandServiceResult.Data.DateEffectiveFormat, "dd/MM/yyyy HH:mm", Culture).Date.ToString("dd.MM");
                        CommandDetail.FormattedLabelForView = string.Format(Properties.Resources.LabelCommandeForView, commandServiceResult.Data.ArticlesPlatsCount, commandServiceResult.Data.ArticlesCount);
                        IsVisibleButtonCancel = (CommandDetail.Statut.LogicValue == Properties.Resources.StatutLogicalValueCancel) ? false : true;
                        return true;
                    }
                    else
                        return false;
                }
                else
                {                    
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, commandServiceResult.ExceptionMessage);
                    return false;
                }                
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                return false;
            }
            finally
            {
                EndWork();
            }
        }
        #endregion

        #region M�thodes
        // Update de commande detail pour OrderDetailView
        public async Task UpdateOrderDetail(int orderId)
        {
            try
            {
                BeginWork();
                // Update de commande detail pour OrderDetailView
                CommandService = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.DAOServices.Services.ICommandeService>();
                var commandResult = await CommandService.GetCommandeGerantAsync(GlobalConfigService.Data.Token.TokenVal, orderId);
                if (commandResult.Success)
                {
                    if (commandResult.Data != null)
                        CommandDetail = new CommandDetailViewModelItem().SetCommandDetailEncapsulation(commandResult.Data);
                }
                else
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, commandResult.ExceptionMessage);
            }
            catch (Exception e)
            {
                //Afficher message d'erreur
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        #endregion

        #region Commandes

        protected override async void OnViewStatutPopInCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                var popInVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IPopInViewModel>();
                var success = await popInVM.PrepareVMForChangeStatut((Commande)p_Parameter, true);
                if(success)
                    Sodexo.Framework.Services.InteractionService().ViewPopIn(popInVM);
            }
            catch (Exception ex)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
                EndWork();
            }
        }
       
        protected override async void OnCancelOrderCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                if (await Sodexo.Framework.Services.InteractionService().Confirm(Properties.Resources.MessageForCancelOrder, Properties.Resources.MessageForCancelOrder))
                {
                    // appel du webservice pour annuler la commande
                    var commandServiceResult = await CommandService.UpdateCommandeStatutAsync(GlobalConfigService.Data.Token.TokenVal, OrderId, Constants.CancelLogicValue);
                    if (commandServiceResult.Success)
                    {
                        // Mettre � jour la liste des stocks et la liste des commandes
                        var DashBoardVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IDashboardViewModel>();
                       // var resultUpdateDashboardView = await DashBoardVM.UpdateDashboardView(dateCommand);
                        // on ferme OrderDetailView
                        Sodexo.Framework.Services.InteractionService().Close();
                    }
                    else
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, commandServiceResult.ExceptionMessage);
                }
            }
            catch (Exception e)
            {
                //Afficher message d'erreur
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        #endregion


    
    }
}
