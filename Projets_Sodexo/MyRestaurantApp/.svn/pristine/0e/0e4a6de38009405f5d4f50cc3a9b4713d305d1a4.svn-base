﻿<?xml version="1.0" encoding="utf-8" ?>
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:ctrl="clr-namespace:Sodexo.Controls;assembly=Sodexo.MicroFramework.XamarinForms"
             xmlns:mfx="clr-namespace:Sodexo;assembly=Sodexo.MicroFramework.XamarinForms"
             xmlns:local="clr-namespace:MyRestauApp.Common.Views;assembly=MyRestauApp.Common.Views"
             xmlns:sodexobh="clr-namespace:Sodexo.Behaviors;assembly=Sodexo.MicroFramework.XamarinForms"
             xmlns:contentViews="clr-namespace:MyRestauApp.Common.Views;assembly=MyRestauApp.Common.Views"
             Style="{DynamicResource ContentPageStyle}"
             x:Class="MyRestauApp.Common.Views.OrderDetailView">

  <ContentPage.Resources>
    <ResourceDictionary>
      <mfx:ImageNameConverter x:Key="ImageNameConverter"/>
    </ResourceDictionary>
  </ContentPage.Resources>
  
  <Grid RowSpacing="0" Style="{DynamicResource DefaultGridStyle}">
    <Grid.Behaviors>
      <sodexobh:ActivityIndicatorBehavior/>
    </Grid.Behaviors>
    
    <Grid.RowDefinitions>
      <RowDefinition Height="2*"/>
      <RowDefinition Height="9*"/>
      <RowDefinition Height="Auto"/>
    </Grid.RowDefinitions>

    <!--region Header-->
    <contentViews:Header Grid.Row="0" Style="{DynamicResource DefaultContentViewStyle}" HeaderType="IsCommande" HeaderContentType="IsText" />
    <!--endregion-->
    
    <!--region Content-->
    <Grid Grid.Row="1" Grid.RowSpacing="0" BackgroundColor="{DynamicResource GerantSecondaryPageBackgroundColor}">
      <Grid.RowDefinitions>
        <RowDefinition Height="*"/>
        <RowDefinition Height="5*"/>
      </Grid.RowDefinitions>

      <!--region Entête-->
      <Grid Grid.Row="0" Grid.ColumnSpacing="0" Padding="20,22" BackgroundColor="{DynamicResource GerantSecondaryPageBackgroundColor}">
        <Grid.ColumnDefinitions>
          <ColumnDefinition Width="*"/>
          <ColumnDefinition Width="Auto"/>
          <ColumnDefinition Width="*"/>
          <ColumnDefinition Width="Auto"/>
          <ColumnDefinition Width="*"/>
        </Grid.ColumnDefinitions>
        
        <!--region Colonne Plats/Produits-->
        <Grid Grid.Column="0" Grid.RowSpacing="0" HorizontalOptions="FillAndExpand">
          <Grid.RowDefinitions>
            <RowDefinition Height="*"/>
            <RowDefinition Height="*"/>
          </Grid.RowDefinitions>

          <Label Grid.Row="0" Text="{Binding CommandDetail.ArticlesPlatsCount}" TextColor="{DynamicResource SecondaryTextColor}" FontSize="Small" FontAttributes="Bold"
                 HorizontalOptions="CenterAndExpand"  VerticalOptions="End" VerticalTextAlignment="End"/>

          <Label Grid.Row="1" Text="{Binding CommandDetail.ArticlesCount}" TextColor="{DynamicResource SecondaryTextColor}" FontSize="Small" FontAttributes="Bold"
                 HorizontalOptions="CenterAndExpand" VerticalOptions="Start" VerticalTextAlignment="Start"/>
        </Grid>
        <!--endregion-->
        
        <!--region Separateur-->
        <Image x:Name="firstSeparator"
               Grid.Column="1"
               Source="{local:ImageResource MyRestauApp.Common.Views.Images.VerticalDottedSeparator.png}"
               VerticalOptions="CenterAndExpand"
               HorizontalOptions="Center"/>
        <!--endregion-->

        <!--region Colonne Prix Total-->
        <StackLayout Grid.Column="2" HorizontalOptions="FillAndExpand" VerticalOptions="FillAndExpand">
          <Label Text="360" TextColor="{DynamicResource SecondaryTextColor}" FontSize="40" HorizontalOptions="CenterAndExpand" VerticalOptions="Center"/>
          
        </StackLayout>
        <!--endregion-->

        <!--region Separateur-->
        <Image x:Name="secondSeparator"
               Grid.Column="3"
               Source="{local:ImageResource MyRestauApp.Common.Views.Images.VerticalDottedSeparator.png}"
               VerticalOptions="CenterAndExpand"
               HorizontalOptions="Center"/>
        <!--endregion-->
        
        <!--region Colonne Bouton Prêt-->
        <StackLayout Grid.Column="4" HorizontalOptions="FillAndExpand" VerticalOptions="FillAndExpand" Padding="10,0">
          <Image Source="{local:ImageResource MyRestauApp.Common.Views.Images.ImgBtPret.png}" HorizontalOptions="FillAndExpand" VerticalOptions="FillAndExpand">
          </Image>
          <StackLayout.GestureRecognizers>
            <TapGestureRecognizer Command="{Binding ViewStatutPopInCommand}" CommandParameter="{Binding .}"/>
          </StackLayout.GestureRecognizers>
        </StackLayout>
        <!--endregion-->
        
      </Grid>
      <!--endregion-->
      
      <!--region Scrollable Content-->
      <ScrollView Grid.Row="1" Orientation="Vertical">
          
        <!--region RepeaterComposantes-->
        <ctrl:RepeaterView  ItemsSource="{Binding CommandDetail.ArticlesByComposanteRecettes}" VerticalOptions="Fill">
          <ctrl:RepeaterView.ItemTemplate>
            <DataTemplate>

              <Grid RowSpacing="0">
                <Grid.RowDefinitions>
                  <RowDefinition Height="60"/>
                  <RowDefinition Height="Auto"/>
                </Grid.RowDefinitions>
                  
                <!--region Header Composantes-->
                <StackLayout Grid.Row="0" HorizontalOptions="FillAndExpand" VerticalOptions="FillAndExpand" Padding="15" BackgroundColor="{DynamicResource GerantPageBackgroundColor}">
                  <Label Text="{Binding Nom}" TextColor="{DynamicResource TertiaryTextColor}" FontSize="Small" FontAttributes="Bold" HorizontalOptions="StartAndExpand" VerticalOptions="CenterAndExpand"/>
                </StackLayout>
                <!--endregion-->
                  
                <!--region RepeaterRecettes-->
                <ctrl:RepeaterView Grid.Row="1"  ItemsSource="{Binding Articles}" HasDottedLines="True" ItemSpacing="10" BackgroundColor="{DynamicResource RepeaterColor}" Padding="0,10">
                  <ctrl:RepeaterView.ItemTemplate>
                    <DataTemplate>
                        
                      <Grid Grid.Row="0" RowSpacing="0" ColumnSpacing="0" HorizontalOptions="Fill" VerticalOptions="Fill" Padding="15,10">
                        <Grid.ColumnDefinitions>
                          <ColumnDefinition Width="*"/>
                          <ColumnDefinition Width="Auto"/>
                        </Grid.ColumnDefinitions>
                        <Grid.RowDefinitions>
                          <RowDefinition Height="Auto"/>
                          <RowDefinition Height="Auto"/>
                        </Grid.RowDefinitions>
                          
                        <Label Grid.Row="0" Grid.Column="0" Text="{Binding LibelleCommercial}" TextColor="{DynamicResource SecondaryTextColor}" FontSize="26" HorizontalOptions="StartAndExpand" VerticalOptions="Fill"/>
                        <Label Grid.Row="0" Grid.Column="1" Text="{Binding Quantite}" TextColor="{DynamicResource SecondaryTextColor}" FontSize="26" HorizontalOptions="StartAndExpand" VerticalOptions="Fill"/>
                        <Label Grid.Row="1" Grid.Column="0" Grid.ColumnSpan="2" Text="{Binding LibelleTechnique}" IsVisible="{Binding doubleRowVisibility}" TextColor="{DynamicResource TertiaryTextColor}" FontSize="24" HorizontalOptions="StartAndExpand" VerticalOptions="Fill"/>
                      </Grid>
                        
                    </DataTemplate>
                  </ctrl:RepeaterView.ItemTemplate>
                </ctrl:RepeaterView>
                <!--endregion-->
                  
              </Grid>                
            </DataTemplate>
          </ctrl:RepeaterView.ItemTemplate>
        </ctrl:RepeaterView>
        <!--endregion-->
          
      </ScrollView>
      <!--endregion-->
      
    </Grid>
    <!--endregion-->

    <!--region Annuler la commande-->
    <!--<contentViews:StackButtonImage Grid.Row="2"/>-->
    <!--<ctrl:CustomButton Grid.Row="2" CenteredItems="True"
                       ImageButton="ImageOnLeft" ImageButtonSource="Sodexo.Images.Gerant.Basket.png" VerticalImageOption="CenterAndExpand" HorizontalImageOption="End"
                       ButtonLabel="Annuler la commande" IsLabelUnderlined="True" VerticalLabelOption="CenterAndExpand" HorizontalLabelOption="StartAndExpand"/>-->
    <!--endregion-->

  </Grid>
</ContentPage>