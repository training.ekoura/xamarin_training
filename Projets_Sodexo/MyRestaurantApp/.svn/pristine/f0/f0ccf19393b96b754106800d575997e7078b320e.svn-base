﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyRestauApp.Common.ViewModels.ItemsViewModel;
using Sodexo;
using MyRestauApp.Common.DAOServices.Models;

namespace MyRestauApp.Common.ViewModels
{
    public interface IDashboardViewModel : IViewModel
    {
        Sodexo.IDelegateCommand ChangeDateCommand { get; }
        Sodexo.IDelegateCommand ChangeStatutCommand { get; }
        CommandDashboardViewModel CommandDashBoard { get; set; }
        CommandDetailViewModelItem CommandDetail { get; set; }
        string DateDashBoard { get; set; }
        string DateDashBoardFormattedLabel { get; set; }
        bool IsAddQuantity { get; set; }
        bool IsStockDispo { get; set; }
        bool IsVisibleButtonCancelCommand { get; set; }
        System.Collections.ObjectModel.ObservableCollection<StockArticleViewModelItem> ListStock { get; }
        Sodexo.IDelegateCommand LoadListOrdersCommand { get; }
        Sodexo.IDelegateCommand OpenDetailViewCommand { get; }
        StockArticleViewModelItem SelectedStock { get; set; }
        string ZoneRestaurationId { get; set; }

        //Les méthodes
        Task<bool> PrepareDashboardVM(bool isOffline);
        Task<bool> PrepareCommandeDashboard(string date);
        Task<bool> PrepareStock(string date);
        Task<bool> UpdateDashboardView(string date);
        Task UpdateCommandes(int commandeId, StatutItemViewModel selectedStatut);
        Task ChangeStockQuantity(object selectedStock, bool isAddQuantity);
    }
}
