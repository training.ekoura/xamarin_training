﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML OrderDetailViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  Sodexo.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;

namespace MyRestauApp.Common.ViewModels
{
	public abstract partial class OrderDetailViewModelBase : Sodexo.ViewModel
	{
		protected override void OnInitialize()
		{
			base.OnInitialize();

			// Initialisation de la commande ViewStatutPopInCommand.
			ViewStatutPopInCommand = new DelegateCommand(ViewStatutPopInCommand_CanExecute, ViewStatutPopInCommand_Execute);
			// Initialisation de la commande CancelOrderCommand.
			CancelOrderCommand = new DelegateCommand(CancelOrderCommand_CanExecute, CancelOrderCommand_Execute);
		}

		#region === Propriétés ===

			#region === Propriété : OrderId ===

				public const string OrderId_PROPERTYNAME = "OrderId";

				private int _OrderId;
				///<summary>
				/// Propriété : OrderId
				///</summary>
				public int OrderId
				{
					get
					{
						return GetValue<int>(() => _OrderId);
					}
					set
					{
						SetValue<int>(() => _OrderId, (v) => _OrderId = v, value, OrderId_PROPERTYNAME,  DoOrderIdBeforeSet, DoOrderIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoOrderIdBeforeSet(string p_PropertyName, int p_OldValue, int p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoOrderIdAfterSet(string p_PropertyName, int p_OldValue, int p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyOrderIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : HeaderText ===

				public const string HeaderText_PROPERTYNAME = "HeaderText";

				private string _HeaderText;
				///<summary>
				/// Propriété : HeaderText
				///</summary>
				public string HeaderText
				{
					get
					{
						return GetValue<string>(() => _HeaderText);
					}
					set
					{
						SetValue<string>(() => _HeaderText, (v) => _HeaderText = v, value, HeaderText_PROPERTYNAME,  DoHeaderTextBeforeSet, DoHeaderTextAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoHeaderTextBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoHeaderTextAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyHeaderTextDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsVisibleButtonCancel ===

				public const string IsVisibleButtonCancel_PROPERTYNAME = "IsVisibleButtonCancel";

				private bool _IsVisibleButtonCancel;
				///<summary>
				/// Propriété : IsVisibleButtonCancel
				///</summary>
				public bool IsVisibleButtonCancel
				{
					get
					{
						return GetValue<bool>(() => _IsVisibleButtonCancel);
					}
					set
					{
						SetValue<bool>(() => _IsVisibleButtonCancel, (v) => _IsVisibleButtonCancel = v, value, IsVisibleButtonCancel_PROPERTYNAME,  DoIsVisibleButtonCancelBeforeSet, DoIsVisibleButtonCancelAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsVisibleButtonCancelBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsVisibleButtonCancelAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsVisibleButtonCancelDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CommandDetail ===

				public const string CommandDetail_PROPERTYNAME = "CommandDetail";

				private CommandDetailViewModelItem _CommandDetail;
				///<summary>
				/// Propriété : CommandDetail
				///</summary>
				public CommandDetailViewModelItem CommandDetail
				{
					get
					{
						return GetValue<CommandDetailViewModelItem>(() => _CommandDetail);
					}
					set
					{
						SetValue<CommandDetailViewModelItem>(() => _CommandDetail, (v) => _CommandDetail = v, value, CommandDetail_PROPERTYNAME,  DoCommandDetailBeforeSet, DoCommandDetailAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCommandDetailBeforeSet(string p_PropertyName, CommandDetailViewModelItem p_OldValue, CommandDetailViewModelItem p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCommandDetailAfterSet(string p_PropertyName, CommandDetailViewModelItem p_OldValue, CommandDetailViewModelItem p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCommandDetailDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ViewStatutPopInCommand ===

				public const string ViewStatutPopInCommand_PROPERTYNAME = "ViewStatutPopInCommand";

				private IDelegateCommand _ViewStatutPopInCommand;
				///<summary>
				/// Propriété : ViewStatutPopInCommand
				///</summary>
				public IDelegateCommand ViewStatutPopInCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ViewStatutPopInCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ViewStatutPopInCommand, (v) => _ViewStatutPopInCommand = v, value, ViewStatutPopInCommand_PROPERTYNAME,  DoViewStatutPopInCommandBeforeSet, DoViewStatutPopInCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoViewStatutPopInCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoViewStatutPopInCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyViewStatutPopInCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CancelOrderCommand ===

				public const string CancelOrderCommand_PROPERTYNAME = "CancelOrderCommand";

				private IDelegateCommand _CancelOrderCommand;
				///<summary>
				/// Propriété : CancelOrderCommand
				///</summary>
				public IDelegateCommand CancelOrderCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _CancelOrderCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _CancelOrderCommand, (v) => _CancelOrderCommand = v, value, CancelOrderCommand_PROPERTYNAME,  DoCancelOrderCommandBeforeSet, DoCancelOrderCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCancelOrderCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCancelOrderCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCancelOrderCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux commandes ===

			// Notez que les propriétés de commandes sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Commande : ViewStatutPopInCommand ===


				private bool ViewStatutPopInCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnViewStatutPopInCommand_CanExecute(l_Parameter);
				}

				private void ViewStatutPopInCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnViewStatutPopInCommand_Execute(l_Parameter);
				}

				protected virtual bool OnViewStatutPopInCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnViewStatutPopInCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : CancelOrderCommand ===


				private bool CancelOrderCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnCancelOrderCommand_CanExecute(l_Parameter);
				}

				private void CancelOrderCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnCancelOrderCommand_Execute(l_Parameter);
				}

				protected virtual bool OnCancelOrderCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnCancelOrderCommand_Execute(object p_Parameter);

			#endregion


		#endregion

	}

	public partial class OrderDetailViewModel : OrderDetailViewModelBase
	{
	}
}
