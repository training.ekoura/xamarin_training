
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using MyRestauApp.Common.DAOServices;
using MyRestauApp.Common.DAOServices.Models;
using MyRestauApp.Common.DAOServices.Requests;
using MyRestauApp.Common.ViewModels.ItemsViewModel;
using Sodexo;
using MyRestauApp.Common.DAOServices.Services;
using System.Globalization;

namespace MyRestauApp.Common.ViewModels
{
    public partial class PopInViewModel : PopInViewModelBase, IPopInViewModel
    {
        #region Proprietes
        private ICommandeService CommandService;
        private IStockService StockService;
        private IGlobalConfigService GlobalConfigService;
        private CultureInfo Culture;
        #endregion

        #region Initialisation
        protected override void OnInitialize()
        {
            base.OnInitialize();
            Initialisation();
        }

        public void Initialisation()
        {
            CommandService = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.DAOServices.Services.ICommandeService>();
            StockService = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.DAOServices.Services.IStockService>();
            GlobalConfigService = Sodexo.Framework.Services.GlobalConfigService();
            Culture = new CultureInfo(Properties.Resources.CultureInfoCurrentCultureName.Replace("_", "-"));
        }
        #endregion End Initialisation

        #region Case Popin Calendrier
        public async Task<bool> PrepareVMForChangeDate(DateTime selectedDate)
        {
            try
            {
                BeginWork();
                IsPopInVisibile = true;
                IsCalendarPopInVisibile = true;
                IsStatutPopInVisibile = false;
                IsStockPopInVisibile = false;
                SelectedDate = selectedDate;
                return true;
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                return false;
            }
            finally
            {
                EndWork();
            }
        }

        protected override async void OnChangeDateCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                if((p_Parameter!=null) && (SelectedDate!=(DateTime)p_Parameter))
                {
                    SelectedDate = (DateTime)p_Parameter;
                    var DashBoardVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IDashboardViewModel>();
                    var selectedDateFormatted = SelectedDate.ToString("dd/MM/yyyy HH:mm:ss", Culture);
                    var result = await DashBoardVM.UpdateDashboardView(selectedDateFormatted);
                    if (result)
                    {
                        IsPopInVisibile = false;
                        IsCalendarPopInVisibile = false;
                    }
                }
                //on traite le cas -date n'a pas chang� donc on faiit rien
                else
                {
                    IsPopInVisibile = false;
                    IsCalendarPopInVisibile = false;
                }
            }
            catch (Exception e)
            {
                //Afficher message d'erreur
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }
        #endregion

        #region Case Popin Add/Remove Stock
        public async Task<bool> PrepareVMForChangeStock(StockArticleViewModelItem stockArticleSelected, bool isAddQuantity)
        {
            try
            {
                BeginWork();
                IsPopInVisibile = true;
                IsStockPopInVisibile = true;
                IsAddQuantity = isAddQuantity;
                TypedNumber = string.Empty;
                StockArticleSelected = stockArticleSelected;
                HeadTitleStockLabel = (IsAddQuantity ? Properties.Resources.AddStock : Properties.Resources.RemoveStock);
                ButtonAddRemoveStockText = (IsAddQuantity ? Properties.Resources.AddButtonStock : Properties.Resources.RemoveButtonStock);
                return true;
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                return false;
            }
            finally
            {
                EndWork();
            }
        }

        protected override async void OnUpdateStockCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                Initialisation();
                int FinalTypedNumber;
                if (int.TryParse(TypedNumber, out FinalTypedNumber))
                {
                    if (FinalTypedNumber > 0)
                    {
                        // La quantit� � updater est n�gative ou positive 
                        FinalTypedNumber = (IsAddQuantity) ? FinalTypedNumber : FinalTypedNumber * (-1);
                        var l_NetworkService = Sodexo.Framework.Services.Container().Resolve<INetworkService>();
                        if (!l_NetworkService.IsConnected)
                        {
                            Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.ErrorNetwork);
                        }
                        else
                        {
                            var requestUpdateStock = new UpdateStockRequest()
                            {
                                Token = GlobalConfigService.Data.Token.TokenVal,
                                ZoneRestaurationId = StockArticleSelected.ZoneRestaurationId,
                                ArticleId = StockArticleSelected.Id,
                                AddQuantite = FinalTypedNumber,
                            };
                            var resultUpdateStock = await StockService.UpdateStock(requestUpdateStock);
                            if (resultUpdateStock.Success)
                            {
                                // Mettre � jour la liste des stocks
                                var DashBoardVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IDashboardViewModel>();

                                var selectedDateFormatted = SelectedDate.ToString("dd/MM/yyyy HH:mm:ss", Culture);
                                var result = await DashBoardVM.UpdateDashboardView(selectedDateFormatted);
                                if (result)
                                {
                                    IsPopInVisibile = false;
                                    IsStockPopInVisibile = false;
                                }
                            }
                            else
                                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, resultUpdateStock.ExceptionMessage);
                        }
                    }
                    else
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.ErrorUpdateStockQuantity);
                }
                else
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.ErrorUpdateStockQuantity);
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        #endregion

        #region Case Popin Change Statut

        public async Task<bool> PrepareVMForChangeStatut(Commande command, bool isDetailView)
        {
            try
            {
                BeginWork();
                IsDetailView = isDetailView;
                IsPopInVisibile = true;
                IsStatutPopInVisibile = true;
                CommandSelected = new CommandDetailViewModelItem().SetCommandDetailEncapsulation(command);
                if (CommandSelected != null && CommandSelected.ListStatutsPossibles != null)
                    ListStatuts.AddRange(CommandSelected.ListStatutsPossibles.Select(n => new StatutItemViewModel().SetCommandStatutEncapsulation(n)));
                SelectedStatut = new StatutItemViewModel().SetCommandStatutEncapsulation(CommandSelected.Statut);
                return true;
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                return false;
            }
            finally
            {
                EndWork();
            }
        }

        protected override async void OnUpdateStatutCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                var l_NetworkService = Sodexo.Framework.Services.Container().Resolve<INetworkService>();
                if (!l_NetworkService.IsConnected)
                {
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.ErrorNetwork);
                }
                else
                {
                    SelectedStatut = (StatutItemViewModel)p_Parameter;
                    var resultUpdateCommand = await CommandService.UpdateCommandeStatutAsync(GlobalConfigService.Data.Token.TokenVal, CommandSelected.Id, SelectedStatut.LogicValue);
                    if (resultUpdateCommand.Success)
                    {
                        if (IsDetailView)
                        {
                            //Mise � jour de la page d�tail et la page d'avant
                            var OrderDetailVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IOrderDetailViewModel>();
                            await OrderDetailVM.UpdateOrderDetail(CommandSelected.Id);
                        }
                        var DashBoardVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IDashboardViewModel>();
                        await DashBoardVM.UpdateCommandes(CommandSelected.Id, SelectedStatut);
                        IsPopInVisibile = false;
                        IsStatutPopInVisibile = false;
                    }
                    else
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, resultUpdateCommand.ExceptionMessage);

                }
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }
        #endregion

        // Clic sur le bouton fermer sur les trois popIn
        protected override async void OnClosePopInCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                IsStockPopInVisibile = false;
                IsStatutPopInVisibile = false;
                IsCalendarPopInVisibile = false;
                IsPopInVisibile = false;
            }
            finally
            {
                EndWork();
            }
        }
    }
}
