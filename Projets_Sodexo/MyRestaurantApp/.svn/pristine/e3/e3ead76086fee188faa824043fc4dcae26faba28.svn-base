﻿<?xml version="1.0" encoding="utf-8" ?>
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:ctrl="clr-namespace:Sodexo.Controls;assembly=Sodexo.MicroFramework.XamarinForms"
             xmlns:mfx="clr-namespace:Sodexo;assembly=Sodexo.MicroFramework.XamarinForms"
             xmlns:local="clr-namespace:MyRestauApp.Common.Views;assembly=MyRestauApp.Common.Views"
             xmlns:resx="clr-namespace:MyRestauApp.Common.Views.Properties;"
             xmlns:sodexobh="clr-namespace:Sodexo.Behaviors;assembly=Sodexo.MicroFramework.XamarinForms"
             xmlns:contentViews="clr-namespace:MyRestauApp.Common.Views;assembly=MyRestauApp.Common.Views"
             Style="{DynamicResource ContentPageStyle}"
             x:Class="MyRestauApp.Common.Views.OrderListView">

  <ContentPage.Resources>
    <ResourceDictionary>
      <mfx:BooleanNegationConverter x:Key="BooleanNegationConverter"/>
    </ResourceDictionary>
  </ContentPage.Resources>
  
  <Grid RowSpacing="0" BackgroundColor="{DynamicResource GerantPageBackgroundColor}">
    <Grid.Behaviors>
      <sodexobh:ActivityIndicatorBehavior/>
    </Grid.Behaviors>
    
    <Grid.RowDefinitions>
      <RowDefinition Height="2*"/>
      <RowDefinition Height="9*"/>
    </Grid.RowDefinitions>
    
    <!--region Header-->
    <contentViews:Header Grid.Row="0" Style="{DynamicResource DefaultContentViewStyle}" HeaderType="IsCommande" HeaderContentType="IsCalendar" />
    <!--endregion-->
    
    <!--region Content-->
    <Grid Grid.Row="1" RowSpacing="0">
      <Grid.RowDefinitions>
        <RowDefinition Height="*"/>
        <RowDefinition Height="10*"/>
      </Grid.RowDefinitions>
      
      <!--region Entête-->
      <Grid Grid.Row="0" Grid.ColumnSpacing="0" Padding="20,0" BackgroundColor="{DynamicResource GerantPageBackgroundColor}">
        <Grid.ColumnDefinitions>
          <ColumnDefinition Width="2*"/>
          <ColumnDefinition Width="4*"/>
          <ColumnDefinition Width="2*"/>
        </Grid.ColumnDefinitions>
      
        <Label x:Name="lbTest" Grid.Column="0" Text="{Static resx:Resources.OrderListView_To_Deliver}" TextColor="{DynamicResource SecondaryTextColor}" FontSize="Small" HorizontalOptions="StartAndExpand" VerticalOptions="CenterAndExpand"/>
        <Label Grid.Column="1" Text="{Static resx:Resources.OrderListView_Infos}" TextColor="{DynamicResource SecondaryTextColor}" FontSize="Small" HorizontalOptions="StartAndExpand" VerticalOptions="CenterAndExpand"/>
        <Label Grid.Column="2" Text="{Static resx:Resources.OrderListView_Status}" TextColor="{DynamicResource SecondaryTextColor}" FontSize="Small" HorizontalOptions="StartAndExpand" VerticalOptions="CenterAndExpand"/>
      </Grid>
      <!--endregion-->
      
      <!--region Scrollable Content-->
      <ScrollView Grid.Row="1" BackgroundColor="{DynamicResource GerantSecondaryPageBackgroundColor}" IsVisible="{Binding IsNoOrdersDispo, Converter={StaticResource BooleanNegationConverter}}">
        <ctrl:RepeaterView x:Name="Repeater" ItemSpacing="10" HasDottedLines="True" Padding="10,10,10,10" BackgroundColor="{DynamicResource RepeaterColor}" ItemsSource="{Binding CommandDashBoard.CommandesGerant}">
          <ctrl:RepeaterView.ItemTemplate>
            <DataTemplate>

              <Grid ColumnSpacing="0" Padding="20,0">
                <Grid.ColumnDefinitions>
                  <ColumnDefinition Width="2*"/>
                  <ColumnDefinition Width="4*"/>
                  <ColumnDefinition Width="2*"/>
                </Grid.ColumnDefinitions>
              
                <!--region A Livrer-->
                <StackLayout Grid.Column="0" HorizontalOptions="FillAndExpand" VerticalOptions="FillAndExpand">
                  <Label Text="{Binding FormattedDateForView}" TextColor="{DynamicResource SecondaryTextColor}" FontSize="Small" HorizontalOptions="StartAndExpand" VerticalOptions="CenterAndExpand"/>
                </StackLayout>
                <!--endregion-->
              
                <!--region Infos-->
                <StackLayout Grid.Column="1" Padding="0,10">
                  <Grid RowSpacing="0">
                    <Grid.RowDefinitions>
                      <RowDefinition Height="*"/>
                      <RowDefinition Height="*"/>
                    </Grid.RowDefinitions>
                  
                    <Label Grid.Row="0" Text="{Binding FormattedLabelFirstLastName}" TextColor="{DynamicResource SecondaryTextColor}" FontSize="Small" HorizontalOptions="StartAndExpand" VerticalOptions="EndAndExpand" FontAttributes="Bold"/>
                    <Label Grid.Row="1" Text="{Binding FormattedLabelForView}" TextColor="{DynamicResource SecondaryTextColor}" FontSize="Micro" HorizontalOptions="StartAndExpand" VerticalOptions="StartAndExpand"/>
                  </Grid>
                </StackLayout>
                <!--endregion-->
              
                <!--region Statut-->
                <StackLayout x:Name="stackImgStatut" Grid.Column="2" HorizontalOptions="FillAndExpand" VerticalOptions="FillAndExpand">
                  <Image x:Name="imgButtonStatut" HorizontalOptions="FillAndExpand" VerticalOptions="FillAndExpand"/>
                </StackLayout>
                <!--endregion-->

                <Grid.GestureRecognizers>
                  <TapGestureRecognizer Command="{Binding Path=BindingContext.OpenDetailViewCommand, Source={x:Reference Repeater}}" CommandParameter="{Binding .}" />
                </Grid.GestureRecognizers>
                
              </Grid>
          
            </DataTemplate>
          </ctrl:RepeaterView.ItemTemplate>
        </ctrl:RepeaterView>
      </ScrollView>
      <!--endregion-->

      <StackLayout Grid.Row="1" IsVisible="{Binding IsNoOrdersDispo}" Padding="10,5" HeightRequest="350" VerticalOptions="FillAndExpand" HorizontalOptions="FillAndExpand">
        <StackLayout BackgroundColor="White" VerticalOptions="FillAndExpand" HorizontalOptions="FillAndExpand">
          <Label Text="{Static resx:Resources.NoCommnade_Label}" TextColor="Black" FontSize="Medium" VerticalOptions="FillAndExpand" HorizontalOptions="FillAndExpand" VerticalTextAlignment="Center" HorizontalTextAlignment="Center"/>
        </StackLayout>
      </StackLayout>
      
    </Grid>
    <!--endregion-->
    
  </Grid>
</ContentPage>