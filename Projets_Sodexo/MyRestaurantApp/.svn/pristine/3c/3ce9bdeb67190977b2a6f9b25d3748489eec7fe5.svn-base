
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;
using System.Threading.Tasks;
using MyRestauApp.Common.DAOServices.Requests;
using MyRestauApp.Common.DAOServices.Services;

namespace MyRestauApp.Common.ViewModels
{
	public partial class ContextViewModel : ContextViewModelBase , IContextViewModel
	{
        #region === Propri�t�s ===
        private string Locale = Properties.Resources.CultureInfoCurrentCultureName;
        #endregion

        protected override void OnInitialize()
        {
            base.OnInitialize();
           
        }

        public async Task AppStarter()
        {
            try
            {
                var l_NetworkService = Sodexo.Framework.Services.Container().Resolve<INetworkService>();
                var GlobalConfigService = Sodexo.Framework.Services.GlobalConfigService();
                var DataOfflineService = Sodexo.Framework.Services.DataOfflineService();
                var userDataOffline = DataOfflineService.Data != null && DataOfflineService.Data.ListUserData != null ? DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId).FirstOrDefault() : null;
                var canConnect = true;
                var isOffline = Sodexo.Framework.Services.Container().Resolve<INetworkService>().IsConnected? false : true;
                if (((DateTime.Now - GlobalConfigService.Data.TokenDate).TotalHours) > Constants.HoursValidToken)
                {
                    #region refresh des tokens
                    var AuthService = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.DAOServices.Services.IAuthService>();
                    var requestRefresh = new RefreshRequest()
                    {
                        Token = GlobalConfigService.Data.Token.TokenVal,
                        Locale = Locale,
                        Os = GlobalConfigService.Data.DeviceOs,
                        AppName = GlobalConfigService.Data.AppName,
                        AppVersion = GlobalConfigService.Data.AppVersion
                    };

                    var result = await AuthService.RefreshAsync(requestRefresh);
                    if (result.Success)
                    {
                        GlobalConfigService.Data.Token = result.Data;
                        GlobalConfigService.Data.TokenDate = DateTime.Now;
                        GlobalConfigService.SaveConfig();
                        var commandService = Sodexo.Framework.Services.Container().Resolve<ICommandeService>();
                        //R�cup�ration des status possibles pour filtrer
                        var statutsResult = await commandService.GetListCommandeStatutsAsync(Properties.Resources.CultureInfoCurrentCultureName);
                        if (statutsResult.Success && statutsResult.Data != null)
                        {
                            GlobalConfigService.Data.ListStatutSaved = new List<DAOServices.Models.StatutCommande>();
                            int i = 1;
                            foreach (var statut in statutsResult.Data)
                            {
                                statut.IsChecked = true;
                                statut.IdForView = i;
                                i++;
                            }
                            GlobalConfigService.Data.ListStatutSaved.AddRange(statutsResult.Data);
                            GlobalConfigService.SaveConfig();
                        }
                    #endregion
                    }
                    else
                    {
                        var isError = true;
                        if (result.ExceptionComesFromNetwork)
                        {
                            if (userDataOffline != null && userDataOffline.DashboradDataByZR != null && userDataOffline.DashboradDataByZR.FirstOrDefault().DashboardUserByDate != null
                               &&  userDataOffline.DashboradDataByZR.FirstOrDefault().DashboardUserByDate.First().CommandeDashboard != null)
                            {
                                // Cas ou il y a des donn�es dans le DataOfflineService
                                DataOfflineService.Data.CurrentUser = userDataOffline;
                                DataOfflineService.SaveConfig();
                                isOffline = true;
                                isError = false;
                            }
                        }
                        else if(result.ExceptionType == "TokenInvalidException")
                        {
                            GlobalConfigService.Data.Token = null;
                            GlobalConfigService.SaveConfig();
                            isError = false;
                        }
                        if (isError)
                        {
                           
                            // Cas ou l'erreur du refresh provient pas d'un d�fault de connexion ou d'un token invalide
                            await Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, result.ExceptionMessage);
                            canConnect = false;
                        }
                    }
                }
                if (canConnect)
                {
                        if (GlobalConfigService.Data != null
                         && GlobalConfigService.Data.Token != null
                        )
                        {
                            // R�cup�ration du burger
                            var burgerVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IBurgerViewModel>();
                            var successBurger = await burgerVM.PrepareVM();
                            if (successBurger)
                            {
                                // Initialisation de DashboardViewModel
                                var l_InitialisationDashboradModel = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IDashboardViewModel>();
                                // On se situe dans le cas ou l'on est connect�
                                l_InitialisationDashboradModel.BurgerVM = burgerVM;
                                var success = await l_InitialisationDashboradModel.PrepareDashboardVM(isOffline);
                                // Redirection vers MasterDetailPageView
                                if (success)
                                    Sodexo.Framework.Services.InteractionService().Set(ViewName.MasterDetailPage, null);
                            }
                        }
                        else
                        {
                            System.Threading.Tasks.Task.Delay(2000).ContinueWith((t) =>
                            {
                                Sodexo.Framework.Services.InteractionService().Set(ViewName.ConnexionView);
                            }, System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());
                        }
                    }
                
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.Error);
            }

        }

        public async Task RefreshDashboard()
        {
            var dashboradVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IDashboardViewModel>();
            if (!string.IsNullOrEmpty(dashboradVM.DateDashBoard))
            {
                await dashboradVM.refreshDashborad() ;
            }
        }
	}
}
