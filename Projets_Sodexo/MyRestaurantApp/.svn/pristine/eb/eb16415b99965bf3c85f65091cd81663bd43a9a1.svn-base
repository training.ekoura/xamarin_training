﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML DashboardViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  Sodexo.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;

namespace MyRestauApp.Common.ViewModels
{
	public abstract partial class DashboardViewModelBase : Sodexo.ViewModel
	{
		protected override void OnInitialize()
		{
			base.OnInitialize();

			// Initialisation de la collection ListStock.
			ListStock = new System.Collections.ObjectModel.ObservableCollection<StockArticleViewModelItem>();
			// Initialisation de la commande ChangeDateCommand.
			ChangeDateCommand = new DelegateCommand(ChangeDateCommand_CanExecute, ChangeDateCommand_Execute);
			// Initialisation de la commande ChangeStatutCommand.
			ChangeStatutCommand = new DelegateCommand(ChangeStatutCommand_CanExecute, ChangeStatutCommand_Execute);
			// Initialisation de la commande LoadListOrdersCommand.
			LoadListOrdersCommand = new DelegateCommand(LoadListOrdersCommand_CanExecute, LoadListOrdersCommand_Execute);
			// Initialisation de la commande OpenDetailViewCommand.
			OpenDetailViewCommand = new DelegateCommand(OpenDetailViewCommand_CanExecute, OpenDetailViewCommand_Execute);
		}

		#region === Propriétés ===

			#region === Propriété : DateDashBoard ===

				public const string DateDashBoard_PROPERTYNAME = "DateDashBoard";

				private string _DateDashBoard;
				///<summary>
				/// Propriété : DateDashBoard
				///</summary>
				public string DateDashBoard
				{
					get
					{
						return GetValue<string>(() => _DateDashBoard);
					}
					set
					{
						SetValue<string>(() => _DateDashBoard, (v) => _DateDashBoard = v, value, DateDashBoard_PROPERTYNAME,  DoDateDashBoardBeforeSet, DoDateDashBoardAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDateDashBoardBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDateDashBoardAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDateDashBoardDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DateDashBoardFormattedLabel ===

				public const string DateDashBoardFormattedLabel_PROPERTYNAME = "DateDashBoardFormattedLabel";

				private string _DateDashBoardFormattedLabel;
				///<summary>
				/// Propriété : DateDashBoardFormattedLabel
				///</summary>
				public string DateDashBoardFormattedLabel
				{
					get
					{
						return GetValue<string>(() => _DateDashBoardFormattedLabel);
					}
					set
					{
						SetValue<string>(() => _DateDashBoardFormattedLabel, (v) => _DateDashBoardFormattedLabel = v, value, DateDashBoardFormattedLabel_PROPERTYNAME,  DoDateDashBoardFormattedLabelBeforeSet, DoDateDashBoardFormattedLabelAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDateDashBoardFormattedLabelBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDateDashBoardFormattedLabelAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDateDashBoardFormattedLabelDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CommandDashBoard ===

				public const string CommandDashBoard_PROPERTYNAME = "CommandDashBoard";

				private CommandDashboardViewModel _CommandDashBoard;
				///<summary>
				/// Propriété : CommandDashBoard
				///</summary>
				public CommandDashboardViewModel CommandDashBoard
				{
					get
					{
						return GetValue<CommandDashboardViewModel>(() => _CommandDashBoard);
					}
					set
					{
						SetValue<CommandDashboardViewModel>(() => _CommandDashBoard, (v) => _CommandDashBoard = v, value, CommandDashBoard_PROPERTYNAME,  DoCommandDashBoardBeforeSet, DoCommandDashBoardAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCommandDashBoardBeforeSet(string p_PropertyName, CommandDashboardViewModel p_OldValue, CommandDashboardViewModel p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCommandDashBoardAfterSet(string p_PropertyName, CommandDashboardViewModel p_OldValue, CommandDashboardViewModel p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCommandDashBoardDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ZoneRestaurationId ===

				public const string ZoneRestaurationId_PROPERTYNAME = "ZoneRestaurationId";

				private string _ZoneRestaurationId;
				///<summary>
				/// Propriété : ZoneRestaurationId
				///</summary>
				public string ZoneRestaurationId
				{
					get
					{
						return GetValue<string>(() => _ZoneRestaurationId);
					}
					set
					{
						SetValue<string>(() => _ZoneRestaurationId, (v) => _ZoneRestaurationId = v, value, ZoneRestaurationId_PROPERTYNAME,  DoZoneRestaurationIdBeforeSet, DoZoneRestaurationIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoZoneRestaurationIdBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoZoneRestaurationIdAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyZoneRestaurationIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsAddQuantity ===

				public const string IsAddQuantity_PROPERTYNAME = "IsAddQuantity";

				private bool _IsAddQuantity;
				///<summary>
				/// Propriété : IsAddQuantity
				///</summary>
				public bool IsAddQuantity
				{
					get
					{
						return GetValue<bool>(() => _IsAddQuantity);
					}
					set
					{
						SetValue<bool>(() => _IsAddQuantity, (v) => _IsAddQuantity = v, value, IsAddQuantity_PROPERTYNAME,  DoIsAddQuantityBeforeSet, DoIsAddQuantityAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsAddQuantityBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsAddQuantityAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsAddQuantityDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectedStock ===

				public const string SelectedStock_PROPERTYNAME = "SelectedStock";

				private StockArticleViewModelItem _SelectedStock;
				///<summary>
				/// Propriété : SelectedStock
				///</summary>
				public StockArticleViewModelItem SelectedStock
				{
					get
					{
						return GetValue<StockArticleViewModelItem>(() => _SelectedStock);
					}
					set
					{
						SetValue<StockArticleViewModelItem>(() => _SelectedStock, (v) => _SelectedStock = v, value, SelectedStock_PROPERTYNAME,  DoSelectedStockBeforeSet, DoSelectedStockAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectedStockBeforeSet(string p_PropertyName, StockArticleViewModelItem p_OldValue, StockArticleViewModelItem p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectedStockAfterSet(string p_PropertyName, StockArticleViewModelItem p_OldValue, StockArticleViewModelItem p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectedStockDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsStockDispo ===

				public const string IsStockDispo_PROPERTYNAME = "IsStockDispo";

				private bool _IsStockDispo;
				///<summary>
				/// Propriété : IsStockDispo
				///</summary>
				public bool IsStockDispo
				{
					get
					{
						return GetValue<bool>(() => _IsStockDispo);
					}
					set
					{
						SetValue<bool>(() => _IsStockDispo, (v) => _IsStockDispo = v, value, IsStockDispo_PROPERTYNAME,  DoIsStockDispoBeforeSet, DoIsStockDispoAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsStockDispoBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsStockDispoAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsStockDispoDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsVisibleButtonCancelCommand ===

				public const string IsVisibleButtonCancelCommand_PROPERTYNAME = "IsVisibleButtonCancelCommand";

				private bool _IsVisibleButtonCancelCommand;
				///<summary>
				/// Propriété : IsVisibleButtonCancelCommand
				///</summary>
				public bool IsVisibleButtonCancelCommand
				{
					get
					{
						return GetValue<bool>(() => _IsVisibleButtonCancelCommand);
					}
					set
					{
						SetValue<bool>(() => _IsVisibleButtonCancelCommand, (v) => _IsVisibleButtonCancelCommand = v, value, IsVisibleButtonCancelCommand_PROPERTYNAME,  DoIsVisibleButtonCancelCommandBeforeSet, DoIsVisibleButtonCancelCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsVisibleButtonCancelCommandBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsVisibleButtonCancelCommandAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsVisibleButtonCancelCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CommandDetail ===

				public const string CommandDetail_PROPERTYNAME = "CommandDetail";

				private CommandDetailViewModelItem _CommandDetail;
				///<summary>
				/// Propriété : CommandDetail
				///</summary>
				public CommandDetailViewModelItem CommandDetail
				{
					get
					{
						return GetValue<CommandDetailViewModelItem>(() => _CommandDetail);
					}
					set
					{
						SetValue<CommandDetailViewModelItem>(() => _CommandDetail, (v) => _CommandDetail = v, value, CommandDetail_PROPERTYNAME,  DoCommandDetailBeforeSet, DoCommandDetailAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCommandDetailBeforeSet(string p_PropertyName, CommandDetailViewModelItem p_OldValue, CommandDetailViewModelItem p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCommandDetailAfterSet(string p_PropertyName, CommandDetailViewModelItem p_OldValue, CommandDetailViewModelItem p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCommandDetailDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ListStock ===

				public const string ListStock_PROPERTYNAME = "ListStock";

				private System.Collections.ObjectModel.ObservableCollection<StockArticleViewModelItem> _ListStock;
				///<summary>
				/// Propriété : ListStock
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<StockArticleViewModelItem> ListStock
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<StockArticleViewModelItem>>(() => _ListStock);
					}
					private set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<StockArticleViewModelItem>>(() => _ListStock, (v) => _ListStock = v, value, ListStock_PROPERTYNAME,  DoListStockBeforeSet, DoListStockAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoListStockBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<StockArticleViewModelItem> p_OldValue, System.Collections.ObjectModel.ObservableCollection<StockArticleViewModelItem> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionListStock_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoListStockAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<StockArticleViewModelItem> p_OldValue, System.Collections.ObjectModel.ObservableCollection<StockArticleViewModelItem> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionListStock_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyListStockDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ChangeDateCommand ===

				public const string ChangeDateCommand_PROPERTYNAME = "ChangeDateCommand";

				private IDelegateCommand _ChangeDateCommand;
				///<summary>
				/// Propriété : ChangeDateCommand
				///</summary>
				public IDelegateCommand ChangeDateCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ChangeDateCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ChangeDateCommand, (v) => _ChangeDateCommand = v, value, ChangeDateCommand_PROPERTYNAME,  DoChangeDateCommandBeforeSet, DoChangeDateCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoChangeDateCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoChangeDateCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyChangeDateCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ChangeStatutCommand ===

				public const string ChangeStatutCommand_PROPERTYNAME = "ChangeStatutCommand";

				private IDelegateCommand _ChangeStatutCommand;
				///<summary>
				/// Propriété : ChangeStatutCommand
				///</summary>
				public IDelegateCommand ChangeStatutCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ChangeStatutCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ChangeStatutCommand, (v) => _ChangeStatutCommand = v, value, ChangeStatutCommand_PROPERTYNAME,  DoChangeStatutCommandBeforeSet, DoChangeStatutCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoChangeStatutCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoChangeStatutCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyChangeStatutCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LoadListOrdersCommand ===

				public const string LoadListOrdersCommand_PROPERTYNAME = "LoadListOrdersCommand";

				private IDelegateCommand _LoadListOrdersCommand;
				///<summary>
				/// Propriété : LoadListOrdersCommand
				///</summary>
				public IDelegateCommand LoadListOrdersCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _LoadListOrdersCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _LoadListOrdersCommand, (v) => _LoadListOrdersCommand = v, value, LoadListOrdersCommand_PROPERTYNAME,  DoLoadListOrdersCommandBeforeSet, DoLoadListOrdersCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLoadListOrdersCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLoadListOrdersCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLoadListOrdersCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : OpenDetailViewCommand ===

				public const string OpenDetailViewCommand_PROPERTYNAME = "OpenDetailViewCommand";

				private IDelegateCommand _OpenDetailViewCommand;
				///<summary>
				/// Propriété : OpenDetailViewCommand
				///</summary>
				public IDelegateCommand OpenDetailViewCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _OpenDetailViewCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _OpenDetailViewCommand, (v) => _OpenDetailViewCommand = v, value, OpenDetailViewCommand_PROPERTYNAME,  DoOpenDetailViewCommandBeforeSet, DoOpenDetailViewCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoOpenDetailViewCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoOpenDetailViewCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyOpenDetailViewCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux collections ===

			// Notez que les propriétés de collection sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Collection : ListStock ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection ListStock aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionListStock_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionListStock_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionListStock_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection ListStock (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionListStock_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionListStock_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionListStock_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection ListStock aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionListStock_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionListStock_ItemPropertyChanged;
							OnListStockItemAdded((StockArticleViewModelItem)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection ListStock (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionListStock_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnListStockItemRemoved((StockArticleViewModelItem)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionListStock_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionListStock_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionListStock_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionListStock_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ListStock_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyListStockDependencies();
				}

				private void WatchCollectionListStock_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ListStock_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyListStockDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection ListStock.
				///</summary>
				protected virtual void OnListStockItemAdded(StockArticleViewModelItem p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection ListStock.
				///</summary>
				protected virtual void OnListStockItemRemoved(StockArticleViewModelItem p_Item)
				{
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux commandes ===

			// Notez que les propriétés de commandes sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Commande : ChangeDateCommand ===


				private bool ChangeDateCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnChangeDateCommand_CanExecute(l_Parameter);
				}

				private void ChangeDateCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnChangeDateCommand_Execute(l_Parameter);
				}

				protected virtual bool OnChangeDateCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnChangeDateCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ChangeStatutCommand ===


				private bool ChangeStatutCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnChangeStatutCommand_CanExecute(l_Parameter);
				}

				private void ChangeStatutCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnChangeStatutCommand_Execute(l_Parameter);
				}

				protected virtual bool OnChangeStatutCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnChangeStatutCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : LoadListOrdersCommand ===


				private bool LoadListOrdersCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnLoadListOrdersCommand_CanExecute(l_Parameter);
				}

				private void LoadListOrdersCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnLoadListOrdersCommand_Execute(l_Parameter);
				}

				protected virtual bool OnLoadListOrdersCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnLoadListOrdersCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : OpenDetailViewCommand ===


				private bool OpenDetailViewCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnOpenDetailViewCommand_CanExecute(l_Parameter);
				}

				private void OpenDetailViewCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnOpenDetailViewCommand_Execute(l_Parameter);
				}

				protected virtual bool OnOpenDetailViewCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnOpenDetailViewCommand_Execute(object p_Parameter);

			#endregion


		#endregion

	}

	public partial class DashboardViewModel : DashboardViewModelBase
	{
	}
}
