﻿<?xml version="1.0" encoding="utf-8" ?>
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:ctrl="clr-namespace:Sodexo.Controls;assembly=Sodexo.MicroFramework.XamarinForms"
             xmlns:mfx="clr-namespace:Sodexo;assembly=Sodexo.MicroFramework.XamarinForms"
             xmlns:local="clr-namespace:MyRestauApp.Common.Views;assembly=MyRestauApp.Common.Views"
             xmlns:sodexobh="clr-namespace:Sodexo.Behaviors;assembly=Sodexo.MicroFramework.XamarinForms"
             xmlns:contentViews="clr-namespace:MyRestauApp.Common.Views;assembly=MyRestauApp.Common.Views"
             Style="{DynamicResource ContentPageStyle}"
             x:Class="MyRestauApp.Common.Views.DashboardView">

  <Grid Style="{DynamicResource DefaultGridStyle}">
    <Grid.Behaviors>
      <sodexobh:ActivityIndicatorBehavior/>
    </Grid.Behaviors>
    
    <Grid.RowDefinitions>
      <RowDefinition Height="2*"/>
      <RowDefinition Height="9*"/>
    </Grid.RowDefinitions>

    <!--region Header-->
    <contentViews:Header Grid.Row="0" Style="{DynamicResource DefaultContentViewStyle}" HeaderType="IsDashboard" HeaderContentType="IsCalendar" />
    <!--endregion-->

    <!--region Scrollable Content-->
    <ScrollView Grid.Row="1">
      <Grid Style="{DynamicResource DefaultGridStyle}">
        <Grid.RowDefinitions>
          <RowDefinition Height="Auto"/>
          <RowDefinition Height="*"/>
        </Grid.RowDefinitions>

        <!--region Entête-->
        <Grid Grid.Row="0" Grid.ColumnSpacing="0" Grid.RowSpacing="20" Padding="10,20,10,10">
          <Grid.ColumnDefinitions>
            <ColumnDefinition Width="*"/>
            <ColumnDefinition Width="Auto"/>
            <ColumnDefinition Width="*"/>
            <ColumnDefinition Width="Auto"/>
            <ColumnDefinition Width="*"/>
          </Grid.ColumnDefinitions>
          <Grid.RowDefinitions>
            <RowDefinition Height="*"/>
            <RowDefinition Height="*"/>
          </Grid.RowDefinitions>

          <!--region Colonne Commandes en cours-->
          <Grid Grid.Column="0" Grid.Row="0" Grid.RowSpacing="0" Padding="20,0">
            <Grid.RowDefinitions>
              <RowDefinition Height="Auto"/>
              <RowDefinition Height="Auto"/>
            </Grid.RowDefinitions>

            <Label Grid.Row="0" Text="{Binding CommandDashBoard.NbrRestantes}" Style="{DynamicResource GerantEntete36LabelStyle}" />
            <Label Grid.Row="1" Text="commandes en cours" Style="{DynamicResource GerantEntete16LabelStyle}" LineBreakMode="WordWrap" />
          </Grid>
          <!--endregion-->

          <!--region Separateur-->
          <Image x:Name="firstSeparator"
                  Grid.Column="1" Grid.Row="0"
                  Source="{local:ImageResource MyRestauApp.Common.Views.Images.VerticalDottedSeparator.png}"
                  VerticalOptions="CenterAndExpand"
                  HorizontalOptions="Center"/>
          <!--endregion-->

          <!--region Colonne Commandes retirées-->
          <Grid Grid.Column="2" Grid.Row="0" Grid.RowSpacing="0">
            <Grid.RowDefinitions>
              <RowDefinition Height="Auto"/>
              <RowDefinition Height="Auto"/>
            </Grid.RowDefinitions>

            <Label Grid.Row="0" Text="{Binding CommandDashBoard.NbrRetirees}" Style="{DynamicResource GerantEntete36LabelStyle}" />
            <Label Grid.Row="1" Text="commandes retirées" Style="{DynamicResource GerantEntete16LabelStyle}" LineBreakMode="WordWrap" />
          </Grid>
          <!--endregion-->

          <!--region Separateur-->
          <Image x:Name="secondSeparator"
                  Grid.Column="3" Grid.Row="0"
                  Source="{local:ImageResource MyRestauApp.Common.Views.Images.VerticalDottedSeparator.png}"
                  VerticalOptions="CenterAndExpand"
                  HorizontalOptions="Center"/>
          <!--endregion-->

          <!--region Colonne Revenus journaliers-->
          <Grid Grid.Column="4" Grid.Row="0" Grid.RowSpacing="0">
            <Grid.RowDefinitions>
              <RowDefinition Height="Auto"/>
              <RowDefinition Height="Auto"/>
            </Grid.RowDefinitions>


            <Label Grid.Row="0" Text="{Binding CommandDashBoard.NbrVendues}" Style="{DynamicResource GerantEntete36LabelStyle}" />
            <Label Grid.Row="1" Text="de revenus aujourd'hui" Style="{DynamicResource GerantEntete16LabelStyle}" LineBreakMode="WordWrap" />
          </Grid>
          <!--endregion-->
        
          <!--region Bouton Voir les commandes du jour-->
          <ctrl:RoundedButton x:Name="btVoirCommandesJour" Grid.Row="1" Grid.Column="0" Grid.ColumnSpan="5" Text="VOIR LES COMMANDES DU JOUR" Command ="{Binding LoadListOrdersCommand}"  Style="{DynamicResource RoundedButtonStyle}"  />
          <!--endregion-->

        </Grid>
        <!--endregion-->
      
        <!--region Content-->
        <ctrl:RepeaterView x:Name="ListStockRepeater" Grid.Row="1" VerticalOptions="FillAndExpand" HorizontalOptions="FillAndExpand" ItemsSource="{Binding ListStock}">
          <ctrl:RepeaterView.ItemTemplate>
            <DataTemplate>

              <Grid RowSpacing="0" Padding="20,10">
                <Grid.RowDefinitions>
                  <RowDefinition Height="Auto"/>
                  <RowDefinition Height="Auto"/>
                </Grid.RowDefinitions>

                <!--region Layout Bleu-->
                <Grid Grid.Row="0" RowSpacing="0" Style="{DynamicResource SecondaryThemedGridStyle}" Padding="20,15">
                  <Grid.RowDefinitions>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto"/>
                  </Grid.RowDefinitions>

                  <Label Grid.Row="0" Text="{Binding LibelleCommercial}" TextColor="{DynamicResource PrimaryTextColor}" FontSize="26" VerticalOptions="End"/>
                
                  <Label Grid.Row="1" Text="{Binding LibelleTechnique}" TextColor="{DynamicResource TertiaryTextColor}" FontSize="22" VerticalOptions="Start"/>
                </Grid>
                <!--endregion-->

                <!--region Detail-->
                <Grid Grid.Row="1" RowSpacing="0" Style="{DynamicResource WhiteThemedGridStyle}" Padding="20,10,20,15">
                  <Grid.RowDefinitions>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto"/>
                  </Grid.RowDefinitions>
                  <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="*"/>
                    <ColumnDefinition Width="*"/>
                    <ColumnDefinition Width="*"/>
                  </Grid.ColumnDefinitions>

                  <!--region Colonne Vendus-->
                  <StackLayout Grid.Row="0" Grid.Column="0" Orientation="Horizontal" HorizontalOptions="Start">
                  
                    <Label Text="Vendus :"
                            TextColor="{DynamicResource TertiaryTextColor}"
                            HorizontalOptions="Start" VerticalOptions="Fill"
                           VerticalTextAlignment="End" />

                    <Label Text="{Binding NbVendus}"
                            TextColor="{DynamicResource SecondaryTextColor}"
                            FontSize="26"
                            HorizontalOptions="StartAndExpand"
                            VerticalOptions="Fill"/>
                  
                  </StackLayout>
                  <!--endregion-->
                
                  <!--region Colonne Réservés-->
                  <StackLayout Grid.Row="0" Grid.Column="1" Orientation="Horizontal" HorizontalOptions="Center">
                  
                    <Label Text="Réservés :"
                            TextColor="{DynamicResource TertiaryTextColor}"
                            HorizontalOptions="Start"
                            VerticalOptions="End"/>

                    <Label Text="{Binding NbRestants}"
                            TextColor="{DynamicResource SecondaryTextColor}"
                            FontSize="26"
                            HorizontalOptions="StartAndExpand"
                            VerticalOptions="Start"/>
                  
                  </StackLayout>
                  <!--endregion-->

                  <!--region Colonne Retirés-->
                  <StackLayout Grid.Row="0" Grid.Column="2" Orientation="Horizontal" HorizontalOptions="End">

                    <Label Text="Retirés :"
                            TextColor="{DynamicResource TertiaryTextColor}"
                            HorizontalOptions="Start"
                            VerticalOptions="End"/>

                    <Label Text="{Binding NbVendus}"
                            TextColor="{DynamicResource SecondaryTextColor}"
                            FontSize="26"
                            HorizontalOptions="StartAndExpand"
                            VerticalOptions="Start"/>
                  
                  </StackLayout>
                  <!--endregion-->
                
                  <!--region Horizontal Separator-->
                  <StackLayout Grid.Row="1" Grid.Column="0" Grid.ColumnSpan="3" Padding="0,10">
                    <ctrl:dottedLabel LineBreakMode="NoWrap" FontSize="6"/>
                  </StackLayout>
                  <!--endregion-->
                
                  <!--region Ligne Stock-->
                  <Label Grid.Row="2"
                          Grid.Column="0"
                          Grid.ColumnSpan="2"
                          Text="{Binding Stock, StringFormat='STOCK : {0}'}"
                          TextColor="{DynamicResource SecondaryTextColor}"
                          FontSize="26"
                          HorizontalOptions="StartAndExpand"
                          VerticalOptions="CenterAndExpand"/>

                  <StackLayout Grid.Row="2" Grid.Column="2" Orientation="Horizontal" HorizontalOptions="End">
                  
                    <Image Source="{local:ImageResource MyRestauApp.Common.Views.Images.btMoins.png}"
                            HeightRequest="60"
                            WidthRequest="60"
                            HorizontalOptions="CenterAndExpand"
                            VerticalOptions="CenterAndExpand"/>

                    <Image Source="{local:ImageResource MyRestauApp.Common.Views.Images.btPlus.png}"
                            HeightRequest="60"
                            WidthRequest="60"
                            HorizontalOptions="CenterAndExpand"
                            VerticalOptions="CenterAndExpand"/>
                  
                  </StackLayout>
                  <!--endregion-->
                
                </Grid>
                <!--endregion-->
                
              </Grid>
            
            </DataTemplate>
          </ctrl:RepeaterView.ItemTemplate>
        </ctrl:RepeaterView>
        <!--endregion-->
      
      </Grid>
    </ScrollView>
    <!--endregion-->
    
  </Grid>
</ContentPage>