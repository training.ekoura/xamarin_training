
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using MyRestauApp.Common.DAOServices;
using MyRestauApp.Common.DAOServices.Models;
using MyRestauApp.Common.DAOServices.Requests;
using MyRestauApp.Common.ViewModels.ItemsViewModel;
using Sodexo;
using MyRestauApp.Common.DAOServices.Services;


namespace MyRestauApp.Common.ViewModels
{
    public partial class DashboardViewModel : DashboardViewModelBase, IDashboardViewModel
    {
        #region Proprietes
        private ICommandeService CommandService;
        private IStockService StockService;
        private IUserService UserService;
        private IGlobalConfigService GlobalConfigService;
        private IDataOfflineService DataOfflineService;
        private bool IsWorkingView = false;
        #endregion

        #region Initialisation
        protected override void OnInitialize()
        {
            base.OnInitialize();
            ResolveInitiliazation();

        }

        private void ResolveInitiliazation()
        {
            CommandService = Sodexo.Framework.Services.Container().Resolve<ICommandeService>();
            StockService = Sodexo.Framework.Services.Container().Resolve<IStockService>();
            UserService = Sodexo.Framework.Services.Container().Resolve<IUserService>();
            GlobalConfigService = Sodexo.Framework.Services.GlobalConfigService();
            DataOfflineService = Sodexo.Framework.Services.DataOfflineService();
        }
        #endregion End Initialisation

        #region M�thodes
        // Pr�paration du ViewModel pour afficher les informations du Tableau de bord sur la date du jour, appel� � partir de ConnexionViewModel
        public async Task<bool> PrepareDashboardVM()
        {
            try
            {
                BeginWork();
                ZoneRestaurationId = string.Empty;
                ResolveInitiliazation();
                // R�cup�rer la date � afficher dans l'en-t�te
                var cultureForDate = new System.Globalization.CultureInfo(Properties.Resources.CultureInfoCurrentCultureName.Replace("_", "-"));
                DateDashBoard = DateTime.Now.Date.ToString("dd/MM/yyyy HH:mm", cultureForDate);
                // Date d'aujourd'hui � afficher
                DateDashBoardFormattedLabel = Properties.Resources.ForToday + " - " + DateTime.ParseExact(DateDashBoard, "dd/MM/yyyy HH:mm", cultureForDate).ToString("dddd d MMMM", cultureForDate);
                // Chargement de DashBoard avec utilisation de la date du jour pour le premier chargement du ViewModel
                var userResult = await UserService.GetUser(GlobalConfigService.Data.Token.TokenVal);
                if (userResult.Success)
                {
                    if (userResult.Data != null && userResult.Data.Site != null && userResult.Data.Site.FirstOrDefault() != null && userResult.Data.Site.FirstOrDefault().ZonesRestauration != null
                             && userResult.Data.Site.FirstOrDefault().ZonesRestauration.Count > 0)
                    {
                        ZoneRestaurationId = userResult.Data.Site.FirstOrDefault().ZonesRestauration.FirstOrDefault().Id;
                        DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId).FirstOrDefault().ZoneRestaurationId = ZoneRestaurationId;
                        DataOfflineService.SaveConfig();
                        return await UpdateDashboardView(string.Empty);
                    }
                    else
                    {
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.ErrorGetUser);
                        return false;
                    }
                }
                else
                {
                    var isError = true;
                    if (userResult.ExceptionComesFromNetwork)
                    {
                        ZoneRestaurationId = DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId).FirstOrDefault().ZoneRestaurationId;
                        isError = !string.IsNullOrEmpty(ZoneRestaurationId) ? false : true;// on a pas de data enregistr� en Offline donc affiche le message d 'erreur
                    }
                    if (isError)
                    {
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, userResult.ExceptionMessage);
                        return false;
                    }
                    else
                        return await UpdateDashboardView(string.Empty);
                }
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                return false;
            }
            finally
            {
                EndWork();
            }
        }

        // Clic sur le bouton d'annulation de commande sur OrderDetailView
        public async Task DeleteCommand(int commandId, string dateCommand, string restaurationId)
        {
            try
            {
                BeginWork();
                if (await Sodexo.Framework.Services.InteractionService().Confirm(Properties.Resources.MessageForCancelOrder, Properties.Resources.MessageForCancelOrder))
                {
                    // appel du webservice pour annuler la commande
                    ResolveInitiliazation();
                    var commandServiceResult = await CommandService.UpdateCommandeStatutAsync(GlobalConfigService.Data.Token.TokenVal, commandId, Properties.Resources.StatutLogicalValueCancel);
                    if (commandServiceResult.Success)
                    {
                        // Mettre � jour la liste des stocks et la liste des commandes
                        await UpdateDashboardView(dateCommand);
                        // on ferme OrderDetailView
                        Sodexo.Framework.Services.InteractionService().Close();
                    }
                    else
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, commandServiceResult.ExceptionMessage);
                }
            }
            catch (Exception e)
            {
                //Afficher message d'erreur
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        // Refresh de la View DashboardView
        public async Task<bool> UpdateDashboardView(string date)
        {
            try
            {
                BeginWork();
                //1 ---  Premi�re partie de la vue ---
                var requestDashboardGerant = new GetDashboardGerantRequest()
                {
                    Token = GlobalConfigService.Data.Token.TokenVal,
                    Date = date
                };
                var commandServiceResult = await CommandService.GetDashboardGerantAsync(requestDashboardGerant);
                if (commandServiceResult.Success)
                {
                    if (commandServiceResult.Data != null)
                    {
                        CommandDashBoard = new CommandDashboardViewModel().SetCommandDashBoardEncapsulation(commandServiceResult.Data);
                        //On enregistre pour le mode offline
                        DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId).FirstOrDefault().CommandeDashboard = commandServiceResult.Data;
                        DataOfflineService.SaveConfig();
                    }
                    //TODO-rimeh Ici v�rifier si data est null, on affiche quoi au user
                }
                else
                {
                    var isError = true;
                    if (commandServiceResult.ExceptionComesFromNetwork)
                    {
                        var commandeDashboard = DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId).FirstOrDefault().CommandeDashboard;
                        isError = commandeDashboard == null ? true : false;
                    }

                    if (isError)
                    {
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, commandServiceResult.ExceptionMessage);
                        return false;
                    }
                    else
                        CommandDashBoard = new CommandDashboardViewModel().SetCommandDashBoardEncapsulation(
                            DataOfflineService.Data.ListUserData
                            .Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                            .FirstOrDefault().CommandeDashboard);
                }
                //2 --- Seconde partie de la vue ---    
                ListStock.Clear();
                var stockResult = await StockService.GetStocks(GlobalConfigService.Data.Token.TokenVal, ZoneRestaurationId, date);
                if (stockResult.Success)
                {
                    IsStockDispo = stockResult.Data == null || stockResult.Data.Count > 0 ? true : false;
                    if (stockResult.Data != null && stockResult.Data.Count > 0)
                    {
                        ListStock.AddRange(stockResult.Data.OrderBy(x => x.LibelleCommercial).ToList().Select(n => new StockArticleViewModelItem().SetStockArticleEncapsulation(n)));
                        //On enregistre pour le mode offline
                        DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId).FirstOrDefault().ListStocks = stockResult.Data;
                        DataOfflineService.SaveConfig();
                    }
                }
                else
                {
                    var isError = true;
                    if (stockResult.ExceptionComesFromNetwork)
                        isError = DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId).FirstOrDefault().ListStocks == null ? true : false;
                    if (isError)
                    {
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, stockResult.ExceptionMessage);
                        return false;
                    }
                    else
                    {
                        ListStock.AddRange(DataOfflineService.Data.ListUserData
                            .Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                            .FirstOrDefault().ListStocks.OrderBy(x => x.LibelleCommercial)
                            .ToList().Select(n => new StockArticleViewModelItem()
                                .SetStockArticleEncapsulation(n)));
                        IsStockDispo = ListStock != null || ListStock.Count > 0 ? true : false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                //Afficher message d'erreur
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                return false;
            }
            finally
            {
                EndWork();
            }
        }

        // Refresh de la liste des commandes et de la commande
        public async Task UpdateCommandes(string p_zoneRestaurationId, int p_commandeId)
        {
            try
            {
                BeginWork();
                // Update de Dashboard pour OrderListView
                var requestDashboardGerant = new GetDashboardGerantRequest()
                {
                    Token = GlobalConfigService.Data.Token.TokenVal,
                    Date = DateDashBoard,
                    ZoneRestaurationId = p_zoneRestaurationId
                };
                var commandServiceResult = await CommandService.GetDashboardGerantAsync(requestDashboardGerant);
                if (commandServiceResult.Success)
                {
                    if (commandServiceResult.Data != null )
                        CommandDashBoard = new CommandDashboardViewModel().SetCommandDashBoardEncapsulation(commandServiceResult.Data);
                }
                else
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, commandServiceResult.ExceptionMessage);

                // Update de commande detail pour OrderDetailView
                CommandService = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.DAOServices.Services.ICommandeService>();
                var commandResult = await CommandService.GetCommandeGerantAsync(GlobalConfigService.Data.Token.TokenVal, p_commandeId);
                if (commandResult.Success)
                {
                    if (commandResult.Data != null)
                        CommandDetail = new CommandDetailViewModelItem().SetCommandDetailEncapsulation(commandResult.Data);
                }
                else
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, commandResult.ExceptionMessage);
            }
            catch (Exception e)
            {
                //Afficher message d'erreur
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        // Clic sur le bouton back
        public async Task BackBtnClick()
        {
            try
            {
                BeginWork();
                Sodexo.Framework.Services.InteractionService().Close();
            }
            finally
            {
                EndWork();
            }
        }

        #endregion M�thodes

        #region Commandes
        // Commande au clic sur le bouton pour voir les commandes en cours sur DashboardView 
        protected override async void OnOpenOrderListViewCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                await Sodexo.Framework.Services.InteractionService().Open(ViewName.OrderListView, this);
            }
            catch (Exception e)
            {
                //Afficher message d'erreur
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        // Commande au clic sur le bouton pour voir les commandes en cours sur DashboardView 
        protected override async void OnOpenDetailViewCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                CommandDetail = new CommandDetailViewModelItem().SetCommandDetailEncapsulation((Commande)p_Parameter);
                IsVisibleButtonCancelCommand = (CommandDetail.statutLibelleGerant == Properties.Resources.StatutLogicalValueCancel) ? false : true;
                await Sodexo.Framework.Services.InteractionService().Open(ViewName.OrderDetailView, this);
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        // Clic sur le - / +  sur DashboardView pour ouvrir la popIn StockPopIn
        public async Task ViewStockPlusMoinsPopIn(string stockArticleSelectedId, bool isAddQuantity)
        {
            try
            {
                BeginWork();
                var stockArticleSelected = ListStock.Where(x => x.Id == stockArticleSelectedId).FirstOrDefault();
                var popInVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IPopInViewModel>();
                var resultPreparePopIn = popInVM.PrepareStockVM((StockArticleViewModelItem)stockArticleSelected, isAddQuantity);
                Sodexo.Framework.Services.InteractionService().ViewPopIn((IViewModel)popInVM);
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }        

        // Clic sur l'ic�ne calendrier pour ouvrir une popin
        protected override void OnViewCalendarPopInCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                var popInVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IPopInViewModel>();
                var resultPreparePopIn = popInVM.PrepareCalendarVM();
                Sodexo.Framework.Services.InteractionService().ViewPopIn((IViewModel)popInVM);
            }
            catch (Exception ex)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
                EndWork();
            }
        }

        // Clic sur le statut pour ouvrir une popin
        protected override void OnViewStatutPopInCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                var popInVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IPopInViewModel>();
                popInVM.PrepareStatutVM((Commande)p_Parameter);
                Sodexo.Framework.Services.InteractionService().ViewPopIn((IViewModel)popInVM);
            }
            catch (Exception ex)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
                EndWork();
            }
        }
        #endregion End Commandes

    }
}
