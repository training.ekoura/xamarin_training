﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common
{
    public static class Constants
    {
        //Domain       
#if DEBUG
        //QUAL
        public const string DOMAIN = "http://ws.portail.int.sodexonet.com/ws/";
#else
        //prod
        public const string DOMAIN = "https://ws.votreextranet.fr/ws/";
#endif
        //WebService Names
        public const string AuthService = "V3_00/AuthSol.cfc";
        public const string UserService = "V3_00/UserSol.cfc";
        public const string CommandeService = "V3_00/Commande.cfc";
        public const string NotificationService = "V3_00/Notification.cfc";
        public const string StockService = "V3_00/Stock.cfc";

        public const int CacheService = 60;
        public const int CacheServiceFluidite = 5;

        public const string AppName = "myresto";

    }
}
