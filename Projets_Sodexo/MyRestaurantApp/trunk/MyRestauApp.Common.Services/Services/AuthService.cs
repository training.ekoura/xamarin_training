﻿using MyRestauApp.Common.DAOServices.Models;
using MyRestauApp.Common.DAOServices.Requests;
using Sodexo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Services
{
    public class AuthService : IAuthService
    {

        /// <summary>
        /// Méthode permettant la connexion
        /// </summary>
        /// <param name="p_LoginOrMail">login de l'utilisateur</param>
        /// <param name="p_Pwd">Mot de passe</param>
        /// <param name="p_PwdMode">mode du mot de passe</param>
        /// <param name="p_Locale">locale</param>
        /// <returns>la liste des tokens de l'utilisateur</returns>
        public Task<RemoteCallResult<Token>> ConnectAsync(ConnectRequest p_request)
        {
            return Sodexo.Framework.Services.RemoteCallService().Call<Token>(Constants.AuthService, "connect", new
            {
                loginOrMail = p_request.LoginOrMail,
                pwd = p_request.Pwd,
                locale = p_request.Locale,
                origin = Constants.AppName,
                os = p_request.Os,
                appVersion = p_request.AppVersion,
                appName = p_request.AppName
            });
        }


        /// <summary>
        /// Méthode permettant la déconnexion
        /// </summary>
        /// <param name="p_Token">Le token de l'utilisateur</param>
        /// <returns>Succès ou echec</returns>
        public Task<RemoteCallResult> DisconnectAsync(string p_Token, string p_Separator, string p_Locale)
        {
            return Sodexo.Framework.Services.RemoteCallService().Call(Constants.AuthService, "disconnect", new
            {
                token = p_Token,
                delimiter = p_Separator,
                locale = p_Locale,
                origin = Constants.AppName
            });
        }


        /// <summary>
        /// Méthode permettant de faire un Refresh des tokens users
        /// </summary>
        /// <param name="p_ListToken">La liste des tokens </param>
        /// <param name="p_Separator">Séparateur</param>
        /// <param name="p_Locale">Locale</param>
        /// <returns>La liste de nouveaux tokens</returns>
        public Task<RemoteCallResult<Token>> RefreshAsync(RefreshRequest p_Request)
        {
            return Sodexo.Framework.Services.RemoteCallService().Call<Token>(Constants.AuthService, "refresh", new
            {
                token = p_Request.Token,
                locale = p_Request.Locale,
                origin = Constants.AppName,
                os = p_Request.Os,
                appVersion = p_Request.AppVersion,
                appName = p_Request.AppName
            });
        }



    }
}
