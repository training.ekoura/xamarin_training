﻿using System;
namespace MyRestauApp.Common.DAOServices.Services
{
    public interface IAuthService
    {
        System.Threading.Tasks.Task<Sodexo.RemoteCallResult<MyRestauApp.Common.DAOServices.Models.Token>> ConnectAsync(MyRestauApp.Common.DAOServices.Requests.ConnectRequest p_request);
        System.Threading.Tasks.Task<Sodexo.RemoteCallResult> DisconnectAsync(string p_Token, string p_Separator, string p_Locale);
        System.Threading.Tasks.Task<Sodexo.RemoteCallResult<MyRestauApp.Common.DAOServices.Models.Token>> RefreshAsync(MyRestauApp.Common.DAOServices.Requests.RefreshRequest p_Request);
    }
}
