﻿using MyRestauApp.Common.DAOServices.Models;
using MyRestauApp.Common.DAOServices.Requests;
using Sodexo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Services
{
    public class NotificationService : INotificationService
    {
        public Task<RemoteCallResult> AddUserSolAsync(string p_Token)
        {
            return Sodexo.Framework.Services.RemoteCallService().Call(Constants.NotificationService, "addUserSol", new
            {
                token = p_Token
            });
        }

        public Task<RemoteCallResult<NotificationBatchConfig>> GetNotificationBatchConfigAsync(GetNotificationBatchConfigRequest p_Request)
        {
            return Sodexo.Framework.Services.RemoteCallService().Call<NotificationBatchConfig>(Constants.NotificationService, "getNotificationBatchConfig", new
            {
                os = p_Request.Os,
                appName = p_Request.AppName,
                date = p_Request.Date,
                hash = p_Request.Hash,
                origin = p_Request.Hash,
                locale = p_Request.Locale
            });
        }
    }
}
