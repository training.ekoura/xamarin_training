﻿using System;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Services
{
    public interface IUserService
    {
        Task<Sodexo.RemoteCallResult<MyRestauApp.Common.DAOServices.Models.UserSol>> GetUser(string p_Token);
    }
}