﻿using System;
namespace MyRestauApp.Common.DAOServices.Services
{
    public interface IDataOfflineService
    {
        MyRestauApp.Common.DAOServices.Models.DataOfflineModel Data { get; }
        void LoadConfig();
        void SaveConfig();
    }
}
