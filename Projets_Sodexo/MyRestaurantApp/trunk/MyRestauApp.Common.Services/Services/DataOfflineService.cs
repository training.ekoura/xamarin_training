﻿using MyRestauApp.Common.DAOServices.Models;
using Sodexo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Services
{
    public class DataOfflineService : MyRestauApp.Common.DAOServices.Services.IDataOfflineService
    {
          #region Champs
        private string ConfigFileName = "CONFIG_B8A2283CD2944B93B7867110F101AC71.config";
        private IFileService FileService;
        #endregion

        private DataOfflineModel _Data;
        public DataOfflineModel Data
        {
            get { return _Data; }
        }

        public DataOfflineService()
        {
            FileService = Sodexo.Framework.Services.Container().Resolve<IFileService>();
            LoadConfig();
        }

        public void LoadConfig()
        {
            if (FileService.Exist(ConfigFileName))
            {
                _Data = FileService.LoadObject<DataOfflineModel>(ConfigFileName);
              
            }
            else
            {
                _Data = new DataOfflineModel();
                _Data.ListUserData = new List<UserData>();
             
            }
        }

        public void SaveConfig()
        {
            FileService.SaveObject<DataOfflineModel>(ConfigFileName, _Data);
        }
    }
}
namespace Sodexo
{
    public static class DataOfflineServiceExtension
    {
        public static MyRestauApp.Common.DAOServices.Services.IDataOfflineService DataOfflineService(this Sodexo.IService p_Service)
        {
            return Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.DAOServices.Services.IDataOfflineService>();
        }
    }
}