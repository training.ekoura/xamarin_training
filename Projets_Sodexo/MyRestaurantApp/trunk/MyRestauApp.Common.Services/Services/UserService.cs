﻿using MyRestauApp.Common.DAOServices.Models;
using Sodexo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Services
{
    public class UserService : IUserService
    {
        public Task<RemoteCallResult<UserSol>> GetUser(string p_Token)
        {
            return Sodexo.Framework.Services.RemoteCallService().Call<UserSol>(Constants.UserService, "getUser", new
            {
                token = p_Token
            });
        }
    }
}
