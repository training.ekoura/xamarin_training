﻿using System;
namespace MyRestauApp.Common.DAOServices.Services
{
    public interface IGlobalConfigService
    {
        MyRestauApp.Common.DAOServices.Models.GlobalConfigModel Data { get; }
        void LoadConfig();
        void SaveConfig();
    }
}
