﻿using System;
namespace MyRestauApp.Common.DAOServices.Services
{
    public interface INotificationService
    {
        System.Threading.Tasks.Task<Sodexo.RemoteCallResult> AddUserSolAsync(string p_Token);
        System.Threading.Tasks.Task<Sodexo.RemoteCallResult<MyRestauApp.Common.DAOServices.Models.NotificationBatchConfig>> GetNotificationBatchConfigAsync(MyRestauApp.Common.DAOServices.Requests.GetNotificationBatchConfigRequest p_Request);
    }
}
