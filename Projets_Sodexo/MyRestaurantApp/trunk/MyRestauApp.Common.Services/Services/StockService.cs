﻿using MyRestauApp.Common.DAOServices.Models;
using MyRestauApp.Common.DAOServices.Requests;
using Sodexo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Services
{
    public class StockService : IStockService
    {
        public Task<RemoteCallResult<List<StockArticle>>> GetStocks(string p_Token, string p_ZoneRestaurationId, string p_Date)
        {
            return Sodexo.Framework.Services.RemoteCallService().Call<List<StockArticle>>(Constants.StockService, "getStocks", new
            {
                token = p_Token,
                zoneRestaurationId = p_ZoneRestaurationId,
                date = p_Date
            });
        }

        public Task<RemoteCallResult> UpdateStock(UpdateStockRequest p_Request)
        {
            return Sodexo.Framework.Services.RemoteCallService().Call(Constants.StockService, "updateStock", new
            {
                token = p_Request.Token,
                zoneRestaurationId = p_Request.ZoneRestaurationId,
                articleId = p_Request.ArticleId,
                addQuantite = p_Request.AddQuantite,
                date = p_Request.Date
            });
        }
    }
}
