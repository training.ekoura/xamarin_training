﻿using MyRestauApp.Common.DAOServices.Models;
using MyRestauApp.Common.DAOServices.Requests;
using Sodexo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Services
{
    public class CommandeService : ICommandeService
    {
        public Task<RemoteCallResult<Commande>> GetCommandeGerantAsync(string p_Token, int p_CommandeId)
        {
            return Sodexo.Framework.Services.RemoteCallService().Call<Commande>(Constants.CommandeService, "getCommandeGerant", new
            {
                token = p_Token,
                commandeId = p_CommandeId
            });
        }

        public Task<RemoteCallResult<CommandeDashboard>> GetDashboardGerantAsync(GetDashboardGerantRequest p_Request)
        {
            return Sodexo.Framework.Services.RemoteCallService().Call<CommandeDashboard>(Constants.CommandeService, "getDashboardGerant", new
            {
                token = p_Request.Token,
                dateDebut = p_Request.DateDebut,//au format dd/mm/yyyy
                dateFin = p_Request.DateFin,//au format dd/mm/yyyy
                zoneRestaurationId = p_Request.ZoneRestaurationId,
                listCommandeStatutLogicValue = p_Request.ListCommandesStatutLogicValue,
                delimiter = p_Request.Delimiter
            });
        }

        public Task<RemoteCallResult<List<Commande>>> GetCommandesGerantAsync(GetDashboardGerantRequest p_Request)
        {
            return Sodexo.Framework.Services.RemoteCallService().Call<List<Commande>>(Constants.CommandeService, "getCommandesGerant", new
            {
                token = p_Request.Token,
                dateDebut = p_Request.DateDebut,//au format dd/mm/yyyy
                dateFin = p_Request.DateFin,//au format dd/mm/yyyy
                zoneRestaurationId = p_Request.ZoneRestaurationId,
                listCommandeStatutLogicValue = p_Request.ListCommandesStatutLogicValue,
                delimiter = p_Request.Delimiter
            });
        }

        public Task<RemoteCallResult> UpdateCommandeStatutAsync(string p_Token, int p_CommandeId, string p_StatutLogicValue)
        {
            return Sodexo.Framework.Services.RemoteCallService().Call(Constants.CommandeService, "updateCommandeStatut", new
            {
                token = p_Token,
                commandeId = p_CommandeId,
                statutLogicValue = p_StatutLogicValue
            });
        }

        public Task<RemoteCallResult<List<StatutCommande>>> GetCommandeGerantActionPossibleAsync(string p_Token, int p_CommandeId)
        {
            return Sodexo.Framework.Services.RemoteCallService().Call<List<StatutCommande>>(Constants.CommandeService, "getCommandeGerantStatutsPossibles", new
            {
                token = p_Token,
                commandeId = p_CommandeId
            });
        }

        public Task<RemoteCallResult<List<StatutCommande>>> GetListCommandeStatutsAsync(string p_Locale)
        {
            return Sodexo.Framework.Services.RemoteCallService().Call<List<StatutCommande>>(Constants.CommandeService, "listCommandeStatuts", new
            {
                origin = Constants.AppName,
                locale = p_Locale
            });
        }

    }
}
