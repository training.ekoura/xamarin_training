﻿using MyRestauApp.Common.DAOServices.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Services
{
    public interface IStockService
    {
        Task<Sodexo.RemoteCallResult<List<MyRestauApp.Common.DAOServices.Models.StockArticle>>> GetStocks(string p_Token, string p_ZoneRestaurationId, string p_Date);
        Task<Sodexo.RemoteCallResult> UpdateStock(UpdateStockRequest p_Request);
    }
}