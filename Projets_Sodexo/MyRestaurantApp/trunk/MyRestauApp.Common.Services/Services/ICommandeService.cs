﻿using MyRestauApp.Common.DAOServices.Models;
using MyRestauApp.Common.DAOServices.Requests;
using Sodexo;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace MyRestauApp.Common.DAOServices.Services
{
    public interface ICommandeService
    {
        System.Threading.Tasks.Task<Sodexo.RemoteCallResult<MyRestauApp.Common.DAOServices.Models.Commande>> GetCommandeGerantAsync(string p_Token, int p_CommandeId);
        System.Threading.Tasks.Task<Sodexo.RemoteCallResult> UpdateCommandeStatutAsync(string p_Token, int p_CommandeId, string p_StatutLogicValue);
        Task<RemoteCallResult<List<StatutCommande>>> GetCommandeGerantActionPossibleAsync(string p_Token, int p_CommandeId);
        Task<RemoteCallResult<List<StatutCommande>>> GetListCommandeStatutsAsync(string p_Locale);
        Task<RemoteCallResult<List<Commande>>> GetCommandesGerantAsync(GetDashboardGerantRequest p_Request);
        Task<RemoteCallResult<CommandeDashboard>> GetDashboardGerantAsync(GetDashboardGerantRequest p_Request);
    }
}
