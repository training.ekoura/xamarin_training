﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Requests
{
    public class GetDashboardGerantRequest
    {
        public string Token { get; set; }
        public string DateDebut { get; set; }
        public string DateFin { get; set; }
        public string ZoneRestaurationId { get; set; }
        public string ListCommandesStatutLogicValue { get; set; }
        public string Delimiter { get; set; }
    }
}
