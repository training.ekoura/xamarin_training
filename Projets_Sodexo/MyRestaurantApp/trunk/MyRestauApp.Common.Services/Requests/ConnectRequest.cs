﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Requests
{
    public class ConnectRequest
    {
        public string LoginOrMail { get; set; }
        public string Pwd { get; set; }
        public string Locale { get; set; }
        public string Os { get; set; }
        public string AppVersion { get; set; }
        public string AppName { get; set; }
    }
}
