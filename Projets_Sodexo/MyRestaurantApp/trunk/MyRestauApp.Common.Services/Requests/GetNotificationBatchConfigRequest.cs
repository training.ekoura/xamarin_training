﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Requests
{
    public class GetNotificationBatchConfigRequest
    {
        public string Os { get; set; }
        public string AppName { get; set; }
        public string Date { get; set; }
        public string Hash { get; set; }
        public string Origin { get; set; }
        public string Locale { get; set; }
    }
}
