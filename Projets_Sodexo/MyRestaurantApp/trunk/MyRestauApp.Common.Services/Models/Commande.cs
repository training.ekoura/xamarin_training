﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Models
{
    public class Commande
    {
        [JsonProperty(PropertyName = "cid")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "cdc")]
        public string DateCreation { get; set; }

        [JsonProperty(PropertyName = "cdcfmt")]
        public string DateCreationFormat { get; set; }

        [JsonProperty(PropertyName = "cde")]
        public string DateEffective { get; set; }

        [JsonProperty(PropertyName = "cdefmt")]
        public string DateEffectiveFormat { get; set; }

        [JsonProperty(PropertyName = "cptt")]
        public double PrixTotalTTC { get; set; }

        [JsonProperty(PropertyName = "cpttfmt")]
        public string PrixTotalTTCFormat { get; set; }

        [JsonProperty(PropertyName = "cmpl")]
        public string MoyenPaiementLibelle { get; set; }

        [JsonProperty(PropertyName = "cmplv")]
        public string MoyenPaiementLogicValue { get; set; }

        [JsonProperty(PropertyName = "cia")]
        public bool IsAnnulable { get; set; }

        [JsonProperty(PropertyName = "sp")]
        public List<StatutCommande> ListStatutsPossibles { get; set; }

        [JsonProperty(PropertyName = "s")]
        public StatutCommande Statut { get; set; }

        [JsonProperty(PropertyName = "scd")]
        public string StatutCourantDate { get; set; }

        [JsonProperty(PropertyName = "scdfmt")]
        public string StatutCourantDateFormat { get; set; }

        [JsonProperty(PropertyName = "clid")]
        public int ClientId { get; set; }

        [JsonProperty(PropertyName = "zrid")]
        public string ZoneRestaurationId { get; set; }

        [JsonProperty(PropertyName = "zrn")]
        public string ZoneRestaurationNom { get; set; }

        [JsonProperty(PropertyName = "ac")]
        public int ArticlesCount { get; set; }

        [JsonProperty(PropertyName = "apc")]
        public int ArticlesPlatsCount { get; set; }

        [JsonProperty(PropertyName = "up")]
        public Convive UserPortail { get; set; }

        [JsonProperty(PropertyName = "abcr")]
        public List<ComposanteRecette> ArticlesByComposanteRecettes { get; set; }


        public string FormattedDateForView { get; set; }

        public string FormattedLabelForView { get; set; }

        public string FormattedLabelPlat { get; set; }

        public string FormattedLabelProduit { get; set; }

        public string FormattedLabelFirstLastName { get; set; }

        public bool IsNew { get; set; }

    }
}
