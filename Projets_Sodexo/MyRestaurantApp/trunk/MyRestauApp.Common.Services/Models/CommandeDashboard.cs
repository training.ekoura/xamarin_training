﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Models
{
    public class CommandeDashboard
    {
        [JsonProperty(PropertyName = "cnr")]
        public int NbrRestantes { get; set; }

        [JsonProperty(PropertyName = "cnre")]
        public int NbrRetirees { get; set; }

        [JsonProperty(PropertyName = "cnv")]
        public int NbrVendues { get; set; }

    }
}
