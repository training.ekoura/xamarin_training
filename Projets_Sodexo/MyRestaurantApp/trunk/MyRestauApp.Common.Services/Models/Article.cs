﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Models
{
    public class Article
    {
        [JsonProperty(PropertyName = "aid")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "alc")]
        public string LibelleCommercial { get; set; }

        [JsonProperty(PropertyName = "alt")]
        public string LibelleTechnique { get; set; }

        [JsonProperty(PropertyName = "aq")]
        public int Quantite { get; set; }
    }
}
