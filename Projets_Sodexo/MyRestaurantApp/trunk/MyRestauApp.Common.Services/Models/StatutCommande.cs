﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Models
{
    public class StatutCommande
    {
        [JsonProperty(PropertyName = "slv")]
        public string LogicValue { get; set; }

        [JsonProperty(PropertyName = "slg")]
        public string LibelleGerant { get; set; }

        [JsonProperty(PropertyName = "scc")]
        public string StatutCodeCouleur { get; set; }


        public int IdForView {get;set;}
        public bool IsChecked {get;set;}
        
    }
}
