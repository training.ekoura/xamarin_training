﻿using MyRestauApp.Common.DAOServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Models
{
    public class DataOfflineModel
    {
        // Les data à stocker
        public List<UserData> ListUserData { get; set; }
        public UserData CurrentUser { get; set; }
    }

    public class UserData
    {
        public UserSol UserDetails { get; set; }
        public int UserId { get; set; }
        public string LoginUser { get; set; }
        public List<DashboardDataByZR> DashboradDataByZR { get; set; }
        public DateTime DateWhenLastConnected { get; set; }
    }

    public class DashboardDataByZR
    {
        public string ZoneRestaurationId { get; set; }
        public List<DashboardUserByDate> DashboardUserByDate { get; set; }
    }

    public class DashboardUserByDate
    {
        public string DateDebut { get; set; }
        public string DateFin { get; set; }
        public CommandeDashboard CommandeDashboard { get; set; }
        public List<StockArticle> ListStocks { get; set; }
        public List<Commande> ListCommandes { get; set; }
    }


}
