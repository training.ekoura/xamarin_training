﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Models
{
    public class Site
    {
        [JsonProperty(PropertyName = "cs")]
        public string CodeSite { get; set; }

        [JsonProperty(PropertyName = "sl")]
        public string SiteLibelle { get; set; }

        [JsonProperty(PropertyName = "zr")]
        public List<ZoneRestauration> ZonesRestauration { get; set; }
    }
}
