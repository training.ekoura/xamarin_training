﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Models
{
    public class ZoneRestauration
    {
        [JsonProperty(PropertyName = "zrid")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "zrn")]
        public string Nom { get; set; }

        [JsonProperty(PropertyName = "zripr")]
        public bool IsPrecommande { get; set; }

        [JsonProperty(PropertyName = "zrtlv")]
        public string TypeLogicValue { get; set; }
    }
}
