﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Models
{
    public class StockArticle
    {
        [JsonProperty(PropertyName = "aid")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "zrid")]
        public string ZoneRestaurationId { get; set; }

        [JsonProperty(PropertyName = "alc")]
        public string LibelleCommercial { get; set; }

        [JsonProperty(PropertyName = "alt")]
        public string LibelleTechnique { get; set; }

        [JsonProperty(PropertyName = "as")]
        public string Stock { get; set; }

        [JsonProperty(PropertyName = "anr")]
        public string NbRestants { get; set; }

        [JsonProperty(PropertyName = "anre")]
        public string NbRetires { get; set; }

        [JsonProperty(PropertyName = "anv")]
        public string NbVendus { get; set; }
    }
}
