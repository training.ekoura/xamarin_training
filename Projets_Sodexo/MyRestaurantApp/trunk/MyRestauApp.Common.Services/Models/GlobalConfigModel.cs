﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Models
{
    public class GlobalConfigModel
    {
        public Token Token { get; set; }

        public DateTime TokenDate { get; set; }

        public DateTime DateWhenOnSleep { get; set; }
        
        public string DeviceOs { get; set; }

        public string AppVersion { get; set; }

        public string AppName { get; set; }

        public string LoginMail { get; set; }


        public List<StatutCommande> ListStatutSaved { get; set; }

        public List<int> ViewedOrdersIds { get; set; }
    }
}
