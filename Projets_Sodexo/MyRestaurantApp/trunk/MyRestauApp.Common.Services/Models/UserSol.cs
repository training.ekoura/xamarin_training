﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.DAOServices.Models
{
    public class UserSol
    {
        [JsonProperty(PropertyName = "usid")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "usn")]
        public string Nom { get; set; }

        [JsonProperty(PropertyName = "usp")]
        public string Prenom { get; set; }

        [JsonProperty(PropertyName = "usni")]
        public string NotificationId { get; set; }

        [JsonProperty(PropertyName = "s")]
        public List<Site> Site { get; set; }
    }
}
