﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sodexo;
using Xamarin.Forms;

namespace MyRestauApp.Common.Views
{
    public partial class Header : ContentView
    {
        public enum HeaderContentTypes
        {
            IsPubelle,
            IsCalendar,
            IsText,
            None
        }

        public enum HeaderTypes
        {
            IsDashboard,
            IsCommande,
            IsAlert,
            None
        }

        public bool Drawer;

        public Header()
        {
            StyleId = "HeaderContentView";
            InitializeComponent();

            menuButton.IsVisible = HeaderType == HeaderTypes.IsDashboard ? true : false;
            backButton.IsVisible = HeaderType != HeaderTypes.IsDashboard ? true : false;

            CalendarFilterStack.IsVisible = HeaderContentType == HeaderContentTypes.IsCalendar ? true : false;
            TextStack.IsVisible = HeaderContentType == HeaderContentTypes.IsText ? true : false;

            filterGrid.IsVisible = (HeaderType == HeaderTypes.IsCommande) && (HeaderContentType == HeaderContentTypes.IsCalendar) ? true : false;

            menuButton.Clicked += menuButton_Clicked;
            backButton.Clicked += backButton_Clicked;
            HeaderText.Text = HeaderType == HeaderTypes.IsDashboard ? Properties.Resources.HeaderType_Dashboard_label : HeaderType == HeaderTypes.IsCommande ? Properties.Resources.HeaderType_Command_label : Properties.Resources.HeaderType_Alert_label;

            filterImage.SetBinding(Image.SourceProperty, "IsVisibleFilterEnable", BindingMode.Default, new FilterImageSourceConverter());
        }

        #region === HeaderType ===

        public static readonly BindableProperty HeaderTypeProperty = BindableProperty.Create<Header, HeaderTypes>(p => p.HeaderType, HeaderTypes.None, propertyChanged: (d, o, n) => (d as Header).HeaderType_Changed(o, n));

        private void HeaderType_Changed(HeaderTypes oldvalue, HeaderTypes newvalue)
        {
            menuButton.IsVisible = HeaderType == HeaderTypes.IsDashboard ? true : false;
            HeaderText.Text = HeaderType == HeaderTypes.IsDashboard ? Properties.Resources.HeaderType_Dashboard_label : HeaderType == HeaderTypes.IsCommande ? Properties.Resources.HeaderType_Command_label : Properties.Resources.HeaderType_Alert_label;
            backButton.IsVisible = HeaderType != HeaderTypes.IsDashboard ? true : false;
        }

        public HeaderTypes HeaderType
        {
            get { return (HeaderTypes)GetValue(HeaderTypeProperty); }
            set { SetValue(HeaderTypeProperty, value); }
        }

        #endregion

        #region === HeaderContentType ===

        public static readonly BindableProperty HeaderContentTypeProperty = BindableProperty.Create<Header, HeaderContentTypes>(p => p.HeaderContentType, HeaderContentTypes.None, propertyChanged: (d, o, n) => (d as Header).HeaderContentType_Changed(o, n));

        private void HeaderContentType_Changed(HeaderContentTypes oldvalue, HeaderContentTypes newvalue)
        {
            CalendarFilterStack.IsVisible = HeaderContentType == HeaderContentTypes.IsCalendar ? true : false;
            TextStack.IsVisible = HeaderContentType == HeaderContentTypes.IsText ? true : false;
            filterGrid.IsVisible = (HeaderType == HeaderTypes.IsCommande) && (HeaderContentType == HeaderContentTypes.IsCalendar) ? true : false;
        }

        public HeaderContentTypes HeaderContentType
        {
            get { return (HeaderContentTypes)GetValue(HeaderContentTypeProperty); }
            set { SetValue(HeaderContentTypeProperty, value); }
        }

        #endregion

        void menuButton_Clicked(object sender, EventArgs e)
        {
            var mdp = this.Parent.Parent.Parent as MasterDetailPageView;
            OnToggleRequest();
            mdp.IsPresented = true;
        }

        public void OnToggleRequest()
        {
            Drawer = !Drawer;
        }

        void backButton_Clicked(object sender, EventArgs e)
        {
            Sodexo.Framework.Services.InteractionService().Close(this.Parent.Parent.GetType().Name);
        }

    }
}
