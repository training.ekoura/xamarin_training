﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyRestauApp.Common.Views
{
    public partial class BurgerMenuView : ContentPage, IBurgerMenuView
    {
        public BurgerMenuView()
        {
            InitializeComponent();
            // Hide navigation bar
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
        }
    }
}
