﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyRestauApp.Common.Themes
{
    public class ThemeMySodexo
    {
        public static void ThemeCallback(ResourceDictionary p_ResourceDictionary)
        {
            Theme.Apply(p_ResourceDictionary, new ThemeParameters()
            {
                ThemeExtension = "",
                ConnexionText = "My Sodexo",
                ThemeMaincolor = Color.FromHex("#241C4A"),
                StackOpacity = 0.85,
                HeaderLabelBackgroundColor = Color.White,
                HeaderLabelOpacity = 1,
                MainButtonBackgroundColor = Color.FromHex("#D60722"),
                TetiaryButtonTextColor= Color.White,
                HomePageLogoBackgroundColor = Color.White,
                HomePageFormBackgroundColor = Color.FromHex("#F2F2F2"),
                CarrouselDividerColor = Color.FromHex("#241C4A"),
                SecbuttonBackgroundColor = Color.FromHex("#F2F2F2"),
                OtherPageFormBackgroundColor = Color.White,
                TextColor = Color.FromHex("#241C4A"),
                EntryTextColor = Color.FromHex("#999999"),
                ListViewTextColor = Color.FromHex("#241C4A"),
                CreationPageUnderBarLabel = Color.FromHex("#999999"),
                MenuListSideImage = "waiter",
                ListBackGroundColor = Color.Black,
                NewsUnderImageBarColor = Color.FromHex("#241C4A"),
                BadgeTextColor = Color.FromHex("#241C4A"),
                BadgeStack1Color = Color.White,
                BadgeStack2Color = Color.White,

                BadgeBorderColor = Color.FromHex("#241C4A"),
                BadgeDividerColor = Color.FromHex("#D60722"),
                BadgeAmountColor = Color.FromHex("#241C4A"),
                BadgeSelectedAmountColor = Color.White,
                BadgeSelectedBackgroundColor = Color.FromHex("#D60722"),
                BadgeListSeperatorColor = Color.Gray,
                FooterImageMultiplier = 1,
                ServiceViewHeaderColor = Color.FromHex("#241C4A"),
                ServiceViewStackOpacity = 1,
                ServiceViewHeaderTextColor = Color.White,
                ServiceViewPrimaryColor = Color.FromHex("#969696"),
                ServiceViewSecondaryColor = Color.FromHex("#E30613"),
                ParameterDividerLabelColor = Color.FromHex("#A9A9A9"),
                MenuViewMainHeaderColor = Color.FromHex("#65676A"),
                MenuViewMainHeaderOpacity = 1,
                MenuScrollViewBackgroundColor = Color.White,
                MenuScrollViewBackgroundColorOpacity = 1,
                //MenuViewRecetteNomColor = Color.FromHex("#A9A9A9"),
                MenuViewRecetteNomColor = Color.FromHex("#241C4A"),
                l_ComposanteLayoutColor = Color.White,
                AllergeneTextColor=Color.FromHex("#241C4A"),

                HamburgerBackGroundColor = Color.White,
                HamburgerFavorisAndAjouterBackGroundColor = Color.FromHex("#D3D0C9"),
                HamburgerFavorisAndAjouterSeperatorColor = Color.White,
                HamburgerListViewLabelAndLabelColor = Color.FromHex("#241C4A"),
                HeaderBarLabelTextColor = Color.FromHex("#241C4A"),
                NewsTitleTextColor = Color.FromHex("#E30613"),
                CreationListViewSeperatorColor = Color.FromHex("#241C4A"),
				CreationPageUnderBarLabelBottom = Color.FromHex("#636363"),
                BurgerStatusBarColor = Color.White,
                UnderLineZrColor = Color.FromHex("#cccccc"),

                NewMenuViewMainHeaderColor = Color.FromHex("#241C4A"),
                NewMenuViewMainHeaderOpacity = 1,
                NewMenuViewSwipeOptionBackgroundColor = Color.FromHex("#D60722"),
                NewMenuViewSwipeOptionOpacity = 1,
                NewMenuViewDateListViewTextColor = Color.FromHex("#241C4A"),
                NewMenuViewDateListViewSeperatorBackgroundColor = Color.Gray,
                NewMenuViewVoirLaCarteBackgroundColor = Color.FromHex("#5C6165"),
                NewMenuViewButtomNotificationBackgroundColor = Color.White,
                NewMenuViewButtomNotificationTextColor = Color.FromHex("#241C4A"),
                UnderBarLineColor = Color.FromHex("#241C4A"),
                TypeRecetteColor = Color.FromHex("#75797D"),
                TypeRecetteLabelStackBackgroundColor = Color.FromHex("#75797D"),
                TypeRecetteListStackBackgroundColor = Color.White,
                OrderNotificationTextColor = Color.White,
                OrderNotificationBackgroundColor = Color.FromHex("#D60722"),
                OrderProcessButtonTextColor = Color.White,
                OrderProcessItemHeaderTextColor = Color.FromHex("#75797D"),
                OrderProcessLabelTextColor = Color.FromHex("#424242"),
                OrderNotificationDateTextColor = Color.White,
                OrderProcessItemPlusTextColor = Color.FromHex("#75797D"),
                RecetteDetailUnderBarColor = Color.FromHex("#D5021D"),
                RecetteDetailTitleStackBackGroundColor = Color.FromHex("#1D2860"),
                RecetteDetailHeaderTitleColor = Color.White,
                
                RecetteDetailSectionStackBackGroundColor = Color.FromHex("#D5021D"),
                CartButtonBackColor = Color.FromHex("#424242"),
				OrderProcessPopUpHeaderTitleTextColor = Color.FromHex("1D2860"),

                #region Gerant

                GerantMainColor = Color.FromHex("#C10418"),
                GerantSecondaryColor = Color.FromHex("#241C4B"),
                GerantPageBackgroundColor = Color.FromHex("#F2F2F2"),
                GerantSecondaryPageBackgroundColor = Color.White,
                PrimaryTextColor = Color.White,
                SecondaryTextColor=Color.Black,
                TertiaryTextColor = Color.Gray,
                RepeaterColor = Color.White,
                PopinBackgroundColor = Color.White,
                RoundedButtonColor = Color.FromHex("#C30D21"),
                FixedButtoncolor = Color.FromHex("#F2F2F2")

                #endregion
            });
        }
    }
}
