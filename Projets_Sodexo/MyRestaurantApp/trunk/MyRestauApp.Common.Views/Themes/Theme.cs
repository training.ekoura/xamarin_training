﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Sodexo.Controls;
using Sodexo;

namespace MyRestauApp.Common.Themes
{
    /// <summary>
    /// Nous utilisons une classe de paramètres pour centraliser et normaliser les thèmes.
    /// Sans cette logique, toute différence de délcaration dans un thème pourrait avoir
    /// des effets de bord lors du passage d'un thème à l'autre.
    /// </summary>
    public class ThemeParameters
    {
        private const string ND = "[NOT DEFINED]";
        public const string ImageDirectory = "MySodexo.Common.Views.";
        public const string BackgroundImageLocation = "MySodexo.Common.Views.Images.BackGroundImage";
        public const string Logo = "Logo";
        public const string BurgerFooter = "BurgerFooter";
        public const string Back = "back";
        public const string Menu = "Menu";
        public const string News = "News";
        public const string CreerBars = "CreerBars";

        public ThemeParameters()
        {
            HeaderLabelBackgroundColor = Color.Black;
            ThemeMaincolor = Color.White;
        }

        public string ThemeExtension { get; set; }
        public Color TypeRecetteColor { get; set; }
        public Color TypeRecetteLabelStackBackgroundColor { get; set; }
        public Color TypeRecetteListStackBackgroundColor { get; set; }
        public Color ThemeMaincolor { get; set; }
        public Color CartButtonBackColor { get; set; }
        public Color UnderLineZrColor { get; set; }
        public string ConnexionText { get; set; }
        public string MenuListSideImage { get; set; }
        public Color HeaderLabelBackgroundColor { get; set; }
        public Color HomePageFormBackgroundColor { get; set; }
        public Color CarrouselDividerColor { get; set; }
        public Color HeaderBarLabelTextColor { get; set; }
        public Color NewsTitleTextColor { get; set; }
        public Color OtherPageFormBackgroundColor { get; set; }
        public Color HomePageLogoBackgroundColor { get; set; }
        public Color SecbuttonBackgroundColor { get; set; }
        public Color MainButtonBackgroundColor { get; set; }
        public Color MenuScrollViewBackgroundColor { get; set; }
        public double MenuScrollViewBackgroundColorOpacity { get; set; }
        public Color TetiaryButtonTextColor { get; set; }
        public Color TextColor { get; set; }
        public Color EntryTextColor { get; set; }
        public Color ListViewTextColor { get; set; }
        public Color CreationPageUnderBarLabel { get; set; }
        public Color CreationPageUnderBarLabelBottom { get; set; }
        public string BackGroundImage { get; set; }
        public double StackOpacity { get; set; }
        public Color ListBackGroundColor { get; set; }
        public double FooterImageMultiplier { get; set; }
        public double HeaderLabelOpacity { get; set; }
        public Color BadgeStack1Color { get; set; }
        public Color BadgeStack2Color { get; set; }
        public Color NewsUnderImageBarColor { get; set; }
        public Color BadgeAmountColor { get; set; }
        public Color BadgeTextColor { get; set; }
        public Color BadgeSelectedAmountColor { get; set; }
        public Color BadgeSelectedBackgroundColor { get; set; }
        public Color BadgeDividerColor { get; set; }
        public Color BadgeListSeperatorColor { get; set; }
        public Color ParameterDividerLabelColor { get; set; }
        public Color ServiceViewHeaderColor { get; set; }
        public double ServiceViewStackOpacity { get; set; }
        public Color ServiceViewHeaderTextColor { get; set; }
        public Color ServiceViewPrimaryColor { get; set; }
        public Color ServiceViewSecondaryColor { get; set; }
        public Color MenuViewMainHeaderColor { get; set; }
        public double MenuViewMainHeaderOpacity { get; set; }
        public Color MenuViewRecetteNomColor { get; set; }
        public Color l_ComposanteLayoutColor { get; set; }
        public Color AllergeneTextColor { get; set; }
        public Color BadgeBorderColor { get; set; }

        public Color HamburgerBackGroundColor { get; set; }
        public Color HamburgerFavorisAndAjouterBackGroundColor { get; set; }
        public Color HamburgerFavorisAndAjouterSeperatorColor { get; set; }
        public Color HamburgerListViewLabelAndLabelColor { get; set; }
        public Color CreationListViewSeperatorColor { get; set; }
		public Color BurgerStatusBarColor { get; set; }

        public Color NewMenuViewMainHeaderColor { get; set; }
        public double NewMenuViewMainHeaderOpacity { get; set; }
        public Color NewMenuViewSwipeOptionBackgroundColor { get; set; }
        public double NewMenuViewSwipeOptionOpacity { get; set; }
        public Color UnderBarLineColor { get; set; }

        public Color NewMenuViewDateListViewTextColor { get; set; }
        public Color NewMenuViewDateListViewStatusTextColor { get; set; }
        public Color NewMenuViewDateListViewSeperatorBackgroundColor { get; set; }
        public Color NewMenuViewVoirLaCarteBackgroundColor { get; set; }
        public Color NewMenuViewButtomNotificationBackgroundColor { get; set; }
        public Color NewMenuViewButtomNotificationTextColor { get; set; }
        public Color OrderNotificationTextColor { get; set; }
        public Color OrderNotificationDateTextColor { get; set; }
        public Color OrderProcessButtonTextColor { get; set; }
        public Color OrderProcessItemHeaderTextColor { get; set; }
        public Color OrderProcessPopUpHeaderTitleTextColor { get; set; }
        public Color OrderProcessItemPlusTextColor { get; set; }
        public Color OrderProcessLabelTextColor { get; set; }
        public Color OrderNotificationBackgroundColor { get; set; }
        public Color RecetteDetailUnderBarColor{ get; set; }
        public Color RecetteDetailTitleStackBackGroundColor { get; set; }
        public Color RecetteDetailHeaderTitleColor { get; set; }
        public Color RecetteDetailSectionStackBackGroundColor { get; set; }
        public Color allergenConcerneColor { get; set; }
        public Color allergenNonConcerneColor { get; set; }

        #region Gerant

        public Color GerantMainColor { get; set; }
        public Color GerantSecondaryColor { get; set; }
        public Color GerantPageBackgroundColor { get; set; }
        public Color GerantSecondaryPageBackgroundColor { get; set; }
        public Color PrimaryTextColor { get; set; }
        public Color SecondaryTextColor { get; set; }
        public Color TertiaryTextColor { get; set; }
        public Color RepeaterColor { get; set; }
        public Color PopinBackgroundColor { get; set; }
        public Color FixedButtoncolor { get; set; }
        public Color RoundedButtonColor { get; set; }

        #endregion

    }

    /// <summary>
    /// Cette classe et cette méthode existe pour les même raisons qu'exprimées ci-dessus.
    /// </summary>
    public static class Theme
    {
        #region ==ResourceKeys==
        public const string BackGroundImage = "BackGroundImage";
        public const string ConnexionScreenImageStyle = "ConnexionScreenImageStyle";

        public const string CONST_DeleteImageStyle = "CONST_DeleteImageStyle";
        
        public const string MENUS_DATEPANEL_COLOR = "MenusDatePanelColor";
        public const string MENUS_DATEPANEL_BACKCOLOR = "MenusDatePanelBackColor";
        public const string MENUS_PREVIOUS_IMAGESOURCE = "MenusPreviousImageSource";
        public const string CHECKBOX_OFF_IMAGESOURCE = "Checkboxoffimagesource";
        public const string CHECKBOX_ON_IMAGESOURCE = "Checkboxonimagesource";
        public const string MENUS_NEXT_IMAGESOURCE = "MenusNextImageSource";
        public const string MENUS_MENUPANEL_COLOR = "MenusMenuPanelColor";
        public const string MENUS_MENUPANEL_BACKCOLOR = "MenusMenuDatePanelBackColor";
        public const string MENUS_RECETTENOM_COLOR = "MENUS_RECETTENOM_COLOR";
        public const string MENUS_LabelComposante_COLOR = "MENUS_LabelComposante_COLOR";
        public const string MENUS_INFO_DIVIDER_LABEL = "M_infoDividerLabel";
        public const string MENUS_TEXT_STYLE = "dTextStyle";
        public const string MENUS_ALLERGENE_STYLE = "allergeneStyle";
        public const string MENUS_ALLERGENEHIDE_STYLE = "allergeneHideStyle";
        public const string MENUS_SCROLLVIEW_STYLE = "MenuScrollViewStyle";
        public const string CONST_ORDERPROCESSITEMHEADERTEXTCOLOR = "CONST_ORDERPROCESSITEMHEADERTEXTCOLOR";
        public const string CONST_ORDERPROCESSLABELTEXTCOLOR = "CONST_ORDERPROCESSLABELTEXTCOLOR";
        public const string CONST_ORDERPROCESSITEMPLUSTEXTCOLOR = "CONST_ORDERPROCESSITEMPLUSTEXTCOLOR";
        public const string CONST_ORDERPROCESSPOPUPHEADERLABELTEXTCOLOR = "CONST_ORDERPROCESSPOPUPHEADERLABELTEXTCOLOR";
        public const string CONST_RECETTEDETAILTOPANDBOTTOMBARLABEL = "CONST_RECETTEDETAILTOPANDBOTTOMBARLABEL";
        public const string CONST_RECETTEDETAILTITLESTACKBACKGROUNDCOLOR = "CONST_RECETTEDETAILTITLESTACKBACKGROUNDCOLOR";
        public const string CONST_RECETTEDETAILHEADERTITLECOLOR = "CONST_RECETTEDETAILHEADERTITLECOLOR";
        public const string CONST_RECETTEDETAILSECTIONSTACKBACKGROUNDCOLOR = "CONST_RECETTEDETAILSECTIONSTACKBACKGROUNDCOLOR";
        public const string CONST_RECETTEDETAILLIKEONSTYLE = "CONST_RECETTEDETAILLIKEONSTYLE";
        public const string CONST_RECETTEDETAILLIKESTYLE = "CONST_RECETTEDETAILLIKESTYLE";
        public const string CONST_EMPTYCART_STYLE = "CONST_EMPTYCART_STYLE";
        public const string CONST_REMOVECARD_STYLE = "CONST_REMOVECARD_STYLE";
        public const string CONST_STACK_BACKGROUNDCOLOR_TRANSPARENT = "CONST_STACK_BACKGROUNDCOLOR_TRANSPARENT";

        public const string ENFANT_EDIT_IMAGESOURCE = "EnfantEditImageSource";
        public const string ENFANT_DELETE_IMAGESOURCE = "EnfantDeleteImageSource";

        public const string ListViewItemImageBG_IMAGESOURCE = "ListViewItemImageBG_IMAGESOURCE";

        public const string BackImage = "BackImage";
        public const string MenuImage = "MenuImage";

        public const string BadgeStack2LabelStyle = "BadgeStack2LabelStyle";
        public const string BadgeStack3LabelStyle = "BadgeStack3LabelStyle";
        public const string BadgeDividerStyle = "BadgeDividerStyle";
        public const string ActivityIndicatorMessageStyle = "ActivityIndicatorMessageStyle";
        public const string HeaderBarLabel = "HeaderBarLabel";
        public const string HamBurgerHeaderBarLabel = "HamBurgerHeaderBarLabel";
        public const string NewsTitleStyle = "NewsTitleStyle";
        public const string HeaderUnderBarLabel = "HeaderUnderBarLabel";
        public const string NewsUnderImageBar = "NewsUnderImageBar";
        public const string PictoUnderBarLabel = "PictoUnderBarLabel";
        public const string CarrouselDividerLabel = "CarrouselDividerLabel";
        public const string UnderBarLine = "UnderBarLine";
        public const string UnderZRline = "UnderZRline";
        public const string UnderZRDetailLine = "UnderZRDetailLine";
        public const string ListViewBarLabel = "ListViewBarLabel";
        public const string M_infoDividerLabel = "M_infoDividerLabel";
        public const string N_infoDividerLabel = "N_infoDividerLabel";
        public const string CreationPageUnderBarLabel = "CreationPageUnderBarLabel";
        public const string CreationPageUnderBarLabelBottom = "CreationPageUnderBarLabelBottom";
        public const string Hyperlinkstyle = "Hyperlinkstyle";
        public const string TextStyle = "TextStyle";
        public const string ThemedTextStyle = "ThemedTextStyle";
        public const string AllergeneTextStyle = "AllergeneTextStyle";
        public const string dTextStyle = "dTextStyle";
        public const string PrimaryHeaderTextStyle = "PrimaryHeaderTextStyle";
        public const string PrimaryTextStyle = "PrimaryTextStyle";
        public const string SeconadryTextStyle = "SeconadryTextStyle";
        public const string ListTextStyle = "ListTextStyle";
        public const string CListTextStyle = "CListTextStyle";
        public const string HamburgerListViewAndLabelStyle = "HamburgerListViewAndLabelStyle";
        public const string CreationLabelStyle = "CreationLabelStyle";
        public const string NewBodyStyle = "NewBodyStyle";
        public const string ValidationTextStyle = "ValidationTextStyle";
        public const string ThemedLabelStyle = "ThemedLabelStyle";
        public const string PictoSondageImageStyle = "PictoSondageImageStyle";
        

        public const string BadgeAmountButtonStyle = "BadgeAmountButtonStyle";
        public const string ThemedSelectedAmountStyle = "ThemedSelectedAmountStyle";
        public const string MainbuttonStyle = "MainbuttonStyle";
        public const string OrderProcesMainbuttonStyle = "OrderProcesMainbuttonStyle";
        public const string CONST_CARTBUTTONSTYLE = "CONST_CARTBUTTONSTYLE";
        public const string SecbuttonStyle = "SecbuttonStyle";
        public const string TertiarybuttonStyle = "TertiarybuttonStyle";
        public const string BackButtonImageStyle = "BackButtonImageStyle";
        public const string MenuButtonImageStyle = "MenuButtonImageStyle";
        public const string OverlayCancelButtonStyle = "OverlayCancelButtonStyle";
        public const string BadgeButtonStyle = "BadgeButtonStyle";
        public const string radioButtonCheckedImageStyle = "radioButtonCheckedImageStyle";
        public const string radioButtonUncheckedImageStyle = "radioButtonUncheckedImageStyle";


        public const string ForwardImageStyle = "ForwardImageStyle";
        public const string LogoImageStyle = "LogoImageStyle";
        public const string AproposLogoImageStyle = "AproposLogoImageStyle";
        public const string NewsImageStyle = "NewsImageStyle";
        public const string SondageImageStyle = "SondageImageStyle";
        public const string SondageQuestionBackgroundImageStyle = "SondageQuestionBackgroundImageStyle";
        public const string HeartBreakSondageImageStyle = "HeartBreakSondageImageStyle";
        public const string LikeSondageImageStyle = "LikeSondageImageStyle";
        public const string DisLikeSondageImageStyle = "DisLikeSondageImageStyle";
        public const string OrSondageImageStyle = "OrSondageImageStyle";
        
        public const string SideImageStyle = "SideImageStyle";
        public const string HomeSideImageStyle = "HomeSideImageStyle";
        public const string MailPictoStyle = "MailPictoStyle";
        public const string TelPictoStyle = "TelPictoStyle";
        public const string BurgerImageStyle = "BurgerImageStyle";
        public const string ContactPictoStyle = "ContactPictoStyle";
        public const string CreerBar1ImageStyle = "CreerBar1ImageStyle";
        public const string CreerBar2ImageStyle = "CreerBar2ImageStyle";
        public const string CreerBar3ImageStyle = "CreerBar3ImageStyle";
        public const string PictoFormuleImageStyle = "PictoFormuleImageStyle";
        public const string InstitutuionImageStyle = "InstitutuionImageStyle";
        public const string ZoneRestaurationImageStyle = "ZoneRestaurationImageStyle";
        public const string ZoneRestaurationInfosPictoStyle = "ZoneRestaurationInfosPictoStyle";
        public const string SmallZoneRestaurationInfosPictoStyle = "SmallZoneRestaurationInfosPictoStyle";
        public const string CalendarPictoStyle = "CalendarPictoStyle";
        public const string HorlogeImageStyle = "HorlogeImageStyle";
        public const string EntreeImageStyle = "EntreeImageStyle";
        public const string WPFooterImageStyle = "WPFooterImageStyle";
        public const string AndroidFooterImageStyle = "AndroidFooterImageStyle";
        public const string iOSFooterImageStyle = "iOSFooterImageStyle";
        public const string HamburgerFooterImageStyle = "HamburgerFooterImageStyle";
        public const string HomeBarImageStyle = "HomeBarImageStyle";



        public const string BurgerStatusBarStyle = "BurgerStatusBarStyle";
        public const string SliderStyle = "SliderStyle";
        public const string searchStyle = "searchStyle";
        public const string StackStyle = "StackStyle";
        public const string CONST_CARTSTACKBUTTONBACKCOLOR = "CONST_CARTSTACKBUTTONBACKCOLOR";
        public const string CustStackStyle = "CustStackStyle";
        public const string Cust2StackStyle = "Cust2StackStyle";
        public const string HamburgerStyle = "HamburgerStyle";
        public const string HamburgerFavorisAndAjouterStyle = "HamburgerFavorisAndAjouterStyle";
        public const string PrimaryBadgeStackStyle = "PrimaryBadgeStackStyle";
        public const string SecondaryBadgeStackStyle = "SecondaryBadgeStackStyle";
        public const string LogoStackStyle = "LogoStackStyle";
        public const string FormStackStyle = "FormStackStyle";
        public const string ActivityIndicatorStyle = "ActivityIndicatorStyle";
        public const string FieldHeader = "FieldHeader";
        public const string ListViewstyle = "ListViewstyle";
        public const string BadgeListViewstyle = "BadgeListViewstyle";
        public const string mSitesListViewstyle = "mSitesListViewstyle";
        public const string PickerStyle = "PickerStyle";

        public const string IMAGE_CIRCLE_TYPE_RECETTE_BORDER_COLOR = "IMAGE_CIRCLE_TYPE_RECETTE_BORDER_COLOR";
        public const string IMAGE_CIRCLE_TYPE_RECETTE_LABEL_BACKGROUNDCOLOR = "IMAGE_CIRCLE_TYPE_RECETTE_LABEL_BACKGROUNDCOLOR";

        public const string GridUp = "GridUp";

        public const string PanierImageStyle = "PanierImageStyle";
        public const string LacarteImageStyle = "LacarteImageStyle";
        public const string LaCarteForwardImageStyle = "LaCarteForwardImageStyle";
        public const string AlertImageStyle = "AlertImageStyle";
        public const string NotificationImageStyle = "NotificationImageStyle";
        public const string CartImageStyle = "CartImageStyle";
        public const string CheckImageStyle = "CheckImageStyle";
        
        public const string TimerImageStyle = "TimerImageStyle";
        public const string PlusRecetteImageStyle = "PlusRecetteImageStyle";
        public const string InfoRecetteImageStyle = "InfoRecetteImageStyle";

        public const string NewMenuViewButtomNotificationTextStyle = "NewMenuViewButtomNotificationTextStyle";
        public const string NewMenuViewDateListViewTextColorStyle = "NewMenuViewDateListViewTextColorStyle";
        public const string NewMenuViewDateListViewStatusTextColorStyle = "NewMenuViewDateListViewStatusTextColorStyle";
        public const string OrderNotificationLabelTextColor = "OrderNotificationLabelTextColor";
        public const string OrderNotificationDateTextColor = "OrderNotificationDateTextColor";

        public const string SpecialWPStack2 = "SpecialWPStack2";

        public const string NewMenuViewDateListViewStyle = "NewMenuViewDateListViewStyle";

        public const string OrderNotificationStackBackgroundColor = "OrderNotificationStackBackgroundColor";
        public const string NewMenuViewMainHeaderStyle = "NewMenuViewMainHeaderStyle";
        public const string NewMenuViewSwipeOptionStyle = "NewMenuViewSwipeOptionStyle";
        public const string NewMenuViewVoirLaCarteStyle = "NewMenuViewVoirLaCarteStyle";
        public const string NewMenuViewButtomNotificationStyle = "NewMenuViewButtomNotificationStyle";
        public const string TypeRecetteBackGroundColor = "TypeRecetteBackGroundColor";
        public const string PickerStyle2 = "PickerStyle2";

        public const string PinImageStyle = "PinImageStyle";
        public const string MesCommandesForwardImageStyle = "MesCommandesForwardImageStyle";

        public const string CommandImageStyle = "CommandImageStyle";
        public const string CommandStatusImageStyle = "CommandStatusImageStyle";
        public const string HighLineArcticleQtyImageStyle = "HighLineArcticleQtyImageStyle";
        public const string StarImageStyle = "StarImageStyle";

        public const string ThemedRoundedButtonStyle = "ThemedRoundedButtonStyle";
        public const string AllergeneConcerneStyle = "AllergeneConcerneStyle";
        public const string AllergeneNonConcerneStyle = "AllergeneNonConcerneStyle";

        //Gerant Keys =============================================
        public const string ContentPageStyle = "ContentPageStyle";
        public const string DefaultGridStyle = "DefaultGridStyle";
        public const string DefaultContentViewStyle = "DefaultContentViewStyle";
        public const string PopinStyle = "PopinStyle";
        public const string TitlePopinStyle = "TitlePopinStyle";
        public const string GerantPrimaryTextStyle = "GerantPrimaryTextStyle";
        public const string GerantSecondaryTextStyle = "GerantSecondaryTextStyle";
        public const string GerantEntete36LabelStyle = "GerantEntete36LabelStyle";
        public const string GerantEntete16LabelStyle = "GerantEntete16LabelStyle";
        public const string MainThemedGridStyle = "MainThemedGridStyle";
        public const string SecondaryThemedGridStyle = "SecondaryThemedGridStyle";
        public const string WhiteThemedGridStyle = "WhiteThemedGridStyle";
        public const string notifImage = "notifImage";
        public const string notifButtonImageStyle = "notifButtonImageStyle";
        public const string RoundedButtonStyle = "RoundedButtonStyle";
        public const string RoundedCustomButtonStyle = "RoundedCustomButtonStyle";
        public const string PopUpHeaderStyle = "PopUpHeaderStyle";
        public const string popIncancelImageStyle = "popIncancelImageStyle";
        public const string PopinContentTextStyle = "PopinContentTextStyle";
        public const string GerantPageBackgroundColor = "GerantPageBackgroundColor";
        public const string GerantSecondaryPageBackgroundColor = "GerantSecondaryPageBackgroundColor";
        public const string GerantMainColor = "GerantMainColor";
        public const string GerantSecondaryColor = "GerantSecondaryColor";
        public const string RepeaterColor = "RepeaterColor";
        public const string PrimaryTextColor = "PrimaryTextColor";
        public const string SecondaryTextColor = "SecondaryTextColor";
        public const string TertiaryTextColor = "TertiaryTextColor";
        public const string DarkPaddedPopInStyle = "DarkPaddedPopInStyle";
        public const string PopinBackgroundColor = "PopinBackgroundColor";

        // Anciens styles MySodexo réutilisés pour MyRestau
        public const string EntryStyle = "EntryStyle";
        public const string SecMainbuttonStyle = "SecMainbuttonStyle";
        public const string SpecialWPStack = "SpecialWPStack";
        public const string FixedButtonColorStyle = "FixedButtonColorStyle";
        public const string BUTTON_FIXED_HEIGHT = "BUTTON_FIXED_HEIGHT";
        public const string BUTTON_LABEL_STYLE = "BUTTON_LABEL_STYLE";
        public const string BUTTON_EXTENDEDLABEL_STYLE = "BUTTON_EXTENDEDLABEL_STYLE";
        public const string OrderListViewItemHeight = "OrderListViewItemHeight";
        public const string ConnexionPopInStyle = "ConnexionPopInStyle";
        public const string ConnexionTitlePopinStyle = "ConnexionTitlePopinStyle";
        public const string HeaderImageStyle = "HeaderImageStyle";
        public const string HeaderImageStackStyle = "HeaderImageStackStyle";
        public const string HeaderImageGridStyle = "HeaderImageGridStyle";
        public const string ChangeStockImageStyle = "ChangeStockImageStyle";
        #endregion

        public static void Apply(ResourceDictionary p_ResourceDictionary, ThemeParameters p_Parameters)
        {
            // Il est possible d'adapter aussi le thème en  fonction de la plateforme.
            // Il est possible de faire toutes sortes de calcules, comme par exemple retrouver
            // la résolution du Device et calculer la taille de police la plus appropriée.
            // Toutes sortes de scénarios sont possibles : c'est la raison des thèmes en C#.
            // Ici nous adaptons simplement la taille de police par défaut.

            p_ResourceDictionary.Add(GridUp, 4);

            //Custon defined locations and values to be used in differentiating themes and OS below
            #region ==Local File Locations per theme===
            string ConnexionScreenImageLocation = "";
            string pictoFormuleLocation = "";
            string HorlogeImageLocation = "";
            string EntreImageLocation = "";
            string ZoneRestaurationImageLocation = "";
            string ZoneRestaurationInfoPictoLocation = "";
            string SmallZoneRestaurationInfoPictoLocation = "";

            string LogoLocation = "";
            string AproposLogoLocation = "";
            string checkBoxOffLocation = "";
            string checkBoxOnLocation = "";
            
            string l_NBurgerFooterLocation = "";
            double backButtonWidth = 0;
            string creerBarImageLocation = "";
            string creerBar2ImageLocation = "";
            string creerBar3ImageLocation = "";

            string pinImageLocation = "";

            double l_burgerIconWidth = 0;
            string backImageLocation = "";
            string MenuImageLocation = "";
            string HeaderImageLocation = "";
            string FooterImageLocation = "";
            double ContactPictoImageWidth = 0;
            string PictoMailLocation = "";
            string PictoTelLocation = "";
            string ForwardImageLocation = "";
            string CalendarPictoImageLocation = "";
            string MenuPreviousDate = "";
            string MenuNextDate = "";
            string showAllergeneLocation = "";
            string hideAllergeneLocation = "";
            string defaultSondageBackGroundImage = "";
            string staticNotificationBackgroundImage = "";
            string staticCartImage = "";
            string staticCheckImage = "";
            string defaultSondageBackGroundRadiusImage = "";
            string defaultLikeSondageBackGroundImage = "";
            string defaultDisLikeSondageBackGroundImage = "";
            string defaultOrSondageBackGroundImage = "";
            string pictoSondageImagePath = "";
            string timerImageLocation = "";
            string panierImageLocation = "";
            string LaCarteImageLocation = "";
            string LaCarteForwardImageLocation = "";
            string MesCommandesForwardImageLocation = "";
            string alertImageLocation = "";
            string PlusRecetteImageLocation = "";
            string InfoRecetteImageLocation = "";
            string Const_RecetteLikeOnImagePath = "";
            string Const_RecetteLikeImagePath = "";
            string Const_DeleteImagePath = "";

            string CommandImageLocation = "";
            string CommandStatusImageLocation = "";   
            string HighLineArcticleQtyImagePath = "";

            string StarImageLocation = "";
            string Const_EmptyCart_Path = "";
            string Const_RemoveCard_Path = "MySodexo.Common.Views.Images.Icons.ErrorSco.png";

            string radioButtonCheckedImageLocation = "";
            string radioButtonUncheckedImageLocation = "";

            string notifImageLocation = "";
            #endregion

            p_ResourceDictionary.Add(BackGroundImage, "");

            //Used to filter based on different theme config
            #region === OS Resources(Switch Statements) ===

            switch (Device.OS)
            {
                case TargetPlatform.Android:
                    ConnexionScreenImageLocation = String.Format("background.png");
                    LogoLocation = String.Format("Logo{0}.png", p_Parameters.ThemeExtension);
                    AproposLogoLocation = String.Format("AproposLogo{0}.png", p_Parameters.ThemeExtension);
                    checkBoxOffLocation = String.Format("CaseNonCoche{0}.png", p_Parameters.ThemeExtension);
                    checkBoxOnLocation = String.Format("CaseCoche{0}.png", p_Parameters.ThemeExtension);
                    l_NBurgerFooterLocation = String.Format("{0}{1}.png", ThemeParameters.BurgerFooter, p_Parameters.ThemeExtension);
                    backButtonWidth = 90;
                    creerBarImageLocation = String.Format("Creer1_Bar_{0}.png", p_Parameters.ThemeExtension);
                    creerBar2ImageLocation = String.Format("Creer2_Bar_{0}.png", p_Parameters.ThemeExtension);
                    creerBar3ImageLocation = String.Format("Creer3_Bar_{0}.png", p_Parameters.ThemeExtension);
                    l_burgerIconWidth = 30;
                    backImageLocation = String.Format("back{0}.png", p_Parameters.ThemeExtension);
                    HeaderImageLocation = String.Format("Header{0}.png", p_Parameters.ThemeExtension);
                    FooterImageLocation = String.Format("footer{0}.png", p_Parameters.ThemeExtension);
                    staticNotificationBackgroundImage = String.Format("notification{0}.png", p_Parameters.ThemeExtension);
                    staticCartImage = String.Format("cart.png");
                    staticCheckImage = String.Format("Check{0}.png", p_Parameters.ThemeExtension);
                    MenuImageLocation = String.Format("Menu{0}.png", p_Parameters.ThemeExtension);
                    //p_ResourceDictionary.Add("BackGroundImage", String.Format("BackGroundImage{0}.png", p_Parameters.ThemeExtension));
                    p_ResourceDictionary["BackGroundImage"] = String.Format("BackGroundImage{0}.png", p_Parameters.ThemeExtension);

                    PictoMailLocation = String.Format("Mail{0}.png", p_Parameters.ThemeExtension);
                    PictoTelLocation = String.Format("Tel{0}.png", p_Parameters.ThemeExtension);
                    CalendarPictoImageLocation = String.Format("Calendar{0}.png", p_Parameters.ThemeExtension);

                    ForwardImageLocation = String.Format("forward{0}.png", p_Parameters.ThemeExtension);

                    pinImageLocation = String.Format("pin{0}.png", p_Parameters.ThemeExtension);
                    showAllergeneLocation = String.Format("show{0}.png", p_Parameters.ThemeExtension);
                    hideAllergeneLocation = String.Format("hide{0}.png", p_Parameters.ThemeExtension);

                    //Section Godwin
                    ContactPictoImageWidth = 50;
                    ZoneRestaurationInfoPictoLocation = "Information.png";
                    SmallZoneRestaurationInfoPictoLocation = "SmallInformation.png";
                    ZoneRestaurationImageLocation = "PictoEcoleScolaire.png";
                    HorlogeImageLocation = "Horloge.png";
                    EntreImageLocation = String.Format("DefaultSondageImageInspEdu.png", p_Parameters.ThemeExtension);
                    pictoFormuleLocation = String.Format("{0}{1}.png",ThemeParameters.News, p_Parameters.ThemeExtension);
                    MenuPreviousDate = "MenuPrevious.png";
                    MenuNextDate = "MenuNext.png";
                    defaultSondageBackGroundImage = String.Format("DefaultSondageImage{0}.png", p_Parameters.ThemeExtension);
                    defaultSondageBackGroundRadiusImage = "FondArrondi.png";
                    defaultLikeSondageBackGroundImage = String.Format("Like{0}.png", p_Parameters.ThemeExtension);
                    defaultDisLikeSondageBackGroundImage = "Dislike.png";
                    defaultOrSondageBackGroundImage = String.Format("Ou{0}.png", p_Parameters.ThemeExtension);
                    pictoSondageImagePath = String.Format("News{0}.png", p_Parameters.ThemeExtension);

                    timerImageLocation = String.Format("timer{0}.png", p_Parameters.ThemeExtension);
                    panierImageLocation = String.Format("panier{0}.png", p_Parameters.ThemeExtension);
                    LaCarteImageLocation = String.Format("lacarte.png");
                    LaCarteForwardImageLocation = String.Format("lacarteforward.png");
                    alertImageLocation = String.Format("alert{0}.png",p_Parameters.ThemeExtension);
                    
                    PlusRecetteImageLocation = String.Format("PlusCommand{0}.png", p_Parameters.ThemeExtension);
                    InfoRecetteImageLocation = String.Format("InfoRecette.png");
                    Const_RecetteLikeOnImagePath = String.Format("RecetteLikeOn{0}.png", p_Parameters.ThemeExtension);
                    Const_RecetteLikeImagePath = String.Format("RecetteLike{0}.png", p_Parameters.ThemeExtension);
                    Const_DeleteImagePath = "HistoryCancel.png";

                    MesCommandesForwardImageLocation = String.Format("mesCommandeForward.png", p_Parameters.ThemeExtension);
                    CommandImageLocation = String.Format("Command.png");
                    CommandStatusImageLocation = String.Format("CommandStatus.png");
                    HighLineArcticleQtyImagePath = String.Format("PlusCommandHighlight{0}.png", p_Parameters.ThemeExtension);

                    StarImageLocation = String.Format("stars{0}.png", p_Parameters.ThemeExtension);
                    Const_EmptyCart_Path = "Drawing.png";

                    radioButtonCheckedImageLocation = String.Format("optionSelected{0}.png", p_Parameters.ThemeExtension);
                    radioButtonUncheckedImageLocation = String.Format("optionUnselected{0}.png", p_Parameters.ThemeExtension);

                    notifImageLocation = String.Format("Notif{0}.png", p_Parameters.ThemeExtension);
                    break;

                case TargetPlatform.WinPhone:
                    ConnexionScreenImageLocation = String.Format("Assets/splash.png");
                    LogoLocation = String.Format("Assets/Logo{0}.png", p_Parameters.ThemeExtension);
                    AproposLogoLocation = String.Format("Assets/AproposLogo{0}.png", p_Parameters.ThemeExtension);
                    checkBoxOffLocation = String.Format("Assets/CaseNonCoche{0}.png", p_Parameters.ThemeExtension);
                    checkBoxOnLocation = String.Format("Assets/CaseCoche{0}.png", p_Parameters.ThemeExtension);
                    l_NBurgerFooterLocation = String.Format("Assets/{0}{1}.png", ThemeParameters.BurgerFooter, p_Parameters.ThemeExtension);
                    backButtonWidth = 90;
                    creerBarImageLocation = String.Format("Assets/Creer1_Bar_{0}.png", p_Parameters.ThemeExtension);
                    creerBar2ImageLocation = String.Format("Assets/Creer2_Bar_{0}.png", p_Parameters.ThemeExtension);
                    creerBar3ImageLocation = String.Format("Assets/Creer3_Bar_{0}.png", p_Parameters.ThemeExtension);
                    l_burgerIconWidth = 25;
                    backImageLocation = String.Format("Assets/back{0}.png", p_Parameters.ThemeExtension);
                    HeaderImageLocation = String.Format("Assets/Header{0}.png", p_Parameters.ThemeExtension);
                    FooterImageLocation = String.Format("Assets/footer{0}.png", p_Parameters.ThemeExtension);
                    MenuImageLocation = String.Format("Assets/Menu{0}.png", p_Parameters.ThemeExtension);
                    //p_ResourceDictionary.Add("BackGroundImage", String.Format("Assets/BackGroundImage{0}.png", p_Parameters.ThemeExtension));
                    p_ResourceDictionary["BackGroundImage"] = String.Format("Assets/BackGroundImage{0}.png", p_Parameters.ThemeExtension);
                    

                    PictoMailLocation = String.Format("Assets/Mail{0}.png", p_Parameters.ThemeExtension);
                    PictoTelLocation = String.Format("Assets/Tel{0}.png", p_Parameters.ThemeExtension);
                    CalendarPictoImageLocation = String.Format("Assets/Calendar{0}.png", p_Parameters.ThemeExtension);

                    ForwardImageLocation = String.Format("Assets/forward{0}.png", p_Parameters.ThemeExtension);

                    pinImageLocation = String.Format("Assets/pin{0}.png", p_Parameters.ThemeExtension);
                    showAllergeneLocation = String.Format("Assets/show{0}.png", p_Parameters.ThemeExtension);
                    hideAllergeneLocation = String.Format("Assets/hide{0}.png", p_Parameters.ThemeExtension);

                    //Section Godwin
                    ContactPictoImageWidth = 50;
                    ZoneRestaurationInfoPictoLocation = "Assets/Information.png";
                    SmallZoneRestaurationInfoPictoLocation = "Assets/SmallInformation.png";
                    ZoneRestaurationImageLocation = "Assets/PictoEcoleScolaire.png";
                    HorlogeImageLocation = "Assets/Horloge.png";
                    EntreImageLocation = String.Format("Assets/DefaultSondageImageInspEdu.png", p_Parameters.ThemeExtension);
                    pictoFormuleLocation = String.Format("Assets/{0}{1}.png", ThemeParameters.News, p_Parameters.ThemeExtension);
                    MenuPreviousDate = "Assets/MenuPrevious.png";
                    MenuNextDate = "Assets/MenuNext.png";
                    defaultSondageBackGroundImage = String.Format("Assets/DefaultSondageImage{0}.png", p_Parameters.ThemeExtension);
                    defaultSondageBackGroundRadiusImage = "Assets/FondArrondi.png";
                    defaultLikeSondageBackGroundImage = String.Format("Assets/Like{0}.png", p_Parameters.ThemeExtension);
                    defaultDisLikeSondageBackGroundImage = "Assets/Dislike.png";
                    defaultOrSondageBackGroundImage = String.Format("Assets/Ou{0}.png", p_Parameters.ThemeExtension);
                    pictoSondageImagePath = String.Format("Assets/News{0}.png", p_Parameters.ThemeExtension);
                    staticNotificationBackgroundImage = String.Format("Assets/notification{0}.png", p_Parameters.ThemeExtension);
                    staticCartImage = String.Format("Assets/cart.png");
                    staticCheckImage = String.Format("Assets/Check{0}.png", p_Parameters.ThemeExtension);

                    timerImageLocation = String.Format("Assets/timer{0}.png", p_Parameters.ThemeExtension);
                    panierImageLocation = String.Format("Assets/panier{0}.png", p_Parameters.ThemeExtension);
                    LaCarteImageLocation = String.Format("Assets/lacarte.png");
                    LaCarteForwardImageLocation = String.Format("Assets/lacarteforward.png");
                    alertImageLocation = String.Format("Assets/alert{0}.png", p_Parameters.ThemeExtension);

                    PlusRecetteImageLocation = String.Format("Assets/PlusCommand{0}.png", p_Parameters.ThemeExtension);
                    InfoRecetteImageLocation = String.Format("Assets/InfoRecette.png");
                    Const_RecetteLikeOnImagePath = String.Format("Assets/RecetteLikeOn{0}.png", p_Parameters.ThemeExtension);
                    Const_RecetteLikeImagePath = String.Format("Assets/RecetteLike{0}.png", p_Parameters.ThemeExtension);
                    Const_DeleteImagePath = "Assets/HistoryCancel.png";

                    MesCommandesForwardImageLocation = String.Format("Assets/mesCommandeForward.png");
                    CommandImageLocation = String.Format("Assets/Command.png");
                    CommandStatusImageLocation = String.Format("Assets/CommandStatus.png");
                    HighLineArcticleQtyImagePath = String.Format("Assets/PlusCommandHighlight{0}.png", p_Parameters.ThemeExtension);

                    StarImageLocation = String.Format("Assets/stars{0}.png", p_Parameters.ThemeExtension);
                    Const_EmptyCart_Path = "Assets/Drawing.png";

                    radioButtonCheckedImageLocation = String.Format("Assets/optionSelected{0}.png", p_Parameters.ThemeExtension);
                    radioButtonUncheckedImageLocation = String.Format("Assets/optionUnselected{0}.png", p_Parameters.ThemeExtension);
                    notifImageLocation = String.Format("Assets/Notif{0}.png", p_Parameters.ThemeExtension);
                    break;

                case TargetPlatform.iOS:
                    ConnexionScreenImageLocation = String.Format("Default.png");
                    LogoLocation = String.Format("Logo{0}.png", p_Parameters.ThemeExtension);
                    AproposLogoLocation = String.Format("AproposLogo{0}.png", p_Parameters.ThemeExtension);
                    checkBoxOffLocation = String.Format("CaseNonCoche{0}.png", p_Parameters.ThemeExtension);
                    checkBoxOnLocation = String.Format("CaseCoche{0}.png", p_Parameters.ThemeExtension);
                    l_NBurgerFooterLocation = String.Format("{0}{1}.png", ThemeParameters.BurgerFooter, p_Parameters.ThemeExtension);
                    backButtonWidth = 60;
                    creerBarImageLocation = String.Format("Creer1_Bar_{0}.png", p_Parameters.ThemeExtension);
                    creerBar2ImageLocation = String.Format("Creer2_Bar_{0}.png", p_Parameters.ThemeExtension);
                    creerBar3ImageLocation = String.Format("Creer3_Bar_{0}.png", p_Parameters.ThemeExtension);
                    l_burgerIconWidth = 30;
                    backImageLocation = String.Format("back{0}.png", p_Parameters.ThemeExtension);
                    HeaderImageLocation = String.Format("Header{0}.png", p_Parameters.ThemeExtension);
                    FooterImageLocation = String.Format("footer{0}.png", p_Parameters.ThemeExtension);
                    MenuImageLocation = String.Format("Menu{0}.png", p_Parameters.ThemeExtension);
                    //p_ResourceDictionary.Add("BackGroundImage", String.Format("BackGroundImage{0}.png", p_Parameters.ThemeExtension));
                    p_ResourceDictionary["BackGroundImage"] = String.Format("BackGroundImage{0}.png", p_Parameters.ThemeExtension);
                    
                    PictoMailLocation = String.Format("Mail{0}.png", p_Parameters.ThemeExtension);
                    PictoTelLocation = String.Format("Tel{0}.png", p_Parameters.ThemeExtension);
                    CalendarPictoImageLocation = String.Format("Calendar{0}.png", p_Parameters.ThemeExtension);

                    ForwardImageLocation = String.Format("forward{0}.png", p_Parameters.ThemeExtension);

                    pinImageLocation = String.Format("pin{0}.png", p_Parameters.ThemeExtension);
                    showAllergeneLocation = String.Format("show{0}.png", p_Parameters.ThemeExtension);
                    hideAllergeneLocation = String.Format("hide{0}.png", p_Parameters.ThemeExtension);

                    //Section Godwin
                    ContactPictoImageWidth = 50;
                    //footerImageWidth = footerImageWidth * p_Parameters.FooterImageMultiplier;
                    ZoneRestaurationInfoPictoLocation = "Information.png";
                    SmallZoneRestaurationInfoPictoLocation = "SmallInformation.png";
                    ZoneRestaurationImageLocation = "PictoEcoleScolaire.png";
                    HorlogeImageLocation = "Horloge.png";
                    EntreImageLocation = String.Format("DefaultSondageImageInspEdu.png", p_Parameters.ThemeExtension);
                    pictoFormuleLocation = String.Format("{0}{1}.png", ThemeParameters.News, p_Parameters.ThemeExtension);
                    MenuPreviousDate = "MenuPrevious.png";
                    MenuNextDate = "MenuNext.png";
                    staticNotificationBackgroundImage = String.Format("notification{0}.png", p_Parameters.ThemeExtension);
                    defaultSondageBackGroundImage = String.Format("DefaultSondageImage{0}.png", p_Parameters.ThemeExtension);
                    defaultSondageBackGroundRadiusImage = "FondArrondi.png";
                    defaultLikeSondageBackGroundImage = String.Format("Like{0}.png", p_Parameters.ThemeExtension);
                    defaultDisLikeSondageBackGroundImage = "Dislike.png";
                    defaultOrSondageBackGroundImage = String.Format("Ou{0}.png", p_Parameters.ThemeExtension);
                    pictoSondageImagePath = String.Format("News{0}.png", p_Parameters.ThemeExtension);

                    timerImageLocation = String.Format("timer{0}.png", p_Parameters.ThemeExtension);
                    panierImageLocation = String.Format("panier{0}.png", p_Parameters.ThemeExtension);
                    LaCarteImageLocation = String.Format("lacarte.png");
                    LaCarteForwardImageLocation = String.Format("lacarteforward.png");
                    alertImageLocation = String.Format("alert{0}.png", p_Parameters.ThemeExtension);
                    staticCartImage = String.Format("cart.png");
                    staticCheckImage = String.Format("Check{0}.png", p_Parameters.ThemeExtension);

                    PlusRecetteImageLocation = String.Format("PlusCommand{0}.png", p_Parameters.ThemeExtension);
                    InfoRecetteImageLocation = String.Format("InfoRecette.png");
                    Const_RecetteLikeOnImagePath = String.Format("RecetteLikeOn{0}.png", p_Parameters.ThemeExtension);
                    Const_RecetteLikeImagePath = String.Format("RecetteLike{0}.png", p_Parameters.ThemeExtension);
                    Const_DeleteImagePath = "HistoryCancel.png";

                    MesCommandesForwardImageLocation = String.Format("mesCommandeForward.png");
                    CommandImageLocation = String.Format("Command.png");
                    CommandStatusImageLocation = String.Format("CommandStatus.png");
                    HighLineArcticleQtyImagePath = String.Format("PlusCommandHighlight{0}.png", p_Parameters.ThemeExtension);

                    StarImageLocation = String.Format("stars{0}.png", p_Parameters.ThemeExtension);
                    Const_EmptyCart_Path = "Drawing.png";

                    radioButtonCheckedImageLocation = String.Format("optionSelected{0}.png", p_Parameters.ThemeExtension);
                    radioButtonUncheckedImageLocation = String.Format("optionUnselected{0}.png", p_Parameters.ThemeExtension);

                    notifImageLocation = String.Format("Notif{0}.png", p_Parameters.ThemeExtension);
                    break;

                default:
                    break;
            }
            #endregion


            // Création des ressources à base de Key.
            // Ces ressources peuvent être utilisée en tout point de l'UI avec la syntaxe suivante:
            // Property="{DynamicResource NomDeLaKey}"
            #region === Ressources Key ===

            p_ResourceDictionary.Add(BackImage, FileImageSource.FromFile(backImageLocation));
            p_ResourceDictionary.Add(MenuImage, FileImageSource.FromFile(MenuImageLocation));
            p_ResourceDictionary.Add(notifImage, FileImageSource.FromFile(notifImageLocation));

            p_ResourceDictionary.Add(GerantMainColor, p_Parameters.GerantMainColor);
            p_ResourceDictionary.Add(GerantSecondaryColor, p_Parameters.GerantSecondaryColor);
            p_ResourceDictionary.Add(GerantPageBackgroundColor, p_Parameters.GerantPageBackgroundColor);
            p_ResourceDictionary.Add(GerantSecondaryPageBackgroundColor, p_Parameters.GerantSecondaryPageBackgroundColor);

            p_ResourceDictionary.Add(PrimaryTextColor, p_Parameters.PrimaryTextColor);
            p_ResourceDictionary.Add(SecondaryTextColor, p_Parameters.SecondaryTextColor);
            p_ResourceDictionary.Add(TertiaryTextColor, p_Parameters.TertiaryTextColor);

            p_ResourceDictionary.Add(RepeaterColor, p_Parameters.RepeaterColor);
            p_ResourceDictionary.Add(PopinBackgroundColor, p_Parameters.PopinBackgroundColor);


            //**for menuList**//
            //for Date
            p_ResourceDictionary.Add(MENUS_DATEPANEL_BACKCOLOR, p_Parameters.NewMenuViewSwipeOptionBackgroundColor);
            p_ResourceDictionary.Add(MENUS_DATEPANEL_COLOR, Color.White);
            p_ResourceDictionary.Add(MENUS_PREVIOUS_IMAGESOURCE, ImageSource.FromFile(MenuPreviousDate));
            p_ResourceDictionary.Add(CHECKBOX_OFF_IMAGESOURCE, ImageSource.FromFile(checkBoxOffLocation));
            p_ResourceDictionary.Add(CHECKBOX_ON_IMAGESOURCE, ImageSource.FromFile(checkBoxOnLocation));
            p_ResourceDictionary.Add(MENUS_NEXT_IMAGESOURCE, ImageSource.FromFile(MenuNextDate));

            //for MenuNom (When there are more than one meal)
            p_ResourceDictionary.Add(MENUS_MENUPANEL_BACKCOLOR, p_Parameters.l_ComposanteLayoutColor);
            p_ResourceDictionary.Add(MENUS_MENUPANEL_COLOR, p_Parameters.ThemeMaincolor);
            p_ResourceDictionary.Add(MENUS_RECETTENOM_COLOR, p_Parameters.MenuViewRecetteNomColor);
            p_ResourceDictionary.Add(MENUS_LabelComposante_COLOR, p_Parameters.TextColor);

            //For Buttons
            p_ResourceDictionary.Add(BUTTON_FIXED_HEIGHT, (double)Device.OnPlatform(55, 48, 66));
            p_ResourceDictionary.Add(OrderListViewItemHeight, (double)Device.OnPlatform(55, 60, 70));

            #endregion

            // Création des styles par défaut.
            #region === Styles ===

                #region === Labels ===
                //allergen concerne style
                p_ResourceDictionary.Add(AllergeneConcerneStyle, new Style(typeof(Label))
                {
                    Setters=
                    {
                        new Setter{Property = Label.TextColorProperty, Value = Color.Red},
                         new Setter{Property = Label.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Small, typeof(Label))}
                    }
                });

                                
                p_ResourceDictionary.Add(AllergeneNonConcerneStyle, new Style(typeof(Label))
                {
                    Setters=
                    {
                        new Setter{Property = Label.TextColorProperty, Value = p_Parameters.AllergeneTextColor},
                         new Setter{Property = Label.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Small, typeof(Label))}
                    }
                });

                //NewMenuViewDateListViewTextColor
                p_ResourceDictionary.Add(NewMenuViewDateListViewTextColorStyle, new Style(typeof(Label))
                {
                    Setters =
                            {
                                    new Setter{Property = Label.TextColorProperty, Value = p_Parameters.NewMenuViewDateListViewTextColor}
                            }
                });

                //NewMenuViewDateListViewTextColor
                p_ResourceDictionary.Add(NewMenuViewDateListViewStatusTextColorStyle, new Style(typeof(Label))
                {
                    Setters =
                            {
                                    new Setter{Property = Label.TextColorProperty, Value = p_Parameters.ThemeMaincolor}
                            }
                });

                p_ResourceDictionary.Add(CONST_RECETTEDETAILHEADERTITLECOLOR, new Style(typeof(Label))
                {
                    Setters =
                            {
                                    new Setter{Property = Label.TextColorProperty, Value = p_Parameters.RecetteDetailHeaderTitleColor}
                            }
                });  
                
                //under badgeView badgeStack2LabelColor
                p_ResourceDictionary.Add(BadgeStack2LabelStyle, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.TextColorProperty, Value = p_Parameters.BadgeAmountColor},
                                new Setter{Property = Label.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Small, typeof(Label))},
                                new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                                new Setter{Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                                new Setter{Property = Label.HorizontalTextAlignmentProperty, Value =TextAlignment.Center},
                                new Setter{Property = Label.VerticalTextAlignmentProperty, Value =TextAlignment.Center}
                        }
                });

                p_ResourceDictionary.Add(BadgeStack3LabelStyle, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.TextColorProperty, Value = p_Parameters.BadgeAmountColor},
                        }
                });


                //under badgeView Divider
                p_ResourceDictionary.Add(BadgeDividerStyle, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.BadgeDividerColor}
                        }
                });

                p_ResourceDictionary.Add(ActivityIndicatorMessageStyle, new Style(typeof(Label))
                {
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = p_Parameters.ThemeMaincolor},
                            new Setter{Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.Center},
                            new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.Center}
                        }
                });

                p_ResourceDictionary.Add(HeaderBarLabel, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.HeaderLabelBackgroundColor},
                                new Setter{Property = Label.OpacityProperty, Value = p_Parameters.HeaderLabelOpacity},
                                new Setter{Property = Label.TextProperty, Value = p_Parameters.ConnexionText},
                                new Setter{Property = Label.TextColorProperty, Value = p_Parameters.HeaderBarLabelTextColor},
                                new Setter{Property = Label.HorizontalTextAlignmentProperty, Value = TextAlignment.Center},
                                new Setter{Property = Label.VerticalTextAlignmentProperty, Value = TextAlignment.Center},
                                new Setter{Property = Label.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Medium, typeof(Label))},
                                new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand}
                                //new Setter{Property = Label.HeightRequestProperty, Value = HeaderBarHeight}
                        }
                });

			    p_ResourceDictionary.Add(HamBurgerHeaderBarLabel, new Style(typeof(Label))
			    {
				    BaseResourceKey = HeaderBarLabel,
				    Setters =
					    {
						    new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.HamburgerBackGroundColor}
					    }
			    });

                p_ResourceDictionary.Add(NewsTitleStyle, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.TextColorProperty, Value = p_Parameters.NewsTitleTextColor},
                                new Setter{Property = Label.OpacityProperty, Value = p_Parameters.HeaderLabelOpacity},
                                new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand}
                        }
                });

                //under navigationBars
                p_ResourceDictionary.Add(HeaderUnderBarLabel, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.ThemeMaincolor},
                                new Setter{Property = Label.HeightRequestProperty, Value = 2}
                        }
                });

                p_ResourceDictionary.Add(NewsUnderImageBar, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.NewsUnderImageBarColor},
                                new Setter{Property = Label.HeightRequestProperty, Value = 3}
                        }
                });

                //jusst for news Picto Carousel
                p_ResourceDictionary.Add(PictoUnderBarLabel, new Style(typeof(Label))
                {
                    BaseResourceKey = HeaderUnderBarLabel,
                    Setters =
                        {
                                new Setter{Property = Label.HeightRequestProperty, Value = 2},
                                new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.Start}
                        }
                });

                //just for news Picto Carousel
                p_ResourceDictionary.Add(CarrouselDividerLabel, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.CarrouselDividerColor},
                                new Setter{Property = Label.HeightRequestProperty, Value = 2},
                                new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.Start}
                        }
                });

                //jusst for news Picto Carousel
                p_ResourceDictionary.Add(UnderBarLine, new Style(typeof(Label))
                {                    
                    Setters =
                        {
                                new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.UnderBarLineColor},
                                new Setter{Property = Label.HeightRequestProperty, Value = 1},
                                new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.Start}
                        }
                });




                //Just for ZR underline
                p_ResourceDictionary.Add(UnderZRline, new Style(typeof(Label))
                {
                    Setters =
                        {
                            new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.UnderLineZrColor},
                            new Setter{Property = Label.HeightRequestProperty, Value = 1},
                            new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.Start}
                        }
                });

                //Just for ZRDetail underline
                p_ResourceDictionary.Add(UnderZRDetailLine, new Style(typeof(Label))
                {
                    Setters =
                        {
                            new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.ThemeMaincolor},
                            new Setter{Property = Label.HeightRequestProperty, Value = 1},
                            new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.Start}
                        }
                });

                //Label used for List in AcueilView
                p_ResourceDictionary.Add(ListViewBarLabel, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.ThemeMaincolor},
                                new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                                new Setter{Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.Fill},
                                new Setter{Property = Label.WidthRequestProperty, Value = 5}
                        }
                });

                //Label used as divider in parametres 1st option Block
                p_ResourceDictionary.Add(M_infoDividerLabel, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.ParameterDividerLabelColor},
                                new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                                new Setter{Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.Fill},
                                new Setter{Property = Label.HeightRequestProperty, Value = 1}
                        }
                });

                //Label used as divider in updateSiteCaseEdu for rimeh
                p_ResourceDictionary.Add(N_infoDividerLabel, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.ParameterDividerLabelColor},
                                new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                                new Setter{Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.Fill},
                                new Setter{Property = Label.HeightRequestProperty, Value = 1}
                        }
                });

                //
                p_ResourceDictionary.Add(CreationPageUnderBarLabel, new Style(typeof(Label))
                {
                    BaseResourceKey = HeaderUnderBarLabel,
                    Setters =
                        {
                                new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.CreationPageUnderBarLabel},
                                new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.End},
                                //new Setter{Property = Label.HorizontalTextAlignmentProperty , Value = LayoutOptions.Center}
                        }
                });

                p_ResourceDictionary.Add(CreationPageUnderBarLabelBottom, new Style(typeof(Label))
                {
                    BaseResourceKey = HeaderUnderBarLabel,
                    Setters =
                        {
                                new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.CreationPageUnderBarLabelBottom},
                                new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.End},
                                //new Setter{Property = Label.HorizontalTextAlignmentProperty , Value = LayoutOptions.Center}
                        }
                });

                //Hyperlink
                p_ResourceDictionary.Add(Hyperlinkstyle, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.TextColor}
                        }
                });

                //label style
                p_ResourceDictionary.Add(TextStyle, new Style(typeof(Label))
                {
                    BaseResourceKey = "SubtitleStyle",
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = p_Parameters.TextColor},
                        }
                });

                //for Labels that use ThemeMain Colours ONly
                p_ResourceDictionary.Add(ThemedTextStyle, new Style(typeof(Label))
                {
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = p_Parameters.ThemeMaincolor}
                        }
                });

                p_ResourceDictionary.Add(AllergeneTextStyle, new Style(typeof(Label))
                {
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = p_Parameters.AllergeneTextColor}
                        }
                });

                //for parametreView 
                p_ResourceDictionary.Add(dTextStyle, new Style(typeof(Label))
                {
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = p_Parameters.TextColor}
                        }
                });

                //for ServiceView ONly (HeaderText near Info)
                p_ResourceDictionary.Add("yacinestyle", new Style(typeof(Label))
                {
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = p_Parameters.ServiceViewHeaderTextColor}
                        }

                });

                //for ServiceView ONly (Content and List Header on calendarImage Row)PrimaryHeaderTextStyle
                p_ResourceDictionary.Add(PrimaryTextStyle, new Style(typeof(Label))
                {
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = p_Parameters.ServiceViewPrimaryColor}
                        }

                });

                //for ServiceView ONly (Amount)
                p_ResourceDictionary.Add(SeconadryTextStyle, new Style(typeof(Label))
                {
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = p_Parameters.ServiceViewSecondaryColor}
                        }

                });

                //label style for HomeView List template
                p_ResourceDictionary.Add(ListTextStyle, new Style(typeof(Label))
                {
                    BaseResourceKey = "ListItemDetailTextStyle",
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = p_Parameters.ListViewTextColor},
                            new Setter{Property = Label.VerticalOptionsProperty , Value = LayoutOptions.CenterAndExpand},
                            new Setter{Property = Label.HorizontalTextAlignmentProperty , Value = LayoutAlignment.Center},
                            new Setter{Property = Label.VerticalTextAlignmentProperty , Value = LayoutAlignment.Start},
                            new Setter{Property = Label.FontSizeProperty , Value = Device.GetNamedSize(NamedSize.Small, typeof(Label))}
                        }
                });

                //label style for Creation1 List template
                p_ResourceDictionary.Add(CListTextStyle, new Style(typeof(Label))
                {
                    BaseResourceKey = "ListItemDetailTextStyle",
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = p_Parameters.TextColor},
                            new Setter{Property = Label.VerticalOptionsProperty , Value = LayoutOptions.CenterAndExpand},
                            new Setter{Property = Label.HorizontalTextAlignmentProperty , Value = LayoutAlignment.Center},
                            new Setter{Property = Label.VerticalTextAlignmentProperty , Value = LayoutAlignment.Start},
                            new Setter{Property = Label.FontSizeProperty , Value = Device.GetNamedSize(NamedSize.Small, typeof(Label))}
                        }
                });

                //hamburgerListview
                p_ResourceDictionary.Add(HamburgerListViewAndLabelStyle, new Style(typeof(Label))
                {
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = p_Parameters.HamburgerListViewLabelAndLabelColor}
                        }
                });

                p_ResourceDictionary.Add(CreationLabelStyle, new Style(typeof(Label))
                {
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = p_Parameters.TextColor}
                        }
                });

                //Labels in EnfantListView
                p_ResourceDictionary.Add(NewBodyStyle, new Style(typeof(Label))
                {
                    BaseResourceKey = "BodyStyle",
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = p_Parameters.TextColor},
                        }
                });

                //validation Label Style
                p_ResourceDictionary.Add(ValidationTextStyle, new Style(typeof(Label))
                {
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = Color.Red},
                            new Setter{Property = Label.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Micro ,typeof(Label))},
                            new Setter{Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.EndAndExpand},
                            new Setter{Property = Label.BackgroundColorProperty, Value = Color.Transparent},
                            new Setter{Property = Label.VerticalTextAlignmentProperty, Value = TextAlignment.End}
                        }
                });

                //Coloured Label for design Purposes
                p_ResourceDictionary.Add(ThemedLabelStyle, new Style(typeof(Label))
                {
                    Setters =
                            {
                                new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.ThemeMaincolor},
                                new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand}
                            }
                });

                //
                p_ResourceDictionary.Add(NewMenuViewButtomNotificationTextStyle, new Style(typeof(Label))
                {
                    Setters = 
                        {
                            new Setter{Property = Label.TextColorProperty , Value = p_Parameters.NewMenuViewButtomNotificationTextColor}
                        }
                });

                // For notification bullet
                p_ResourceDictionary.Add(OrderNotificationLabelTextColor, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.TextColorProperty, Value = p_Parameters.OrderNotificationTextColor}
                        }
                });

                p_ResourceDictionary.Add(OrderNotificationDateTextColor, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.TextColorProperty, Value = p_Parameters.OrderNotificationDateTextColor}
                        }
                });

                p_ResourceDictionary.Add(CONST_ORDERPROCESSITEMHEADERTEXTCOLOR, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.TextColorProperty, Value = p_Parameters.OrderProcessItemHeaderTextColor}
                        }
                });

                p_ResourceDictionary.Add(CONST_ORDERPROCESSITEMPLUSTEXTCOLOR, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.TextColorProperty, Value = p_Parameters.OrderProcessItemPlusTextColor}
                        }
                });

                p_ResourceDictionary.Add(CONST_ORDERPROCESSPOPUPHEADERLABELTEXTCOLOR, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.TextColorProperty, Value = p_Parameters.OrderProcessPopUpHeaderTitleTextColor}
                        }
                });

                p_ResourceDictionary.Add(OrderProcesMainbuttonStyle, new Style(typeof(Label))
                {
                    Setters =
                        {
                            new Setter{Property = Label.TextColorProperty, Value = p_Parameters.OrderProcessButtonTextColor},
                        }
                });

                p_ResourceDictionary.Add(CONST_ORDERPROCESSLABELTEXTCOLOR, new Style(typeof(Label))
                {
                    Setters =
                        {
                                new Setter{Property = Label.TextColorProperty, Value = p_Parameters.OrderProcessLabelTextColor},
                                //new Setter{Property = Label.LineBreakModeProperty, Value = LineBreakMode.WordWrap}
                        }
                });
            
                #endregion

                #region === Buttons ===
                //
                p_ResourceDictionary.Add(BadgeAmountButtonStyle, new Style(typeof(Sodexo.Controls.RoundedButton))
                {
                    Setters =
                         {
                               new Setter{Property = Sodexo.Controls.RoundedButton.BackgroundColorProperty, Value = Color.Transparent},
                               new Setter{Property = Sodexo.Controls.RoundedButton.TextColorProperty, Value = p_Parameters.BadgeTextColor},
                               new Setter{Property = Sodexo.Controls.RoundedButton.BorderColorProperty, Value = p_Parameters.BadgeBorderColor},
                               new Setter{Property = Sodexo.Controls.RoundedButton.BorderWidthProperty, Value = 1},
                               new Setter{Property = Sodexo.Controls.RoundedButton.BorderRadiusProperty, Value = 1.13},
                               new Setter{Property = Sodexo.Controls.RoundedButton.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Medium, typeof(Button))},
                               new Setter{Property = Sodexo.Controls.RoundedButton.HeightRequestProperty, Value = Device.OnPlatform(45,55,80)},
                               new Setter{Property = Sodexo.Controls.RoundedButton.VerticalOptionsProperty, Value = LayoutOptions.StartAndExpand},
                               new Setter{Property = Sodexo.Controls.RoundedButton.HorizontalOptionsProperty, Value = LayoutOptions.Fill}
                         }
                });

                //for the selected BadgeAmount Button
                p_ResourceDictionary.Add(ThemedSelectedAmountStyle, new Style(typeof(Sodexo.Controls.RoundedButton))
                {
                    Setters =
                         {
                               new Setter{Property = Sodexo.Controls.RoundedButton.BackgroundColorProperty, Value = p_Parameters.BadgeSelectedBackgroundColor},
                               new Setter{Property = Sodexo.Controls.RoundedButton.TextColorProperty, Value = p_Parameters.BadgeSelectedAmountColor},
                               new Setter{Property = Sodexo.Controls.RoundedButton.BorderColorProperty, Value = Color.White},
                               new Setter{Property = Sodexo.Controls.RoundedButton.BorderWidthProperty, Value = 1},
                               new Setter{Property = Sodexo.Controls.RoundedButton.BorderRadiusProperty, Value = 1.13},
                               new Setter{Property = Sodexo.Controls.RoundedButton.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Medium, typeof(Label))},
                               new Setter{Property = Sodexo.Controls.RoundedButton.HeightRequestProperty, Value = Device.OnPlatform(45,55,80)},
                               new Setter{Property = Sodexo.Controls.RoundedButton.VerticalOptionsProperty, Value = LayoutOptions.StartAndExpand},
                               new Setter{Property = Sodexo.Controls.RoundedButton.HorizontalOptionsProperty, Value = LayoutOptions.Fill}
                         }
                });

                //for the Allergene Button
                p_ResourceDictionary.Add(MENUS_ALLERGENE_STYLE, new Style(typeof(Sodexo.Controls.RoundedButton))
                {
                    Setters =
                         {
                               new Setter{Property = Sodexo.Controls.RoundedButton.BackgroundColorProperty, Value = Color.Transparent},
                               new Setter{Property = Sodexo.Controls.RoundedButton.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                               new Setter{Property = Sodexo.Controls.RoundedButton.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                               new Setter{Property = Sodexo.Controls.RoundedButton.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Small, typeof(Button))},
                               new Setter{Property = Sodexo.Controls.RoundedButton.TextColorProperty, Value = p_Parameters.TextColor},
                               new Setter{Property = Sodexo.Controls.RoundedButton.BorderColorProperty, Value = Color.FromHex("#cccccc")},
                               new Setter{Property = Sodexo.Controls.RoundedButton.ImageProperty, Value = (FileImageSource)ImageSource.FromFile(showAllergeneLocation)},
                               //new Setter{Property = Sodexo.Controls.RoundedButton.HeightRequestProperty, Value = 90},
                               new Setter{Property = Sodexo.Controls.RoundedButton.BorderWidthProperty, Value = 1},
                               new Setter{Property = Sodexo.Controls.RoundedButton.BorderRadiusProperty, Value = 1.13},
                         }
                });

                //for the Allergene Button
                p_ResourceDictionary.Add(MENUS_ALLERGENEHIDE_STYLE, new Style(typeof(Sodexo.Controls.RoundedButton))
                {
                    Setters =
                         {
                               new Setter{Property = Sodexo.Controls.RoundedButton.BackgroundColorProperty, Value = Color.Transparent},
                               new Setter{Property = Sodexo.Controls.RoundedButton.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                               new Setter{Property = Sodexo.Controls.RoundedButton.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                               new Setter{Property = Sodexo.Controls.RoundedButton.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Small, typeof(Button))},
                               new Setter{Property = Sodexo.Controls.RoundedButton.TextColorProperty, Value = p_Parameters.TextColor},
                               new Setter{Property = Sodexo.Controls.RoundedButton.BorderColorProperty, Value = Color.FromHex("#cccccc")},
                               new Setter{Property = Sodexo.Controls.RoundedButton.ImageProperty, Value = (FileImageSource)ImageSource.FromFile(hideAllergeneLocation)},
                               //new Setter{Property = Sodexo.Controls.RoundedButton.HeightRequestProperty, Value = 90},
                               new Setter{Property = Sodexo.Controls.RoundedButton.BorderWidthProperty, Value = 1},
                               new Setter{Property = Sodexo.Controls.RoundedButton.BorderRadiusProperty, Value = 1.13},
                         }
                });

                //creer mon compte
                p_ResourceDictionary.Add(MainbuttonStyle, new Style(typeof(Button))
                {
                    Setters =
                         {
                               new Setter{Property = Button.BackgroundColorProperty, Value = p_Parameters.MainButtonBackgroundColor},
                               new Setter{Property = Button.TextColorProperty, Value = Color.White},
                               new Setter{Property = Button.BorderWidthProperty, Value = 0},
                               new Setter{Property = Button.BorderColorProperty, Value = Color.Transparent},
                               new Setter{Property = Button.BorderRadiusProperty, Value = 0},
                               new Setter{Property = Button.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Medium, typeof(Button))},
                               new Setter{Property = Button.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                               new Setter{Property = Button.HorizontalOptionsProperty, Value = LayoutOptions.Fill}
                         }
                });

                p_ResourceDictionary.Add(CONST_CARTBUTTONSTYLE, new Style(typeof(Button))
                {
                    BaseResourceKey = MainbuttonStyle,
                    Setters =
                         {
                               new Setter{Property = Button.BackgroundColorProperty, Value = p_Parameters.CartButtonBackColor},
                               new Setter{Property = Button.TextColorProperty, Value = Color.White},
                               new Setter{Property = Button.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand}
                         }
                });
                
                //me connecter copy to be used by Label
                p_ResourceDictionary.Add("SecMainbuttonLabelStyle", new Style(typeof(Label))
                {
                    BaseResourceKey = MainbuttonStyle,
                    Setters =
                         {
                               new Setter{Property = Label.BackgroundColorProperty, Value = p_Parameters.ThemeMaincolor},
                               new Setter{Property = Label.TextColorProperty, Value = Color.White},
                               new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand}
                         }
                });

                //mot de passe oublie
                p_ResourceDictionary.Add(SecbuttonStyle, new Style(typeof(Button))
                {
                    BaseResourceKey = MainbuttonStyle,
                    Setters =
                            {
                                new Setter{Property = Button.BackgroundColorProperty, Value = p_Parameters.HomePageFormBackgroundColor},
                                new Setter{Property = Button.OpacityProperty, Value = p_Parameters.StackOpacity},
                                new Setter{Property = Button.TextColorProperty, Value = p_Parameters.TextColor},
                                new Setter{Property = Button.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                                new Setter{Property = Button.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Small, typeof(Button))}
                            }
                });

                //recherche autor de moi 
                p_ResourceDictionary.Add(TertiarybuttonStyle, new Style(typeof(Button))
                {
                    BaseResourceKey = MainbuttonStyle,
                    Setters =
                            {
                                new Setter{Property = Button.TextColorProperty, Value = p_Parameters.TetiaryButtonTextColor},
                                new Setter{Property = Button.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Small, typeof(Button))}
                            }
                });


                //backbutton
                p_ResourceDictionary.Add(BackButtonImageStyle, new Style(typeof(Button))
                {
                    Setters =
                        {
                            new Setter{Property = Button.WidthRequestProperty, Value = backButtonWidth},
                            new Setter{Property = Button.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                            new Setter{Property = Button.VerticalOptionsProperty, Value =  LayoutOptions.FillAndExpand},
                            new Setter{Property = Button.BackgroundColorProperty, Value =  Color.Transparent},
                            new Setter{Property = Button.BorderColorProperty, Value = Color.Transparent}
                            //Proper way to be used 
                            //new Setter{Property = Button.ImageProperty, Value = FileImageSource.FromFile("Assets/backInsp.png")}
                            //workaround but with navigation issues
                            //new Setter{Property = Button.ImageProperty, Value = backImageLocation }
                        }
                });

                //backbutton
                p_ResourceDictionary.Add(MenuButtonImageStyle, new Style(typeof(Button))
                {
                    BaseResourceKey = BackButtonImageStyle
                });

                //Nitification button
                p_ResourceDictionary.Add(notifButtonImageStyle, new Style(typeof(Button))
                {
                    BaseResourceKey = BackButtonImageStyle
                });



                //overlay Cancel Button Style
                p_ResourceDictionary.Add(ThemedRoundedButtonStyle, new Style(typeof(Sodexo.Controls.RoundedButton))
                {
                    Setters =
                            {
                                new Setter{Property = Label.TextColorProperty, Value = p_Parameters.ThemeMaincolor},
                            }
                });

                //badge button on HomeView
                p_ResourceDictionary.Add(BadgeButtonStyle, new Style(typeof(Button))
                {
                    Setters =
                            {
                                new Setter{Property = Button.BackgroundColorProperty, Value = Color.White},
                                new Setter{Property = Button.BorderWidthProperty, Value = 0},
                                new Setter{Property = Button.BorderColorProperty, Value = Color.Transparent},
                                new Setter{Property = Button.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                                new Setter{Property = Button.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand}
                            }
                });

            #endregion

                #region === Images ===
                p_ResourceDictionary.Add(CONST_REMOVECARD_STYLE, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromResource(Const_RemoveCard_Path)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                            new Setter{Property = Image.HeightRequestProperty, Value = 20},
                            new Setter{Property = Image.WidthRequestProperty, Value = 20},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                            new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand}
                        }
                });




                p_ResourceDictionary.Add(CONST_EMPTYCART_STYLE, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(Const_EmptyCart_Path)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                            new Setter{Property = Image.HeightRequestProperty, Value = 60},
                            new Setter{Property = Image.WidthRequestProperty, Value = 60},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                            new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand}
                        }
                });

                p_ResourceDictionary.Add(HighLineArcticleQtyImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(HighLineArcticleQtyImagePath)},
                        }
                });

                p_ResourceDictionary.Add(CommandImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(CommandImageLocation)},
                        }
                });

                p_ResourceDictionary.Add(CommandStatusImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(CommandStatusImageLocation)},
                        }
                });

                //for "next" on parametresView
                p_ResourceDictionary.Add(CONST_RECETTEDETAILLIKEONSTYLE, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(Const_RecetteLikeOnImagePath)},
                        }
                });

                p_ResourceDictionary.Add(CONST_DeleteImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(Const_DeleteImagePath)}
                        }
                });

                p_ResourceDictionary.Add(CONST_RECETTEDETAILLIKESTYLE, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(Const_RecetteLikeImagePath)},
                        }
                });

                p_ResourceDictionary.Add(ConnexionScreenImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(ConnexionScreenImageLocation)}
                        }
                });
                p_ResourceDictionary.Add(ForwardImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(ForwardImageLocation)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                            new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                            new Setter{Property = Image.HeightRequestProperty, Value = 30}
                        }
                });

                p_ResourceDictionary.Add(radioButtonCheckedImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(radioButtonCheckedImageLocation)},
                        }
                });

                p_ResourceDictionary.Add(radioButtonUncheckedImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(radioButtonUncheckedImageLocation)},
                        }
                });

                //for "la Carte" on menuorderView
                p_ResourceDictionary.Add(LacarteImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey= ForwardImageStyle,
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(LaCarteImageLocation)},
                        }
                });

                //for "next" on menuorderView
                p_ResourceDictionary.Add(LaCarteForwardImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey= ForwardImageStyle,
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(LaCarteForwardImageLocation)},
                        }
                });

                //for "next" on mes commandes
                p_ResourceDictionary.Add(MesCommandesForwardImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey = ForwardImageStyle,
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(MesCommandesForwardImageLocation)},
                        }
                });

                //for "alert" on menuorderView
                p_ResourceDictionary.Add(AlertImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey= ForwardImageStyle,
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(alertImageLocation)},
                        }
                });

                //for "panier" on menuorderView ListView
                p_ResourceDictionary.Add(PanierImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey = ForwardImageStyle,
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(panierImageLocation)},
                        }
                });

                p_ResourceDictionary.Add(StarImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(StarImageLocation)},
                        }
                });

                //for "+Command" 
                p_ResourceDictionary.Add(PlusRecetteImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey = ForwardImageStyle,
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(PlusRecetteImageLocation)},
                        }
                });

                //for "imageInfoCommand" 
                p_ResourceDictionary.Add(InfoRecetteImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey = ForwardImageStyle,
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(InfoRecetteImageLocation)},
                        }
                });

                p_ResourceDictionary.Add(NotificationImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey = ForwardImageStyle,
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(staticNotificationBackgroundImage)}
                        }
                });

                p_ResourceDictionary.Add(CartImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey = ForwardImageStyle,
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(staticCartImage)},
                        }
                });

                p_ResourceDictionary.Add(CheckImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey = ForwardImageStyle,
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(staticCheckImage)},
                        }
                });


                //for "timer" on menuorderView ListView
                p_ResourceDictionary.Add(TimerImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey = ForwardImageStyle,
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(timerImageLocation)},
                        }
                });


                //LogoLocation comme inspiration pour le lancement
                p_ResourceDictionary.Add(LogoImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(LogoLocation)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.Fill}
                            //new Setter{Property = Image.HeightRequestProperty, Value=200}
                        }
                });

                p_ResourceDictionary.Add(AproposLogoImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(AproposLogoLocation)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.Fill}
                        }
                });

                //SideImage pour les Home View
                p_ResourceDictionary.Add(HomeSideImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            //new Setter{Property = Image.WidthRequestProperty, Value = 40},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand}
                            //new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.StartAndExpand}
                        }
                });

                //Mail Picto
                p_ResourceDictionary.Add(MailPictoStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(PictoMailLocation)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand}
                            //new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.StartAndExpand}
                        }
                });

                //Tel Picto
                p_ResourceDictionary.Add(TelPictoStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(PictoTelLocation)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand}
                            //new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.StartAndExpand}
                        }
                });

                // Sondage Background Image link
                p_ResourceDictionary.Add(SondageImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(defaultSondageBackGroundImage)}
                        }
                });

                // Image border radius for Sondage.
                p_ResourceDictionary.Add(SondageQuestionBackgroundImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(defaultSondageBackGroundRadiusImage)}
                        }
                });

                p_ResourceDictionary.Add(LikeSondageImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(defaultLikeSondageBackGroundImage)}
                        }
                });

                p_ResourceDictionary.Add(DisLikeSondageImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(defaultDisLikeSondageBackGroundImage)}
                        }
                });

                p_ResourceDictionary.Add(OrSondageImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(defaultOrSondageBackGroundImage)}
                        }
                });

                p_ResourceDictionary.Add(PictoSondageImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(pictoSondageImagePath)}
                        }
                });


                p_ResourceDictionary.Add(BurgerImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit}
                        }
                });

                //SideImage pour les contact View
                p_ResourceDictionary.Add(ContactPictoStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.WidthRequestProperty, Value = ContactPictoImageWidth},
                        }
                });

                //CreerBar ImageLocation  pour  1ere page -creation de compte
                p_ResourceDictionary.Add(CreerBar1ImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(creerBarImageLocation)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                            new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.Center},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.Center}
                        }
                });

                //CreerBar ImageLocation2  pour  2eme page -creation de compte
                p_ResourceDictionary.Add(CreerBar2ImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey = CreerBar1ImageStyle,
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(creerBar2ImageLocation)}
                        }
                });

                //CreerBar ImageLocation3  pour  3eme page -creation de compte
                p_ResourceDictionary.Add(CreerBar3ImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey = CreerBar1ImageStyle,
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(creerBar3ImageLocation)}
                        }
                });

                //Picto pour le formule
                p_ResourceDictionary.Add(PictoFormuleImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(pictoFormuleLocation)},
                            //new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                            //new Setter{Property = Image.HeightRequestProperty, Value = 60},
                            //new Setter{Property = Image.WidthRequestProperty, Value = 60},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                            new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand}
                        }
                });


                //Picto pour le 00000000
                p_ResourceDictionary.Add(ZoneRestaurationImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey = InstitutuionImageStyle,
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(ZoneRestaurationImageLocation)},
                        }
                });

                //Picto pour le Large Info picto 
                p_ResourceDictionary.Add(ZoneRestaurationInfosPictoStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(ZoneRestaurationInfoPictoLocation)},
                        }
                });

                //Picto pour le small Info picto
                p_ResourceDictionary.Add(SmallZoneRestaurationInfosPictoStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(SmallZoneRestaurationInfoPictoLocation)},
                        }
                });

                //Calendar picto for ServiceView
                p_ResourceDictionary.Add(CalendarPictoStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(CalendarPictoImageLocation)}
                        }
                });

                //horloge Style
                p_ResourceDictionary.Add(HorlogeImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(HorlogeImageLocation)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFill},
                        }
                });

                p_ResourceDictionary.Add(EntreeImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(EntreImageLocation)}
                        }
                });

                p_ResourceDictionary.Add(WPFooterImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(FooterImageLocation)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFill},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                            new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand}
                        }
                });

                //Android Footer
                p_ResourceDictionary.Add(AndroidFooterImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(FooterImageLocation)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFill},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                            new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand}
                        }
                });

                //iOS Footer
                p_ResourceDictionary.Add(iOSFooterImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(FooterImageLocation)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFill},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                            new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand}
                        }
                });

                //PinImage on entreprise search
                p_ResourceDictionary.Add(PinImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(pinImageLocation)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                            new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand}
                        }
                });


                //Hamburger Footer Style
                p_ResourceDictionary.Add(HamburgerFooterImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            /*new Setter{Property = Image.SourceProperty, Value = ImageSource.FromResource(BurgerFooterLocation)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                            new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.EndAndExpand},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand}*/
                            //new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand}
                            //new Setter{Property = Image.HeightRequestProperty, Value=200}
                            new Setter{Property = Image.SourceProperty, Value = FileImageSource.FromFile(l_NBurgerFooterLocation)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.Fill},
                            new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.EndAndExpand}
                        }
                });

                // HomeBar Image style.
                p_ResourceDictionary.Add(HomeBarImageStyle, new Style(typeof(Image))
                {
                    Setters =
                        {
                            new Setter{Property = Image.BackgroundColorProperty, Value = Color.Transparent},
                            new Setter{Property = Image.SourceProperty, Value = ImageSource.FromFile(HeaderImageLocation)},
                            new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                            new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                            new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand}
                        }
                });

                #endregion

                #region === StackLayouts ===

                p_ResourceDictionary.Add(CONST_STACK_BACKGROUNDCOLOR_TRANSPARENT, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = new Color(0,0,0,0.7)}
                        }
                });

                p_ResourceDictionary.Add(CONST_RECETTEDETAILSECTIONSTACKBACKGROUNDCOLOR, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.RecetteDetailSectionStackBackGroundColor}
                        }
                });

                p_ResourceDictionary.Add(CONST_RECETTEDETAILTOPANDBOTTOMBARLABEL, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.HeightRequestProperty, Value = 2},
                                new Setter{Property = StackLayout.VerticalOptionsProperty, Value = LayoutOptions.Start},
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.RecetteDetailUnderBarColor}
                        }
                });

                p_ResourceDictionary.Add(CONST_RECETTEDETAILTITLESTACKBACKGROUNDCOLOR, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.RecetteDetailTitleStackBackGroundColor}
                        }
                });

                //used to give the overlay look and feel 
                p_ResourceDictionary.Add(StackStyle, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.OtherPageFormBackgroundColor},
                                new Setter{Property = StackLayout.OpacityProperty, Value = p_Parameters.StackOpacity}
                        }
                });

                p_ResourceDictionary.Add(CONST_CARTSTACKBUTTONBACKCOLOR, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.CartButtonBackColor}
                        }
                });

                p_ResourceDictionary.Add(SpecialWPStack2, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.MainButtonBackgroundColor}
                        }
                });

                //for ServiceView Coloured Header with info Icon
                p_ResourceDictionary.Add(CustStackStyle, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.ServiceViewHeaderColor},
                                new Setter{Property = StackLayout.OpacityProperty, Value = p_Parameters.ServiceViewStackOpacity}
                        }
                });

                //for MenuView Coloured Header with info Icon
                p_ResourceDictionary.Add(Cust2StackStyle, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.MenuViewMainHeaderColor},
                                new Setter{Property = StackLayout.OpacityProperty, Value = p_Parameters.MenuViewMainHeaderOpacity}
                        }
                });

                //Hamburger Background
                p_ResourceDictionary.Add(HamburgerStyle, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.HamburgerBackGroundColor}
                        }
                });

                //for badgeView (solde details and mise a jour date)
                p_ResourceDictionary.Add(PrimaryBadgeStackStyle, new Style(typeof(StackLayout))
                {
                    Setters = 
                        {
                            new Setter{Property = StackLayout.BackgroundColorProperty , Value = p_Parameters.BadgeStack1Color}
                        }
                });

                //for badgeView ( badge amounts background)
                p_ResourceDictionary.Add(SecondaryBadgeStackStyle, new Style(typeof(StackLayout))
                {
                    Setters = 
                        {
                            new Setter{Property = StackLayout.BackgroundColorProperty , Value = p_Parameters.BadgeStack2Color}
                        }
                });

                //for ConnexionView Logo
                p_ResourceDictionary.Add(LogoStackStyle, new Style(typeof(StackLayout))
                {
                    Setters = 
                        {
                            new Setter{Property = StackLayout.BackgroundColorProperty , Value = p_Parameters.HomePageLogoBackgroundColor}
                        }
                });

                //for ConnexionView Form
                p_ResourceDictionary.Add(FormStackStyle, new Style(typeof(StackLayout))
                {
                    Setters = 
                        {
                            new Setter{Property = StackLayout.BackgroundColorProperty , Value = p_Parameters.HomePageFormBackgroundColor},
                            new Setter{Property = StackLayout.OpacityProperty , Value = p_Parameters.StackOpacity}
                        }
                });

                //for MenuOrderItemView Coloured Header with info Icon
                p_ResourceDictionary.Add(NewMenuViewMainHeaderStyle, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.NewMenuViewMainHeaderColor},
                                new Setter{Property = StackLayout.OpacityProperty, Value = p_Parameters.NewMenuViewMainHeaderOpacity}
                        }
                });

                // for MenuOrderItemView scrollview with custom data cells
                p_ResourceDictionary.Add(NewMenuViewSwipeOptionStyle, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.NewMenuViewSwipeOptionBackgroundColor},
                                //new Setter{Property = StackLayout.OpacityProperty, Value = p_Parameters.MenuScrollViewBackgroundColorOpacity}
                        }
                });

                // for MenuOrderItemView La Carte section
                p_ResourceDictionary.Add(NewMenuViewVoirLaCarteStyle, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.NewMenuViewVoirLaCarteBackgroundColor},
                                new Setter{Property = StackLayout.OpacityProperty, Value = p_Parameters.NewMenuViewMainHeaderOpacity},
                        }
                });

                // for MenuOrderItemView info notification
                p_ResourceDictionary.Add(NewMenuViewButtomNotificationStyle, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.NewMenuViewButtomNotificationBackgroundColor}
                        }
                });

                // for MenuOrderItemView info notification
                p_ResourceDictionary.Add(TypeRecetteBackGroundColor, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.TypeRecetteListStackBackgroundColor}
                        }
                });

                // For notification bullet
                p_ResourceDictionary.Add(OrderNotificationStackBackgroundColor, new Style(typeof(StackLayout))
                {
                    Setters =
                        {
                                new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.OrderNotificationBackgroundColor}
                        }
                });
                #endregion

                #region === Grids ===   

                p_ResourceDictionary.Add(BurgerStatusBarStyle, new Style(typeof(Grid))
                {
                    Setters = 
				    {
						    (Device.OS == TargetPlatform.iOS) ? new Setter{Property = Grid.BackgroundColorProperty , Value = p_Parameters.BurgerStatusBarColor} : new Setter{Property = Grid.BackgroundColorProperty , Value = Color.Transparent}
				    }
                });
                #endregion

                #region === ListViews ===

                //Hamburger Favoris BackGround
                p_ResourceDictionary.Add(HamburgerFavorisAndAjouterStyle, new Style(typeof(ListView))
                {
                    Setters =
                        {
                                new Setter{Property = ListView.BackgroundColorProperty, Value = p_Parameters.HamburgerFavorisAndAjouterBackGroundColor},
                                new Setter{Property = ListView.SeparatorColorProperty, Value = p_Parameters.HamburgerFavorisAndAjouterSeperatorColor}
                        }
                });

                //For the forms with validation
                p_ResourceDictionary.Add(ListViewstyle, new Style(typeof(ListView))
                {
                    Setters =
                    {
                            new Setter{Property = ListView.BackgroundColorProperty, Value = Color.Transparent},
                            new Setter{Property = ListView.SeparatorColorProperty, Value = p_Parameters.CreationListViewSeperatorColor},
                            new Setter{Property = ListView.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand}
                    }
                });

                p_ResourceDictionary.Add(BadgeListViewstyle, new Style(typeof(ListView))
                {
                    Setters =
                    {
                            new Setter{Property = ListView.BackgroundColorProperty, Value = Color.Transparent},
                            new Setter{Property = ListView.SeparatorColorProperty, Value = p_Parameters.BadgeListSeperatorColor}
                    }
                });


                p_ResourceDictionary.Add(mSitesListViewstyle, new Style(typeof(ListView))
                {
                    Setters =
                    {
                            new Setter{Property = ListView.BackgroundColorProperty, Value = Color.Transparent},
                            new Setter{Property = ListView.SeparatorColorProperty, Value = p_Parameters.TextColor}
                    }
                });

                //MenuOrder DateListView
                p_ResourceDictionary.Add(NewMenuViewDateListViewStyle, new Style(typeof(ListView))
                {
                    Setters =
                    {
                            new Setter{Property = ListView.SeparatorColorProperty, Value = p_Parameters.NewMenuViewDateListViewSeperatorBackgroundColor}
                    }
                });

                #endregion

                #region === Others ===

                //SliderStyle
                p_ResourceDictionary.Add(SliderStyle, new Style(typeof(Slider))
                {
                    Setters =
                        {
                                //new Setter{Property = Slider.BackgroundColorProperty, Value = p_Parameters.ThemeMaincolor},
                                new Setter{Property = Slider.VerticalOptionsProperty, Value = LayoutOptions.Fill},
                                new Setter{Property = Slider.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand}
                        }
                });

                //searchStyle
                p_ResourceDictionary.Add(searchStyle, new Style(typeof(SearchBar))
                {
                    Setters =
                        {
                                new Setter{Property = SearchBar.CancelButtonColorProperty, Value = p_Parameters.ThemeMaincolor}
                        }
                });


                p_ResourceDictionary.Add(ActivityIndicatorStyle, new Style(typeof(ActivityIndicator))
                {
                    Setters = 
                    {
                        new Setter{Property = ActivityIndicator.ColorProperty , Value = p_Parameters.ThemeMaincolor},
                        new Setter{Property = ActivityIndicator.HorizontalOptionsProperty, Value = LayoutOptions.Center},
                        new Setter{Property = ActivityIndicator.VerticalOptionsProperty, Value = LayoutOptions.Center}
                    }
                });

                //For the forms with validation
                p_ResourceDictionary.Add(FieldHeader, new Style(typeof(Field))
                {
                    Setters =
                    {
                            new Setter{Property = Field.LabelColorProperty, Value = p_Parameters.TextColor}
                    }
                });

                /* 
                //For the forms with validation
                p_ResourceDictionary.Add(PickerStyle, new Style(typeof(Sodexo.Controls.Picker))
                {
                    Setters =
                    {
                            new Setter{Property = Sodexo.Controls.Picker.BackgroundColorProperty, Value = Color.White}
                    }
                });

                //For the forms with validation
                p_ResourceDictionary.Add(PickerStyle2, new Style(typeof(Sodexo.Controls.CustomPlusPicker))
                {
                    Setters =
                    {
                            new Setter{Property = Sodexo.Controls.CustomPlusPicker.BackgroundColorProperty, Value = Color.White}
                    }
                });
                
                // Image circle border color
                p_ResourceDictionary.Add(IMAGE_CIRCLE_TYPE_RECETTE_BORDER_COLOR, new Style(typeof(ImageCircle.Forms.Plugin.Abstractions.CircleImage))
                {
                    Setters =
                    {
                            new Setter{Property = ImageCircle.Forms.Plugin.Abstractions.CircleImage.BorderColorProperty, Value = p_Parameters.TypeRecetteColor}
                    }
                });
                */
                // Stack recette background color.
                p_ResourceDictionary.Add(IMAGE_CIRCLE_TYPE_RECETTE_LABEL_BACKGROUNDCOLOR, new Style(typeof(StackLayout))
                {
                    Setters =
                    {
                            new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.TypeRecetteLabelStackBackgroundColor}
                    }
                });

                // for menu scrollview
                p_ResourceDictionary.Add(MENUS_SCROLLVIEW_STYLE, new Style(typeof(ScrollView))
                {
                    Setters =
                    {
                            //new Setter{Property = ScrollView.BackgroundColorProperty, Value = p_Parameters.MenuScrollViewBackgroundColor},
                            new Setter{Property = ScrollView.BackgroundColorProperty, Value = Color.White},
                            new Setter{Property = ScrollView.OpacityProperty, Value = p_Parameters.MenuScrollViewBackgroundColorOpacity}
                    }
                });

                
                #endregion

                #region ==Gerant==
                
                // Default Look for all pages
                p_ResourceDictionary.Add(ContentPageStyle, new Style(typeof(ContentPage))
                {
                    Setters =
                    {
                        new Setter{Property = ContentPage.BackgroundColorProperty, Value = p_Parameters.GerantPageBackgroundColor},
                        new Setter{Property = ContentPage.PaddingProperty, Value = Device.OnPlatform(new Thickness(0,20,0,0),0,0)}
                    }
                });

                p_ResourceDictionary.Add(PopUpHeaderStyle, new Style(typeof(ContentView))
                {
                    Setters =
                    {
                        new Setter{Property = ContentView.HeightRequestProperty, Value = 60},
                        new Setter{Property = ContentView.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = ContentView.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                    }
                });

                #region == Images ==
                p_ResourceDictionary.Add(popIncancelImageStyle, new Style(typeof(Image))
                {
                    Setters =
                    {
                        new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFill},
                        new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.Center},
                        new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.Center},

                    }
                });

                p_ResourceDictionary.Add(HeaderImageStyle, new Style(typeof(Image))
                {
                    Setters =
                    {
                        new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                        new Setter{Property = Image.HeightRequestProperty, Value = (double)Device.OnPlatform(35,40,50)},
                        new Setter{Property = Image.WidthRequestProperty, Value = (double)Device.OnPlatform(35,40,50)},
                        new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                        new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand},

                    }
                });

                p_ResourceDictionary.Add(ChangeStockImageStyle, new Style(typeof(Image))
                {
                    BaseResourceKey= HeaderImageStyle,
                    Setters =
                    {
                        new Setter{Property = Image.AspectProperty, Value = Aspect.AspectFit},
                        new Setter{Property = Image.HeightRequestProperty, Value = (double)Device.OnPlatform(40,40,45)},
                        new Setter{Property = Image.WidthRequestProperty, Value = (double)Device.OnPlatform(40,40,45)},
                        new Setter{Property = Image.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                        new Setter{Property = Image.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand},

                    }
                });
                #endregion

                #region == Grid ==
                p_ResourceDictionary.Add(HeaderImageGridStyle, new Style(typeof(Grid))
                {
                    Setters =
                    {
                        new Setter{Property = Grid.WidthRequestProperty, Value = (double)Device.OnPlatform(40,45,55)},
                        new Setter{Property = Grid.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = Grid.HorizontalOptionsProperty, Value = LayoutOptions.End},

                    }
                });

                p_ResourceDictionary.Add(PopinStyle, new Style(typeof(Grid))
                {
                    Setters =
                    {
                        new Setter{Property = Grid.BackgroundColorProperty, Value = p_Parameters.PopinBackgroundColor}
                    }
                });

                p_ResourceDictionary.Add(DefaultGridStyle, new Style(typeof(Grid))
                {
                    Setters =
                    {
                        new Setter{Property = Grid.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = Grid.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = Grid.PaddingProperty, Value = 0},
                        new Setter{Property = Grid.ColumnSpacingProperty, Value = 0},
                        new Setter{Property = Grid.RowSpacingProperty, Value = 0},
                    }
                });

                p_ResourceDictionary.Add(MainThemedGridStyle, new Style(typeof(Grid))
                {
                    BaseResourceKey= DefaultGridStyle,
                    Setters =
                    {
                        new Setter{Property = Grid.BackgroundColorProperty, Value = p_Parameters.GerantMainColor},
                    }
                });

                p_ResourceDictionary.Add(SecondaryThemedGridStyle, new Style(typeof(Grid))
                {
                    BaseResourceKey = DefaultGridStyle,
                    Setters =
                    {
                        new Setter{Property = Grid.BackgroundColorProperty, Value = p_Parameters.GerantSecondaryColor},
                    }
                });

                p_ResourceDictionary.Add(WhiteThemedGridStyle, new Style(typeof(Grid))
                {
                    BaseResourceKey = DefaultGridStyle,
                    Setters =
                    {
                        new Setter{Property = Grid.BackgroundColorProperty, Value = Color.White},
                    }
                });

                #endregion

                #region == Label ==
                p_ResourceDictionary.Add(TitlePopinStyle, new Style(typeof(Label))
                {
                    Setters =
                    {
                        new Setter{Property = Label.TextColorProperty, Value = Color.Black},
                        new Setter{Property = Label.FontAttributesProperty, Value = FontAttributes.Bold},
                        new Setter{Property = Label.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Medium, typeof(Label))},
                        new Setter{Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = Label.VerticalTextAlignmentProperty, Value = TextAlignment.Center},
                        new Setter{Property = Label.HorizontalTextAlignmentProperty, Value = TextAlignment.Center},
                    }
                });

                p_ResourceDictionary.Add(ConnexionTitlePopinStyle, new Style(typeof(Label))
                {
                    Setters =
                    {
                        new Setter{Property = Label.TextColorProperty, Value = Color.Black},
                        new Setter{Property = Label.FontAttributesProperty, Value = FontAttributes.Bold},
                        new Setter{Property = Label.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Large, typeof(Label))},
                        new Setter{Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = Label.VerticalTextAlignmentProperty, Value = TextAlignment.Center},
                        new Setter{Property = Label.HorizontalTextAlignmentProperty, Value = TextAlignment.Center},
                    }
                });

                p_ResourceDictionary.Add(PopinContentTextStyle, new Style(typeof(Label))
                {
                    BaseResourceKey=TitlePopinStyle,
                    Setters =
                    {
                        new Setter{Property = Label.TextColorProperty, Value = Color.FromHex("#666666")},
                        new Setter{Property = Label.FontAttributesProperty, Value = FontAttributes.None},
                        new Setter{Property = Label.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Medium, typeof(Label))},
                        new Setter{Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.Fill},
                    }
                });

                p_ResourceDictionary.Add(GerantEntete36LabelStyle, new Style(typeof(Label))
                {
                    Setters =
                    {
                        new Setter{Property = Label.TextColorProperty, Value = p_Parameters.TextColor},
                        //new Setter{Property = Label.FontAttributesProperty, Value = FontAttributes.Bold},
                        new Setter{Property = Label.FontSizeProperty, Value = Device.OnPlatform(40,40,40)},
                        new Setter{Property = Label.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = Label.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = Label.VerticalTextAlignmentProperty, Value = TextAlignment.Center},
                        new Setter{Property = Label.HorizontalTextAlignmentProperty, Value = TextAlignment.Center},
                        new Setter{Property = Label.LineBreakModeProperty, Value = LineBreakMode.NoWrap},
                    }
                });

                p_ResourceDictionary.Add(GerantEntete16LabelStyle, new Style(typeof(Label))
                {
                    BaseResourceKey= GerantEntete36LabelStyle,
                    Setters =
                    {
                        //new Setter{Property = Label.FontAttributesProperty, Value = FontAttributes.Bold},
                        new Setter{Property = Label.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Medium, typeof(Label))},
                    }
                });

                p_ResourceDictionary.Add(BUTTON_LABEL_STYLE, new Style(typeof(Label))
                {
                    Setters =
                        {
                            new Setter{Property = Label.TextColorProperty, Value = Color.Gray},
                            new Setter{Property = Label.FontAttributesProperty, Value = FontAttributes.Bold},
                            new Setter{Property = Label.VerticalTextAlignmentProperty, Value = TextAlignment.Center},
                            new Setter{Property = Label.HorizontalTextAlignmentProperty, Value = TextAlignment.Center},
                            new Setter{Property = Label.FontSizeProperty, Value = "{StaticResource BUTTON_LABEL_FONTSIZE}"}
                        }
                });

                p_ResourceDictionary.Add(BUTTON_EXTENDEDLABEL_STYLE, new Style(typeof(Sodexo.Controls.ExtendedLabel))
                {
                    Setters =
                        {
                            new Setter{Property = Sodexo.Controls.ExtendedLabel.IsUnderlineProperty, Value = true},
                            new Setter{Property = Sodexo.Controls.ExtendedLabel.TextColorProperty, Value = Color.Gray},
                            new Setter{Property = Sodexo.Controls.ExtendedLabel.FontAttributesProperty, Value = FontAttributes.Bold},
                            new Setter{Property = Sodexo.Controls.ExtendedLabel.VerticalTextAlignmentProperty, Value = TextAlignment.Center},
                            new Setter{Property = Sodexo.Controls.ExtendedLabel.HorizontalTextAlignmentProperty, Value = TextAlignment.Center},
                            new Setter{Property = Sodexo.Controls.ExtendedLabel.FontSizeProperty, Value = "{StaticResource BUTTON_LABEL_FONTSIZE}"}
                        }
                });

                #endregion

                #region === Entry ===

                // Entry Input Style
                p_ResourceDictionary.Add(EntryStyle, new Style(typeof(Entry))
                {
                    Setters =
                    {
                        new Setter{Property = Entry.TextColorProperty, Value = p_Parameters.EntryTextColor},
                        new Setter{Property = Entry.VerticalOptionsProperty, Value = LayoutOptions.Start},
                        new Setter{Property = Entry.BackgroundColorProperty, Value = Color.White}
                    }
                });

                #endregion

                #region == Button ==
                // Me connecter
                p_ResourceDictionary.Add(SecMainbuttonStyle, new Style(typeof(Button))
                {
                    BaseResourceKey = MainbuttonStyle,
                    Setters =
                    {
                        new Setter{Property = Button.BackgroundColorProperty, Value = p_Parameters.ThemeMaincolor},
                        new Setter{Property = Button.TextColorProperty, Value = Color.White},
                        new Setter{Property = Button.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand}
                    }
                });

                #endregion

                #region == StackLayout ==
                p_ResourceDictionary.Add(HeaderImageStackStyle, new Style(typeof(StackLayout))
                {
                    Setters =
                    {
                        new Setter{Property = StackLayout.WidthRequestProperty, Value = (double)Device.OnPlatform(40,45,55)},
                        new Setter{Property = StackLayout.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = StackLayout.HorizontalOptionsProperty, Value = LayoutOptions.End},

                    }
                });

                p_ResourceDictionary.Add(SpecialWPStack, new Style(typeof(StackLayout))
                {
                    Setters =
                    {
                        new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.ThemeMaincolor}
                    }
                });

                p_ResourceDictionary.Add(FixedButtonColorStyle, new Style(typeof(StackLayout))
                {
                    Setters =
                    {
                        new Setter{Property = StackLayout.BackgroundColorProperty, Value = p_Parameters.FixedButtoncolor}
                    }
                });

                p_ResourceDictionary.Add(DarkPaddedPopInStyle, new Style(typeof(StackLayout))
                {
                    Setters =
                    {
                        new Setter{Property = StackLayout.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = StackLayout.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = StackLayout.BackgroundColorProperty, Value = new Color(0, 0, 0, 0.7)},
                        new Setter{Property = StackLayout.PaddingProperty, Value = Device.Idiom==TargetIdiom.Phone ? new Thickness(10,0) : new Thickness(140,0)}
                    }
                });

                p_ResourceDictionary.Add(ConnexionPopInStyle, new Style(typeof(StackLayout))
                {
                    Setters =
                    {
                        new Setter{Property = StackLayout.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = StackLayout.HorizontalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                        new Setter{Property = StackLayout.BackgroundColorProperty, Value = new Color(0, 0, 0, 0.7)},
                        new Setter{Property = StackLayout.PaddingProperty, Value = Device.Idiom==TargetIdiom.Phone ? new Thickness(30,0) : new Thickness(140,0)}
                    }
                });

                #endregion

                #region ==Rounded Butons ==
                p_ResourceDictionary.Add(RoundedButtonStyle, new Style(typeof(Sodexo.Controls.RoundedButton))
                {
                    Setters =
                    {
                        new Setter{Property = Sodexo.Controls.RoundedButton.BackgroundColorProperty, Value = Color.Transparent},
                        new Setter{Property = Sodexo.Controls.RoundedButton.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                        new Setter{Property = Sodexo.Controls.RoundedButton.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                        new Setter{Property = Sodexo.Controls.RoundedButton.FontSizeProperty, Value = Device.GetNamedSize(NamedSize.Small, typeof(Button)) },
                        new Setter{Property = Sodexo.Controls.RoundedButton.FontAttributesProperty, Value = FontAttributes.Bold},
                        new Setter{Property = Sodexo.Controls.RoundedButton.TextColorProperty, Value = p_Parameters.RoundedButtonColor},
                        new Setter{Property = Sodexo.Controls.RoundedButton.BorderColorProperty, Value = p_Parameters.RoundedButtonColor},
                        new Setter{Property = Sodexo.Controls.RoundedButton.BorderWidthProperty, Value = 3},
                        new Setter{Property = Sodexo.Controls.RoundedButton.HeightRequestProperty, Value = Device.OnPlatform(35,40,80)},
                        new Setter{Property = Sodexo.Controls.RoundedButton.BorderRadiusProperty, Value = Device.OnPlatform(13,25,3)},
                    }
                });

                p_ResourceDictionary.Add(RoundedCustomButtonStyle, new Style(typeof(Sodexo.Controls.RoundedButton))
                {
                    Setters =
                    {
                        new Setter{Property = Sodexo.Controls.RoundedButton.VerticalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                        new Setter{Property = Sodexo.Controls.RoundedButton.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                        new Setter{Property = Sodexo.Controls.RoundedButton.FontSizeProperty, Value = 20},
                        new Setter{Property = Sodexo.Controls.RoundedButton.FontAttributesProperty, Value = FontAttributes.Bold},
                        new Setter{Property = Sodexo.Controls.RoundedButton.TextColorProperty, Value = p_Parameters.RoundedButtonColor},
                        new Setter{Property = Sodexo.Controls.RoundedButton.BackgroundColorProperty, Value = p_Parameters.RoundedButtonColor},
                        new Setter{Property = Sodexo.Controls.RoundedButton.BorderColorProperty, Value = p_Parameters.RoundedButtonColor},
                        new Setter{Property = Sodexo.Controls.RoundedButton.BorderWidthProperty, Value = 3},
                        new Setter{Property = Sodexo.Controls.RoundedButton.BorderRadiusProperty, Value = Device.OnPlatform(3,40,3)}
                    }
                });
                #endregion

                #endregion

            #endregion

                #region === Ajouté par Michaël pour les couleursdes text et pickers ===

                // Vu avec Guillaume : On met en fond une couleur qui rappel le thème.

                //p_ResourceDictionary.Add(new Style(typeof(Entry))
                //{
                //    Setters = 
                //    {
                //        new Setter{Property = Entry.BackgroundColorProperty ,  Value = Color.FromHex("FFFFCA9E")},
                //    }
                //});

                p_ResourceDictionary.Add(new Style(typeof(Xamarin.Forms.Picker))
                {
                    Setters = 
                    {
                        new Setter{Property = Xamarin.Forms.Picker.BackgroundColorProperty , Value = Color.FromHex("FFDDDDDD")},
                    }
                });

                
            #endregion

        }
    }
}
