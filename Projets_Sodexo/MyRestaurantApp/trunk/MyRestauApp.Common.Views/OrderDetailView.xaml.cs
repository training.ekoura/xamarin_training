﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Sodexo.Controls;

namespace MyRestauApp.Common.Views
{
    public partial class OrderDetailView : ContentPage, IOrderDetailView
    {
        Grid SavedSubGrid;
        CustomButton SavedAnnulerButton;

        public OrderDetailView()
        {
            InitializeComponent();
            // Hide navigation bar
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);

            annulerButton.PropertyChanged += annulerButton_PropertyChanged;
        }

        void annulerButton_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            SavedSubGrid = subGrid;
            SavedAnnulerButton = annulerButton;

            if (e.PropertyName == "IsVisible")
            {
                if (!(sender as CustomButton).IsVisible)
                {
                    Grid.SetRowSpan(subGrid, 2);
                }
                else
                {
                    Grid.SetRowSpan(subGrid, 1);
                    Grid.SetRowSpan(annulerButton, 1);
                }
            }
        }
    }



    [ContentProperty("Source")]
    public class ImageResourceExtension : IMarkupExtension
    {
        public string Source { get; set; }
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return ImageSource.FromResource(Source);
        }
    }
}