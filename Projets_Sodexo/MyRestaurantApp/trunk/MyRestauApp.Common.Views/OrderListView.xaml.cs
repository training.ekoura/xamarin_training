﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Sodexo;
using Sodexo.Controls;
using MyRestauApp.Common.ViewModels;

namespace MyRestauApp.Common.Views
{
    public partial class OrderListView : ContentPage, IOrderListView
    {
        bool hasLoaded = false;

        public OrderListView()
        {
            InitializeComponent();
            // Hide navigation bar
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
        }

        protected override void OnDisappearing()
        {
            if(Device.OS==TargetPlatform.WinPhone)
            {
                if (!hasLoaded)
                    hasLoaded = true;
            }
        }

        protected async override void OnAppearing()
        {
            if (Device.OS == TargetPlatform.WinPhone)
            {
                if (hasLoaded)
                {
                    Repeater.Update();
                }
            }
        }
    }
}