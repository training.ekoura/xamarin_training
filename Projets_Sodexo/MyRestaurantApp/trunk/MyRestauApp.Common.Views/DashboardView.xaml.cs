﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using MyRestauApp.Common.ViewModels;
using Sodexo;
namespace MyRestauApp.Common.Views
{
    public partial class DashboardView : ContentPage, IDashboardView
    {
        bool hasLoaded = false;
        public DashboardView()
        {
            InitializeComponent();
            // Hide navigation bar
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            ListStockRepeater.Padding = Device.OnPlatform(new Thickness(0, 0, 0, 0), new Thickness(0, 0, 0, 0), new Thickness(0, 0, 0, 65));

            

            ScheduleRefreshThread();
        }

        void LocalTapCommand(object sender, EventArgs e)
        {

            var SelectedStockArticleViewModelItem = ((Xamarin.Forms.TapGestureRecognizer)((Xamarin.Forms.Image)sender).GestureRecognizers[0]).CommandParameter;
            var l_TypeImage = ((Xamarin.Forms.TapGestureRecognizer)((Xamarin.Forms.Image)sender).GestureRecognizers[0]).StyleId;
            bool isAddQuantity = bool.Parse(l_TypeImage) ? true :false;
            
            var localBindinContext = (DashboardViewModel)this.BindingContext;
            localBindinContext.ChangeStockQuantity(SelectedStockArticleViewModelItem, isAddQuantity);
        }

        private void ScheduleRefreshThread()
        {
            //try
            //{
            //    // Start a timer that runs after 1 minute.
            //    Device.StartTimer(TimeSpan.FromMinutes(1), () =>
            //    {
            //        Task.Factory.StartNew( () =>
            //        {
            //            // Update the UI 
            //            Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IContextViewModel>().RefreshDashboard();

            //         });
            //        return true;
            //    });
            //}catch(Exception ex)
            //{

            //}

            try
            {
                // Start a timer that runs after 1 minute.
                Device.StartTimer(TimeSpan.FromMinutes(1), () =>
                {
                    Task.Factory.StartNew(async () =>
                    {
                        // Update the UI
                        Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IContextViewModel>().RefreshDashboard();

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            // Update the UI
                            // ...
                            // Now repeat by scheduling a new request
                            ScheduleRefreshThread();
                        });

                    });
                    return false;
                });
            }
            catch (Exception ex)
            {

            }
        }

        protected override void OnAppearing()
        {
            if(!hasLoaded)
            {
                var btVoirCommandesJour = new Sodexo.Controls.RoundedButton()
                {
                    StyleId = "btVoirCommandesJour",
                    Text = Properties.Resources.Dashboard_See_CommandeOf_Day,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions = LayoutOptions.Fill
                };
                btVoirCommandesJour.SetDynamicResource(Sodexo.Controls.RoundedButton.StyleProperty, Themes.Theme.RoundedButtonStyle);
                btVoirCommandesJour.SetBinding(Sodexo.Controls.RoundedButton.CommandProperty, "LoadListOrdersCommand");

                Grid.SetRow(btVoirCommandesJour, 1);
                subGrid.Children.Add(btVoirCommandesJour);

                hasLoaded = true;
            }
        }
    }
}
