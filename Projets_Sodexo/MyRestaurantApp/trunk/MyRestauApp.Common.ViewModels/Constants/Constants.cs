﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.ViewModels
{
    public static class Constants
    {
        public const string StringThemeSodexo = "Sodexo";
        public const string ONRESUME = "OnResume";

        public const string StringExceptionTypeNoSite = "AccountWithoutOpenPortailException";

        // CLEAR OU MD5
        public const string PwdMode = "CLEAR";

        // Liste des thèmes
        public const string ThemeMySodexo = "my-sodexo";
        public const int HoursValidToken = 12;

        public const string CancelLogicValue = "ANNULEE";    
    }
}
