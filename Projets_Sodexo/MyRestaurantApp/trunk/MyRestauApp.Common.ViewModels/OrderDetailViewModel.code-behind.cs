using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using MyRestauApp.Common.DAOServices;
using MyRestauApp.Common.DAOServices.Models;
using MyRestauApp.Common.DAOServices.Requests;
using MyRestauApp.Common.ViewModels.ItemsViewModel;
using Sodexo;
using MyRestauApp.Common.DAOServices.Services;
using System.Globalization;

namespace MyRestauApp.Common.ViewModels
{
    public partial class OrderDetailViewModel : OrderDetailViewModelBase, IOrderDetailViewModel
    {
        #region Proprietes
        private ICommandeService CommandService;
        private IGlobalConfigService GlobalConfigService;
        private IDataOfflineService DataOfflineService;
        private CultureInfo Culture;
        DashboardUserByDate DashboardSaved;
        #endregion

        #region Initialisation
        protected override void OnInitialize()
        {
            base.OnInitialize();
            ResolveInitiliazation();
        }

        public void ResolveInitiliazation()
        {
            CommandService = Sodexo.Framework.Services.Container().Resolve<ICommandeService>();
            GlobalConfigService = Sodexo.Framework.Services.GlobalConfigService();
            Culture = new CultureInfo(Properties.Resources.CultureInfoCurrentCultureName.Replace("_", "-"));
            DataOfflineService = Sodexo.Framework.Services.DataOfflineService();
        }
        #endregion End Initialisation

        // Appel� � partir de la vue OrderListView via DashboardViewModel
        #region Prepare
        public async Task<bool> PrepareOrderDetailVM(int commandId, string date)
        {
            try
            {
                BeginWork();
                ResolveInitiliazation();
                OrderId = commandId;
                SelectedDate = date;
                try
                {
                    //Pour le mode offline
                    DashboardSaved = DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                         .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).First().DashboardUserByDate.Where(x => x.DateDebut == date).FirstOrDefault();
                }catch
                {
                    DashboardSaved = null;
                }
                var commandServiceResult = await CommandService.GetCommandeGerantAsync(GlobalConfigService.Data.Token.TokenVal, commandId);
                if (commandServiceResult.Success)
                {
                    if (commandServiceResult.Data != null)
                    {
                        //Ajout dans Global config pour dire la commande n'est plus nouvelle
                        if (!GlobalConfigService.Data.ViewedOrdersIds.Contains(commandId))
                            GlobalConfigService.Data.ViewedOrdersIds.Add(commandId);
                        GlobalConfigService.SaveConfig();
                        var ordersVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IOrdersViewModel>();
                        ordersVM.ListOrders.Where(x => x.Id == commandId).FirstOrDefault().IsNew = false;
                        Order = new OrderViewModelItem().SetOrderEncapsulation(commandServiceResult.Data);
                        HeaderText = Order.ZoneRestaurationNom;
                        Order.FormattedDateForView = DateTime.ParseExact(commandServiceResult.Data.DateEffectiveFormat, "dd/MM/yyyy HH:mm", Culture).ToString("d MMMM", Culture);
                        Order.FormattedLabelPlat = string.Format(Properties.Resources.LabelPlatOrder, commandServiceResult.Data.ArticlesPlatsCount);
                        Order.FormattedLabelProduit = string.Format(Properties.Resources.LabelProduitOrder, commandServiceResult.Data.ArticlesCount);
                        IsVisibleButtonCancel = (Order.IsAnnulable) ? true : false;
                        Order.FormattedLabelFirstLastName = string.Format("{0} {1}", Order.UserPortail.FirstName, Order.UserPortail.LastName);

                        return true;
                    }
                    else
                    {
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.ErrorGetCommande);
                        return false;
                    }
                }
                else
                {
                    var isError = true;
                    if (commandServiceResult.ExceptionComesFromNetwork)
                    {
                        if (DashboardSaved != null && DashboardSaved.ListCommandes!=null)
                        {
                            isError = false;
                            var command = DashboardSaved.ListCommandes.Where(x => x.Id == OrderId).FirstOrDefault();
                            HeaderText = command.ZoneRestaurationNom;
                            command.FormattedDateForView = DateTime.ParseExact(command.DateEffectiveFormat, "dd/MM/yyyy HH:mm", Culture).Date.ToString("dd.MM");
                            command.FormattedLabelPlat = string.Format(Properties.Resources.LabelPlatOrder, command.ArticlesPlatsCount);
                            command.FormattedLabelProduit = string.Format(Properties.Resources.LabelProduitOrder, command.ArticlesCount);
                            IsVisibleButtonCancel = (command.IsAnnulable) ? true : false;
                            command.FormattedLabelFirstLastName = command.UserPortail.FirstName + " " + command.UserPortail.LastName;
                            Order = new OrderViewModelItem().SetOrderEncapsulation(command);
                            return true;
                        }
                    }
                    if (isError)
                    {
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, commandServiceResult.ExceptionMessage);
                        return false;
                    }
                    return false;
                }
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                return false;
            }
            finally
            {
                EndWork();
            }
        }
        #endregion

        #region M�thodes
        // Update de commande detail pour OrderDetailView
        public async Task UpdateOrderDetail(int orderId)
        {
            try
            {
                BeginWork();
                ResolveInitiliazation();
                // Update de commande detail pour OrderDetailView
                CommandService = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.DAOServices.Services.ICommandeService>();
                var commandResult = await CommandService.GetCommandeGerantAsync(GlobalConfigService.Data.Token.TokenVal, orderId);
                if (commandResult.Success)
                {
                    if (commandResult.Data != null)
                    {
                        Order = new OrderViewModelItem().SetOrderEncapsulation(commandResult.Data);
                        HeaderText = Order.ZoneRestaurationNom;
                        //  CommandDetail.FormattedDateForView = DateTime.ParseExact(commandServiceResult.Data.DateEffectiveFormat, "dd/MM/yyyy HH:mm", Culture).Date.ToString("dd.MM");
                        Order.FormattedDateForView = DateTime.ParseExact(commandResult.Data.DateEffectiveFormat, "dd/MM/yyyy HH:mm", Culture).ToString("d MMMM", Culture);
                        Order.FormattedLabelPlat = string.Format(Properties.Resources.LabelPlatOrder, commandResult.Data.ArticlesPlatsCount);
                        Order.FormattedLabelProduit = string.Format(Properties.Resources.LabelProduitOrder, commandResult.Data.ArticlesCount);
                        IsVisibleButtonCancel = (Order.IsAnnulable) ? true : false;
                        Order.FormattedLabelFirstLastName = Order.UserPortail.FirstName + " " + Order.UserPortail.LastName;
                      
                    }
                }
                else
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, commandResult.ExceptionMessage);
            }
            catch (Exception e)
            {
                //Afficher message d'erreur
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        public void UpdateOrdersList()
        {
            var ordersVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IOrdersViewModel>();
            ordersVM.ListOrders.Where(x => x.Id == Order.Id).FirstOrDefault().Statut.LogicValue = Order.Statut.LogicValue;
            ordersVM.ListOrders.Where(x => x.Id == Order.Id).FirstOrDefault().Statut.LibelleGerant = Order.Statut.LibelleGerant;
            ordersVM.ListOrders.Where(x => x.Id == Order.Id).FirstOrDefault().ListStatutsPossibles = Order.ListStatutsPossibles;
            ordersVM.ListOrders.Where(x => x.Id == Order.Id).FirstOrDefault().Statut.StatutCodeCouleur = Order.Statut.StatutCodeCouleur;
            var delete = GlobalConfigService.Data.ListStatutSaved.Where(x => x.LogicValue == Order.Statut.LogicValue).FirstOrDefault().IsChecked ? false : true;
            if (delete)
                ordersVM.ListOrders.Remove(ordersVM.ListOrders.Where(x => x.Id == Order.Id).FirstOrDefault());
                  
            try
            {
                //On met � jour aussi pour le mode offline
                //R�cup�rer la commande enregistr� � utiliser en cas de besoin
                if (DashboardSaved != null)
                {
                    if (DashboardSaved.ListCommandes != null)
                        DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                               .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).First().DashboardUserByDate.Where(x => x.DateDebut == ordersVM.DateDashBoard).FirstOrDefault().ListCommandes.Clear();
                    else
                        DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                          .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).First().DashboardUserByDate.Where(x => x.DateDebut == ordersVM.DateDashBoard).FirstOrDefault().ListCommandes = new List<Commande>();

                    DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                           .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).FirstOrDefault().DashboardUserByDate.Where(x => x.DateDebut == ordersVM.DateDashBoard).FirstOrDefault().ListCommandes.AddRange(ordersVM.ListOrders.Select(x => x.GetOrderEncapsulation()));

                }
            }
            catch (Exception ex)
            {
                // impossible de mettre � jour le mode offline 
            }
        }

        #endregion

        #region Commandes

        protected override async void OnViewStatutPopInCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                if (Order != null && Order.ListStatutsPossibles != null && Order.ListStatutsPossibles.Count > 0)
                {
                    var popInVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IPopInViewModel>();
                    var success = await popInVM.PrepareVMForChangeStatut(Order, true);
                    if (success)
                        await Sodexo.Framework.Services.InteractionService().ViewPopIn(popInVM, ViewName.PlatstatusContentView);
                }
                else
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.OrderStatutError);
            }
            catch (Exception ex)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
                EndWork();
            }
        }

        protected override async void OnCancelOrderCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();

                var popInVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IPopInViewModel>();
                popInVM.PrepareVMForAskYesNo(false);
                await Sodexo.Framework.Services.InteractionService().ViewPopIn(popInVM, ViewName.YesNoContentView,IsBurger: false);
            }
            catch (Exception e)
            {
                //Afficher message d'erreur
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        #endregion
    }
}
