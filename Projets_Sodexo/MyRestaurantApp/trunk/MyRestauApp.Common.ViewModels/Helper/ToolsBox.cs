﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.ViewModels.Helper
{
    public static class ToolsBox
    {

        public static String SeletedListViewItemColor(string theme) {

            switch (theme)
            {
                case Constants.ThemeMySodexo:
                    return "#F07F13";
                default:
                    return "";
            }

        }


        public static String SeletedThemeName(string theme)
        {

            switch (theme)
            {
                case Constants.ThemeMySodexo:
                    return "Sodexo ";

                default:
                    return "";
            }

        }

        public static String ThemeNameToImage(string theme)
        {

            switch (theme)
            {
                case Constants.ThemeMySodexo:
                    return "";
                default:
                    return "";
            }

        }

    }
}
