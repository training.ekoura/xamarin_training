
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;
using System.Globalization;
using MyRestauApp.Common.DAOServices.Services;
using System.Threading.Tasks;

namespace MyRestauApp.Common.ViewModels
{
    public partial class BurgerViewModel : BurgerViewModelBase, IBurgerViewModel
    {

        #region Proprietes
        private MyRestauApp.Common.DAOServices.Services.IAuthService AuthService;
        private MyRestauApp.Common.DAOServices.Services.IUserService UserService;
        private string Locale = Properties.Resources.CultureInfoCurrentCultureName;
        private IGlobalConfigService GlobalConfigService;
        private CultureInfo Culture;
        #endregion

        #region Initialisation
        protected override void OnInitialize()
        {
            base.OnInitialize();
            ResolveInitialization();

        }

        private void ResolveInitialization()
        {
            UserService = Sodexo.Framework.Services.Container().Resolve<IUserService>();
            GlobalConfigService = Sodexo.Framework.Services.GlobalConfigService();
            Culture = new CultureInfo(Properties.Resources.CultureInfoCurrentCultureName.Replace("_", "-"));
            AuthService = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.DAOServices.Services.IAuthService>();
        }
        #endregion

        public async Task<bool> PrepareVM()
        {
            try
            {
                BeginWork();
                ResolveInitialization();
                UserImage = "res|MySodexo.Common.Views.Images.DefaultContact.png";
                var DataOfflineService = Sodexo.Framework.Services.DataOfflineService();
                var userDataOffline = DataOfflineService.Data != null && DataOfflineService.Data.ListUserData != null ? DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId).FirstOrDefault() : null;
                var userResult = await UserService.GetUser(GlobalConfigService.Data.Token.TokenVal);
                if (userResult.Success)
                {
                    if (userResult.Data != null)
                    {
                        UserFirstName = userResult.Data.Prenom;
                        UserLastName = userResult.Data.Nom;
                        SiteName = userResult.Data.Site.LastOrDefault().SiteLibelle;
                        if (userDataOffline != null)
                        {
                            DataOfflineService.Data.ListUserData.Where(x => x.LoginUser == userDataOffline.LoginUser).FirstOrDefault().UserDetails = userResult.Data;
                            DataOfflineService.SaveConfig();
                        }
                    }
                    else
                    {
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.ErrorGetUser);
                        return false;
                    }
                }
                else
                {
                    var isError = true;
                    if (userResult.ExceptionComesFromNetwork)
                    {
                        //On va voir si on a des donn�es en offline
                        if (userDataOffline != null && userDataOffline.UserDetails != null)
                        {
                            UserFirstName = userDataOffline.UserDetails.Prenom;
                            UserLastName = userDataOffline.UserDetails.Nom;
                            SiteName = userDataOffline.UserDetails.Site.LastOrDefault().SiteLibelle;
                            isError = false;
                        }
                    }
                    if (isError)
                    {
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, userResult.ExceptionMessage);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                return false;
            }
            finally
            {
                EndWork();
            }
        }

        protected override async void OnDisconnectCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                var popInVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IPopInViewModel>();
                popInVM.PrepareVMForAskYesNo(true);
                await Sodexo.Framework.Services.InteractionService().ViewPopIn(popInVM, ViewName.YesNoContentView, IsBurger:true);
            }
            catch (Exception ex)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.DisconnectImpossible);
            }
            finally
            {
                EndWork();
            }
        }

    }
}
