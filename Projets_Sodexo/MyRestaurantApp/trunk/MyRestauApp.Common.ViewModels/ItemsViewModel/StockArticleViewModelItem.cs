﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML StockArticleViewModelItem
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  Sodexo.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;

namespace MyRestauApp.Common.ViewModels
{
	public abstract partial class StockArticleViewModelItemBase : Sodexo.ViewModel
	{
		#region === Propriétés ===

			#region === Propriété : Id ===

				public const string Id_PROPERTYNAME = "Id";

				private System.String _Id;
				///<summary>
				/// Propriété : Id
				///</summary>
				public System.String Id
				{
					get
					{
						return GetValue<System.String>(StockArticleEncapsulation_Id_GetValue);
					}
					set
					{
						SetValue<System.String>(StockArticleEncapsulation_Id_GetValue, StockArticleEncapsulation_Id_SetValue, value, Id_PROPERTYNAME,  DoIdBeforeSet, DoIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIdBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIdAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ZoneRestaurationId ===

				public const string ZoneRestaurationId_PROPERTYNAME = "ZoneRestaurationId";

				private System.String _ZoneRestaurationId;
				///<summary>
				/// Propriété : ZoneRestaurationId
				///</summary>
				public System.String ZoneRestaurationId
				{
					get
					{
						return GetValue<System.String>(StockArticleEncapsulation_ZoneRestaurationId_GetValue);
					}
					set
					{
						SetValue<System.String>(StockArticleEncapsulation_ZoneRestaurationId_GetValue, StockArticleEncapsulation_ZoneRestaurationId_SetValue, value, ZoneRestaurationId_PROPERTYNAME,  DoZoneRestaurationIdBeforeSet, DoZoneRestaurationIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoZoneRestaurationIdBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoZoneRestaurationIdAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyZoneRestaurationIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LibelleCommercial ===

				public const string LibelleCommercial_PROPERTYNAME = "LibelleCommercial";

				private System.String _LibelleCommercial;
				///<summary>
				/// Propriété : LibelleCommercial
				///</summary>
				public System.String LibelleCommercial
				{
					get
					{
						return GetValue<System.String>(StockArticleEncapsulation_LibelleCommercial_GetValue);
					}
					set
					{
						SetValue<System.String>(StockArticleEncapsulation_LibelleCommercial_GetValue, StockArticleEncapsulation_LibelleCommercial_SetValue, value, LibelleCommercial_PROPERTYNAME,  DoLibelleCommercialBeforeSet, DoLibelleCommercialAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLibelleCommercialBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLibelleCommercialAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLibelleCommercialDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : LibelleTechnique ===

				public const string LibelleTechnique_PROPERTYNAME = "LibelleTechnique";

				private System.String _LibelleTechnique;
				///<summary>
				/// Propriété : LibelleTechnique
				///</summary>
				public System.String LibelleTechnique
				{
					get
					{
						return GetValue<System.String>(StockArticleEncapsulation_LibelleTechnique_GetValue);
					}
					set
					{
						SetValue<System.String>(StockArticleEncapsulation_LibelleTechnique_GetValue, StockArticleEncapsulation_LibelleTechnique_SetValue, value, LibelleTechnique_PROPERTYNAME,  DoLibelleTechniqueBeforeSet, DoLibelleTechniqueAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLibelleTechniqueBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLibelleTechniqueAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLibelleTechniqueDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Stock ===

				public const string Stock_PROPERTYNAME = "Stock";

				private System.String _Stock;
				///<summary>
				/// Propriété : Stock
				///</summary>
				public System.String Stock
				{
					get
					{
						return GetValue<System.String>(StockArticleEncapsulation_Stock_GetValue);
					}
					set
					{
						SetValue<System.String>(StockArticleEncapsulation_Stock_GetValue, StockArticleEncapsulation_Stock_SetValue, value, Stock_PROPERTYNAME,  DoStockBeforeSet, DoStockAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoStockBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoStockAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyStockDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : NbRestants ===

				public const string NbRestants_PROPERTYNAME = "NbRestants";

				private System.String _NbRestants;
				///<summary>
				/// Propriété : NbRestants
				///</summary>
				public System.String NbRestants
				{
					get
					{
						return GetValue<System.String>(StockArticleEncapsulation_NbRestants_GetValue);
					}
					set
					{
						SetValue<System.String>(StockArticleEncapsulation_NbRestants_GetValue, StockArticleEncapsulation_NbRestants_SetValue, value, NbRestants_PROPERTYNAME,  DoNbRestantsBeforeSet, DoNbRestantsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoNbRestantsBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoNbRestantsAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyNbRestantsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : NbRetires ===

				public const string NbRetires_PROPERTYNAME = "NbRetires";

				private System.String _NbRetires;
				///<summary>
				/// Propriété : NbRetires
				///</summary>
				public System.String NbRetires
				{
					get
					{
						return GetValue<System.String>(StockArticleEncapsulation_NbRetires_GetValue);
					}
					set
					{
						SetValue<System.String>(StockArticleEncapsulation_NbRetires_GetValue, StockArticleEncapsulation_NbRetires_SetValue, value, NbRetires_PROPERTYNAME,  DoNbRetiresBeforeSet, DoNbRetiresAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoNbRetiresBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoNbRetiresAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyNbRetiresDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : NbVendus ===

				public const string NbVendus_PROPERTYNAME = "NbVendus";

				private System.String _NbVendus;
				///<summary>
				/// Propriété : NbVendus
				///</summary>
				public System.String NbVendus
				{
					get
					{
						return GetValue<System.String>(StockArticleEncapsulation_NbVendus_GetValue);
					}
					set
					{
						SetValue<System.String>(StockArticleEncapsulation_NbVendus_GetValue, StockArticleEncapsulation_NbVendus_SetValue, value, NbVendus_PROPERTYNAME,  DoNbVendusBeforeSet, DoNbVendusAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoNbVendusBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoNbVendusAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyNbVendusDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux encapsulations ===


			#region === Encapsulation : StockArticleEncapsulation ===

				// Champ pour l'encapsulation.
				// Les propriétés implémentées font directement référence à ce champ.
				private MyRestauApp.Common.DAOServices.Models.StockArticle _StockArticleEncapsulation;

				///<summary>
				/// Encapsulation : StockArticleEncapsulation.
				///</summary>
				public MyRestauApp.Common.DAOServices.Models.StockArticle GetStockArticleEncapsulation()
				{
					return _StockArticleEncapsulation;
				}

				///<summary>
				/// Encapsulation : StockArticleEncapsulation.
				///</summary>
				public StockArticleViewModelItem SetStockArticleEncapsulation(MyRestauApp.Common.DAOServices.Models.StockArticle p_Value)
				{
					// En vue d'éventuelles notification (si initialisé), nous récupérons les anciennes valeurs (valeurs actuelles).
					var l_OldValueId = _StockArticleEncapsulation == null || !IsInitialized ? default(System.String) : Id;
					var l_OldValueZoneRestaurationId = _StockArticleEncapsulation == null || !IsInitialized ? default(System.String) : ZoneRestaurationId;
					var l_OldValueLibelleCommercial = _StockArticleEncapsulation == null || !IsInitialized ? default(System.String) : LibelleCommercial;
					var l_OldValueLibelleTechnique = _StockArticleEncapsulation == null || !IsInitialized ? default(System.String) : LibelleTechnique;
					var l_OldValueStock = _StockArticleEncapsulation == null || !IsInitialized ? default(System.String) : Stock;
					var l_OldValueNbRestants = _StockArticleEncapsulation == null || !IsInitialized ? default(System.String) : NbRestants;
					var l_OldValueNbRetires = _StockArticleEncapsulation == null || !IsInitialized ? default(System.String) : NbRetires;
					var l_OldValueNbVendus = _StockArticleEncapsulation == null || !IsInitialized ? default(System.String) : NbVendus;

					// Affectation du nouvel object d'encapsulation.
					_StockArticleEncapsulation = p_Value;

					// Si pas initialisé, inutile d'aller plus loin.
					if(!IsInitialized) return this as StockArticleViewModelItem;

					// Nous allons notifier, nous avons besoin des valeurs actuelles.
					var l_NewValueId = _StockArticleEncapsulation == null ? default(System.String) : Id;
					var l_NewValueZoneRestaurationId = _StockArticleEncapsulation == null ? default(System.String) : ZoneRestaurationId;
					var l_NewValueLibelleCommercial = _StockArticleEncapsulation == null ? default(System.String) : LibelleCommercial;
					var l_NewValueLibelleTechnique = _StockArticleEncapsulation == null ? default(System.String) : LibelleTechnique;
					var l_NewValueStock = _StockArticleEncapsulation == null ? default(System.String) : Stock;
					var l_NewValueNbRestants = _StockArticleEncapsulation == null ? default(System.String) : NbRestants;
					var l_NewValueNbRetires = _StockArticleEncapsulation == null ? default(System.String) : NbRetires;
					var l_NewValueNbVendus = _StockArticleEncapsulation == null ? default(System.String) : NbVendus;

					// Nous notifions.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Id_PROPERTYNAME, l_OldValueId, l_NewValueId));
					NotifyIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ZoneRestaurationId_PROPERTYNAME, l_OldValueZoneRestaurationId, l_NewValueZoneRestaurationId));
					NotifyZoneRestaurationIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LibelleCommercial_PROPERTYNAME, l_OldValueLibelleCommercial, l_NewValueLibelleCommercial));
					NotifyLibelleCommercialDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(LibelleTechnique_PROPERTYNAME, l_OldValueLibelleTechnique, l_NewValueLibelleTechnique));
					NotifyLibelleTechniqueDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Stock_PROPERTYNAME, l_OldValueStock, l_NewValueStock));
					NotifyStockDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(NbRestants_PROPERTYNAME, l_OldValueNbRestants, l_NewValueNbRestants));
					NotifyNbRestantsDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(NbRetires_PROPERTYNAME, l_OldValueNbRetires, l_NewValueNbRetires));
					NotifyNbRetiresDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(NbVendus_PROPERTYNAME, l_OldValueNbVendus, l_NewValueNbVendus));
					NotifyNbVendusDependencies();

					return this as StockArticleViewModelItem;
				}

				#region === Propriété : Id ===

					///<summary>
					/// Méthode utilisée par la propriété Id pour la lecture de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private System.String StockArticleEncapsulation_Id_GetValue()
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur StockArticleEncapsulation.", Id_PROPERTYNAME));
						return _StockArticleEncapsulation.Id;
					}

					///<summary>
					/// Méthode utilisée par la propriété Id pour l'affectation de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private void StockArticleEncapsulation_Id_SetValue(System.String p_Value)
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur StockArticleEncapsulation.", Id_PROPERTYNAME));
						_StockArticleEncapsulation.Id = p_Value;
					}


				#endregion

				#region === Propriété : ZoneRestaurationId ===

					///<summary>
					/// Méthode utilisée par la propriété ZoneRestaurationId pour la lecture de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private System.String StockArticleEncapsulation_ZoneRestaurationId_GetValue()
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur StockArticleEncapsulation.", ZoneRestaurationId_PROPERTYNAME));
						return _StockArticleEncapsulation.ZoneRestaurationId;
					}

					///<summary>
					/// Méthode utilisée par la propriété ZoneRestaurationId pour l'affectation de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private void StockArticleEncapsulation_ZoneRestaurationId_SetValue(System.String p_Value)
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur StockArticleEncapsulation.", ZoneRestaurationId_PROPERTYNAME));
						_StockArticleEncapsulation.ZoneRestaurationId = p_Value;
					}


				#endregion

				#region === Propriété : LibelleCommercial ===

					///<summary>
					/// Méthode utilisée par la propriété LibelleCommercial pour la lecture de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private System.String StockArticleEncapsulation_LibelleCommercial_GetValue()
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur StockArticleEncapsulation.", LibelleCommercial_PROPERTYNAME));
						return _StockArticleEncapsulation.LibelleCommercial;
					}

					///<summary>
					/// Méthode utilisée par la propriété LibelleCommercial pour l'affectation de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private void StockArticleEncapsulation_LibelleCommercial_SetValue(System.String p_Value)
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur StockArticleEncapsulation.", LibelleCommercial_PROPERTYNAME));
						_StockArticleEncapsulation.LibelleCommercial = p_Value;
					}


				#endregion

				#region === Propriété : LibelleTechnique ===

					///<summary>
					/// Méthode utilisée par la propriété LibelleTechnique pour la lecture de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private System.String StockArticleEncapsulation_LibelleTechnique_GetValue()
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur StockArticleEncapsulation.", LibelleTechnique_PROPERTYNAME));
						return _StockArticleEncapsulation.LibelleTechnique;
					}

					///<summary>
					/// Méthode utilisée par la propriété LibelleTechnique pour l'affectation de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private void StockArticleEncapsulation_LibelleTechnique_SetValue(System.String p_Value)
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur StockArticleEncapsulation.", LibelleTechnique_PROPERTYNAME));
						_StockArticleEncapsulation.LibelleTechnique = p_Value;
					}


				#endregion

				#region === Propriété : Stock ===

					///<summary>
					/// Méthode utilisée par la propriété Stock pour la lecture de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private System.String StockArticleEncapsulation_Stock_GetValue()
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur StockArticleEncapsulation.", Stock_PROPERTYNAME));
						return _StockArticleEncapsulation.Stock;
					}

					///<summary>
					/// Méthode utilisée par la propriété Stock pour l'affectation de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private void StockArticleEncapsulation_Stock_SetValue(System.String p_Value)
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur StockArticleEncapsulation.", Stock_PROPERTYNAME));
						_StockArticleEncapsulation.Stock = p_Value;
					}


				#endregion

				#region === Propriété : NbRestants ===

					///<summary>
					/// Méthode utilisée par la propriété NbRestants pour la lecture de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private System.String StockArticleEncapsulation_NbRestants_GetValue()
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur StockArticleEncapsulation.", NbRestants_PROPERTYNAME));
						return _StockArticleEncapsulation.NbRestants;
					}

					///<summary>
					/// Méthode utilisée par la propriété NbRestants pour l'affectation de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private void StockArticleEncapsulation_NbRestants_SetValue(System.String p_Value)
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur StockArticleEncapsulation.", NbRestants_PROPERTYNAME));
						_StockArticleEncapsulation.NbRestants = p_Value;
					}


				#endregion

				#region === Propriété : NbRetires ===

					///<summary>
					/// Méthode utilisée par la propriété NbRetires pour la lecture de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private System.String StockArticleEncapsulation_NbRetires_GetValue()
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur StockArticleEncapsulation.", NbRetires_PROPERTYNAME));
						return _StockArticleEncapsulation.NbRetires;
					}

					///<summary>
					/// Méthode utilisée par la propriété NbRetires pour l'affectation de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private void StockArticleEncapsulation_NbRetires_SetValue(System.String p_Value)
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur StockArticleEncapsulation.", NbRetires_PROPERTYNAME));
						_StockArticleEncapsulation.NbRetires = p_Value;
					}


				#endregion

				#region === Propriété : NbVendus ===

					///<summary>
					/// Méthode utilisée par la propriété NbVendus pour la lecture de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private System.String StockArticleEncapsulation_NbVendus_GetValue()
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur StockArticleEncapsulation.", NbVendus_PROPERTYNAME));
						return _StockArticleEncapsulation.NbVendus;
					}

					///<summary>
					/// Méthode utilisée par la propriété NbVendus pour l'affectation de la valeur corresponsande de StockArticleEncapsulation.
					///</summary>
					private void StockArticleEncapsulation_NbVendus_SetValue(System.String p_Value)
					{
						if(_StockArticleEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur StockArticleEncapsulation.", NbVendus_PROPERTYNAME));
						_StockArticleEncapsulation.NbVendus = p_Value;
					}


				#endregion


			#endregion

		#endregion
	}

	public partial class StockArticleViewModelItem : StockArticleViewModelItemBase
	{
	}
}
