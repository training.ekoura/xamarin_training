﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML CommandDashboardViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  Sodexo.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;

namespace MyRestauApp.Common.ViewModels
{
	public abstract partial class CommandDashboardViewModelBase : Sodexo.ViewModel
	{
		#region === Propriétés ===

			#region === Propriété : NbrRestantes ===

				public const string NbrRestantes_PROPERTYNAME = "NbrRestantes";

				private System.Int32 _NbrRestantes;
				///<summary>
				/// Propriété : NbrRestantes
				///</summary>
				public System.Int32 NbrRestantes
				{
					get
					{
						return GetValue<System.Int32>(CommandDashBoardEncapsulation_NbrRestantes_GetValue);
					}
					set
					{
						SetValue<System.Int32>(CommandDashBoardEncapsulation_NbrRestantes_GetValue, CommandDashBoardEncapsulation_NbrRestantes_SetValue, value, NbrRestantes_PROPERTYNAME,  DoNbrRestantesBeforeSet, DoNbrRestantesAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoNbrRestantesBeforeSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoNbrRestantesAfterSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyNbrRestantesDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : NbrRetirees ===

				public const string NbrRetirees_PROPERTYNAME = "NbrRetirees";

				private System.Int32 _NbrRetirees;
				///<summary>
				/// Propriété : NbrRetirees
				///</summary>
				public System.Int32 NbrRetirees
				{
					get
					{
						return GetValue<System.Int32>(CommandDashBoardEncapsulation_NbrRetirees_GetValue);
					}
					set
					{
						SetValue<System.Int32>(CommandDashBoardEncapsulation_NbrRetirees_GetValue, CommandDashBoardEncapsulation_NbrRetirees_SetValue, value, NbrRetirees_PROPERTYNAME,  DoNbrRetireesBeforeSet, DoNbrRetireesAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoNbrRetireesBeforeSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoNbrRetireesAfterSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyNbrRetireesDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : NbrVendues ===

				public const string NbrVendues_PROPERTYNAME = "NbrVendues";

				private System.Int32 _NbrVendues;
				///<summary>
				/// Propriété : NbrVendues
				///</summary>
				public System.Int32 NbrVendues
				{
					get
					{
						return GetValue<System.Int32>(CommandDashBoardEncapsulation_NbrVendues_GetValue);
					}
					set
					{
						SetValue<System.Int32>(CommandDashBoardEncapsulation_NbrVendues_GetValue, CommandDashBoardEncapsulation_NbrVendues_SetValue, value, NbrVendues_PROPERTYNAME,  DoNbrVenduesBeforeSet, DoNbrVenduesAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoNbrVenduesBeforeSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoNbrVenduesAfterSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyNbrVenduesDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux encapsulations ===


			#region === Encapsulation : CommandDashBoardEncapsulation ===

				// Champ pour l'encapsulation.
				// Les propriétés implémentées font directement référence à ce champ.
				private MyRestauApp.Common.DAOServices.Models.CommandeDashboard _CommandDashBoardEncapsulation;

				///<summary>
				/// Encapsulation : CommandDashBoardEncapsulation.
				///</summary>
				public MyRestauApp.Common.DAOServices.Models.CommandeDashboard GetCommandDashBoardEncapsulation()
				{
					return _CommandDashBoardEncapsulation;
				}

				///<summary>
				/// Encapsulation : CommandDashBoardEncapsulation.
				///</summary>
				public CommandDashboardViewModel SetCommandDashBoardEncapsulation(MyRestauApp.Common.DAOServices.Models.CommandeDashboard p_Value)
				{
					// En vue d'éventuelles notification (si initialisé), nous récupérons les anciennes valeurs (valeurs actuelles).
					var l_OldValueNbrRestantes = _CommandDashBoardEncapsulation == null || !IsInitialized ? default(System.Int32) : NbrRestantes;
					var l_OldValueNbrRetirees = _CommandDashBoardEncapsulation == null || !IsInitialized ? default(System.Int32) : NbrRetirees;
					var l_OldValueNbrVendues = _CommandDashBoardEncapsulation == null || !IsInitialized ? default(System.Int32) : NbrVendues;

					// Affectation du nouvel object d'encapsulation.
					_CommandDashBoardEncapsulation = p_Value;

					// Si pas initialisé, inutile d'aller plus loin.
					if(!IsInitialized) return this as CommandDashboardViewModel;

					// Nous allons notifier, nous avons besoin des valeurs actuelles.
					var l_NewValueNbrRestantes = _CommandDashBoardEncapsulation == null ? default(System.Int32) : NbrRestantes;
					var l_NewValueNbrRetirees = _CommandDashBoardEncapsulation == null ? default(System.Int32) : NbrRetirees;
					var l_NewValueNbrVendues = _CommandDashBoardEncapsulation == null ? default(System.Int32) : NbrVendues;

					// Nous notifions.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(NbrRestantes_PROPERTYNAME, l_OldValueNbrRestantes, l_NewValueNbrRestantes));
					NotifyNbrRestantesDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(NbrRetirees_PROPERTYNAME, l_OldValueNbrRetirees, l_NewValueNbrRetirees));
					NotifyNbrRetireesDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(NbrVendues_PROPERTYNAME, l_OldValueNbrVendues, l_NewValueNbrVendues));
					NotifyNbrVenduesDependencies();

					return this as CommandDashboardViewModel;
				}

				#region === Propriété : NbrRestantes ===

					///<summary>
					/// Méthode utilisée par la propriété NbrRestantes pour la lecture de la valeur corresponsande de CommandDashBoardEncapsulation.
					///</summary>
					private System.Int32 CommandDashBoardEncapsulation_NbrRestantes_GetValue()
					{
						if(_CommandDashBoardEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur CommandDashBoardEncapsulation.", NbrRestantes_PROPERTYNAME));
						return _CommandDashBoardEncapsulation.NbrRestantes;
					}

					///<summary>
					/// Méthode utilisée par la propriété NbrRestantes pour l'affectation de la valeur corresponsande de CommandDashBoardEncapsulation.
					///</summary>
					private void CommandDashBoardEncapsulation_NbrRestantes_SetValue(System.Int32 p_Value)
					{
						if(_CommandDashBoardEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur CommandDashBoardEncapsulation.", NbrRestantes_PROPERTYNAME));
						_CommandDashBoardEncapsulation.NbrRestantes = p_Value;
					}


				#endregion

				#region === Propriété : NbrRetirees ===

					///<summary>
					/// Méthode utilisée par la propriété NbrRetirees pour la lecture de la valeur corresponsande de CommandDashBoardEncapsulation.
					///</summary>
					private System.Int32 CommandDashBoardEncapsulation_NbrRetirees_GetValue()
					{
						if(_CommandDashBoardEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur CommandDashBoardEncapsulation.", NbrRetirees_PROPERTYNAME));
						return _CommandDashBoardEncapsulation.NbrRetirees;
					}

					///<summary>
					/// Méthode utilisée par la propriété NbrRetirees pour l'affectation de la valeur corresponsande de CommandDashBoardEncapsulation.
					///</summary>
					private void CommandDashBoardEncapsulation_NbrRetirees_SetValue(System.Int32 p_Value)
					{
						if(_CommandDashBoardEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur CommandDashBoardEncapsulation.", NbrRetirees_PROPERTYNAME));
						_CommandDashBoardEncapsulation.NbrRetirees = p_Value;
					}


				#endregion

				#region === Propriété : NbrVendues ===

					///<summary>
					/// Méthode utilisée par la propriété NbrVendues pour la lecture de la valeur corresponsande de CommandDashBoardEncapsulation.
					///</summary>
					private System.Int32 CommandDashBoardEncapsulation_NbrVendues_GetValue()
					{
						if(_CommandDashBoardEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur CommandDashBoardEncapsulation.", NbrVendues_PROPERTYNAME));
						return _CommandDashBoardEncapsulation.NbrVendues;
					}

					///<summary>
					/// Méthode utilisée par la propriété NbrVendues pour l'affectation de la valeur corresponsande de CommandDashBoardEncapsulation.
					///</summary>
					private void CommandDashBoardEncapsulation_NbrVendues_SetValue(System.Int32 p_Value)
					{
						if(_CommandDashBoardEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur CommandDashBoardEncapsulation.", NbrVendues_PROPERTYNAME));
						_CommandDashBoardEncapsulation.NbrVendues = p_Value;
					}


				#endregion


			#endregion

		#endregion
	}

	public partial class CommandDashboardViewModel : CommandDashboardViewModelBase
	{
	}
}
