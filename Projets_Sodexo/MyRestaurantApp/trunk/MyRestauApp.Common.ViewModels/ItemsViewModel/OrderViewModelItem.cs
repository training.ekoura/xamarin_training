﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML OrderViewModelItem
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  Sodexo.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;

namespace MyRestauApp.Common.ViewModels
{
	public abstract partial class OrderViewModelItemBase : Sodexo.ViewModel
	{
		#region === Propriétés ===

			#region === Propriété : Id ===

				public const string Id_PROPERTYNAME = "Id";

				private System.Int32 _Id;
				///<summary>
				/// Propriété : Id
				///</summary>
				public System.Int32 Id
				{
					get
					{
						return GetValue<System.Int32>(OrderEncapsulation_Id_GetValue);
					}
					set
					{
						SetValue<System.Int32>(OrderEncapsulation_Id_GetValue, OrderEncapsulation_Id_SetValue, value, Id_PROPERTYNAME,  DoIdBeforeSet, DoIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIdBeforeSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIdAfterSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DateCreation ===

				public const string DateCreation_PROPERTYNAME = "DateCreation";

				private System.String _DateCreation;
				///<summary>
				/// Propriété : DateCreation
				///</summary>
				public System.String DateCreation
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_DateCreation_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_DateCreation_GetValue, OrderEncapsulation_DateCreation_SetValue, value, DateCreation_PROPERTYNAME,  DoDateCreationBeforeSet, DoDateCreationAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDateCreationBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDateCreationAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDateCreationDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DateCreationFormat ===

				public const string DateCreationFormat_PROPERTYNAME = "DateCreationFormat";

				private System.String _DateCreationFormat;
				///<summary>
				/// Propriété : DateCreationFormat
				///</summary>
				public System.String DateCreationFormat
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_DateCreationFormat_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_DateCreationFormat_GetValue, OrderEncapsulation_DateCreationFormat_SetValue, value, DateCreationFormat_PROPERTYNAME,  DoDateCreationFormatBeforeSet, DoDateCreationFormatAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDateCreationFormatBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDateCreationFormatAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDateCreationFormatDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DateEffective ===

				public const string DateEffective_PROPERTYNAME = "DateEffective";

				private System.String _DateEffective;
				///<summary>
				/// Propriété : DateEffective
				///</summary>
				public System.String DateEffective
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_DateEffective_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_DateEffective_GetValue, OrderEncapsulation_DateEffective_SetValue, value, DateEffective_PROPERTYNAME,  DoDateEffectiveBeforeSet, DoDateEffectiveAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDateEffectiveBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDateEffectiveAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDateEffectiveDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DateEffectiveFormat ===

				public const string DateEffectiveFormat_PROPERTYNAME = "DateEffectiveFormat";

				private System.String _DateEffectiveFormat;
				///<summary>
				/// Propriété : DateEffectiveFormat
				///</summary>
				public System.String DateEffectiveFormat
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_DateEffectiveFormat_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_DateEffectiveFormat_GetValue, OrderEncapsulation_DateEffectiveFormat_SetValue, value, DateEffectiveFormat_PROPERTYNAME,  DoDateEffectiveFormatBeforeSet, DoDateEffectiveFormatAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDateEffectiveFormatBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDateEffectiveFormatAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDateEffectiveFormatDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : PrixTotalTTC ===

				public const string PrixTotalTTC_PROPERTYNAME = "PrixTotalTTC";

				private System.Double _PrixTotalTTC;
				///<summary>
				/// Propriété : PrixTotalTTC
				///</summary>
				public System.Double PrixTotalTTC
				{
					get
					{
						return GetValue<System.Double>(OrderEncapsulation_PrixTotalTTC_GetValue);
					}
					set
					{
						SetValue<System.Double>(OrderEncapsulation_PrixTotalTTC_GetValue, OrderEncapsulation_PrixTotalTTC_SetValue, value, PrixTotalTTC_PROPERTYNAME,  DoPrixTotalTTCBeforeSet, DoPrixTotalTTCAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoPrixTotalTTCBeforeSet(string p_PropertyName, System.Double p_OldValue, System.Double p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoPrixTotalTTCAfterSet(string p_PropertyName, System.Double p_OldValue, System.Double p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyPrixTotalTTCDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : PrixTotalTTCFormat ===

				public const string PrixTotalTTCFormat_PROPERTYNAME = "PrixTotalTTCFormat";

				private System.String _PrixTotalTTCFormat;
				///<summary>
				/// Propriété : PrixTotalTTCFormat
				///</summary>
				public System.String PrixTotalTTCFormat
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_PrixTotalTTCFormat_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_PrixTotalTTCFormat_GetValue, OrderEncapsulation_PrixTotalTTCFormat_SetValue, value, PrixTotalTTCFormat_PROPERTYNAME,  DoPrixTotalTTCFormatBeforeSet, DoPrixTotalTTCFormatAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoPrixTotalTTCFormatBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoPrixTotalTTCFormatAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyPrixTotalTTCFormatDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : MoyenPaiementLibelle ===

				public const string MoyenPaiementLibelle_PROPERTYNAME = "MoyenPaiementLibelle";

				private System.String _MoyenPaiementLibelle;
				///<summary>
				/// Propriété : MoyenPaiementLibelle
				///</summary>
				public System.String MoyenPaiementLibelle
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_MoyenPaiementLibelle_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_MoyenPaiementLibelle_GetValue, OrderEncapsulation_MoyenPaiementLibelle_SetValue, value, MoyenPaiementLibelle_PROPERTYNAME,  DoMoyenPaiementLibelleBeforeSet, DoMoyenPaiementLibelleAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoMoyenPaiementLibelleBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoMoyenPaiementLibelleAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyMoyenPaiementLibelleDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : MoyenPaiementLogicValue ===

				public const string MoyenPaiementLogicValue_PROPERTYNAME = "MoyenPaiementLogicValue";

				private System.String _MoyenPaiementLogicValue;
				///<summary>
				/// Propriété : MoyenPaiementLogicValue
				///</summary>
				public System.String MoyenPaiementLogicValue
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_MoyenPaiementLogicValue_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_MoyenPaiementLogicValue_GetValue, OrderEncapsulation_MoyenPaiementLogicValue_SetValue, value, MoyenPaiementLogicValue_PROPERTYNAME,  DoMoyenPaiementLogicValueBeforeSet, DoMoyenPaiementLogicValueAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoMoyenPaiementLogicValueBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoMoyenPaiementLogicValueAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyMoyenPaiementLogicValueDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsAnnulable ===

				public const string IsAnnulable_PROPERTYNAME = "IsAnnulable";

				private System.Boolean _IsAnnulable;
				///<summary>
				/// Propriété : IsAnnulable
				///</summary>
				public System.Boolean IsAnnulable
				{
					get
					{
						return GetValue<System.Boolean>(OrderEncapsulation_IsAnnulable_GetValue);
					}
					set
					{
						SetValue<System.Boolean>(OrderEncapsulation_IsAnnulable_GetValue, OrderEncapsulation_IsAnnulable_SetValue, value, IsAnnulable_PROPERTYNAME,  DoIsAnnulableBeforeSet, DoIsAnnulableAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsAnnulableBeforeSet(string p_PropertyName, System.Boolean p_OldValue, System.Boolean p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsAnnulableAfterSet(string p_PropertyName, System.Boolean p_OldValue, System.Boolean p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsAnnulableDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ListStatutsPossibles ===

				public const string ListStatutsPossibles_PROPERTYNAME = "ListStatutsPossibles";

				private System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.StatutCommande> _ListStatutsPossibles;
				///<summary>
				/// Propriété : ListStatutsPossibles
				///</summary>
				public System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.StatutCommande> ListStatutsPossibles
				{
					get
					{
						return GetValue<System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.StatutCommande>>(OrderEncapsulation_ListStatutsPossibles_GetValue);
					}
					set
					{
						SetValue<System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.StatutCommande>>(OrderEncapsulation_ListStatutsPossibles_GetValue, OrderEncapsulation_ListStatutsPossibles_SetValue, value, ListStatutsPossibles_PROPERTYNAME,  DoListStatutsPossiblesBeforeSet, DoListStatutsPossiblesAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoListStatutsPossiblesBeforeSet(string p_PropertyName, System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.StatutCommande> p_OldValue, System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.StatutCommande> p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoListStatutsPossiblesAfterSet(string p_PropertyName, System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.StatutCommande> p_OldValue, System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.StatutCommande> p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyListStatutsPossiblesDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Statut ===

				public const string Statut_PROPERTYNAME = "Statut";

				private MyRestauApp.Common.DAOServices.Models.StatutCommande _Statut;
				///<summary>
				/// Propriété : Statut
				///</summary>
				public MyRestauApp.Common.DAOServices.Models.StatutCommande Statut
				{
					get
					{
						return GetValue<MyRestauApp.Common.DAOServices.Models.StatutCommande>(OrderEncapsulation_Statut_GetValue);
					}
					set
					{
						SetValue<MyRestauApp.Common.DAOServices.Models.StatutCommande>(OrderEncapsulation_Statut_GetValue, OrderEncapsulation_Statut_SetValue, value, Statut_PROPERTYNAME,  DoStatutBeforeSet, DoStatutAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoStatutBeforeSet(string p_PropertyName, MyRestauApp.Common.DAOServices.Models.StatutCommande p_OldValue, MyRestauApp.Common.DAOServices.Models.StatutCommande p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoStatutAfterSet(string p_PropertyName, MyRestauApp.Common.DAOServices.Models.StatutCommande p_OldValue, MyRestauApp.Common.DAOServices.Models.StatutCommande p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyStatutDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : StatutCourantDate ===

				public const string StatutCourantDate_PROPERTYNAME = "StatutCourantDate";

				private System.String _StatutCourantDate;
				///<summary>
				/// Propriété : StatutCourantDate
				///</summary>
				public System.String StatutCourantDate
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_StatutCourantDate_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_StatutCourantDate_GetValue, OrderEncapsulation_StatutCourantDate_SetValue, value, StatutCourantDate_PROPERTYNAME,  DoStatutCourantDateBeforeSet, DoStatutCourantDateAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoStatutCourantDateBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoStatutCourantDateAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyStatutCourantDateDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : StatutCourantDateFormat ===

				public const string StatutCourantDateFormat_PROPERTYNAME = "StatutCourantDateFormat";

				private System.String _StatutCourantDateFormat;
				///<summary>
				/// Propriété : StatutCourantDateFormat
				///</summary>
				public System.String StatutCourantDateFormat
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_StatutCourantDateFormat_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_StatutCourantDateFormat_GetValue, OrderEncapsulation_StatutCourantDateFormat_SetValue, value, StatutCourantDateFormat_PROPERTYNAME,  DoStatutCourantDateFormatBeforeSet, DoStatutCourantDateFormatAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoStatutCourantDateFormatBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoStatutCourantDateFormatAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyStatutCourantDateFormatDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ClientId ===

				public const string ClientId_PROPERTYNAME = "ClientId";

				private System.Int32 _ClientId;
				///<summary>
				/// Propriété : ClientId
				///</summary>
				public System.Int32 ClientId
				{
					get
					{
						return GetValue<System.Int32>(OrderEncapsulation_ClientId_GetValue);
					}
					set
					{
						SetValue<System.Int32>(OrderEncapsulation_ClientId_GetValue, OrderEncapsulation_ClientId_SetValue, value, ClientId_PROPERTYNAME,  DoClientIdBeforeSet, DoClientIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoClientIdBeforeSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoClientIdAfterSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyClientIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ZoneRestaurationId ===

				public const string ZoneRestaurationId_PROPERTYNAME = "ZoneRestaurationId";

				private System.String _ZoneRestaurationId;
				///<summary>
				/// Propriété : ZoneRestaurationId
				///</summary>
				public System.String ZoneRestaurationId
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_ZoneRestaurationId_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_ZoneRestaurationId_GetValue, OrderEncapsulation_ZoneRestaurationId_SetValue, value, ZoneRestaurationId_PROPERTYNAME,  DoZoneRestaurationIdBeforeSet, DoZoneRestaurationIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoZoneRestaurationIdBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoZoneRestaurationIdAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyZoneRestaurationIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ZoneRestaurationNom ===

				public const string ZoneRestaurationNom_PROPERTYNAME = "ZoneRestaurationNom";

				private System.String _ZoneRestaurationNom;
				///<summary>
				/// Propriété : ZoneRestaurationNom
				///</summary>
				public System.String ZoneRestaurationNom
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_ZoneRestaurationNom_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_ZoneRestaurationNom_GetValue, OrderEncapsulation_ZoneRestaurationNom_SetValue, value, ZoneRestaurationNom_PROPERTYNAME,  DoZoneRestaurationNomBeforeSet, DoZoneRestaurationNomAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoZoneRestaurationNomBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoZoneRestaurationNomAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyZoneRestaurationNomDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ArticlesCount ===

				public const string ArticlesCount_PROPERTYNAME = "ArticlesCount";

				private System.Int32 _ArticlesCount;
				///<summary>
				/// Propriété : ArticlesCount
				///</summary>
				public System.Int32 ArticlesCount
				{
					get
					{
						return GetValue<System.Int32>(OrderEncapsulation_ArticlesCount_GetValue);
					}
					set
					{
						SetValue<System.Int32>(OrderEncapsulation_ArticlesCount_GetValue, OrderEncapsulation_ArticlesCount_SetValue, value, ArticlesCount_PROPERTYNAME,  DoArticlesCountBeforeSet, DoArticlesCountAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoArticlesCountBeforeSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoArticlesCountAfterSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyArticlesCountDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ArticlesPlatsCount ===

				public const string ArticlesPlatsCount_PROPERTYNAME = "ArticlesPlatsCount";

				private System.Int32 _ArticlesPlatsCount;
				///<summary>
				/// Propriété : ArticlesPlatsCount
				///</summary>
				public System.Int32 ArticlesPlatsCount
				{
					get
					{
						return GetValue<System.Int32>(OrderEncapsulation_ArticlesPlatsCount_GetValue);
					}
					set
					{
						SetValue<System.Int32>(OrderEncapsulation_ArticlesPlatsCount_GetValue, OrderEncapsulation_ArticlesPlatsCount_SetValue, value, ArticlesPlatsCount_PROPERTYNAME,  DoArticlesPlatsCountBeforeSet, DoArticlesPlatsCountAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoArticlesPlatsCountBeforeSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoArticlesPlatsCountAfterSet(string p_PropertyName, System.Int32 p_OldValue, System.Int32 p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyArticlesPlatsCountDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UserPortail ===

				public const string UserPortail_PROPERTYNAME = "UserPortail";

				private MyRestauApp.Common.DAOServices.Models.Convive _UserPortail;
				///<summary>
				/// Propriété : UserPortail
				///</summary>
				public MyRestauApp.Common.DAOServices.Models.Convive UserPortail
				{
					get
					{
						return GetValue<MyRestauApp.Common.DAOServices.Models.Convive>(OrderEncapsulation_UserPortail_GetValue);
					}
					set
					{
						SetValue<MyRestauApp.Common.DAOServices.Models.Convive>(OrderEncapsulation_UserPortail_GetValue, OrderEncapsulation_UserPortail_SetValue, value, UserPortail_PROPERTYNAME,  DoUserPortailBeforeSet, DoUserPortailAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUserPortailBeforeSet(string p_PropertyName, MyRestauApp.Common.DAOServices.Models.Convive p_OldValue, MyRestauApp.Common.DAOServices.Models.Convive p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUserPortailAfterSet(string p_PropertyName, MyRestauApp.Common.DAOServices.Models.Convive p_OldValue, MyRestauApp.Common.DAOServices.Models.Convive p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUserPortailDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ArticlesByComposanteRecettes ===

				public const string ArticlesByComposanteRecettes_PROPERTYNAME = "ArticlesByComposanteRecettes";

				private System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.ComposanteRecette> _ArticlesByComposanteRecettes;
				///<summary>
				/// Propriété : ArticlesByComposanteRecettes
				///</summary>
				public System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.ComposanteRecette> ArticlesByComposanteRecettes
				{
					get
					{
						return GetValue<System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.ComposanteRecette>>(OrderEncapsulation_ArticlesByComposanteRecettes_GetValue);
					}
					set
					{
						SetValue<System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.ComposanteRecette>>(OrderEncapsulation_ArticlesByComposanteRecettes_GetValue, OrderEncapsulation_ArticlesByComposanteRecettes_SetValue, value, ArticlesByComposanteRecettes_PROPERTYNAME,  DoArticlesByComposanteRecettesBeforeSet, DoArticlesByComposanteRecettesAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoArticlesByComposanteRecettesBeforeSet(string p_PropertyName, System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.ComposanteRecette> p_OldValue, System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.ComposanteRecette> p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoArticlesByComposanteRecettesAfterSet(string p_PropertyName, System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.ComposanteRecette> p_OldValue, System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.ComposanteRecette> p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyArticlesByComposanteRecettesDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FormattedDateForView ===

				public const string FormattedDateForView_PROPERTYNAME = "FormattedDateForView";

				private System.String _FormattedDateForView;
				///<summary>
				/// Propriété : FormattedDateForView
				///</summary>
				public System.String FormattedDateForView
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_FormattedDateForView_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_FormattedDateForView_GetValue, OrderEncapsulation_FormattedDateForView_SetValue, value, FormattedDateForView_PROPERTYNAME,  DoFormattedDateForViewBeforeSet, DoFormattedDateForViewAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFormattedDateForViewBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFormattedDateForViewAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFormattedDateForViewDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FormattedLabelForView ===

				public const string FormattedLabelForView_PROPERTYNAME = "FormattedLabelForView";

				private System.String _FormattedLabelForView;
				///<summary>
				/// Propriété : FormattedLabelForView
				///</summary>
				public System.String FormattedLabelForView
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_FormattedLabelForView_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_FormattedLabelForView_GetValue, OrderEncapsulation_FormattedLabelForView_SetValue, value, FormattedLabelForView_PROPERTYNAME,  DoFormattedLabelForViewBeforeSet, DoFormattedLabelForViewAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFormattedLabelForViewBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFormattedLabelForViewAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFormattedLabelForViewDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FormattedLabelPlat ===

				public const string FormattedLabelPlat_PROPERTYNAME = "FormattedLabelPlat";

				private System.String _FormattedLabelPlat;
				///<summary>
				/// Propriété : FormattedLabelPlat
				///</summary>
				public System.String FormattedLabelPlat
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_FormattedLabelPlat_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_FormattedLabelPlat_GetValue, OrderEncapsulation_FormattedLabelPlat_SetValue, value, FormattedLabelPlat_PROPERTYNAME,  DoFormattedLabelPlatBeforeSet, DoFormattedLabelPlatAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFormattedLabelPlatBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFormattedLabelPlatAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFormattedLabelPlatDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FormattedLabelProduit ===

				public const string FormattedLabelProduit_PROPERTYNAME = "FormattedLabelProduit";

				private System.String _FormattedLabelProduit;
				///<summary>
				/// Propriété : FormattedLabelProduit
				///</summary>
				public System.String FormattedLabelProduit
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_FormattedLabelProduit_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_FormattedLabelProduit_GetValue, OrderEncapsulation_FormattedLabelProduit_SetValue, value, FormattedLabelProduit_PROPERTYNAME,  DoFormattedLabelProduitBeforeSet, DoFormattedLabelProduitAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFormattedLabelProduitBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFormattedLabelProduitAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFormattedLabelProduitDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FormattedLabelFirstLastName ===

				public const string FormattedLabelFirstLastName_PROPERTYNAME = "FormattedLabelFirstLastName";

				private System.String _FormattedLabelFirstLastName;
				///<summary>
				/// Propriété : FormattedLabelFirstLastName
				///</summary>
				public System.String FormattedLabelFirstLastName
				{
					get
					{
						return GetValue<System.String>(OrderEncapsulation_FormattedLabelFirstLastName_GetValue);
					}
					set
					{
						SetValue<System.String>(OrderEncapsulation_FormattedLabelFirstLastName_GetValue, OrderEncapsulation_FormattedLabelFirstLastName_SetValue, value, FormattedLabelFirstLastName_PROPERTYNAME,  DoFormattedLabelFirstLastNameBeforeSet, DoFormattedLabelFirstLastNameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFormattedLabelFirstLastNameBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFormattedLabelFirstLastNameAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFormattedLabelFirstLastNameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsNew ===

				public const string IsNew_PROPERTYNAME = "IsNew";

				private System.Boolean _IsNew;
				///<summary>
				/// Propriété : IsNew
				///</summary>
				public System.Boolean IsNew
				{
					get
					{
						return GetValue<System.Boolean>(OrderEncapsulation_IsNew_GetValue);
					}
					set
					{
						SetValue<System.Boolean>(OrderEncapsulation_IsNew_GetValue, OrderEncapsulation_IsNew_SetValue, value, IsNew_PROPERTYNAME,  DoIsNewBeforeSet, DoIsNewAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsNewBeforeSet(string p_PropertyName, System.Boolean p_OldValue, System.Boolean p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsNewAfterSet(string p_PropertyName, System.Boolean p_OldValue, System.Boolean p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsNewDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux encapsulations ===


			#region === Encapsulation : OrderEncapsulation ===

				// Champ pour l'encapsulation.
				// Les propriétés implémentées font directement référence à ce champ.
				private MyRestauApp.Common.DAOServices.Models.Commande _OrderEncapsulation;

				///<summary>
				/// Encapsulation : OrderEncapsulation.
				///</summary>
				public MyRestauApp.Common.DAOServices.Models.Commande GetOrderEncapsulation()
				{
					return _OrderEncapsulation;
				}

				///<summary>
				/// Encapsulation : OrderEncapsulation.
				///</summary>
				public OrderViewModelItem SetOrderEncapsulation(MyRestauApp.Common.DAOServices.Models.Commande p_Value)
				{
					// En vue d'éventuelles notification (si initialisé), nous récupérons les anciennes valeurs (valeurs actuelles).
					var l_OldValueId = _OrderEncapsulation == null || !IsInitialized ? default(System.Int32) : Id;
					var l_OldValueDateCreation = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : DateCreation;
					var l_OldValueDateCreationFormat = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : DateCreationFormat;
					var l_OldValueDateEffective = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : DateEffective;
					var l_OldValueDateEffectiveFormat = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : DateEffectiveFormat;
					var l_OldValuePrixTotalTTC = _OrderEncapsulation == null || !IsInitialized ? default(System.Double) : PrixTotalTTC;
					var l_OldValuePrixTotalTTCFormat = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : PrixTotalTTCFormat;
					var l_OldValueMoyenPaiementLibelle = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : MoyenPaiementLibelle;
					var l_OldValueMoyenPaiementLogicValue = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : MoyenPaiementLogicValue;
					var l_OldValueIsAnnulable = _OrderEncapsulation == null || !IsInitialized ? default(System.Boolean) : IsAnnulable;
					var l_OldValueListStatutsPossibles = _OrderEncapsulation == null || !IsInitialized ? default(System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.StatutCommande>) : ListStatutsPossibles;
					var l_OldValueStatut = _OrderEncapsulation == null || !IsInitialized ? default(MyRestauApp.Common.DAOServices.Models.StatutCommande) : Statut;
					var l_OldValueStatutCourantDate = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : StatutCourantDate;
					var l_OldValueStatutCourantDateFormat = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : StatutCourantDateFormat;
					var l_OldValueClientId = _OrderEncapsulation == null || !IsInitialized ? default(System.Int32) : ClientId;
					var l_OldValueZoneRestaurationId = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : ZoneRestaurationId;
					var l_OldValueZoneRestaurationNom = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : ZoneRestaurationNom;
					var l_OldValueArticlesCount = _OrderEncapsulation == null || !IsInitialized ? default(System.Int32) : ArticlesCount;
					var l_OldValueArticlesPlatsCount = _OrderEncapsulation == null || !IsInitialized ? default(System.Int32) : ArticlesPlatsCount;
					var l_OldValueUserPortail = _OrderEncapsulation == null || !IsInitialized ? default(MyRestauApp.Common.DAOServices.Models.Convive) : UserPortail;
					var l_OldValueArticlesByComposanteRecettes = _OrderEncapsulation == null || !IsInitialized ? default(System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.ComposanteRecette>) : ArticlesByComposanteRecettes;
					var l_OldValueFormattedDateForView = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : FormattedDateForView;
					var l_OldValueFormattedLabelForView = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : FormattedLabelForView;
					var l_OldValueFormattedLabelPlat = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : FormattedLabelPlat;
					var l_OldValueFormattedLabelProduit = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : FormattedLabelProduit;
					var l_OldValueFormattedLabelFirstLastName = _OrderEncapsulation == null || !IsInitialized ? default(System.String) : FormattedLabelFirstLastName;
					var l_OldValueIsNew = _OrderEncapsulation == null || !IsInitialized ? default(System.Boolean) : IsNew;

					// Affectation du nouvel object d'encapsulation.
					_OrderEncapsulation = p_Value;

					// Si pas initialisé, inutile d'aller plus loin.
					if(!IsInitialized) return this as OrderViewModelItem;

					// Nous allons notifier, nous avons besoin des valeurs actuelles.
					var l_NewValueId = _OrderEncapsulation == null ? default(System.Int32) : Id;
					var l_NewValueDateCreation = _OrderEncapsulation == null ? default(System.String) : DateCreation;
					var l_NewValueDateCreationFormat = _OrderEncapsulation == null ? default(System.String) : DateCreationFormat;
					var l_NewValueDateEffective = _OrderEncapsulation == null ? default(System.String) : DateEffective;
					var l_NewValueDateEffectiveFormat = _OrderEncapsulation == null ? default(System.String) : DateEffectiveFormat;
					var l_NewValuePrixTotalTTC = _OrderEncapsulation == null ? default(System.Double) : PrixTotalTTC;
					var l_NewValuePrixTotalTTCFormat = _OrderEncapsulation == null ? default(System.String) : PrixTotalTTCFormat;
					var l_NewValueMoyenPaiementLibelle = _OrderEncapsulation == null ? default(System.String) : MoyenPaiementLibelle;
					var l_NewValueMoyenPaiementLogicValue = _OrderEncapsulation == null ? default(System.String) : MoyenPaiementLogicValue;
					var l_NewValueIsAnnulable = _OrderEncapsulation == null ? default(System.Boolean) : IsAnnulable;
					var l_NewValueListStatutsPossibles = _OrderEncapsulation == null ? default(System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.StatutCommande>) : ListStatutsPossibles;
					var l_NewValueStatut = _OrderEncapsulation == null ? default(MyRestauApp.Common.DAOServices.Models.StatutCommande) : Statut;
					var l_NewValueStatutCourantDate = _OrderEncapsulation == null ? default(System.String) : StatutCourantDate;
					var l_NewValueStatutCourantDateFormat = _OrderEncapsulation == null ? default(System.String) : StatutCourantDateFormat;
					var l_NewValueClientId = _OrderEncapsulation == null ? default(System.Int32) : ClientId;
					var l_NewValueZoneRestaurationId = _OrderEncapsulation == null ? default(System.String) : ZoneRestaurationId;
					var l_NewValueZoneRestaurationNom = _OrderEncapsulation == null ? default(System.String) : ZoneRestaurationNom;
					var l_NewValueArticlesCount = _OrderEncapsulation == null ? default(System.Int32) : ArticlesCount;
					var l_NewValueArticlesPlatsCount = _OrderEncapsulation == null ? default(System.Int32) : ArticlesPlatsCount;
					var l_NewValueUserPortail = _OrderEncapsulation == null ? default(MyRestauApp.Common.DAOServices.Models.Convive) : UserPortail;
					var l_NewValueArticlesByComposanteRecettes = _OrderEncapsulation == null ? default(System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.ComposanteRecette>) : ArticlesByComposanteRecettes;
					var l_NewValueFormattedDateForView = _OrderEncapsulation == null ? default(System.String) : FormattedDateForView;
					var l_NewValueFormattedLabelForView = _OrderEncapsulation == null ? default(System.String) : FormattedLabelForView;
					var l_NewValueFormattedLabelPlat = _OrderEncapsulation == null ? default(System.String) : FormattedLabelPlat;
					var l_NewValueFormattedLabelProduit = _OrderEncapsulation == null ? default(System.String) : FormattedLabelProduit;
					var l_NewValueFormattedLabelFirstLastName = _OrderEncapsulation == null ? default(System.String) : FormattedLabelFirstLastName;
					var l_NewValueIsNew = _OrderEncapsulation == null ? default(System.Boolean) : IsNew;

					// Nous notifions.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Id_PROPERTYNAME, l_OldValueId, l_NewValueId));
					NotifyIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(DateCreation_PROPERTYNAME, l_OldValueDateCreation, l_NewValueDateCreation));
					NotifyDateCreationDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(DateCreationFormat_PROPERTYNAME, l_OldValueDateCreationFormat, l_NewValueDateCreationFormat));
					NotifyDateCreationFormatDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(DateEffective_PROPERTYNAME, l_OldValueDateEffective, l_NewValueDateEffective));
					NotifyDateEffectiveDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(DateEffectiveFormat_PROPERTYNAME, l_OldValueDateEffectiveFormat, l_NewValueDateEffectiveFormat));
					NotifyDateEffectiveFormatDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(PrixTotalTTC_PROPERTYNAME, l_OldValuePrixTotalTTC, l_NewValuePrixTotalTTC));
					NotifyPrixTotalTTCDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(PrixTotalTTCFormat_PROPERTYNAME, l_OldValuePrixTotalTTCFormat, l_NewValuePrixTotalTTCFormat));
					NotifyPrixTotalTTCFormatDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(MoyenPaiementLibelle_PROPERTYNAME, l_OldValueMoyenPaiementLibelle, l_NewValueMoyenPaiementLibelle));
					NotifyMoyenPaiementLibelleDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(MoyenPaiementLogicValue_PROPERTYNAME, l_OldValueMoyenPaiementLogicValue, l_NewValueMoyenPaiementLogicValue));
					NotifyMoyenPaiementLogicValueDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(IsAnnulable_PROPERTYNAME, l_OldValueIsAnnulable, l_NewValueIsAnnulable));
					NotifyIsAnnulableDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ListStatutsPossibles_PROPERTYNAME, l_OldValueListStatutsPossibles, l_NewValueListStatutsPossibles));
					NotifyListStatutsPossiblesDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(Statut_PROPERTYNAME, l_OldValueStatut, l_NewValueStatut));
					NotifyStatutDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(StatutCourantDate_PROPERTYNAME, l_OldValueStatutCourantDate, l_NewValueStatutCourantDate));
					NotifyStatutCourantDateDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(StatutCourantDateFormat_PROPERTYNAME, l_OldValueStatutCourantDateFormat, l_NewValueStatutCourantDateFormat));
					NotifyStatutCourantDateFormatDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ClientId_PROPERTYNAME, l_OldValueClientId, l_NewValueClientId));
					NotifyClientIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ZoneRestaurationId_PROPERTYNAME, l_OldValueZoneRestaurationId, l_NewValueZoneRestaurationId));
					NotifyZoneRestaurationIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ZoneRestaurationNom_PROPERTYNAME, l_OldValueZoneRestaurationNom, l_NewValueZoneRestaurationNom));
					NotifyZoneRestaurationNomDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ArticlesCount_PROPERTYNAME, l_OldValueArticlesCount, l_NewValueArticlesCount));
					NotifyArticlesCountDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ArticlesPlatsCount_PROPERTYNAME, l_OldValueArticlesPlatsCount, l_NewValueArticlesPlatsCount));
					NotifyArticlesPlatsCountDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(UserPortail_PROPERTYNAME, l_OldValueUserPortail, l_NewValueUserPortail));
					NotifyUserPortailDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ArticlesByComposanteRecettes_PROPERTYNAME, l_OldValueArticlesByComposanteRecettes, l_NewValueArticlesByComposanteRecettes));
					NotifyArticlesByComposanteRecettesDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(FormattedDateForView_PROPERTYNAME, l_OldValueFormattedDateForView, l_NewValueFormattedDateForView));
					NotifyFormattedDateForViewDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(FormattedLabelForView_PROPERTYNAME, l_OldValueFormattedLabelForView, l_NewValueFormattedLabelForView));
					NotifyFormattedLabelForViewDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(FormattedLabelPlat_PROPERTYNAME, l_OldValueFormattedLabelPlat, l_NewValueFormattedLabelPlat));
					NotifyFormattedLabelPlatDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(FormattedLabelProduit_PROPERTYNAME, l_OldValueFormattedLabelProduit, l_NewValueFormattedLabelProduit));
					NotifyFormattedLabelProduitDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(FormattedLabelFirstLastName_PROPERTYNAME, l_OldValueFormattedLabelFirstLastName, l_NewValueFormattedLabelFirstLastName));
					NotifyFormattedLabelFirstLastNameDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(IsNew_PROPERTYNAME, l_OldValueIsNew, l_NewValueIsNew));
					NotifyIsNewDependencies();

					return this as OrderViewModelItem;
				}

				#region === Propriété : Id ===

					///<summary>
					/// Méthode utilisée par la propriété Id pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.Int32 OrderEncapsulation_Id_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", Id_PROPERTYNAME));
						return _OrderEncapsulation.Id;
					}

					///<summary>
					/// Méthode utilisée par la propriété Id pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_Id_SetValue(System.Int32 p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", Id_PROPERTYNAME));
						_OrderEncapsulation.Id = p_Value;
					}


				#endregion

				#region === Propriété : DateCreation ===

					///<summary>
					/// Méthode utilisée par la propriété DateCreation pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_DateCreation_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", DateCreation_PROPERTYNAME));
						return _OrderEncapsulation.DateCreation;
					}

					///<summary>
					/// Méthode utilisée par la propriété DateCreation pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_DateCreation_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", DateCreation_PROPERTYNAME));
						_OrderEncapsulation.DateCreation = p_Value;
					}


				#endregion

				#region === Propriété : DateCreationFormat ===

					///<summary>
					/// Méthode utilisée par la propriété DateCreationFormat pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_DateCreationFormat_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", DateCreationFormat_PROPERTYNAME));
						return _OrderEncapsulation.DateCreationFormat;
					}

					///<summary>
					/// Méthode utilisée par la propriété DateCreationFormat pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_DateCreationFormat_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", DateCreationFormat_PROPERTYNAME));
						_OrderEncapsulation.DateCreationFormat = p_Value;
					}


				#endregion

				#region === Propriété : DateEffective ===

					///<summary>
					/// Méthode utilisée par la propriété DateEffective pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_DateEffective_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", DateEffective_PROPERTYNAME));
						return _OrderEncapsulation.DateEffective;
					}

					///<summary>
					/// Méthode utilisée par la propriété DateEffective pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_DateEffective_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", DateEffective_PROPERTYNAME));
						_OrderEncapsulation.DateEffective = p_Value;
					}


				#endregion

				#region === Propriété : DateEffectiveFormat ===

					///<summary>
					/// Méthode utilisée par la propriété DateEffectiveFormat pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_DateEffectiveFormat_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", DateEffectiveFormat_PROPERTYNAME));
						return _OrderEncapsulation.DateEffectiveFormat;
					}

					///<summary>
					/// Méthode utilisée par la propriété DateEffectiveFormat pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_DateEffectiveFormat_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", DateEffectiveFormat_PROPERTYNAME));
						_OrderEncapsulation.DateEffectiveFormat = p_Value;
					}


				#endregion

				#region === Propriété : PrixTotalTTC ===

					///<summary>
					/// Méthode utilisée par la propriété PrixTotalTTC pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.Double OrderEncapsulation_PrixTotalTTC_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", PrixTotalTTC_PROPERTYNAME));
						return _OrderEncapsulation.PrixTotalTTC;
					}

					///<summary>
					/// Méthode utilisée par la propriété PrixTotalTTC pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_PrixTotalTTC_SetValue(System.Double p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", PrixTotalTTC_PROPERTYNAME));
						_OrderEncapsulation.PrixTotalTTC = p_Value;
					}


				#endregion

				#region === Propriété : PrixTotalTTCFormat ===

					///<summary>
					/// Méthode utilisée par la propriété PrixTotalTTCFormat pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_PrixTotalTTCFormat_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", PrixTotalTTCFormat_PROPERTYNAME));
						return _OrderEncapsulation.PrixTotalTTCFormat;
					}

					///<summary>
					/// Méthode utilisée par la propriété PrixTotalTTCFormat pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_PrixTotalTTCFormat_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", PrixTotalTTCFormat_PROPERTYNAME));
						_OrderEncapsulation.PrixTotalTTCFormat = p_Value;
					}


				#endregion

				#region === Propriété : MoyenPaiementLibelle ===

					///<summary>
					/// Méthode utilisée par la propriété MoyenPaiementLibelle pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_MoyenPaiementLibelle_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", MoyenPaiementLibelle_PROPERTYNAME));
						return _OrderEncapsulation.MoyenPaiementLibelle;
					}

					///<summary>
					/// Méthode utilisée par la propriété MoyenPaiementLibelle pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_MoyenPaiementLibelle_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", MoyenPaiementLibelle_PROPERTYNAME));
						_OrderEncapsulation.MoyenPaiementLibelle = p_Value;
					}


				#endregion

				#region === Propriété : MoyenPaiementLogicValue ===

					///<summary>
					/// Méthode utilisée par la propriété MoyenPaiementLogicValue pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_MoyenPaiementLogicValue_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", MoyenPaiementLogicValue_PROPERTYNAME));
						return _OrderEncapsulation.MoyenPaiementLogicValue;
					}

					///<summary>
					/// Méthode utilisée par la propriété MoyenPaiementLogicValue pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_MoyenPaiementLogicValue_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", MoyenPaiementLogicValue_PROPERTYNAME));
						_OrderEncapsulation.MoyenPaiementLogicValue = p_Value;
					}


				#endregion

				#region === Propriété : IsAnnulable ===

					///<summary>
					/// Méthode utilisée par la propriété IsAnnulable pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.Boolean OrderEncapsulation_IsAnnulable_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", IsAnnulable_PROPERTYNAME));
						return _OrderEncapsulation.IsAnnulable;
					}

					///<summary>
					/// Méthode utilisée par la propriété IsAnnulable pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_IsAnnulable_SetValue(System.Boolean p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", IsAnnulable_PROPERTYNAME));
						_OrderEncapsulation.IsAnnulable = p_Value;
					}


				#endregion

				#region === Propriété : ListStatutsPossibles ===

					///<summary>
					/// Méthode utilisée par la propriété ListStatutsPossibles pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.StatutCommande> OrderEncapsulation_ListStatutsPossibles_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", ListStatutsPossibles_PROPERTYNAME));
						return _OrderEncapsulation.ListStatutsPossibles;
					}

					///<summary>
					/// Méthode utilisée par la propriété ListStatutsPossibles pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_ListStatutsPossibles_SetValue(System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.StatutCommande> p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", ListStatutsPossibles_PROPERTYNAME));
						_OrderEncapsulation.ListStatutsPossibles = p_Value;
					}


				#endregion

				#region === Propriété : Statut ===

					///<summary>
					/// Méthode utilisée par la propriété Statut pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private MyRestauApp.Common.DAOServices.Models.StatutCommande OrderEncapsulation_Statut_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", Statut_PROPERTYNAME));
						return _OrderEncapsulation.Statut;
					}

					///<summary>
					/// Méthode utilisée par la propriété Statut pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_Statut_SetValue(MyRestauApp.Common.DAOServices.Models.StatutCommande p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", Statut_PROPERTYNAME));
						_OrderEncapsulation.Statut = p_Value;
					}


				#endregion

				#region === Propriété : StatutCourantDate ===

					///<summary>
					/// Méthode utilisée par la propriété StatutCourantDate pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_StatutCourantDate_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", StatutCourantDate_PROPERTYNAME));
						return _OrderEncapsulation.StatutCourantDate;
					}

					///<summary>
					/// Méthode utilisée par la propriété StatutCourantDate pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_StatutCourantDate_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", StatutCourantDate_PROPERTYNAME));
						_OrderEncapsulation.StatutCourantDate = p_Value;
					}


				#endregion

				#region === Propriété : StatutCourantDateFormat ===

					///<summary>
					/// Méthode utilisée par la propriété StatutCourantDateFormat pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_StatutCourantDateFormat_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", StatutCourantDateFormat_PROPERTYNAME));
						return _OrderEncapsulation.StatutCourantDateFormat;
					}

					///<summary>
					/// Méthode utilisée par la propriété StatutCourantDateFormat pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_StatutCourantDateFormat_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", StatutCourantDateFormat_PROPERTYNAME));
						_OrderEncapsulation.StatutCourantDateFormat = p_Value;
					}


				#endregion

				#region === Propriété : ClientId ===

					///<summary>
					/// Méthode utilisée par la propriété ClientId pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.Int32 OrderEncapsulation_ClientId_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", ClientId_PROPERTYNAME));
						return _OrderEncapsulation.ClientId;
					}

					///<summary>
					/// Méthode utilisée par la propriété ClientId pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_ClientId_SetValue(System.Int32 p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", ClientId_PROPERTYNAME));
						_OrderEncapsulation.ClientId = p_Value;
					}


				#endregion

				#region === Propriété : ZoneRestaurationId ===

					///<summary>
					/// Méthode utilisée par la propriété ZoneRestaurationId pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_ZoneRestaurationId_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", ZoneRestaurationId_PROPERTYNAME));
						return _OrderEncapsulation.ZoneRestaurationId;
					}

					///<summary>
					/// Méthode utilisée par la propriété ZoneRestaurationId pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_ZoneRestaurationId_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", ZoneRestaurationId_PROPERTYNAME));
						_OrderEncapsulation.ZoneRestaurationId = p_Value;
					}


				#endregion

				#region === Propriété : ZoneRestaurationNom ===

					///<summary>
					/// Méthode utilisée par la propriété ZoneRestaurationNom pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_ZoneRestaurationNom_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", ZoneRestaurationNom_PROPERTYNAME));
						return _OrderEncapsulation.ZoneRestaurationNom;
					}

					///<summary>
					/// Méthode utilisée par la propriété ZoneRestaurationNom pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_ZoneRestaurationNom_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", ZoneRestaurationNom_PROPERTYNAME));
						_OrderEncapsulation.ZoneRestaurationNom = p_Value;
					}


				#endregion

				#region === Propriété : ArticlesCount ===

					///<summary>
					/// Méthode utilisée par la propriété ArticlesCount pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.Int32 OrderEncapsulation_ArticlesCount_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", ArticlesCount_PROPERTYNAME));
						return _OrderEncapsulation.ArticlesCount;
					}

					///<summary>
					/// Méthode utilisée par la propriété ArticlesCount pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_ArticlesCount_SetValue(System.Int32 p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", ArticlesCount_PROPERTYNAME));
						_OrderEncapsulation.ArticlesCount = p_Value;
					}


				#endregion

				#region === Propriété : ArticlesPlatsCount ===

					///<summary>
					/// Méthode utilisée par la propriété ArticlesPlatsCount pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.Int32 OrderEncapsulation_ArticlesPlatsCount_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", ArticlesPlatsCount_PROPERTYNAME));
						return _OrderEncapsulation.ArticlesPlatsCount;
					}

					///<summary>
					/// Méthode utilisée par la propriété ArticlesPlatsCount pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_ArticlesPlatsCount_SetValue(System.Int32 p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", ArticlesPlatsCount_PROPERTYNAME));
						_OrderEncapsulation.ArticlesPlatsCount = p_Value;
					}


				#endregion

				#region === Propriété : UserPortail ===

					///<summary>
					/// Méthode utilisée par la propriété UserPortail pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private MyRestauApp.Common.DAOServices.Models.Convive OrderEncapsulation_UserPortail_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", UserPortail_PROPERTYNAME));
						return _OrderEncapsulation.UserPortail;
					}

					///<summary>
					/// Méthode utilisée par la propriété UserPortail pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_UserPortail_SetValue(MyRestauApp.Common.DAOServices.Models.Convive p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", UserPortail_PROPERTYNAME));
						_OrderEncapsulation.UserPortail = p_Value;
					}


				#endregion

				#region === Propriété : ArticlesByComposanteRecettes ===

					///<summary>
					/// Méthode utilisée par la propriété ArticlesByComposanteRecettes pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.ComposanteRecette> OrderEncapsulation_ArticlesByComposanteRecettes_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", ArticlesByComposanteRecettes_PROPERTYNAME));
						return _OrderEncapsulation.ArticlesByComposanteRecettes;
					}

					///<summary>
					/// Méthode utilisée par la propriété ArticlesByComposanteRecettes pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_ArticlesByComposanteRecettes_SetValue(System.Collections.Generic.List<MyRestauApp.Common.DAOServices.Models.ComposanteRecette> p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", ArticlesByComposanteRecettes_PROPERTYNAME));
						_OrderEncapsulation.ArticlesByComposanteRecettes = p_Value;
					}


				#endregion

				#region === Propriété : FormattedDateForView ===

					///<summary>
					/// Méthode utilisée par la propriété FormattedDateForView pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_FormattedDateForView_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", FormattedDateForView_PROPERTYNAME));
						return _OrderEncapsulation.FormattedDateForView;
					}

					///<summary>
					/// Méthode utilisée par la propriété FormattedDateForView pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_FormattedDateForView_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", FormattedDateForView_PROPERTYNAME));
						_OrderEncapsulation.FormattedDateForView = p_Value;
					}


				#endregion

				#region === Propriété : FormattedLabelForView ===

					///<summary>
					/// Méthode utilisée par la propriété FormattedLabelForView pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_FormattedLabelForView_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", FormattedLabelForView_PROPERTYNAME));
						return _OrderEncapsulation.FormattedLabelForView;
					}

					///<summary>
					/// Méthode utilisée par la propriété FormattedLabelForView pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_FormattedLabelForView_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", FormattedLabelForView_PROPERTYNAME));
						_OrderEncapsulation.FormattedLabelForView = p_Value;
					}


				#endregion

				#region === Propriété : FormattedLabelPlat ===

					///<summary>
					/// Méthode utilisée par la propriété FormattedLabelPlat pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_FormattedLabelPlat_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", FormattedLabelPlat_PROPERTYNAME));
						return _OrderEncapsulation.FormattedLabelPlat;
					}

					///<summary>
					/// Méthode utilisée par la propriété FormattedLabelPlat pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_FormattedLabelPlat_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", FormattedLabelPlat_PROPERTYNAME));
						_OrderEncapsulation.FormattedLabelPlat = p_Value;
					}


				#endregion

				#region === Propriété : FormattedLabelProduit ===

					///<summary>
					/// Méthode utilisée par la propriété FormattedLabelProduit pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_FormattedLabelProduit_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", FormattedLabelProduit_PROPERTYNAME));
						return _OrderEncapsulation.FormattedLabelProduit;
					}

					///<summary>
					/// Méthode utilisée par la propriété FormattedLabelProduit pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_FormattedLabelProduit_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", FormattedLabelProduit_PROPERTYNAME));
						_OrderEncapsulation.FormattedLabelProduit = p_Value;
					}


				#endregion

				#region === Propriété : FormattedLabelFirstLastName ===

					///<summary>
					/// Méthode utilisée par la propriété FormattedLabelFirstLastName pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.String OrderEncapsulation_FormattedLabelFirstLastName_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", FormattedLabelFirstLastName_PROPERTYNAME));
						return _OrderEncapsulation.FormattedLabelFirstLastName;
					}

					///<summary>
					/// Méthode utilisée par la propriété FormattedLabelFirstLastName pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_FormattedLabelFirstLastName_SetValue(System.String p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", FormattedLabelFirstLastName_PROPERTYNAME));
						_OrderEncapsulation.FormattedLabelFirstLastName = p_Value;
					}


				#endregion

				#region === Propriété : IsNew ===

					///<summary>
					/// Méthode utilisée par la propriété IsNew pour la lecture de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private System.Boolean OrderEncapsulation_IsNew_GetValue()
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Get impossible sur OrderEncapsulation.", IsNew_PROPERTYNAME));
						return _OrderEncapsulation.IsNew;
					}

					///<summary>
					/// Méthode utilisée par la propriété IsNew pour l'affectation de la valeur corresponsande de OrderEncapsulation.
					///</summary>
					private void OrderEncapsulation_IsNew_SetValue(System.Boolean p_Value)
					{
						if(_OrderEncapsulation == null) throw new Exception(string.Format("Le model d'encapsulation n'est pas défini. Set impossible sur OrderEncapsulation.", IsNew_PROPERTYNAME));
						_OrderEncapsulation.IsNew = p_Value;
					}


				#endregion


			#endregion

		#endregion
	}

	public partial class OrderViewModelItem : OrderViewModelItemBase
	{
	}
}
