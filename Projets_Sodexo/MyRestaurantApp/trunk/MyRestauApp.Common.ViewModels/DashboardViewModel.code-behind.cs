
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using MyRestauApp.Common.DAOServices;
using MyRestauApp.Common.DAOServices.Models;
using MyRestauApp.Common.DAOServices.Requests;
using MyRestauApp.Common.ViewModels.ItemsViewModel;
using Sodexo;
using MyRestauApp.Common.DAOServices.Services;
using System.Globalization;


namespace MyRestauApp.Common.ViewModels
{
    public partial class DashboardViewModel : DashboardViewModelBase, IDashboardViewModel
    {

        #region Proprietes
        private ICommandeService CommandService;
        private IStockService StockService;
        private IUserService UserService;
        private IGlobalConfigService GlobalConfigService;
        private IDataOfflineService DataOfflineService;
        private bool IsWorkingView = false;
        private CultureInfo Culture;
        DashboardUserByDate DashboardSaved;
        #endregion

        #region Initialisation
        protected override void OnInitialize()
        {
            base.OnInitialize();
            ResolveInitialization();

        }

        private void ResolveInitialization()
        {
            CommandService = Sodexo.Framework.Services.Container().Resolve<ICommandeService>();
            StockService = Sodexo.Framework.Services.Container().Resolve<IStockService>();
            UserService = Sodexo.Framework.Services.Container().Resolve<IUserService>();
            GlobalConfigService = Sodexo.Framework.Services.GlobalConfigService();
            DataOfflineService = Sodexo.Framework.Services.DataOfflineService();
            Culture = new CultureInfo(Properties.Resources.CultureInfoCurrentCultureName.Replace("_", "-"));
        }
        #endregion End Initialisation

        #region Pr�paration VM
        public async Task<bool> PrepareDashboardVM(bool isOffline)
        {
            try
            {
                BeginWork();

                ZoneRestaurationId = string.Empty;
                ResolveInitialization();
                // R�cup�rer la date � afficher dans l'en-t�te
                DateDashBoard = isOffline ? DataOfflineService.Data.CurrentUser.DashboradDataByZR.FirstOrDefault().DashboardUserByDate.FirstOrDefault().DateDebut : DateTime.Now.Date.ToString("dd/MM/yyyy HH:mm:ss", Culture);

                var userResult = await UserService.GetUser(GlobalConfigService.Data.Token.TokenVal);
                if (userResult.Success)
                {
                    if (userResult.Data != null && userResult.Data.Site != null && userResult.Data.Site.FirstOrDefault() != null && userResult.Data.Site.FirstOrDefault().ZonesRestauration != null
                             && userResult.Data.Site.FirstOrDefault().ZonesRestauration.Count > 0)
                    {
                        ZoneRestaurationId = userResult.Data.Site.LastOrDefault().ZonesRestauration.LastOrDefault().Id;
                        DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId).FirstOrDefault().DashboradDataByZR.FirstOrDefault().ZoneRestaurationId = ZoneRestaurationId;
                        DataOfflineService.SaveConfig();
                        return await UpdateDashboardView(DateDashBoard);
                    }
                    else
                    {
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.ErrorGetUser);
                        return false;
                    }
                }
                else
                {
                    var isError = true;
                    if (userResult.ExceptionComesFromNetwork)
                    {
                        ZoneRestaurationId = DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId).FirstOrDefault().DashboradDataByZR.FirstOrDefault().ZoneRestaurationId;
                        isError = !string.IsNullOrEmpty(ZoneRestaurationId) ? false : true;// on a pas de data enregistr� en Offline donc affiche le message d 'erreur
                    }
                    if (isError)
                    {
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, userResult.ExceptionMessage);
                        return false;
                    }
                    else
                        return await UpdateDashboardView(DateDashBoard);
                }
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                return false;
            }
            finally
            {
                EndWork();
            }
        }

        public async Task<bool> UpdateDashboardView(string date = null)
        {
            try
            {
                BeginWork();
                if (date == null)
                    date = DateDashBoard;
                //R�cup�ration de la dashboard data pour le mode offline si besoin
                try
                {
                    //R�cup�rer la commande enregistr� � utiliser en cas de besoin
                    DashboardSaved = DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                              .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).FirstOrDefault().DashboardUserByDate.Where(x => x.DateDebut == date).FirstOrDefault();
                }
                catch
                {
                    DashboardSaved = null;
                }
                //1 ---  Premi�re partie de la vue ---
                var result = await PrepareCommandeDashboard(date);
                //2 --- Seconde partie de la vue ---    
                var success = result;
                if (result)
                    success = await PrepareStock(date);

                if (success)
                {
                    var dateValue = DateTime.ParseExact(date, "dd/MM/yyyy HH:mm:ss", Culture).Date;
                    DateDashBoard = dateValue.ToString("dd/MM/yyyy HH:mm:ss", Culture);
                    var formattedDate = string.Empty;
                    if (dateValue == DateTime.Now.Date)
                        DateDashBoardFormattedLabel = Properties.Resources.ForToday + " - " + DateTime.ParseExact(DateDashBoard, "dd/MM/yyyy HH:mm:ss", Culture).ToString("d MMMM", Culture);
                    else
                        DateDashBoardFormattedLabel = DateTime.ParseExact(DateDashBoard, "dd/MM/yyyy HH:mm:ss", Culture).ToString("dddd d MMMM", Culture);
                    return success;
                }
                return false;
            }
            catch (Exception e)
            {
                //Afficher message d'erreur
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                return false;
            }
            finally
            {
                EndWork();
            }
        }



        public async Task<bool> PrepareCommandeDashboard(string date)
        {
            var requestDashboardGerant = new GetDashboardGerantRequest()
            {
                Token = GlobalConfigService.Data.Token.TokenVal,
                DateDebut = date,
                DateFin = date,
                ZoneRestaurationId = ZoneRestaurationId
            };

            var commandServiceResult = await CommandService.GetDashboardGerantAsync(requestDashboardGerant);
            if (commandServiceResult.Success)
            {
                if (commandServiceResult.Data != null)
                {

                    CommandDashBoard = new CommandDashboardViewModel().SetCommandDashBoardEncapsulation(commandServiceResult.Data);
                    //sets to false by on prepare and treats change of date on dashBoardView & orderListView because both views use this method.
                    CanOrderListShowUpdate = false;
                    //On enregistre pour le mode offline
                    if (DashboardSaved == null)
                    {
                        //j'ajoute la commande dashborad avec sa date
                        DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                          .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).FirstOrDefault().DashboardUserByDate.Add(new DashboardUserByDate() { DateDebut = date, CommandeDashboard = commandServiceResult.Data });

                        DashboardSaved = DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                          .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).FirstOrDefault().DashboardUserByDate.Where(x => x.DateDebut == date).FirstOrDefault();
                    }
                    else
                    {
                        //on met � jour les data de commande Dashborad pour cette date 
                        DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                         .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId)
                         .FirstOrDefault().DashboardUserByDate.Where(x => x.DateDebut == date)
                         .FirstOrDefault().CommandeDashboard = commandServiceResult.Data;
                    }
                    DataOfflineService.SaveConfig();
                }
            }
            else
            {
                var isError = true;
                if (commandServiceResult.ExceptionComesFromNetwork)
                    isError = DashboardSaved == null || DashboardSaved.CommandeDashboard == null ? true : false;

                if (isError)
                {
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, commandServiceResult.ExceptionMessage);
                    return false;
                }
                else
                    CommandDashBoard = new CommandDashboardViewModel().SetCommandDashBoardEncapsulation(
                       DashboardSaved.CommandeDashboard);
            }
            return true;
        }

        public async Task<bool> PrepareStock(string date)
        {
            ListStock.Clear();

            var stockResult = await StockService.GetStocks(GlobalConfigService.Data.Token.TokenVal, ZoneRestaurationId, date);
            if (stockResult.Success)
            {
                IsStockDispo = stockResult.Data == null || stockResult.Data.Count > 0 ? true : false;
                if (stockResult.Data != null && stockResult.Data.Count > 0)
                {
                    ListStock.AddRange(stockResult.Data.OrderBy(x => x.LibelleCommercial).ToList().Select(n => new StockArticleViewModelItem().SetStockArticleEncapsulation(n)));
                    //On enregistre pour le mode offline
                    if (DashboardSaved == null)
                    {
                        //j'ajoute la liste des stocks avec sa date
                        DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                        .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).FirstOrDefault().DashboardUserByDate.Add(new DashboardUserByDate() { DateDebut = date, ListStocks = stockResult.Data });

                        DashboardSaved = DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                         .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).FirstOrDefault().DashboardUserByDate.Where(x => x.DateDebut == date).FirstOrDefault();
                    }
                    else
                    {
                        //on met � jour la liste des stocks pour cette date 
                        DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                       .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).FirstOrDefault().DashboardUserByDate.Where(x => x.DateDebut == date).FirstOrDefault().ListStocks = stockResult.Data;
                    }
                    DataOfflineService.SaveConfig();
                }
            }
            else
            {
                var isError = true;
                if (stockResult.ExceptionComesFromNetwork)
                    isError = DashboardSaved == null || DashboardSaved.ListStocks == null ? true : false;
                if (isError)
                {
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, stockResult.ExceptionMessage);
                    return false;
                }
                else
                {
                    ListStock.AddRange(DashboardSaved.ListStocks.OrderBy(x => x.LibelleCommercial)
                        .ToList().Select(n => new StockArticleViewModelItem()
                            .SetStockArticleEncapsulation(n)));
                    IsStockDispo = ListStock != null || ListStock.Count > 0 ? true : false;
                }
            }
            return true;
        }




        #endregion

        #region Commandes de la premi�re page Dashboard

        protected override async void OnLoadListOrdersCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                //prepare View Orders
                var ordersVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IOrdersViewModel>();
                var success = await ordersVM.PrepareOrdersVM(DateDashBoard, ZoneRestaurationId);
                if (success)
                    Sodexo.Framework.Services.InteractionService().Open(ViewName.OrderListView, ordersVM);
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        protected override async void OnChangeDateCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                var popInVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IPopInViewModel>();
                //var selectedDate = DateTime.ParseExact(DateDashBoard, "dd/MM/yyyy HH:mm:ss", Culture).ToString("dd/MM/yyyy", Culture);
                var selectedDate = DateTime.ParseExact(DateDashBoard, "dd/MM/yyyy HH:mm:ss", Culture).Date;
                popInVM.IsFromListView = false;
                popInVM.PrepareVMForChangeDate(selectedDate);
                await Sodexo.Framework.Services.InteractionService().ViewPopIn(popInVM, ViewName.CalndarContentView);
            }
            catch (Exception ex)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
                EndWork();
            }
        }

        public async Task ChangeStockQuantity(object selectedStock, bool isAddQuantity)
        {
            try
            {
                BeginWork();
                var selectedDate = DateTime.ParseExact(DateDashBoard, "dd/MM/yyyy HH:mm:ss", Culture).Date;

                SelectedStock = ListStock.Where(x => x.Id == ((StockArticleViewModelItem)selectedStock).Id).FirstOrDefault();
                var popInVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IPopInViewModel>();
                var resultPreparePopIn = popInVM.PrepareVMForChangeStock(SelectedStock, isAddQuantity, selectedDate);
                await Sodexo.Framework.Services.InteractionService().ViewPopIn(popInVM, ViewName.ModifierStockContentView);
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        #endregion


        //Refresh pour le thread qui tourne ==> ne pas toucher
        public async Task<bool> refreshDashborad()
        {
            try
            {

                var date = DateDashBoard;
                var dateValue = DateTime.ParseExact(date, "dd/MM/yyyy HH:mm:ss", Culture).Date;
                DateDashBoard = dateValue.ToString("dd/MM/yyyy HH:mm:ss", Culture);
                var formattedDate = string.Empty;
                if (dateValue == DateTime.Now.Date)
                    DateDashBoardFormattedLabel = Properties.Resources.ForToday + " - " + DateTime.ParseExact(DateDashBoard, "dd/MM/yyyy HH:mm:ss", Culture).ToString("d MMMM", Culture);
                else
                    DateDashBoardFormattedLabel = DateTime.ParseExact(DateDashBoard, "dd/MM/yyyy HH:mm:ss", Culture).ToString("dddd d MMMM", Culture);
                var result = await PrepareCommandeDashboard(date);
                var ordersVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IOrdersViewModel>();
                await ordersVM.PrepareOrdersList(date, ZoneRestaurationId);
                return result;
            }
            catch (Exception e)
            {
                //Afficher message d'erreur
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                return false;
            }

        }
        public async Task<bool> refreshOnlyDashborad()
        {
            try
            {
                var date = DateDashBoard;
                var dateValue = DateTime.ParseExact(date, "dd/MM/yyyy HH:mm:ss", Culture).Date;
                DateDashBoard = dateValue.ToString("dd/MM/yyyy HH:mm:ss", Culture);
                var formattedDate = string.Empty;
                if (dateValue == DateTime.Now.Date)
                    DateDashBoardFormattedLabel = Properties.Resources.ForToday + " - " + DateTime.ParseExact(DateDashBoard, "dd/MM/yyyy HH:mm:ss", Culture).ToString("d MMMM", Culture);
                else
                    DateDashBoardFormattedLabel = DateTime.ParseExact(DateDashBoard, "dd/MM/yyyy HH:mm:ss", Culture).ToString("dddd d MMMM", Culture);

                var result = await PrepareCommandeDashboard(date);
                return result;
            }
            catch (Exception e)
            {
                //Afficher message d'erreur
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                return false;
            }

        }
        protected override void OnFilterCommand_Execute(object p_Parameter)
        {

        }
    }
}
