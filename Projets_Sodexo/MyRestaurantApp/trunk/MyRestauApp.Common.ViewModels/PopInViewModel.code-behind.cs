using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using MyRestauApp.Common.DAOServices;
using MyRestauApp.Common.DAOServices.Models;
using MyRestauApp.Common.DAOServices.Requests;
using MyRestauApp.Common.ViewModels.ItemsViewModel;
using Sodexo;
using MyRestauApp.Common.DAOServices.Services;
using System.Globalization;

namespace MyRestauApp.Common.ViewModels
{
    public partial class PopInViewModel : PopInViewModelBase, IPopInViewModel
    {
        #region Proprietes
        private ICommandeService CommandService;
        private IStockService StockService;
        private IGlobalConfigService GlobalConfigService;
        private CultureInfo Culture;
        private MyRestauApp.Common.DAOServices.Services.IAuthService AuthService;
        private string Locale = Properties.Resources.CultureInfoCurrentCultureName;
        #endregion

        #region Initialisation

        protected override void OnInitialize()
        {
            base.OnInitialize();
            Initialisation();
        }

        public void Initialisation()
        {
            CommandService = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.DAOServices.Services.ICommandeService>();
            StockService = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.DAOServices.Services.IStockService>();
            GlobalConfigService = Sodexo.Framework.Services.GlobalConfigService();
            Culture = new CultureInfo(Properties.Resources.CultureInfoCurrentCultureName.Replace("_", "-"));
            AuthService = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.DAOServices.Services.IAuthService>();
        }

        #endregion End

        #region Case Popin Calendrier

        public async Task<bool> PrepareVMForChangeDate(DateTime selectedDate)
        {
            try
            {
                BeginWork();
                IsPopInVisibile = true;
                SelectedDate = selectedDate;
                return true;
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                return false;
            }
            finally
            {
                EndWork();
            }
        }

        protected override async void OnChangeDateCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                if ((p_Parameter != null) && (SelectedDate != (DateTime)p_Parameter))
                {
                    var SelectedDatePrevious = SelectedDate;
                    SelectedDate = (DateTime)p_Parameter;
                    var DashBoardVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IDashboardViewModel>();
                    var selectedDateFormatted = SelectedDate.ToString("dd/MM/yyyy HH:mm:ss", Culture);
                    var canUpdateDashboard = true;
                    if (IsFromListView)
                    {
                        //Update La page de la liste commandes et la page dashboard sinon que la page dashboard
                        var ordersVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IOrdersViewModel>();
                        var listStatutLogicValue = string.Join(",", ordersVM.ListStatusToFilter.Where(x => x.IsChecked == true).ToList().Select(y => y.LogicValue));
                        var success = await ordersVM.RefreshOrdersList(selectedDateFormatted, listStatutLogicValue);
                        if (!success)
                            canUpdateDashboard = false;
                    }
                    var canContinue = canUpdateDashboard;
                    if (canUpdateDashboard)
                        canContinue = await DashBoardVM.UpdateDashboardView(selectedDateFormatted);
                    if (canContinue)
                        IsPopInVisibile = false;
                    else
                        SelectedDate = SelectedDatePrevious;
                }
                //on traite le cas -date n'a pas chang� donc on fait rien
                else
                {
                    IsPopInVisibile = false;
                }
            }
            catch (Exception e)
            {
                //Afficher message d'erreur
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        #endregion

        #region Case Popin Add/Remove Stock
        public async Task<bool> PrepareVMForChangeStock(StockArticleViewModelItem stockArticleSelected, bool isAddQuantity, DateTime selectedDate)
        {
            try
            {
                BeginWork();
                IsPopInVisibile = true;
                IsAddQuantity = isAddQuantity;
                TypedNumber = string.Empty;
                StockArticleSelected = stockArticleSelected;
                HeadTitleStockLabel = (IsAddQuantity ? Properties.Resources.AddStock : Properties.Resources.RemoveStock);
                ButtonAddRemoveStockText = (IsAddQuantity ? Properties.Resources.AddButtonStock : Properties.Resources.RemoveButtonStock);
                SelectedDate = selectedDate;
                return true;
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                return false;
            }
            finally
            {
                EndWork();
            }
        }

        protected override async void OnUpdateStockCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                Initialisation();
                int FinalTypedNumber;
                if (int.TryParse(TypedNumber, out FinalTypedNumber))
                {
                    if (FinalTypedNumber > 0)
                    {
                        // La quantit� � updater est n�gative ou positive 
                        FinalTypedNumber = (IsAddQuantity) ? FinalTypedNumber : FinalTypedNumber * (-1);
                        var l_NetworkService = Sodexo.Framework.Services.Container().Resolve<INetworkService>();
                        if (!l_NetworkService.IsConnected)
                        {
                            Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.ErrorNetwork);
                        }
                        else
                        {
                            var selectedDateFormatted = SelectedDate.ToString("dd/MM/yyyy HH:mm:ss", Culture);
                            var requestUpdateStock = new UpdateStockRequest()
                            {
                                Token = GlobalConfigService.Data.Token.TokenVal,
                                ZoneRestaurationId = StockArticleSelected.ZoneRestaurationId,
                                ArticleId = StockArticleSelected.Id,
                                AddQuantite = FinalTypedNumber,
                                Date = selectedDateFormatted
                            };
                            var resultUpdateStock = await StockService.UpdateStock(requestUpdateStock);
                            if (resultUpdateStock.Success)
                            {
                                // Mettre � jour la liste des stocks
                                var DashBoardVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IDashboardViewModel>();
                                var result = await DashBoardVM.UpdateDashboardView();
                                if (result)
                                {
                                    IsPopInVisibile = false;
                                    await Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Success, Properties.Resources.UpdateStockSuccess, 3, AlertType.Success);
                                }
                            }
                            else
                                await Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, resultUpdateStock.ExceptionMessage);
                        }
                    }
                    else
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.ErrorUpdateStockQuantity);
                }
                else
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.ErrorUpdateStockQuantity);
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        #endregion

        #region Case Popin Change Statut

        public async Task<bool> PrepareVMForChangeStatut(OrderViewModelItem command, bool isDetailView)
        {
            try
            {
                BeginWork();
                IsDetailView = isDetailView;
                IsPopInVisibile = true;
                CommandSelected = command;
                if (CommandSelected != null && CommandSelected.ListStatutsPossibles != null)
                    ListStatuts.AddRange(CommandSelected.ListStatutsPossibles.Select(n => new StatutItemViewModel().SetCommandStatutEncapsulation(n)));
                SelectedStatut = new StatutItemViewModel().SetCommandStatutEncapsulation(CommandSelected.Statut);
                return true;
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                return false;
            }
            finally
            {
                EndWork();
            }
        }

        protected override async void OnUpdateStatutCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                var l_NetworkService = Sodexo.Framework.Services.Container().Resolve<INetworkService>();
                if (!l_NetworkService.IsConnected)
                {
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.ErrorNetwork);
                }
                else
                {
                    SelectedStatut = (StatutItemViewModel)p_Parameter;
                    var resultUpdateCommand = await CommandService.UpdateCommandeStatutAsync(GlobalConfigService.Data.Token.TokenVal, CommandSelected.Id, SelectedStatut.LogicValue);
                    if (resultUpdateCommand.Success)
                    {
                        //Ajout dans Global config pour dire la commande n'est plus nouvelle
                        if (!GlobalConfigService.Data.ViewedOrdersIds.Contains(CommandSelected.Id))
                            GlobalConfigService.Data.ViewedOrdersIds.Add(CommandSelected.Id);
                        GlobalConfigService.SaveConfig();
                        if (IsDetailView)
                        {
                            //Mise � jour de la page d�tail et la page d'avant
                            var OrderDetailVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IOrderDetailViewModel>();
                            await OrderDetailVM.UpdateOrderDetail(CommandSelected.Id);
                        }
                        var ordersVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IOrdersViewModel>();
                        await ordersVM.UpdateCommandes(CommandSelected.Id, SelectedStatut);

                        var dashboardVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IDashboardViewModel>();
                        await dashboardVM.refreshOnlyDashborad();

                        //force refresh of list
                        ordersVM.DoRefresh = !ordersVM.DoRefresh;
                        await Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Success, Properties.Resources.UpdateStatutSucces, 3, AlertType.Success);
                        IsPopInVisibile = false;

                    }
                    else
                        await Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, resultUpdateCommand.ExceptionMessage);

                }
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        #endregion

        #region Case Popin Filter statut
        public void PrepareVMForFilterStatut()
        {
            try
            {
                BeginWork();
                IsPopInVisibile = true;
                ListStatuts.Clear();
                var ordersViewModel = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IOrdersViewModel>();
                var GlobalConfigSaved = Sodexo.Framework.Services.GlobalConfigService();
                GlobalConfigSaved.LoadConfig();
                if (GlobalConfigSaved.Data.ListStatutSaved != null && GlobalConfigSaved.Data.ListStatutSaved.Count > 0)
                    ListStatuts.AddRange(GlobalConfigSaved.Data.ListStatutSaved.Select(x => new StatutItemViewModel().SetCommandStatutEncapsulation(x)));
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        protected override async void OnFilterStatutCommand_Execute(object p_Parameter)
        {
            try
            {

                BeginWork();
                var l_NetworkService = Sodexo.Framework.Services.Container().Resolve<INetworkService>();
                if (!l_NetworkService.IsConnected)
                {
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.ErrorNetwork);
                }
                else
                {
                    IsPopInVisibile = false;
                    var ordersViewModel = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IOrdersViewModel>();
                    ordersViewModel.ListStatusToFilter.Clear();
                    ordersViewModel.ListStatusToFilter.AddRange(ListStatuts);

                    GlobalConfigService.Data.ListStatutSaved = new List<StatutCommande>();
                    GlobalConfigService.Data.ListStatutSaved.AddRange(ordersViewModel.ListStatusToFilter.Select(x => x.GetCommandStatutEncapsulation()));
                    GlobalConfigService.SaveConfig();
                    ordersViewModel.IsVisibleFilterEnable = ListStatuts != null ? (ListStatuts.Any(x => x.IsChecked == false) ? true : false) : false;
                    var listStatutLogicValue = string.Join(",", ListStatuts.Where(x => x.IsChecked == true).ToList().Select(y => y.LogicValue));
                    await ordersViewModel.RefreshOrdersListWithBeginEndWork(ordersViewModel.DateDashBoard, listStatutLogicValue);
                }
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }
        #endregion End Case Popin Filter statut

        #region Case Popin Yes/No (Disconnect/Cancel)

        public void PrepareVMForAskYesNo(bool isDisconnect)
        {
            try
            {
                BeginWork();
                IsPopInVisibile = true;
                IsDisconnect = isDisconnect;
                TextLabelPopinYesNo = IsDisconnect ? Properties.Resources.DisconnectText : Properties.Resources.CancelOrderText;
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        protected override async void OnClickYesCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();

                if (IsDisconnect)
                {
                    try
                    {
                        // D�connexion
                        var result = await AuthService.DisconnectAsync(GlobalConfigService.Data.Token.TokenVal, ",", Locale);

                        if (result.Success)
                        {
                            GlobalConfigService.Data.Token = null;
                            GlobalConfigService.SaveConfig();
                        }
                        else Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, result.ExceptionMessage);
                    }
                    finally { Sodexo.Framework.Services.InteractionService().Set(ViewName.ConnexionView, null); }
                }
                else
                {
                    // Annuler la commande
                    var OrderDetailVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IOrderDetailViewModel>();
                    var commandServiceResult = await CommandService.UpdateCommandeStatutAsync(GlobalConfigService.Data.Token.TokenVal, OrderDetailVM.OrderId, Constants.CancelLogicValue);

                    if (commandServiceResult.Success)
                    {
                        await OrderDetailVM.UpdateOrderDetail(OrderDetailVM.OrderId);
                        var ordersVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IOrdersViewModel>();
                        ordersVM.DoRefresh = !ordersVM.DoRefresh;
                        //
                        Sodexo.Framework.Services.InteractionService().Close();
                    }
                    else Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, commandServiceResult.ExceptionMessage);
                }
            }
            catch (Exception ex) { Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message); }
            finally { EndWork(); }
        }

        protected override void OnClickNoCommand_Execute(object p_Parameter)
        {
            IsPopInVisibile = false;
        }

        #endregion


        // Clic sur le bouton fermer sur les trois popIn
        protected override async void OnClosePopInCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                IsPopInVisibile = false;
            }
            finally
            {
                EndWork();
            }
        }
    }
}
