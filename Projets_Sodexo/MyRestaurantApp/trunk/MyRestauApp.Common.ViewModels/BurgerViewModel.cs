﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML BurgerViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  Sodexo.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;

namespace MyRestauApp.Common.ViewModels
{
	public abstract partial class BurgerViewModelBase : Sodexo.ViewModel
	{
		protected override void OnInitialize()
		{
			base.OnInitialize();

			// Initialisation de la commande DisconnectCommand.
			DisconnectCommand = new DelegateCommand(DisconnectCommand_CanExecute, DisconnectCommand_Execute);
		}

		#region === Propriétés ===

			#region === Propriété : UserFirstName ===

				public const string UserFirstName_PROPERTYNAME = "UserFirstName";

				private string _UserFirstName;
				///<summary>
				/// Propriété : UserFirstName
				///</summary>
				public string UserFirstName
				{
					get
					{
						return GetValue<string>(() => _UserFirstName);
					}
					set
					{
						SetValue<string>(() => _UserFirstName, (v) => _UserFirstName = v, value, UserFirstName_PROPERTYNAME,  DoUserFirstNameBeforeSet, DoUserFirstNameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUserFirstNameBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUserFirstNameAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUserFirstNameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UserLastName ===

				public const string UserLastName_PROPERTYNAME = "UserLastName";

				private string _UserLastName;
				///<summary>
				/// Propriété : UserLastName
				///</summary>
				public string UserLastName
				{
					get
					{
						return GetValue<string>(() => _UserLastName);
					}
					set
					{
						SetValue<string>(() => _UserLastName, (v) => _UserLastName = v, value, UserLastName_PROPERTYNAME,  DoUserLastNameBeforeSet, DoUserLastNameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUserLastNameBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUserLastNameAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUserLastNameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UserImage ===

				public const string UserImage_PROPERTYNAME = "UserImage";

				private string _UserImage;
				///<summary>
				/// Propriété : UserImage
				///</summary>
				public string UserImage
				{
					get
					{
						return GetValue<string>(() => _UserImage);
					}
					set
					{
						SetValue<string>(() => _UserImage, (v) => _UserImage = v, value, UserImage_PROPERTYNAME,  DoUserImageBeforeSet, DoUserImageAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUserImageBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUserImageAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUserImageDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SiteName ===

				public const string SiteName_PROPERTYNAME = "SiteName";

				private string _SiteName;
				///<summary>
				/// Propriété : SiteName
				///</summary>
				public string SiteName
				{
					get
					{
						return GetValue<string>(() => _SiteName);
					}
					set
					{
						SetValue<string>(() => _SiteName, (v) => _SiteName = v, value, SiteName_PROPERTYNAME,  DoSiteNameBeforeSet, DoSiteNameAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSiteNameBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSiteNameAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySiteNameDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DisconnectCommand ===

				public const string DisconnectCommand_PROPERTYNAME = "DisconnectCommand";

				private IDelegateCommand _DisconnectCommand;
				///<summary>
				/// Propriété : DisconnectCommand
				///</summary>
				public IDelegateCommand DisconnectCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _DisconnectCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _DisconnectCommand, (v) => _DisconnectCommand = v, value, DisconnectCommand_PROPERTYNAME,  DoDisconnectCommandBeforeSet, DoDisconnectCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDisconnectCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDisconnectCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDisconnectCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux commandes ===

			// Notez que les propriétés de commandes sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Commande : DisconnectCommand ===


				private bool DisconnectCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnDisconnectCommand_CanExecute(l_Parameter);
				}

				private void DisconnectCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnDisconnectCommand_Execute(l_Parameter);
				}

				protected virtual bool OnDisconnectCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnDisconnectCommand_Execute(object p_Parameter);

			#endregion


		#endregion

	}

	public partial class BurgerViewModel : BurgerViewModelBase
	{
	}
}
