﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyRestauApp.Common.ViewModels.ItemsViewModel;
using MyRestauApp.Common.DAOServices.Models;
using Sodexo;

namespace MyRestauApp.Common.ViewModels
{
    public interface IPopInViewModel : IViewModel
    {
        string ButtonAddRemoveStockText { get; set; }
        Sodexo.IDelegateCommand ChangeDateCommand { get; }
        OrderViewModelItem CommandSelected { get; set; }
        string HeadTitleStockLabel { get; set; }
        bool IsAddQuantity { get; set; }
        bool IsDetailView { get; set; }
        Sodexo.IDelegateCommand ClosePopInCommand { get; }
        System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel> ListStatuts { get; }
        DateTime SelectedDate { get; set; }
        StatutItemViewModel SelectedStatut { get; set; }
        StockArticleViewModelItem StockArticleSelected { get; set; }
        string TypedNumber { get; set; }
        Sodexo.IDelegateCommand UpdateStatutCommand { get; }
        Sodexo.IDelegateCommand UpdateStockCommand { get; }
        Sodexo.IDelegateCommand ClickYesCommand { get; }
        Sodexo.IDelegateCommand ClickNoCommand { get; }
        Sodexo.IDelegateCommand FilterStatutCommand { get; } 
        string TextLabelPopinYesNo { get; set; }
        bool IsFromListView { get; set; }
        bool IsFromFilterView { get; set; }

        //Les méthodes
        Task<bool> PrepareVMForChangeStock(StockArticleViewModelItem stockArticleViewModelItem, bool IsAddQuantity, DateTime selectedDate);
        Task<bool> PrepareVMForChangeStatut(OrderViewModelItem command, bool isDetailView);
        Task<bool> PrepareVMForChangeDate(DateTime selectedDate);
        void PrepareVMForFilterStatut();
        void PrepareVMForAskYesNo(bool isDisconnect);

    }
}
