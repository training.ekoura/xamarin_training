﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyRestauApp.Common.ViewModels.ItemsViewModel;
using Sodexo;
using MyRestauApp.Common.DAOServices.Models;

namespace MyRestauApp.Common.ViewModels
{
    public interface IDashboardViewModel : IViewModel
    {
        IBurgerViewModel BurgerVM { get; set; }
        bool CanOrderListShowUpdate { get; set; }
        Sodexo.IDelegateCommand ChangeDateCommand { get; }
        CommandDashboardViewModel CommandDashBoard { get; set; }
        int CommandesCount { get; set; }
        string DateDashBoard { get; set; }
        string DateDashBoardFormattedLabel { get; set; }
        Sodexo.IDelegateCommand FilterCommand { get; }
        bool IsAddQuantity { get; set; }
        bool IsStockDispo { get; set; }
        bool IsVisibleButtonCancelCommand { get; set; }
        System.Collections.ObjectModel.ObservableCollection<StockArticleViewModelItem> ListStock { get; }
        Sodexo.IDelegateCommand LoadListOrdersCommand { get; }
        StockArticleViewModelItem SelectedStock { get; set; }
        string ZoneRestaurationId { get; set; }

        //Les méthodes
        Task<bool> PrepareDashboardVM(bool isOffline);
        Task<bool> PrepareCommandeDashboard(string date);
        Task<bool> PrepareStock(string date);
        Task<bool> UpdateDashboardView(string date=null);
        Task ChangeStockQuantity(object selectedStock, bool isAddQuantity);
        Task<bool> refreshDashborad();
        Task<bool> refreshOnlyDashborad();

    
    }
}
