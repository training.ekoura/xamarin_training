﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sodexo;

namespace MyRestauApp.Common.ViewModels
{
    public interface IContextViewModel: IViewModel
    {
        Task AppStarter();
        Task RefreshDashboard();
    }
}
