﻿using Sodexo;
using System;
using System.Threading.Tasks;
namespace MyRestauApp.Common.ViewModels
{
    public interface IBurgerViewModel : IViewModel
    {
        Sodexo.IDelegateCommand DisconnectCommand { get; }
        string UserFirstName { get; set; }
        string UserLastName { get; set; }
        string SiteName { get; set; }

        //Méthodes
        Task<bool> PrepareVM();
    }
}
