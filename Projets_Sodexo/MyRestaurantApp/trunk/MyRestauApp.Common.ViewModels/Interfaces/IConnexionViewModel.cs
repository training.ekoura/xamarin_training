﻿using Sodexo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.ViewModels
{
    public interface IConnexionViewModel : IViewModel
    {
        string LoginEmail { get; set; }
        string LoginPassword { get; set; }
        Sodexo.IDelegateCommand LoginCommand { get; }
    }
}
