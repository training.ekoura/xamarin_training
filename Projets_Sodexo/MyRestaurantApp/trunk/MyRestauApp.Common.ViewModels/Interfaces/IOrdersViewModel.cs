﻿using Sodexo;
using System;
using System.Threading.Tasks;
namespace MyRestauApp.Common.ViewModels
{
    public interface IOrdersViewModel : IViewModel
    {
        Sodexo.IDelegateCommand ChangeDateCommand { get; }
        Sodexo.IDelegateCommand ChangeStatutCommand { get; }
        string DateDashBoard { get; set; }
        string DateDashBoardFormattedLabel { get; set; }
        bool DoRefresh { get; set; }
        bool IsVisibleFilterEnable { get; set; }        
        Sodexo.IDelegateCommand FilterStatutCommand { get; }
        bool IsNoOrdersDispo { get; set; }
        System.Collections.ObjectModel.ObservableCollection<OrderViewModelItem> ListOrders { get; }
        System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel> ListStatusToFilter { get; }
        Sodexo.IDelegateCommand OpenDetailViewCommand { get; }
        string ZoneRestaurationId { get; set; }

        //Méthodes
        Task<bool> PrepareOrdersVM(string date, string zoneRestaurationId);
        Task UpdateCommandes(int commandeId, StatutItemViewModel selectedStatut);
        Task<bool> RefreshOrdersList(string date, string listStatut = null);
        Task<bool> PrepareOrdersList(string date, string zoneRestaurationId);
        Task<bool> RefreshOrdersListWithBeginEndWork(string date, string listStatut=null);
    }
}
