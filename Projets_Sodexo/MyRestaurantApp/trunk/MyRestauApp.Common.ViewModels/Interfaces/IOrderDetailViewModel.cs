﻿using Sodexo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestauApp.Common.ViewModels
{
    public interface IOrderDetailViewModel : IViewModel
    {
        Sodexo.IDelegateCommand CancelOrderCommand { get; }
        string HeaderText { get; set; }
        bool IsVisibleButtonCancel { get; set; }
        OrderViewModelItem Order { get; set; }
        int OrderId { get; set; }
        string SelectedDate { get; set; }
        Sodexo.IDelegateCommand ViewStatutPopInCommand { get; }
        string ZoneRestaurationId { get; set; }

        //Méthodes
        Task<bool> PrepareOrderDetailVM(int commandId, string date);
        Task UpdateOrderDetail(int orderId);
    }
}
