﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML ConnexionViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  Sodexo.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;

namespace MyRestauApp.Common.ViewModels
{
	public abstract partial class ConnexionViewModelBase : Sodexo.ViewModel
	{
		protected override void OnInitialize()
		{
			base.OnInitialize();

			// Initialisation de la commande LoginCommand.
			LoginCommand = new DelegateCommand(LoginCommand_CanExecute, LoginCommand_Execute);
		}

		protected override void AfterInitialize()
		{
			base.AfterInitialize();

			// Notification des dépendances de la propriété LoginEmail.
			NotifyLoginEmailDependencies();
			// Notification des dépendances de la propriété LoginPassword.
			NotifyLoginPasswordDependencies();
		}

		#region === Propriétés ===

			#region === Propriété : LoginEmail ===

				public const string LoginEmail_PROPERTYNAME = "LoginEmail";

				private string _LoginEmail;
				///<summary>
				/// Propriété : LoginEmail
				///</summary>
				public string LoginEmail
				{
					get
					{
						return GetValue<string>(() => _LoginEmail);
					}
					set
					{
						SetValue<string>(() => _LoginEmail, (v) => _LoginEmail = v, value, LoginEmail_PROPERTYNAME,  DoLoginEmailBeforeSet, DoLoginEmailAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLoginEmailBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLoginEmailAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
					// Puisque cette propriété a des dépendances, il faut les notifier.
					NotifyLoginEmailDependencies();
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLoginEmailDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
					DoLoginCommandUpdate();
				}

			#endregion

			#region === Propriété : LoginPassword ===

				public const string LoginPassword_PROPERTYNAME = "LoginPassword";

				private string _LoginPassword;
				///<summary>
				/// Propriété : LoginPassword
				///</summary>
				public string LoginPassword
				{
					get
					{
						return GetValue<string>(() => _LoginPassword);
					}
					set
					{
						SetValue<string>(() => _LoginPassword, (v) => _LoginPassword = v, value, LoginPassword_PROPERTYNAME,  DoLoginPasswordBeforeSet, DoLoginPasswordAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLoginPasswordBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLoginPasswordAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
					// Puisque cette propriété a des dépendances, il faut les notifier.
					NotifyLoginPasswordDependencies();
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLoginPasswordDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
					DoLoginCommandUpdate();
				}

			#endregion

			#region === Propriété : LoginCommand ===

				public const string LoginCommand_PROPERTYNAME = "LoginCommand";

				private IDelegateCommand _LoginCommand;
				///<summary>
				/// Propriété : LoginCommand
				///</summary>
				public IDelegateCommand LoginCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _LoginCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _LoginCommand, (v) => _LoginCommand = v, value, LoginCommand_PROPERTYNAME,  DoLoginCommandBeforeSet, DoLoginCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLoginCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLoginCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLoginCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux mises à jour (Update) ===

			#region === Mise à jour : LoginCommandUpdate ===

				private bool _IsInLoginCommandUpdate;
				///<summary>
				/// Cette méthode est appelée pour provoquer la mise à jour : LoginCommandUpdate.
				///</summary>
				private void DoLoginCommandUpdate()
				{
					lock(Locker)
					{
						if(_IsInLoginCommandUpdate) return;
						try
						{
							_IsInLoginCommandUpdate = true;
							OnLoginCommandUpdate_Execute();
						}
						finally
						{
							_IsInLoginCommandUpdate = false;
						}
					}
				}

				///<summary>
				/// Méthode de mise à jour : LoginCommandUpdate.
				///</summary>
				private void OnLoginCommandUpdate_Execute()
				{
					LoginCommand.InvalidateCanExecute();
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux commandes ===

			// Notez que les propriétés de commandes sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Commande : LoginCommand ===


				private bool LoginCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnLoginCommand_CanExecute(l_Parameter);
				}

				private void LoginCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnLoginCommand_Execute(l_Parameter);
				}

				protected virtual bool OnLoginCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnLoginCommand_Execute(object p_Parameter);

			#endregion


		#endregion

	}

	public partial class ConnexionViewModel : ConnexionViewModelBase
	{
	}
}
