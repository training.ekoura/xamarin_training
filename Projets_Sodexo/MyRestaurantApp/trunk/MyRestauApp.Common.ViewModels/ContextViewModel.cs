﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML ContextViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  Sodexo.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;

namespace MyRestauApp.Common.ViewModels
{
	public abstract partial class ContextViewModelBase : Sodexo.ViewModel
	{
	}

	public partial class ContextViewModel : ContextViewModelBase
	{
	}
}
