﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML PopInViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  Sodexo.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;

namespace MyRestauApp.Common.ViewModels
{
	public abstract partial class PopInViewModelBase : Sodexo.ViewModel
	{
		protected override void OnInitialize()
		{
			base.OnInitialize();

			// Initialisation de la collection ListStatuts.
			ListStatuts = new System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel>();
			// Initialisation de la commande ClosePopInCommand.
			ClosePopInCommand = new DelegateCommand(ClosePopInCommand_CanExecute, ClosePopInCommand_Execute);
			// Initialisation de la commande ChangeDateCommand.
			ChangeDateCommand = new DelegateCommand(ChangeDateCommand_CanExecute, ChangeDateCommand_Execute);
			// Initialisation de la commande UpdateStatutCommand.
			UpdateStatutCommand = new DelegateCommand(UpdateStatutCommand_CanExecute, UpdateStatutCommand_Execute);
			// Initialisation de la commande UpdateStockCommand.
			UpdateStockCommand = new DelegateCommand(UpdateStockCommand_CanExecute, UpdateStockCommand_Execute);
			// Initialisation de la commande ClickYesCommand.
			ClickYesCommand = new DelegateCommand(ClickYesCommand_CanExecute, ClickYesCommand_Execute);
			// Initialisation de la commande ClickNoCommand.
			ClickNoCommand = new DelegateCommand(ClickNoCommand_CanExecute, ClickNoCommand_Execute);
			// Initialisation de la commande FilterStatutCommand.
			FilterStatutCommand = new DelegateCommand(FilterStatutCommand_CanExecute, FilterStatutCommand_Execute);
		}

		#region === Propriétés ===

			#region === Propriété : IsPopInVisibile ===

				public const string IsPopInVisibile_PROPERTYNAME = "IsPopInVisibile";

				private bool _IsPopInVisibile;
				///<summary>
				/// Propriété : IsPopInVisibile
				///</summary>
				public bool IsPopInVisibile
				{
					get
					{
						return GetValue<bool>(() => _IsPopInVisibile);
					}
					set
					{
						SetValue<bool>(() => _IsPopInVisibile, (v) => _IsPopInVisibile = v, value, IsPopInVisibile_PROPERTYNAME,  DoIsPopInVisibileBeforeSet, DoIsPopInVisibileAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsPopInVisibileBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsPopInVisibileAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsPopInVisibileDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectedDate ===

				public const string SelectedDate_PROPERTYNAME = "SelectedDate";

				private DateTime _SelectedDate;
				///<summary>
				/// Propriété : SelectedDate
				///</summary>
				public DateTime SelectedDate
				{
					get
					{
						return GetValue<DateTime>(() => _SelectedDate);
					}
					set
					{
						SetValue<DateTime>(() => _SelectedDate, (v) => _SelectedDate = v, value, SelectedDate_PROPERTYNAME,  DoSelectedDateBeforeSet, DoSelectedDateAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectedDateBeforeSet(string p_PropertyName, DateTime p_OldValue, DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectedDateAfterSet(string p_PropertyName, DateTime p_OldValue, DateTime p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectedDateDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsFromListView ===

				public const string IsFromListView_PROPERTYNAME = "IsFromListView";

				private bool _IsFromListView;
				///<summary>
				/// Propriété : IsFromListView
				///</summary>
				public bool IsFromListView
				{
					get
					{
						return GetValue<bool>(() => _IsFromListView);
					}
					set
					{
						SetValue<bool>(() => _IsFromListView, (v) => _IsFromListView = v, value, IsFromListView_PROPERTYNAME,  DoIsFromListViewBeforeSet, DoIsFromListViewAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsFromListViewBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsFromListViewAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsFromListViewDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : CommandSelected ===

				public const string CommandSelected_PROPERTYNAME = "CommandSelected";

				private OrderViewModelItem _CommandSelected;
				///<summary>
				/// Propriété : CommandSelected
				///</summary>
				public OrderViewModelItem CommandSelected
				{
					get
					{
						return GetValue<OrderViewModelItem>(() => _CommandSelected);
					}
					set
					{
						SetValue<OrderViewModelItem>(() => _CommandSelected, (v) => _CommandSelected = v, value, CommandSelected_PROPERTYNAME,  DoCommandSelectedBeforeSet, DoCommandSelectedAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoCommandSelectedBeforeSet(string p_PropertyName, OrderViewModelItem p_OldValue, OrderViewModelItem p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoCommandSelectedAfterSet(string p_PropertyName, OrderViewModelItem p_OldValue, OrderViewModelItem p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyCommandSelectedDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : SelectedStatut ===

				public const string SelectedStatut_PROPERTYNAME = "SelectedStatut";

				private StatutItemViewModel _SelectedStatut;
				///<summary>
				/// Propriété : SelectedStatut
				///</summary>
				public StatutItemViewModel SelectedStatut
				{
					get
					{
						return GetValue<StatutItemViewModel>(() => _SelectedStatut);
					}
					set
					{
						SetValue<StatutItemViewModel>(() => _SelectedStatut, (v) => _SelectedStatut = v, value, SelectedStatut_PROPERTYNAME,  DoSelectedStatutBeforeSet, DoSelectedStatutAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoSelectedStatutBeforeSet(string p_PropertyName, StatutItemViewModel p_OldValue, StatutItemViewModel p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoSelectedStatutAfterSet(string p_PropertyName, StatutItemViewModel p_OldValue, StatutItemViewModel p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifySelectedStatutDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsDetailView ===

				public const string IsDetailView_PROPERTYNAME = "IsDetailView";

				private bool _IsDetailView;
				///<summary>
				/// Propriété : IsDetailView
				///</summary>
				public bool IsDetailView
				{
					get
					{
						return GetValue<bool>(() => _IsDetailView);
					}
					set
					{
						SetValue<bool>(() => _IsDetailView, (v) => _IsDetailView = v, value, IsDetailView_PROPERTYNAME,  DoIsDetailViewBeforeSet, DoIsDetailViewAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsDetailViewBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsDetailViewAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsDetailViewDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsAddQuantity ===

				public const string IsAddQuantity_PROPERTYNAME = "IsAddQuantity";

				private bool _IsAddQuantity;
				///<summary>
				/// Propriété : IsAddQuantity
				///</summary>
				public bool IsAddQuantity
				{
					get
					{
						return GetValue<bool>(() => _IsAddQuantity);
					}
					set
					{
						SetValue<bool>(() => _IsAddQuantity, (v) => _IsAddQuantity = v, value, IsAddQuantity_PROPERTYNAME,  DoIsAddQuantityBeforeSet, DoIsAddQuantityAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsAddQuantityBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsAddQuantityAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsAddQuantityDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : TypedNumber ===

				public const string TypedNumber_PROPERTYNAME = "TypedNumber";

				private string _TypedNumber;
				///<summary>
				/// Propriété : TypedNumber
				///</summary>
				public string TypedNumber
				{
					get
					{
						return GetValue<string>(() => _TypedNumber);
					}
					set
					{
						SetValue<string>(() => _TypedNumber, (v) => _TypedNumber = v, value, TypedNumber_PROPERTYNAME,  DoTypedNumberBeforeSet, DoTypedNumberAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTypedNumberBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTypedNumberAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTypedNumberDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : StockArticleSelected ===

				public const string StockArticleSelected_PROPERTYNAME = "StockArticleSelected";

				private StockArticleViewModelItem _StockArticleSelected;
				///<summary>
				/// Propriété : StockArticleSelected
				///</summary>
				public StockArticleViewModelItem StockArticleSelected
				{
					get
					{
						return GetValue<StockArticleViewModelItem>(() => _StockArticleSelected);
					}
					set
					{
						SetValue<StockArticleViewModelItem>(() => _StockArticleSelected, (v) => _StockArticleSelected = v, value, StockArticleSelected_PROPERTYNAME,  DoStockArticleSelectedBeforeSet, DoStockArticleSelectedAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoStockArticleSelectedBeforeSet(string p_PropertyName, StockArticleViewModelItem p_OldValue, StockArticleViewModelItem p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoStockArticleSelectedAfterSet(string p_PropertyName, StockArticleViewModelItem p_OldValue, StockArticleViewModelItem p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyStockArticleSelectedDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : HeadTitleStockLabel ===

				public const string HeadTitleStockLabel_PROPERTYNAME = "HeadTitleStockLabel";

				private string _HeadTitleStockLabel;
				///<summary>
				/// Propriété : HeadTitleStockLabel
				///</summary>
				public string HeadTitleStockLabel
				{
					get
					{
						return GetValue<string>(() => _HeadTitleStockLabel);
					}
					set
					{
						SetValue<string>(() => _HeadTitleStockLabel, (v) => _HeadTitleStockLabel = v, value, HeadTitleStockLabel_PROPERTYNAME,  DoHeadTitleStockLabelBeforeSet, DoHeadTitleStockLabelAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoHeadTitleStockLabelBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoHeadTitleStockLabelAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyHeadTitleStockLabelDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ButtonAddRemoveStockText ===

				public const string ButtonAddRemoveStockText_PROPERTYNAME = "ButtonAddRemoveStockText";

				private string _ButtonAddRemoveStockText;
				///<summary>
				/// Propriété : ButtonAddRemoveStockText
				///</summary>
				public string ButtonAddRemoveStockText
				{
					get
					{
						return GetValue<string>(() => _ButtonAddRemoveStockText);
					}
					set
					{
						SetValue<string>(() => _ButtonAddRemoveStockText, (v) => _ButtonAddRemoveStockText = v, value, ButtonAddRemoveStockText_PROPERTYNAME,  DoButtonAddRemoveStockTextBeforeSet, DoButtonAddRemoveStockTextAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoButtonAddRemoveStockTextBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoButtonAddRemoveStockTextAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyButtonAddRemoveStockTextDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsDisconnect ===

				public const string IsDisconnect_PROPERTYNAME = "IsDisconnect";

				private bool _IsDisconnect;
				///<summary>
				/// Propriété : IsDisconnect
				///</summary>
				public bool IsDisconnect
				{
					get
					{
						return GetValue<bool>(() => _IsDisconnect);
					}
					set
					{
						SetValue<bool>(() => _IsDisconnect, (v) => _IsDisconnect = v, value, IsDisconnect_PROPERTYNAME,  DoIsDisconnectBeforeSet, DoIsDisconnectAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsDisconnectBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsDisconnectAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsDisconnectDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : TextLabelPopinYesNo ===

				public const string TextLabelPopinYesNo_PROPERTYNAME = "TextLabelPopinYesNo";

				private string _TextLabelPopinYesNo;
				///<summary>
				/// Propriété : TextLabelPopinYesNo
				///</summary>
				public string TextLabelPopinYesNo
				{
					get
					{
						return GetValue<string>(() => _TextLabelPopinYesNo);
					}
					set
					{
						SetValue<string>(() => _TextLabelPopinYesNo, (v) => _TextLabelPopinYesNo = v, value, TextLabelPopinYesNo_PROPERTYNAME,  DoTextLabelPopinYesNoBeforeSet, DoTextLabelPopinYesNoAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTextLabelPopinYesNoBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTextLabelPopinYesNoAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTextLabelPopinYesNoDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsFromFilterView ===

				public const string IsFromFilterView_PROPERTYNAME = "IsFromFilterView";

				private bool _IsFromFilterView;
				///<summary>
				/// Propriété : IsFromFilterView
				///</summary>
				public bool IsFromFilterView
				{
					get
					{
						return GetValue<bool>(() => _IsFromFilterView);
					}
					set
					{
						SetValue<bool>(() => _IsFromFilterView, (v) => _IsFromFilterView = v, value, IsFromFilterView_PROPERTYNAME,  DoIsFromFilterViewBeforeSet, DoIsFromFilterViewAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsFromFilterViewBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsFromFilterViewAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsFromFilterViewDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ListStatuts ===

				public const string ListStatuts_PROPERTYNAME = "ListStatuts";

				private System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel> _ListStatuts;
				///<summary>
				/// Propriété : ListStatuts
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel> ListStatuts
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel>>(() => _ListStatuts);
					}
					private set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel>>(() => _ListStatuts, (v) => _ListStatuts = v, value, ListStatuts_PROPERTYNAME,  DoListStatutsBeforeSet, DoListStatutsAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoListStatutsBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionListStatuts_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoListStatutsAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionListStatuts_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyListStatutsDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ClosePopInCommand ===

				public const string ClosePopInCommand_PROPERTYNAME = "ClosePopInCommand";

				private IDelegateCommand _ClosePopInCommand;
				///<summary>
				/// Propriété : ClosePopInCommand
				///</summary>
				public IDelegateCommand ClosePopInCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ClosePopInCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ClosePopInCommand, (v) => _ClosePopInCommand = v, value, ClosePopInCommand_PROPERTYNAME,  DoClosePopInCommandBeforeSet, DoClosePopInCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoClosePopInCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoClosePopInCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyClosePopInCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ChangeDateCommand ===

				public const string ChangeDateCommand_PROPERTYNAME = "ChangeDateCommand";

				private IDelegateCommand _ChangeDateCommand;
				///<summary>
				/// Propriété : ChangeDateCommand
				///</summary>
				public IDelegateCommand ChangeDateCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ChangeDateCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ChangeDateCommand, (v) => _ChangeDateCommand = v, value, ChangeDateCommand_PROPERTYNAME,  DoChangeDateCommandBeforeSet, DoChangeDateCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoChangeDateCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoChangeDateCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyChangeDateCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UpdateStatutCommand ===

				public const string UpdateStatutCommand_PROPERTYNAME = "UpdateStatutCommand";

				private IDelegateCommand _UpdateStatutCommand;
				///<summary>
				/// Propriété : UpdateStatutCommand
				///</summary>
				public IDelegateCommand UpdateStatutCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _UpdateStatutCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _UpdateStatutCommand, (v) => _UpdateStatutCommand = v, value, UpdateStatutCommand_PROPERTYNAME,  DoUpdateStatutCommandBeforeSet, DoUpdateStatutCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUpdateStatutCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUpdateStatutCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUpdateStatutCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : UpdateStockCommand ===

				public const string UpdateStockCommand_PROPERTYNAME = "UpdateStockCommand";

				private IDelegateCommand _UpdateStockCommand;
				///<summary>
				/// Propriété : UpdateStockCommand
				///</summary>
				public IDelegateCommand UpdateStockCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _UpdateStockCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _UpdateStockCommand, (v) => _UpdateStockCommand = v, value, UpdateStockCommand_PROPERTYNAME,  DoUpdateStockCommandBeforeSet, DoUpdateStockCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoUpdateStockCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoUpdateStockCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyUpdateStockCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ClickYesCommand ===

				public const string ClickYesCommand_PROPERTYNAME = "ClickYesCommand";

				private IDelegateCommand _ClickYesCommand;
				///<summary>
				/// Propriété : ClickYesCommand
				///</summary>
				public IDelegateCommand ClickYesCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ClickYesCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ClickYesCommand, (v) => _ClickYesCommand = v, value, ClickYesCommand_PROPERTYNAME,  DoClickYesCommandBeforeSet, DoClickYesCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoClickYesCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoClickYesCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyClickYesCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ClickNoCommand ===

				public const string ClickNoCommand_PROPERTYNAME = "ClickNoCommand";

				private IDelegateCommand _ClickNoCommand;
				///<summary>
				/// Propriété : ClickNoCommand
				///</summary>
				public IDelegateCommand ClickNoCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ClickNoCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ClickNoCommand, (v) => _ClickNoCommand = v, value, ClickNoCommand_PROPERTYNAME,  DoClickNoCommandBeforeSet, DoClickNoCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoClickNoCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoClickNoCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyClickNoCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FilterStatutCommand ===

				public const string FilterStatutCommand_PROPERTYNAME = "FilterStatutCommand";

				private IDelegateCommand _FilterStatutCommand;
				///<summary>
				/// Propriété : FilterStatutCommand
				///</summary>
				public IDelegateCommand FilterStatutCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _FilterStatutCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _FilterStatutCommand, (v) => _FilterStatutCommand = v, value, FilterStatutCommand_PROPERTYNAME,  DoFilterStatutCommandBeforeSet, DoFilterStatutCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFilterStatutCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFilterStatutCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFilterStatutCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux collections ===

			// Notez que les propriétés de collection sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Collection : ListStatuts ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection ListStatuts aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionListStatuts_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionListStatuts_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionListStatuts_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection ListStatuts (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionListStatuts_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionListStatuts_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionListStatuts_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection ListStatuts aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionListStatuts_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionListStatuts_ItemPropertyChanged;
							OnListStatutsItemAdded((StatutItemViewModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection ListStatuts (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionListStatuts_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnListStatutsItemRemoved((StatutItemViewModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionListStatuts_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionListStatuts_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionListStatuts_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionListStatuts_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ListStatuts_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyListStatutsDependencies();
				}

				private void WatchCollectionListStatuts_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ListStatuts_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyListStatutsDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection ListStatuts.
				///</summary>
				protected virtual void OnListStatutsItemAdded(StatutItemViewModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection ListStatuts.
				///</summary>
				protected virtual void OnListStatutsItemRemoved(StatutItemViewModel p_Item)
				{
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux commandes ===

			// Notez que les propriétés de commandes sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Commande : ClosePopInCommand ===


				private bool ClosePopInCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnClosePopInCommand_CanExecute(l_Parameter);
				}

				private void ClosePopInCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnClosePopInCommand_Execute(l_Parameter);
				}

				protected virtual bool OnClosePopInCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnClosePopInCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ChangeDateCommand ===


				private bool ChangeDateCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnChangeDateCommand_CanExecute(l_Parameter);
				}

				private void ChangeDateCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnChangeDateCommand_Execute(l_Parameter);
				}

				protected virtual bool OnChangeDateCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnChangeDateCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : UpdateStatutCommand ===


				private bool UpdateStatutCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnUpdateStatutCommand_CanExecute(l_Parameter);
				}

				private void UpdateStatutCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnUpdateStatutCommand_Execute(l_Parameter);
				}

				protected virtual bool OnUpdateStatutCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnUpdateStatutCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : UpdateStockCommand ===


				private bool UpdateStockCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnUpdateStockCommand_CanExecute(l_Parameter);
				}

				private void UpdateStockCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnUpdateStockCommand_Execute(l_Parameter);
				}

				protected virtual bool OnUpdateStockCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnUpdateStockCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ClickYesCommand ===


				private bool ClickYesCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnClickYesCommand_CanExecute(l_Parameter);
				}

				private void ClickYesCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnClickYesCommand_Execute(l_Parameter);
				}

				protected virtual bool OnClickYesCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnClickYesCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ClickNoCommand ===


				private bool ClickNoCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnClickNoCommand_CanExecute(l_Parameter);
				}

				private void ClickNoCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnClickNoCommand_Execute(l_Parameter);
				}

				protected virtual bool OnClickNoCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnClickNoCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : FilterStatutCommand ===


				private bool FilterStatutCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnFilterStatutCommand_CanExecute(l_Parameter);
				}

				private void FilterStatutCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnFilterStatutCommand_Execute(l_Parameter);
				}

				protected virtual bool OnFilterStatutCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnFilterStatutCommand_Execute(object p_Parameter);

			#endregion


		#endregion

	}

	public partial class PopInViewModel : PopInViewModelBase
	{
	}
}
