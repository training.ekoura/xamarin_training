﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML OrdersViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  Sodexo.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;

namespace MyRestauApp.Common.ViewModels
{
	public abstract partial class OrdersViewModelBase : Sodexo.ViewModel
	{
		protected override void OnInitialize()
		{
			base.OnInitialize();

			// Initialisation de la collection ListOrders.
			ListOrders = new System.Collections.ObjectModel.ObservableCollection<OrderViewModelItem>();
			// Initialisation de la collection ListStatusToFilter.
			ListStatusToFilter = new System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel>();
			// Initialisation de la commande ChangeStatutCommand.
			ChangeStatutCommand = new DelegateCommand(ChangeStatutCommand_CanExecute, ChangeStatutCommand_Execute);
			// Initialisation de la commande OpenDetailViewCommand.
			OpenDetailViewCommand = new DelegateCommand(OpenDetailViewCommand_CanExecute, OpenDetailViewCommand_Execute);
			// Initialisation de la commande FilterStatutCommand.
			FilterStatutCommand = new DelegateCommand(FilterStatutCommand_CanExecute, FilterStatutCommand_Execute);
			// Initialisation de la commande ChangeDateCommand.
			ChangeDateCommand = new DelegateCommand(ChangeDateCommand_CanExecute, ChangeDateCommand_Execute);
		}

		#region === Propriétés ===

			#region === Propriété : DateDashBoard ===

				public const string DateDashBoard_PROPERTYNAME = "DateDashBoard";

				private string _DateDashBoard;
				///<summary>
				/// Propriété : DateDashBoard
				///</summary>
				public string DateDashBoard
				{
					get
					{
						return GetValue<string>(() => _DateDashBoard);
					}
					set
					{
						SetValue<string>(() => _DateDashBoard, (v) => _DateDashBoard = v, value, DateDashBoard_PROPERTYNAME,  DoDateDashBoardBeforeSet, DoDateDashBoardAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDateDashBoardBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDateDashBoardAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDateDashBoardDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DateDashBoardFormattedLabel ===

				public const string DateDashBoardFormattedLabel_PROPERTYNAME = "DateDashBoardFormattedLabel";

				private string _DateDashBoardFormattedLabel;
				///<summary>
				/// Propriété : DateDashBoardFormattedLabel
				///</summary>
				public string DateDashBoardFormattedLabel
				{
					get
					{
						return GetValue<string>(() => _DateDashBoardFormattedLabel);
					}
					set
					{
						SetValue<string>(() => _DateDashBoardFormattedLabel, (v) => _DateDashBoardFormattedLabel = v, value, DateDashBoardFormattedLabel_PROPERTYNAME,  DoDateDashBoardFormattedLabelBeforeSet, DoDateDashBoardFormattedLabelAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDateDashBoardFormattedLabelBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDateDashBoardFormattedLabelAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDateDashBoardFormattedLabelDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ZoneRestaurationId ===

				public const string ZoneRestaurationId_PROPERTYNAME = "ZoneRestaurationId";

				private string _ZoneRestaurationId;
				///<summary>
				/// Propriété : ZoneRestaurationId
				///</summary>
				public string ZoneRestaurationId
				{
					get
					{
						return GetValue<string>(() => _ZoneRestaurationId);
					}
					set
					{
						SetValue<string>(() => _ZoneRestaurationId, (v) => _ZoneRestaurationId = v, value, ZoneRestaurationId_PROPERTYNAME,  DoZoneRestaurationIdBeforeSet, DoZoneRestaurationIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoZoneRestaurationIdBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoZoneRestaurationIdAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyZoneRestaurationIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsNoOrdersDispo ===

				public const string IsNoOrdersDispo_PROPERTYNAME = "IsNoOrdersDispo";

				private bool _IsNoOrdersDispo;
				///<summary>
				/// Propriété : IsNoOrdersDispo
				///</summary>
				public bool IsNoOrdersDispo
				{
					get
					{
						return GetValue<bool>(() => _IsNoOrdersDispo);
					}
					set
					{
						SetValue<bool>(() => _IsNoOrdersDispo, (v) => _IsNoOrdersDispo = v, value, IsNoOrdersDispo_PROPERTYNAME,  DoIsNoOrdersDispoBeforeSet, DoIsNoOrdersDispoAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsNoOrdersDispoBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsNoOrdersDispoAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsNoOrdersDispoDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DoRefresh ===

				public const string DoRefresh_PROPERTYNAME = "DoRefresh";

				private bool _DoRefresh;
				///<summary>
				/// Propriété : DoRefresh
				///</summary>
				public bool DoRefresh
				{
					get
					{
						return GetValue<bool>(() => _DoRefresh);
					}
					set
					{
						SetValue<bool>(() => _DoRefresh, (v) => _DoRefresh = v, value, DoRefresh_PROPERTYNAME,  DoDoRefreshBeforeSet, DoDoRefreshAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDoRefreshBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDoRefreshAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDoRefreshDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : IsVisibleFilterEnable ===

				public const string IsVisibleFilterEnable_PROPERTYNAME = "IsVisibleFilterEnable";

				private bool _IsVisibleFilterEnable;
				///<summary>
				/// Propriété : IsVisibleFilterEnable
				///</summary>
				public bool IsVisibleFilterEnable
				{
					get
					{
						return GetValue<bool>(() => _IsVisibleFilterEnable);
					}
					set
					{
						SetValue<bool>(() => _IsVisibleFilterEnable, (v) => _IsVisibleFilterEnable = v, value, IsVisibleFilterEnable_PROPERTYNAME,  DoIsVisibleFilterEnableBeforeSet, DoIsVisibleFilterEnableAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIsVisibleFilterEnableBeforeSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIsVisibleFilterEnableAfterSet(string p_PropertyName, bool p_OldValue, bool p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIsVisibleFilterEnableDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ListOrders ===

				public const string ListOrders_PROPERTYNAME = "ListOrders";

				private System.Collections.ObjectModel.ObservableCollection<OrderViewModelItem> _ListOrders;
				///<summary>
				/// Propriété : ListOrders
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<OrderViewModelItem> ListOrders
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<OrderViewModelItem>>(() => _ListOrders);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<OrderViewModelItem>>(() => _ListOrders, (v) => _ListOrders = v, value, ListOrders_PROPERTYNAME,  DoListOrdersBeforeSet, DoListOrdersAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoListOrdersBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<OrderViewModelItem> p_OldValue, System.Collections.ObjectModel.ObservableCollection<OrderViewModelItem> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionListOrders_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoListOrdersAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<OrderViewModelItem> p_OldValue, System.Collections.ObjectModel.ObservableCollection<OrderViewModelItem> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionListOrders_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyListOrdersDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ListStatusToFilter ===

				public const string ListStatusToFilter_PROPERTYNAME = "ListStatusToFilter";

				private System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel> _ListStatusToFilter;
				///<summary>
				/// Propriété : ListStatusToFilter
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel> ListStatusToFilter
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel>>(() => _ListStatusToFilter);
					}
					set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel>>(() => _ListStatusToFilter, (v) => _ListStatusToFilter = v, value, ListStatusToFilter_PROPERTYNAME,  DoListStatusToFilterBeforeSet, DoListStatusToFilterAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoListStatusToFilterBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionListStatusToFilter_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoListStatusToFilterAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<StatutItemViewModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionListStatusToFilter_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyListStatusToFilterDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ChangeStatutCommand ===

				public const string ChangeStatutCommand_PROPERTYNAME = "ChangeStatutCommand";

				private IDelegateCommand _ChangeStatutCommand;
				///<summary>
				/// Propriété : ChangeStatutCommand
				///</summary>
				public IDelegateCommand ChangeStatutCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ChangeStatutCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ChangeStatutCommand, (v) => _ChangeStatutCommand = v, value, ChangeStatutCommand_PROPERTYNAME,  DoChangeStatutCommandBeforeSet, DoChangeStatutCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoChangeStatutCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoChangeStatutCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyChangeStatutCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : OpenDetailViewCommand ===

				public const string OpenDetailViewCommand_PROPERTYNAME = "OpenDetailViewCommand";

				private IDelegateCommand _OpenDetailViewCommand;
				///<summary>
				/// Propriété : OpenDetailViewCommand
				///</summary>
				public IDelegateCommand OpenDetailViewCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _OpenDetailViewCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _OpenDetailViewCommand, (v) => _OpenDetailViewCommand = v, value, OpenDetailViewCommand_PROPERTYNAME,  DoOpenDetailViewCommandBeforeSet, DoOpenDetailViewCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoOpenDetailViewCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoOpenDetailViewCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyOpenDetailViewCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : FilterStatutCommand ===

				public const string FilterStatutCommand_PROPERTYNAME = "FilterStatutCommand";

				private IDelegateCommand _FilterStatutCommand;
				///<summary>
				/// Propriété : FilterStatutCommand
				///</summary>
				public IDelegateCommand FilterStatutCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _FilterStatutCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _FilterStatutCommand, (v) => _FilterStatutCommand = v, value, FilterStatutCommand_PROPERTYNAME,  DoFilterStatutCommandBeforeSet, DoFilterStatutCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoFilterStatutCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoFilterStatutCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyFilterStatutCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : ChangeDateCommand ===

				public const string ChangeDateCommand_PROPERTYNAME = "ChangeDateCommand";

				private IDelegateCommand _ChangeDateCommand;
				///<summary>
				/// Propriété : ChangeDateCommand
				///</summary>
				public IDelegateCommand ChangeDateCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _ChangeDateCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _ChangeDateCommand, (v) => _ChangeDateCommand = v, value, ChangeDateCommand_PROPERTYNAME,  DoChangeDateCommandBeforeSet, DoChangeDateCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoChangeDateCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoChangeDateCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyChangeDateCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux collections ===

			// Notez que les propriétés de collection sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Collection : ListOrders ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection ListOrders aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionListOrders_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionListOrders_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionListOrders_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection ListOrders (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionListOrders_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionListOrders_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionListOrders_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection ListOrders aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionListOrders_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionListOrders_ItemPropertyChanged;
							OnListOrdersItemAdded((OrderViewModelItem)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection ListOrders (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionListOrders_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnListOrdersItemRemoved((OrderViewModelItem)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionListOrders_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionListOrders_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionListOrders_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionListOrders_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ListOrders_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyListOrdersDependencies();
				}

				private void WatchCollectionListOrders_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ListOrders_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyListOrdersDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection ListOrders.
				///</summary>
				protected virtual void OnListOrdersItemAdded(OrderViewModelItem p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection ListOrders.
				///</summary>
				protected virtual void OnListOrdersItemRemoved(OrderViewModelItem p_Item)
				{
				}

			#endregion

			#region === Collection : ListStatusToFilter ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection ListStatusToFilter aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionListStatusToFilter_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionListStatusToFilter_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionListStatusToFilter_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection ListStatusToFilter (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionListStatusToFilter_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionListStatusToFilter_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionListStatusToFilter_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection ListStatusToFilter aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionListStatusToFilter_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionListStatusToFilter_ItemPropertyChanged;
							OnListStatusToFilterItemAdded((StatutItemViewModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection ListStatusToFilter (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionListStatusToFilter_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnListStatusToFilterItemRemoved((StatutItemViewModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionListStatusToFilter_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionListStatusToFilter_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionListStatusToFilter_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionListStatusToFilter_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ListStatusToFilter_PROPERTYNAME + "[]", null, null));

					// Nous notifions les dépendances.
					NotifyListStatusToFilterDependencies();
				}

				private void WatchCollectionListStatusToFilter_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Nous notifions qu'il y a eu un changement sur la collection.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(ListStatusToFilter_PROPERTYNAME + "[]", null, null));

					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyListStatusToFilterDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection ListStatusToFilter.
				///</summary>
				protected virtual void OnListStatusToFilterItemAdded(StatutItemViewModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection ListStatusToFilter.
				///</summary>
				protected virtual void OnListStatusToFilterItemRemoved(StatutItemViewModel p_Item)
				{
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux commandes ===

			// Notez que les propriétés de commandes sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Commande : ChangeStatutCommand ===


				private bool ChangeStatutCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnChangeStatutCommand_CanExecute(l_Parameter);
				}

				private void ChangeStatutCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnChangeStatutCommand_Execute(l_Parameter);
				}

				protected virtual bool OnChangeStatutCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnChangeStatutCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : OpenDetailViewCommand ===


				private bool OpenDetailViewCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnOpenDetailViewCommand_CanExecute(l_Parameter);
				}

				private void OpenDetailViewCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnOpenDetailViewCommand_Execute(l_Parameter);
				}

				protected virtual bool OnOpenDetailViewCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnOpenDetailViewCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : FilterStatutCommand ===


				private bool FilterStatutCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnFilterStatutCommand_CanExecute(l_Parameter);
				}

				private void FilterStatutCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnFilterStatutCommand_Execute(l_Parameter);
				}

				protected virtual bool OnFilterStatutCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnFilterStatutCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : ChangeDateCommand ===


				private bool ChangeDateCommand_CanExecute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					return OnChangeDateCommand_CanExecute(l_Parameter);
				}

				private void ChangeDateCommand_Execute(object p_Parameter)
				{
					var l_Parameter = (object)p_Parameter;
					OnChangeDateCommand_Execute(l_Parameter);
				}

				protected virtual bool OnChangeDateCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				protected abstract void OnChangeDateCommand_Execute(object p_Parameter);

			#endregion


		#endregion

	}

	public partial class OrdersViewModel : OrdersViewModelBase
	{
	}
}
