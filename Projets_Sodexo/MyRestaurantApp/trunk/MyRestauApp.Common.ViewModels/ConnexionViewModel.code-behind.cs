
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MyRestauApp.Common.DAOServices;
using MyRestauApp.Common.DAOServices.Requests;
using Sodexo;
using System.Globalization;
using MyRestauApp.Common.DAOServices.Models;
using MyRestauApp.Common.DAOServices.Services;

namespace MyRestauApp.Common.ViewModels
{
    public partial class ConnexionViewModel : ConnexionViewModelBase, IConnexionViewModel
    {
        #region Proprietes
        private MyRestauApp.Common.DAOServices.Services.IAuthService AuthService;
        //private MyRestauApp.Common.Services.IGlobalConfigService GlobalConfigService;
        private string Locale = Properties.Resources.CultureInfoCurrentCultureName;
        private CultureInfo Culture;
        #endregion


        #region Initialisation
        /// <summary>
        /// Initialisation
        /// </summary>
        protected override void OnInitialize()
        {
            base.OnInitialize();
            AuthService = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.DAOServices.Services.IAuthService>();
            Culture = new CultureInfo(Properties.Resources.CultureInfoCurrentCultureName.Replace("_", "-"));

        }
        #endregion

        protected override void OnValidate(IValidationContext p_Context)
        {
            base.OnValidate(p_Context);
            if (string.IsNullOrWhiteSpace(LoginEmail))
                p_Context.AddError(Properties.Resources.RequiredEntryField, LoginEmail_PROPERTYNAME);
            if (string.IsNullOrWhiteSpace(LoginPassword)) p_Context.AddError(Properties.Resources.RequiredEntryField, LoginPassword_PROPERTYNAME);
        }

        protected override async void OnLoginCommand_Execute(object p_Parameter)
        {
            var isValide = Validate();
            if (isValide)
            {
                try
                {
                    BeginWork();
                    var GlobalConfigService = Sodexo.Framework.Services.GlobalConfigService();
                    Culture = new CultureInfo(Properties.Resources.CultureInfoCurrentCultureName.Replace("_", "-"));

                    var request = new ConnectRequest()
                    {
                        LoginOrMail = LoginEmail,
                        Pwd = LoginPassword,
                        Locale = Locale,
                        AppName = GlobalConfigService.Data.AppName,
                        AppVersion = GlobalConfigService.Data.AppVersion,
                        Os = GlobalConfigService.Data.DeviceOs
                    };
                    var DataOfflineService = Sodexo.Framework.Services.DataOfflineService();
                    var userDataOffline = DataOfflineService.Data != null && DataOfflineService.Data.ListUserData != null ? DataOfflineService.Data.ListUserData.Where(x => x.LoginUser == LoginEmail).FirstOrDefault() : null;
                    AuthService = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.DAOServices.Services.IAuthService>();
                    var connectResult = await AuthService.ConnectAsync(request);
                    var canConnect = false;
                    var isOffline = false;
                    if (connectResult.Success)
                    {
                        var commandService = Sodexo.Framework.Services.Container().Resolve<ICommandeService>();
                        //Récupération des status possibles pour filtrer
                        var statutsResult = await commandService.GetListCommandeStatutsAsync(Properties.Resources.CultureInfoCurrentCultureName);
                        if (statutsResult.Success && statutsResult.Data != null)
                        {
                            int i = 1;
                            foreach (var statut in statutsResult.Data)
                            {
                                statut.IsChecked = true;
                                statut.IdForView = i;
                                i++;
                            }
                            GlobalConfigService.Data.ListStatutSaved = new List<DAOServices.Models.StatutCommande>();
                            GlobalConfigService.Data.ListStatutSaved.AddRange(statutsResult.Data);
                            GlobalConfigService.SaveConfig();
                        }
                        if (userDataOffline == null)
                            DataOfflineService.Data.ListUserData.Add(new DAOServices.Models.UserData()
                            {
                                LoginUser = LoginEmail,
                                DateWhenLastConnected = DateTime.Now,
                                DashboradDataByZR = new List<DashboardDataByZR>()
                            { 
                                new DashboardDataByZR()
                                { 
                                DashboardUserByDate = new List<DashboardUserByDate>()
                                {
                                    new DashboardUserByDate()
                                    {
                                        DateDebut = DateTime.Now.Date.ToString("dd/MM/yyyy HH:mm:ss", Culture),
                                        ListStocks = new List<StockArticle>()
                                    }
                                }
                            }
                          }
                            });
                        DataOfflineService.Data.ListUserData.Where(x => x.LoginUser == LoginEmail).FirstOrDefault().UserId = connectResult.Data.UserSolId;
                        DataOfflineService.Data.CurrentUser = DataOfflineService.Data.ListUserData.Where(x => x.LoginUser == LoginEmail).FirstOrDefault();
                        DataOfflineService.SaveConfig();
                        GlobalConfigService.Data.Token = connectResult.Data;
                        GlobalConfigService.SaveConfig();
                        canConnect = true;
                    }
                    else
                    {
                        if (connectResult.ExceptionComesFromNetwork)
                        {
                            if (userDataOffline != null && userDataOffline.DashboradDataByZR != null &&
                                 userDataOffline.DashboradDataByZR.FirstOrDefault().DashboardUserByDate != null
                                && userDataOffline.DashboradDataByZR.FirstOrDefault().DashboardUserByDate.FirstOrDefault().CommandeDashboard != null)
                            {
                                DataOfflineService.Data.CurrentUser = userDataOffline;
                                DataOfflineService.SaveConfig();
                                //J'ai des data je dois les récupérer en mode offline
                                canConnect = true;
                                isOffline = true;
                            }
                            else
                            {
                                await Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, connectResult.ExceptionMessage);
                            }

                        }
                        else
                            await Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, connectResult.ExceptionMessage);
                    }
                    if (canConnect)
                    {
                        var burgerVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IBurgerViewModel>();
                        var successBurger = await burgerVM.PrepareVM();
                        if (successBurger)
                        {
                            var DashBoardVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IDashboardViewModel>();
                            DashBoardVM.BurgerVM = burgerVM;
                            var success = await DashBoardVM.PrepareDashboardVM(isOffline);
                            if (success)
                                await Sodexo.Framework.Services.InteractionService().Open(ViewName.MasterDetailPage, DashBoardVM);
                        }
                    }
                }
                catch (Exception e)
                {
                    //Afficher message d'erreur
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
                }
                finally
                {
                    EndWork();
                }
            }
        }
    }
}
