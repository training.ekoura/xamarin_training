
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;
using MyRestauApp.Common.DAOServices.Services;
using System.Globalization;
using System.Threading.Tasks;
using MyRestauApp.Common.DAOServices.Models;
using MyRestauApp.Common.DAOServices.Requests;

namespace MyRestauApp.Common.ViewModels
{
    public partial class OrdersViewModel : OrdersViewModelBase, IOrdersViewModel
    {
        #region Proprietes
        private ICommandeService CommandService;
        private IStockService StockService;
        private IUserService UserService;
        private IGlobalConfigService GlobalConfigService;
        private IDataOfflineService DataOfflineService;
        private bool IsWorkingView = false;
        private CultureInfo Culture;
        DashboardUserByDate DashboardSaved;
        #endregion

        #region Initialisation

        protected override void OnInitialize()
        {
            base.OnInitialize();
            ResolveInitialization();
        }
        private void ResolveInitialization()
        {
            CommandService = Sodexo.Framework.Services.Container().Resolve<ICommandeService>();
            StockService = Sodexo.Framework.Services.Container().Resolve<IStockService>();
            UserService = Sodexo.Framework.Services.Container().Resolve<IUserService>();
            GlobalConfigService = Sodexo.Framework.Services.GlobalConfigService();
            DataOfflineService = Sodexo.Framework.Services.DataOfflineService();
            Culture = new CultureInfo(Properties.Resources.CultureInfoCurrentCultureName.Replace("_", "-"));
        }

        #endregion End Initialisation

        public async Task<bool> PrepareOrdersVM(string date, string zoneRestaurationId)
        {
            try
            {
                BeginWork();
                return await PrepareOrdersList(date, zoneRestaurationId);

            }
            catch (Exception ex)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                return false;
            }
            finally
            {
                EndWork();
            }
        }


        public async Task<bool> PrepareOrdersList(string date, string zoneRestaurationId)
        {
            ResolveInitialization();
            //Intialisation des variables
            ZoneRestaurationId = zoneRestaurationId;
            IsVisibleFilterEnable = false;
            DateDashBoard = date;
            //Ajoute le filtre par statut
            ListStatusToFilter.Clear();
            GlobalConfigService.LoadConfig();
            if (GlobalConfigService.Data.ListStatutSaved != null && GlobalConfigService.Data.ListStatutSaved.Count > 0)
                ListStatusToFilter.AddRange(GlobalConfigService.Data.ListStatutSaved.Select(x => new StatutItemViewModel().SetCommandStatutEncapsulation(x)));
            var listStatutLogicValue = string.Join(",", ListStatusToFilter.Where(x => x.IsChecked == true).ToList().Select(y => y.LogicValue));
            IsVisibleFilterEnable = ListStatusToFilter != null ? (ListStatusToFilter.Any(x => x.IsChecked == false) ? true : false) : false;
            return await RefreshOrdersList(date, listStatutLogicValue);
        }
        public async Task<bool> RefreshOrdersList(string date, string listStatut = null)
        {
            try
            {

                var dateValue = DateTime.ParseExact(date, "dd/MM/yyyy HH:mm:ss", Culture).Date;
                DateDashBoard = dateValue.ToString("dd/MM/yyyy HH:mm:ss", Culture);
                var formattedDate = string.Empty;
                if (dateValue == DateTime.Now.Date)
                    DateDashBoardFormattedLabel = Properties.Resources.ForToday + " - " + DateTime.ParseExact(DateDashBoard, "dd/MM/yyyy HH:mm:ss", Culture).ToString("d MMMM", Culture);
                else
                    DateDashBoardFormattedLabel = DateTime.ParseExact(DateDashBoard, "dd/MM/yyyy HH:mm:ss", Culture).ToString("dddd d MMMM", Culture);

                try
                {
                    //R�cup�rer la commande enregistr� � utiliser en cas de besoin
                    DashboardSaved = DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                              .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).FirstOrDefault().DashboardUserByDate.Where(x => x.DateDebut == date).FirstOrDefault();
                }
                catch
                {
                    DashboardSaved = null;
                }
                var requestDashboardGerant = new GetDashboardGerantRequest()
                {
                    Token = GlobalConfigService.Data.Token.TokenVal,
                    DateDebut = date,
                    DateFin = date,
                    ZoneRestaurationId = ZoneRestaurationId,
                    ListCommandesStatutLogicValue = listStatut,
                    Delimiter = ","
                };
                var result = await CommandService.GetCommandesGerantAsync(requestDashboardGerant);
                if (result.Success)
                {
                    if (result.Data != null)
                    {
                        List<Commande> listCommandes = result.Data.OrderByDescending(x => x.Id).ToList();
                        IsNoOrdersDispo = (listCommandes != null && listCommandes.ToList().Count > 0) ? false : true;
                        foreach (var order in listCommandes)
                        {
                            order.FormattedDateForView = DateTime.ParseExact(order.DateEffectiveFormat, "dd/MM/yyyy HH:mm", Culture).Date.ToString("dd.MM");
                            order.FormattedLabelForView = string.Format(Properties.Resources.LabelCommandeForView, order.ArticlesPlatsCount, order.ArticlesCount);
                            order.FormattedLabelFirstLastName = order.UserPortail.FirstName + " " + order.UserPortail.LastName;
                            order.IsNew = GlobalConfigService.Data.ViewedOrdersIds.Contains(order.Id) ? false : true;

                        }

                        var ListOrdersIntermediaire = new System.Collections.ObjectModel.ObservableCollection<OrderViewModelItem>();
                        ListOrdersIntermediaire.Clear();
                        ListOrdersIntermediaire.AddRange(listCommandes.Select(x => new OrderViewModelItem().SetOrderEncapsulation(x)));
                        ListOrders = ListOrdersIntermediaire;
                        //On enregistre pour le mode offline
                        if (DashboardSaved == null)
                        {
                            //j'ajoute la commande dashborad avec sa date
                            DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                              .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).FirstOrDefault().DashboardUserByDate.Add(new DashboardUserByDate() { DateDebut = date, ListCommandes = listCommandes });

                            DashboardSaved = DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                         .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).FirstOrDefault().DashboardUserByDate.Where(x => x.DateDebut == date).FirstOrDefault();
                        }
                        else
                        {
                            //on met � jour les data de commande Dashborad pour cette date 
                            DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                                 .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId)
                                 .FirstOrDefault().DashboardUserByDate.Where(x => x.DateDebut == date)
                                 .FirstOrDefault().ListCommandes = listCommandes;
                        }
                        DataOfflineService.SaveConfig();
                    }
                }
                else
                {
                    var isError = true;
                    if (result.ExceptionComesFromNetwork)
                        isError = DashboardSaved == null || DashboardSaved.ListCommandes == null ? true : false;

                    if (isError)
                    {
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, result.ExceptionMessage);
                        return false;
                    }
                    else
                    {
                        var ListOrdersIntermediaireOffLine = new System.Collections.ObjectModel.ObservableCollection<OrderViewModelItem>();
                        ListOrdersIntermediaireOffLine.Clear();
                        ListOrdersIntermediaireOffLine.AddRange(DashboardSaved.ListCommandes.Select(x => new OrderViewModelItem().SetOrderEncapsulation(x)));
                        ListOrders = ListOrdersIntermediaireOffLine;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
                return false;
            }
        }

        #region Commandes de la deuxi�me page
        // Commande au clic sur le bouton pour voir les commandes en cours sur DashboardView 
        protected override async void OnOpenDetailViewCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                var commande = (OrderViewModelItem)p_Parameter;
                var orderDetailsVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IOrderDetailViewModel>();
                orderDetailsVM.ZoneRestaurationId = ZoneRestaurationId;
                var success = await orderDetailsVM.PrepareOrderDetailVM(commande.Id, DateDashBoard);
                if (success)
                    Sodexo.Framework.Services.InteractionService().Open(ViewName.OrderDetailView, orderDetailsVM);
            }
            catch (Exception e)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }

        // Clic sur le statut pour changer le statut de la commande
        protected override async void OnChangeStatutCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                var commande = (OrderViewModelItem)p_Parameter;
                if (commande != null && commande.ListStatutsPossibles != null && commande.ListStatutsPossibles.Count > 0)
                {
                    var popInVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IPopInViewModel>();
                    var result = await CommandService.GetCommandeGerantActionPossibleAsync(GlobalConfigService.Data.Token.TokenVal, commande.Id);
                    if (result.Success)
                    {
                        ListOrders.Where(x => x.Id == commande.Id).FirstOrDefault().ListStatutsPossibles = result.Data;
                        popInVM.PrepareVMForChangeStatut((OrderViewModelItem)p_Parameter, false);
                        await Sodexo.Framework.Services.InteractionService().ViewPopIn(popInVM, ViewName.PlatstatusContentView);
                    }
                    else
                        Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, result.ExceptionMessage);
                }
                else
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.OrderStatutError);
            }
            catch (Exception ex)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
                EndWork();
            }
        }

        protected override async void OnFilterStatutCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                var popInVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IPopInViewModel>();
                popInVM.PrepareVMForFilterStatut();
                await Sodexo.Framework.Services.InteractionService().ViewPopIn(popInVM, ViewName.StatusFilterContentView);
            }
            catch (Exception ex)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
                EndWork();
            }
        }

        public async Task UpdateCommandes(int commandeId, StatutItemViewModel selectedStatut)
        {
            try
            {
                BeginWork();
                var l_NetworkService = Sodexo.Framework.Services.Container().Resolve<INetworkService>();
                if (!l_NetworkService.IsConnected)
                {
                    Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, Properties.Resources.ErrorNetwork);
                }
                else
                {
                    //pas besoin d'appeler le WS pour mettre � jour le statut de la commande, �a nous fait gagner de temps d'ex�cution si on le fait manuellement
                    ListOrders.Where(x => x.Id == commandeId).FirstOrDefault().Statut.LogicValue = selectedStatut.LogicValue;
                    ListOrders.Where(x => x.Id == commandeId).FirstOrDefault().Statut.LibelleGerant = selectedStatut.LibelleGerant;
                    ListOrders.Where(x => x.Id == commandeId).FirstOrDefault().Statut.StatutCodeCouleur = selectedStatut.StatutCodeCouleur;
                    ListOrders.Where(x => x.Id == commandeId).FirstOrDefault().IsNew = false;

                    var delete = GlobalConfigService.Data.ListStatutSaved.Where(x => x.LogicValue == selectedStatut.LogicValue).FirstOrDefault().IsChecked ? false : true;
                    if (delete)
                        ListOrders.Remove(ListOrders.Where(x => x.Id == commandeId).FirstOrDefault());

                    //On met � jour aussi pour le mode offline
                    //R�cup�rer la commande enregistr� � utiliser en cas de besoin
                    try
                    {
                        if (DashboardSaved != null)
                        {
                            if (DashboardSaved.ListCommandes != null)
                                DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                                       .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).First().DashboardUserByDate.Where(x => x.DateDebut == DateDashBoard).FirstOrDefault().ListCommandes.Clear();
                            else
                                DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                                  .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).First().DashboardUserByDate.Where(x => x.DateDebut == DateDashBoard).FirstOrDefault().ListCommandes = new List<Commande>();

                            DataOfflineService.Data.ListUserData.Where(x => x.UserId == GlobalConfigService.Data.Token.UserSolId)
                                       .FirstOrDefault().DashboradDataByZR.Where(x => x.ZoneRestaurationId == ZoneRestaurationId).First().DashboardUserByDate.Where(x => x.DateDebut == DateDashBoard).FirstOrDefault().ListCommandes.AddRange(ListOrders.Select(x => x.GetOrderEncapsulation()));

                            DataOfflineService.SaveConfig();
                        }
                    }
                    catch
                    {
                        //Pas de mise � jour possible , on fait rien pour l'instant
                    }
                }
            }
            catch (Exception e)
            {
                //Afficher message d'erreur
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, e.Message);
            }
            finally
            {
                EndWork();
            }
        }


        protected override async void OnChangeDateCommand_Execute(object p_Parameter)
        {
            try
            {
                BeginWork();
                var popInVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IPopInViewModel>();
                //var selectedDate = DateTime.ParseExact(DateDashBoard, "dd/MM/yyyy HH:mm:ss", Culture).ToString("dd/MM/yyyy", Culture);
                var selectedDate = DateTime.ParseExact(DateDashBoard, "dd/MM/yyyy HH:mm:ss", Culture).Date;
                popInVM.IsFromListView = true;
                popInVM.PrepareVMForChangeDate(selectedDate);
                await Sodexo.Framework.Services.InteractionService().ViewPopIn(popInVM, ViewName.CalndarContentView);
            }
            catch (Exception ex)
            {
                Sodexo.Framework.Services.InteractionService().Alert(Properties.Resources.Error, ex.Message);
            }
            finally
            {
                EndWork();
            }
        }




        #endregion
        public async Task<bool> RefreshOrdersListWithBeginEndWork(string date, string listStatut = null)
        {
            try
            {
                BeginWork();
                return await RefreshOrdersList(date, listStatut);
            }
            finally
            {
                EndWork();
            }
        }

    }
}
