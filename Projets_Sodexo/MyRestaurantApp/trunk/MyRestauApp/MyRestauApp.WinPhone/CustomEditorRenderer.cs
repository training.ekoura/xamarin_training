﻿using System;
using Xamarin.Forms.Platform.WinPhone;
using Xamarin.Forms;
using MySodexo;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Sodexo.Controls;
using MyRestauApp;



[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]

namespace MyRestauApp
{
    public class CustomEditorRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                Control.BorderThickness = new System.Windows.Thickness(1);
                Control.BorderBrush = Helper.ColorToBrush("#cccccc");
                Control.Foreground = Helper.ColorToBrush("#000000");

                var editor = e.NewElement as CustomEditor;
                Control.FontSize = editor.FontSize;
            }
        }
    }
}
