﻿using System;
using Xamarin.Forms.Platform.WinPhone;
using Xamarin.Forms;
using MySodexo;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Microsoft.Phone.Controls;
using MyRestauApp;
using Sodexo.Controls;


[assembly: ExportRenderer(typeof(Entry), typeof(CustomEntryRenderer))]

namespace MyRestauApp
{
    public class CustomEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                var textBox = (PhoneTextBox)Control.Children[0];

                //the entry must have a styleId to "StockEntry" to hide borders
                if (e.NewElement.StyleId != "StockEntry")
                {
                    textBox.BorderBrush = Helper.ColorToBrush("#cccccc");
                    textBox.BorderThickness = new System.Windows.Thickness(1);
                }
                textBox.MaxLength= (e.NewElement as CustomEntry).MaxLength;
            }
        }
    }
}
