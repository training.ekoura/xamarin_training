using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyRestauApp
{
    class ApplicationService : Sodexo.IApplicationService
    {
        public void Close()
        {
            System.Windows.Application.Current.Terminate();
        }
    }
}