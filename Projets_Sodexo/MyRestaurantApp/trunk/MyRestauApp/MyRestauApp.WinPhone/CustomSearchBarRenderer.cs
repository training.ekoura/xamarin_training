﻿using System;
using Xamarin.Forms.Platform.WinPhone;
using Xamarin.Forms;
using MySodexo;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using MyRestauApp;



[assembly: ExportRenderer(typeof(SearchBar), typeof(CustomSearchBarRenderer))]
namespace MyRestauApp
{
    public class CustomSearchBarRenderer : SearchBarRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<SearchBar> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                Control.BorderThickness = new System.Windows.Thickness(1);
                Control.BorderBrush = Helper.ColorToBrush("#cccccc");
            }
        }
    }
}
