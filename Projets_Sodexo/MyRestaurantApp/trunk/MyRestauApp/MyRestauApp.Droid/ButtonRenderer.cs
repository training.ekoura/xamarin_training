using System;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using MyRestauApp;

[assembly: ExportRenderer (typeof (Button), typeof (ListButtonRenderer))]


namespace MyRestauApp
{
	public class ListButtonRenderer : ButtonRenderer
	{

		protected override void OnElementChanged (ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged (e);

			Control.Focusable = false;
		}
	}
}