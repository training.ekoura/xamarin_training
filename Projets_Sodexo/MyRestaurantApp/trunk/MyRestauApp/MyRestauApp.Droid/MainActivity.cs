﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using MyRestauApp.Common;
using ImageCircle.Forms.Plugin.Droid;
using Android.Graphics.Drawables;

namespace MyRestauApp.Droid
{
    [Activity(Label = "Mes Outils", Icon = "@drawable/icon", MainLauncher = true, 
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        ScreenOrientation = ScreenOrientation.Portrait, LaunchMode = LaunchMode.SingleTask)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            global::MyRestauApp.Initializer.RegisterAll();
            // Desable Navigation bar on Android
            global::Xamarin.Forms.Forms.SetTitleBarVisibility(Xamarin.Forms.AndroidTitleBarVisibility.Never);


            ImageCircleRenderer.Init();

            if ((int)Android.OS.Build.VERSION.SdkInt >= (int)Android.OS.BuildVersionCodes.Lollipop)
            {
                Window w = Window;
                w.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
                Window.SetStatusBarColor(Android.Graphics.Color.Rgb(193,4,24));
            }          
          
            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new MyRestauApp.Common.App("my-sodexo"));
        }
    }
}

