using System;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using MyRestauApp;
using Android.Widget;
using Android.Views;
using Sodexo.Controls;
using Android.Graphics;


[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]

namespace MyRestauApp
{
    public class CustomEditorRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                var shape = new Android.Graphics.Drawables.ShapeDrawable(new Android.Graphics.Drawables.Shapes.RectShape());
                shape.Paint.Color = Xamarin.Forms.Color.FromHex("#cccccc").ToAndroid();
                shape.Paint.StrokeWidth = 5;
                shape.Paint.SetStyle(Android.Graphics.Paint.Style.Stroke);

                Control.SetBackgroundDrawable(shape);

                var editor = e.NewElement as CustomEditor;
                Control.TextSize = (float)editor.FontSize;
                Control.SetTextColor(Xamarin.Forms.Color.Black.ToAndroid());
                Control.SetHintTextColor(Xamarin.Forms.Color.FromHex("#cccccc").ToAndroid());
                Control.Hint = editor.PlaceHolder;
            }
        }
    }
}