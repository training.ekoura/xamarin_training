using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using System;
using MyRestauApp;

[assembly: ExportRenderer (typeof (ListView), typeof (ScrollListViewRenderer))]

namespace MyRestauApp
{
    class ScrollListViewRenderer : ListViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);

            Control.Focusable = false;
        }
    }
}
