﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Sodexo;
using Android.Net;

namespace MyRestauApp
{
    internal class NetworkService : ObservableObject, INetworkService
    {
        public NetworkService()
        {
            _NetworkStatusBroadcastReceiver = new NetworkStatusBroadcastReceiver(Update);
            Application.Context.RegisterReceiver(_NetworkStatusBroadcastReceiver, new IntentFilter(ConnectivityManager.ConnectivityAction));
            Update();
        }

        private NetworkStatusBroadcastReceiver _NetworkStatusBroadcastReceiver;

        private bool _IsConnected;
        public bool IsConnected
        {
            get { return GetValue<bool>(() => _IsConnected); }
            private set { SetValue<bool>(() => _IsConnected, (b) => { _IsConnected = b; }, value, "IsConnected", null, NotifyIsConnectedChanged); }
        }

        private void NotifyIsConnectedChanged(string s, bool p_OldValue, bool p_NewValue)
        {
            if (StateChanged != null)
            {
                StateChanged(this, new EventArgs());
            }
        }

        public event EventHandler<EventArgs> StateChanged;

        private void Update()
        {
            var connectivityManager = (ConnectivityManager)Application.Context.GetSystemService(Context.ConnectivityService);
            var activeNetworkInfo = connectivityManager.ActiveNetworkInfo;
            if (activeNetworkInfo != null && activeNetworkInfo.IsConnectedOrConnecting)
            {
                IsConnected = true;
            }
            else
            {
                IsConnected = false;
            }
        }
    }

    [BroadcastReceiver()]
    public class NetworkStatusBroadcastReceiver : BroadcastReceiver
    {
        public NetworkStatusBroadcastReceiver()
        {

        }

        public NetworkStatusBroadcastReceiver(Action p_Callback)
        {
            _Callback = p_Callback;
        }

        Action _Callback;

        public override void OnReceive(Context context, Intent intent)
        {
            _Callback();
        }
    }
}