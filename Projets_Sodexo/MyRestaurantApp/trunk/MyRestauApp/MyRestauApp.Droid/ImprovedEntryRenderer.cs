using System;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using MyRestauApp;
using Android.Widget;
using Android.Views;
using Sodexo.Controls;
using Android.Views.InputMethods;


[assembly: ExportRenderer(typeof(Entry), typeof(ImprovedEntryRenderer))]

namespace MyRestauApp
{
    public class ImprovedEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                var nativeEditText = Control;

                //the entry must have a styleId to "NoBorderEntry" to hide borders
                if (e.NewElement.StyleId != "NoBorderEntry")
                {
                    var shape = new Android.Graphics.Drawables.ShapeDrawable(new Android.Graphics.Drawables.Shapes.RectShape());
                    shape.Paint.Color = Color.FromHex("#cccccc").ToAndroid();
                    shape.Paint.StrokeWidth = 5;
                    shape.Paint.SetStyle(Android.Graphics.Paint.Style.Stroke);

                    nativeEditText.SetBackgroundDrawable(shape);
                }
            }
        }
    }
}
