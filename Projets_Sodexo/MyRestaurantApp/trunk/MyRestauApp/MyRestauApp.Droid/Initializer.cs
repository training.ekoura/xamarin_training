﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sodexo;

namespace MyRestauApp
{
    internal class Initializer
    {
        public static void RegisterAll()
        {
            Sodexo.Framework.Services.Container().Register<IApplicationService, ApplicationService>(new SingletonInstanceManager());
            Sodexo.Framework.Services.Container().Register<INetworkService, NetworkService>(new SingletonInstanceManager());
            Sodexo.Framework.Services.Container().Register<IFileService, FileService>(new SingletonInstanceManager());
            Sodexo.Framework.Services.Container().Register<IGestureService, GestureService>(new SingletonInstanceManager());
            Sodexo.Framework.Services.Container().Register<ILocalizeService, LocalizeService>(new SingletonInstanceManager());
        }
    }
}
