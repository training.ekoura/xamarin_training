using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sodexo.Controls;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using MyRestauApp;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace MyRestauApp
{
    class CustomEditorRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                Control.Layer.BorderWidth = 1;
                Control.Layer.BorderColor = Helper.FromHexString("#cccccc").CGColor;

                var editor = e.NewElement as CustomEditor;
                Control.Font = UIFont.SystemFontOfSize((float)editor.FontSize);
                Control.TextColor = UIColor.Black;
            }
        }
    }
}