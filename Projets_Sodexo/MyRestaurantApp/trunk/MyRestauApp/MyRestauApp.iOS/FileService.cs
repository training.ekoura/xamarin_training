﻿using Sodexo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MyRestauApp
{
    public class FileService : IFileService
    {
        static string sPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public bool Exist(string p_FileName)
        {
            p_FileName = Path.Combine(sPath, p_FileName);
            return System.IO.File.Exists(p_FileName);
        }

        public string GetFileWPath(string p_FileName)
        {
            return System.IO.Path.Combine(sPath, p_FileName);
        }

        public string[] GetFilesInPath()
        {
            return Directory.GetFiles(sPath);
        }


        public void Delete(string p_FileName)
        {
            p_FileName = Path.Combine(sPath, p_FileName);
            System.IO.File.Delete(p_FileName);
        }

        public void SaveImage(MemoryStream p_Stream, string p_FileName)
        {
            using (FileStream file = new FileStream(System.IO.Path.Combine(sPath, p_FileName), FileMode.Create, System.IO.FileAccess.Write))
            {
                byte[] bytes = new byte[p_Stream.Length];
                p_Stream.Read(bytes, 0, (int)p_Stream.Length);
                file.Write(bytes, 0, bytes.Length);
                p_Stream.Close();
            }
        }


        public MemoryStream LoadImage(string p_FileName)
        {
            MemoryStream ms = new MemoryStream();
            using (FileStream file = new FileStream(System.IO.Path.Combine(sPath, p_FileName), FileMode.Open, FileAccess.Read))
            {
                byte[] bytes = new byte[file.Length];
                file.Read(bytes, 0, (int)file.Length);
                ms.Write(bytes, 0, (int)file.Length);
            }
            return ms;
        }



        public StringBuilder LoadText(string p_FileName)
        {
            p_FileName = Path.Combine(sPath, p_FileName);
            if (System.IO.File.Exists(p_FileName))
            {
                using (var l_Stream = System.IO.File.OpenText(p_FileName))
                {
                    return new StringBuilder(l_Stream.ReadToEnd());
                }
            }
            return new StringBuilder();
        }

        public void SaveText(string p_FileName, StringBuilder p_Text)
        {
            p_FileName = Path.Combine(sPath, p_FileName);
            using (var l_stream = System.IO.File.OpenWrite(p_FileName))
            using (var l_StreamWriter = new System.IO.StreamWriter(l_stream))
            {
                l_StreamWriter.Write(p_Text.ToString());
            }
        }

        public T LoadObject<T>(string p_FileName, T p_DefaultValue = default(T))
        {
            var l_SerializerService = Sodexo.Framework.Services.SerializerService();
            if (Exist(p_FileName))
            {
                return l_SerializerService.Deserialize<T>(LoadText(p_FileName));
            }
            else
            {
                return p_DefaultValue;
            }
        }

        public void SaveObject<T>(string p_FileName, T p_Object)
        {
            var l_SerializerService = Sodexo.Framework.Services.SerializerService();
            SaveText(p_FileName, l_SerializerService.Serialize<T>(p_Object));
        }

        public DateTime GetLastWriteTime(string p_FileName)
        {
            p_FileName = Path.Combine(sPath, p_FileName);
            return System.IO.File.GetLastWriteTime(p_FileName);
        }


        public void AppendText(string p_FileName, StringBuilder p_Text)
        {
            p_FileName = Path.Combine(sPath, p_FileName);
            using (var l_StreamWriter = System.IO.File.AppendText(p_FileName))
            {
                l_StreamWriter.Write(p_Text.ToString());
            }
        }
    }
}
