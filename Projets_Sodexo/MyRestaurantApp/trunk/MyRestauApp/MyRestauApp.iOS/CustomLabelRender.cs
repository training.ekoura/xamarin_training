﻿using Foundation;
using MyRestauApp;
using Sodexo.Controls;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomLabel), typeof(CustomLabelRender))]
namespace MyRestauApp
{

    public static class NSStringExtensions
    {
        public static NSAttributedString AsAttributedString(this string input, NSDocumentType docType)
        {
            NSError err = null;
            var rtn = new NSAttributedString(input, new NSAttributedStringDocumentAttributes { DocumentType = docType }, ref err);
            if (err == null)
            {
                return rtn;
            }
            throw new NSErrorException(err);
        }
    }
    public class CustomLabelRender : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            //To do later 
            //var text = Control.Text.AsAttributedString(NSDocumentType.HTML);
            //Control.AttributedText = new UITextView { AttributedText = text }.AttributedText;
            Control.Text = System.Text.RegularExpressions.Regex.Replace(Control.Text.Replace("<br />", Environment.NewLine), "<[^>]*>", "");

        }
    }
}
