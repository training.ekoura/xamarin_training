using MyRestauApp;
using Sodexo.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace MyRestauApp
{
    public class CustomEntryRenderer : EntryRenderer
    {
		CustomEntry base_entry;

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
			base_entry = (CustomEntry)this.Element;

            base.OnElementChanged(e);

            if (Control != null && base_entry != null)
            {
                //the entry must have a styleId to "NoBorderEntry" to hide borders
                if (e.NewElement.StyleId == "StockEntry")
                {
					Control.Layer.BorderColor =  Color.Red.ToCGColor();
                }
				SetFont(base_entry);
				ResizeHeight ();

                Control.ShouldChangeCharacters = (textField, range, replacementString) =>
                {
                    var newLength = textField.Text.Length + replacementString.Length - range.Length;
					return newLength <= base_entry.MaxLength;
                };

                SetReturnType(base_entry);

                Control.ShouldReturn += (UITextField tf) =>
                {
                    base_entry.InvokeCompleted();
                    return true;
                };
            }
        }

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base_entry = (CustomEntry)this.Element;

			base.OnElementPropertyChanged (sender, e);
			if (string.IsNullOrEmpty (e.PropertyName) && e.PropertyName == "Font")
			{
				SetFont (base_entry);
				ResizeHeight ();
			}
		}

        private void SetReturnType(CustomEntry entry)
        {
            ReturnType type = entry.ReturnType;

            switch (type)
            {
                case ReturnType.Go:
                    Control.ReturnKeyType = UIReturnKeyType.Go;
                    break;
                case ReturnType.Next:
                    Control.ReturnKeyType = UIReturnKeyType.Next;
                    break;
                case ReturnType.Send:
                    Control.ReturnKeyType = UIReturnKeyType.Send;
                    break;
                case ReturnType.Search:
                    Control.ReturnKeyType = UIReturnKeyType.Search;
                    break;
                case ReturnType.Done:
                    Control.ReturnKeyType = UIReturnKeyType.Done;
                    break;
                default:
                    Control.ReturnKeyType = UIReturnKeyType.Default;
                    break;

            }
        }

		private void SetFont(CustomEntry view)
		{
			UIFont uiFont;
			if (view.Font != Font.Default && (uiFont = view.Font.ToUIFont ()) != null) {
				Control.Font = uiFont;
			} else if (view.Font == Font.Default)
				Control.Font = UIFont.SystemFontOfSize(17f);
		}

		private void ResizeHeight()
		{
			
			if (Element.HeightRequest>= 0)
				return;
			var height = Math.Max (Bounds.Height, new UITextField {
				Font = Control.Font
			}.IntrinsicContentSize.Height);
				

			//Control.Frame = new Rectangle (0.0f, 0.0f, (float)Element.Width, height).ToRectangleF();
			Control.Frame = new CoreGraphics.CGRect((nfloat)(-10.0), (nfloat)0.0, (nfloat)(Element.Width), (nfloat)(height));
			Control.Layer.BorderColor = Color.Red.ToCGColor();
			Element.HeightRequest = height;
            Element.WidthRequest = Element.StyleId == "StockEntry" ? 130 : Element.Width;
			Element.HorizontalTextAlignment = TextAlignment.Center;
			SetNeedsDisplay ();
		}
    }

}


