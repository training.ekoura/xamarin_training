﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sodexo;
using Xamarin.Forms;
using MyRestauApp.Common.Views;
using MyRestauApp.Common.ViewModels;

namespace MyRestauApp.Common
{
    public class App : Application
    {
        // Properties
        static string Theme;
        static string Version = "1.0";
        static string VersionForWs = "1.0"; // format x.y.z

        public App(string p_Theme)
        {
            Theme = p_Theme;
        }

        protected override void OnStart()
        {
            Sodexo.Framework.Initialize();
            Sodexo.FrameworkXamarinForms.Initialize();

            #region splashScreen
          //  if (Device.OS == TargetPlatform.Android)
          //  {
                var l_Grid = new Grid()
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Padding = new Thickness(0)
                };

                l_Grid.SizeChanged += l_Grid_SizeChanged;

                var l_SplashImageName = Device.OnPlatform("Default.png", "background.png", "Assets/splash.png");

                var l_Photo = new Image()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    Aspect = Xamarin.Forms.Aspect.AspectFill,
                    Source = ImageSource.FromFile(l_SplashImageName)
                };

                var l_Version = new Label()
                {
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.Center,
                    VerticalOptions = LayoutOptions.End,
                    Text = "Version " + Version,
                    TranslationY = -10
                };

                l_Grid.Children.Add(l_Photo);
                l_Grid.Children.Add(l_Version);

                var l_Page = new ContentPage()
                {
                    Content = l_Grid
                };

                NavigationPage.SetHasBackButton(l_Page, false);
                NavigationPage.SetHasNavigationBar(l_Page, false);

                Sodexo.Framework.Services.InteractionService().Set(l_Page);
           // }
            #endregion

            // Initialisation des DAOServices
            MyRestauApp.Common.DAOServices.Initializer.RegisterAll();

            // Initilisation des views
            MyRestauApp.Common.Views.Initializer.RegisterAll();

            // Initialisation des ViewModels
            MyRestauApp.Common.ViewModels.Initializer.RegisterAll();

            var GlobalConfigService = Sodexo.Framework.Services.GlobalConfigService();

            #region ============================= Gestion du thème =============================
            // Gestion du Thême
            //var GlobalConfigService = Sodexo.Framework.Services.;
            //GlobalConfigService.ThemeName = Theme;
            //GlobalConfigService.Data.DeviceOs = Device.OS == TargetPlatform.Android ? "Android" : (Device.OS == TargetPlatform.iOS ? "iOS" : "WindowsPhone");
            //GlobalConfigService.Data.AppVersion = VersionForWs;

            var l_ThemeService = Sodexo.Framework.Services.ThemeService() as Sodexo.Themes.IThemeServiceXamarin;
            Sodexo.Framework.Services.InteractionService().ThemeNameValue = MyRestauApp.Common.ViewModels.Helper.ToolsBox.ThemeNameToImage(Theme);

            // On enregistre tous les thèmes possible dans l'application.
            l_ThemeService.Register(ThemeName.MySodexo, Themes.ThemeMySodexo.ThemeCallback);


            switch (Theme)
            {
                case ThemeName.MySodexo:
                    l_ThemeService.Apply(ThemeName.MySodexo);
                    //GlobalConfigService.Data.AppName = GlobalConstants.MySodexo;
                    break;
                default:
                    l_ThemeService.Apply(ThemeName.MySodexo);
                    //GlobalConfigService.Data.AppName = GlobalConstants.MySodexo;
                    break;
            }


            #endregion

            #region culture(langage)
            var cultureDevice = Sodexo.Framework.Services.Container().Resolve<ILocalizeService>().GetCurrentCultureInfo();
            if (cultureDevice.Name.Contains("en"))
            {

                Views.Properties.Resources.Culture = new System.Globalization.CultureInfo("en-US");
                ViewModels.Properties.Resources.Culture = new System.Globalization.CultureInfo("en-US");
            }
            else
            {
                Views.Properties.Resources.Culture = new System.Globalization.CultureInfo("fr-FR");
                ViewModels.Properties.Resources.Culture = new System.Globalization.CultureInfo("fr-FR");
            }
            #endregion

            

            if (GlobalConfigService.Data != null
                    && GlobalConfigService.Data.Token != null
                   )
            {
                // Actuellement le SplashScreen est affiché. L'opération suivante est longue et aboutie à un Set de la IMasterDetailPage.
                var contextViewModelVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IContextViewModel>();
                contextViewModelVM.AppStarter();
            }
            else
            {
                // Actuellement le SplashScreen est affiché. L'opération suivante est rapide et aboutie à un Set de ConnectionPage.
                // Comme c'est rapide, histoire de laisser une peu le SplashScreen, on diffère (on peut car il y a déjà une UI d'afficher donc cela ne va pas planter).
                System.Threading.Tasks.Task.Delay(2000).ContinueWith((t) =>
                {
                    var l_ChangeStatusBarColor = false;

                    // On applique la couleur du statusBar sur IOS
                    Sodexo.Framework.Services.InteractionService().Set<IConnexionView>(null, l_ChangeStatusBarColor);

                }, System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            var GlobalConfigService = Sodexo.Framework.Services.GlobalConfigService();
            GlobalConfigService.Data.DateWhenOnSleep = DateTime.Now;
        }

        protected override async void OnResume()
        {
            // Handle when your app resumes
            var GlobalConfigService = Sodexo.Framework.Services.GlobalConfigService();
            if (GlobalConfigService.Data != null && GlobalConfigService.Data.Token != null)
            {
                if ((DateTime.Now - GlobalConfigService.Data.DateWhenOnSleep).TotalMinutes >= 30 ? true : false)
                {
                    
                    #region splashScreen
                    var l_Grid = new Grid()
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        Padding = new Thickness(0)
                    };

                    l_Grid.SizeChanged += l_Grid_SizeChanged;

                    var l_SplashImageName = Device.OnPlatform("Default.png", "background.png", "Assets/splash.png");

                    var l_Photo = new Image()
                    {
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        Aspect = Xamarin.Forms.Aspect.AspectFill,
                        Source = ImageSource.FromFile(l_SplashImageName)
                    };

                    var l_Version = new Label()
                    {
                        TextColor = Color.Black,
                        HorizontalOptions = LayoutOptions.Center,
                        VerticalOptions = LayoutOptions.End,
                        Text = "Version " + Version,
                        TranslationY = -10
                    };

                    l_Grid.Children.Add(l_Photo);
                    l_Grid.Children.Add(l_Version);

                    var l_Page = new ContentPage()
                    {
                        Content = l_Grid
                    };

                    NavigationPage.SetHasBackButton(l_Page, false);
                    NavigationPage.SetHasNavigationBar(l_Page, false);

                    Sodexo.Framework.Services.InteractionService().Set(l_Page);

                    #endregion
                     
                    await Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IContextViewModel>().AppStarter();
                }
                else
                {
                    // Mise à jour de la DashboardViewModel
                    var dashboardVM = Sodexo.Framework.Services.Container().Resolve<MyRestauApp.Common.ViewModels.IDashboardViewModel>();
                    if (!string.IsNullOrEmpty(dashboardVM.DateDashBoard))
                    {
                       var resultDashboardVMRefresh = await dashboardVM.refreshOnlyDashborad();
                    }
                };
            }
            else
            {
                System.Threading.Tasks.Task.Delay(2000).ContinueWith((t) =>
                {
                    Sodexo.Framework.Services.InteractionService().Set(ViewName.ConnexionView);
                }, System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());
            }

        }

        #region ============================= Useful fonctions =============================
        void l_Grid_SizeChanged(object sender, EventArgs e)
        {
            Sodexo.Framework.Services.InteractionService().DeviceHeight = MainPage.Height;
            Sodexo.Framework.Services.InteractionService().DeviceWidth = MainPage.Width;
        }
        #endregion
    }
}
