﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sodexo.Behaviors
{
    public class ViewButtonBehaviour : Xamarin.Forms.Behavior<View>
    {

        protected override void OnAttachedTo(View view)
        {
            base.OnAttachedTo(view);

            var viewLayoutTapGesture = new TapGestureRecognizer();
            viewLayoutTapGesture.Tapped += async (object sender, EventArgs e) =>
            {
                //animate a click effect
                await view.ScaleTo(0.85, 35, Easing.SinIn);
                await view.ScaleTo(1, 35, Easing.SinInOut);
            };

            view.GestureRecognizers.Add(viewLayoutTapGesture);
        }

        protected override void OnDetachingFrom(View view)
        {
            base.OnDetachingFrom(view);
        }
    }
}
