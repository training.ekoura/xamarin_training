﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sodexo.Behaviors
{
    public class BurgerSwipeBehaviour : Xamarin.Forms.Behavior<Controls.TouchContentView>
    {

        protected override void OnAttachedTo(Controls.TouchContentView view)
        {
            base.OnAttachedTo(view);

            var l_gr = new SwipeGestureRecognizer();
            l_gr.Right += async (s, e) =>
            {
                //animate out
                await view.TranslateTo(480, 0, 100, Easing.Linear);
                //remove from defaultGrid (homeView totalPage Grid)
                (view.Parent as Grid).Children.Remove(view);
            };
            view.GestureRecognizers.Add(l_gr);
            Gestures.SetHook(view, true);
        }

        protected override void OnDetachingFrom(Controls.TouchContentView view)
        {
            base.OnDetachingFrom(view);
        }
    }
}
