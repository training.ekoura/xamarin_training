﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Sodexo.Controls;

using Xamarin.Forms;

namespace Sodexo.ContentViews
{
    public class CalndarContentView : ContentView
    {
        Label labl;
        public CalndarContentView()
        {
            VerticalOptions = LayoutOptions.FillAndExpand;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            Padding = new Thickness(20, 0);

            var calendar = new Calendar() { };
            calendar.SetBinding(Calendar.ReceivedDateProperty, "SelectedDate");

            var voirCommandesButton = new RoundedButton()
            {
                Text = Properties.Resources.Calendar_ContentView_Button_Text,
                //FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Button)),
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.Center
            };

            //voirCommandesButton.Clicked += (object sender, EventArgs e) =>{

            //    var parentView = (sender as RoundedButton).Parent.Parent.Parent.Parent.Parent.Parent.Parent;
            //    var children = (parentView as Grid).Children;
            //    children.RemoveAt(children.Count - 1);
            //    children.RemoveAt(children.Count - 1);

            //};
            voirCommandesButton.SetDynamicResource(RoundedButton.StyleProperty, "RoundedButtonStyle");
            voirCommandesButton.SetBinding(RoundedButton.CommandProperty, "ChangeDateCommand");
            voirCommandesButton.SetBinding(RoundedButton.CommandParameterProperty, new Binding("SelectedDate") { Source = calendar });

            var ContentStack = new StackLayout()
            {
                Padding = new Thickness(10, 20),
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Spacing=0,
                Children = { calendar, voirCommandesButton }
            };

            Content = ContentStack;

            this.SetBinding(ContentView.IsVisibleProperty, "IsCalendarPopInVisibile");
        }
    }
}
