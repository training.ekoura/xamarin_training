﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Sodexo.Controls;

using Xamarin.Forms;

namespace Sodexo.ContentViews
{
    public class PlatstatusContentView : ContentView
    {
        public PlatstatusContentView()
        {
            VerticalOptions = LayoutOptions.FillAndExpand;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            Padding = new Thickness(20, 0);

            #region Header
            var header = new Label()
            {
                Text="Changez le statut de plat",
                //FontSize=32,
                //TextColor=Color.Black,
                //VerticalOptions=LayoutOptions.Fill,
                //HorizontalOptions=LayoutOptions.FillAndExpand,
                //VerticalTextAlignment=TextAlignment.Center,
                //HorizontalTextAlignment=TextAlignment.Center
            };
            header.SetDynamicResource(Label.StyleProperty, "TitlePopinStyle");
            #endregion

            #region Button (ajouter/reduire)
            var buttonList = new RepeaterView()
            {
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                ItemSpacing = 5,
                 #region ==Template==
                ItemTemplate = new DataTemplate(() =>
                {
                    var commandButton = new CustomButton()
                    {
                        ButtonType=CustomButton.ButtonTypes.Rounded
                    };
                    //commandButton.SetBinding(CustomButton.ButtonColorProperty, "");
                    commandButton.SetBinding(CustomButton.ButtonLabelProperty, "LibelleGerant");
                    commandButton.SetBinding(CustomButton.TapCommandParameterProperty, ".");
                    commandButton.SetBinding(CustomButton.TapCommandProperty, new Binding("UpdateStatutCommand") { Source=this.BindingContext });

                    return commandButton;
                })
                #endregion
            };
            buttonList.SetBinding(RepeaterView.ItemsSourceProperty, "ListStatuts");

            var modifierButtonStack = new StackLayout()
            {
                Padding = new Thickness(40, 0),
                Spacing=20,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Children = { buttonList }
            };

            #endregion

            var ContentStack = new StackLayout()
            {
                Padding = new Thickness(10, 30),
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Spacing=15,
                Children = { header, modifierButtonStack }
            };

            Content = ContentStack;

            this.SetBinding(ContentView.IsVisibleProperty, "IsStatutPopInVisibile");
        }
    }
}
