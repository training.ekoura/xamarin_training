﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Xamarin.Forms;

namespace Sodexo.ContentViews
{
    public class PopInView : ContentView
    {
        public PopInView()
        {
            var ContentStack = new StackLayout()
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Spacing=0,
                Children =
                {
                    new PopUpheader(),
                    new ModifierStockContentView(),
                    new PlatstatusContentView(),
                    new CalndarContentView(),
                    new DeconnexionContentView(){IsVisible = false}
                }
            };

            var ContentStackFormater = new StackLayout()
            {
                Children = { ContentStack }
            };
            ContentStackFormater.SetDynamicResource(StackLayout.StyleProperty, "DarkPaddedPopInStyle");

            Content = ContentStackFormater;

            this.SetBinding(ContentView.IsVisibleProperty, "IsPopInVisibile");
            this.Behaviors.Add(new Behaviors.PopInCloseBehaviour());
        }
    }
}
