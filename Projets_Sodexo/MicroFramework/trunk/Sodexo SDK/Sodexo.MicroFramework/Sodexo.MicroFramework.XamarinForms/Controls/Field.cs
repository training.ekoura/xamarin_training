﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sodexo.Controls
{
    [ContentProperty("FieldContent")]
    public class Field : Xamarin.Forms.ContentView
    {
        #region === PropertyName ===

        public static readonly BindableProperty PropertyNameProperty = BindableProperty.Create<Field, string>(p => p.PropertyName, default(string), propertyChanged: (d, o, n) => (d as Field).PropertyName_Changed(o, n));

        private void PropertyName_Changed(string oldvalue, string newvalue)
        {
            UpdateErrors();
        }

        public string PropertyName
        {
            get { return (string)GetValue(PropertyNameProperty); }
            set { SetValue(PropertyNameProperty, value); }
        }

        #endregion

        #region === Content ===

        public static readonly BindableProperty FieldContentProperty = BindableProperty.Create<Field, object>(p => p.FieldContent, null, propertyChanged: (d, o, n) => (d as Field).Content_Changed(o, n));

        private void Content_Changed(object oldvalue, object newvalue)
        {
            _ContentFrame.Content = null;

            if (newvalue is View)
            {
                _ContentFrame.Content = newvalue as View;
            }

            if(newvalue is Entry)
            {

            }
        }

        public object FieldContent
        {
            get { return (object)GetValue(FieldContentProperty); }
            set { SetValue(FieldContentProperty, value); }
        }


        #endregion

        #region === Label ===

        public static readonly BindableProperty LabelProperty = BindableProperty.Create<Field, string>(p => p.Label, "[No Label]", propertyChanged: (d, o, n) => (d as Field).Label_Changed(o, n));

        private void Label_Changed(string oldvalue, string newvalue)
        {
            if (_FieldHeaderLabel == null) return;
            _FieldHeaderLabel.Text = newvalue;
        }

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        #endregion

        #region === LabelColor ===

        public static readonly BindableProperty LabelColorProperty = BindableProperty.Create<Field, Color>(p => p.LabelColor, Device.OnPlatform<Color>(Color.Black, Color.White, Color.White), propertyChanged: (d, o, n) => (d as Field).LabelColor_Changed(o, n));

        private void LabelColor_Changed(Color oldvalue, Color newvalue)
        {
            if (_FieldHeaderLabel == null) return;
            _FieldHeaderLabel.TextColor = newvalue;
        }

        public Color LabelColor
        {
            get { return (Color)GetValue(LabelColorProperty); }
            set { SetValue(LabelColorProperty, value); }
        }

        #endregion

        #region === LabelIsVisible ===

        public static readonly BindableProperty LabelIsVisibleProperty = BindableProperty.Create<Field, bool>(p => p.LabelIsVisible, true, propertyChanged: (d, o, n) => (d as Field).LabelIsVisible_Changed(o, n));

        private void LabelIsVisible_Changed(bool oldvalue, bool newvalue)
        {
            if (_FieldHeaderLabel == null) return;
            _FieldHeaderLabel.IsVisible = newvalue;
        }

        public bool LabelIsVisible
        {
            get { return (bool)GetValue(LabelIsVisibleProperty); }
            set { SetValue(LabelIsVisibleProperty, value); }
        }

        #endregion

        #region === ValidationErrorColor ===

        public static readonly BindableProperty ValidationErrorColorProperty = BindableProperty.Create<Field, Color>(p => p.ValidationErrorColor, Color.Red, propertyChanged: (d, o, n) => (d as Field).ValidationErrorColor_Changed(o, n));

        private void ValidationErrorColor_Changed(Color oldvalue, Color newvalue)
        {

        }

        public Color ValidationErrorColor
        {
            get { return (Color)GetValue(ValidationErrorColorProperty); }
            set { SetValue(ValidationErrorColorProperty, value); }
        }

        #endregion

        #region === Property ===

        public static readonly BindableProperty PropertyProperty = BindableProperty.Create<Field, Binding>(p => p.Property, default(Binding), propertyChanged: (d, o, n) => (d as Field).Property_Changed(o, n));

        private void Property_Changed(Binding oldvalue, Binding newvalue)
        {

        }

        public Binding Property
        {
            get { return (Binding)GetValue(PropertyProperty); }
            set { SetValue(PropertyProperty, value); }
        }

        #endregion

        private bool _IsLoaded = false;

        private Grid _RootGrid;

        private RowDefinition _HeaderRow;

        private RowDefinition _ContentRow;

        private Label _FieldHeaderLabel;

        private Label _FieldErrorLabel;

        private Frame _ContentFrame;

        public Field()
        {
            _RootGrid = new Grid();

            _HeaderRow = new RowDefinition() { Height = new GridLength(0, GridUnitType.Auto) };
            _ContentRow = new RowDefinition();
            _RootGrid.RowDefinitions.Add(_HeaderRow);
            _RootGrid.RowDefinitions.Add(_ContentRow);

            _FieldHeaderLabel = new Label();
            _FieldHeaderLabel.Text = Label;
            _FieldHeaderLabel.TextColor = LabelColor;
            _FieldHeaderLabel.IsVisible = LabelIsVisible;
            Grid.SetRow(_FieldHeaderLabel, 0);
            _RootGrid.Children.Add(_FieldHeaderLabel);

            _FieldErrorLabel = new Label();
            _FieldErrorLabel.HorizontalOptions = LayoutOptions.End;
            Grid.SetRow(_FieldErrorLabel, 0);
            _RootGrid.Children.Add(_FieldErrorLabel);

            _ContentFrame = new Frame();
            _ContentFrame.Padding = new Thickness(0);
            _ContentFrame.HasShadow = false;
            Grid.SetRow(_ContentFrame, 1);
            _RootGrid.Children.Add(_ContentFrame);

            this.Padding = new Thickness(0);
            this.Content = _RootGrid;

            UpdateErrors();
        }

        #region === Binding Context et évènements erreurs ===

        private object _OldBindingContext;

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            if (_OldBindingContext is INotifyDataErrorInfo)
            {
                (_OldBindingContext as INotifyDataErrorInfo).ErrorsChanged -= ValidationControl_ErrorsChanged;
            }
            _OldBindingContext = BindingContext;
            if(BindingContext is INotifyDataErrorInfo)
            {
                (BindingContext as INotifyDataErrorInfo).ErrorsChanged += ValidationControl_ErrorsChanged;                
            }

            UpdateErrors();
        }

        void ValidationControl_ErrorsChanged(object sender, DataErrorsChangedEventArgs e)
        {
            UpdateErrors();
        }

        #endregion

        void UpdateErrors()
        {
            try
            {
                var errors = default(List<object>);
                var ndei = (BindingContext as INotifyDataErrorInfo);
                if (ndei != null)
                {
                    errors = ndei.GetErrors(PropertyName).Cast<object>().Where(e => e != null).ToList();
                }

                if (errors != null && errors.Count > 0)
                {
                    _ContentFrame.OutlineColor = ValidationErrorColor;
                    if (Device.OS == TargetPlatform.Android)
                    {
                        _ContentFrame.BackgroundColor = ValidationErrorColor;
                    }
                    _FieldErrorLabel.TextColor = ValidationErrorColor;
                    
                    _FieldErrorLabel.IsVisible = true;
                    if (errors.Count == 1)
                    {
                        _FieldErrorLabel.Text = errors.First().ToString();
                    }
                    else
                    {
                        _FieldErrorLabel.Text = errors.Select(e=>e.ToString()).Aggregate((a, b) => a + "," + b);
                    }
                }
                else
                {
                    _ContentFrame.OutlineColor = Color.Transparent;
                    _ContentFrame.BackgroundColor = Color.Transparent;
                    _FieldErrorLabel.IsVisible = false;
                }
            }
            catch
            {

            }
        }
    }
}
