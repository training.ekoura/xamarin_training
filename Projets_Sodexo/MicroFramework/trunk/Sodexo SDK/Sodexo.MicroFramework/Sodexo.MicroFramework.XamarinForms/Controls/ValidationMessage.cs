﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sodexo.Controls
{
    public class ValidationMessage : Xamarin.Forms.Label
    {
        public ValidationMessage()
        {
            //this.TextColor = Color.Red;
            this.FontAttributes = Xamarin.Forms.FontAttributes.Italic;
            this.FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label));
        }

        private object _OldBindingContext;

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            if (_OldBindingContext is INotifyDataErrorInfo)
            {
                (_OldBindingContext as INotifyDataErrorInfo).ErrorsChanged -= ValidationControl_ErrorsChanged;
            }
            _OldBindingContext = BindingContext;
            if(BindingContext is INotifyDataErrorInfo)
            {
                (BindingContext as INotifyDataErrorInfo).ErrorsChanged += ValidationControl_ErrorsChanged;                
            }
        }

        void ValidationControl_ErrorsChanged(object sender, DataErrorsChangedEventArgs e)
        {
            try
            {
                var ndei = (BindingContext as INotifyDataErrorInfo);
                if (ndei != null)
                {
                    var errors = ndei.GetErrors(PropertyName).Cast<object>().ToList();
                    if (errors.Count > 0)
                    {
                        var l_text = string.Empty;
                        foreach (var err in errors)
                        {
                            if(err!=null)
                            {
                                if (!string.IsNullOrWhiteSpace(l_text)) l_text += "\n";
                                l_text += " * " + err.ToString();
                            }
                        }
                        this.IsVisible = true;
                        this.Text = l_text;
                    }
                    else
                    {
                        this.IsVisible = false;
                    }
                }
            }
            catch
            {

            }
        }

        public static readonly BindableProperty PropertyNameProperty = BindableProperty.Create<ValidationMessage, string>(p => p.PropertyName, null, propertyChanged: OnPropertyNameChanged);

        private static void OnPropertyNameChanged(BindableObject bindable, string oldvalue, string newvalue)
        {

        }

        public string PropertyName
        {
            get { return (string)GetValue(PropertyNameProperty); }
            set { SetValue(PropertyNameProperty, value); }
        }


    }
}
