﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sodexo.Controls
{
    public class Calendar : ContentView
    {
        
        DateTime calendarInUseDate;
        DateTime currentCalendarDate;
        
        Label CalendarHeader;
        StackLayout mainStack;
        StackLayout CalendarStack;
        private string LeftImagePath = "Sodexo.Images.calendar_left.png";
        private string RightImagePath = "Sodexo.Images.calendar_right.png";
        private double DefaultImageSize = 65;

        public Calendar()
        {
            Build();
            //ReceivedDate= DateTime.Today.ToString();
        }

        #region === ReceivedDate ===

        public static readonly BindableProperty ReceivedDateProperty = BindableProperty.Create<Calendar, DateTime>(p => p.ReceivedDate, default(DateTime), propertyChanged: (d, o, n) => (d as Calendar).ReceivedDateProperty_Changed(o, n));

        private void ReceivedDateProperty_Changed(DateTime oldvalue, DateTime newvalue)
        {
            calendarInUseDate = (!String.IsNullOrEmpty(newvalue.ToString())) ? newvalue : DateTime.Today;
            SelectedDate = newvalue;
            refreshDateBackgrounds();
            UpdateCalendar(calendarInUseDate);
        }

        public DateTime ReceivedDate
        {
            get { return (DateTime)GetValue(ReceivedDateProperty); }
            set { SetValue(ReceivedDateProperty, value); }
        }

        #endregion

        #region === SelectedDate ===

        public static readonly BindableProperty SelectedDateProperty = BindableProperty.Create<Calendar, DateTime>(p => p.SelectedDate, default(DateTime), propertyChanged: (d, o, n) => (d as Calendar).SelectedDate_Changed(o, n));

        private void SelectedDate_Changed(DateTime oldvalue, DateTime newvalue)
        {
        }

        public DateTime SelectedDate
        {
            get { return (DateTime)GetValue(SelectedDateProperty); }
            set { SetValue(SelectedDateProperty, value); }
        }

        #endregion

        private void Build()
        {
            //only visual elemnts are added here and initialised.
            //logic is done elsewhere

            #region Header 

            #region ============================= Left Image =============================
            var l_LeftImage = new Image 
            {
                HeightRequest = DefaultImageSize,
                WidthRequest = DefaultImageSize
            };
            l_LeftImage.Source = ImageSource.FromResource(LeftImagePath);

            var l_ImageLeftGesture = new TapGestureRecognizer();
            l_ImageLeftGesture.Tapped += prevMonthButton_Clicked;
            l_LeftImage.GestureRecognizers.Add(l_ImageLeftGesture);

            #endregion


            #region ============================= Right Image =============================
            var l_RightImage = new Image
            {
                HeightRequest = DefaultImageSize,
                WidthRequest = DefaultImageSize
            };
            l_RightImage.Source = ImageSource.FromResource(RightImagePath);

            var l_ImageRightGesture = new TapGestureRecognizer();
            l_ImageRightGesture.Tapped += nextMonthButton_Clicked;
            l_RightImage.GestureRecognizers.Add(l_ImageRightGesture);

            #endregion


            /*var prevMonthButton = new RoundedButton()
            {
                Text = "<",
                //TextColor = Color.Red,
                //BorderColor = Color.Red,
                BorderWidth = 3,
                BackgroundColor = Color.Transparent,
                HorizontalOptions = LayoutOptions.Fill
            };
            prevMonthButton.SetDynamicResource(RoundedButton.StyleProperty, "RoundedButtonStyle");
            prevMonthButton.SetDynamicResource(RoundedButton.TextColorProperty, "GerantMainColor");
            prevMonthButton.SetDynamicResource(RoundedButton.BorderColorProperty, "GerantMainColor");
            prevMonthButton.Clicked += prevMonthButton_Clicked;*/

            CalendarHeader = new Label()
            {
                TextColor = Color.White,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center
            };
            CalendarHeader.SetDynamicResource(Label.BackgroundColorProperty, "GerantSecondaryColor");
           
            /*var nextMonthButton = new RoundedButton()
            {
                Text = ">",
                //TextColor = Color.Red,
                //BorderColor = Color.Red,
                BorderWidth = 3,
                BackgroundColor = Color.Transparent,
                HorizontalOptions = LayoutOptions.Fill
            };
            nextMonthButton.SetDynamicResource(RoundedButton.StyleProperty, "RoundedButtonStyle");
            nextMonthButton.SetDynamicResource(RoundedButton.TextColorProperty, "GerantMainColor");
            nextMonthButton.SetDynamicResource(RoundedButton.BorderColorProperty, "GerantMainColor");
            nextMonthButton.Clicked += nextMonthButton_Clicked;*/

            var headerStack = new StackLayout()
            {
                WidthRequest=350,
                Spacing=10,
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions=LayoutOptions.Center,
                Orientation = StackOrientation.Horizontal,
                Children = { l_LeftImage, CalendarHeader, l_RightImage }
            };
            #endregion

            #region dateNames
            var dateNameStack = new StackLayout()
            {
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Orientation = StackOrientation.Horizontal
            };

            var ListofDaysShortName = new List<string>() { "D", "L", "M", "M", "J", "V", "S", };

            foreach (var day in ListofDaysShortName)
            {
                var dateLabel = new Label()
                {
                    Text = day,
                    FontSize=Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Center
                };
                dateLabel.SetDynamicResource(Label.TextColorProperty, "GerantSecondaryColor");

                var dateNameGrid = new Grid()
                {
                    WidthRequest = Device.OnPlatform(35, 40, 50),
                    HeightRequest = Device.OnPlatform(30,30,50),
                    HorizontalOptions=LayoutOptions.FillAndExpand,
                    VerticalOptions=LayoutOptions.Fill,
                };

                Grid.SetColumn(dateLabel, 0);
                Grid.SetRow(dateLabel, 0);
                dateNameGrid.Children.Add(dateLabel);

                dateNameStack.Children.Add(dateNameGrid);
            }

            #endregion

            #region dates
            CalendarStack = new StackLayout()
            {
                Padding = new Thickness(0, 0, 0, 5),
                Spacing=0,
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            for (int i = 0; i <= 5; i++)
            {
                var subStack = new StackLayout()
                {
                    Orientation = StackOrientation.Horizontal,
                    VerticalOptions = LayoutOptions.Fill,
                    HorizontalOptions = LayoutOptions.FillAndExpand
                };

                for (int j = 0; j <= 6; j++)
                {
                    var dateNumber = new Label()
                    {
                        TextColor = Color.Gray,
                        FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Center
                    };

                    var dateNumberGrid = new Grid()
                    {
                        WidthRequest = Device.OnPlatform(35, 40, 50),
                        HeightRequest = Device.OnPlatform(30, 30, 50),
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.Fill
                    };

                    Grid.SetColumn(dateNumber, 0);
                    Grid.SetRow(dateNumber, 0);
                    dateNumberGrid.Children.Add(dateNumber);

                    subStack.Children.Add(dateNumberGrid);
                }

                CalendarStack.Children.Add(subStack);
            }
            #endregion

            mainStack = new StackLayout()
            {
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.White,
                Spacing=0,
                Children = { headerStack, dateNameStack, CalendarStack }
            };

            Content = mainStack;
        }

        void prevMonthButton_Clicked(object sender, EventArgs e)
        {
            RevertSelectedDate();
            calendarInUseDate = calendarInUseDate.AddMonths(-1);
            refreshDateBackgrounds();
            UpdateCalendar(calendarInUseDate);
        }

        void nextMonthButton_Clicked(object sender, EventArgs e)
        {
            RevertSelectedDate();
            calendarInUseDate = calendarInUseDate.AddMonths(1);
            refreshDateBackgrounds();
            UpdateCalendar(calendarInUseDate);
        }

        public void UpdateCalendar(DateTime objdate)
        {
            //var unTouchedObject = Convert.ToDateTime(ReceivedDate);
            CalendarHeader.Text = objdate.ToString("MMMM yyyy");
            objdate = new DateTime(objdate.Year, objdate.Month, 1);
            int dayOfWeek = (int)objdate.DayOfWeek + 1;
            int daysOfMonth = DateTime.DaysInMonth(objdate.Year, objdate.Month);
            int i = 1;
            foreach (var stack in CalendarStack.Children)
            {
                foreach (var gridContainer in (stack as StackLayout).Children)
                {
                    var singleDateGrid = (gridContainer as Grid);
                    singleDateGrid.BackgroundColor = Color.Transparent;
                    var singleDateLabel = singleDateGrid.Children[0] as Label;
                    if (i >= dayOfWeek && i < (daysOfMonth + dayOfWeek))
                    {
                        singleDateLabel.Text = (i - dayOfWeek + 1).ToString();
                        if ((i == Convert.ToDateTime(ReceivedDate).Day+3)&&( objdate.Month == Convert.ToDateTime(ReceivedDate).Month))
                        {
                            singleDateLabel.SetDynamicResource(Label.BackgroundColorProperty, "GerantMainColor");
                            singleDateLabel.TextColor = Color.White;
                        }
                    }
                    else
                    {
                        singleDateLabel.Text = "";
                    }

                    var singleDateGridTapGesture = new TapGestureRecognizer();
                    singleDateGridTapGesture.Tapped += (object sender, EventArgs e) =>
                    {

                        var currentBlock = ((sender as Grid).Children[0] as Label);

                        if (!String.IsNullOrEmpty(currentBlock.Text))
                        {
                            refreshDateBackgrounds();
                            currentBlock.SetDynamicResource(Label.BackgroundColorProperty, "GerantMainColor");
                            currentBlock.TextColor = Color.White;
                            string date = currentBlock.Text;
                            string formattedDate = String.Format("{0} {1}", date, CalendarHeader.Text);
                            SelectedDate = Convert.ToDateTime(formattedDate);
                        }
                    };
                    singleDateGrid.GestureRecognizers.Add(singleDateGridTapGesture);
                    i++;
                }
            }
        }

        private void refreshDateBackgrounds()
        {
            //sets each date design back to default
            foreach (var stack in CalendarStack.Children)
            {
                foreach (var gridContainer in (stack as StackLayout).Children)
                {
                    var singleDateGrid = (gridContainer as Grid);
                    var singleDateLabel = (singleDateGrid.Children[0] as Label);
                    singleDateLabel.BackgroundColor = Color.Transparent;
                    singleDateLabel.TextColor = Color.Gray;
                }
            }
        }

        private void RevertSelectedDate()
        {
            //used when month is changed.
            //sets SelectedDate property to Initail ReceivedDate property
            SelectedDate = ReceivedDate;
        }
    }
}
