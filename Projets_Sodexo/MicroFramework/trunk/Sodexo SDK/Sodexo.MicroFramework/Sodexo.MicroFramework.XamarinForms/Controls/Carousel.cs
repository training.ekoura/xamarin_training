﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sodexo.Controls
{
    /// <summary>
    /// CarousselView (aproximatif).
    /// L'objectif n'est pas de développer un View parfait répondant à tous les besoins,
    /// mais de surfacer le plus rapidement possible les besoins du projet en cours dans les échéances données.
    /// Nous ne voulons pas pour le moment faire de renderer pour des raisons de temps.
    /// </summary>
    public class Carousel : ContentView
    {
        public enum carauselType
        {
            Old,
            InfoPage
        }

        private View l_Item;

        private Grid temp_Item;

        private Grid _RootGrid;

        private Grid _ItemsGrid;

        private Grid _TouchGrid;

        private Grid _LeftNavigationGrid;

        private Grid _RightNavigationGrid;

        private StackLayout _BulletLayout;

        private DateTime _StartGesture;

        private Boolean CarouselSourceIsReady = false;

        public Carousel()
        {
            this.SizeChanged += Carousel_SizeChanged;

        }

        void Carousel_SizeChanged(object sender, EventArgs e)
        {


        }

        #region === Type ===

        public static readonly BindableProperty TypeProperty = BindableProperty.Create<Carousel, carauselType>(p => p.Type, carauselType.Old, propertyChanged: (d, o, n) => (d as Carousel).Type_Changed(o, n));

        /// <summary>
        /// </summary>
        /// <param name="oldvalue"></param>
        /// <param name="newvalue"></param>
        private void Type_Changed(carauselType oldvalue, carauselType newvalue)
        {

        }

        public carauselType Type
        {
            get { return (carauselType)GetValue(TypeProperty); }
            set { SetValue(TypeProperty, value); }
        }

        #endregion
        

        #region  === ItemsSource ===

            private List<View> _ItemsSourceListOfView;

            public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create<Carousel, object>(p => p.ItemsSource, null, propertyChanged: (d, o, n) => (d as Carousel).ItemsSource_Changed(o, n));

            /// <summary>
            /// Handler qui se déclanche quand la collection du Caroussel change.
            /// </summary>
            /// <param name="oldvalue"></param>
            /// <param name="newvalue"></param>
            private void ItemsSource_Changed(object oldvalue, object newvalue)
            {

                var _DoBuild = true;
                if (oldvalue is INotifyCollectionChanged)
                {
                    _DoBuild = false;
                    _InternalIndex = 0;
                    // On met la valeur du SelectedIndex à -1
                    SelectedIndex = -1;

                    // On déclanche un build suite à ce changement.
                    (oldvalue as INotifyCollectionChanged).CollectionChanged -= Carousel_CollectionChanged;
                }

                if (newvalue is INotifyCollectionChanged)
                {
                    _DoBuild = false;
                    // On déclanche un build suite à ce changement.
                    (newvalue as INotifyCollectionChanged).CollectionChanged += Carousel_CollectionChanged;
                }

                // Reconstruction du caroussel.
                if(_DoBuild || (oldvalue == null && newvalue != null))
                    Build();

            }

            // Met à jour le caroussel à chaque que la collection change.
            void Carousel_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
            {
                Build();
            }

            // Collection qui doit populer le caroussel. 
            public object ItemsSource
            {
                get { return (object)GetValue(ItemsSourceProperty); }
                set { SetValue(ItemsSourceProperty, value); }
            }

        #endregion

        #region === SelectedItem ===

            public static readonly BindableProperty SelectedItemProperty = BindableProperty.Create<Carousel, object>(p => p.SelectedItem, default(object), propertyChanged: (d, o, n) => (d as Carousel).SelectedItem_Changed(o, n));

            private void SelectedItem_Changed(object oldvalue, object newvalue)
            {
                // TODO :   Traitement after SelectedItem changed.
                //          ==> Reply : Nothing for the moment.
             
            }

            public object SelectedItem
            {
                get { return (object)GetValue(SelectedItemProperty); }
                set { SetValue(SelectedItemProperty, value); }
            }

        #endregion

        #region === SelectedIndex ===

            public static readonly BindableProperty SelectedIndexProperty = BindableProperty.Create<Carousel, int>(p => p.SelectedIndex, -1, propertyChanged: (d, o, n) => (d as Carousel).SelectedIndex_Changed(o, n));

            private void SelectedIndex_Changed(int oldvalue, int newvalue)
            {
                // TODO :   Traitement after SelectedItem changed.
                //          ==> Reply : Nothing for the moment.
            }

            public int SelectedIndex
            {
                get { return (int)GetValue(SelectedIndexProperty); }
                set { SetValue(SelectedIndexProperty, value); }
            }

        #endregion

        #region === ItemTemplate ===

            public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create<Carousel, DataTemplate>(p => p.ItemTemplate, null, propertyChanged: (d, o, n) => (d as Carousel).ItemTemplate_Changed(o, n));
            
            // J'ai commenté le build ne voyant pas son utilité pour le moment.
            private void ItemTemplate_Changed(DataTemplate oldvalue, DataTemplate newvalue)
            {
                //Build();
                temp_Item = (ItemTemplate.CreateContent() as View) as Grid;                
            }

            public DataTemplate ItemTemplate
            {
                get { return (DataTemplate)GetValue(ItemTemplateProperty); }
                set { SetValue(ItemTemplateProperty, value); }
            }

        #endregion

        #region === TapCommand ===

            public static readonly BindableProperty TapCommandProperty = BindableProperty.Create<Carousel, ICommand>(p => p.TapCommand, default(ICommand), propertyChanged: (d, o, n) => (d as Carousel).TapCommand_Changed(o, n));

            private void TapCommand_Changed(ICommand oldvalue, ICommand newvalue)
            {

            }

            public ICommand TapCommand
            {
                get { return (ICommand)GetValue(TapCommandProperty); }
                set { SetValue(TapCommandProperty, value); }
            }

        #endregion

        #region === TapCommandParameter ===

            public static readonly BindableProperty TapCommandParameterProperty = BindableProperty.Create<Carousel, object>(p => p.TapCommandParameter, default(object), propertyChanged: (d, o, n) => (d as Carousel).TapCommandParameter_Changed(o, n));

            private void TapCommandParameter_Changed(object oldvalue, object newvalue)
            {

            }

            public object TapCommandParameter
            {
                get { return (object)GetValue(TapCommandParameterProperty); }
                set { SetValue(TapCommandParameterProperty, value); }
            }

        #endregion

        #region === ThemeParameter ===

            public static readonly BindableProperty ThemeParameterProperty = BindableProperty.Create<Carousel, object>(p => p.ThemeParameter, default(object), propertyChanged: (d, o, n) => (d as Carousel).ThemeParameter_Changed(o, n));

            /// <summary>
            /// Pour mettre à jour les bonnes couleurs des puces.
            /// </summary>
            /// <param name="oldvalue"></param>
            /// <param name="newvalue"></param>
            private void ThemeParameter_Changed(object oldvalue, object newvalue)
            {
                //   Build();
            }

            public object ThemeParameter
            {
                get { return (object)GetValue(ThemeParameterProperty); }
                set { SetValue(ThemeParameterProperty, value); }
            }

        #endregion

        #region === EndTraitement ===

            public static readonly BindableProperty EndTraitementProperty = BindableProperty.Create<Carousel, object>(p => p.EndTraitement, default(object), propertyChanged: (d, o, n) => (d as Carousel).EndTraitement_Changed(o, n));

            /// <summary>
            /// </summary>
            /// <param name="oldvalue"></param>
            /// <param name="newvalue"></param>
            private void EndTraitement_Changed(object oldvalue, object newvalue)
            {
                if (oldvalue == null && (bool)newvalue == true)
                    CarouselSourceIsReady = true;
                else
                    CarouselSourceIsReady = false;
            }

            public object EndTraitement
            {
                get { return (object)GetValue(EndTraitementProperty); }
                set { SetValue(EndTraitementProperty, value); }
            }

        #endregion

        #region === BulletHorizontalPosition ===

            public static readonly BindableProperty BulletHorizontalPositionProperty = BindableProperty.Create<Carousel, LayoutOptions>(p => p.BulletHorizontalPosition, LayoutOptions.End, propertyChanged: (d, o, n) => (d as Carousel).BulletHorizontalPosition_Changed(o, n));

            /// <summary>
            /// </summary>
            /// <param name="oldvalue"></param>
            /// <param name="newvalue"></param>
            private void BulletHorizontalPosition_Changed(LayoutOptions oldvalue, LayoutOptions newvalue)
            {

            }

            [TypeConverter(typeof(LayoutOptionsConverter))]
            public LayoutOptions BulletHorizontalPosition
            {
                get { return (LayoutOptions)GetValue(BulletHorizontalPositionProperty); }
                set { SetValue(BulletHorizontalPositionProperty, value); }
            }

            #endregion

        #region === bulletVerticalPosition ===

            public static readonly BindableProperty BulletVerticalPositionProperty = BindableProperty.Create<Carousel, LayoutOptions>(p => p.BulletVerticalPosition, LayoutOptions.End, propertyChanged: (d, o, n) => (d as Carousel).BulletVerticalPositionProperty_Changed(o, n));

            /// <summary>
            /// </summary>
            /// <param name="oldvalue"></param>
            /// <param name="newvalue"></param>
            private void BulletVerticalPositionProperty_Changed(LayoutOptions oldvalue, LayoutOptions newvalue)
            {

            }

            [TypeConverter(typeof(LayoutOptionsConverter))]
            public LayoutOptions BulletVerticalPosition
            {
                get { return (LayoutOptions)GetValue(BulletVerticalPositionProperty); }
                set { SetValue(BulletVerticalPositionProperty, value); }
            }

            #endregion

        #region === BulletRow ===

            public static readonly BindableProperty BulletRowProperty = BindableProperty.Create<Carousel, int>(p => p.BulletRow, default(int), propertyChanged: (d, o, n) => (d as Carousel).BulletRow_Changed(o, n));

            /// <summary>
            /// </summary>
            /// <param name="oldvalue"></param>
            /// <param name="newvalue"></param>
            private void BulletRow_Changed(int oldvalue, int newvalue)
            {

            }

            public int BulletRow
            {
                get { return (int)GetValue(BulletRowProperty); }
                set { SetValue(BulletRowProperty, value); }
            }

        #endregion

        #region === BulletColumn ===

            public static readonly BindableProperty BulletColumnProperty = BindableProperty.Create<Carousel, int>(p => p.BulletColumn, default(int), propertyChanged: (d, o, n) => (d as Carousel).BulletColumn_Changed(o, n));

            /// <summary>
            /// </summary>
            /// <param name="oldvalue"></param>
            /// <param name="newvalue"></param>
            private void BulletColumn_Changed(int oldvalue, int newvalue)
            {

            }

            public int BulletColumn
            {
                get { return (int)GetValue(BulletColumnProperty); }
                set { SetValue(BulletColumnProperty, value); }
            }

        #endregion

        public event EventHandler<EventArgs> Tap;

        private int _InternalIndex = 0;
        private Double _DeviceWidth = 0;

        /// <summary>
        /// Permet de charger la première news du caroussel.
        /// </summary>
        /// <returns></returns>
        async Task SendWithDelay(bool p_IsFromBuild = false)
        {
            await Task.Delay(500);
            UpdateZero();
        }

        /// <summary>
        /// Cacher des informations du caroussel avant le chargement des données.
        /// </summary>
        void HideCarouselChildren() {
            //_RootGrid.Opacity = 0;
        }

        /// <summary>
        /// Affiche le carousel après les traitements.
        /// </summary>
        void ShowCarouselChildren()
        {
            if (CarouselSourceIsReady) { 
                ApplyBulletTheme(0);
            }
        }

        void ApplyBulletTheme(int p_Index)
        {
            if (_BulletLayout.Children != null && _BulletLayout.Children.Count>0 && p_Index < _BulletLayout.Children.Count)
            switch ((string)ThemeParameter)
            {
                case ThemeNameForCarousel.MySodexo:
                    (_BulletLayout.Children[p_Index] as Image).Source = ImageSource.FromResource("Sodexo.Images.BulletSodexo.png");
                    break;
                case ThemeNameForCarousel.InspirationEntreprise:
                    (_BulletLayout.Children[p_Index] as Image).Source = ImageSource.FromResource("Sodexo.Images.BulletInsp.png");
                    break;
                case ThemeNameForCarousel.InspirationScolaire:
                    (_BulletLayout.Children[p_Index] as Image).Source = ImageSource.FromResource("Sodexo.Images.BulletInspE.png");
                    break;
                case ThemeNameForCarousel.MySodexo360:
                    (_BulletLayout.Children[p_Index] as Image).Source = ImageSource.FromResource("Sodexo.Images.Bullet360.png");
                    break;
                case ThemeNameForCarousel.Score:
                    (_BulletLayout.Children[p_Index] as Image).Source = ImageSource.FromResource("Sodexo.Images.BulletSco.png");
                    break;
                case ThemeNameForCarousel.CityLight:
                    (_BulletLayout.Children[p_Index] as Image).Source = ImageSource.FromResource("Sodexo.Images.BulletCity.png");
                    break;
                default:
                    (_BulletLayout.Children[p_Index] as Image).Source = ImageSource.FromResource("Sodexo.Images.BulletSodexo.png");
                    break;
            }

        }


        /// <summary>
        /// Permet de mettre en avant le premier slide du caroussel après le build.
        /// </summary>
        void UpdateZero()
        {
            var l_NCount = (ItemsSource as IEnumerable).Cast<object>().Count();
            _InternalIndex = l_NCount - 1;

            if (l_NCount > 1)
            {
                if (_DeviceWidth <= Double.Parse("0") && _RootGrid.Width > Double.Parse("0"))
                    _DeviceWidth = _RootGrid.Width;

                for (var l_RelativeIndex = 0; l_RelativeIndex < l_NCount; l_RelativeIndex++)
                {
                    var l_Index = _InternalIndex + l_RelativeIndex;
                    while (l_Index >= l_NCount) l_Index = l_Index - l_NCount;
                    var l_Item = _ItemsSourceListOfView[0];
                    if (_ItemsSourceListOfView.Count > l_Index)
                        l_Item = _ItemsSourceListOfView[l_Index];

                    // Positionement de l'élément là où ils doit être au début de l'animation.
                    l_Item.TranslationX = _DeviceWidth * l_RelativeIndex;
                    // Animation.
                    l_Item.TranslateTo(l_Item.TranslationX - 1 * _DeviceWidth, 0, 200, Easing.CubicOut);
                }

                _ItemsGrid.TranslateTo(0, 0, 200, Easing.CubicOut);

                _InternalIndex = 0;

                ApplyBulletTheme(0);
            }
            else
            {
                ApplyBulletTheme(0);
            }

            ShowCarouselChildren();
        }

        /// <summary>
        /// Construction du caroussel.
        /// </summary>
        void Build()
        {

            // Le build est imparfait car il est global.
            // Mais c'est suffisant pour répondre aux besoins.
            if (ItemTemplate == null || !(ItemsSource is IEnumerable)) return;
            
            var OldCarouselTypeGrid = new Grid()
            {
                IsClippedToBounds = true,
            };

            if (Type != carauselType.InfoPage)
            {
                _RootGrid = OldCarouselTypeGrid;
            }
            else
            {
                temp_Item.IsClippedToBounds = true;
                _RootGrid = temp_Item; ;
            }
            

            _ItemsGrid = new Grid();
            Grid.SetRowSpan(_ItemsGrid, Math.Max(1, _RootGrid.RowDefinitions.Count));
            _RootGrid.Children.Add(_ItemsGrid);

            _ItemsSourceListOfView = new List<View>();

            HideCarouselChildren();


            ////////////////////////////////////////////////////////////////////////////////////////////
            // On boucle sur la collection pour construire le caroussel.
            foreach (var data in (ItemsSource as IEnumerable).Cast<object>())
            {
                l_Item = ItemTemplate.CreateContent() as View;

                if (l_Item != null)
                {
                    try
                    {
                        l_Item.BindingContext = data;
                        _ItemsGrid.Children.Add(l_Item);
                        _ItemsSourceListOfView.Add(l_Item);
                    }
                    catch
                    {

                    }
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////////
            // Ajout d'une grille pour gérer les gestures
            _TouchGrid = new Grid();
            _TouchGrid.BackgroundColor = Color.Transparent;
            _RootGrid.Children.Add(_TouchGrid);
            _TouchGrid.GestureRecognizers.Add(new BaseGestureRecognizer(
                () =>
                {
                    _StartGesture = DateTime.Now;
                },
                (delta) =>
                {
                    _ItemsGrid.TranslationX = delta.TotalX;
                },
                (delta) =>
                {
                    if (ItemsSource is IEnumerable)
                    {
                        var l_Count = (ItemsSource as IEnumerable).Cast<object>().Count();
                        // Slide précédente
                        if (delta.TotalX > 50 && SelectedIndex >= 0)
                        {
                            MoveTo(SelectedIndex - 1);
                        }

                        // Slide suivante
                        else if (delta.TotalX < -50 && SelectedIndex <= l_Count - 1)
                        {
                            MoveTo(SelectedIndex + 1);
                        }

                        // Quand on sélectionne un slide.
                        else if (delta.TotalX < 5 && delta.TotalY < 5 && DateTime.Now.Subtract(_StartGesture) < TimeSpan.FromMilliseconds(200))
                        {
                            
                            //if (SelectedItem == null && l_Count == 1) {
                            if (SelectedItem == null && ItemsSource != null)
                            {
                                SelectedItem = (ItemsSource as IEnumerable<object>).ElementAt(0);
                            }

                            if (TapCommand != null)
                            {
                                TapCommand.Execute(TapCommandParameter);
                            }
                            if (Tap != null)
                            {
                                Tap(this, new EventArgs());
                            }
                        }
                    }

                    // La transition des différents slides.
                    _ItemsGrid.TranslateTo(0, 0, 200, Easing.CubicOut);
                }
            ));
            Gestures.SetHook(_TouchGrid, true);

            ////////////////////////////////////////////////////////////////////////////////////////////
            // Ajout des éléments latéraux pour navigation
            // C'est temporaire. On attendra les remontées utilisateurs pour changer ça.
            _LeftNavigationGrid = new Grid();
            _LeftNavigationGrid.HorizontalOptions = LayoutOptions.Start;
            _LeftNavigationGrid.Padding = new Thickness(5);
            _RootGrid.Children.Add(_LeftNavigationGrid);

            var l_LeftImage = new Image();
            l_LeftImage.HeightRequest = 80;
            l_LeftImage.HorizontalOptions = LayoutOptions.Center;
            l_LeftImage.VerticalOptions = LayoutOptions.Center;
            l_LeftImage.Source = ImageSource.FromResource("Sodexo.Images.left.png");
            _LeftNavigationGrid.Children.Add(l_LeftImage);

            _RightNavigationGrid = new Grid();
            _RightNavigationGrid.HorizontalOptions = LayoutOptions.End;
            _RightNavigationGrid.Padding = new Thickness(5);
            _RightNavigationGrid.BackgroundColor = Color.Transparent;

            _RootGrid.Children.Add(_RightNavigationGrid);

            var l_RightImage = new Image();
            l_RightImage.HeightRequest = 80;
            l_RightImage.HorizontalOptions = LayoutOptions.Center;
            l_RightImage.VerticalOptions = LayoutOptions.Center;
            l_RightImage.Source = ImageSource.FromResource("Sodexo.Images.right.png");
            _RightNavigationGrid.Children.Add(l_RightImage);
            l_LeftImage.GestureRecognizers.Add(new TapGestureRecognizer((v) =>
            {
                Update(-1);
            }));
            l_RightImage.GestureRecognizers.Add(new TapGestureRecognizer((v) =>
            {
                Update(+1);
            }));
            
            ////////////////////////////////////////////////////////////////////////////////////////////
            // Grille avec les puces.


            _BulletLayout = new StackLayout();
            _BulletLayout.Orientation = StackOrientation.Horizontal;
            _BulletLayout.HorizontalOptions = BulletHorizontalPosition;
            _BulletLayout.VerticalOptions = BulletVerticalPosition;
            _BulletLayout.Padding = new Thickness(5);

            if (Type != carauselType.InfoPage)
            {
                _RootGrid.Children.Add(_BulletLayout);
            }
            else
            {
                Grid.SetRow(_BulletLayout, BulletRow);
                Grid.SetColumn(_BulletLayout, BulletColumn);
                _RootGrid.Children.Add(_BulletLayout);
            }

            
           if(  _ItemsSourceListOfView.Count>1)
            for (var l_Index = 0; l_Index < _ItemsSourceListOfView.Count; l_Index++)
            {
                var l_Image = new Image() { WidthRequest = 10 };
                _BulletLayout.Children.Add(l_Image);

                var l_IndexLambda = l_Index;
                l_Image.GestureRecognizers.Add(new TapGestureRecognizer((v) =>
                {
                    MoveTo(l_IndexLambda);
                }));

                (_BulletLayout.Children[l_Index] as Image).Source = ImageSource.FromResource("Sodexo.Images.bullet.png");
            }

            if (_BulletLayout.Children.Count > 0)
            {
                ApplyBulletTheme(0);
            }

         

            Content = _RootGrid;

            ////////////////////////////////////////////////////////////////////////////////////////////
            // On sélectionne par défaut la première slide
            SelectedIndex = 0;
            if ((ItemsSource as IEnumerable<object>).Count() > 0)
                SelectedItem = (ItemsSource as IEnumerable<object>).ElementAt(0);
            // TODO-GAV : Désactivation des flèches "gauche et droite".
            //_LeftNavigationGrid.IsVisible = l_Count > 1 && SelectedIndex > 0;
            //_RightNavigationGrid.IsVisible = l_Count > 1 && SelectedIndex < l_Count - 1;
            _LeftNavigationGrid.IsVisible = false;
            _RightNavigationGrid.IsVisible = false;
            
            // On fait un Update du Caroussel.
            SendWithDelay(true);

            InvalidateLayout();
        }

        

        /// <summary>
        /// Déplacement 
        /// </summary>
        /// <param name="p_Index"></param>
        void MoveTo(int p_Index)
        {
            Update(p_Index - _InternalIndex);
        }

        private bool _IsInUpdate = false;

        void Update(int p_Direction = 0, bool p_IsFromBuild = false)
        {
            var x = ThemeParameter;
            if (_RootGrid == null) return;
            try
            {
                _IsInUpdate = true;
                var l_OldIndex = SelectedIndex;
                var l_Count = _ItemsSourceListOfView == null ? 0 : _ItemsSourceListOfView.Count;

                if (l_Count > 0)
                {
                    //var l_CurrentItem = _ItemsSourceListOfView[_Index];
                    var l_NewIndex = _InternalIndex + p_Direction;
                    while (l_NewIndex < 0) l_NewIndex = l_NewIndex + l_Count;
                    while (l_NewIndex >= l_Count) l_NewIndex = l_NewIndex - l_Count;

                    if (l_OldIndex != l_NewIndex)
                    {
                        if (p_Direction < 0)
                        {
                            for (var l_RelativeIndex = 0; l_RelativeIndex < l_Count; l_RelativeIndex++)
                            {
                                var l_Index = _InternalIndex - l_RelativeIndex;
                                while (l_Index < 0) l_Index = l_Index + l_Count;
                                var l_Item = _ItemsSourceListOfView[0];
                                if (_ItemsSourceListOfView.Count > l_Index)
                                    l_Item = _ItemsSourceListOfView[l_Index];
                                // Positionement de l'élément là où ils doit être au début de l'animation.
                                l_Item.TranslationX = -_RootGrid.Width * l_RelativeIndex;
                                // Animation.
                                l_Item.TranslateTo(l_Item.TranslationX - p_Direction * _RootGrid.Width, 0, 200, Easing.CubicOut);
                            }
                        }
                        else if (p_Direction >= 0)
                        {

                            for (var l_RelativeIndex = 0; l_RelativeIndex < l_Count; l_RelativeIndex++)
                            {
                                var l_Index = _InternalIndex + l_RelativeIndex;
                                while (l_Index >= l_Count) l_Index = l_Index - l_Count;
                                var l_Item = _ItemsSourceListOfView[0];
                                if (_ItemsSourceListOfView.Count > l_Index)
                                    l_Item = _ItemsSourceListOfView[l_Index];

                                // Positionement de l'élément là où ils doit être au début de l'animation.
                                l_Item.TranslationX = _RootGrid.Width * l_RelativeIndex;
                                // Animation.
                                l_Item.TranslateTo(l_Item.TranslationX - p_Direction * _RootGrid.Width, 0, 200, Easing.CubicOut);
                            }
                        }

                        _InternalIndex = l_NewIndex;
                    }

                    // On applique la bonne puce par rapport au bon thème.
                    var themevalue = ThemeParameter;
                    for (var l_Index = 0; l_Index < l_Count; l_Index++)
                    {
                        if (l_Index == l_NewIndex)
                        {
                            //Ici on change d'image selon le thème
                            ApplyBulletTheme(l_Index);
                        }
                        else
                        {
                            (_BulletLayout.Children[l_Index] as Image).Source = ImageSource.FromResource("Sodexo.Images.bullet.png");
                        }
                    }

                    SelectedIndex = _InternalIndex;
                    if (ItemsSource is IEnumerable && (ItemsSource as IEnumerable<object>).Count() > _InternalIndex)
                    {
                        SelectedItem = (ItemsSource as IEnumerable<object>).ElementAt(_InternalIndex);
                    }
                    else
                    {
                        SelectedItem = null;
                    }
                }

                // TODO-GAV : Désactivation des flèches "gauche et droite".
                //_LeftNavigationGrid.IsVisible = l_Count > 1 && SelectedIndex > 0;
                //_RightNavigationGrid.IsVisible = l_Count > 1 && SelectedIndex < l_Count - 1;
                _LeftNavigationGrid.IsVisible = false;
                _RightNavigationGrid.IsVisible = false;
            }
            finally
            {
                _IsInUpdate = false;
            }
        }
    }
}
