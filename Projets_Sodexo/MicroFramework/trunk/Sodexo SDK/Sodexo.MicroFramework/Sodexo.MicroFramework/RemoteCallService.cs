﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Sodexo
{
    // Exemple d'uri:
    //http://ws.main.portail.int.sodexonet.com/ws/V1_00/news.cfc?method=getCurrentNews&clientId=345&token&returnformat=json
    public class RemoteCallService : Sodexo.IRemoteCallService
    {
        private const string GET = "GET";

        private const string POST = "POST";

        private const string CACHE_CONTROL = "Cache-Control";

        private const string CACHE_CONTROL_NO_CACHE = "no-cache";

        private const string LOCAL_CACHE_EXTENSION = ".cache";

        private const string LOCAL_CACHE_REFERENCES = "CacheReferences" + LOCAL_CACHE_EXTENSION;

        private string _ServerUri;
        public string ServerUri
        {
            get { return _ServerUri; }
            set { _ServerUri = value; }
        }

        private string _HttpNoCacheParameterName = "nocache";
        public string HttpNoCacheParameterName
        {
            get { return _HttpNoCacheParameterName; }
            set { _HttpNoCacheParameterName = value; }
        }

        private bool _LocalStorageCacheIsEnabled = true;
        public bool LocalStorageCacheIsEnabled
        {
            get { return _LocalStorageCacheIsEnabled; }
            set { _LocalStorageCacheIsEnabled = value; }
        }

        private object _GlobalParameters;
        public object GlobalParameters
        {
            get { return _GlobalParameters; }
            set { _GlobalParameters = value; }
        }

        private RemoteCallServiceMode _Mode = RemoteCallServiceMode.Sodexo;
        public RemoteCallServiceMode Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }

        private object _Locker = new object();

        private IFileService _FileService;

        public RemoteCallService()
        {
            _FileService = Sodexo.Framework.Services.FileService(false);
            LoadCacheReferences();
        }

        #region === Cache References ===

        // Nous sommes contraint de gérer ainsi le cache car la taille des noms de fichiers
        // est trop limitée et qu'une gestion directe ne fonctionne pas.

        private Dictionary<string, string> _CacheReferences;

        private void LoadCacheReferences()
        {
            if (_FileService == null) return;

            lock (_Locker)
            {
                _CacheReferences = new Dictionary<string, string>();
                if (_FileService.Exist(LOCAL_CACHE_REFERENCES))
                {
                    var l_Result = _FileService.LoadText(LOCAL_CACHE_REFERENCES);
                    if (l_Result.Length > 0)
                    {
                        foreach (var l_Entry in (from l in l_Result.ToString().Split('\n')
                                                 let parts = l.Split('|')
                                                 select new { Key = parts[0], Value = parts[1] }))
                        {
                            _CacheReferences[l_Entry.Key] = l_Entry.Value;
                        }
                    }
                }
            }
        }

        private void SaveCacheReferences()
        {
            if (_FileService == null) return;

            lock (_Locker)
            {
                var l_Content = new StringBuilder();
                foreach (var l_Entry in _CacheReferences)
                {
                    if (l_Content.Length > 0) l_Content.Append('\n');
                    l_Content.Append(l_Entry.Key + '|' + l_Entry.Value);
                }
                _FileService.SaveText(LOCAL_CACHE_REFERENCES, l_Content);
            }
        }

        private string GetCacheReference(string p_Uri)
        {
            if (_FileService == null) return null;

            lock (_Locker)
            {
                if (!_CacheReferences.ContainsKey(p_Uri))
                {
                    _CacheReferences[p_Uri] = Guid.NewGuid().ToString("N") + LOCAL_CACHE_EXTENSION;
                    SaveCacheReferences();
                }
                return _CacheReferences[p_Uri];
            }
        }

        public void DeleteCacheReference(string p_Uri)
        {
            try
            {
                if (_CacheReferences.Keys.Any(k => k.Contains(p_Uri)))
                {
                    var matchingKey = _CacheReferences.Keys.Where(k => k.Contains(p_Uri)).ToList();
                    foreach (var sKey in matchingKey)
                        _CacheReferences.Remove(sKey);
                }
            }
            catch (Exception e)
            {
            }

        }
        #endregion

        private string MakeUri(string p_ServiceName, string p_MethodName, object p_Parameters)
        {
            var l_Uri = default(string);

            if (_Mode == RemoteCallServiceMode.Sodexo)
            {
                l_Uri = new Uri(new Uri(_ServerUri, UriKind.Absolute), new Uri(p_ServiceName, UriKind.Relative)).ToString();
            }
            else
            {
                l_Uri = new Uri(new Uri(_ServerUri, UriKind.Absolute), new Uri(p_ServiceName + "/" + p_MethodName, UriKind.Relative)).ToString();
            }

            //Construire l'URI quand meme s'il y'a pas de paramètres
            var l_Query = string.Empty;

            Action<string, object> l_AddToQuery = (p, v) =>
            {
                if (string.IsNullOrWhiteSpace(l_Query))
                {
                    l_Query = "?";
                }
                else if (!string.IsNullOrWhiteSpace(l_Query))
                {
                    l_Query += "&";
                }

                if (v != null)
                {
                    double d;
                    if (double.TryParse(v.ToString(), out d))
                        l_Query += p + "=" + System.Net.WebUtility.UrlEncode(v.ToString().Replace(",", "."));
                    else
                        l_Query += p + "=" + System.Net.WebUtility.UrlEncode(v.ToString());
                }
                else
                {
                    l_Query += p;
                }
            };

            if (_Mode == RemoteCallServiceMode.Sodexo)
            {
                l_AddToQuery("method", p_MethodName);
            }

            // Ajout des éventuels paramètres généraux.
            if (_GlobalParameters != null)
            {
                foreach (var l_Property in _GlobalParameters.GetType().GetTypeInfo().DeclaredProperties)
                {
                    l_AddToQuery(l_Property.Name, l_Property.GetValue(_GlobalParameters));
                }
            }

            // Ajout de l'éventuel paramètre pour le cache.
            if (!string.IsNullOrWhiteSpace(HttpNoCacheParameterName))
            {
                l_AddToQuery(HttpNoCacheParameterName, Guid.NewGuid().ToString("N"));
            }

            if (p_Parameters != null)
            {
                // Ajout des paramètres transmis.
                foreach (var l_Property in p_Parameters.GetType().GetTypeInfo().DeclaredProperties)
                {
                    l_AddToQuery(l_Property.Name, l_Property.GetValue(p_Parameters));
                }

            }
            // Ajout de la query finale.
            if (!string.IsNullOrWhiteSpace(l_Query))
            {
                l_Uri += l_Query;
            }
            return l_Uri;
        }

        public Task<RemoteCallResult> Call(string p_ServiceName, string p_MethodName = null, object p_Parameters = null, TimeSpan p_CacheValideDuration = default(TimeSpan))
        {
            return CallEx<RemoteCallResult>(p_ServiceName, p_MethodName, p_Parameters, p_CacheValideDuration);
        }

        public Task<RemoteCallResult<TResult>> Call<TResult>(string p_ServiceName, string p_MethodName = null, object p_Parameters = null, TimeSpan p_CacheValideDuration = default(TimeSpan))
        {
            return CallEx<RemoteCallResult<TResult>>(p_ServiceName, p_MethodName, p_Parameters, p_CacheValideDuration);
        }

        private Task<T> CallEx<T>(string p_ServiceName, string p_MethodName = null, object p_Parameters = null, TimeSpan p_CacheValideDuration = default(TimeSpan))
            where T : RemoteCallResult
        {
            var tcs = new TaskCompletionSource<T>();

            try
            {
                var l_Uri = MakeUri(p_ServiceName, p_MethodName, p_Parameters);

                var l_NetworkService = Sodexo.Framework.Services.Container().Resolve<INetworkService>();

                // Si on doit gérer le cache, on a besoin d'un nom de fichier pour le cache.
                var l_CacheFileName = default(string);

                if (p_CacheValideDuration != default(TimeSpan) && LocalStorageCacheIsEnabled)
                {
                    // ATTENTION : Nous devons recalculer l'Uri sans le paramètre 'NoCache' car sinon nous aurons à chaque fois une
                    // Uri différente et donc pas de nom de fichier cache constant.                
                    l_CacheFileName = p_ServiceName + "." + p_MethodName;

                    // Ajout des paramètres
                    if (p_Parameters != null)
                    {
                        foreach (var l_Property in p_Parameters.GetType().GetTypeInfo().DeclaredProperties)
                        {
                            var l_Value = l_Property.GetValue(p_Parameters);
                            l_CacheFileName += "|" + l_Property.Name + "=" + (l_Value == null ? "" : l_Value.ToString());
                        }
                    }

                    // ATTENTION :  La taille du nom de fichier ne doit pas excéder 255 caractères, il
                    // est possible que cela pose certains problèmes avec certains services.
                    l_CacheFileName = GetCacheReference(l_CacheFileName);
                }

                if (
                        (
                            p_CacheValideDuration != default(TimeSpan)
                            && LocalStorageCacheIsEnabled
                            && _FileService != null
                            && _FileService.Exist(l_CacheFileName)
                        )
                    &&
                        (
                            DateTime.Now.Subtract(_FileService.GetLastWriteTime(l_CacheFileName)) < p_CacheValideDuration
                            || !l_NetworkService.IsConnected
                        )
                    )
                {
                    // Le fichier de cache est encore valide.
                    Task.Run(() =>
                    {
                        // On Run une task juste histoire d'avoir aussi un scénario asyncrhone dans ce cas.
                        // C'est une histoire de consistance de scénarios.
                        try
                        {
                            var l_Result = _FileService.LoadObject<T>(l_CacheFileName);
                            tcs.SetResult(l_Result);
                        }
                        catch (Exception l_Exception)
                        {
                            var l_Result = Activator.CreateInstance<T>();
                            l_Result.Success = false;
                            l_Result.ExceptionMessage = l_Exception.Message;
                            l_Result.SetCommunicationException(l_Exception);
                            tcs.SetResult(l_Result);
                        }
                    });
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Status Network = " + l_NetworkService.IsConnected);
                    if (!l_NetworkService.IsConnected) throw new System.Net.WebException(Properties.Resources.ERR_NETWORK);
                    var l_sw = System.Diagnostics.Stopwatch.StartNew();
                    System.Diagnostics.Debug.WriteLine("--Avant appel webservice");
                    System.Diagnostics.Debug.WriteLine(String.Format("   Time  = {0}", l_sw.ElapsedMilliseconds));
                    System.Diagnostics.Debug.WriteLine(String.Format("    WS = {0}", l_Uri));
                    System.Diagnostics.Debug.WriteLine("--");

                    // Il faut appeler le serveur.
                    var l_WebRequest = System.Net.HttpWebRequest.CreateHttp(l_Uri);
                    l_WebRequest.Method = GET;
                    l_WebRequest.Headers[CACHE_CONTROL] = CACHE_CONTROL_NO_CACHE;

                    AsyncCallback l_EndResponseStream = (p_AsyncResult) =>
                    {
                        try
                        {
                            var l_WebResponse = l_WebRequest.EndGetResponse(p_AsyncResult);
                            System.Diagnostics.Debug.WriteLine(String.Format("juste après appel webservice -  Duration = {0}", l_sw.ElapsedMilliseconds));
                            var l_Stream = l_WebResponse.GetResponseStream();
                            System.Diagnostics.Debug.WriteLine(String.Format("getResponse appel webservice -  Duration = {0}", l_sw.ElapsedMilliseconds));

                            var l_SerializerService = Sodexo.Framework.Services.Container().Resolve<ISerializerService>();

                            using (var l_StreamReader = new System.IO.StreamReader(l_Stream))
                            {
                                var l_Result = default(T);
                                System.Diagnostics.Debug.WriteLine(String.Format("l_StreamReader.ReadToEnd deb -  Duration = {0}", l_sw.ElapsedMilliseconds));

                                var l_SB = new StringBuilder(l_StreamReader.ReadToEnd());
                                System.Diagnostics.Debug.WriteLine(String.Format("l_StreamReader.ReadToEnd fin-  Duration = {0}", l_sw.ElapsedMilliseconds));
                                try
                                {
                                    l_Result = l_SerializerService.Deserialize<T>(l_SB);
                                    System.Diagnostics.Debug.WriteLine(String.Format("after Deserialize -  Duration = {0}", l_sw.ElapsedMilliseconds));

                                }
                                catch (Exception l_DeserializeException)
                                {
                                    var l_Message = string.Format("Deserialize Exception, Service={0}, Method={1} (Look to the content in the debug traces).", p_ServiceName, p_MethodName);
                                    System.Diagnostics.Debug.WriteLine(l_Message
                                        + "\n----- CONTENT -----"
                                        + "\n" + l_SB.ToString()
                                        + "\n-------------------");
                                    throw new Exception(l_Message);
                                }

                                if (p_CacheValideDuration != default(TimeSpan)
                                    && LocalStorageCacheIsEnabled
                                    && _FileService != null
                                    && l_Result.Success)
                                {
                                    System.Diagnostics.Debug.WriteLine(String.Format("before save to file -  Duration = {0}", l_sw.ElapsedMilliseconds));
                                    // Nous allons enregistrer le fichier de cache.
                                    _FileService.SaveObject<T>(l_CacheFileName, l_Result);
                                    System.Diagnostics.Debug.WriteLine(String.Format("after save to file -  Duration = {0}", l_sw.ElapsedMilliseconds));
                                }

                                // Nous transmettons le resultat.
                                tcs.SetResult(l_Result as T);
                            }
                        }
                        catch (System.Net.WebException l_Exception)
                        {
                            var l_Result = Activator.CreateInstance<T>();
                            l_Result.Success = false;
                            l_Result.SetNetworkException();
                            tcs.SetResult(l_Result);
                        }
                        catch (Exception l_Exception)
                        {
                            var l_Result = Activator.CreateInstance<T>();
                            l_Result.Success = false;
                            l_Result.ExceptionMessage = l_Exception.Message;
                            l_Result.SetCommunicationException(l_Exception);
                            tcs.SetResult(l_Result);
                        }
                    };

                    AsyncCallback l_EndRequestStream = (p_AsyncResult) =>
                    {
                        try
                        {
                            var l_Stream = l_WebRequest.EndGetRequestStream(p_AsyncResult);
                            l_WebRequest.BeginGetResponse(l_EndResponseStream, null);
                        }
                        catch (Exception l_Exception)
                        {
                            var l_Result = Activator.CreateInstance<T>();
                            l_Result.Success = false;
                            l_Result.ExceptionMessage = l_Exception.Message;
                            l_Result.SetCommunicationException(l_Exception);
                            tcs.SetResult(l_Result);
                        }
                    };

                    if (l_WebRequest.Method == GET)
                    {
                        l_WebRequest.BeginGetResponse(l_EndResponseStream, null);
                    }
                    else if (l_WebRequest.Method == POST)
                    {
                        l_WebRequest.BeginGetRequestStream(l_EndRequestStream, null);
                    }
                }
            }
            catch (System.Net.WebException l_Exception)
            {
                Task.Run(() =>
                {
                    var l_Result = Activator.CreateInstance<T>();
                    l_Result.Success = false;
                    l_Result.SetNetworkException();
                    tcs.SetResult(l_Result);
                });
            }
            catch (Exception l_Exception)
            {
                Task.Run(() =>
                {
                    var l_Result = Activator.CreateInstance<T>();
                    l_Result.Success = false;
                    l_Result.ExceptionMessage = l_Exception.Message;
                    l_Result.SetCommunicationException(l_Exception);
                    tcs.SetResult(l_Result);
                });
            }

            return tcs.Task;
        }
    }
}

