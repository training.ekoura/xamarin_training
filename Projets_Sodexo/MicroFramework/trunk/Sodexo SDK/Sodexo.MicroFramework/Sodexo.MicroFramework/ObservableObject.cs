﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sodexo
{
    public class ObservableObject : INotifyPropertyChanged, INotifyDataErrorInfo, IObservableObject
    {
        private bool _IsInitialized = false;
        public bool IsInitialized
        {
            get { return _IsInitialized; }
        }

        private object _Master;
        public object Master
        {
            get { return _Master; }
            protected set 
            {
                if (_Master != null) throw new Exception("Master a déjà été définie.");
                if (_IsInitialized) throw new Exception("Master ne peut pas être défini après l'initialisation.");
                _Master = value; 
            }
        }

        private bool _IsInDoInitialize = false;

        private void DoInitialize()
        {
            lock (Locker)
            {
                if (_IsInDoInitialize) return;
                try
                {
                    _IsInDoInitialize = true;
                    OnInitialize();
                    _IsInitialized = true;
                    AfterInitialize();
                }
                finally
                {
                    _IsInDoInitialize = false;
                }
            }
        }

        protected virtual void OnInitialize()
        {

        }

        protected virtual void AfterInitialize()
        {

        }

        protected void NotifyPropertyChanged(AdvancedPropertyChangeEventArgs p_EventArgs)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("PROPERTY CHANGED : {0}", p_EventArgs.PropertyName);
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, p_EventArgs);
                }
            }
            catch(Exception l_Exception)
            {
                System.Diagnostics.Debug.WriteLine("===> NotifyPropertyChanged : {0}", l_Exception.Message);
            }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        private object _Locker = new object();
        protected object Locker
        {
            get { return _Locker; }
        }

        protected T GetValue<T>(Func<T> p_GetValueCallback)
        {
            lock (_Locker)
            {
                if (!_IsInitialized) DoInitialize();
                return p_GetValueCallback();
            }
        }

        protected void SetValue<T>(Func<T> p_GetValueCallback, Action<T> p_SetValueCallback,
            T p_NewValue,
            string p_PropertyName,
            Action<string,T,T> p_BeforeSetCallback = null,
            Action<string,T,T> p_AfterSetCallback = null)
        {
            lock (_Locker)
            {
                var l_OldValue = p_GetValueCallback();
                if (!Equals(l_OldValue, p_NewValue))
                {
                    if (p_BeforeSetCallback!=null)
                    {
                        p_BeforeSetCallback(p_PropertyName, l_OldValue, p_NewValue);
                    }
                           
                    p_SetValueCallback(p_NewValue);

                    if (p_AfterSetCallback != null)
                    {
                        p_AfterSetCallback(p_PropertyName, l_OldValue, p_NewValue);
                    }

                    if (_IsInitialized)
                    {
                        NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(p_PropertyName, l_OldValue, p_NewValue));
                    }

                    if (Master == null && HasErrors)
                    {
                        Validate(false);
                    }
                }
            }
        }

        #region === this ===

        public object this[string p_Expression]
        {
            get
            {
                lock(_Locker)
                {
                    if (string.IsNullOrWhiteSpace(p_Expression)) return string.Empty;
                    if(p_Expression.StartsWith("state:"))
                    {
                        var l_StateName = p_Expression.Remove(0,6);
                        var l_Parts = l_StateName.Split('|');
                        if (l_Parts.Length == 1)
                        {
                            if (_States == null) return string.Empty;
                            var l_State = GetState(l_StateName);
                            return l_State == null ? string.Empty : l_State.ToString();
                        }
                        else if (l_Parts.Length==2)
                        {
                            if (_States == null) return false;
                            l_StateName = l_Parts[0];
                            var l_Value = l_Parts[1];
                            var l_State = GetState(l_StateName);
                            var l_StateString = l_State == null ? string.Empty : l_State.ToString();
                            return l_StateString == l_Value;
                        }
                    }
                    return string.Empty;
                }
            }
        }

        #endregion

        #region === State ===

        private const string STATE_NOTIFICATION_FORMAT = "Item[state:{0}]";
        private const string STATE_NOTIFICATION2_FORMAT = "Item[state:{0}|{1}]";

        private Dictionary<Type, object> _States;

        private void NotifyStateChange(Type l_Key, object l_OldValue, object p_NewValue)
        {
            NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(string.Format(STATE_NOTIFICATION_FORMAT, l_Key.Name), l_OldValue, p_NewValue));
            if (l_OldValue != null)
            {
                NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(string.Format(STATE_NOTIFICATION2_FORMAT, l_Key.Name, l_OldValue), true, false));
            }
            if (p_NewValue != null)
            {
                NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(string.Format(STATE_NOTIFICATION2_FORMAT, l_Key.Name, p_NewValue), false, true));
            }
        }

        protected void SetState<TState>(TState p_State)
            where TState : struct
        {
            lock (_Locker)
            {
                if (_States == null) _States = new Dictionary<Type, object>();
                var l_Key = typeof(TState);
                var l_OldValue = default(object);

                // On recherche une éventuelle ancienne valeur.
                if(_States.ContainsKey(l_Key))
                {
                    l_OldValue = _States[l_Key];
                }

                // Si pas de changement... Inutile d'aller plus loin.
                if (Equals(l_OldValue, p_State)) return;

                // Stockage.
                _States[l_Key] = p_State;

                // Nous notifions l'acquisition de l'état.
                NotifyStateChange(l_Key, l_OldValue, p_State);
            }
        }

        protected TState? GetState<TState>()
            where TState : struct
        {
            lock (_Locker)
            {
                if (_States == null) return null;
                var l_Key = typeof(TState);
                if (_States.ContainsKey(typeof(TState)))
                {
                    return (TState)(object)_States[l_Key];
                }
                return null;
            }
        }

        protected object GetState(string p_StateName)
        {
            lock (_Locker)
            {
                if (_States == null) return null;
                var l_Key = (from k in _States.Keys where k.Name == p_StateName select k).FirstOrDefault();
                if (l_Key == null) return null;
                if (_States.ContainsKey(l_Key))
                {
                    return _States[l_Key];
                }
                return null;
            }
        }

        protected void ClearState<TState>()
            where TState : struct
        {
            lock (_Locker)
            {
                if (_States == null) return;
                var l_Key = typeof(TState);
                if (!_States.ContainsKey(l_Key)) return;
                var l_OldValue = _States[l_Key];
                _States.Remove(l_Key);
                NotifyStateChange(l_Key, l_OldValue, null);
            }
        }

        protected void ClearStates()
        {
            lock (_Locker)
            {
                if (_States == null) return;
                var l_States = _States.ToList();
                _States.Clear();
                foreach (var l_Entry in l_States)
                {
                    NotifyStateChange(l_Entry.Key, _States[l_Entry.Key], null);
                }
            }
        }

        #endregion

        #region === Validation ===

        private List<Action<IValidationContext>> _ValidationMethodList;

        /// <summary>
        /// Déclanche le cycle de validation standard.
        /// OnValidate sera appelée.
        /// </summary>
        /// <param name="p_CompleteValidation">false, signifie que dans OnValidate, les validations complexes ne seront pas faites.</param>
        public bool Validate(bool p_CompleteValidation = true)
        {
            return Validate(p_CompleteValidation, null);
        }

        /// <summary>
        /// Plus aucune règle de validation ne sera appliquée.
        /// Dans la pratique, même OnValidate sera pas appelée.
        /// </summary>
        public void ValidateRulesClear()
        {
            lock (_Locker)
            {
                _ValidationMethodList = new List<Action<IValidationContext>>();
            }
        }

        /// <summary>
        /// Les règles de validation par défaut seront utilisées.
        /// Dans la pratique, OnValidate sera appelée.
        /// </summary>
        public void ValidateRulesDefault()
        {
            lock (_Locker)
            {
                _ValidationMethodList = null;
            }
        }

        /// <summary>
        /// Remplace des règles de validation actuelle.
        /// Dans la pratique, même OnValidate sera pas appelée (sauf si p_ValidationCallback = OnValidate).
        /// </summary>
        /// <param name="p_ValidationCallback"></param>
        public void ValidateRulesReplace(Action<IValidationContext> p_ValidationCallback)
        {
            lock (_Locker)
            {
                _ValidationMethodList = new List<Action<IValidationContext>>();
                _ValidationMethodList.Add(p_ValidationCallback);
            }
        }

        /// <summary>
        /// Ajoute des règles de validation à celle déjà en cours.
        /// </summary>
        /// <param name="p_ValidationCallback"></param>
        public void ValidateRulesAdd(Action<IValidationContext> p_ValidationCallback)
        {
            lock (_Locker)
            {
                if (_ValidationMethodList == null)
                {
                    _ValidationMethodList = new List<Action<IValidationContext>>();
                    _ValidationMethodList.Add(OnValidate);
                }
                _ValidationMethodList.Add(p_ValidationCallback);
            }
        }

        /// <summary>
        /// Supprime des règles de validation.
        /// </summary>
        /// <param name="p_ValidationCallback"></param>
        public void ValidateRulesRemove(Action<IValidationContext> p_ValidationCallback)
        {
            lock (_Locker)
            {
                if (_ValidationMethodList != null)
                {
                    _ValidationMethodList.Remove(p_ValidationCallback);
                    if(_ValidationMethodList.Count == 1 && _ValidationMethodList.First() == OnValidate)
                    {
                        _ValidationMethodList = null;
                    }
                }                
            }
        }

        internal bool Validate(bool p_CompleteValidation, Action<IValidationContext> p_OnValidatePlusAction)
        {
            lock (_Locker)
            {
                ClearErrors();
                var l_Context = new ObservableObjectValidationContext(ClearErrors, AddError, p_CompleteValidation);

                // Appel des comportements de validation.
                if (_ValidationMethodList == null)
                {
                    OnValidate(l_Context);
                }
                else
                {
                    foreach(var l_ValidationMethod in _ValidationMethodList)
                    {
                        l_ValidationMethod(l_Context);
                    }
                }
                
                // Appel du comportement de validation étendu.
                if(p_OnValidatePlusAction!=null)
                {
                    p_OnValidatePlusAction(l_Context);
                }
                
                // Notifications.
                foreach (var e in _Errors) NotifyErrorsChanged(e.PropertyName);
                return !HasErrors;
            }
        }

        protected virtual void OnValidate(IValidationContext p_Context)
        {

        }

        private List<ObservableObjectDataErrorInfo> _Errors = new List<ObservableObjectDataErrorInfo>();

        private void ClearErrors()
        {
            lock (_Locker)
            {
                var list = (from error in _Errors select error.PropertyName).Distinct().ToList();
                _Errors.Clear();
                foreach (var propertyName in list) NotifyErrorsChanged(propertyName);
            }
        }

        private void AddError(string p_Message, params string[] p_Properties)
        {
            lock (_Locker)
            {
                if (p_Properties != null && p_Properties.Length > 0)
                {
                    foreach (var p in p_Properties)
                    {
                        _Errors.Add(new ObservableObjectDataErrorInfo(p, p_Message));
                        NotifyErrorsChanged(p);
                    }
                }
            }
        }

        public bool HasErrors
        {
            get
            {
                lock (_Locker)
                { return _Errors != null && _Errors.Count > 0; }
            }
        }

        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            lock (_Locker)
            {
                var result = (from error in _Errors where error.PropertyName == propertyName select error);
                return result;
            }
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        protected void NotifyErrorsChanged(string p_PropertyName)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("ERROR CHANGED : {0}", p_PropertyName);
                if (ErrorsChanged != null)
                {
                    ErrorsChanged(this, new DataErrorsChangedEventArgs(p_PropertyName));
                }
            }
            catch (Exception l_Exception)
            {
                System.Diagnostics.Debug.WriteLine("===> NotifyErrorsChanged : {0}", l_Exception.Message);
            }
        }

        #endregion

        #region === Work ===

        private bool _IsWorking;
        public bool IsWorking
        {
            get { return GetValue<bool>(()=>_IsWorking); }
            private set { SetValue<bool>(() => _IsWorking, (b) => { _IsWorking = b; }, value, "IsWorking"); }
        }

        private int _WorkCount = 0;

        protected void BeginWork()
        {
            var l_NeedEvent = false;

            lock(_Locker)
            {
                _WorkCount++;
                if(_WorkCount == 1)
                {
                    IsWorking = true;
                    SetState(WorkingState.IsWorking);
                    l_NeedEvent = true;
                }
            }

            if (l_NeedEvent && WorkStarted != null)
            {
                WorkStarted(this, new EventArgs());
            }
        }

        protected void EndWork()
        {
            var l_NeedEvent = false;

            lock (_Locker)
            {
                if (_WorkCount == 1)
                {
                    IsWorking = false;
                    SetState(WorkingState.Normal);
                    l_NeedEvent = true;
                }
                _WorkCount--;
            }

            if (l_NeedEvent && WorkStoped != null)
            {
                WorkStoped(this, new EventArgs());
            }
        }

        public event EventHandler<EventArgs> WorkStarted;

        public event EventHandler<EventArgs> WorkStoped;

        #endregion
    }

    public enum WorkingState
    {
        Normal, IsWorking
    }
}
