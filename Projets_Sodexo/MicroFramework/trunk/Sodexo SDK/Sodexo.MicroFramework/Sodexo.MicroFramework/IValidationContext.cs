﻿using System;
using System.Collections;

namespace Sodexo
{
    public interface IValidationContext
    {
        void AddError(string p_Message, params string[] p_Properties);
        void ClearErrors();
        bool CompleteValidation { get; }
        bool Involve<T>(T p_Item, Action<IValidationContext, T> p_Action = null);
        bool InvolveItems<T>(System.Collections.Generic.IEnumerable<T> p_Items, Action<IValidationContext, T> p_Action = null);
    }
}
