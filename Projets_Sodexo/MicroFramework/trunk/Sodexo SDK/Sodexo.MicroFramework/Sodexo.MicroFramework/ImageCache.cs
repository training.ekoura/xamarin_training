﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Sodexo
{

    public class ImageCache
    {

        // Cache of recently used images
        static List<string> ImagesCached;
        static bool isInitialize = false;
        private static IFileService _FileService = Sodexo.Framework.Services.Container().Resolve<IFileService>();

        public ImageCache()
        {
            lock (this)
            {
                if (!isInitialize)
                {
                    System.Diagnostics.Debug.WriteLine("deb initialize");
                    ImagesCached = _FileService.GetFilesInPath().ToList();
                    isInitialize = true;
                    System.Diagnostics.Debug.WriteLine("fin initialize");
                    System.Diagnostics.Debug.WriteLine(ImagesCached.ToString());
                }
            }
        }

        public void AddImage(MemoryStream image, string uri)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("ADD" + uri);

                Task.Run(() => _FileService.SaveImage(image, GetFileName(uri)))
                    .ContinueWith(t => ImagesCached.Add(GetFileName(uri))
                    , TaskContinuationOptions.OnlyOnRanToCompletion).ConfigureAwait(false);
                
            }
            catch (Exception)
            {
                
            }

        }

        private string GetFileName(string uri)
        {

            if (!String.IsNullOrEmpty(uri))
            {
                Uri _myUri = new Uri(uri);
                return _myUri.AbsolutePath.Replace("/","").ToLower();
            }
            else
                return String.Empty;
        }

        public string GetImagePath(string uri)
        {
            return _FileService.GetFileWPath(GetFileName(uri));
        }

        public MemoryStream GetImage(string uri)
        {
            System.Diagnostics.Debug.WriteLine("GET" + uri);
            return _FileService.LoadImage(GetFileName(uri));
        }

        public bool IsCached(string uri)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Cache" + uri);
                Uri _myUri = new Uri(uri);
                if (ImagesCached.Where(x => x == GetFileName(uri)).FirstOrDefault() != null)
                {
                    System.Diagnostics.Debug.WriteLine("Cache true");
                    return true;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Cache false");
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
