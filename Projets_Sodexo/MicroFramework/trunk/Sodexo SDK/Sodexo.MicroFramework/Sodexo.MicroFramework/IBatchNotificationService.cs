﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sodexo
{
    public interface IBatchNotificationService
    {
        void StartBatch(string APIKey);
        bool RegisterUser(string userNotificationId);
        bool HasBatchStarted { get; set; }
    }
}
