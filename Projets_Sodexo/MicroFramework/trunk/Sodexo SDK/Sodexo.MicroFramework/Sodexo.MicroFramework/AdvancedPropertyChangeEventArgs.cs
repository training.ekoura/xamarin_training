﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sodexo
{
    public class AdvancedPropertyChangeEventArgs : System.ComponentModel.PropertyChangedEventArgs
    {
        public AdvancedPropertyChangeEventArgs(string p_PropertyName, object p_OldValue, object p_NewValue) 
            :base(p_PropertyName)
        {
            _OldValue = p_OldValue;
            _NewValue = p_NewValue;
        }

        private object _OldValue;
        public object OldValue
        {
            get { return _OldValue; }
        }

        private object _NewValue;
        public object NewValue
        {
            get { return _NewValue; }
        }
    }
}
