﻿using System;
namespace Sodexo.Themes
{
    /// <summary>
    /// Service de gestion des thèmes.
    /// 
    /// Une autre raison du choix du C# à la place du Xaml, c'est que lorsqu'un thème est appliqué, il peut
    /// très bien faire des register de View afin de modifier les Views utilisées.
    /// 
    /// Les ressources définies par les thèmes
    /// </summary>
    public interface IThemeService
    {
        /// <summary>
        /// Applique un thème précédemment défini.
        /// </summary>
        /// <param name="p_ThemeName">Nom du thème.</param>
        void Apply(string p_ThemeName);

        /// <summary>
        /// Retourne ne nom du thème courant.
        /// Si la valeur est null, c'est qu'aucun thème n'a été défini.
        /// </summary>
        string Current { get; }
        
        /// <summary>
        /// Liste des noms de thèmes définis.
        /// </summary>
        string[] Themes { get; }
    }

    public class ThemeServiceException : Exception
    {
        public ThemeServiceException(string p_Message)
            : base(p_Message)
        {

        }
    }
}


