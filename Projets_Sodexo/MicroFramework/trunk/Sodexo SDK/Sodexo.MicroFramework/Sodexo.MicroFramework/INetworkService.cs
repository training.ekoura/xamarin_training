﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sodexo
{
    public interface INetworkService
    {
        bool IsConnected { get; }

        event EventHandler<EventArgs> StateChanged;
    }
}
