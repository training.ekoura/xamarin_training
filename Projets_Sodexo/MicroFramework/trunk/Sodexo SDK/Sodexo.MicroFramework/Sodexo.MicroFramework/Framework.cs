﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Sodexo
{
    // TODO MLB : Implémenter la gestion GPS
    // TODO MLB : Implémenter un scheduler.
    // TODO MLB : S'assurer du fonctionnement de INetworkService sur les différentes plateformes.
    // TODO : Dans InteractionService, s'assurer que pop event ne se déclanche que quand un close est fait.

    public static class Framework
    {
        public static void Initialize()
        {
            // Service de sérialisation
            Sodexo.Framework.Services.Container().Register<Sodexo.ISerializerService, Sodexo.SerializerService>(new Sodexo.SingletonInstanceManager());

            // Service des états applicatifs.
            Sodexo.Framework.Services.Container().Register<Sodexo.IApplicationStateService, Sodexo.ApplicationStateService>(new Sodexo.SingletonInstanceManager());

            // Service de gestion des assemblies.
            Sodexo.Framework.Services.Container().Register<Sodexo.IAssemblyService, Sodexo.AssemblyService>(new Sodexo.SingletonInstanceManager());

            // Enregistrement du NetworkService (Fake)
            //Sodexo.Framework.Services.Container().Register<Sodexo.INetworkService, Sodexo.NetworkService>(new Sodexo.SingletonInstanceManager(), RegisterBehavior.OverrideIfIsRegistred);

            // Enregistrement du service HTTP
            Sodexo.Framework.Services.Container().Register<Sodexo.IHttpService, Sodexo.HttpService>(new Sodexo.SingletonInstanceManager());

            // Enregistrement du service Remote
            Sodexo.Framework.Services.Container().Register<Sodexo.IRemoteCallService, Sodexo.RemoteCallService>(new Sodexo.SingletonInstanceManager());

            // Enregistrement de l'assembly
            Sodexo.Framework.Services.AssemblyService().RegisterAssembly(typeof(Framework).GetTypeInfo().Assembly);
        }

        public static IService Services
        {
            get { return null; }
        }
    }
}
