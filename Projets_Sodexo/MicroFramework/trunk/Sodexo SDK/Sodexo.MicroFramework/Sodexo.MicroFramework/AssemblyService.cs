﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sodexo
{
	public class AssemblyService : Sodexo.IAssemblyService
	{
		private List<System.Reflection.Assembly> _Assemblies = new List<System.Reflection.Assembly>();
		public System.Reflection.Assembly[] Assemblies
		{
			get { return _Assemblies.ToArray(); }
		}

		public void RegisterAssembly(System.Reflection.Assembly p_Assembly)
		{
			lock (_Assemblies)
			{
				_Assemblies.Add(p_Assembly);
			}
		}

		public System.IO.Stream GetResourceStream(string p_Name)
		{
			// lock (_Assemblies)
			// {
			if (String.IsNullOrEmpty(p_Name))
				return null;
			var l_Parts = p_Name.Split(':');
			var l_Assembly = (from a in _Assemblies
							  from rn in a.GetManifestResourceNames()
							  where rn == p_Name
							  select a).FirstOrDefault();
			if (l_Assembly != null)
			{
				return l_Assembly.GetManifestResourceStream(p_Name);
			}

			return null;
			//}
		}
	}
}
