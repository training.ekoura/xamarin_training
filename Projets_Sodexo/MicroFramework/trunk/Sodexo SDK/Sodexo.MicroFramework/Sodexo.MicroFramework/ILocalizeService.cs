﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sodexo
{
    public interface ILocalizeService
    {
        System.Globalization.CultureInfo GetCurrentCultureInfo();
    }
}
