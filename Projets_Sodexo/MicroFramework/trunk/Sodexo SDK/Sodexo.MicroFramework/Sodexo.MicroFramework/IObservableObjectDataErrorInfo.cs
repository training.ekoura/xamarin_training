﻿using System;
namespace Sodexo
{
    public interface IObservableObjectDataErrorInfo
    {
        string Message { get; }
        string PropertyName { get; }
        string ToString();
    }
}
