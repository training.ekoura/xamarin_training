﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sodexo
{
    public class MultiValueComparer
    {
        private List<int> _HashCodes;

        private bool AnyDifference(params object[] p_Values)
        {
            if (p_Values == null || !p_Values.Any()) return false;

            var l_OldHashCodes = _HashCodes;
            _HashCodes = (from v in p_Values select v == null ? 0 : v.GetHashCode()).ToList();

            if (l_OldHashCodes == null) return true;

            for (var i = 0; i < p_Values.Length;i++)
            {
                var l_OldValue = (l_OldHashCodes.Count < i) ? l_OldHashCodes[i] : 0;
                var l_NewValue = (_HashCodes.Count < i) ? _HashCodes[i] : 0;
                if(l_OldValue != l_NewValue)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool AnyDifference(ref MultiValueComparer p_Comparer, params object[] p_Values)
        {
            if (p_Comparer == null)
            {
                p_Comparer = new MultiValueComparer();
            }
            return p_Comparer.AnyDifference(p_Values);
        }
    }
}
