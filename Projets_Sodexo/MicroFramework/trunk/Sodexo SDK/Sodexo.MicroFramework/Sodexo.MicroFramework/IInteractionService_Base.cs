﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sodexo
{
    public abstract class Interaction : IInteraction
    {
        private TaskCompletionSource<IInteractionResult> _TaskCompletionSource;

        public Task<IInteractionResult> Run()
        {
            _TaskCompletionSource = new TaskCompletionSource<IInteractionResult>();
            DoRun();
            return _TaskCompletionSource.Task;
        }

        public void Close()
        {
            DoClose();
            _TaskCompletionSource.SetResult(new InteractionResult(false));
        }

        public void Cancel()
        {
            DoCancel();
            _TaskCompletionSource.SetResult(new InteractionResult(true));
        }

        protected abstract void DoRun();

        protected abstract void DoClose();

        protected abstract void DoCancel();
    }

    public abstract class ParameterizedInteraction<TParameters> : IParameterizedInteraction<TParameters>
    {
        private TaskCompletionSource<IInteractionResult> _TaskCompletionSource;

        public Task<IInteractionResult> Run(TParameters p_Parameters)
        {
            _TaskCompletionSource = new TaskCompletionSource<IInteractionResult>();
            DoRun(p_Parameters);
            return _TaskCompletionSource.Task;
        }

        public void Close()
        {
            DoClose();
            _TaskCompletionSource.SetResult(new InteractionResult(false));
        }

        public void Cancel()
        {
            DoCancel();
            _TaskCompletionSource.SetResult(new InteractionResult(true));
        }

        protected abstract void DoRun(TParameters p_Parameters);

        protected abstract void DoClose();

        protected abstract void DoCancel();
    }

    public abstract class Interaction<TResult> : IInteraction<TResult>
    {
        private TaskCompletionSource<IInteractionResult<TResult>> _TaskCompletionSource;

        public Task<IInteractionResult<TResult>> Run()
        {
            _TaskCompletionSource = new TaskCompletionSource<IInteractionResult<TResult>>();
            DoRun();
            return _TaskCompletionSource.Task;
        }

        public void Close(TResult p_Result)
        {
            DoClose(p_Result);
            _TaskCompletionSource.SetResult(new InteractionResult<TResult>(p_Result));
        }

        public void Cancel()
        {
            DoCancel();
            _TaskCompletionSource.SetResult(new InteractionResult<TResult>(default(TResult), true));
        }

        protected abstract void DoRun();

        protected abstract void DoClose(TResult p_Result);

        protected abstract void DoCancel();
    }

    public abstract class ParameterizedInteraction<TParameters, TResult> : IParameterizedInteraction<TParameters, TResult>
    {
        private TaskCompletionSource<IInteractionResult<TResult>> _TaskCompletionSource;

        public Task<IInteractionResult<TResult>> Run(TParameters p_Parameters)
        {
            _TaskCompletionSource = new TaskCompletionSource<IInteractionResult<TResult>>();
            DoRun(p_Parameters);
            return _TaskCompletionSource.Task;
        }

        public void Close(TResult p_Result)
        {
            DoClose(p_Result);
            _TaskCompletionSource.SetResult(new InteractionResult<TResult>(p_Result));
        }

        public void Cancel()
        {
            DoCancel();
            _TaskCompletionSource.SetResult(new InteractionResult<TResult>(default(TResult), true));
        }

        protected abstract void DoRun(TParameters p_Parameters);

        protected abstract void DoClose(TResult p_Result);

        protected abstract void DoCancel();
    }

    public class InteractionResult : IInteractionResult
    {
        public InteractionResult(bool p_IsCanceled = false)
        {
            _IsCanceled = p_IsCanceled;
        }

        private bool _IsCanceled;
        public bool IsCanceled
        {
            get { return _IsCanceled; }
        }
    }

    public class InteractionResult<TResult> : InteractionResult, IInteractionResult<TResult>
    {
        public InteractionResult()
            : base(true)
        {

        }

        public InteractionResult(TResult p_Result, bool p_IsCanceled = false)
            : base(p_IsCanceled)
        {
            _Result = p_Result;
        }

        private TResult _Result;
        public TResult Result
        {
            get { return _Result; }
        }
    }
}
