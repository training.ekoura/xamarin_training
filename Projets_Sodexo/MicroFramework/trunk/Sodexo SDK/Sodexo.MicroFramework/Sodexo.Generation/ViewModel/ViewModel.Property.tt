﻿<#@ template language="C#" debug="True" #><#+
	public partial class elementProperty 
	{
		private List<Action> _CodeBeforeSet = new List<Action>();

		public void AddCodeBeforeSet(Action p_Action)
        {
			_CodeBeforeSet.Add(p_Action);
        }

		private List<Action> _CodeAfterSet = new List<Action>();

		public void AddCodeAfterSet(Action p_Action)
        {
			_CodeAfterSet.Add(p_Action);
        }

		private List<Action> _CodeDependency = new List<Action>();

		public void AddCodeDependency(Action p_Action)
        {
			_CodeDependency.Add(p_Action);
        }

		private Action _CodeForGetValue;

		public void SetCodeForGetValue(Action p_Action)
        {
			_CodeForGetValue = p_Action;
        }

		private Action _CodeForSetValue;

		public void SetCodeForSetValue(Action p_Action)
        {
			_CodeForSetValue = p_Action;
        }

		public void Analyse_Phase1()
		{
        }

		public void Analyse_Phase2()
		{

        }

		public void Analyse_Phase3()
		{
			// Code qui sera inséré dans le constructeur.
			if(!string.IsNullOrWhiteSpace(DefaultValue))
            {
				CurrentViewModel.Contract.AddCodeToConstructor(()=>
				{
					WriteComment("Initialisation de la propriété {0} avec sa valeur par défaut.", Name);
					WriteCodeLine("{0} = {1};", Name, DefaultValue);
                });
            }

			// Si il y a des dépendences.
			if(_CodeDependency.Count>0)
            {
				CurrentViewModel.Contract.AddCodeAfterInit(()=>
				{
					WriteComment("Notification des dépendances de la propriété {0}.", Name);
					WriteCodeLine("Notify{0}Dependencies();", Name);
                });				
            }
        }

		public void Write()
		{


			using(RegionBlock("Propriété : {0}", Name))
            {
				WriteCodeLine("public const string {0}_PROPERTYNAME = \"{0}\";", Name);

				WriteCodeLine();

				WriteCodeLine("private {0} _{1};", Type, Name);
				WriteCommentSummary(CommentConcat("Propriété : " + Name, Description));
				WriteCodeLine("{2} {0} {1}", Type, Name, GetAccessor(Get, Set));
				using(CodeBlock())
				{
					WriteCodeLine("{0}get", GetAccessorForGet(Get, Set));
					using(CodeBlock())
					{
						WriteCode("return GetValue<{0}>(",Type, Name);
						if(_CodeForGetValue == null)
                        {
							WriteCode("() => _{1}", Type, Name);
                        }
						else
                        {
							_CodeForGetValue();
                        }
						WriteCodeLine(");");
					}
					WriteCodeLine("{0}set", GetAccessorForSet(Get, Set));
					using(CodeBlock())
					{
						WriteCode("SetValue<{0}>(",Type, Name);						
						if(_CodeForGetValue == null)
						{
							WriteCode("() => _{1}", Type, Name);
						}
						else
						{
							_CodeForGetValue();
						}
						WriteCode(", ");
						if(_CodeForSetValue == null)
						{
							WriteCode("(v) => _{1} = v", Type, Name);
						}
						else
						{
							_CodeForSetValue();
						}
						WriteCodeLine(", value, {1}_PROPERTYNAME,  Do{1}BeforeSet, Do{1}AfterSet);", Type, Name);					
					}
				}

				WriteCodeLine();

				WriteCommentSummary("Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.");
				WriteCodeLine("private void Do{0}BeforeSet(string p_PropertyName, {1} p_OldValue, {1} p_NewValue)", Name, Type);
				using(CodeBlock())
				{
					foreach(var l_Code in _CodeBeforeSet)
					{
						l_Code();
					}
				}

				WriteCodeLine();

				WriteCommentSummary("Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.");
				WriteCodeLine("private void Do{0}AfterSet(string p_PropertyName, {1} p_OldValue, {1} p_NewValue)", Name, Type);
				using(CodeBlock())
				{
					foreach(var l_Code in _CodeAfterSet)
					{
						l_Code();
					}
					if(_CodeDependency.Count>0)
                    {
						WriteComment("Puisque cette propriété a des dépendances, il faut les notifier.");
						WriteCodeLine("Notify{0}Dependencies();", Name);
                    }
				}

				WriteCodeLine();			

				WriteCommentSummary("Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.");
				WriteCodeLine("private void Notify{0}Dependencies()", Name, Type);
				using(CodeBlock())
				{
					WriteComment("Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.");
					WriteComment("Pour info, après l'initialisation, les calculs de dépendances seront effectués.");
					WriteCodeLine("if(!IsInitialized) return;");
					foreach(var l_Code in _CodeDependency)
					{
						l_Code();
					}
				}
            }

			WriteCodeLine();
		}

		public static string GetAccessor(accessor p_AccessorGet, accessor p_AccessorSet)
		{
			if(p_AccessorGet == p_AccessorSet) return p_AccessorGet.ToString().ToLower();
			return ((accessor)(Math.Min((int)p_AccessorGet, (int)p_AccessorSet))).ToString().ToLower();
		}

		public static string GetAccessorForGet(accessor p_AccessorGet, accessor p_AccessorSet)
		{
			if(p_AccessorGet == p_AccessorSet) return string.Empty;
			var l_Accessor = (accessor)(Math.Min((int)p_AccessorGet, (int)p_AccessorSet));
			if(p_AccessorGet == l_Accessor) return string.Empty;
			return ((accessor)(Math.Max((int)l_Accessor, (int)p_AccessorGet))).ToString().ToLower()+" ";
		}

		public static string GetAccessorForSet(accessor p_AccessorGet, accessor p_AccessorSet)
		{
			if(p_AccessorGet == p_AccessorSet) return string.Empty;
			var l_Accessor = (accessor)(Math.Min((int)p_AccessorGet, (int)p_AccessorSet));
			if(p_AccessorSet == l_Accessor) return string.Empty;
			return ((accessor)(Math.Max((int)l_Accessor, (int)p_AccessorSet))).ToString().ToLower()+" ";
		}
	}	
#>
