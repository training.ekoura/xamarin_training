﻿
// --------------------------------------------------------------------------------
// Le code de ce fichier a été auto-généré à partir du fichier XML InvoiceViewModel
// Ne modifiez jamais directement ce fichier. Modifiez plutôt le fichier XML puis
// relancez la génération.
// --------------------------------------------------------------------------------
// Générateur : Maximus.
// Templates :  Sodexo.
// Contact :    Michaël LEBRETON - 06 35 96 03 01 - mlebreton@netkoders.com.
// --------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Sodexo;

namespace Sodexo.Wpf.Test.ViewModels
{
	public abstract partial class InvoiceViewModelBase : Sodexo.ViewModel
	{
		protected override void OnInitialize()
		{
			base.OnInitialize();

			// Initialisation de la collection Lines.
			Lines = new System.Collections.ObjectModel.ObservableCollection<InvoiceLineViewModel>();
			// Initialisation de la collection Taxes.
			Taxes = new System.Collections.ObjectModel.ObservableCollection<TaxeKeyValue>();
			// Initialisation de la commande AddLineCommand.
			AddLineCommand = new DelegateCommand(AddLineCommand_CanExecute, AddLineCommand_Execute);
			// Initialisation de la commande DeleteLineCommand.
			DeleteLineCommand = new DelegateCommand(DeleteLineCommand_CanExecute, DeleteLineCommand_Execute);
			// Initialisation de la commande OkCommand.
			OkCommand = new DelegateCommand(OkCommand_CanExecute, OkCommand_Execute);
		}

		protected override void AfterInitialize()
		{
			base.AfterInitialize();

			// Notification des dépendances de la propriété Number.
			NotifyNumberDependencies();
			// Notification des dépendances de la propriété Date.
			NotifyDateDependencies();
			// Notification des dépendances de la propriété Description.
			NotifyDescriptionDependencies();
			// Notification des dépendances de la propriété Lines.
			NotifyLinesDependencies();
		}

		#region === Propriétés ===

			#region === Propriété : Id ===

				public const string Id_PROPERTYNAME = "Id";

				private Guid _Id;
				///<summary>
				/// Propriété : Id
				///</summary>
				public Guid Id
				{
					get
					{
						return GetValue<Guid>(() => _Id);
					}
					set
					{
						SetValue<Guid>(() => _Id, (v) => _Id = v, value, Id_PROPERTYNAME,  DoIdBeforeSet, DoIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoIdBeforeSet(string p_PropertyName, Guid p_OldValue, Guid p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoIdAfterSet(string p_PropertyName, Guid p_OldValue, Guid p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Number ===

				public const string Number_PROPERTYNAME = "Number";

				private string _Number;
				///<summary>
				/// Propriété : Number
				///</summary>
				public string Number
				{
					get
					{
						return GetValue<string>(() => _Number);
					}
					set
					{
						SetValue<string>(() => _Number, (v) => _Number = v, value, Number_PROPERTYNAME,  DoNumberBeforeSet, DoNumberAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoNumberBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoNumberAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
					// Puisque cette propriété a des dépendances, il faut les notifier.
					NotifyNumberDependencies();
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyNumberDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
					DoTestUpdate();
					DoAddLineCommandUpdate();
				}

			#endregion

			#region === Propriété : Date ===

				public const string Date_PROPERTYNAME = "Date";

				private DateTime? _Date;
				///<summary>
				/// Propriété : Date
				///</summary>
				public DateTime? Date
				{
					get
					{
						return GetValue<DateTime?>(() => _Date);
					}
					set
					{
						SetValue<DateTime?>(() => _Date, (v) => _Date = v, value, Date_PROPERTYNAME,  DoDateBeforeSet, DoDateAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDateBeforeSet(string p_PropertyName, DateTime? p_OldValue, DateTime? p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDateAfterSet(string p_PropertyName, DateTime? p_OldValue, DateTime? p_NewValue)
				{
					// Puisque cette propriété a des dépendances, il faut les notifier.
					NotifyDateDependencies();
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDateDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
					DoTestUpdate();
				}

			#endregion

			#region === Propriété : Description ===

				public const string Description_PROPERTYNAME = "Description";

				private string _Description;
				///<summary>
				/// Propriété : Description
				///</summary>
				public string Description
				{
					get
					{
						return GetValue<string>(() => _Description);
					}
					set
					{
						SetValue<string>(() => _Description, (v) => _Description = v, value, Description_PROPERTYNAME,  DoDescriptionBeforeSet, DoDescriptionAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDescriptionBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDescriptionAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
					// Puisque cette propriété a des dépendances, il faut les notifier.
					NotifyDescriptionDependencies();
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDescriptionDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
					DoTestUpdate();
				}

			#endregion

			#region === Propriété : TotalExcludingTaxes ===

				public const string TotalExcludingTaxes_PROPERTYNAME = "TotalExcludingTaxes";

				private decimal _TotalExcludingTaxes;
				///<summary>
				/// Propriété : TotalExcludingTaxes
				///</summary>
				public decimal TotalExcludingTaxes
				{
					get
					{
						return GetValue<decimal>(() => _TotalExcludingTaxes);
					}
					protected set
					{
						SetValue<decimal>(() => _TotalExcludingTaxes, (v) => _TotalExcludingTaxes = v, value, TotalExcludingTaxes_PROPERTYNAME,  DoTotalExcludingTaxesBeforeSet, DoTotalExcludingTaxesAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTotalExcludingTaxesBeforeSet(string p_PropertyName, decimal p_OldValue, decimal p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTotalExcludingTaxesAfterSet(string p_PropertyName, decimal p_OldValue, decimal p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTotalExcludingTaxesDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : TotalIncludingTaxes ===

				public const string TotalIncludingTaxes_PROPERTYNAME = "TotalIncludingTaxes";

				private decimal _TotalIncludingTaxes;
				///<summary>
				/// Propriété : TotalIncludingTaxes
				///</summary>
				public decimal TotalIncludingTaxes
				{
					get
					{
						return GetValue<decimal>(() => _TotalIncludingTaxes);
					}
					protected set
					{
						SetValue<decimal>(() => _TotalIncludingTaxes, (v) => _TotalIncludingTaxes = v, value, TotalIncludingTaxes_PROPERTYNAME,  DoTotalIncludingTaxesBeforeSet, DoTotalIncludingTaxesAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTotalIncludingTaxesBeforeSet(string p_PropertyName, decimal p_OldValue, decimal p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTotalIncludingTaxesAfterSet(string p_PropertyName, decimal p_OldValue, decimal p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTotalIncludingTaxesDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : TotalTaxes ===

				public const string TotalTaxes_PROPERTYNAME = "TotalTaxes";

				private decimal _TotalTaxes;
				///<summary>
				/// Propriété : TotalTaxes
				///</summary>
				public decimal TotalTaxes
				{
					get
					{
						return GetValue<decimal>(() => _TotalTaxes);
					}
					protected set
					{
						SetValue<decimal>(() => _TotalTaxes, (v) => _TotalTaxes = v, value, TotalTaxes_PROPERTYNAME,  DoTotalTaxesBeforeSet, DoTotalTaxesAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTotalTaxesBeforeSet(string p_PropertyName, decimal p_OldValue, decimal p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTotalTaxesAfterSet(string p_PropertyName, decimal p_OldValue, decimal p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTotalTaxesDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Lines ===

				public const string Lines_PROPERTYNAME = "Lines";

				private System.Collections.ObjectModel.ObservableCollection<InvoiceLineViewModel> _Lines;
				///<summary>
				/// Propriété : Lines
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<InvoiceLineViewModel> Lines
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<InvoiceLineViewModel>>(() => _Lines);
					}
					private set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<InvoiceLineViewModel>>(() => _Lines, (v) => _Lines = v, value, Lines_PROPERTYNAME,  DoLinesBeforeSet, DoLinesAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoLinesBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<InvoiceLineViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<InvoiceLineViewModel> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionLines_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoLinesAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<InvoiceLineViewModel> p_OldValue, System.Collections.ObjectModel.ObservableCollection<InvoiceLineViewModel> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionLines_Attach(p_NewValue);
					// Puisque cette propriété a des dépendances, il faut les notifier.
					NotifyLinesDependencies();
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyLinesDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
					DoTotalUpdate();
					DoAddLineCommandUpdate();
					DoDeleteLineCommandUpdate();
				}

			#endregion

			#region === Propriété : Taxes ===

				public const string Taxes_PROPERTYNAME = "Taxes";

				private System.Collections.ObjectModel.ObservableCollection<TaxeKeyValue> _Taxes;
				///<summary>
				/// Propriété : Taxes
				///</summary>
				public System.Collections.ObjectModel.ObservableCollection<TaxeKeyValue> Taxes
				{
					get
					{
						return GetValue<System.Collections.ObjectModel.ObservableCollection<TaxeKeyValue>>(() => _Taxes);
					}
					private set
					{
						SetValue<System.Collections.ObjectModel.ObservableCollection<TaxeKeyValue>>(() => _Taxes, (v) => _Taxes = v, value, Taxes_PROPERTYNAME,  DoTaxesBeforeSet, DoTaxesAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTaxesBeforeSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<TaxeKeyValue> p_OldValue, System.Collections.ObjectModel.ObservableCollection<TaxeKeyValue> p_NewValue)
				{
					// Nous ne surveillons plus la collection.
					WatchCollectionTaxes_Dettach(p_OldValue);
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTaxesAfterSet(string p_PropertyName, System.Collections.ObjectModel.ObservableCollection<TaxeKeyValue> p_OldValue, System.Collections.ObjectModel.ObservableCollection<TaxeKeyValue> p_NewValue)
				{
					// Nous devons surveiller la collection.
					WatchCollectionTaxes_Attach(p_NewValue);
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTaxesDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : Test ===

				public const string Test_PROPERTYNAME = "Test";

				private string _Test;
				///<summary>
				/// Propriété : Test
				///</summary>
				public string Test
				{
					get
					{
						return GetValue<string>(() => _Test);
					}
					private set
					{
						SetValue<string>(() => _Test, (v) => _Test = v, value, Test_PROPERTYNAME,  DoTestBeforeSet, DoTestAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoTestBeforeSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoTestAfterSet(string p_PropertyName, string p_OldValue, string p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyTestDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : AddLineCommand ===

				public const string AddLineCommand_PROPERTYNAME = "AddLineCommand";

				private IDelegateCommand _AddLineCommand;
				///<summary>
				/// Propriété : AddLineCommand
				///</summary>
				public IDelegateCommand AddLineCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _AddLineCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _AddLineCommand, (v) => _AddLineCommand = v, value, AddLineCommand_PROPERTYNAME,  DoAddLineCommandBeforeSet, DoAddLineCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoAddLineCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoAddLineCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyAddLineCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : DeleteLineCommand ===

				public const string DeleteLineCommand_PROPERTYNAME = "DeleteLineCommand";

				private IDelegateCommand _DeleteLineCommand;
				///<summary>
				/// Propriété : DeleteLineCommand
				///</summary>
				public IDelegateCommand DeleteLineCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _DeleteLineCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _DeleteLineCommand, (v) => _DeleteLineCommand = v, value, DeleteLineCommand_PROPERTYNAME,  DoDeleteLineCommandBeforeSet, DoDeleteLineCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoDeleteLineCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoDeleteLineCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyDeleteLineCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : OkCommand ===

				public const string OkCommand_PROPERTYNAME = "OkCommand";

				private IDelegateCommand _OkCommand;
				///<summary>
				/// Propriété : OkCommand
				///</summary>
				public IDelegateCommand OkCommand
				{
					get
					{
						return GetValue<IDelegateCommand>(() => _OkCommand);
					}
					private set
					{
						SetValue<IDelegateCommand>(() => _OkCommand, (v) => _OkCommand = v, value, OkCommand_PROPERTYNAME,  DoOkCommandBeforeSet, DoOkCommandAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoOkCommandBeforeSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoOkCommandAfterSet(string p_PropertyName, IDelegateCommand p_OldValue, IDelegateCommand p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyOkCommandDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : PhoneId ===

				public const string PhoneId_PROPERTYNAME = "PhoneId";

				private System.Guid _PhoneId;
				///<summary>
				/// Propriété : PhoneId
				///</summary>
				public System.Guid PhoneId
				{
					get
					{
						return GetValue<System.Guid>(MachinEncapsulation_PhoneId_GetValue);
					}
					set
					{
						SetValue<System.Guid>(MachinEncapsulation_PhoneId_GetValue, MachinEncapsulation_PhoneId_SetValue, value, PhoneId_PROPERTYNAME,  DoPhoneIdBeforeSet, DoPhoneIdAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoPhoneIdBeforeSet(string p_PropertyName, System.Guid p_OldValue, System.Guid p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoPhoneIdAfterSet(string p_PropertyName, System.Guid p_OldValue, System.Guid p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyPhoneIdDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion

			#region === Propriété : PhoneNumber ===

				public const string PhoneNumber_PROPERTYNAME = "PhoneNumber";

				private System.String _PhoneNumber;
				///<summary>
				/// Propriété : PhoneNumber
				///</summary>
				public System.String PhoneNumber
				{
					get
					{
						return GetValue<System.String>(MachinEncapsulation_PhoneNumber_GetValue);
					}
					set
					{
						SetValue<System.String>(MachinEncapsulation_PhoneNumber_GetValue, MachinEncapsulation_PhoneNumber_SetValue, value, PhoneNumber_PROPERTYNAME,  DoPhoneNumberBeforeSet, DoPhoneNumberAfterSet);
					}
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires avant la motification d'une propriété.
				///</summary>
				private void DoPhoneNumberBeforeSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode se charge d'effectuer tous les traitements nécessaires après motification d'une propriété.
				///</summary>
				private void DoPhoneNumberAfterSet(string p_PropertyName, System.String p_OldValue, System.String p_NewValue)
				{
				}

				///<summary>
				/// Cette méthode est utilisée en interne pour notifier les dépendances de la propriétés.
				///</summary>
				private void NotifyPhoneNumberDependencies()
				{
					// Si l'objet n'est pas encore intialisé, il ne faut pas poursuivre.
					// Pour info, après l'initialisation, les calculs de dépendances seront effectués.
					if(!IsInitialized) return;
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux collections ===

			// Notez que les propriétés de collection sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Collection : Lines ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection Lines aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionLines_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionLines_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionLines_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection Lines (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionLines_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionLines_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionLines_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection Lines aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionLines_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionLines_ItemPropertyChanged;
							OnLinesItemAdded((InvoiceLineViewModel)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection Lines (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionLines_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnLinesItemRemoved((InvoiceLineViewModel)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionLines_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionLines_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionLines_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionLines_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions les dépendances.
					NotifyLinesDependencies();
				}

				private void WatchCollectionLines_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyLinesDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection Lines.
				///</summary>
				protected virtual void OnLinesItemAdded(InvoiceLineViewModel p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection Lines.
				///</summary>
				protected virtual void OnLinesItemRemoved(InvoiceLineViewModel p_Item)
				{
				}

			#endregion

			#region === Collection : Taxes ===

				///<summary>
				/// Cette méthode se charge d'attacher la nouvelle collection Taxes aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionTaxes_Attach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons pas INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollectionTaxes_CollectionChanged;
						}
						// Nous devons surveiller les éventuels éléments déjà présent dans la collection.
						WatchCollectionTaxes_ItemsAttach(p_Collection.Cast<object>());
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher la collection Taxes (précédement attachée aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionTaxes_Dettach(IEnumerable p_Collection)
				{
					if(p_Collection != null)
					{
						// Nous ne surveillons plus les éventuels éléments présent dans la collection.
						WatchCollectionTaxes_ItemsDettach(p_Collection.Cast<object>());
						// Nous ne surveillons INotifyCollectionChanged.
						if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)
						{
							(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollectionTaxes_CollectionChanged;
						}
					}
				}

				///<summary>
				/// Cette méthode se charge d'attacher les items de la collection Taxes aux évènements de surveillance du ViewModel.
				///</summary>
				private void WatchCollectionTaxes_ItemsAttach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollectionTaxes_ItemPropertyChanged;
							OnTaxesItemAdded((TaxeKeyValue)l_Item);
						}
					}
				}

				///<summary>
				/// Cette méthode se charge de déttacher les items de la collection Taxes (précédement attachés aux évènements de surveillance du ViewModel).
				///</summary>
				private void WatchCollectionTaxes_ItemsDettach(IEnumerable<object> p_Items)
				{
					foreach(var l_Item in p_Items)
					{
						if(l_Item is INotifyPropertyChanged)
						{
							OnTaxesItemRemoved((TaxeKeyValue)l_Item);
							(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollectionTaxes_ItemPropertyChanged;
						}
					}
				}

				private void WatchCollectionTaxes_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
				{
					// Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.
					if(e.OldItems != null) WatchCollectionTaxes_ItemsAttach(e.OldItems.Cast<object>());

					// Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,
					// pour surveiller les changements des items et notifier les dépendances.
					if(e.NewItems != null) WatchCollectionTaxes_ItemsAttach(e.NewItems.Cast<object>());

					// Nous notifions les dépendances.
					NotifyTaxesDependencies();
				}

				private void WatchCollectionTaxes_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
				{
					// Une propriété d'un des items a changée.
					// A terme,il pourra être utile d'optimiser ce mécanisme.
					NotifyTaxesDependencies();
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection Taxes.
				///</summary>
				protected virtual void OnTaxesItemAdded(TaxeKeyValue p_Item)
				{
				}

				///<summary>
				/// Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection Taxes.
				///</summary>
				protected virtual void OnTaxesItemRemoved(TaxeKeyValue p_Item)
				{
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux propriétés calculées ===

			// Notez que les propriétés calculées sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Propriété calculée : Test  ===

				///<summary>
				/// Méthode de calcule de la propriété : Test.
				///</summary>
				protected abstract string OnTest_Calculate();


			#endregion

		#endregion

		#region === Spécificitées liées aux mises à jour (Update) ===

			#region === Mise à jour : TotalUpdate ===

				private bool _IsInTotalUpdate;
				///<summary>
				/// Cette méthode est appelée pour provoquer la mise à jour : TotalUpdate.
				///</summary>
				private void DoTotalUpdate()
				{
					lock(Locker)
					{
						if(_IsInTotalUpdate) return;
						try
						{
							_IsInTotalUpdate = true;
							OnTotalUpdate_Execute();
						}
						finally
						{
							_IsInTotalUpdate = false;
						}
					}
				}

				///<summary>
				/// Méthode de mise à jour : TotalUpdate.
				///</summary>
				protected abstract void OnTotalUpdate_Execute();

			#endregion

			#region === Mise à jour : TestUpdate ===

				private bool _IsInTestUpdate;
				///<summary>
				/// Cette méthode est appelée pour provoquer la mise à jour : TestUpdate.
				///</summary>
				private void DoTestUpdate()
				{
					lock(Locker)
					{
						if(_IsInTestUpdate) return;
						try
						{
							_IsInTestUpdate = true;
							OnTestUpdate_Execute();
						}
						finally
						{
							_IsInTestUpdate = false;
						}
					}
				}

				///<summary>
				/// Méthode de mise à jour : TestUpdate.
				///</summary>
				private void OnTestUpdate_Execute()
				{
					Test = OnTest_Calculate();
				}

			#endregion

			#region === Mise à jour : AddLineCommandUpdate ===

				private bool _IsInAddLineCommandUpdate;
				///<summary>
				/// Cette méthode est appelée pour provoquer la mise à jour : AddLineCommandUpdate.
				///</summary>
				private void DoAddLineCommandUpdate()
				{
					lock(Locker)
					{
						if(_IsInAddLineCommandUpdate) return;
						try
						{
							_IsInAddLineCommandUpdate = true;
							OnAddLineCommandUpdate_Execute();
						}
						finally
						{
							_IsInAddLineCommandUpdate = false;
						}
					}
				}

				///<summary>
				/// Méthode de mise à jour : AddLineCommandUpdate.
				///</summary>
				private void OnAddLineCommandUpdate_Execute()
				{
					AddLineCommand.InvalidateCanExecute();
				}

			#endregion

			#region === Mise à jour : DeleteLineCommandUpdate ===

				private bool _IsInDeleteLineCommandUpdate;
				///<summary>
				/// Cette méthode est appelée pour provoquer la mise à jour : DeleteLineCommandUpdate.
				///</summary>
				private void DoDeleteLineCommandUpdate()
				{
					lock(Locker)
					{
						if(_IsInDeleteLineCommandUpdate) return;
						try
						{
							_IsInDeleteLineCommandUpdate = true;
							OnDeleteLineCommandUpdate_Execute();
						}
						finally
						{
							_IsInDeleteLineCommandUpdate = false;
						}
					}
				}

				///<summary>
				/// Méthode de mise à jour : DeleteLineCommandUpdate.
				///</summary>
				private void OnDeleteLineCommandUpdate_Execute()
				{
					DeleteLineCommand.InvalidateCanExecute();
				}

			#endregion


		#endregion

		#region === Spécificitées liées aux commandes ===

			// Notez que les propriétés de commandes sont implémentées dans la région 'Propriétés'.
			// Vous ne trouverez ci-après que le code spécifique.

			#region === Commande : AddLineCommand ===

				private void AddLineCommand_EnsureParameter(object p_Parameter)
				{
					if(p_Parameter == null && (typeof(object) == typeof(string) || typeof(object).IsClass)) return;
					if(!(p_Parameter is object)) throw new Exception("Le paramètre transmis via la commande 'AddLineCommand' doit être de type 'object'.");
				}

				private bool AddLineCommand_CanExecute(object p_Parameter)
				{
					AddLineCommand_EnsureParameter(p_Parameter);
					var l_Parameter = (object)p_Parameter;
					return OnAddLineCommand_CanExecute(l_Parameter);
				}

				private void AddLineCommand_Execute(object p_Parameter)
				{
					AddLineCommand_EnsureParameter(p_Parameter);
					var l_Parameter = (object)p_Parameter;
					OnAddLineCommand_Execute(l_Parameter);
				}

				public virtual bool OnAddLineCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				public abstract void OnAddLineCommand_Execute(object p_Parameter);

			#endregion

			#region === Commande : DeleteLineCommand ===

				private void DeleteLineCommand_EnsureParameter(object p_Parameter)
				{
					if(p_Parameter == null && (typeof(InvoiceLineViewModel) == typeof(string) || typeof(InvoiceLineViewModel).IsClass)) return;
					if(!(p_Parameter is InvoiceLineViewModel)) throw new Exception("Le paramètre transmis via la commande 'DeleteLineCommand' doit être de type 'InvoiceLineViewModel'.");
				}

				private bool DeleteLineCommand_CanExecute(object p_Parameter)
				{
					DeleteLineCommand_EnsureParameter(p_Parameter);
					var l_Parameter = (InvoiceLineViewModel)p_Parameter;
					return OnDeleteLineCommand_CanExecute(l_Parameter);
				}

				private void DeleteLineCommand_Execute(object p_Parameter)
				{
					DeleteLineCommand_EnsureParameter(p_Parameter);
					var l_Parameter = (InvoiceLineViewModel)p_Parameter;
					OnDeleteLineCommand_Execute(l_Parameter);
				}

				public virtual bool OnDeleteLineCommand_CanExecute(InvoiceLineViewModel p_Parameter)
				{
					return true;
				}

				public abstract void OnDeleteLineCommand_Execute(InvoiceLineViewModel p_Parameter);

			#endregion

			#region === Commande : OkCommand ===

				private void OkCommand_EnsureParameter(object p_Parameter)
				{
					if(p_Parameter == null && (typeof(object) == typeof(string) || typeof(object).IsClass)) return;
					if(!(p_Parameter is object)) throw new Exception("Le paramètre transmis via la commande 'OkCommand' doit être de type 'object'.");
				}

				private bool OkCommand_CanExecute(object p_Parameter)
				{
					OkCommand_EnsureParameter(p_Parameter);
					var l_Parameter = (object)p_Parameter;
					return OnOkCommand_CanExecute(l_Parameter);
				}

				private void OkCommand_Execute(object p_Parameter)
				{
					OkCommand_EnsureParameter(p_Parameter);
					var l_Parameter = (object)p_Parameter;
					OnOkCommand_Execute(l_Parameter);
				}

				public virtual bool OnOkCommand_CanExecute(object p_Parameter)
				{
					return true;
				}

				public abstract void OnOkCommand_Execute(object p_Parameter);

			#endregion


		#endregion

		#region === Spécificitées liées aux encapsulations ===


			#region === Encapsulation : MachinEncapsulation ===

				// Champ pour l'encapsulation.
				// Les propriétés implémentées font directement référence à ce champ.
				private Sodexo.Wpf.Test.ViewModels.PhoneViewModel _MachinEncapsulation;

				///<summary>
				/// Encapsulation : MachinEncapsulation.
				///</summary>
				public Sodexo.Wpf.Test.ViewModels.PhoneViewModel GetMachinEncapsulation()
				{
					return _MachinEncapsulation;
				}

				///<summary>
				/// Encapsulation : MachinEncapsulation.
				///</summary>
				public void SetMachinEncapsulation(Sodexo.Wpf.Test.ViewModels.PhoneViewModel p_Value)
				{
					// En vue d'éventuelles notification (si initialisé), nous récupérons les anciennes valeurs (valeurs actuelles).
					var l_OldValuePhoneId = _MachinEncapsulation == null || !IsInitialized ? default(System.Guid) : PhoneId;
					var l_OldValuePhoneNumber = _MachinEncapsulation == null || !IsInitialized ? default(System.String) : PhoneNumber;

					// Affectation du nouvel object d'encapsulation.
					_MachinEncapsulation = p_Value;

					// Si pas initialisé, inutile d'aller plus loin.
					if(!IsInitialized) return;

					// Nous allons notifier, nous avons besoin des valeurs actuelles.
					var l_NewValuePhoneId = _MachinEncapsulation == null ? default(System.Guid) : PhoneId;
					var l_NewValuePhoneNumber = _MachinEncapsulation == null ? default(System.String) : PhoneNumber;

					// Nous notifions.
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(PhoneId_PROPERTYNAME, l_OldValuePhoneId, l_NewValuePhoneId));
					NotifyPhoneIdDependencies();
					NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs(PhoneNumber_PROPERTYNAME, l_OldValuePhoneNumber, l_NewValuePhoneNumber));
					NotifyPhoneNumberDependencies();
				}

				#region === Propriété : PhoneId ===

					///<summary>
					/// Méthode utilisée par la propriété PhoneId pour la lecture de la valeur corresponsande de MachinEncapsulation.
					///</summary>
					private System.Guid MachinEncapsulation_PhoneId_GetValue()
					{
						// Nous renvoyons la valeur par défaut si le type encapsulé n'est pas encore défini.
						// Ce choix sera sans doute revu plus tard.
						return _MachinEncapsulation != null ? _MachinEncapsulation.Id : default(System.Guid);
					}

					///<summary>
					/// Méthode utilisée par la propriété PhoneId pour l'affectation de la valeur corresponsande de MachinEncapsulation.
					///</summary>
					private void MachinEncapsulation_PhoneId_SetValue(System.Guid p_Value)
					{
						// Nous ignorons les set si le type encapsulé n'est pas encore défini.
						// Ce choix sera sans doute revu plus tard.
						if(_MachinEncapsulation == null) return;
						_MachinEncapsulation.Id = p_Value;
					}


				#endregion

				#region === Propriété : PhoneNumber ===

					///<summary>
					/// Méthode utilisée par la propriété PhoneNumber pour la lecture de la valeur corresponsande de MachinEncapsulation.
					///</summary>
					private System.String MachinEncapsulation_PhoneNumber_GetValue()
					{
						// Nous renvoyons la valeur par défaut si le type encapsulé n'est pas encore défini.
						// Ce choix sera sans doute revu plus tard.
						return _MachinEncapsulation != null ? _MachinEncapsulation.Number : default(System.String);
					}

					///<summary>
					/// Méthode utilisée par la propriété PhoneNumber pour l'affectation de la valeur corresponsande de MachinEncapsulation.
					///</summary>
					private void MachinEncapsulation_PhoneNumber_SetValue(System.String p_Value)
					{
						// Nous ignorons les set si le type encapsulé n'est pas encore défini.
						// Ce choix sera sans doute revu plus tard.
						if(_MachinEncapsulation == null) return;
						_MachinEncapsulation.Number = p_Value;
					}


				#endregion


			#endregion

		#endregion
	}

	public partial class InvoiceViewModel : InvoiceViewModelBase
	{
	}
}
