﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sodexo.Controls
{
    public class CustomButton : ContentView
    {
        private StackLayout _RootStackLayout;
        private Label buttonLabel;



        public CustomButton()
        {
            Build();
        }

        private void Build()
        {

            var dataTemplate = new DataTemplate(() =>
                {
                    _RootStackLayout = new StackLayout()
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand
                    };
                    _RootStackLayout.SetDynamicResource(StackLayout.HeightRequestProperty, "BUTTON_STACK_HEIGHT");

                    var childStack = new StackLayout()
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand
                    };

                    childStack.SetDynamicResource(StackLayout.StyleProperty, "SpecialWPStack");

                    var StackTapGesture = new TapGestureRecognizer();

                    StackTapGesture.Tapped += (object sender, EventArgs e) =>
                    {
                        if (TapCommand != null)
                        {
                            TapCommand.Execute(TapCommandParameter);
                        }
                    };
                    childStack.GestureRecognizers.Add(StackTapGesture);

                    buttonLabel = new Label();

                    buttonLabel.SetDynamicResource(Label.StyleProperty, "BUTTON_LABEL_STYLE");

                    childStack.Children.Add(buttonLabel);


                    _RootStackLayout.Children.Add(childStack);

                    return _RootStackLayout;
                    
                });

            this.ItemTemplate = dataTemplate;
            Content = ItemTemplate.CreateContent() as View;
        }

        #region === TapCommandParameter ===

        public static readonly BindableProperty TapCommandParameterProperty = BindableProperty.Create<CustomButton, object>(p => p.TapCommandParameter, default(object), propertyChanged: (d, o, n) => (d as CustomButton).TapCommandParameter_Changed(o, n));

        private void TapCommandParameter_Changed(object oldvalue, object newvalue)
        {

        }

        public object TapCommandParameter
        {
            get { return (object)GetValue(TapCommandParameterProperty); }
            set { SetValue(TapCommandParameterProperty, value); }
        }

        #endregion

        #region === ItemTemplate ===

            public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create<CustomButton, DataTemplate>(p => p.ItemTemplate, null, propertyChanged: (d, o, n) => (d as CustomButton).ItemTemplate_Changed(o, n));
            
            // J'ai commenté le build ne voyant pas son utilité pour le moment.
            private void ItemTemplate_Changed(DataTemplate oldvalue, DataTemplate newvalue)
            {
                //Build();
            }

            public DataTemplate ItemTemplate
            {
                get { return (DataTemplate)GetValue(ItemTemplateProperty); }
                set { SetValue(ItemTemplateProperty, value); }
            }

        #endregion

        #region === TapCommand ===

            public static readonly BindableProperty TapCommandProperty = BindableProperty.Create<CustomButton, ICommand>(p => p.TapCommand, default(ICommand), propertyChanged: (d, o, n) => (d as CustomButton).TapCommand_Changed(o, n));

            private void TapCommand_Changed(ICommand oldvalue, ICommand newvalue)
            {
            }

            public ICommand TapCommand
            {
                get { return (ICommand)GetValue(TapCommandProperty); }
                set { SetValue(TapCommandProperty, value); }
            }

        #endregion

        #region === ButtonLabel ===

            public static readonly BindableProperty ButtonLabelProperty = BindableProperty.Create<CustomButton, string>(p => p.ButtonLabel, default(string), propertyChanged: (d, o, n) => (d as CustomButton).ButtonLabel_Changed(o, n));

            /// <summary>
            /// Pour mettre à jour les bonnes couleurs des puces.
            /// </summary>
            /// <param name="oldvalue"></param>
            /// <param name="newvalue"></param>
            private void ButtonLabel_Changed(string oldvalue, string newvalue)
            {
                buttonLabel.Text = newvalue;
            }

            public string ButtonLabel
            {
                get { return (string)GetValue(ButtonLabelProperty); }
                set { SetValue(ButtonLabelProperty, value); }
            }

        #endregion

    }
}
