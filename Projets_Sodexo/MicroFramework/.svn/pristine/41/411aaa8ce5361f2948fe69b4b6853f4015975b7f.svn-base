﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sodexo.Controls
{
    public class Calendar : ContentView
    {
        StackLayout mainStack;
        DateTime calendarDate;
        DateTime currentCalendarDate;
        Label CalendarHeader;
        StackLayout CalendarStack;

        public Calendar()
        {
            calendarDate = DateTime.Today;
            Build();
            UpdateCalendar(calendarDate);
        }

        #region === SelectedDate ===

        public static readonly BindableProperty ButtonLabelProperty = BindableProperty.Create<Calendar, string>(p => p.SelectedDate, default(string), propertyChanged: (d, o, n) => (d as Calendar).SelectedDate_Changed(o, n));

        private void SelectedDate_Changed(string oldvalue, string newvalue)
        {
            //Build();
        }

        public string SelectedDate
        {
            get { return (string)GetValue(ButtonLabelProperty); }
            set { SetValue(ButtonLabelProperty, value); }
        }

        #endregion

        private void Build()
        {

            #region Header 
            var prevMonthButton = new RoundedButton()
            {
                Text = "-",
                //TextColor = Color.Red,
                //BorderColor = Color.Red,
                BorderWidth = 3,
                BackgroundColor = Color.Transparent,
                HorizontalOptions = LayoutOptions.Fill
            };
            prevMonthButton.SetDynamicResource(RoundedButton.StyleProperty, "RoundedButtonStyle");
            prevMonthButton.SetDynamicResource(RoundedButton.TextColorProperty, "GerantMainColor");
            prevMonthButton.SetDynamicResource(RoundedButton.BorderColorProperty, "GerantMainColor");
            prevMonthButton.Clicked += prevMonthButton_Clicked;

            CalendarHeader = new Label()
            {
                TextColor = Color.White,
                FontSize = 26,
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center
            };
            CalendarHeader.SetDynamicResource(Label.BackgroundColorProperty, "GerantSecondaryColor");
            var nextMonthButton = new RoundedButton()
            {
                Text = "+",
                //TextColor = Color.Red,
                //BorderColor = Color.Red,
                BorderWidth = 3,
                BackgroundColor = Color.Transparent,
                HorizontalOptions = LayoutOptions.Fill
            };
            nextMonthButton.SetDynamicResource(RoundedButton.StyleProperty, "RoundedButtonStyle");
            nextMonthButton.SetDynamicResource(RoundedButton.TextColorProperty, "GerantMainColor");
            nextMonthButton.SetDynamicResource(RoundedButton.BorderColorProperty, "GerantMainColor");
            nextMonthButton.Clicked += nextMonthButton_Clicked;

            var headerStack = new StackLayout()
            {
                WidthRequest=350,
                Spacing=10,
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions=LayoutOptions.Center,
                Orientation = StackOrientation.Horizontal,
                Children = { prevMonthButton, CalendarHeader, nextMonthButton }
            };
            #endregion

            #region dateNames
            var dateNameStack = new StackLayout()
            {
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Orientation = StackOrientation.Horizontal
            };

            var ListofDaysShortName = new List<string>() { "D", "L", "M", "M", "J", "V", "S", };

            foreach (var day in ListofDaysShortName)
            {
                var dateLabel = new Label()
                {
                    Text = day,
                    FontSize=18,
                    TextColor = Color.Blue,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Center
                };

                var dateNameGrid = new Grid()
                {
                    WidthRequest = 50,
                    HeightRequest = 50,
                    HorizontalOptions=LayoutOptions.FillAndExpand,
                    VerticalOptions=LayoutOptions.Fill,
                };

                Grid.SetColumn(dateLabel, 0);
                Grid.SetRow(dateLabel, 0);
                dateNameGrid.Children.Add(dateLabel);

                dateNameStack.Children.Add(dateNameGrid);
            }

            #endregion

            #region dates
            CalendarStack = new StackLayout()
            {
                Padding = new Thickness(0, 0, 0, 5),
                Spacing=0,
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            for (int i = 0; i <= 5; i++)
            {
                var subStack = new StackLayout()
                {
                    Orientation = StackOrientation.Horizontal,
                    VerticalOptions = LayoutOptions.Fill,
                    HorizontalOptions = LayoutOptions.FillAndExpand
                };

                for (int j = 0; j <= 6; j++)
                {
                    var dateNumber = new Label()
                    {
                        TextColor = Color.Gray,
                        FontSize=18,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Center
                    };

                    var dateNumberGrid = new Grid()
                    {
                        WidthRequest = 50,
                        HeightRequest=50,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.Fill
                    };

                    Grid.SetColumn(dateNumber, 0);
                    Grid.SetRow(dateNumber, 0);
                    dateNumberGrid.Children.Add(dateNumber);

                    subStack.Children.Add(dateNumberGrid);
                }

                CalendarStack.Children.Add(subStack);
            }
            #endregion

            mainStack = new StackLayout()
            {
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.White,
                Children = { headerStack, dateNameStack, CalendarStack }
            };

            Content = mainStack;
        }

        void prevMonthButton_Clicked(object sender, EventArgs e)
        {
            clearCurrentCalendarDate();
            calendarDate = calendarDate.AddMonths(-1);
            UpdateCalendar(calendarDate);
            refreshDateBackgrounds();
        }

        void nextMonthButton_Clicked(object sender, EventArgs e)
        {
            clearCurrentCalendarDate();
            calendarDate = calendarDate.AddMonths(1);
            UpdateCalendar(calendarDate);
            refreshDateBackgrounds();
        }

        public void UpdateCalendar(DateTime objdate)
        {
            CalendarHeader.Text = objdate.ToString("MMMM yyyy");
            objdate = new DateTime(objdate.Year, objdate.Month, 1);
            int dayOfWeek = (int)objdate.DayOfWeek + 1;
            int daysOfMonth = DateTime.DaysInMonth(objdate.Year, objdate.Month);
            int i = 1;
            foreach (var stack in CalendarStack.Children)
            {
                foreach (var gridContainer in (stack as StackLayout).Children)
                {
                    var singleDateGrid = (gridContainer as Grid);
                    singleDateGrid.BackgroundColor = Color.Transparent;
                    var singleDateLabel = singleDateGrid.Children[0] as Label;
                    if (i >= dayOfWeek && i < (daysOfMonth + dayOfWeek))
                    {
                        singleDateLabel.Text = (i - dayOfWeek + 1).ToString();
                    }
                    else
                    {
                        singleDateLabel.Text = "";
                    }

                    var singleDateGridTapGesture = new TapGestureRecognizer();
                    singleDateGridTapGesture.Tapped += (object sender, EventArgs e) =>
                    {

                        var currentBlock = ((sender as Grid).Children[0] as Label);

                        if (!String.IsNullOrEmpty(currentBlock.Text))
                        {
                            refreshDateBackgrounds();
                            currentBlock.SetDynamicResource(Label.BackgroundColorProperty, "GerantMainColor");
                            currentBlock.TextColor = Color.White;
                            string date = currentBlock.Text;
                            string formattedDate = String.Format("{0} {1}", date, CalendarHeader.Text);
                            SelectedDate = Convert.ToDateTime(formattedDate).ToString();
                        }
                    };
                    singleDateGrid.GestureRecognizers.Add(singleDateGridTapGesture);
                    i++;
                }
            }
        }

        private void refreshDateBackgrounds()
        {
            foreach (var stack in CalendarStack.Children)
            {
                foreach (var gridContainer in (stack as StackLayout).Children)
                {
                    var singleDateGrid = (gridContainer as Grid);
                    var singleDateLabel = (singleDateGrid.Children[0] as Label);
                    singleDateLabel.BackgroundColor = Color.Transparent;
                    singleDateLabel.TextColor = Color.Gray;
                }
            }
        }

        private void clearCurrentCalendarDate()
        {
            currentCalendarDate = DateTime.MinValue;
            SelectedDate = currentCalendarDate.ToString();
        }
    }
}
