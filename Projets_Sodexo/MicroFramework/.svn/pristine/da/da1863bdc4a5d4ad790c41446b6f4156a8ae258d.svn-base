﻿<#@ template language="C#" debug="True" #><#+
	public partial class elementCollection {

		private elementProperty _Property;

		public void Analyse_Phase1()
        {
			_Property = new elementProperty()
			{
				Name = Name,
				Type = string.Format("System.Collections.ObjectModel.ObservableCollection<{0}>", ItemType),
				Set = accessor.Public
            };

			CurrentViewModel.Contract.Property.Add(_Property);
        }

		public void Analyse_Phase2()
        {
			// Code qui sera inséré dans le constructeur.
			CurrentViewModel.Contract.AddCodeToConstructor(()=>
			{
				WriteComment("Initialisation de la collection {0}.", Name);
				WriteCodeLine("{0} = new System.Collections.ObjectModel.ObservableCollection<{1}>();", Name, ItemType);
            });

			_Property.AddCodeBeforeSet(()=>{
				WriteComment("Nous ne surveillons plus la collection.");
				WriteCodeLine("WatchCollection{0}_Dettach(p_OldValue);", Name);
            });

			_Property.AddCodeAfterSet(()=>{
				WriteComment("Nous devons surveiller la collection.");
				WriteCodeLine("WatchCollection{0}_Attach(p_NewValue);", Name);
            });
        }

		public void Analyse_Phase3()
		{
        }

		public void Write()
        {
            using(RegionBlock("Collection : {0}", Name))
            {
                WriteCommentSummary("Cette méthode se charge d'attacher la nouvelle collection {0} aux évènements de surveillance du ViewModel.", Name);
                WriteCodeLine("private void WatchCollection{0}_Attach(IEnumerable p_Collection)", Name);
                using(CodeBlock())
                {
                    WriteCodeLine("if(p_Collection != null)");
                    using(CodeBlock())
                    {
                        WriteComment("Nous ne surveillons pas INotifyCollectionChanged.");
                        WriteCodeLine("if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)");
                        using(CodeBlock())
                        {
                            WriteCodeLine("(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged += WatchCollection{0}_CollectionChanged;", Name);
                        }
                        WriteComment("Nous devons surveiller les éventuels éléments déjà présent dans la collection.");
                        WriteCodeLine("WatchCollection{0}_ItemsAttach(p_Collection.Cast<object>());", Name);
                    }
                }
                    
                WriteCodeLine();
                    
                WriteCommentSummary("Cette méthode se charge de déttacher la collection {0} (précédement attachée aux évènements de surveillance du ViewModel).", Name);
                WriteCodeLine("private void WatchCollection{0}_Dettach(IEnumerable p_Collection)", Name);
                using(CodeBlock())
                {
                    WriteCodeLine("if(p_Collection != null)");
                    using(CodeBlock())
                    {
                        WriteComment("Nous ne surveillons plus les éventuels éléments présent dans la collection.");
                        WriteCodeLine("WatchCollection{0}_ItemsDettach(p_Collection.Cast<object>());", Name);
                        WriteComment("Nous ne surveillons INotifyCollectionChanged.");
                        WriteCodeLine("if(p_Collection is System.Collections.Specialized.INotifyCollectionChanged)");
                        using(CodeBlock())
                        {
                            WriteCodeLine("(p_Collection as System.Collections.Specialized.INotifyCollectionChanged).CollectionChanged -= WatchCollection{0}_CollectionChanged;", Name);
                        }
                    }
                }
                    
                WriteCodeLine();
                    
                WriteCommentSummary("Cette méthode se charge d'attacher les items de la collection {0} aux évènements de surveillance du ViewModel.", Name);
                WriteCodeLine("private void WatchCollection{0}_ItemsAttach(IEnumerable<object> p_Items)", Name);
                using(CodeBlock())
                {
                    WriteCodeLine("foreach(var l_Item in p_Items)");
                    using(CodeBlock())
                    {
                        WriteCodeLine("if(l_Item is INotifyPropertyChanged)");
                        using(CodeBlock())
                        {
                            WriteCodeLine("(l_Item as INotifyPropertyChanged).PropertyChanged += WatchCollection{0}_ItemPropertyChanged;", Name);
                            WriteCodeLine("On{0}ItemAdded(({1})l_Item);", Name, ItemType);
                        }
                    }
                }
                    
                WriteCodeLine();
                    
                WriteCommentSummary("Cette méthode se charge de déttacher les items de la collection {0} (précédement attachés aux évènements de surveillance du ViewModel).", Name);
                WriteCodeLine("private void WatchCollection{0}_ItemsDettach(IEnumerable<object> p_Items)", Name);
                using(CodeBlock())
                {
                    WriteCodeLine("foreach(var l_Item in p_Items)");
                    using(CodeBlock())
                    {
                        WriteCodeLine("if(l_Item is INotifyPropertyChanged)");
                        using(CodeBlock())
                        {
                            WriteCodeLine("On{0}ItemRemoved(({1})l_Item);", Name, ItemType);
                            WriteCodeLine("(l_Item as INotifyPropertyChanged).PropertyChanged -= WatchCollection{0}_ItemPropertyChanged;", Name);
                        }
                    }
                }
                    
                WriteCodeLine();
                    
                WriteCodeLine("private void WatchCollection{0}_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)", Name);
                using(CodeBlock())
                {
                    WriteComment("Si certains items ont été supprimés, il faut se désabonner de INotifyPropertyChanged.");
                    WriteCodeLine("if(e.OldItems != null) WatchCollection{0}_ItemsAttach(e.OldItems.Cast<object>());", Name);
                        
                    WriteCodeLine();
                        
                    WriteComment("Si certains items ont été supprimés, il faut s'abonner à INotifyPropertyChanged,");
                    WriteComment("pour surveiller les changements des items et notifier les dépendances.");
                    WriteCodeLine("if(e.NewItems != null) WatchCollection{0}_ItemsAttach(e.NewItems.Cast<object>());", Name);
                        
                    WriteCodeLine();

					WriteComment("Nous notifions qu'il y a eu un changement sur la collection.");
                    WriteCodeLine("NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs({0}_PROPERTYNAME + \"[]\", null, null));", Name);

					WriteCodeLine();
					
                    WriteComment("Nous notifions les dépendances.");
                    WriteCodeLine("Notify{0}Dependencies();", Name);
                }
                    
                WriteCodeLine();
                    
                WriteCodeLine("private void WatchCollection{0}_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)", Name);
                using(CodeBlock())
                {
					WriteComment("Nous notifions qu'il y a eu un changement sur la collection.");
                    WriteCodeLine("NotifyPropertyChanged(new AdvancedPropertyChangeEventArgs({0}_PROPERTYNAME + \"[]\", null, null));", Name);

					WriteCodeLine();

                    WriteComment("Une propriété d'un des items a changée.");
                    WriteComment("A terme,il pourra être utile d'optimiser ce mécanisme.");
                    WriteCodeLine("Notify{0}Dependencies();", Name);
                }
                    
                WriteCodeLine();
                    
                WriteCommentSummary("Cette méthode peut être surchargée par les héritiers pour réagir à l'ajout d'un item dans la collection {0}.", Name);
                WriteCodeLine("protected virtual void On{0}ItemAdded({1} p_Item)", Name, ItemType);
                using(CodeBlock())
                {
					
                }
                    
                WriteCodeLine();
                    
                WriteCommentSummary("Cette méthode peut être surchargée par les héritiers pour réagir au retrait d'un item dans la collection {0}.", Name);
                WriteCodeLine("protected virtual void On{0}ItemRemoved({1} p_Item)", Name, ItemType);
                using(CodeBlock())
                {
					
                }
            } 

			WriteCodeLine();
        }
    }	
#>