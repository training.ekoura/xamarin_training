﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sodexo.Controls
{
    public class AdvancedPicker : ContentView
    {
        // Properties
        protected Grid _RootGrid = new Grid();
        protected StackLayout PagingStackLayout;
        protected ICommand SelectedCommand;
        protected StackLayout ItemsStackLayout;
        protected Boolean Wait = false;
        private Boolean _isScrollAutomaticInitialized;
        protected int l_PaddingValue = 12;
        protected double l_ItemWidth;
        private ContentPage _Page;
        private Image _PlusImage;
        private Grid _Overlay;
        private Label _Header;
        private int _NbValuePerView=3;

        /// <summary>
        /// Constructor
        /// </summary>
        public AdvancedPicker()
        {
            SetDynamicResource(ContentPage.BackgroundImageProperty, "BackGroundImage");
            _RootGrid = null;

            // Root Grid
            _RootGrid = new Grid()
            {
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.Fill,
            };
            _RootGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            _RootGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

            // Header du picker , 
            // By default, we set a if user don't send a SelectionTextProperty
            _Header = new Label
            {
                Text = Properties.Resources.PICKER_Header,
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                HeightRequest = 35,
                VerticalTextAlignment = TextAlignment.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                TextColor = Color.Black
            };

            // Size of each box in the ScrollView
            l_ItemWidth = (Sodexo.Framework.Services.InteractionService().DeviceWidth - l_PaddingValue * 2) / _NbValuePerView;

            // ItemsList for all numbers
            ItemsStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Padding = new Thickness(0, 0, 0, 0),
                Spacing = 0,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };

            {
                // Image du Picker
                _PlusImage = new Image()
                {
                    TranslationX = -20
                };

                // Default icone for the picker.
                //_PlusImage.SetDynamicResource(Image.StyleProperty, "CONST_ORDER_ADD_IMAGE_STYLE");
                Grid.SetColumn(_PlusImage, 0);
                Grid.SetRow(_PlusImage, 0);
                _RootGrid.Children.Add(_PlusImage);

                // Un overlay est utiliser pour normalement empêcher le focus et l'apparition du keyboard.
                _Overlay = new Grid()
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    BackgroundColor = Color.Transparent
                };
                Grid.SetColumn(_Overlay, 0);
                Grid.SetRow(_Overlay, 0);
                _Overlay.GestureRecognizers.Add(new TapGestureRecognizer((v) =>
                {
                    // Lorsque l'on tap l'overlay, on ouvre le picker.
                    _Open();
                }));
                _RootGrid.Children.Add(_Overlay);
            }
            Content = _RootGrid;
        }

        /// <summary>
        /// Open the picker.
        /// </summary>
        public void _Open()
        {
           if (!(ItemsSource is IEnumerable) || _Page != null) return;
            if (!IsEnabled) return;

            ActualElementIndex = (this.SelectedIndex == -1) ? 0 : this.SelectedIndex;

            SetActivePage();

            var l_InteractionService = Sodexo.Framework.Services.InteractionService() as NavigationPageInteractionService;

            // A chaque foi on reconstruit la page.
            _Page = new ContentPage()
            {
                Title = Title
            };

            // Diseable page NavigationBar on IOS.
            NavigationPage.SetHasNavigationBar(this, false);

            // Numbers scroll
            var ScrollView = new ScrollView
            {
                Orientation = ScrollOrientation.Horizontal,
                BackgroundColor = Color.White
            };
            
            ScrollView.Scrolled += ScrollView_Scrolled;

            // TODO-GAV : Find another way to do spacing before and end of numbers.
            ScrollView.Content = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Children = { 
                    // Do spacing before the first number.
                    new StackLayout() { WidthRequest = l_ItemWidth, VerticalOptions = LayoutOptions.FillAndExpand, BackgroundColor = Color.Transparent, Children = { new Label { Text = "", FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) } } }, 
                    // Numbers
                    ItemsStackLayout,
                    // Spacing at the end
                    new StackLayout() { WidthRequest = l_ItemWidth, VerticalOptions = LayoutOptions.FillAndExpand, BackgroundColor = Color.Transparent, Children = { new Label { Text = "", FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) } } }        
                }
            };

            // Page background image
            _Page.SetDynamicResource(ContentPage.BackgroundImageProperty, "BackGroundImage");
            {
                #region ==============  Picker Header =================
                    var _HeaderStackLayout = new StackLayout()
                    {
                        BackgroundColor = Color.White,
                        Children =
                        {
                            _Header
                        },
                        Padding = new Thickness(10,10,10,20)
                    };
                #endregion

                #region ==============  Picker image =================
                    var l_PickerImage = new Image()
                    {
                        Aspect = Aspect.AspectFit,
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.Center,
                        HeightRequest = 40,
                        WidthRequest = 40
                    };
                    l_PickerImage.Source = ImageSource.FromResource(String.Format("Sodexo.Images.picker{0}.png", Sodexo.Framework.Services.InteractionService().ThemeNameValue));
                
                    PagingStackLayout = new StackLayout()
                    {
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        WidthRequest = Sodexo.Framework.Services.InteractionService().DeviceWidth - 20,
                        VerticalOptions = LayoutOptions.EndAndExpand,
                        Children = {
                            l_PickerImage
                        }
                    };
                #endregion


                #region ==============  Grid for the whole page without the Button =================
                    var _PageGrid = new Grid
                    {
                        RowDefinitions = { 
                            new RowDefinition(){Height = new GridLength(1.5, GridUnitType.Star)},
                            new RowDefinition(){Height = new GridLength(9, GridUnitType.Star)},
                        },
                        Padding = new Thickness(10,10,10,0),
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand
                    };

                    var _GridforScrollAndPickerImage = new Grid
                    {
                        RowSpacing = 0,
                        RowDefinitions = { 
                            new RowDefinition(){Height = new GridLength(3.5, GridUnitType.Star)},
                            new RowDefinition(){Height = new GridLength(1, GridUnitType.Star)},
                        },
                        BackgroundColor = Color.White,
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand
                    };

                    Grid.SetRow(ScrollView, 0);
                    _GridforScrollAndPickerImage.Children.Add(ScrollView);

                    Grid.SetRow(PagingStackLayout, 0);
                    _GridforScrollAndPickerImage.Children.Add(PagingStackLayout);


                    Grid.SetRow(_HeaderStackLayout, 0);
                    _PageGrid.Children.Add(_HeaderStackLayout);

                    Grid.SetRow(_GridforScrollAndPickerImage, 1);
                    _PageGrid.Children.Add(_GridforScrollAndPickerImage);

                    //Grid.SetRow(PagingStackLayout, 1);
                    //_PageGrid.Children.Add(PagingStackLayout);

                #endregion

                #region =================== Item Command ======================
                    SelectedCommand = new Command<object>(item =>
                    {
                        SelectedIndex = this.ActualElementIndex;

                        l_InteractionService.Close();
                        _RootGrid = null;
                    });
                #endregion

                _Page.Disappearing += _Page_Disappearing;

                #region ==============  Validation Button =================
                    var l_ButtonValidate = new CustomButton()
                    {
                        ButtonLabel = Properties.Resources.BUTTON_Validate
                    };

                    l_ButtonValidate.TapCommand = SelectedCommand;

                    //l_ButtonValidate.GestureRecognizers.Add(buttonGesture);
                #endregion

                #region ================== Root stack for every element ========================
                    var _RootStackLayout = new StackLayout()
                    {
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        Spacing = 0,
                        Children = { 
                            _PageGrid,
                            l_ButtonValidate
                        }
                    };
                #endregion

                _Page.Content = _RootStackLayout;
            }

            l_InteractionService.Open(_Page, true);

            if (ActualElementIndex > 0) {
                Device.StartTimer(TimeSpan.FromSeconds(0.5), () =>
                {
                    ScrollView.ScrollToAsync(this.ActualElement.X, 0, false);
                    return false;
                });
            }
        }

        void _Page_Disappearing(object sender, EventArgs e)
        {
            if (_Page == null) return;
            _Page.Disappearing -= _Page_Disappearing;

            _RootGrid = null;
            _Page = null;
        }

        public int ItemsCount
        {
            get { return this.ItemsStackLayout.Children.Count; }
        }

        protected virtual void SetSelected(ISelectable selectable)
        {
            selectable.IsSelected = true;
        }

        public View ActualElement
        {
            get
            {
                return ItemsStackLayout.Children[ActualElementIndex];
            }
        }
        public int ActualElementIndex { get; set; }

        public bool ScrollToStartOnSelected { get; set; }

        #region === Title ===

        public static readonly BindableProperty TitleProperty = BindableProperty.Create<AdvancedPicker, string>(p => p.Title, default(string), propertyChanged: (d, o, n) => (d as AdvancedPicker).Title_Changed(o, n));

        private void Title_Changed(string oldvalue, string newvalue)
        {

        }

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        #endregion

        #region === SelectedIndex ===

        public static readonly BindableProperty SelectedIndexProperty = BindableProperty.Create<AdvancedPicker, int>(p => p.SelectedIndex, -1, propertyChanged: (d, o, n) => (d as AdvancedPicker).SelectedIndex_Changed(o, n));

        private void SelectedIndex_Changed(int oldvalue, int newvalue)
        {
            var l_List = (ItemsSource as IEnumerable).Cast<object>().ToList();
            SelectedItem = l_List[newvalue];
        }

        public int SelectedIndex
        {
            get { return (int)GetValue(SelectedIndexProperty); }
            set { SetValue(SelectedIndexProperty, value); }
        }

        #endregion

        #region === Selection text ===

        public static readonly BindableProperty SelectionTextProperty = BindableProperty.Create<AdvancedPicker, string>(p => p.SelectionText, default(string), propertyChanged: (d, o, n) => (d as AdvancedPicker).SelectionText_Changed(o, n));

        private void SelectionText_Changed(string oldvalue, string newvalue)
        {
            _Header.Text = newvalue;
        }

        public string SelectionText
        {
            get { return (string)GetValue(SelectionTextProperty); }
            set { SetValue(SelectionTextProperty, value); }
        }

        #endregion

        #region === CommandParameter(case RecetteId) ===

            public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create<AdvancedPicker, string>(p => p.CommandParameter, default(string), propertyChanged: (d, o, n) => (d as AdvancedPicker).CommandParameter_Changed(o, n));

            private void CommandParameter_Changed(string oldvalue, string newvalue)
            {

            }

            public string CommandParameter
            {
                get { return (string)GetValue(CommandParameterProperty); }
                set { SetValue(CommandParameterProperty, value); }
            }

        #endregion

        #region === CommandParameter2(case ArticleQuantite) ===

            public static readonly BindableProperty CommandParameter2Property = BindableProperty.Create<AdvancedPicker, int>(p => p.CommandParameter2, default(int), propertyChanged: (d, o, n) => (d as AdvancedPicker).CommandParameter2_Changed(o, n));

            private void CommandParameter2_Changed(int oldvalue, int newvalue)
            {
                if (ItemsSource is IEnumerable && newvalue > 0)
                {
                    try
                    {

                    }
                    catch (Exception)
                    {
                        //
                    }
                    finally
                    {
                        _NotifyCommandProperty2Changed();
                    }
                }
            }

            public int CommandParameter2
            {
                get { return (int)GetValue(CommandParameter2Property); }
                set { SetValue(CommandParameter2Property, value); }
            }

        #endregion

        public event EventHandler<EventArgs> CommandProperty2Changed;

        private void _NotifyCommandProperty2Changed()
        {
            if (CommandProperty2Changed != null)
            {
                CommandProperty2Changed(this, new EventArgs());
            }
        }

        public event EventHandler<EventArgs> SelectedItemChanged;

        public static readonly BindableProperty ItemsSourceProperty =
            BindableProperty.Create<AdvancedPicker, IEnumerable>(p => p.ItemsSource, default(IEnumerable<object>), BindingMode.TwoWay, null, ItemsSourceChanged);

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly BindableProperty SelectedItemProperty =
            BindableProperty.Create<AdvancedPicker, object>(p => p.SelectedItem, default(object), BindingMode.TwoWay, null, OnSelectedItemChanged);

        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly BindableProperty ItemTemplateProperty =
            BindableProperty.Create<AdvancedPicker, DataTemplate>(p => p.ItemTemplate, default(DataTemplate));
        
        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        private static void ItemsSourceChanged(BindableObject bindable, IEnumerable oldValue, IEnumerable newValue)
        {
            var itemsLayout = (AdvancedPicker)bindable;
            itemsLayout.SetItems();
            itemsLayout.SetPagination();
            //itemsLayout.ScrollAutomaticAsync();

        }

        protected virtual void SetItems()
        {
            ItemsStackLayout.Children.Clear();

            if (ItemsSource == null)
                return;

            foreach (var item in ItemsSource)
                ItemsStackLayout.Children.Add(GetItemView(item));

            //SelectedItem = ItemsSource.OfType<object>().FirstOrDefault();
        }

        protected virtual View GetItemView(object item)
        {
            var content = ItemTemplate.CreateContent();
            var view = content as View;
            view.WidthRequest = l_ItemWidth;

            if (view == null) return null;

            view.BindingContext = item;

            var gesture = new TapGestureRecognizer
            {
                Command = SelectedCommand,
                CommandParameter = item
            };

            AddGesture(view, gesture);

            return view;
        }

        protected void AddGesture(View view, TapGestureRecognizer gesture)
        {
            view.GestureRecognizers.Add(gesture);

            var layout = view as Layout<View>;

            if (layout == null)
                return;

            foreach (var child in layout.Children)
                AddGesture(child, gesture);
        }

        private static void OnSelectedItemChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var itemsView = (AdvancedPicker)bindable;
            if (newValue == oldValue)
                return;

            var selectable = newValue;
            itemsView.SetSelectedItem(selectable);
        }

        protected virtual void SetSelectedItem(object selectedItem)
        {
            var items = ItemsSource;

            var handler = SelectedItemChanged;
            if (handler != null)
                handler(this, new EventArgs());
        }

        protected virtual async void ScrollAutomaticAsync()
        {
            while (!_isScrollAutomaticInitialized)
            {
                _isScrollAutomaticInitialized = true;
                if (!Wait)
                {
                    SetActivePage();
                    await Task.Delay(5000);
                    this.ActualElementIndex++;
                    //await ScrollToActualAsync();
                    _isScrollAutomaticInitialized = false;
                }

            }
        }

        /*private async Task ScrollToActualAsync()
        {
            if (this.ActualElementIndex == this.ItemsCount)
                this.ActualElementIndex = 0;

            if (this.ActualElementIndex < 0)
                this.ActualElementIndex = 0;

            try
            {
                await this.ScrollView.ScrollToAsync(this.ActualElement.X, 0, false);
            }
            catch
            {
                //invalid scroll: sometimes happen
            }
        }*/

        protected virtual void SetPagination()
        {
            this.ActualElementIndex = 0;
        }

        protected virtual void SetActivePage()
        {
            try
            {
                for (int i = 0; i < this.ItemsCount; i++)
                {
                    (ItemsStackLayout.Children[i] as Label).TextColor = Color.FromHex("C2C2C2");
                }
                (ItemsStackLayout.Children[this.ActualElementIndex] as Label).TextColor = Color.Black;
            }
            catch { }
        }

        void ScrollView_Scrolled(object sender, ScrolledEventArgs e)
        {
            if (e.ScrollX % ItemsStackLayout.Children.First().Width != 0 && !Wait)
                Wait = true;

            for (int i = 1; i < this.ItemsCount; i++)
            {
                var previousItemX = ItemsStackLayout.Children[i - 1].X;
                var actualItemX = ItemsStackLayout.Children[i].X;

                if (e.ScrollX >= actualItemX-l_ItemWidth/3 && e.ScrollX <= actualItemX)
                {
                    this.ActualElementIndex = e.ScrollX == actualItemX ? i : ((actualItemX - e.ScrollX - l_ItemWidth) > 0 ? i - 1 : i);
                    SetActivePage();
                }
                else if (e.ScrollX == 0)
                {
                    this.ActualElementIndex = 0;
                    SetActivePage();
                }
            }
        }
    }

    public interface ISelectable
    {
        bool IsSelected { get; set; }

        ICommand SelectCommand { get; set; }
    }
}
