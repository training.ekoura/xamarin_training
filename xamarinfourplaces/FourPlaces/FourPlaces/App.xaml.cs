﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FourPlaces
{
    public partial class App : Application
    {
        public static string token;

        public App()
        {
            InitializeComponent();
            NavigationService.Configure("HomePage", typeof(Views.ListeLieuxPage));
            NavigationService.Configure("AddPlacePage", typeof(Views.AddPlacePage));
            NavigationService.Configure("PlaceMapPage", typeof(Views.PlaceMapPage));
            NavigationService.Configure("LoginPage", typeof(Views.LoginPage));
            NavigationService.Configure("RegisterPage", typeof(Views.RegisterPage));
            NavigationService.Configure("SeeCommentsPage", typeof(Views.SeeCommentsPage));
            NavigationService.Configure("AddCommentPage", typeof(Views.AddCommentPage));
            NavigationService.Configure("ProfilePage", typeof(Views.ProfilePage));
            NavigationService.Configure("ChangePasswordPage", typeof(Views.ChangePasswordPage));
            var mainPage = ((ViewNavigationService)NavigationService).SetRootPage("LoginPage");
            MainPage = mainPage;
        }
        public static INavigationService NavigationService { get; } = new ViewNavigationService();

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
