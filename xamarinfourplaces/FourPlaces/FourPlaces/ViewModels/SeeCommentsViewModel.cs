﻿using Common.Api.Dtos;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ViewModels
{
    class SeeCommentsViewModel : ViewModelBase
    {
        private int _idPlace;
        private PlaceItem _placeItem;
        private List<CommentItem> _comments;
        public List<CommentItem> Comments
        {
            get => _comments;
            set => SetProperty(ref _comments, value);
        }
        public ICommand GoToAddCommentPageCommand { get; }
        public SeeCommentsViewModel(PlaceItem item)
        {
            _idPlace = item.Id;
            _placeItem = item;
            GetComments();
            GoToAddCommentPageCommand = new Command(GoToAddCommentPage);
        }
        public async void GetComments()
        {
            ApiClient apiClient = new ApiClient();
            HttpResponseMessage response = await apiClient.Execute(HttpMethod.Get, "https://td-api.julienmialon.com/places/"+_idPlace);
            Response<PlaceItem> result = await apiClient.ReadFromResponse<Response<PlaceItem>>(response);
            if (result.IsSuccess)
            {
                Comments = result.Data.Comments;
            }
        }
        public async void GoToAddCommentPage()
        {
            await App.NavigationService.NavigateAsync("AddCommentPage", _placeItem);
        }
    }
}
