﻿using Common.Api.Dtos;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ViewModels
{
    class ProfilePageViewModel : ViewModelBase
    {
        private string _firstNameEntry;
        private string _lastNameEntry;
        private string _emailEntry;
        private string _idImage;
        private UserItem _user;
        private bool _warningVisible;
        private string _warningText;
        public string FirstNameEntry
        {
            get => _firstNameEntry;
            set => SetProperty(ref _firstNameEntry, value);
        }
        public string LastNameEntry
        {
            get => _lastNameEntry;
            set => SetProperty(ref _lastNameEntry, value);
        }
        public string EmailEntry
        {
            get => _emailEntry;
            set => SetProperty(ref _emailEntry, value);
        }
        public string IdImageEntry
        {
            get => _idImage;
            set => SetProperty(ref _idImage, value);
        }
        public UserItem User
        {
            get => _user;
            set => SetProperty(ref _user, value);
        }
        public bool WarningVisible
        {
            get => _warningVisible;
            set => SetProperty(ref _warningVisible, value);
        }
        public string WarningText
        {
            get => _warningText;
            set => SetProperty(ref _warningText, value);
        }
        public ICommand UpdateUserCommand { get; }
        public ICommand GoToChangePasswordPageCommand { get; }
        public ProfilePageViewModel()
        {
            GetUser();
            UpdateUserCommand = new Command(UpdateUser);
            GoToChangePasswordPageCommand = new Command(GoToChangePasswordPage);
        }
        public async void GoToChangePasswordPage()
        {
            await App.NavigationService.NavigateAsync("ChangePasswordPage");
        }
        public async void GetUser()
        {
            ApiClient apiClient = new ApiClient();
            Console.WriteLine(App.token);
            HttpResponseMessage response = await apiClient.Execute(HttpMethod.Get, "https://td-api.julienmialon.com/me", token:App.token);
            Response<UserItem> result = await apiClient.ReadFromResponse<Response<UserItem>>(response);
            if (result.IsSuccess)
            {
                User = result.Data;
                FirstNameEntry = User.FirstName;
                LastNameEntry = User.LastName;
                //EmailEntry = User.Email;
                IdImageEntry = User.ImageId.ToString();
            }
        }
        public async void UpdateUser()
        {
            
            ApiClient apiClient = new ApiClient();
            UpdateProfileRequest updateProfileRequest = new UpdateProfileRequest();
            updateProfileRequest.FirstName = FirstNameEntry;
            updateProfileRequest.LastName = LastNameEntry;
            if (!string.IsNullOrWhiteSpace(IdImageEntry))
            {
                updateProfileRequest.ImageId = int.Parse(IdImageEntry);
            }
            else
            {
                updateProfileRequest.ImageId = null;
            }
            HttpResponseMessage response = await apiClient.Execute(new HttpMethod("PATCH"), "https://td-api.julienmialon.com/me", updateProfileRequest, App.token);
            Response<UserItem> result = await apiClient.ReadFromResponse<Response<UserItem>>(response);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Update Profile Success");
                WarningVisible = true;
                WarningText = "Mise à jour réussi";
                FirstNameEntry = result.Data.FirstName;
                LastNameEntry = result.Data.LastName;
                IdImageEntry = result.Data.ImageId.ToString();

            }
            else
            {
                Console.WriteLine("Update Profile Failed");
                Console.WriteLine(response.StatusCode);
                Console.WriteLine(response.ReasonPhrase);
                WarningVisible = true;
                WarningText = "Echec de la mise à jour";
            }
        }
    }
}
