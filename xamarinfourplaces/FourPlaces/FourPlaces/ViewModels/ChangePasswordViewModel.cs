﻿using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ViewModels
{
    class ChangePasswordViewModel : ViewModelBase
    {
        private string _oldPasswordEntry;
        private string _newPasswordEntry;
        private bool _warningVisible;
        private string _warningText;
        public string OldPasswordEntry
        {
            get => _oldPasswordEntry;
            set => SetProperty(ref _oldPasswordEntry, value);
        }
        public string NewPasswordEntry
        {
            get => _newPasswordEntry;
            set => SetProperty(ref _newPasswordEntry, value);
        }
        public bool WarningVisible
        {
            get => _warningVisible;
            set => SetProperty(ref _warningVisible, value);
        }
        public string WarningText
        {
            get => _warningText;
            set => SetProperty(ref _warningText, value);
        }
        public ICommand ChangePasswordCommand { get; }
        public ChangePasswordViewModel()
        {
            ChangePasswordCommand = new Command(ChangePassword);
        }
        public async void ChangePassword()
        {
            ApiClient apiClient = new ApiClient();
            UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest();
            updatePasswordRequest.OldPassword = OldPasswordEntry;
            updatePasswordRequest.NewPassword = NewPasswordEntry;
            HttpResponseMessage response = await apiClient.Execute(new HttpMethod("PATCH"), "https://td-api.julienmialon.com/me/password", updatePasswordRequest, App.token);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Update Password Success");
                WarningVisible = true;
                WarningText = "Mise à jour réussi";
                await App.NavigationService.NavigateAsync("ProfilePage");
            }
            else
            {
                Console.WriteLine("Update Password Failed");
                Console.WriteLine(response.StatusCode);
                Console.WriteLine(response.ReasonPhrase);
                WarningVisible = true;
                WarningText = "Echec de la mise à jour, veuillez remplir correctement les deux champs";
            }
        }
    }
}
