﻿using Common.Api.Dtos;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace FourPlaces.ViewModels
{
    class AddPlaceViewModel : ViewModelBase
    {
        private ICommand _takePictureCommand;
        private ICommand _selectPictureFromGalleryCommand;
        private ImageSource _imageSourceDisplay;
        private string _titleEntry;
        private string _descEntry;
        private double _latitudeEntry;
        private double _longitudeEntry;
        private bool _warningVisible;
        private string _warningText;
        private byte[] imageData;

        public ICommand FillLocationCommand { get; }
        public ICommand SendPlaceCommand { get; }
        public string TitleEntry
        {
            get => _titleEntry;
            set => SetProperty(ref _titleEntry, value);
        }
        public string DescEntry
        {
            get => _descEntry;
            set => SetProperty(ref _descEntry, value);
        }
        public double LatitudeEntry
        {
            get => _latitudeEntry;
            set => SetProperty(ref _latitudeEntry, value);
        }
        public double LongitudeEntry
        {
            get => _longitudeEntry;
            set => SetProperty(ref _longitudeEntry, value);
        }

        public ImageSource ImageSourceDisplay
        {
            get => _imageSourceDisplay;
            set => SetProperty(ref _imageSourceDisplay, value);
        }
        public bool WarningVisible
        {
            get => _warningVisible;
            set => SetProperty(ref _warningVisible, value);
        }
        public string WarningText
        {
            get => _warningText;
            set => SetProperty(ref _warningText, value);
        }

        public AddPlaceViewModel()
        {
            TakePictureCommand = new Command(async () => await TakePicture());
            SelectPictureFromGalleryCommand = new Command(async () => await SelectImageFromGallery());
            FillLocationCommand = new Command(FillLocationAsync);
            SendPlaceCommand = new Command(async() => await PostPlaceAndImage());
        }
        public ICommand TakePictureCommand
        {
            get => _takePictureCommand;
            set => SetProperty(ref _takePictureCommand, value);
        }
        public ICommand SelectPictureFromGalleryCommand
        {
            get => _selectPictureFromGalleryCommand;
            set => SetProperty(ref _selectPictureFromGalleryCommand, value);
        }

        private async void FillLocationAsync()
        {
            try
            {
                var location = await Geolocation.GetLocationAsync();
                if (location != null)
                {
                    WarningVisible = false;
                    LatitudeEntry = location.Latitude;
                    LongitudeEntry = location.Longitude;
                }
            }
            catch(Exception e)
            {
                WarningVisible = true;
                WarningText = "Impossible de récupérer les données gps, le gps est il activé ?";
            }
            
        }
        private async Task TakePicture()
        {
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                if (status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Camera))
                    {
                        await Application.Current.MainPage.DisplayAlert("Permission pour l'appareil photo", "Autoriser l'application à accéder à votre appareil photo", "Ok");
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera });
                    status = results[Permission.Camera];
                }

                if (status == PermissionStatus.Granted)
                {

                    await CrossMedia.Current.Initialize();

                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                    {
                        await Application.Current.MainPage.DisplayAlert("Pas d'appareil photo", "Aucun appareil photo disponible", "Ok");
                        return;
                    }

                    var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                    {
                        PhotoSize = PhotoSize.Small,

                    });

                    if (file == null)
                        return;


                    using (var memoryStream = new MemoryStream())
                    {
                        file.GetStream().CopyTo(memoryStream);
                        imageData = memoryStream.ToArray();
                    }

                    ImageSourceDisplay = ImageSource.FromFile(file.Path);


                }
                else if (status != PermissionStatus.Unknown)
                {
                    await Application.Current.MainPage.DisplayAlert("Appareil photo refusé", "Veuillez autoriser l'accès à l'appareil photo pour continuer", "Ok");
                }
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Erreur", "Appareil photo non disponible", "Ok");
            }
        }

        public async Task SelectImageFromGallery()
        {
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                Console.WriteLine("Photos Not Supported");
                return;
            }

            try
            {
                Stream stream = null;

                var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                {
                    PhotoSize = PhotoSize.Small
                });

                if (file == null)
                    return;

                using (var memoryStream = new MemoryStream())
                {
                    file.GetStream().CopyTo(memoryStream);
                    imageData = memoryStream.ToArray();
                }

                ImageSourceDisplay = ImageSource.FromStream(() =>
                {
                    stream = file.GetStream();
                    return stream;
                });

                using (var memoryStream = new MemoryStream())
                {
                    file.GetStream().CopyTo(memoryStream);
                    var myfile = memoryStream.ToArray();
                }

            }
            catch (Exception ex) { }
        }
        public async Task<int> PostImage()
        {
            int imageId = -1;
            if(imageData != null)
            {
                var imageContent = new ByteArrayContent(imageData);
                ApiClient apiClient = new ApiClient();
                HttpResponseMessage response = await apiClient.Execute(HttpMethod.Post, "https://td-api.julienmialon.com/images", imageContent);
                Response<ImageItem> result = await apiClient.ReadFromResponse<Response<ImageItem>>(response);

                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Image uploaded!");
                    Console.WriteLine("ImageId : " + result.Data.Id);
                    imageId = result.Data.Id;
                }
                else
                {
                    Console.WriteLine("Image upload failed");
                    Console.WriteLine("Status code : "+response.StatusCode);
                }
            }
            else
            {
                Console.WriteLine("No picture selected");
            }
            return imageId;
            
        }

        public async void PostPlace(int imageId)
        {
            ApiClient apiClient = new ApiClient();
            CreatePlaceRequest createPlaceRequest = new CreatePlaceRequest();
            createPlaceRequest.Title = TitleEntry;
            createPlaceRequest.Description = DescEntry;
            createPlaceRequest.ImageId = imageId;
            createPlaceRequest.Latitude = LatitudeEntry;
            createPlaceRequest.Longitude = LongitudeEntry;

            HttpResponseMessage response = await apiClient.Execute(HttpMethod.Post, "https://td-api.julienmialon.com/places", createPlaceRequest, App.token);
            if (response.IsSuccessStatusCode)
            {
                WarningVisible = true;
                WarningText = "Envoi en cours...";
            }
            else
            {
                WarningVisible = true;
                WarningText = "Echec de l'envoi";
            }
        }

        public async Task PostPlaceAndImage()
        {
            bool goToHomePage = false;
            await Task.Run(async () =>
            {
                WarningVisible = true;
                WarningText = "Envoi en cours...";
                
                if (!string.IsNullOrWhiteSpace(TitleEntry) || !string.IsNullOrWhiteSpace(DescEntry))
                {
                    var task = Task.Run(async () => await PostImage());
                    int imageId = task.Result;
                    if (imageId != -1)
                    {
                        Console.WriteLine("imageId.Result : " + imageId);
                        PostPlace(imageId);
                        goToHomePage = true;
                    }
                    else
                    {
                        WarningVisible = true;
                        WarningText = "Echec de l'envoi";
                    }
                }
                else
                {
                    WarningVisible = true;
                    WarningText = "Veuiller renseigner tous les champs";
                }
            });
            if (goToHomePage)
            {
                await App.NavigationService.NavigateAsync("HomePage");
            }
        }
    }
}
