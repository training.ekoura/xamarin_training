﻿using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using TD.Api.Dtos;
using System.Net.Http;
using Common.Api.Dtos;

namespace FourPlaces.ViewModels
{
    class RegisterPageViewModel : ViewModelBase
    {
        private string _firstNameEntry;
        private string _lastNameEntry;
        private string _emailEntry;
        private string _mdpEntry;
        private bool _warningVisible;
        private string _warningText;

        public string FirstNameEntry
        {
            get => _firstNameEntry;
            set => SetProperty(ref _firstNameEntry, value);
        }
        public string LastNameEntry
        {
            get => _lastNameEntry;
            set => SetProperty(ref _lastNameEntry, value);
        }
        public string EmailEntry
        {
            get => _emailEntry;
            set => SetProperty(ref _emailEntry, value);
        }
        public string MdpEntry
        {
            get => _mdpEntry;
            set => SetProperty(ref _mdpEntry, value);
        }

        public ICommand PostRegisterCommand { get; }
        public RegisterPageViewModel()
        {
            PostRegisterCommand = new Command(PostRegister);
        }
        public bool WarningVisible
        {
            get => _warningVisible;
            set => SetProperty(ref _warningVisible, value);
        }
        public string WarningText
        {
            get => _warningText;
            set => SetProperty(ref _warningText, value);
        }
        public async void PostRegister()
        {
            ApiClient apiClient = new ApiClient();
            RegisterRequest registerRequest = new RegisterRequest();
            registerRequest.FirstName = FirstNameEntry;
            registerRequest.LastName = LastNameEntry;
            registerRequest.Email = EmailEntry;
            registerRequest.Password = MdpEntry;
            HttpResponseMessage response = await apiClient.Execute(HttpMethod.Post, "https://td-api.julienmialon.com/auth/register", registerRequest);
            Response<LoginResult> result = await apiClient.ReadFromResponse<Response<LoginResult>>(response);
            if (result.IsSuccess)
            {
                WarningVisible = true;
                WarningText = "Inscription en cours...";
                App.token = result.Data.AccessToken;
                await App.NavigationService.NavigateAsync("HomePage");
            }
            else
            {
                WarningVisible = true;
                WarningText = "Inscription impossible";
            }
        }
    }
}
