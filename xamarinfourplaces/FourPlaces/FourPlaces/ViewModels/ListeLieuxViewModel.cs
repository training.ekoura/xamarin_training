﻿using Common.Api.Dtos;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ViewModels
{
    class ListeLieuxViewModel : ViewModelBase
    {
        private List<PlaceItem> _lieux;
        private ICommand _goToAddPlacePageCommand;
        private PlaceItem _selectedPlaceItem;

        public ICommand GoToProfilePageCommand { get; }

        public ListeLieuxViewModel()
        {
            GetLieux();
            GoToAddPlacePageCommand = new Command(async () => await GoToAddPlace());
            GoToProfilePageCommand = new Command(GoToProfilePage);
        }
        public ICommand GoToAddPlacePageCommand
        {
            get => _goToAddPlacePageCommand;
            set => SetProperty(ref _goToAddPlacePageCommand, value);
        }

        public PlaceItem SelectedPlaceItem
        {
            get => _selectedPlaceItem;
            set
            {
                if (SetProperty(ref _selectedPlaceItem, value) && value != null)
                {
                    GoToPlaceMap(value);
                    SelectedPlaceItem = null;
                }
            }
        }
        

        public List<PlaceItem> Lieux
        {
            get => _lieux;
            set => SetProperty(ref _lieux, value);
        }

        public async void GetLieux()
        {
            ApiClient apiClient = new ApiClient();
            HttpResponseMessage response = await apiClient.Execute(HttpMethod.Get, "https://td-api.julienmialon.com/places");
            Response<List<PlaceItem>> result = await apiClient.ReadFromResponse<Response<List<PlaceItem>>>(response);
            if (result.IsSuccess)
            {
                Lieux = result.Data;
            }
        }
        public async Task GoToAddPlace()
        {
            await App.NavigationService.NavigateAsync("AddPlacePage");
        }
        public async void GoToPlaceMap(PlaceItem item)
        {
            await App.NavigationService.NavigateAsync("PlaceMapPage", item);
        }
        public async void GoToProfilePage()
        {
            await App.NavigationService.NavigateAsync("ProfilePage");
        }


    }
}
