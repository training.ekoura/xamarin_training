﻿using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ViewModels
{
    class PlaceMapViewModel : ViewModelBase
    {
        private PlaceItem _pItem;
        public ICommand GoToSeeCommentsCommand { get; }
        public PlaceMapViewModel(PlaceItem place)
        {
            _pItem = place;
            GoToSeeCommentsCommand = new Command(GoToSeeComments);
        }
        public async void GoToSeeComments()
        {
            await App.NavigationService.NavigateAsync("SeeCommentsPage", _pItem);
        }
    }
}
